CREATE TABLE SS_MESSAGE_MEDICATION
(
	ID							 VARCHAR2(100 BYTE),
	MESSAGEID                    VARCHAR2(100 BYTE),
	MEDICATIONTYPE               VARCHAR2(100 BYTE),
	MEDICATIONDESCRIPTION        VARCHAR2(400 BYTE),
	MEDICATIONCODE               VARCHAR2(20 BYTE),
	MEDICATIONCODETYPE           VARCHAR2(2 BYTE),
	MEDICATIONQUANTITYQUAL       VARCHAR2(15 BYTE),
	MEDICATIONQUANTITY           VARCHAR2(20 BYTE),
	MEDICATIONDIRECTIONS         VARCHAR2(400 BYTE),
	MEDICATIONNOTE               VARCHAR2(400 BYTE),
	MEDICATIONREFILLQUAL         VARCHAR2(3 BYTE),
	MEDICATIONREFILLQUAN         VARCHAR2(20 BYTE),
	MEDICATIONSUBSTITUTION       VARCHAR2(10 BYTE),
	MEDICATIONWRITTENDATE        VARCHAR2(20 BYTE),
	MEDICATIONDOSAGEFORM         VARCHAR2(3 BYTE),
	MEDICATIONSTRENGTH           VARCHAR2(70 BYTE),
	MEDICATIONSTRENGTHUNITS      VARCHAR2(2 BYTE),
	MEDICATIONDRUGDBCODE         VARCHAR2(35 BYTE),
	MEDICATIONDRUGDBCODEQUAL     VARCHAR2(3 BYTE),
	MEDICATIONLASTFILLDATE       VARCHAR2(20 BYTE),
	MEDICATIONDAYSSUPPLY         VARCHAR2(3 BYTE),
	MEDICATIONCLINICALINFOQUAL   VARCHAR2(40 BYTE),
	MEDICATIONPRIMARYQUAL        VARCHAR2(20 BYTE),
	MEDICATIONPRIMARYVALUE       VARCHAR2(40 BYTE),
	MEDICATIONSECONDARYQUAL      VARCHAR2(20 BYTE),
	MEDICATIONSECONDARYVALUE     VARCHAR2(40 BYTE),
	RAWCONTENT					 VARCHAR2(4000 BYTE)
);
ALTER TABLE SS_MESSAGE_MEDICATION ADD (PRIMARY KEY (ID));

ALTER TABLE SS_MESSAGE ADD CHANGETYPE VARCHAR2(1);

ALTER TABLE SS_MESSAGE_CONTENT ADD COOINCLUDED NUMBER(1);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOPAYERMUTUALLYDEFINED VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOPAYERBINLOCATIONNUMBER VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOPAYERPROCESSORIDNUMBER VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOPAYERID VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOPAYERNAME VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOCARDHOLDERID VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOCARDHOLDERFIRSTNAME VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOCARDHOLDERLASTNAME VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOCARDHOLDERMIDDLENAME VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOCARDHOLDERSUFFIX VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOCARDHOLDERPREFIX VARCHAR2(40 BYTE);
ALTER TABLE SS_MESSAGE_CONTENT ADD COOGROUPID VARCHAR2(40 BYTE);

ALTER TABLE SS_MESSAGE_CONTENT ADD MEDICATIONDUE VARCHAR2(1000 BYTE);

CREATE TABLE SS_EXTERNAL_CODE
(
	TYPE						 VARCHAR2(100 BYTE),
	CODE	                     VARCHAR2(20 BYTE),
	DESCRIPTION	                 VARCHAR2(4000 BYTE)
);

INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'AD', 'Additional Drug Needed-Code indicating optimal treatment of the patient condition requiring the addition of a new drug to the existing drug therapy');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'AN', 'Prescription Authentication-Code indicating that circumstances required the pharmacist to verify the validity and/or authenticity of the prescription');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'AR', 'Adverse Drug Reaction-Code indicating an adverse reaction by a patient to a drug');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'AT', 'Additive Toxicity-Code indicating a detection of drugs with similar side effects when used in combination could exhibit a toxic potential greater than either agent by itself');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'CD', 'Chronic Disease Management-The patient is participating in a coordinated health care intervention program');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'CH', 'Call Help Desk-Processor message to recommend the receiver contact the processor/plan');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'CS', 'Patient Complaint/Symptom-Code indicating that in the course of assessment or discussion with the patient, the pharmacist identified an actual or potential problem when the patient presented to the pharmacist complaints or symptoms suggestive of illness requesting evaluation and treatment');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DA', 'Drug-Allergy-Indicates that an adverse immune event may occur due to the patient previously demonstrated heightened allergic response to the drug product in question');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DC', 'Drug-Disease (Inferred)-Indicates that the use of the drug may be inappropriate in light of a specific medical condition that the patient has. The existence of the specific medical condition is inferred from drugs in the patient medication history');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DD', 'Drug-Drug Interaction-Indicates that drug combinations in which the net pharmacologic response may be different from the result expected when each drug is given separately');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DF', 'Drug-Food interaction-Indicates interactions between a drug and certain foods');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DI', 'Drug Incompatibility-Indicates physical and chemical incompatibilities between two or more drugs');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DL', 'Drug-Lab Conflict�Indicates that laboratory values may be altered due to the use of the drug, or that the patient response to the drug may be altered due to a condition that is identified by a certain laboratory value');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DM', 'Apparent Drug Misuse-Code indicating a pattern of drug use by a patient in a manner that is significantly different than that prescribed by the prescriber');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DR', 'Dose Range Conflict-Code indicating that the prescription does not follow recommended medication dosage');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'DS', 'Tobacco Use-Code indicating that a conflict was detected when a prescribed drug is contraindicated or might conflict with the use of tobacco products');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'ED', 'Patient Education/Instruction-Code indicating that a cognitive service whereby the pharmacist performed a patient care activity by providing additional instructions or education to the patient beyond the simple task of explaining the prescriber instructions on the prescription');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'ER', 'Overuse-Code indicating that the current prescription refill is occurring before the days supply of the previous filling should have been exhausted');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'EX', 'Excessive Quantity-Code that documents the quantity is excessive for the single time period for which the drug is being prescribed');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'HD', 'High Dose-Detects drug doses that fall above the standard dosing range');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'IC', 'Iatrogenic Condition-Code indicating that a possible inappropriate use of drugs that are designed to amerliorate complications caused by another medication has been detected');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'ID', 'Ingredient Duplication-Code indicating that simultaneous use of drug products containing one or more identical generic chemical entities has been detected');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'LD', 'Low Dose-Code indicating that the submitted drug doses fall below the standard dosing range');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'LK', 'Lock In Recipient-Code indicating that the professional service was related to a plan/payer constraint on the member whereby the member is required to obtain services from only one specified pharmacy or other provider type, hence the member is �locked in� to using only those providers or pharmacies');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'LR', 'Underuse-Code indicating that a prescription refill that occurred after the days supply of the previous filling should have been exhausted');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'MC', 'Drug-Disease (Reported)-Indicates that the use of the drug may be inappropriate in light of a specific medical condition that the patient has. Information about the specific medical condition was provided by the prescriber, patient or pharmacist');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'MN', 'Insufficient Duration-Code indicating that regimens shorter than the minimal limit of therapy for the drug product, based on the product common uses, has been detected');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'MS', 'Missing Information/Clarification-Code indicating that the prescription order is unclear, incomplete, or illegible with respect to essential information');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'MX', 'Excessive Duration-Detects regimens that are longer than the maximal limit of therapy for a drug product based on the product common uses');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'NA', 'Drug Not Available-Indicates the drug is not currently available from any source');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'NC', 'Non-covered Drug Purchase-Code indicating a cognitive service whereby a patient is counseled, the pharmacist recommendation is accepted and a claim is submitted to the processor requesting payment for the professional pharmacy service only, not the drug');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'ND', 'New Disease/Diagnosis-Code indicating that a professional pharmacy service has been performed for a patient who has a newly diagnosed condition or disease');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'NF', 'Non-Formulary Drug-Code indicating that mandatory formulary enforcement activities have been performed by the pharmacist when the drug is not included on the formulary of the patient pharmacy benefit plan');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'NN', 'Unnecessary Drug-Code indicating that the drug is no longer needed by the patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'NP', 'New Patient Processing-Code indicating that a pharmacist has performed the initial interview and medication history of a new patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'NR', 'Lactation/Nursing Interaction-Code indicating that the drug is excreted in breast milk and may represent a danger to a nursing infant');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'NS', 'Insufficient Quantity-Code indicating that the quantity of dosage units prescribed is insufficient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'OH', 'Alcohol Conflict-Detects when a prescribed drug is contraindicated or might conflict with the use of alcoholic beverages');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'PA', 'Drug-Age-Indicates age-dependent drug problems');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'PC', 'Patient Question/Concern-Code indicating that a request for information/concern was expressed by the patient, with respect to patient care');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'PG', 'Drug-Pregnancy-Indicates pregnancy related drug problems. This information is intended to assist the healthcare professional in weighing the therapeutic value of a drug against possible adverse effects on the fetus');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'PH', 'Preventive Health Care-Code indicating that the provided professional service was to educate the patient regarding measures mitigating possible adverse effects or maximizing the benefits of the product(s) dispensed; or measures to optimize health status, prevent recurrence or exacerbation of problems');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'PN', 'Prescriber Consultation-Code indicating that a prescriber has requested information or a recommendation related to the care of a patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'PP', 'Plan Protocol-Code indicating that a cognitive service whereby a pharmacist, in consultation with the prescriber or using professional judgment, recommends a course of therapy as outlined in the patient plan and submits a claim for the professional service provided');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'PR', 'Prior Adverse Reaction-Code identifying the patient has had a previous atypical reaction to drugs');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'PS', 'Product Selection Opportunity-Code indicating that an acceptable generic substitute or a therapeutic equivalent exists for the drug. This code is intended to support discretionary drug product selection activities by pharmacists');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'RE', 'Suspected Environmental Risk-Code indicating that the professional service was provided to obtain information from the patient regarding suspected environmental factors');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'RF', 'Health Provider Referral-Patient referred to the pharmacist by another health care provider for disease specific or general purposes');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'SC', 'Suboptimal Compliance-Code indicating that professional service was provided to counsel the patient regarding the importance of adherence to the provided instructions and of consistent use of the prescribed product including any ill effects anticipated as a result of non-compliance');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'SD', 'Suboptimal Drug/Indication-Code indicating incorrect, inappropriate, or less than optimal drug prescribed for the patient condition');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'SE', 'Side Effect-Code reporting possible major side effects of the prescribed drug');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'SF', 'Suboptimal Dosage Form-Code indicating incorrect, inappropriate, or less than optimal dosage form for the drug');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'SR', 'Suboptimal Regimen-Code indicating incorrect, inappropriate, or less than optimal dosage regimen specified for the drug in question');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'SX', 'Drug-Gender-Indicates the therapy is inappropriate or contraindicated in either males or females');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'TD', 'Therapeutic-Code indicating that a simultaneous use of different primary generic chemical entities that have the same therapeutic effect was detected');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'TN', 'Laboratory Test Needed-Code indicating that an assessment of the patient suggests that a laboratory test is needed to optimally manage a therapy');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'TP', 'Payer/Processor Question-Code indicating that a payer or processor requested information related to the care of a patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-REASONCODE', 'UD', 'Duplicate Drug-Code indicating that multiple prescriptions of the same drug formulation are present in the patient current medication profile');


INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', '00', 'No intervention');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'AS', 'Patient assessment-Code indicating that an initial evaluation of a patient or complaint/symptom forthe purpose of developing a therapeutic plan');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'CC', 'Coordination of care-Case management activities of a pharmacist related to the care beingdelivered by multiple providers');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'DE', 'Dosing evaluation/determination-Cognitive service whereby the pharmacist reviews and evaluatesthe appropriateness of a prescribed medication�s dose, interval, frequency and/or formulation');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'DP', 'Dosage evaluated-Code indicating that dosage has been evaluated with respect to risk for thepatient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'FE', 'Formulary enforcement-Code indicating that activities including interventions with prescribers andpatients related to the enforcement of a pharmacy benefit plan formulary have occurred. Comment:Use this code for cross-licensed brand products or generic to brand interchange');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'GP', 'Generic product selection-The selection of a chemically and therapeutically identical product to thatspecified by the prescriber for the purpose of achieving cost savings for the payer');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'M0', 'Prescriber consulted-Code indicating prescriber communication related to collection of informationor clarification of a specific limited problem');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'MA', 'Medication administration-Code indicating an action of supplying a medication to a patient throughany of several routes�oral, topical, intravenous, intramuscular, intranasal, etc.');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'MB', 'Overriding benefit-Benefits of the prescribed medication outweigh the risks');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'MP', 'Patient will be monitored-Prescriber is aware of the risk and will be monitoring the patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'MR', 'Medication review-Code indicating comprehensive review and evaluation of a patient�s entiremedication regimen');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'PA', 'Previous patient tolerance-Patient has taken medication previously without issue');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'PE', 'Patient education/instruction-Code indicating verbal and/or written communication by apharmacist to enhance the patient�s knowledge about the condition under treatment or to developskills and competencies related to its management');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'PH', 'Patient medication history-Code indicating the establishment of a medication history database on apatient to serve as the foundation for the ongoing maintenance of a medication profile');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'PM', 'Patient monitoring-Code indicating the evaluation of established therapy for the purpose ofdetermining whether an existing therapeutic plan should be altered');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'P0', 'Patient consulted-Code indicating patient communication related to collection of information orclarification of a specific limited problem');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'PT', 'Perform laboratory test-Code indicating that the pharmacist performed a clinical laboratory test ona patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'R0', 'Pharmacist consulted other source-Code indicating communication related to collection ofinformation or clarification of a specific limited problem');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'RT', 'Recommend laboratory test-Code indicating that the pharmacist recommends the performance of aclinical laboratory test on a patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'SC', 'Self-care consultation-Code indicating activities performed by a pharmacist on behalf of a patientintended to allow the patient to function more effectively on his or her own behalf in healthpromotion and disease prevention, detection, or treatment');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'SW', 'Literature search/review-Code indicating that the pharmacist searches or reviews thepharmaceutical and/or medical literature for information related to the care of a patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'TC', 'Payer/processor consulted-Code indicating communication by a pharmacist to a processor or payerrelated to the care of the patient');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-SERVICECODE', 'TH', 'Therapeutic product interchange-Code indicating that the selection of a therapeutically equivalentproduct to that specified by the prescriber for the purpose of achieving cost savings for the payer');


INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '00', 'Not Specified');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1A', 'Filled As Is, False Positive-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert) and determines the alert is incorrect for that prescription for that patientand fills the prescription as originally written');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1B', 'Filled Prescription As Is-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert) and determines the alert is not relevant for that prescription for that patientand fills the prescription as originally written');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1C', 'Filled, With Different Dose-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert) and fills the prescription with a different dose than was originally prescribed');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1D', 'Filled, With Different Directions-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert) and fills the prescription with different directions than were originally prescribed');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1E', 'Filled, With Different Drug-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert) and fills the prescription with a different drug than was originally prescribed');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1F', 'Filled, With Different Quantity-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert) and fills the prescription with a different quantity than was originally prescribed');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1G', 'Filled, With Prescriber Approval-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert) and fills the prescription after consulting with or obtaining approval from the prescriber');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1H', 'Brand-to-Generic Change-Action whereby a pharmacist dispenses the generic formulation of anoriginally prescribed branded product. Allowed, often mandated, unless the prescriber indicates �DoNot Substitute� on the prescription');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1J', 'Rx-to-OTC Change-Code indicating a cognitive service. The pharmacist reviews and evaluates atherapeutic issue (alert) fills the prescription with an over-the-counter product in lieu of the originally prescribed prescription-only product');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '1K', 'Filled with Different Dosage Form-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert) and fills the prescription with a different dosage form than was originally prescribed');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '2A', 'Prescription Not Filled-Code indicating a cognitive service. The pharmacist reviews and evaluates atherapeutic issue (alert) and determines that the prescription should not be filled as written');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '2B', 'Not Filled, Directions Clarified-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert), consults with the prescriber or using professional judgment, does not fill theprescription and counsels the patient as to the prescriber instructions');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3A', 'Recommendation Accepted-Code indicating a cognitive service. The pharmacist reviews andevaluates a therapeutic issue (alert), recommends a more appropriate product or regimen thendispenses the alternative after consultation with the prescriber');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3B', 'Recommendation Not Accepted-Code indicating a cognitive service. The pharmacist reviews andevaluates a therapeutic issue (alert), recommends a more appropriate product or regimen but the prescriber does not concur');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3C', 'Discontinued Drug-Cognitive service involving the pharmacist review of drug therapy that results inthe removal of a medication from the therapeutic regimen');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3D', 'Regimen Changed-Code indicating a cognitive service. The pharmacist reviews and evaluates atherapeutic issue (alert), recommends a more appropriate regimen then dispenses the recommendedmedication(s) after consultation with the prescriber');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3E', 'Therapy Changed-Code indicating a cognitive service. The pharmacist reviews and evaluates atherapeutic issue (alert), recommends a more appropriate product or regimen then dispenses thealternative after consultation with the prescriber');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3F', 'Therapy Changed cost increased acknowledged-Code indicating a cognitive service. The pharmacistreviews and evaluates a therapeutic issue (alert), recommends a more appropriate product orregimen acknowledging that a cost increase will be incurred, then dispenses the alternative afterconsultation with the prescriber');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3G', 'Drug Therapy Unchanged-Cognitive service whereby the pharmacist reviews and evaluates atherapeutic issue (alert), consults with the prescriber or uses professional judgment andsubsequently fills the prescription as originally written');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3H', 'Follow-Up/Report-Code indicating that additional follow through by the pharmacist is required');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3J', 'Patient Referral-Code indicating the referral of a patient to another health care provider followingevaluation by the pharmacist');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3K', 'Instructions Understood-Indicator used to convey that the patient affirmed understanding of theinstructions provided by the pharmacist regarding the use and handling of the medication dispensed');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3M', 'Compliance Aid Provided-Cognitive service whereby the pharmacist supplies a product that assiststhe patient in complying with instructions for taking medications');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '3N', 'Medication Administered-Cognitive service whereby the pharmacist performs a patient care activityby personally administering the medication');
INSERT INTO SS_EXTERNAL_CODE (TYPE, CODE, DESCRIPTION) VALUES ('DRU-100-RESULTCODE', '4A', 'Prescribed with acknowledgements-Physician is prescribing this medication with knowledge of thepotential conflict');

COMMIT;