﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using System.Text.RegularExpressions;
using EPCSSignature;

public partial class SureScriptInterface_EPCS_Signature_Page : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request["runType"]))
        {
            string propertyPath = ConfigurationManager.AppSettings["OTPPropertyPath"];

            string runType = Request["runType"].ToString();
            if (runType.Equals("sign"))
            {
                Response.Write("This url is expired!");
                Response.End(); 
                OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                string insertSQL = "INSERT INTO EPCS_PRESCRIPTIONS " +
                    " (Drug_Id, Visit_Key, NCPDPID, SS_MESSAGE_ID, Status, Doctor_Id, Patient_Id, Created_Date, ENCRYPTED_CONTENT, ENCRYPTED_KEY, ENCRYPTED_IV, SIGNATURE) " +
                    " VALUES ('848911', '846571drsyedZ771571', '9911557', '3f370ea643b4403b84d0d8a7aca62a93', 'NEWRX--OK', 'drsyed', 'Z771571', to_date('6/1/2015 2:20:05 AM','MM/DD/YYYY hh12:mi:ss AM'), " +
                    " :ParamEncryptedContent, :ParamEncryptedKey, :ParamEncryptedIV, :ParamSignature)";

                OracleCommand cmd = new OracleCommand(insertSQL, connection);

                
                string Medication_Description = "ATROPINE SO4-DIPHENOXYLATE HCL 0.025 MG-2.5 MG TABLET";
                string Medication_Quantity = "30";
                string Medication_RefillQuan = "0";
                string Patient_FirstName = "FELICIA";
                string Patient_LastName = "FLOUNDERS";
                string Patient_Address = "ABC";
                string Patient_City = "ABC";
                string Patient_State = "ABC";
                string Patient_Zip = "12345";
                string PrescriberAgent_FirstName = "Aleem";
                string PrescriberAgent_LastName = "Syed";
                string Prescriber_Address = "1211 W.La Palma Ave  Suite 702";
                string Doctor_DEA = "BS4237625";

                EPCS_Signature epcsSignature = new EPCS_Signature(propertyPath);

                string currentData = epcsSignature.CreateInput(Medication_Description, Medication_Quantity, Medication_RefillQuan, 
                    Patient_FirstName, Patient_LastName, Patient_Address, Patient_City, Patient_State, Patient_Zip,
                    PrescriberAgent_FirstName, PrescriberAgent_LastName, Prescriber_Address, Doctor_DEA);                                

                byte[] encryptedContent, Key, IV;
                encryptedContent = epcsSignature.EncryptBySymmetricKey(currentData, out Key, out IV);

                byte[] encryptedKey = epcsSignature.EncryptByAsymmetricKey(Key);
                byte[] encryptedIV = epcsSignature.EncryptByAsymmetricKey(IV);

                byte[] signature = epcsSignature.Sign(currentData);
                
                cmd.Parameters.Add(new OracleParameter("ParamEncryptedContent", OracleType.Blob)).Value = encryptedContent;
                cmd.Parameters.Add(new OracleParameter("ParamEncryptedKey", OracleType.Blob)).Value = encryptedKey;
                cmd.Parameters.Add(new OracleParameter("ParamEncryptedIV", OracleType.Blob)).Value = encryptedIV;
                cmd.Parameters.Add(new OracleParameter("ParamSignature", OracleType.Blob)).Value = signature;

                connection.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                catch (Exception ex)
                {
                    Response.Write("Error: " + ex.Message);
                    Response.End(); 
                }
                finally
                {
                    connection.Close();
                }                

                Response.Write("Inserted Successfully!");
                Response.End(); 
            }
            else if (runType.Equals("verify"))
            {
                if (!String.IsNullOrEmpty(Request["ssMessageId"]))
                {
                    string ssMessageId = Request["ssMessageId"].ToString();

                    OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                    //string selectSQL = "SELECT Encrypted_Content, Encrypted_Key, Encrypted_IV, Signature FROM EPCS_PRESCRIPTIONS WHERE SS_MESSAGE_ID = '" + ssMessageId  + "' and rownum = 1";

                    OracleCommand cmd = new OracleCommand();

                    connection.Open();
                    cmd.Connection = connection;
                    cmd.CommandText = "WSEPCS.GetPrescriptionDetailForVerify";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new OracleParameter("ParamSSMessageId", OracleType.VarChar)).Value = ssMessageId;

                    cmd.Parameters.Add(new OracleParameter("CurReport", OracleType.Cursor)).Direction = ParameterDirection.Output;
                    

                    try
                    {
                        OracleDataReader reader = cmd.ExecuteReader();

                        EPCS_Signature epcsSignature = new EPCS_Signature(propertyPath);

                        byte[] encryptedContent = null;
                        byte[] encrypted_Key = null;
                        byte[] encrypted_IV = null;
                        byte[] signature = null;

                        string currentData = "";

                        while (reader.Read())
                        {
                            OracleLob oLob1 = reader.GetOracleLob(0);
                            encryptedContent = new byte[oLob1.Length];
                            oLob1.Read(encryptedContent, 0, encryptedContent.Length);

                            OracleLob oLob2 = reader.GetOracleLob(1);
                            encrypted_Key = new byte[oLob2.Length];
                            oLob2.Read(encrypted_Key, 0, encrypted_Key.Length);

                            OracleLob oLob3 = reader.GetOracleLob(2);
                            encrypted_IV = new byte[oLob3.Length];
                            oLob3.Read(encrypted_IV, 0, encrypted_IV.Length);

                            OracleLob oLob4 = reader.GetOracleLob(3);
                            signature = new byte[oLob4.Length];
                            oLob4.Read(signature, 0, signature.Length);

                            string Medication_Description = reader.GetOracleString(4).IsNull ? "" : reader.GetOracleString(4).ToString();
                            string Medication_Quantity = reader.GetOracleString(5).IsNull ? "" : reader.GetOracleString(5).ToString();
                            string Medication_RefillQuan = reader.GetOracleString(6).IsNull ? "" : reader.GetOracleString(6).ToString();
                            string Patient_FirstName = reader.GetOracleString(7).IsNull ? "" : reader.GetOracleString(7).ToString();
                            string Patient_LastName = reader.GetOracleString(8).IsNull ? "" : reader.GetOracleString(8).ToString();
                            string Patient_Address = reader.GetOracleString(9).IsNull ? "" : reader.GetOracleString(9).ToString();
                            string Patient_City = reader.GetOracleString(10).IsNull ? "" : reader.GetOracleString(10).ToString();
                            string Patient_State = reader.GetOracleString(11).IsNull ? "" : reader.GetOracleString(11).ToString();
                            string Patient_Zip = reader.GetOracleString(12).IsNull ? "" : reader.GetOracleString(12).ToString();
                            string PrescriberAgent_FirstName = reader.GetOracleString(13).IsNull ? "" : reader.GetOracleString(13).ToString();
                            string PrescriberAgent_LastName = reader.GetOracleString(14).IsNull ? "" : reader.GetOracleString(14).ToString();
                            string Prescriber_Address = reader.GetOracleString(15).IsNull ? "" : reader.GetOracleString(15).ToString();
                            string Doctor_DEA = reader.GetOracleString(16).IsNull ? "" : reader.GetOracleString(16).ToString();

                            currentData = epcsSignature.CreateInput(Medication_Description, Medication_Quantity, Medication_RefillQuan, 
                                                        Patient_FirstName, Patient_LastName, Patient_Address, Patient_City, Patient_State, Patient_Zip,
                                                        PrescriberAgent_FirstName, PrescriberAgent_LastName, Prescriber_Address, Doctor_DEA);
                        }

                        if (signature != null && signature.Length > 0)
                        {
                            bool isValid = epcsSignature.Verify(currentData, signature);
                            if (isValid)
                            {
                                //Response.Write("Signature is valid!");
                                Response.Write("VALID");
                            }
                            else
                            {
                                //Response.Write("Signature is not valid!");

                                string result = epcsSignature.CheckChanges(currentData, encryptedContent, encrypted_Key, encrypted_IV);
                                Response.Write(result);
                            }
                        }
                        else
                        {
                            Response.Write("Error: Can not retrieve signature!");
                        }

                        cmd.Dispose();
                    }
                    catch (Exception ex)
                    {
                        WEBeDoctorPharmacy.SS_Logger.log("Error in verifying this prescription. SS_MESSAGE_ID = " + ssMessageId + ". Detail: " + ex.Message);
                        Response.Write("Error in verifying this prescription.");
                        Response.End();
                    }
                    finally
                    {
                        connection.Close();
                    }

                    Response.End();
                }
            }
            else if (runType.Equals("check"))
            {
                Response.Write("This url is expired!");
                Response.End();
                if (!String.IsNullOrEmpty(Request["ssMessageId"]))
                {
                    string ssMessageId = Request["ssMessageId"].ToString();

                    OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                    string selectSQL = "SELECT Encrypted_Content, Encrypted_Key, Encrypted_IV FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '123' and rownum = 1";

                    OracleCommand cmd = new OracleCommand(selectSQL, connection);

                    connection.Open();

                    try
                    {
                        OracleDataReader reader = cmd.ExecuteReader();

                        EPCS_Signature epcsSignature = new EPCS_Signature(propertyPath);

                        byte[] encryptedContent = null;
                        byte[] encrypted_Key = null;
                        byte[] encrypted_IV = null;

                        while (reader.Read())
                        {
                            OracleLob oLob1 = reader.GetOracleLob(0);
                            encryptedContent = new byte[oLob1.Length];
                            oLob1.Read(encryptedContent, 0, encryptedContent.Length);

                            OracleLob oLob2 = reader.GetOracleLob(1);
                            encrypted_Key = new byte[oLob2.Length];
                            oLob2.Read(encrypted_Key, 0, encrypted_Key.Length);

                            OracleLob oLob3 = reader.GetOracleLob(2);
                            encrypted_IV = new byte[oLob3.Length];
                            oLob3.Read(encrypted_IV, 0, encrypted_IV.Length);

                        }

                        if (encryptedContent != null && encryptedContent.Length > 0 &&
                            encrypted_Key != null && encrypted_Key.Length > 0 &&
                            encrypted_IV != null && encrypted_IV.Length > 0)
                        {
                            string text = "Hello My World1";

                            string result = epcsSignature.CheckChanges(text, encryptedContent, encrypted_Key, encrypted_IV);
                            Response.Write(result);

                        }
                        else
                        {
                            Response.Write("Error: Can not retrieve signature!");
                        }

                        cmd.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Response.Write("Error: " + ex.Message);
                        Response.End();
                    }
                    finally
                    {
                        connection.Close();
                    }

                    Response.End();
                }
            }
        }
        Response.Write("Invalid Input!");
    }
}