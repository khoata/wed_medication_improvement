﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.OracleClient;


public class PDRDAL
{
	public PDRDAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable Get_PhyLocation_Info(string facilityId)
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "select streetadd as code, orgname as name, addr1 ||'  ' || addr2 as address, city, state,zip, tel as phone, fax   from streetreg where streetadd = '" + facilityId + "'";
        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }

    public static DataTable Get_Patient_Info(string MRNID)
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "select userid as code, first_Name as firstName,last_Name as lastName, birth_date as dob,gender, street as address, city,state,zip,home_phone as phone from patienttable where userid = '" + MRNID + "'";

        //string queryPPortal = "select userid as code, first_Name as firstName,last_Name as lastName, birth_date as dob,gender, street as address, city,state,zip,home_phone as phone from patienttable where userid = '" + MRNID + "'";


        

        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }

    public static DataTable Check_PDR_Access(string Med_Id)
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "select e.streetadd as org_code, e.orgname as name, e.addr1 ||'  ' || e.addr2 as org_address, e.city as org_city, e.state as org_state,e.zip as org_zip, e.tel as org_phone, e.fax as org_fax, c.userid as pat_code,c.first_Name as patient_firstName,c.last_Name as patient_lastName,c. birth_date as patient_dob,c.gender as patient_gender, c.street as patient_address, c.city as patient_city,c.state as patient_state,c.zip as patient_zip,c.home_phone as patient_phone,  b.patientcomm_access,  b.docuser,b.npi as phy_npi,  a.* from notes_medications a, doctortable b, patienttable c , streetreg e where a.prescribed_by= b.docuser and a.patient_id = c.userid and c.facility_id = e.streetadd and a.drug_id= '" + Med_Id + "'";

        //UtilityMethods.AppLogEntry_Pat("Query: " + queryPPortal); 

        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }

    public static DataTable Check_PhyCare_Access(string Docuser, string MRN_ID)
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "select a.physiciancare_access,a.doctor_id,a.state, a.zip,a.npi, a.speciality, b.orgname,b.tel, b.fax, c.userid as Patient_Code, c.first_name as patient_first_name, c.last_name as patient_last_name, c.birth_date, C.GENDER as patient_gender, c.state as Patient_state   from doctortable a,streetreg b, patienttable c where a.FACILITY= b.streetadd and a.FACILITY= C.FACILITY_ID and a.physiciancare_access is not null and a.docuser = '" + Docuser + "' and c.userid = '" + MRN_ID + "'";
                
        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }

    public static DataTable Get_NDC_Code(string BrandCode,string Main_Multum_Drug_code )
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "select ndc_code from ndc_core_description where (OBSELETE_DATE is null or OBSELETE_DATE > SYSDATE) and repackaged = 0 and brand_code='" + BrandCode + "' and main_multum_drug_code= '" + Main_Multum_Drug_code + "' and rownum < 2" ; 
        
        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }

    public static DataTable Get_Immunization_Info(string Visit_key)
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "select cvx_code from notes_immunization where cvx_code is not null and  status= 'Given' and  visit_key = '" + Visit_key + "'";

        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }

    public static DataTable Get_Allergy_Info(string Visit_key)
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "select allergy_text from notes_allergy where allergy_text is not null and visit_key =  '" + Visit_key + "'";

        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }

    public static DataTable Get_Lab_Info(string Visit_key)
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "SELECT result_loinc as lab_code from labcorp_loinc where  RESULT_LOINC != 'UNLOINC' and  ORDER_CODE in (select B.LABCORP_TEST_NUMBER from labcorp_orders a,labcorp_order_data b where a.order_id = b.order_id and a.visit_key = '" + Visit_key + "')";
        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }

    public static DataTable Get_Diag_Info(string Visit_key)
    {
        string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
        OracleConnection oraConn = new OracleConnection(strConn);
        string queryPPortal = "select DIAG_CODE,DIAG_DESC from NOTES_PROBLEMLIST where visit_key = '" + Visit_key + "'";
        try
        {
            oraConn.Open();
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, queryPPortal);

            if (ds == null)
            {
                return null;
            }
            else
            {
                DataTable dt = ds.Tables[0];
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oraConn.Close();
        }

    }
}