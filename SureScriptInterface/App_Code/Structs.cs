﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for Structs
/// </summary>
public class Patient_Comm_Structs
{

    public static RequestAttribues RequestAtt = new RequestAttribues();
    public static RequestParameters RequestParams = new RequestParameters();
    public static PatientInformation PatientInfo = new PatientInformation();
    public static PhysicianLocation PhysicianLoc = new PhysicianLocation();

    public struct RequestAttribues
    {
        public string source;
        public string portalID;
        public string pFormat;
        public string wFormat;
        public string output;
    }

    public struct RequestParameters
    {
        public string rxNumber;
        public string productRxNorm;
        public string productNDC;
        public string refillsTotal;
        public string quantity;
        public string daysSupplied;
        public string sig;
        public string dosageForm;
        public string dose;
        public string frequency;
        public string routeOfAdministration;
        public string renewalIndicator;
        public string prescriptionForm;
        public string priorAuthorization;
    }

    public struct PatientInformation
    {
        public string code;
        public string medicaidID;
        public string firstName;
        public string lastName;
        public string dob;
        public string age;
        public string gender;
        public string address;
        public string city;
        public string state;
        public string zip;
        public string phone;
        public string height;
        public string weight;
        public string language;
        public string preference;
        public string eligibility;
    }

    public struct PhysicianLocation
    {
        public string code;
        public string name;
        public string address;
        public string city;
        public string state;
        public string zip;
        public string phone;
        public string fax;
    }


}

public class Phy_Care_Structs
{
    public static RequestAttribues RequestAtt = new RequestAttribues();
    public static SearchParameters SearchParams = new SearchParameters();
    public static Practitioner Physician = new Practitioner();
    public static Practice Location = new Practice();
    public static Patient PatientInfo = new Patient();

    public struct RequestAttribues
    {
        public string partnerId;
        public string licenseKey;
        public string applicationId;
        public string institutionId;
    }

    public struct SearchParameters
    {
        public string searchtype;
        public string searchterm;
        public string searchcontentTypes;
        public string searchformat;
        public string rxNumber;
        public string refillsTotal;
        public string quantity;
        public string daysSuplied;
        public string sig;
        public string dosageForm;
        public string dose;
        public string frequency;
        public string routeOfAdministration;
        public string renewalIndicator;
        public string prescriptionForm;
        public string startDate;
        public string DAW;
    }

    public struct Practitioner
    {
        public string practitionerid;
        public string state;
        public string zipCode;
        public string primarySpeciality;
        public string npi;
    }
    public struct Practice
    {
        public string name;
        public string phone;
        public string fax;
    }

    public struct Patient
    {
        public string patientCd;
        public string firstName;
        public string lastName;
        public string age;
        public string dob;
        public string gender;
        public string state;
    }
    
}