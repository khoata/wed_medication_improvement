﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using System.Web;
using Persits.PDF;

/// <summary>
/// Summary description for UtilityMethods
/// </summary>
public class UtilityMethods
{
    public static string BuildErrorDetails(Exception exception)
    {
        string strDetails = string.Empty;
        strDetails += System.Environment.NewLine;
        strDetails = "Exception:-";
        strDetails += System.Environment.NewLine;
        strDetails += exception.Message;
        strDetails += System.Environment.NewLine;
        strDetails += "Inner Exception:-";
        strDetails += System.Environment.NewLine;
        strDetails += exception.InnerException;
        strDetails += "Stack Trace:-";
        strDetails += System.Environment.NewLine;
        strDetails += exception.StackTrace;
        strDetails += System.Environment.NewLine;
        return strDetails;
    }

    public static bool AppLogEntry_Phy(string sLog)
    {
        DateTime dt;
        dt = DateTime.Now;
        string LogPath = ConfigurationManager.AppSettings["phy_logs"].ToString();
        try
        {
            if (!System.IO.Directory.Exists(LogPath))
            {
                System.IO.Directory.CreateDirectory(LogPath);
            }

            StreamWriter sw = new StreamWriter(LogPath + "\\" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + ".log", true);
            sw.WriteLine("  " + dt + Environment.NewLine + sLog + Environment.NewLine); sw.Flush(); sw.Close();
        }
        catch (Exception CurrentException)
        {
        }
        return true;
    }

    public static bool AppLogEntry_Pat(string sLog)
    {
        DateTime dt;
        dt = DateTime.Now;
        string LogPath = ConfigurationManager.AppSettings["pat_logs"].ToString();
        try
        {
            if (!System.IO.Directory.Exists(LogPath))
            {
                System.IO.Directory.CreateDirectory(LogPath);
            }

            StreamWriter sw = new StreamWriter(LogPath + "\\" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + ".log", true);
            sw.WriteLine("  " + dt + Environment.NewLine + sLog + Environment.NewLine); sw.Flush(); sw.Close();
        }
        catch (Exception CurrentException)
        {
        }
        return true;
    }

    public static bool SavePDRPrintConfirmationResponse(string Response)
    {
        DateTime dt;
        dt = DateTime.Now;
        string LogPath = ConfigurationManager.AppSettings["pdr_response"].ToString();
        try
        {
            if (!System.IO.Directory.Exists(LogPath))
            {
                System.IO.Directory.CreateDirectory(LogPath);
            }

            StreamWriter sw = new StreamWriter(LogPath + "\\" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + ".log", true);
            sw.WriteLine("  " + dt + Environment.NewLine + Response + Environment.NewLine); sw.Flush(); sw.Close();
        }
        catch (Exception CurrentException)
        {
        }
        return true;
    }

    public static string SaveToPdf(string url)
    {
        string Orientation = System.Configuration.ConfigurationManager.AppSettings["LandscapeOrientation"].ToString();
        PdfManager objPdf = new PdfManager();

        Persits.PDF.PdfDocument objDoc = objPdf.CreateDocument();
        objDoc.PageMode = pdfPageMode.pdfUseThumbs;
        string strtemp = objDoc.ImportFromUrl(url, "debug=true; scale=0.6; hyperlinks=false; drawbackground=true; images=true; landscape=" + Orientation + ";");
        string strFilename = objDoc.Save(HttpContext.Current.Server.MapPath("PDF\\PdfDoc.pdf"), false);

        return strFilename;
    }

    public static string MergeMultiplePDF(string PDFFilesUrl, string TransactionID)
    {
        try
        {
            PdfManager objPdf = new PdfManager();
            PdfDocument objDoc = null;

            DirectoryInfo d = new DirectoryInfo(@PDFFilesUrl);
            FileInfo[] Files = d.GetFiles("*.pdf");
            FileInfo FirstFile = d.GetFiles()[0];

            PdfDocument objDocMaster = objPdf.OpenDocument(PDFFilesUrl + FirstFile.Name);
            for (int a = 1; a < Files.Length; a++)
            {
                objDoc = objPdf.OpenDocument(PDFFilesUrl + Files[a].Name);
                objDocMaster.AppendDocument(objDoc);
            }

            if (!(File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + TransactionID + "/" + TransactionID + ".pdf"))))
                objDocMaster.Save(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + TransactionID + "/" + TransactionID + ".pdf"), false);

            return TransactionID;

        }
        catch (Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in MergeMultiplePDF:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return string.Empty;
        }
    }

    public static string  CalculateAge(DateTime birthDay)
    {
        int years = DateTime.Now.Year - birthDay.Year;

        if ((birthDay.Month > DateTime.Now.Month) || (birthDay.Month == DateTime.Now.Month && birthDay.Day > DateTime.Now.Day))
            years--;

        return years.ToString();
    }

    public static void DeleteFile(string FilePath)
    {
        try
        {
            File.Delete(FilePath); 
        }
        catch (Exception ex)
        { 
            UtilityMethods.AppLogEntry_Pat ( ex.Message.ToString ()) ; 
        }
    }

}