﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Win32.TaskScheduler;
using System.Configuration;
using System.Data.OracleClient;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.IO;

public partial class SureScriptInterface_EPCS_Report : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request["runType"]))
        { 
            string runType = Request["runType"].ToString();
            if (runType.Equals("Manual"))
            {
                if (!String.IsNullOrEmpty(Request["physicianId"]) && !String.IsNullOrEmpty(Request["toDate"]) && !String.IsNullOrEmpty(Request["fromDate"]))
                {
                    string physicianId = Request["physicianId"].ToString();
                    string fromDate = Request["fromDate"].ToString();
                    string toDate = Request["toDate"].ToString();

                    //DateTime _toDate = new DateTime(2014, 6, 12);
                    //string toDate = _toDate.Year + "/" + _toDate.Month + "/" + _toDate.Day;
                    string result = RunEPCSReport_Manual(physicianId, fromDate, toDate);

                    Response.Write(result);
                    Response.End();
                }
            }
            else if (runType.Equals("Auto"))
            {
                string result = RunEPCSReport_Auto();

                Response.Write(result);
                Response.End();
            }
            else if (runType.Equals("Schedule"))
            {
                string result = EPCSReport_Scheduler();

                Response.Write(result);
                Response.End();
            }
        }
        Response.Write("Invalid Input!");
    }

    private string RunEPCSReport_Auto()
    {
        try
        {
            DateTime currentDate = DateTime.Now;
            DateTime previousDate = currentDate.AddMonths(-1);

            DateTime fromDate = new DateTime(previousDate.Year, previousDate.Month, 1);
            //DateTime fromDate = new DateTime(2014, 5, 29);

            DateTime toDate = new DateTime(currentDate.Year, currentDate.Month, 1);
            //DateTime toDate = new DateTime(2014, 7, 29);

            //Loop through all prescribers who have EPCS role

            OracleConnection connectionAuto = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand commandAuto = new OracleCommand();
            connectionAuto.Open();
            commandAuto.Connection = connectionAuto;
            commandAuto.CommandText = "WSEPCS.GetPrescribersHaveEPCS";
            commandAuto.CommandType = CommandType.StoredProcedure;

            commandAuto.Parameters.Add(new OracleParameter("CurReport", OracleType.Cursor)).Direction = ParameterDirection.Output;

            OracleDataReader readerAuto = commandAuto.ExecuteReader();

            while (readerAuto.Read())
            {
                GenerateEPCSReport(readerAuto.GetOracleString(0).ToString(), fromDate, toDate, "Auto");
            }

            readerAuto.Close();
            connectionAuto.Close();

            return "Finish Generating Successfully!";
        }
        catch (Exception ex)
        {
            return "Error when running EPCS Report: " + ex.Message;
        }

    }

    private string RunEPCSReport_Manual(string physicianId, string fromDateString, string toDateString)
    {
        try
        {
            DateTime toDate = DateTime.Parse(toDateString);
            //DateTime fromDate = new DateTime(toDate.Year, toDate.Month, 1);
            DateTime fromDate = DateTime.Parse(fromDateString);

            string result = GenerateEPCSReport(physicianId, fromDate, toDate.AddDays(1), "Manual");

            return result;
        }
        catch (Exception ex)
        {
            return "Error when running EPCS Report: " + ex.Message;
        }
        
    }

    private string GenerateEPCSReport(string physicianId, DateTime fromDate, DateTime toDate, string actionType)
    {
        string folderLocation = HttpRuntime.AppDomainAppPath + "EPCSReportFiles\\";

        string fileName = "EPCSReport_" + physicianId + "_" + fromDate.ToString("yyyyMMdd") + "_" + toDate.AddDays(-1).ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("yyyy-MM-dd--hh-mm-ss-tt") + ".xlsx";

        FileInfo file = new FileInfo(folderLocation + fileName);

        List<EPCSReport> listReports = new List<EPCSReport>();

        OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        OracleCommand command = new OracleCommand();
        connection.Open();
        command.Connection = connection;
        command.CommandText = "WSEPCS.GetEPCSReportDetail";
        command.CommandType = CommandType.StoredProcedure;

        command.Parameters.Add(new OracleParameter("ParamDoctorId", OracleType.VarChar)).Value = physicianId;
        command.Parameters.Add(new OracleParameter("ParamFromDate", OracleType.DateTime)).Value = fromDate;
        command.Parameters.Add(new OracleParameter("ParamToDate", OracleType.DateTime)).Value = toDate;

        command.Parameters.Add(new OracleParameter("CurReport", OracleType.Cursor)).Direction = ParameterDirection.Output;

        OracleDataReader reader = command.ExecuteReader();
        while (reader.Read())
        {
            string DateOfIssuance = reader.GetOracleString(0).IsNull ? "" : reader.GetOracleString(0).ToString();
            string PatientName = reader.GetOracleString(1).IsNull ? "" : reader.GetOracleString(1).ToString();
            string DrugName = reader.GetOracleString(2).IsNull ? "" : reader.GetOracleString(2).ToString();
            string Schedule = reader.GetOracleString(3).IsNull ? "" : reader.GetOracleString(3).ToString();
            string Direction = reader.GetOracleString(4).IsNull ? "" : reader.GetOracleString(4).ToString();
            //string QuantityPrescribed = "";
            string Dispense = reader.GetOracleString(5).IsNull ? "" : reader.GetOracleString(5).ToString();
            string Refill = reader.GetOracleString(6).IsNull ? "" : reader.GetOracleString(6).ToString();
            string ReportType = reader.GetOracleString(7).IsNull ? "" : reader.GetOracleString(7).ToString();
            string PatientAddress = reader.GetOracleString(8).IsNull ? "" : reader.GetOracleString(8).ToString();
            string DoctorName = reader.GetOracleString(9).IsNull ? "" : reader.GetOracleString(9).ToString();
            string DoctorAddress = reader.GetOracleString(10).IsNull ? "" : reader.GetOracleString(10).ToString();
            string DoctorDEA = reader.GetOracleString(11).IsNull ? "" : reader.GetOracleString(11).ToString();

            listReports.Add(new EPCSReport(DateOfIssuance, PatientName, DrugName, Schedule, Direction, Dispense, Refill, ReportType,
                PatientAddress, DoctorName, DoctorAddress, DoctorDEA));
        }

        reader.Close();
        connection.Close();

        if (listReports.Count == 0)
        {
            return "No results in this period!";
        }

        CreateExcel(listReports, file);

        //Insert into EPCS_REPORT table
        OracleCommand command2 = new OracleCommand();
        connection.Open();
        command2.Connection = connection;
        command2.CommandText = "WSEPCS.SaveEPCSReport";
        command2.CommandType = CommandType.StoredProcedure;

        command2.Parameters.Add(new OracleParameter("ParamFileName", OracleType.VarChar)).Value = fileName;
        command2.Parameters.Add(new OracleParameter("ParamDoctorId", OracleType.VarChar)).Value = physicianId;
        command2.Parameters.Add(new OracleParameter("ParamActionType", OracleType.VarChar)).Value = actionType;
        command2.ExecuteOracleScalar();
        connection.Close();

        return "File <a href=\"http://192.168.73.4/SureScriptInterface/EPCSReportFiles/" + fileName + "\">" + fileName + "</a> is generated successfully!";
    }

    private void CreateExcel(List<EPCSReport> reports, FileInfo file)
    {
        // Create the package and make sure you wrap it in a using statement
        using (ExcelPackage package = new ExcelPackage(file))
        {
            // add a new worksheet to the empty workbook
            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Report");

            // --------- Data and styling goes here -------------- //

            // Start adding the header
            // First of all the first row
            worksheet.Cells[1, 1].Value = "Date Of Issuance";
            worksheet.Cells[1, 2].Value = "Patient Name";
            worksheet.Cells[1, 3].Value = "Drug Name";
            worksheet.Cells[1, 4].Value = "Schedule";
            worksheet.Cells[1, 5].Value = "Direction";
            worksheet.Cells[1, 6].Value = "Dispense";
            worksheet.Cells[1, 7].Value = "Refill";
            worksheet.Cells[1, 8].Value = "Type";
            worksheet.Cells[1, 9].Value = "Patient Address";
            worksheet.Cells[1, 10].Value = "Doctor Name";
            worksheet.Cells[1, 11].Value = "Doctor Address";
            worksheet.Cells[1, 12].Value = "Doctor DEA";

            //Ok now format the first row of the heade, but only the first two columns;
            using (ExcelRange range = worksheet.Cells[1, 1, 1, 12])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.Black);
                range.Style.Font.Color.SetColor(Color.WhiteSmoke);
                range.Style.ShrinkToFit = false;
            }

            // Keep track of the row that we're on, but start with four to skip the header
            int rowNumber = 2;

            // Loop through all the companies and add their vehicles
            foreach (EPCSReport item in reports)
            {
                worksheet.Cells[rowNumber, 1].Value = item.Date;
                worksheet.Cells[rowNumber, 2].Value = item.Patient;
                worksheet.Cells[rowNumber, 3].Value = item.Drug;
                worksheet.Cells[rowNumber, 4].Value = item.Schedule;
                worksheet.Cells[rowNumber, 5].Value = item.Direction;
                worksheet.Cells[rowNumber, 6].Value = item.Dispense;
                worksheet.Cells[rowNumber, 7].Value = item.Refill;
                worksheet.Cells[rowNumber, 8].Value = item.Type;
                worksheet.Cells[rowNumber, 9].Value = item.PatientAddress;
                worksheet.Cells[rowNumber, 10].Value = item.Doctor;
                worksheet.Cells[rowNumber, 11].Value = item.DoctorAddress;
                worksheet.Cells[rowNumber, 12].Value = item.DoctorDEA;

                // Add one row and start add the vehicles               
                rowNumber++;
            }

            // Fit the columns according to its content
            worksheet.Column(1).AutoFit();
            worksheet.Column(2).AutoFit();
            worksheet.Column(3).AutoFit();
            worksheet.Column(4).AutoFit();
            worksheet.Column(5).AutoFit();
            worksheet.Column(6).AutoFit();
            worksheet.Column(7).AutoFit();
            worksheet.Column(8).AutoFit();
            worksheet.Column(9).AutoFit();
            worksheet.Column(10).AutoFit();
            worksheet.Column(11).AutoFit();
            worksheet.Column(12).AutoFit();

            // Set some document properties
            package.Workbook.Properties.Title = "EPCS Report";
            package.Workbook.Properties.Company = "WEBeDoctor";

            // save our new workbook and we are done!
            package.Save();

            //-------- Now leaving the using statement
        } // Outside the using statement
    }

    private string EPCSReport_Scheduler()
    {
        string scriptLocation = HttpRuntime.AppDomainAppPath + "Config\\callurl_epcsreport.bat";
        string workingDirectory = HttpRuntime.AppDomainAppPath + "Config";
        //string epcsReportUrl = ConfigurationManager.AppSettings["EPCSReportUrl"];

        try
        {
            string WindowsAccUsr = ConfigurationManager.AppSettings["WindowsAccUsr"];
            string WindowsAccPwd = ConfigurationManager.AppSettings["WindowsAccPwd"];

            using (TaskService ts = new TaskService("", WindowsAccUsr, "Local", WindowsAccPwd, true))
            {
                // Create a trigger that will run at 10 a.m. on the 10th, 20th and last days of July and November
                MonthlyTrigger mTrigger = new MonthlyTrigger(1, MonthsOfTheYear.AllMonths);
                mTrigger.Enabled = true;

                DateTime nextMonth = DateTime.Now.AddMonths(1);
                mTrigger.StartBoundary = new DateTime(nextMonth.Year, nextMonth.Month, 1, 0, 0, 0);
                mTrigger.RunOnLastDayOfMonth = false;

                ExecAction ean = new ExecAction(scriptLocation, null, workingDirectory);

                Task mTask = ts.GetTask("MonthlyEPCSReport");
                TaskDefinition mTd = (mTask == null) ? ts.NewTask() : mTask.Definition;

                mTd.Triggers.Clear();
                mTd.Triggers.Add(mTrigger);
                mTd.Actions.Clear();
                mTd.Actions.Add(ean);
                ts.RootFolder.RegisterTaskDefinition("MonthlyEPCSReport", mTd, TaskCreation.CreateOrUpdate, WindowsAccUsr, WindowsAccPwd, TaskLogonType.Password, null);

                return "Create Scheduler For EPCS Report Successfully!";

            }

        }
        catch (Exception ex)
        {
            return "Error when creating scheduler for EPCS report: " + ex.Message;
        }
    }

}

public class EPCSReport
{
    private string _date;
    private string _patient;
    private string _drug;
    private string _schedule;
    private string _direction;
    private string _dispense;
    private string _refill;
    private string _type;
    private string _patientAddress;
    private string _doctor;
    private string _doctorAddress;
    private string _doctorDEA;
    public EPCSReport(string date, string patient, string drug, string schedule, string direction, string dispense,
        string refill, string type, string patientAddress, string doctor, string doctorAddress, string doctorDEA)
    {
        this._date = date;
        this._patient = patient;
        this._drug = drug;
        this._schedule = schedule;
        this._direction = direction;
        this._dispense = dispense;
        this._refill = refill;
        this._type = type;
        this._patientAddress = patientAddress;
        this._doctor = doctor;
        this._doctorAddress = doctorAddress;
        this._doctorDEA = doctorDEA;
    }
    public string Date { get { return _date; } }
    public string Patient { get { return _patient; } }
    public string Drug { get { return _drug; } }
    public string Schedule { get { return _schedule; } }
    public string Direction { get { return _direction; } }
    public string Dispense { get { return _dispense; } }
    public string Refill { get { return _refill; } }
    public string Type { get { return _type; } }
    public string PatientAddress { get { return _patientAddress; } }
    public string Doctor { get { return _doctor; } }
    public string DoctorAddress { get { return _doctorAddress; } }
    public string DoctorDEA { get { return _doctorDEA; } }
}