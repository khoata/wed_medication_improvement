﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;
using System.Collections;
using WEBeDoctorPharmacy;
using System.Collections.Generic;

public partial class Default_m : System.Web.UI.Page 
{
    //public string Visit_Key;
    //public string Patient_ID;
    //public string MsgType;
    //public string FinalMsg;
    //public string NCPDPID;
    //public string Followup_Flag;
    //public string is_sub;
    //public string Physician_ID;
    //public string selected_mid;
    private StringBuilder sb;
    //private string EnvironmentUrl = "http://192.168.73.4";
    private string EnvironmentUrl = ConfigurationManager.AppSettings["EnvironmentURL"].ToString();

    string str_visit_Key = string.Empty;
    string str_Patient_ID = string.Empty;

  protected void Page_Load(object sender, EventArgs e)
    {
        List<string> PaymentNotes = new List<string>();
        try        
        {
            string[] arrselected_mid = Request.QueryString["selected_mid"].Split(',');

            StringBuilder s = new StringBuilder();
            foreach (string key in arrselected_mid)
            {
                s.AppendLine(key + Request.Form[key] + "^");
            }
            string PostedValues = s.ToString();                        
            PaymentNotes = CallPDR(PostedValues);
            //foreach (string item in PaymentNotes)
            //{
            //    if (!(string.IsNullOrWhiteSpace(item)))
            //    {
            //        Response.Write(item.ToString());
            //    }
            //}
        }
        catch (Exception)
        {
        }

        //StringBuilder sb = new StringBuilder();
        //if (Session["arrMedications"].ToString() != "")
        //{
        //    Response.Write(Session["SavedArray"].ToString());
        //}
        //Session("arrMedications") = arrMedications

        //Response.Write(Request.QueryString["Physician_ID"].ToString() + "<BR>");
        if (!Page.IsPostBack)
        {
            sb = new StringBuilder();
            string[] arrVisitKey = Request.QueryString["Visit_Key"].Split(',');
            string[] arrPatientIDs = Request.QueryString["Patient_ID"].Split(',');
            string[] arrMsg_Type = Request.QueryString["MsgType"].Split(',');
            string[] arrFinal_Msg = Request.QueryString["FinalMsg"].Split(',');
            string[] arrselected_mid = Request.QueryString["selected_mid"].Split(',');
            string[] arrPhysician_ID = Request.QueryString["Physician_ID"].Split(',');
            string[] arris_sub = Request.QueryString["is_sub"].Split(',');
            string[] arrFollowup_Flag = Request.QueryString["Followup_Flag"].Split(',');
            string[] arrNCPDPID = Request.QueryString["NCPDPID"].Split(',');
            string[] arrDrugName = Request.QueryString["drug_name"].Split(',');
            
            //string VisitKey = Request["Visit_Key"].ToString();
            //string PatientID = Request["Patient_ID"].ToString();
            //string Msg_Type = Request["MsgType"].ToString();
            //string Final_Msg = Request["FinalMsg"].ToString();

            //for (int loop = 0; loop <= arrPatientIDs.Length - 1; loop++)
            //{
            //    Response.Write(arrVisitKey[loop] + ", " + arrPatientIDs[loop] + ", NewRX" + ", " + arrFinal_Msg[loop] + ", " + arrselected_mid[loop] + ", " + arrPhysician_ID[loop] + ", " + arris_sub[loop] + ", " + arrFollowup_Flag[loop] + ", " + arrNCPDPID[loop] + "," + arrDrugName[loop] + "<BR>");
            //}

            //Response.End();
            WEBeDoctorPharmacy.SS_Logger.log("Starting SS Integration--------");
            for (int loop = 0; loop <= arrPatientIDs.Length - 1; loop++)
            {
                string pNotes = string.Empty; 
                foreach (string item in PaymentNotes)
                {
                    if (!(string.IsNullOrWhiteSpace(item)))
                    {
                        if (item.Contains(arrselected_mid[loop].ToString()))
                        {
                            pNotes = item.ToString().Replace(arrselected_mid[loop].ToString() + ",", "").Replace("PaymentNotes:", "").Replace("DrugID:", "").Trim() ;
                            break;  
                        }
                        
                    }
                }

                //Response.Write("Before Load Main:" + pNotes);
                LoadMain(arrVisitKey[loop], arrPatientIDs[loop], "NewRX", arrFinal_Msg[loop], arrselected_mid[loop], arrPhysician_ID[loop], arris_sub[loop], arrFollowup_Flag[loop], arrNCPDPID[loop], arrDrugName[loop], pNotes);
            }
            if (arrPatientIDs.Length > 0)
            {
                btnNote.NavigateUrl = EnvironmentUrl + "/practice/notes/display/medication_history.asp?visit_key=" + arrVisitKey[0] + "&patient_id=" + arrPatientIDs[0] + "&show=curr&MsgType=NewRX";
            }
            WEBeDoctorPharmacy.SS_Logger.log("Multiple submission - completed --------");
            Response.Write(sb.ToString());
        }
    }

  protected void LoadMain(string Visit_Key, string Patient_ID, string MsgType, string FinalMsg, string selected_mid, string Physician_ID, string is_sub, string Followup_Flag, string NCPDPID, string DrugName, string sPaymentNotes)
    {
        //if (IsPostBack)
        //{ 
        try
        {

            WEBeDoctorPharmacy.SS_Logger.log("  ===dumping request parms=====");
            WEBeDoctorPharmacy.SS_Logger.log("      Visit_Key=" + Visit_Key);
            WEBeDoctorPharmacy.SS_Logger.log("      Patient_ID=" + Patient_ID);
            WEBeDoctorPharmacy.SS_Logger.log("      MsgType=" + MsgType);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + FinalMsg);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + selected_mid);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + Physician_ID);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + is_sub);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + Followup_Flag);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + NCPDPID);
            WEBeDoctorPharmacy.SS_Logger.log("  ===end dumping request parms=====");

            //string FinalMsg = "NO";
            string SSURL;

            //Response.Write(Visit_Key);
            //Response.Write(Patient_ID);
            //Response.End();

            WEBeDoctorPharmacy.SS_Logger.log(" 1. Creating XMLEnvelope");

            WEBeDoctorPharmacy.Message mMessage = CreateXMLEnvelop(Patient_ID, NCPDPID, Physician_ID, selected_mid, Visit_Key, Followup_Flag, MsgType, sPaymentNotes);
            if (mMessage.msgHeader.MessageID != null)
            {
                //authenticate request
                //if (AuthenticateRequest())
                // {

                WEBeDoctorPharmacy.SS_Logger.log(" 2. Sending Request");
                FileStream FS = new FileStream(@"d:\UserFiles\SSFiles\Request\" + mMessage.msgHeader.MessageID + ".xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                XmlDocument inputXML = new XmlDocument();
                inputXML.Load(FS);
                if (MsgType == "DirDwnld" || MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber")
                {
                    SSURL = ConfigurationManager.AppSettings["DSURL"].ToString();
                }
                else
                {
                    SSURL = ConfigurationManager.AppSettings["URL"].ToString(); ;
                }
                // Send Request.
                //CredentialCache ssUserPWD = new CredentialCache();
                //ssUserPWD.Add(new Uri("https://staging.surescripts.net/WebeDoctorTest/AuthenticatingXmlServer.aspx"), "Digest", new NetworkCredential("WebeDoctorU$er", "r0tc06363w"));


                ArrayList whiteList = new ArrayList();
                whiteList.Add("/Message/Body/RefillResponse/Response");
                whiteList.Add("/Message/Body/RefillResponse/Response/Approved");
                whiteList.Add("/Message/Body/RefillResponse/Response/Denied");
                whiteList.Add("/Message/Body/RefillResponse/Response/DenialReason");
                whiteList.Add("/Message/Body/RefillResponse/Response/ApprovedWithChanges");
                whiteList.Add("/Message/Body/RefillResponse/Response/DeniedNewPrescriptionToFollow");

                SS_Logger.log("----Pruning Response----");
                SS_Logger.log(inputXML.InnerXml);
                inputXML = new XmlPruner().pruneXml(inputXML, whiteList);
                SS_Logger.log("----Finished Pruning Response----");
                SS_Logger.log(inputXML.InnerXml);

                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(inputXML.InnerXml);
                //System.Text.Encoding.Convert(System.Text.Encoding.UTF8, System.Text.Encoding.ASCII, bytes);
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(SSURL);
                string AuthorizationCredentials = EncodeUIDPwd(MsgType);
                if (MsgType != "DirDwnld")
                {
                    req.Headers.Add("Authorization", "Basic " + AuthorizationCredentials);
                }
                //Response.Write(req.Headers);
                //Response.Write(SSURL);
                //req.Credentials = ssUserPWD;
                //bytes.ToString().Replace("?","");
                req.Timeout = 600000;
                req.Method = "Post";
                req.ContentLength = bytes.Length;
                req.ContentType = "text/xml";
                using (Stream requestStream = req.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                try
                {

                    WEBeDoctorPharmacy.SS_Logger.log(" 3. Processing Response");

                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();



                    // Save Response XML to file.
                    Guid FileNameGuid = System.Guid.NewGuid();
                    string FileName = "";

                    Stream responseStream = resp.GetResponseStream();
                    XmlDocument responseXML = new XmlDocument();

                    if (MsgType == "NO" &&
                        (MsgType == "DirDwnld" || MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber"))
                    {
                        WEBeDoctorPharmacy.SS_Logger.log("!!!!!directing response to log instead of processing it!!!!  Later code probably won't work.");
                        WEBeDoctorPharmacy.SS_Logger.log("=========");
                        WEBeDoctorPharmacy.SS_Logger.log("  Response Contents");
                        StringWriter sw = new StringWriter();
                        int b;
                        while ((b = responseStream.ReadByte()) != -1)
                        {
                            sw.Write((char)b);
                        }
                        WEBeDoctorPharmacy.SS_Logger.log(sw.ToString());
                        WEBeDoctorPharmacy.SS_Logger.log("=========");
                    }

                    WEBeDoctorPharmacy.SS_Logger.log(" 4. Unmarshalling XML from response");

                    responseXML.Load(responseStream);
                    FileName = FileNameGuid.ToString().Replace("-", "");

                    WEBeDoctorPharmacy.SS_Logger.log(" 5. Saving Response file");
                    responseXML.Save(@"d:\UserFiles\SSFiles\Response\" + FileName + ".xml");

                    WEBeDoctorPharmacy.SS_Logger.log(" 6. Reading Response file");
                    // Load Response XML into object
                    XmlSerializer Y = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetDefaultNamespace(MsgType));
                    TextReader z = new StreamReader(@"d:\UserFiles\SSFiles\Response\" + FileName + ".xml");
                    WEBeDoctorPharmacy.Message Msg = (WEBeDoctorPharmacy.Message)Y.Deserialize(z);
                    z.Close();


                    //StreamReader readStream = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    //Char[] read = new Char[512];
                    //// Reads 256 characters at a time.    
                    //int count = readStream.Read(read, 0, 512);
                    //while (count > 0)
                    //{
                    //    // Dumps the 256 characters on a string and displays the string to the console.
                    //    String str = new String(read, 0, count);
                    //    Response.Write(str);
                    //    count = readStream.Read(read, 0, 512);

                    //save the status of submission
                    if (Msg.msgBody.Status != null)
                    {
                        if (Msg.msgBody.Status.Code == "010")
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", "Submission Successful");

                            if (mMessage.msgBody.UpdatePrescriber != null)
                            {
                                Msg.UpdatePrescriberSPI(Physician_ID, mMessage.msgBody.UpdatePrescriber.mPrescriber.mPrescriberID.DSSPI, mMessage.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel);
                            }

                            sb.Append("Drug Name " + DrugName + " Submitted Successfully to Pharmacy" + "<BR />");
                            //sb.append("<BR>");
                        }
                        else if (Msg.msgBody.Status.Code == "000")
                        {
                            if (mMessage.msgBody.UpdatePrescriber != null)
                            {
                                Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", "Submission Successful");
                                Msg.UpdatePrescriberSPI(Physician_ID, mMessage.msgBody.UpdatePrescriber.mPrescriber.mPrescriberID.DSSPI, mMessage.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel);
                            }
                            else
                            {
                                Msg.UpdateStatus(Msg.msgHeader.MessageID, "Pending", "Submitted To Next Receiver.");
                            }
                            sb.Append("Drug Name " + DrugName + " Submitted Successfully to Pharmacy" + "<BR />");
                        }
                        else
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "Error", Msg.msgBody.Status.Code.ToString());
                            sb.Append("Drug Name " + DrugName + " could not submitted to Pharmacy due to error" + "<BR />");
                        }
                    }
                    else if (Msg.msgBody.Error != null)
                    {
                        Msg.UpdateStatus(Msg.msgHeader.MessageID, "Error", Msg.msgBody.Error.Description.Replace("'", ""));
                        sb.Append("Drug Name " + DrugName + " could not submitted to Pharmacy due to error" + "<BR />");
                    }

                    // DirectoryService Responses
                    // DirectoryDownload Response
                    else if (Msg.msgBody.DirDwnldResponse != null)
                    {
                        if (Msg.msgBody.DirDwnldResponse.URL != null)
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", Msg.msgBody.DirDwnldResponse.URL.ToString());
                            // download the file
                            DownloadDirectoryFile(Msg.msgBody.DirDwnldResponse.URL.ToString(), Request["style"].ToString());
                        }
                    }
                    else if (Msg.msgBody.AddPrescriberResp != null || Msg.msgBody.AddPrescriberLocationResponse != null)
                    {
                        DSPrescriber response = (Msg.msgBody.AddPrescriberResp != null)
                            ? Msg.msgBody.AddPrescriberResp
                            : (Msg.msgBody.AddPrescriberLocationResponse != null)
                                ? Msg.msgBody.AddPrescriberLocationResponse
                                : null;
                        if (response.mResponse != null && response.mResponse.ApprovedResp != null)
                        {
                            // SPI & ServiceLevel is required
                            // DEANumber never null, only prescribers who have NOT empty DEANumber can be sent to Surescripts
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", "Physician Saved - SPI: " + response.mPrescriber.mPrescriberID.DSSPI);
                            Msg.UpdatePrescriberSPI(Physician_ID, response.mPrescriber.mPrescriberID.DSSPI, response.mPrescriber.mDirectoryInfo.ServiceLevel);
                        }
                    }
                }
                catch (Exception ex)
                {
                    WEBeDoctorPharmacy.SS_Logger.log("!!!Exception in SS interface!");
                    WEBeDoctorPharmacy.SS_Logger.log(ex.Message);
                    WEBeDoctorPharmacy.SS_Logger.log(ex.StackTrace.ToString());
                    Response.Write("Error creating Web Request! -" + ex.Message);
                    sb.Append("Drug Name " + DrugName + " could not submitted to Pharmacy due to error" + "<BR />");
                    //mMessage.UpdateStatus("Error", "Submission Unsuccessful");
                }

                finally
                {
                    if (MsgType == "NewRX" && FinalMsg == "YES")
                    {

                        //Response.Redirect("http://192.168.73.4/practice/notes/display/medication_history.asp?visit_key=" + Visit_Key + "&patient_id=" + Patient_ID + "&show=curr&MsgType=" + MsgType);
                        //Session["visit_key"] = Visit_Key;
                        //Session["patient_id"] = Patient_ID;
                        //Session["show"] = "curr";
                        //Session["MsgType"] = MsgType;
                        //
                    }
                    else
                    {
                    }
                                       


                }
            }
            else
            {
                Response.Write("Error creating the XML file!");
            }
            //}
        }
        catch (Exception Ex1)
        {
            WEBeDoctorPharmacy.SS_Logger.log("!!!Exception in SS interface!!!");
            WEBeDoctorPharmacy.SS_Logger.log(Ex1.Message);
            WEBeDoctorPharmacy.SS_Logger.log(Ex1.StackTrace.ToString());
            Response.Write("Error! -" + Ex1.Message);
            sb.Append("Drug Name " + DrugName + " could not submitted to Pharmacy due to error" + "<BR />");
        }
    }

  protected WEBeDoctorPharmacy.Message CreateXMLEnvelop(string Patient_ID, string NCPDPID, string Physician_ID, string selected_mid, string Visit_Key, string Followup_Flag, string MsgType, string sPaymentNotes)
    {
        try
        {
            string[] inputArgs = new string[11] { null, null, null, null, null, null, null, null, null, null, null };
            string FollowupFlag = "N";
            string RefillResponseContent;

            //inputArgs[0] = PharmacyID.Text.ToString();
            //inputArgs[1] = DoctorID.Text.ToString();
            //inputArgs[2] = PatientID.Text.ToString();
            //inputArgs[3] = "304";
            //inputArgs[4] = VisitKey.Text.ToString();

            //inputArgs[0] = "0001074";
            //inputArgs[1] = "LASERTECH";
            //inputArgs[2] = "sithihou2001";
            //inputArgs[3] = "304";
            //inputArgs[4] = "2297SFarooquilenzgertrude";

            WEBeDoctorPharmacy.Message mMessage;

            //string MsgType = Request["MsgType"].ToString();

            switch (MsgType)
            {
                case "NewRX":
                    //inputArgs[0] = "0001074";
                    inputArgs[0] = NCPDPID;
                    inputArgs[1] = Physician_ID;
                    inputArgs[2] = Patient_ID;
                    inputArgs[3] = selected_mid;
                    inputArgs[4] = Visit_Key;
                    inputArgs[5] = (Request["refres_messageid"] == null ? null : Request["refres_messageid"].ToString());
                    inputArgs[7] = sPaymentNotes;
                    FollowupFlag = Followup_Flag;
                    break;
                case "RefillResponse":
                    //inputArgs[0] = "0001074";
                    inputArgs[0] = Request["NCPDPID"].ToString();
                    inputArgs[1] = Request["Physician_ID"].ToString();
                    inputArgs[2] = Request["Patient_ID"].ToString();
                    inputArgs[3] = Request["selected_mid"].ToString();
                    inputArgs[4] = Request["Visit_Key"].ToString();
                    inputArgs[5] = Request["Response_Type"].ToString();
                    inputArgs[6] = Request["Response_Reason_Code"].ToString();
                    inputArgs[7] = Request["Response_Note"].ToString();
                    inputArgs[8] = Request["request_messageid"].ToString();
                    inputArgs[9] = Request["Refills"];
                    //inputArgs[10] = Request["Substitutions"];
                    
                    break;
                case "DirDwnld":
                    
                    if( Request["style"] != null )
                    {
                        inputArgs[0] = Request["style"].ToString();
                    }
                    else
                    {
                        inputArgs[0] = "";
                    }
                    
                    break;

                case "AddPrescriber":
                case "UpdatePrescriber":
                    inputArgs[1] = Request["Physician_ID"].ToString();

                    int i = 0;

                    if (!(Request["serviceLevel_0"] == null || Request["serviceLevel_0"].ToString() == "N"))
                    {
                        i |= 1;
                    }

                    if (!(Request["serviceLevel_1"] == null || Request["serviceLevel_1"].ToString() == "N"))
                    {
                        i |= 2;
                    }
                    if (!(Request["serviceLevel_2"] == null || Request["serviceLevel_2"].ToString() == "N"))
                    {
                        i |= 4;
                    }
                    if (!(Request["serviceLevel_3"] == null || Request["serviceLevel_3"].ToString() == "N"))
                    {
                        i |= 8;
                    }
                    if (!(Request["serviceLevel_4"] == null || Request["serviceLevel_4"].ToString() == "N"))
                    {
                        i |= 16;
                    }

                    inputArgs[2] = i.ToString();

                    break;
            }


            


            //Response.Write(MsgType+"|"+inputArgs[2].ToString() + "|" + inputArgs[3].ToString() + "|" + inputArgs[4].ToString() + "|" + inputArgs[5].ToString() + "|" + inputArgs[6].ToString() + "|" + inputArgs[7].ToString());

            //12/20/2008 Removed these line since I'm adding the NS info in the .NET type Message itself.
            //mMessage.xmlns = new XmlSerializerNamespaces();
            //mMessage.xmlns.Add("", "http://www.surescripts.com/messaging");

            //8/22/2009 kxu: Do clone/merge logic if it's a RefillResponse and RefillRequest msg file exists.
            if (MsgType == "RefillResponse" && File.Exists(@"d:\UserFiles\SSFiles\Response\" + inputArgs[8] + ".xml"))
            {
                RefillResponseContent = GetTextFromXMLFile(@"d:\UserFiles\SSFiles\Response\" + inputArgs[8] + ".xml").Replace("RefillRequest", "RefillResponse");

                WEBeDoctorPharmacy.SS_Logger.log(RefillResponseContent);

                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(RefillResponseContent);
                MemoryStream ms = new MemoryStream(bytes);
   

                // Deserializes RefillResponse XML Copied from RefillRequest
                XmlSerializer x = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetDefaultNamespace(MsgType));
                //TextWriter tw = new StreamWriter(@"d:\UserFiles\SSFiles\Request\" + 456 + ".xml");
                mMessage = (WEBeDoctorPharmacy.Message)x.Deserialize(ms);
                //x.Serialize(tw, mMessage);

                //figure out if we need approved or approvedwithChanges
                //if (inputArgs[5] != null && inputArgs[5] == "Approved")
                //{
                //    if (mMessage.msgBody.RefResp.mMedication.Refills != null
                //       && mMessage.msgBody.RefResp.mMedication.Refills.Quantity != null
                //       && mMessage.msgBody.RefResp.mMedication.Refills.Quantity != "0")
                //    {
                //        //they sent us a specific amount of refills and we are overriding it
                //        int oldRefills = Int32.Parse(mMessage.msgBody.RefResp.mMedication.Refills.Quantity);
                //        int newRefills = Int32.Parse(inputArgs[9]);

                //        if (oldRefills != newRefills - 1)
                //        {
                //            inputArgs[5] = "ApprovedWithChanges";
                //        }
                //    }
                //}
                


                // Todo List:
                // 1.  Re-generate MessageID
                // 2.  Re-generate SentTime
                mMessage.SetMessageHeaderFromClone();
                // 3.  Construct Response Element
                // 4.  Update Refill Quantity, Subsitution and WrittenDate according to inputArgs values.
                mMessage.SetRefillResponseFromClone(inputArgs);
            }
            else
            {
                mMessage = new WEBeDoctorPharmacy.Message(MsgType, inputArgs);
            }
            mMessage.ConstructHeaderFields();
            if (FollowupFlag == "Y")
            {
                MsgType = "NewRxFollowup";
            }
            mMessage.LoadCorrelatedMessageInfo(MsgType, inputArgs);

            string relatedId = mMessage.msgHeader.RelatesToMessageID;

            if (FollowupFlag == "Y")
            {
                mMessage.msgHeader.RelatesToMessageID = null;
            }

            XmlSerializer Y = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetDefaultNamespace(MsgType));
            //XmlSerializerNamespaces NS = new XmlSerializerNamespaces();
            //NS.Add(string.Empty, "http://www.surescripts.com/messaging");
            TextWriter z = new StreamWriter(@"d:\UserFiles\SSFiles\Request\" + mMessage.msgHeader.MessageID + ".xml");
            Y.Serialize(z, mMessage);
            z.Close();

            if (FollowupFlag == "Y")
            {
                mMessage.msgHeader.RelatesToMessageID = relatedId;
            }


            //save message
            string DrugID = mMessage.Save(mMessage.msgHeader.MessageID, null);



            return mMessage;
        }
        catch(Exception ex)
        {
            Response.Write("Error: " + ex.StackTrace);
            WEBeDoctorPharmacy.SS_Logger.log("Error: " + ex.StackTrace);

            if (ex.InnerException != null)
            {
                WEBeDoctorPharmacy.SS_Logger.log("Inner Error: " + ex.InnerException.StackTrace);

                if (ex.InnerException.InnerException != null)
                {
                    WEBeDoctorPharmacy.SS_Logger.log("Inner inner Error: " + ex.InnerException.InnerException.StackTrace);
                }

            
            }

            return null;
        }
    }

  private string GetTextFromXMLFile(string file)
  {
   StreamReader reader = new StreamReader(file);
   string ret = reader.ReadToEnd();
   reader.Close();
   return ret;
  }
  private bool AuthenticateRequest()
  {
        //Create new user/credential cache for authentication.
      String TestPostData = "Test";
      byte[] bytes = System.Text.Encoding.UTF8.GetBytes(TestPostData);
      CredentialCache ssUserPWD = new CredentialCache();
      ssUserPWD.Add(new Uri("https://staging.surescripts.net/WebeDoctorTest/AuthenticatingXmlServer.aspx"), "Basic", new NetworkCredential("webemd2$ure$cript$", "e$cripswebe23*$%3"));
      string SSURL = "https://staging.surescripts.net/WebeDoctorTest/AuthenticatingXmlServer.aspx";
      HttpWebRequest req = (HttpWebRequest)WebRequest.Create(SSURL);
      req.Credentials = ssUserPWD;
      req.Timeout = 3000;
      req.Method = "Post";
      req.ContentType = "text/xml";
      req.ContentLength = bytes.Length;
      using (Stream requestStream = req.GetRequestStream())
      {
          requestStream.Write(bytes, 0, bytes.Length);
      }
      //Response.Write("Authenticating....");

      HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

      //Response.Write("StatusCode: " + resp.StatusCode);

      if (resp.StatusCode == HttpStatusCode.OK)
      {
          return true;
      }
      return false;
   }
  private string EncodeUIDPwd(string MsgType)
    {
        string UID;
        string PWD ;

        if (MsgType == "DirDwnld")
        {
            UID = "webedoc";
            PWD = CalculateSHA1("ebe0700!");
            WEBeDoctorPharmacy.SS_Logger.log(PWD);
        }
        else{
            UID = "WebeDoctorU$er";
            PWD = "K!TF#:I1T:4xm";
        }
        string EncodedString = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(UID+":"+PWD));

        return EncodedString;
    }
  public static string CalculateSHA1(string text)
    {
        return BitConverter.ToString(SHA1Managed.Create().ComputeHash(Encoding.Default.GetBytes(text))).Replace("-", "");
    }

    private void DownloadDirectoryFile(string FileName,string FileType)
    {
        WebClient client = new WebClient();

        WEBeDoctorPharmacy.SS_Logger.log(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName);

        if (FileType == "full")
        {
            client.DownloadFile(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName,
                                @"d:\UserFiles\SSFiles\DirectoryDownloadFiles\Full\" + FileName);
        }
        else
        {
            client.DownloadFile(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName,
                                @"d:\UserFiles\SSFiles\DirectoryDownloadFiles\Nightly\" + FileName);
        }

    }


    protected void btnNote_Click(object sender, EventArgs e)
    {
        //Response.Redirect(EnvironmentUrl +"/practice/notes/display/medication_history.asp?visit_key=" + Session["visit_key"].ToString() + "&patient_id=" + Session["patient_id"].ToString() + "&show=" + Session["show"].ToString() + "&MsgType=" + Session["MsgType"].ToString());
    }

    #region PDR Methods

    public List<string> CallPDR(string PostedValues)
    {
        string str_Med_Id = string.Empty;
        string str_Docuser = string.Empty;
        List<string> PaymentNotes  = new List<string> () ;
        
        try
        {
            if (NotEmpty(PostedValues))
            {
                string[] sArr = PostedValues.Split('^');

                foreach (string s in sArr)
                {
                    str_Med_Id = s.Trim();
                    //UtilityMethods.AppLogEntry_Pat("Medication IDs: " + str_Med_Id);                    
                    DataTable PatCommAcess = new DataTable();
                    PatCommAcess = PDRDAL.Check_PDR_Access(str_Med_Id);

                    try
                    {
                        if ((PatCommAcess != null) && (PatCommAcess.Rows.Count > 0))
                        {
                            if (PatCommAcess.Rows[0]["patientcomm_access"].ToString() == "1")
                            {
                                string note = ProcessPDR(PatCommAcess);
                                PaymentNotes.Add(note); 
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        UtilityMethods.AppLogEntry_Pat(ex.ToString());
                    }

                }


            }
            return PaymentNotes;           
            

        }
        catch (Exception ex)
        { UtilityMethods.AppLogEntry_Pat(ex.ToString());
        return null;
        }
    }

    public string GetPaymentNotes(string xmldoc)
    {
        try
        {
            string PNotes = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmldoc);
            XmlNodeList elemList = doc.GetElementsByTagName("response");
            PNotes = elemList.ToString();

            XmlNodeList nl = doc.SelectNodes("response");
            XmlNode root = nl[0];
            XmlNode programs = root.ChildNodes[0];

            foreach (XmlNode xnode in programs.ChildNodes)
            {
                foreach (XmlNode node in xnode)
                {
                    string name = node.Name;
                    if (name == "paymentNotes")
                    {
                        PNotes = node.InnerText;
                        return PNotes;
                    }
                }
            }
            return string.Empty;
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }


    public string ProcessPDR(DataTable dtMedications)
    {
        DataTable dtPatInfo = new DataTable();
        string str_DrugName = string.Empty; 
        try
        {
            str_DrugName = dtMedications.Rows[0]["drug_name"].ToString();
            str_visit_Key = dtMedications.Rows[0]["visit_key"].ToString();

            //Request Attributes
            Patient_Comm_Structs.RequestAtt.source = ConfigurationManager.AppSettings["source"].ToString();
            Patient_Comm_Structs.RequestAtt.portalID = ConfigurationManager.AppSettings["portalID"].ToString();
            Patient_Comm_Structs.RequestAtt.pFormat = ConfigurationManager.AppSettings["pFormat"].ToString();
            Patient_Comm_Structs.RequestAtt.wFormat = ConfigurationManager.AppSettings["wFormat"].ToString();
            Patient_Comm_Structs.RequestAtt.output = ConfigurationManager.AppSettings["output"].ToString();

            //Physician Info
            Patient_Comm_Structs.PhysicianLoc.code = dtMedications.Rows[0]["org_code"].ToString();
            Patient_Comm_Structs.PhysicianLoc.name = dtMedications.Rows[0]["name"].ToString();
            Patient_Comm_Structs.PhysicianLoc.address = dtMedications.Rows[0]["org_address"].ToString();
            Patient_Comm_Structs.PhysicianLoc.city = dtMedications.Rows[0]["org_city"].ToString();
            Patient_Comm_Structs.PhysicianLoc.state = dtMedications.Rows[0]["org_state"].ToString();
            Patient_Comm_Structs.PhysicianLoc.zip = dtMedications.Rows[0]["org_zip"].ToString();
            Patient_Comm_Structs.PhysicianLoc.phone = dtMedications.Rows[0]["org_phone"].ToString();
            Patient_Comm_Structs.PhysicianLoc.fax = dtMedications.Rows[0]["org_fax"].ToString();

            //Patient Info
            Patient_Comm_Structs.PatientInfo.code = dtMedications.Rows[0]["pat_code"].ToString();
            Patient_Comm_Structs.PatientInfo.firstName = dtMedications.Rows[0]["patient_firstName"].ToString();
            Patient_Comm_Structs.PatientInfo.lastName = dtMedications.Rows[0]["patient_lastName"].ToString();
            Patient_Comm_Structs.PatientInfo.dob = dtMedications.Rows[0]["patient_dob"].ToString();
            if (dtMedications.Rows[0]["patient_gender"].ToString().ToLower() == "male")
                Patient_Comm_Structs.PatientInfo.gender = "M";
            else if (dtMedications.Rows[0]["patient_gender"].ToString().ToLower() == "female")
                Patient_Comm_Structs.PatientInfo.gender = "F";
            Patient_Comm_Structs.PatientInfo.address = dtMedications.Rows[0]["patient_address"].ToString();
            Patient_Comm_Structs.PatientInfo.city = dtMedications.Rows[0]["patient_city"].ToString();
            Patient_Comm_Structs.PatientInfo.state = dtMedications.Rows[0]["patient_state"].ToString();
            Patient_Comm_Structs.PatientInfo.zip = dtMedications.Rows[0]["patient_zip"].ToString();
            Patient_Comm_Structs.PatientInfo.phone = dtMedications.Rows[0]["patient_phone"].ToString();

            //Request Params
            Patient_Comm_Structs.RequestParams.rxNumber = dtMedications.Rows[0]["drug_id"].ToString();
            //Patient_Comm_Structs.RequestParams.productRxNorm = "15648";
            Patient_Comm_Structs.RequestParams.productNDC = dtMedications.Rows[0]["ndc_code"].ToString();

            //Immunizations                    
            DataTable dtImm = new DataTable();
            List<string> lstImmunizations = new List<string>();
            if (NotEmpty(str_visit_Key))
            {
                dtImm = PDRDAL.Get_Immunization_Info(str_visit_Key);
                if ((dtImm != null) && (dtImm.Rows.Count > 0))
                {
                    if (NotEmpty(dtImm.Rows[0]["cvx_code"].ToString()))
                        lstImmunizations.Add(dtImm.Rows[0]["cvx_code"].ToString());
                }
            }

            //Diagnosis
            List<List<string>> lstdiagnosis = new List<List<string>>();
            DataTable dtDiag = new DataTable();
            if (NotEmpty(str_visit_Key))
            {
                dtDiag = PDRDAL.Get_Diag_Info(str_visit_Key);
                if ((dtDiag != null) && (dtDiag.Rows.Count > 0))
                {
                    if (NotEmpty(dtDiag.Rows[0]["DIAG_CODE"].ToString()))
                    {
                        List<string> lstdiagnosis_detail = new List<string> { "codeSystem:ICD10CM", "value:" + dtDiag.Rows[0]["DIAG_CODE"].ToString(), "description:Diagnosis 1" };
                        lstdiagnosis.Add(lstdiagnosis_detail);
                    }
                }
            }

            //allergies                    
            DataTable dtAllergies = new DataTable();
            List<string> lstAllergies = new List<string>();
            if (NotEmpty(str_visit_Key))
            {
                dtAllergies = PDRDAL.Get_Allergy_Info(str_visit_Key);
                if ((dtAllergies != null) && (dtAllergies.Rows.Count > 0))
                {
                    if (NotEmpty(dtAllergies.Rows[0]["allergy_text"].ToString()))
                        lstAllergies.Add(dtAllergies.Rows[0]["allergy_text"].ToString());
                }
            }

            //Labs                    
            DataTable dtLabs = new DataTable();
            List<string> lstLabs = new List<string>();
            if (NotEmpty(str_visit_Key))
            {
                dtLabs = PDRDAL.Get_Lab_Info(str_visit_Key);
                if ((dtLabs != null) && (dtAllergies.Rows.Count > 0))
                {
                    if (NotEmpty(dtLabs.Rows[0]["lab_code"].ToString()))
                        lstLabs.Add(dtLabs.Rows[0]["lab_code"].ToString());
                }
            }



            //string reqXML = CreateRequestXML(Patient_Comm_Structs.RequestAtt, Patient_Comm_Structs.RequestParams, Patient_Comm_Structs.PhysicianLoc, Patient_Comm_Structs.PatientInfo, lstImmunizations, lstdiagnosis, lstLabs, lstAllergies);
            string reqXML = CreateRequestXML(Patient_Comm_Structs.RequestAtt, Patient_Comm_Structs.RequestParams, Patient_Comm_Structs.PhysicianLoc, Patient_Comm_Structs.PatientInfo, lstImmunizations, lstdiagnosis, lstLabs, lstAllergies);

            if (ConfigurationManager.AppSettings["save_request_xml"].ToString() == "true")
                File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_request_xml/" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "__" + DateTime.Now.Ticks + ".xml"), reqXML);

            string strPostData = reqXML;
            string result = PostData(strPostData);
                        
            string paynote = GetPaymentNotes(result);
            string pnote  = string.Empty ; 

            if (!(string.IsNullOrWhiteSpace(paynote)))
            {
                pnote = "DrugID:" + dtMedications.Rows[0]["drug_id"].ToString() + ", PaymentNotes:" + paynote; 
            }
                        
            
            if (ConfigurationManager.AppSettings["save_response_xml"].ToString() == "true")
                File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_response_xml/" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "__" + DateTime.Now.Ticks + ".xml"), result);

            //Environment.Exit(0);

            //string reqXML = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_request_xml/002.xml"));

            //string resXML = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_response_xml/ResXMLwithPDF.xml"));

            string TransactionID = GeneratePDF(result);

            if (NotEmpty(TransactionID))
            {
                PanelLinks.Controls.Add(new LiteralControl("<br />"));                
                HyperLink hypLink = new HyperLink();
                string LinkPath = System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + TransactionID + "/" + TransactionID + ".pdf");
                hypLink.Text = "View and Print Physicians Desk Reference (PDR) PDF for " + str_DrugName; 
                //hypLink.NavigateUrl = "~/transactionid/" + TransactionID + "/" + TransactionID + ".pdf";

                hypLink.NavigateUrl = "javascript:showPDF(" + TransactionID + ")";               


                //hypLink.Attributes.Add("onClick", "<script>window.open('WebForm1.aspx');</script>"); 
                PanelLinks.Controls.Add(hypLink);
                PanelLinks.Controls.Add(new LiteralControl("<br />"));
            }
            else
            {
                //Label lbl = new Label();
                //lbl.Text = "There is no content from PDR.";
                //PanelLinks.Controls.Add(lbl);
                //PanelLinks.Controls.Add(new LiteralControl("<br />"));
            }
            
            return pnote; 
        }
        catch (Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in ProcessPDR:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return null; 
        }

    }

    public string GetTransctionID(string xmldoc)
    {
        try
        {
            string transactionID = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmldoc);
            XmlNodeList elemList = doc.GetElementsByTagName("response");
            for (int i = 0; i < elemList.Count; i++)
            {
                transactionID = elemList[i].Attributes["transactionID"].Value;
                if (transactionID != string.Empty)
                    break;
            }

            return transactionID;
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    public string GeneratePDF(string xmldoc)
    {
        try
        {
            string transactionID = string.Empty;
            string programID = string.Empty;
            string FinalPDFPath = string.Empty;
            List<string> lstProgramIDs = new List<string>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmldoc);
            XmlNodeList elemList = doc.GetElementsByTagName("response");
            transactionID = elemList[0].Attributes["transactionID"].Value;

            //File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_request_xml/" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "__" + DateTime.Now.Ticks + ".xml"), reqXML);

            XmlNodeList nl = doc.SelectNodes("response");
            XmlNode root = nl[0];
            XmlNode programs = root.ChildNodes[0];

            foreach (XmlNode xnode in programs.ChildNodes)
            {
                programID = xnode.Attributes["id"].Value;
                lstProgramIDs.Add(programID);

                foreach (XmlNode node in xnode)
                {
                    string name = node.Name;
                    if (name == "image")
                    {
                        string value = node.InnerText;

                        if (!(Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID))))
                            Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID));

                        File.WriteAllBytes(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID + "/" + programID + ".pdf"), Convert.FromBase64String(value));
                    }
                }
            }

            if (NotEmpty(programID))
            {
                FinalPDFPath = UtilityMethods.MergeMultiplePDF(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID + "/"), transactionID);


                //foreach (string element in lstProgramIDs)
                //{
                //    UtilityMethods.DeleteFile(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID + "/" + element + ".pdf"));
                //}

                CreateConfirmationXML(transactionID, lstProgramIDs);
            }


            return FinalPDFPath;
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    public void CreateConfirmationXML(string TransctionId, List<string> ProgramIds)
    {
        string ConfirmXML = string.Empty;
        try
        {
            //XML root and declaration
            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(dec);

            //confirmation
            XmlElement confirmation = doc.CreateElement("confirmation");
            confirmation.SetAttribute("transactionID", TransctionId);
            doc.AppendChild(confirmation);

            //programs                        
            XmlElement programs = doc.CreateElement("programs");
            foreach (string element in ProgramIds)
            {
                XmlElement program = doc.CreateElement("program");
                program.SetAttribute("id", element);
                programs.AppendChild(program);
            }
            confirmation.AppendChild(programs);

            ConfirmXML = doc.OuterXml;

            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + TransctionId + "/" + TransctionId + ".xml"), ConfirmXML);
        }
        catch (System.Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in CreateConfirmationXML:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
        }
    }


    [System.Web.Services.WebMethod]
    public static void SendConfirmation(string value)
    {
        try
        {
            string ConfimationXML = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + value + "/" + value + ".xml"));
            string result = string.Empty;
            try
            {
                string sURL = ConfigurationManager.AppSettings["patient_comm_Web_service"].ToString();

                // prepare request
                HttpWebRequest wreq = (HttpWebRequest)WebRequest.Create(sURL);
                wreq.Method = "POST";
                wreq.ContentType = "application/x-www-form-urlencoded";
                wreq.ContentLength = ConfimationXML.Length;

                // post to server
                Stream sw = wreq.GetRequestStream();
                StreamWriter wr = new StreamWriter(sw);
                wr.Write(ConfimationXML);
                wr.Close();

                // get response
                HttpWebResponse httpResponse = (HttpWebResponse)wreq.GetResponse();
                Stream sr = httpResponse.GetResponseStream();
                StreamReader rd = new StreamReader(sr);

                result = rd.ReadToEnd();

                UtilityMethods.SavePDRPrintConfirmationResponse("TransactionID :" + value + " ---> " +  result);

            }
            catch (System.Exception ex)
            {
                UtilityMethods.AppLogEntry_Pat("Error:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    public string PostData(string sData)
    {
        string result = string.Empty;
        try
        {
            string sURL = ConfigurationManager.AppSettings["patient_comm_Web_service"].ToString();

            // prepare request
            HttpWebRequest wreq = (HttpWebRequest)WebRequest.Create(sURL);
            wreq.Method = "POST";
            wreq.ContentType = "application/x-www-form-urlencoded";
            wreq.ContentLength = sData.Length;

            // post to server
            Stream sw = wreq.GetRequestStream();
            StreamWriter wr = new StreamWriter(sw);
            wr.Write(sData);
            wr.Close();

            // get response
            HttpWebResponse httpResponse = (HttpWebResponse)wreq.GetResponse();
            Stream sr = httpResponse.GetResponseStream();
            StreamReader rd = new StreamReader(sr);

            result = rd.ReadToEnd();

        }
        catch (System.Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return ex.Message.ToString();
        }
        return result;
    }

    public string CreateRequestXML(Patient_Comm_Structs.RequestAttribues ReqAttributes, Patient_Comm_Structs.RequestParameters ReqParams, Patient_Comm_Structs.PhysicianLocation PhysicianLoc, Patient_Comm_Structs.PatientInformation PatientInfo, List<string> lstImmunizations, List<List<string>> lstdiagnosis, List<string> lstLabs, List<string> lstAllergies)
    {
        string xmlOutput = string.Empty;
        try
        {
            //XML root and declaration
            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(dec);

            //Request Attributes
            XmlElement root = doc.CreateElement("request");
            if (!(string.IsNullOrEmpty(ReqAttributes.source))) { root.SetAttribute("source", ReqAttributes.source); }
            if (!(string.IsNullOrEmpty(ReqAttributes.portalID))) { root.SetAttribute("portalID", ReqAttributes.portalID); }
            if (!(string.IsNullOrEmpty(ReqAttributes.pFormat))) { root.SetAttribute("pFormat", ReqAttributes.pFormat); }
            if (!(string.IsNullOrEmpty(ReqAttributes.wFormat))) { root.SetAttribute("wFormat", ReqAttributes.wFormat); }
            if (!(string.IsNullOrEmpty(ReqAttributes.output))) { root.SetAttribute("output", ReqAttributes.output); }
            doc.AppendChild(root);

            //Request Parameters
            XmlElement rxNumber = doc.CreateElement("rxNumber");
            rxNumber.InnerText = ReqParams.rxNumber;
            root.AppendChild(rxNumber);

            XmlElement productRxNorm = doc.CreateElement("productRxNorm");
            productRxNorm.InnerText = ReqParams.productRxNorm;
            //root.AppendChild(productRxNorm);

            XmlElement productNDC = doc.CreateElement("productNDC");
            productNDC.InnerText = ReqParams.productNDC;
            root.AppendChild(productNDC);

            //PatientInfo
            XmlElement patient = doc.CreateElement("patient");
            //Paitent Code
            XmlElement patient_code = doc.CreateElement("code");
            patient_code.InnerText = PatientInfo.code;
            patient.AppendChild(patient_code);
            //patient_firstName
            XmlElement patient_firstName = doc.CreateElement("firstName");
            patient_firstName.InnerText = PatientInfo.firstName;
            patient.AppendChild(patient_firstName);
            //patient_lastName
            XmlElement patient_lastName = doc.CreateElement("lastName");
            patient_lastName.InnerText = PatientInfo.lastName;
            patient.AppendChild(patient_lastName);
            //patient_dob
            XmlElement patient_age = doc.CreateElement("dob");
            patient_age.InnerText = PatientInfo.dob;
            patient.AppendChild(patient_age);
            //patient_gender
            XmlElement patient_gender = doc.CreateElement("gender");
            patient_gender.InnerText = PatientInfo.gender;
            patient.AppendChild(patient_gender);
            //Paitent address
            XmlElement patient_address = doc.CreateElement("address");
            patient_address.InnerText = PatientInfo.address;
            patient.AppendChild(patient_address);
            //Paitent city
            XmlElement patient_city = doc.CreateElement("city");
            patient_city.InnerText = PatientInfo.city;
            patient.AppendChild(patient_city);
            //Paitent state
            XmlElement patient_state = doc.CreateElement("state");
            patient_state.InnerText = PatientInfo.state;
            patient.AppendChild(patient_state);
            //Paitent zip
            XmlElement patient_zip = doc.CreateElement("zip");
            patient_zip.InnerText = PatientInfo.zip;
            patient.AppendChild(patient_zip);
            //Paitent phone
            XmlElement patient_phone = doc.CreateElement("phone");
            patient_phone.InnerText = PatientInfo.phone;
            patient.AppendChild(patient_phone);

            root.AppendChild(patient);


            #region "diagnosis"
            //diagnosis
            XmlElement diagnosis = doc.CreateElement("diagnosis");
            foreach (List<string> element in lstdiagnosis)
            {
                XmlElement diagnosischild = doc.CreateElement("diagnosis");
                foreach (string arr in element)
                {
                    if (arr.ToString().Contains("codeSystem"))
                    {
                        diagnosischild.SetAttribute("codeSystem", arr.Replace("codeSystem:", ""));
                        diagnosis.AppendChild(diagnosischild);
                    }
                    else if (arr.ToString().Contains("value"))
                    {
                        diagnosischild.SetAttribute("value", arr.Replace("value:", ""));
                        diagnosis.AppendChild(diagnosischild);
                    }
                    else if (arr.ToString().Contains("description"))
                    {
                        diagnosischild.SetAttribute("description", arr.Replace("description:", ""));
                        diagnosis.AppendChild(diagnosischild);
                    }
                }

            }
            patient.AppendChild(diagnosis);
            #endregion

            #region "Immunization"
            //Labs
            XmlElement immunizations = doc.CreateElement("immunizations");
            foreach (string element in lstImmunizations)
            {
                XmlElement immunization = doc.CreateElement("immunization");
                immunization.SetAttribute("value", element);
                immunizations.AppendChild(immunization);
            }
            patient.AppendChild(immunizations);
            #endregion

            #region "labs"
            //Labs
            XmlElement labs = doc.CreateElement("labs");
            foreach (string element in lstLabs)
            {
                XmlElement lab = doc.CreateElement("lab");
                lab.SetAttribute("value", element);
                labs.AppendChild(lab);
            }
            patient.AppendChild(labs);
            #endregion

            #region "allergies"
            //Allergies
            XmlElement allergies = doc.CreateElement("allergies");
            foreach (string element in lstAllergies)
            {
                XmlElement allergy = doc.CreateElement("allergy");
                allergy.SetAttribute("value", element);
                allergies.AppendChild(allergy);
            }
            patient.AppendChild(allergies);
            #endregion

            //financial
            XmlElement financial = doc.CreateElement("financial");

            //medicalBenefit
            XmlElement medicalBenefit = doc.CreateElement("medicalBenefit");
            financial.AppendChild(medicalBenefit);

            XmlElement medicalBenefit_primary = doc.CreateElement("primary");
            medicalBenefit.AppendChild(medicalBenefit_primary);

            XmlElement medicalBenefit_secondary = doc.CreateElement("secondary");
            medicalBenefit.AppendChild(medicalBenefit_secondary);

            //prescriptionBenefit
            XmlElement prescriptionBenefit = doc.CreateElement("prescriptionBenefit");
            financial.AppendChild(prescriptionBenefit);

            XmlElement prescriptionBenefit_primary = doc.CreateElement("primary");
            prescriptionBenefit.AppendChild(prescriptionBenefit_primary);

            XmlElement prescriptionBenefit_secondary = doc.CreateElement("secondary");
            prescriptionBenefit.AppendChild(prescriptionBenefit_secondary);

            //root.AppendChild(financial);


            //pharmacy
            XmlElement pharmacy = doc.CreateElement("pharmacy");
            //root.AppendChild(pharmacy);



            //prescriber
            XmlElement prescriber = doc.CreateElement("prescriber");
            // root.AppendChild(prescriber);

            //location
            XmlElement location = doc.CreateElement("location");

            XmlElement location_code = doc.CreateElement("code");
            if (ConfigurationManager.AppSettings["mode"].ToString() == "test")
                location_code.InnerText = "999999999";
            else
                location_code.InnerText = PhysicianLoc.code;

            location.AppendChild(location_code);

            XmlElement location_name = doc.CreateElement("name");
            location_name.InnerText = PhysicianLoc.name;
            location.AppendChild(location_name);

            XmlElement location_address = doc.CreateElement("address");
            location_address.InnerText = PhysicianLoc.address;
            location.AppendChild(location_address);

            XmlElement location_city = doc.CreateElement("city");
            location_city.InnerText = PhysicianLoc.city;
            location.AppendChild(location_city);

            XmlElement location_state = doc.CreateElement("state");
            location_state.InnerText = PhysicianLoc.state;
            location.AppendChild(location_state);

            XmlElement location_zip = doc.CreateElement("zip");
            location_zip.InnerText = PhysicianLoc.zip;
            location.AppendChild(location_zip);

            XmlElement location_phone = doc.CreateElement("phone");
            location_phone.InnerText = PhysicianLoc.phone;
            location.AppendChild(location_phone);

            XmlElement location_fax = doc.CreateElement("fax");
            location_fax.InnerText = PhysicianLoc.fax;
            location.AppendChild(location_fax);

            root.AppendChild(location);

            xmlOutput = doc.OuterXml;
        }
        catch (System.Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in CreateRequestXML:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return ex.Message.ToString();
        }
        return xmlOutput;
    }

    public string CreateDemoXML()
    {
        string xmlOutput = string.Empty;
        try
        {
            // Create the xml document containe
            XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);// Create the root element
            XmlElement root = doc.CreateElement("Library");
            doc.AppendChild(root);
            // Create Books
            // Note that to set the text inside the element,
            // you use .InnerText instead of .Value (which will throw an exception).
            // You use SetAttribute to set attribute
            XmlElement book = doc.CreateElement("Book");
            book.SetAttribute("BookType", "Hardcover");
            XmlElement title = doc.CreateElement("Title");
            title.InnerText = "Door Number Three";
            XmlElement author = doc.CreateElement("Author");
            author.InnerText = "O'Leary, Patrick";
            book.AppendChild(title);
            book.AppendChild(author);
            root.AppendChild(book);
            book = doc.CreateElement("Book");
            book.SetAttribute("BookType", "Paperback");
            title = doc.CreateElement("Title");
            title.InnerText = "Lord of Light";
            author = doc.CreateElement("Author");
            author.InnerText = "Zelanzy, Roger";
            book.AppendChild(title);
            book.AppendChild(author);
            root.AppendChild(book);
            xmlOutput = doc.OuterXml;
        }
        catch (System.Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in CreateRequestXML:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return ex.Message.ToString();
        }
        return xmlOutput;
    }

    public static bool NotEmpty(string Element)
    {
        if (!(string.IsNullOrEmpty(Element)))
            return true;
        else
            return false;

    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        string AddMedicationURL = "https://www.webedoctor2.com/practice/notes/display/medication_history.asp";

        //string strvalues = this.HiddenField1.Value.Replace("\n", "");
        //string[] sArr = strvalues.Split('&');

        //foreach (string s in sArr)
        //{
        //    if (s.Contains("visit_key"))
        //        Visit_Key = s;

        //    if (s.Contains("patient_id"))
        //        PatientId = s;
        //}

        Response.Redirect(AddMedicationURL + "?" + str_visit_Key + "&" + str_Patient_ID);
    }
}
