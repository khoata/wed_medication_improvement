﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Microsoft.Win32.TaskScheduler;
using System.Data.OracleClient;
using System.Configuration;
using System.Data;
using System.Security.Principal;

public partial class SureScriptInterface_PharmacyDownload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String username = Request["_u"] == null ? "" : Request["_u"];
        String password = Request["_p"] == null ? "" : Request["_p"];
        String weeklyStartTime = "";
        String nightlyStartTime = "";

        using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            connection.Open();

            OracleCommand command = new OracleCommand();            
            command.Connection = connection;
            command.CommandText = "Select userid, pwd from user_pwd where userid = :userid and pwd = :pwd";
            command.CommandType = CommandType.Text;
            command.Parameters.Add(new OracleParameter("userid", OracleType.VarChar)).Value = username;
            command.Parameters.Add(new OracleParameter("pwd", OracleType.VarChar)).Value = password;
            OracleDataReader reader = command.ExecuteReader();
            command.Dispose();
            if (!reader.Read())
            {
                Response.Redirect("/admin/login.asp");
            }
            else
            {
                command = new OracleCommand();
                command.Connection = connection;
                command.CommandText = "Select value from ad_config where key = 'WEEKLYSTARTTIME'";
                command.CommandType = CommandType.Text;
                reader = command.ExecuteReader();
                weeklyStartTime = reader.Read() ? reader.GetString(0) : "";
                command.Dispose();

                command = new OracleCommand();
                command.Connection = connection;
                command.CommandText = "Select value from ad_config where key = 'NIGHTLYSTARTTIME'";
                command.CommandType = CommandType.Text;
                reader = command.ExecuteReader();
                nightlyStartTime = reader.Read() ? reader.GetString(0) : "";
                command.Dispose();
            }
        }

        if (weeklyStartTime != null && !weeklyStartTime.Equals("") && nightlyStartTime != null && !nightlyStartTime.Equals(""))
        {
            DateTime wStartTime;
            DateTime nStartTime;
            bool wStartTimeErr = DateTime.TryParseExact(weeklyStartTime.Trim(), "M/dd/yyyy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out wStartTime);
            bool nStartTimeErr = DateTime.TryParseExact(nightlyStartTime.Trim(), "M/dd/yyyy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out nStartTime);
            if (wStartTimeErr && nStartTimeErr)
            {
                try
                {
                    string WindowsAccUsr = ConfigurationManager.AppSettings["WindowsAccUsr"];
                    string WindowsAccPwd = ConfigurationManager.AppSettings["WindowsAccPwd"];
                    string WeeklyDownloadURL = ConfigurationManager.AppSettings["WeeklyDownloadURL"];
                    string NightlyDownloadURL = ConfigurationManager.AppSettings["NightlyDownloadURL"];

                    string scriptLocation = HttpRuntime.AppDomainAppPath + "Config\\callurl.bat";
                    using (TaskService ts = new TaskService("", WindowsAccUsr, "Local", WindowsAccPwd, true))
                    {

                        WeeklyTrigger wt = new WeeklyTrigger(ConvertDayOfWeek(wStartTime.DayOfWeek), 1);
                        wt.StartBoundary = wStartTime;
                        //wt.DaysOfWeek = ConvertDayOfWeek(wStartTime.DayOfWeek);                  
                        //wt.WeeksInterval = 1;
                        ExecAction ea = new ExecAction();
                        ea.Path = scriptLocation;
                        ea.Arguments = "\"" + WeeklyDownloadURL + "\"";
                        Task wTask = ts.GetTask("WeeklyPharmacyDownload");
                        TaskDefinition wTd = (wTask == null) ? ts.NewTask() : wTask.Definition;

                        wTd.Triggers.Clear();
                        wTd.Triggers.Add(wt);
                        wTd.Actions.Clear();
                        wTd.Actions.Add(ea);
                        ts.RootFolder.RegisterTaskDefinition("WeeklyPharmacyDownload", wTd, TaskCreation.CreateOrUpdate, WindowsAccUsr, WindowsAccPwd, TaskLogonType.Password, null);

                        DailyTrigger dt = new DailyTrigger(1);
                        dt.StartBoundary = nStartTime;
                        ExecAction ean = new ExecAction();
                        ean.Path = scriptLocation;
                        ean.Arguments = "\"" + NightlyDownloadURL + "\"";
                        Task nTask = ts.GetTask("NightlyPharmacyDownload");
                        TaskDefinition nTd = (nTask == null) ? ts.NewTask() : nTask.Definition;

                        nTd.Triggers.Clear();
                        nTd.Triggers.Add(dt);
                        nTd.Actions.Clear();
                        nTd.Actions.Add(ean);
                        ts.RootFolder.RegisterTaskDefinition("NightlyPharmacyDownload", nTd, TaskCreation.CreateOrUpdate, WindowsAccUsr, WindowsAccPwd, TaskLogonType.Password, null);

                        Response.Write("Create scheduler successfully!");

                    }
                }
                catch (Exception ex)
                {
                    Response.Write("Error when creating scheduler: " + ex.Message);

                }
            }
            else
            {
                Response.Write("Invalid date time.");
            }
        }
        else
        {
            Response.Write("Please enter weekly and nightly download start time.");
        }
    }

    private DaysOfTheWeek ConvertDayOfWeek(DayOfWeek dow)
    {
        switch (dow)
        {
            case(DayOfWeek.Friday):
                return DaysOfTheWeek.Friday;
            case(DayOfWeek.Monday):
                return DaysOfTheWeek.Monday;
            case (DayOfWeek.Saturday):
                return DaysOfTheWeek.Saturday;
            case (DayOfWeek.Sunday):
                return DaysOfTheWeek.Sunday;
            case (DayOfWeek.Thursday):
                return DaysOfTheWeek.Thursday;
            case (DayOfWeek.Tuesday):
                return DaysOfTheWeek.Tuesday;
            case (DayOfWeek.Wednesday):
                return DaysOfTheWeek.Wednesday;
            default:
                return DaysOfTheWeek.AllDays;
        }

    }
}