﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default_m.aspx.cs" Inherits="Default_m" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sure Script Request Submission</title>
     <script type="text/javascript" src="resources/scripts/jquery-1.3.2.min.js"></script>
     <script type="text/javascript">
         function showPDF(tid) {
             SendPrintConfirmation(tid);
             var sURL = "/surescriptInterface/transactionid/" + tid + "/" + tid + ".pdf";
             popScrollWindowMax(sURL, 800, 600);
         }
         function popScrollWindowMax(action, winwidth, winheight) {
             var pw = null;
             pw = window.open("", "", "toolbar=no,width=" + winwidth + ",height=" + winheight + ",directories=no,status=no,scrollbars=yes,resizable=yes,menubar=no");
             if (pw != null) {
                 if (pw.opener == null) {
                     pw.opener = self;
                 }
                 pw.location.href = action;
             }
         }
         function SendPrintConfirmation(trid) {
             var value = trid;
             $.ajax({
                 type: "POST",
                 url: "Default.aspx/SendConfirmation",
                 data: '{value: "' + trid + '" }',
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: OnSuccess,
                 failure: function (response) {
                     // alert(response.d);
                 }
             });
         }
         function OnSuccess(response) {
             // alert(response.d);
         }

    </script>
</head>
<body style="font-family:Arial; font-size:0.8em">
    <form id="form1" runat="server">
    <div>
    <%--<asp:Label ID="lblMessage" Text="Successfully Post Data" runat="server" ></asp:Label>--%>
    <asp:HyperLink id="btnNote" runat="server" text="Back to Current Medication" />
    <!--table width="50%" cellspacing="1" cellpadding="1">
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="Pharmacy ID"></asp:Label></td>
            <td><asp:TextBox ID="PharmacyID" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label2" runat="server" Text="Doctor ID"></asp:Label></td>
            <td><asp:TextBox ID="DoctorID" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label3" runat="server" Text="Patient ID"></asp:Label></td>
            <td><asp:TextBox ID="PatientID" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label4" runat="server" Text="Drug ID"></asp:Label></td>
            <td><asp:TextBox ID="DrugID" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="Label5" runat="server" Text="Visit Key"></asp:Label></td>
            <td><asp:TextBox ID="VisitKey" runat="server"></asp:TextBox></td>
        </tr>  
    </table>
        <input type="submit" value = "Submit" /-->

        <asp:Panel ID="PanelLinks" runat="server"></asp:Panel> 

       <br />

     </div>
    </form>
</body>
</html>
