using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;
using System.Collections;
using WEBeDoctorPharmacy;

public partial class Default_m : System.Web.UI.Page 
{
    //public string Visit_Key;
    //public string Patient_ID;
    //public string MsgType;
    //public string FinalMsg;
    //public string NCPDPID;
    //public string Followup_Flag;
    //public string is_sub;
    //public string Physician_ID;
    //public string selected_mid;
    private StringBuilder sb;
    //private string EnvironmentUrl = "http://192.168.73.4";
    private string EnvironmentUrl = ConfigurationManager.AppSettings["EnvironmentURL"].ToString();

  protected void Page_Load(object sender, EventArgs e)
    {
        //StringBuilder sb = new StringBuilder();
        //if (Session["arrMedications"].ToString() != "")
        //{
        //    Response.Write(Session["SavedArray"].ToString());
        //}
        //Session("arrMedications") = arrMedications

        //Response.Write(Request.QueryString["Physician_ID"].ToString() + "<BR>");
        if (!Page.IsPostBack)
        {
            sb = new StringBuilder();
            string[] arrVisitKey = Request.QueryString["Visit_Key"].Split(',');
            string[] arrPatientIDs = Request.QueryString["Patient_ID"].Split(',');
            string[] arrMsg_Type = Request.QueryString["MsgType"].Split(',');
            string[] arrFinal_Msg = Request.QueryString["FinalMsg"].Split(',');
            string[] arrselected_mid = Request.QueryString["selected_mid"].Split(',');
            string[] arrPhysician_ID = Request.QueryString["Physician_ID"].Split(',');
            string[] arris_sub = Request.QueryString["is_sub"].Split(',');
            string[] arrFollowup_Flag = Request.QueryString["Followup_Flag"].Split(',');
            string[] arrNCPDPID = Request.QueryString["NCPDPID"].Split(',');
            string[] arrDrugName = Request.QueryString["drug_name"].Split(',');


            //string VisitKey = Request["Visit_Key"].ToString();
            //string PatientID = Request["Patient_ID"].ToString();
            //string Msg_Type = Request["MsgType"].ToString();
            //string Final_Msg = Request["FinalMsg"].ToString();

            //for (int loop = 0; loop <= arrPatientIDs.Length - 1; loop++)
            //{
            //    Response.Write(arrVisitKey[loop] + ", " + arrPatientIDs[loop] + ", NewRX" + ", " + arrFinal_Msg[loop] + ", " + arrselected_mid[loop] + ", " + arrPhysician_ID[loop] + ", " + arris_sub[loop] + ", " + arrFollowup_Flag[loop] + ", " + arrNCPDPID[loop] + "," + arrDrugName[loop] + "<BR>");
            //}

            //Response.End();
            WEBeDoctorPharmacy.SS_Logger.log("Starting SS Integration--------");
            for (int loop = 0; loop <= arrPatientIDs.Length - 1; loop++)
            {
                LoadMain(arrVisitKey[loop], arrPatientIDs[loop], "NewRX", arrFinal_Msg[loop], arrselected_mid[loop], arrPhysician_ID[loop], arris_sub[loop], arrFollowup_Flag[loop], arrNCPDPID[loop], arrDrugName[loop]);
            }
            if (arrPatientIDs.Length > 0)
            {
                btnNote.NavigateUrl = EnvironmentUrl + "/practice/notes/display/medication_history.asp?visit_key=" + arrVisitKey[0] + "&patient_id=" + arrPatientIDs[0] + "&show=curr&MsgType=NewRX";
            }
            WEBeDoctorPharmacy.SS_Logger.log("Multiple submission - completed --------");
            Response.Write(sb.ToString());
        }
    }

    protected void LoadMain(string Visit_Key, string Patient_ID, string MsgType, string FinalMsg, string selected_mid, string Physician_ID, string is_sub, string Followup_Flag, string NCPDPID, string DrugName)
    {
        //if (IsPostBack)
        //{ 
        try
        {

            WEBeDoctorPharmacy.SS_Logger.log("  ===dumping request parms=====");
            WEBeDoctorPharmacy.SS_Logger.log("      Visit_Key=" + Visit_Key);
            WEBeDoctorPharmacy.SS_Logger.log("      Patient_ID=" + Patient_ID);
            WEBeDoctorPharmacy.SS_Logger.log("      MsgType=" + MsgType);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + FinalMsg);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + selected_mid);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + Physician_ID);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + is_sub);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + Followup_Flag);
            WEBeDoctorPharmacy.SS_Logger.log("      FinalMsg=" + NCPDPID);
            WEBeDoctorPharmacy.SS_Logger.log("  ===end dumping request parms=====");

            //string FinalMsg = "NO";
            string SSURL;

            //Response.Write(Visit_Key);
            //Response.Write(Patient_ID);
            //Response.End();

            WEBeDoctorPharmacy.SS_Logger.log(" 1. Creating XMLEnvelope");

            WEBeDoctorPharmacy.Message mMessage = CreateXMLEnvelop(Patient_ID, NCPDPID, Physician_ID, selected_mid, Visit_Key, Followup_Flag, MsgType);
            if (mMessage.msgHeader.MessageID != null)
            {
                //authenticate request
                //if (AuthenticateRequest())
                // {

                WEBeDoctorPharmacy.SS_Logger.log(" 2. Sending Request");
                FileStream FS = new FileStream(@"d:\UserFiles\SSFiles\Request\" + mMessage.msgHeader.MessageID + ".xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                XmlDocument inputXML = new XmlDocument();
                inputXML.Load(FS);
                if (MsgType == "DirDwnld" || MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber")
                {
                    SSURL = ConfigurationManager.AppSettings["DSURL"].ToString();
                }
                else
                {
                    SSURL = ConfigurationManager.AppSettings["URL"].ToString(); ;
                }
                // Send Request.
                //CredentialCache ssUserPWD = new CredentialCache();
                //ssUserPWD.Add(new Uri("https://staging.surescripts.net/WebeDoctorTest/AuthenticatingXmlServer.aspx"), "Digest", new NetworkCredential("WebeDoctorU$er", "r0tc06363w"));


                ArrayList whiteList = new ArrayList();
                whiteList.Add("/Message/Body/RefillResponse/Response");
                whiteList.Add("/Message/Body/RefillResponse/Response/Approved");
                whiteList.Add("/Message/Body/RefillResponse/Response/Denied");
                whiteList.Add("/Message/Body/RefillResponse/Response/DenialReason");
                whiteList.Add("/Message/Body/RefillResponse/Response/ApprovedWithChanges");
                whiteList.Add("/Message/Body/RefillResponse/Response/DeniedNewPrescriptionToFollow");

                SS_Logger.log("----Pruning Response----");
                SS_Logger.log(inputXML.InnerXml);
                inputXML = new XmlPruner().pruneXml(inputXML, whiteList);
                SS_Logger.log("----Finished Pruning Response----");
                SS_Logger.log(inputXML.InnerXml);

                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(inputXML.InnerXml);
                //System.Text.Encoding.Convert(System.Text.Encoding.UTF8, System.Text.Encoding.ASCII, bytes);
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(SSURL);
                string AuthorizationCredentials = EncodeUIDPwd(MsgType);
                if (MsgType != "DirDwnld")
                {
                    req.Headers.Add("Authorization", "Basic " + AuthorizationCredentials);
                }
                //Response.Write(req.Headers);
                //Response.Write(SSURL);
                //req.Credentials = ssUserPWD;
                //bytes.ToString().Replace("?","");
                req.Timeout = 600000;
                req.Method = "Post";
                req.ContentLength = bytes.Length;
                req.ContentType = "text/xml";
                using (Stream requestStream = req.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                try
                {

                    WEBeDoctorPharmacy.SS_Logger.log(" 3. Processing Response");

                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();



                    // Save Response XML to file.
                    Guid FileNameGuid = System.Guid.NewGuid();
                    string FileName = "";

                    Stream responseStream = resp.GetResponseStream();
                    XmlDocument responseXML = new XmlDocument();

                    if (MsgType == "NO" &&
                        (MsgType == "DirDwnld" || MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber"))
                    {
                        WEBeDoctorPharmacy.SS_Logger.log("!!!!!directing response to log instead of processing it!!!!  Later code probably won't work.");
                        WEBeDoctorPharmacy.SS_Logger.log("=========");
                        WEBeDoctorPharmacy.SS_Logger.log("  Response Contents");
                        StringWriter sw = new StringWriter();
                        int b;
                        while ((b = responseStream.ReadByte()) != -1)
                        {
                            sw.Write((char)b);
                        }
                        WEBeDoctorPharmacy.SS_Logger.log(sw.ToString());
                        WEBeDoctorPharmacy.SS_Logger.log("=========");
                    }

                    WEBeDoctorPharmacy.SS_Logger.log(" 4. Unmarshalling XML from response");

                    responseXML.Load(responseStream);
                    FileName = FileNameGuid.ToString().Replace("-", "");

                    WEBeDoctorPharmacy.SS_Logger.log(" 5. Saving Response file");
                    responseXML.Save(@"d:\UserFiles\SSFiles\Response\" + FileName + ".xml");

                    WEBeDoctorPharmacy.SS_Logger.log(" 6. Reading Response file");
                    // Load Response XML into object
                    XmlSerializer Y = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message));
                    TextReader z = new StreamReader(@"d:\UserFiles\SSFiles\Response\" + FileName + ".xml");
                    WEBeDoctorPharmacy.Message Msg = (WEBeDoctorPharmacy.Message)Y.Deserialize(z);
                    z.Close();


                    //StreamReader readStream = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    //Char[] read = new Char[512];
                    //// Reads 256 characters at a time.    
                    //int count = readStream.Read(read, 0, 512);
                    //while (count > 0)
                    //{
                    //    // Dumps the 256 characters on a string and displays the string to the console.
                    //    String str = new String(read, 0, count);
                    //    Response.Write(str);
                    //    count = readStream.Read(read, 0, 512);

                    //save the status of submission
                    if (Msg.msgBody.Status != null)
                    {
                        if (Msg.msgBody.Status.Code == "010")
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", "Submission Successful");

                            if (mMessage.msgBody.UpdatePrescriber != null)
                            {
                                Msg.UpdatePrescriberSPI(mMessage.msgBody.UpdatePrescriber.mPrescriber.mPrescriberID.DEANumber, mMessage.msgBody.UpdatePrescriber.mPrescriber.mPrescriberID.SPI, mMessage.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel);
                            }

                            sb.Append("Drug Name " + DrugName + " Submitted Successfully to Pharmacy" + "<BR />");
                            //sb.append("<BR>");
                        }
                        else if (Msg.msgBody.Status.Code == "000")
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", "Submitted To Next Receiver.");

                            if (mMessage.msgBody.UpdatePrescriber != null)
                            {
                                Msg.UpdatePrescriberSPI(mMessage.msgBody.UpdatePrescriber.mPrescriber.mPrescriberID.DEANumber, mMessage.msgBody.UpdatePrescriber.mPrescriber.mPrescriberID.SPI, mMessage.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel);
                            }
                            sb.Append("Drug Name " + DrugName + " Submitted Successfully to Pharmacy" + "<BR />");
                        }
                        else
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "Error", Msg.msgBody.Status.Code.ToString());
                            sb.Append("Drug Name " + DrugName + " could not submitted to Pharmacy due to error" + "<BR />");
                        }
                    }
                    else if (Msg.msgBody.Error != null)
                    {
                        Msg.UpdateStatus(Msg.msgHeader.MessageID, "Error", Msg.msgBody.Error.Description.Replace("'", ""));
                        sb.Append("Drug Name " + DrugName + " could not submitted to Pharmacy due to error" + "<BR />");
                    }

                    // DirectoryService Responses
                    // DirectoryDownload Response
                    else if (Msg.msgBody.DirDwnldResponse != null)
                    {
                        if (Msg.msgBody.DirDwnldResponse.URL != null)
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", Msg.msgBody.DirDwnldResponse.URL.ToString());
                            // download the file
                            DownloadDirectoryFile(Msg.msgBody.DirDwnldResponse.URL.ToString(), Request["style"].ToString());
                        }
                    }
                    else if (Msg.msgBody.AddPrescriberResp != null || Msg.msgBody.AddPrescriberLocationResponse != null)
                    {
                        //if (Msg.msgBody.AddPrescriberResp.mResponse != null)
                        //{
                        //    if (Msg.msgBody.AddPrescriberResp.mResponse.ApprovedResp != null)
                        //    {
                        /// if (Msg.msgBody.AddPrescriberResp.mPrescriber.mPrescriberID.SPI != null)
                        // {
                        // Msg.UpdateStatus(Msg.msgHeader.MessageID,"OK", "Physician Saved - SPI: " + Msg.msgBody.AddPrescriberResp.mPrescriber.mPrescriberID.SPI);
                        // Msg.UpdatePrescriberSPI(Msg.msgBody.AddPrescriberResp.mPrescriber.mPrescriberID.DEANumber, Msg.msgBody.AddPrescriberResp.mPrescriber.mPrescriberID.SPI, Msg.msgBody.AddPrescriberResp.mPrescriber.mDirectoryInfo.ServiceLevel);
                        // }
                        //added by hari
                        if (Msg.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberID.SPI != null)
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", "Physician Saved - SPI: " + Msg.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberID.SPI);
                            Msg.UpdatePrescriberSPI(Msg.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberID.DEANumber, Msg.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberID.SPI, Msg.msgBody.AddPrescriberLocationResponse.mPrescriber.mDirectoryInfo.ServiceLevel);
                        }



                        //    }
                        //}
                    }
                    //}
                }
                catch (Exception ex)
                {
                    WEBeDoctorPharmacy.SS_Logger.log("!!!Exception in SS interface!");
                    WEBeDoctorPharmacy.SS_Logger.log(ex.Message);
                    WEBeDoctorPharmacy.SS_Logger.log(ex.StackTrace.ToString());
                    Response.Write("Error creating Web Request! -" + ex.Message);
                    sb.Append("Drug Name " + DrugName + " could not submitted to Pharmacy due to error" + "<BR />");
                    //mMessage.UpdateStatus("Error", "Submission Unsuccessful");
                }

                finally
                {
                    if (MsgType == "NewRX" && FinalMsg == "YES")
                    {

                        //Response.Redirect("http://192.168.73.4/practice/notes/display/medication_history.asp?visit_key=" + Visit_Key + "&patient_id=" + Patient_ID + "&show=curr&MsgType=" + MsgType);
                        //Session["visit_key"] = Visit_Key;
                        //Session["patient_id"] = Patient_ID;
                        //Session["show"] = "curr";
                        //Session["MsgType"] = MsgType;
                        //
                    }
                    else
                    {
                    }
                                       


                }
            }
            else
            {
                Response.Write("Error creating the XML file!");
            }
            //}
        }
        catch (Exception Ex1)
        {
            WEBeDoctorPharmacy.SS_Logger.log("!!!Exception in SS interface!!!");
            WEBeDoctorPharmacy.SS_Logger.log(Ex1.Message);
            WEBeDoctorPharmacy.SS_Logger.log(Ex1.StackTrace.ToString());
            Response.Write("Error! -" + Ex1.Message);
            sb.Append("Drug Name " + DrugName + " could not submitted to Pharmacy due to error" + "<BR />");
        }
    }

    protected WEBeDoctorPharmacy.Message CreateXMLEnvelop(string Patient_ID, string NCPDPID, string Physician_ID, string selected_mid, string Visit_Key, string Followup_Flag, string MsgType)
    {
        try
        {
            string[] inputArgs = new string[11] { null, null, null, null, null, null, null, null, null, null, null };
            string FollowupFlag = "N";
            string RefillResponseContent;

            //inputArgs[0] = PharmacyID.Text.ToString();
            //inputArgs[1] = DoctorID.Text.ToString();
            //inputArgs[2] = PatientID.Text.ToString();
            //inputArgs[3] = "304";
            //inputArgs[4] = VisitKey.Text.ToString();

            //inputArgs[0] = "0001074";
            //inputArgs[1] = "LASERTECH";
            //inputArgs[2] = "sithihou2001";
            //inputArgs[3] = "304";
            //inputArgs[4] = "2297SFarooquilenzgertrude";

            WEBeDoctorPharmacy.Message mMessage;

            //string MsgType = Request["MsgType"].ToString();

            switch (MsgType)
            {
                case "NewRX":
                    //inputArgs[0] = "0001074";
                    inputArgs[0] = NCPDPID;
                    inputArgs[1] = Physician_ID;
                    inputArgs[2] = Patient_ID;
                    inputArgs[3] = selected_mid;
                    inputArgs[4] = Visit_Key;
                    inputArgs[5] = (Request["refres_messageid"] == null ? null : Request["refres_messageid"].ToString());
                    FollowupFlag = Followup_Flag;
                    break;
                case "RefillResponse":
                    //inputArgs[0] = "0001074";
                    inputArgs[0] = Request["NCPDPID"].ToString();
                    inputArgs[1] = Request["Physician_ID"].ToString();
                    inputArgs[2] = Request["Patient_ID"].ToString();
                    inputArgs[3] = Request["selected_mid"].ToString();
                    inputArgs[4] = Request["Visit_Key"].ToString();
                    inputArgs[5] = Request["Response_Type"].ToString();
                    inputArgs[6] = Request["Response_Reason_Code"].ToString();
                    inputArgs[7] = Request["Response_Note"].ToString();
                    inputArgs[8] = Request["request_messageid"].ToString();
                    inputArgs[9] = Request["Refills"];
                    //inputArgs[10] = Request["Substitutions"];
                    
                    break;
                case "DirDwnld":
                    
                    if( Request["style"] != null )
                    {
                        inputArgs[0] = Request["style"].ToString();
                    }
                    else
                    {
                        inputArgs[0] = "";
                    }
                    
                    break;

                case "AddPrescriber":
                case "UpdatePrescriber":
                    inputArgs[1] = Request["Physician_ID"].ToString();

                    int i = 0;

                    if (!(Request["serviceLevel_0"] == null || Request["serviceLevel_0"].ToString() == "N"))
                    {
                        i |= 1;
                    }

                    if (!(Request["serviceLevel_1"] == null || Request["serviceLevel_1"].ToString() == "N"))
                    {
                        i |= 2;
                    }
                    if (!(Request["serviceLevel_2"] == null || Request["serviceLevel_2"].ToString() == "N"))
                    {
                        i |= 4;
                    }
                    if (!(Request["serviceLevel_3"] == null || Request["serviceLevel_3"].ToString() == "N"))
                    {
                        i |= 8;
                    }
                    if (!(Request["serviceLevel_4"] == null || Request["serviceLevel_4"].ToString() == "N"))
                    {
                        i |= 16;
                    }

                    inputArgs[2] = i.ToString();

                    break;
            }


            


            //Response.Write(MsgType+"|"+inputArgs[2].ToString() + "|" + inputArgs[3].ToString() + "|" + inputArgs[4].ToString() + "|" + inputArgs[5].ToString() + "|" + inputArgs[6].ToString() + "|" + inputArgs[7].ToString());

            //12/20/2008 Removed these line since I'm adding the NS info in the .NET type Message itself.
            //mMessage.xmlns = new XmlSerializerNamespaces();
            //mMessage.xmlns.Add("", "http://www.surescripts.com/messaging");

            //8/22/2009 kxu: Do clone/merge logic if it's a RefillResponse and RefillRequest msg file exists.
            if (MsgType == "RefillResponse" && File.Exists(@"d:\UserFiles\SSFiles\Response\" + inputArgs[8] + ".xml"))
            {
                RefillResponseContent = GetTextFromXMLFile(@"d:\UserFiles\SSFiles\Response\" + inputArgs[8] + ".xml").Replace("RefillRequest", "RefillResponse");

                WEBeDoctorPharmacy.SS_Logger.log(RefillResponseContent);

                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(RefillResponseContent);
                MemoryStream ms = new MemoryStream(bytes);
   

                // Deserializes RefillResponse XML Copied from RefillRequest
                XmlSerializer x = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message));
                //TextWriter tw = new StreamWriter(@"d:\UserFiles\SSFiles\Request\" + 456 + ".xml");
                mMessage = (WEBeDoctorPharmacy.Message)x.Deserialize(ms);
                //x.Serialize(tw, mMessage);

                //figure out if we need approved or approvedwithChanges
                //if (inputArgs[5] != null && inputArgs[5] == "Approved")
                //{
                //    if (mMessage.msgBody.RefResp.mMedication.Refills != null
                //       && mMessage.msgBody.RefResp.mMedication.Refills.Quantity != null
                //       && mMessage.msgBody.RefResp.mMedication.Refills.Quantity != "0")
                //    {
                //        //they sent us a specific amount of refills and we are overriding it
                //        int oldRefills = Int32.Parse(mMessage.msgBody.RefResp.mMedication.Refills.Quantity);
                //        int newRefills = Int32.Parse(inputArgs[9]);

                //        if (oldRefills != newRefills - 1)
                //        {
                //            inputArgs[5] = "ApprovedWithChanges";
                //        }
                //    }
                //}
                


                // Todo List:
                // 1.  Re-generate MessageID
                // 2.  Re-generate SentTime
                mMessage.SetMessageHeaderFromClone();
                // 3.  Construct Response Element
                // 4.  Update Refill Quantity, Subsitution and WrittenDate according to inputArgs values.
                mMessage.SetRefillResponseFromClone(inputArgs);
            }
            else
            {
                mMessage = new WEBeDoctorPharmacy.Message(MsgType, inputArgs);
            }
            mMessage.ConstructHeaderFields();
            if (FollowupFlag == "Y")
            {
                MsgType = "NewRxFollowup";
            }
            mMessage.LoadCorrelatedMessageInfo(MsgType, inputArgs);

            string relatedId = mMessage.msgHeader.RelatesToMessageID;

            if (FollowupFlag == "Y")
            {
                mMessage.msgHeader.RelatesToMessageID = null;
            }
            
            XmlSerializer Y = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message));
            //XmlSerializerNamespaces NS = new XmlSerializerNamespaces();
            //NS.Add(string.Empty, "http://www.surescripts.com/messaging");
            TextWriter z = new StreamWriter(@"d:\UserFiles\SSFiles\Request\" + mMessage.msgHeader.MessageID + ".xml");
            Y.Serialize(z, mMessage);
            z.Close();

            if (FollowupFlag == "Y")
            {
                mMessage.msgHeader.RelatesToMessageID = relatedId;
            }


            //save message
            string DrugID = mMessage.Save(mMessage.msgHeader.MessageID);



            return mMessage;
        }
        catch(Exception ex)
        {
            Response.Write("Error: " + ex.StackTrace);
            WEBeDoctorPharmacy.SS_Logger.log("Error: " + ex.StackTrace);

            if (ex.InnerException != null)
            {
                WEBeDoctorPharmacy.SS_Logger.log("Inner Error: " + ex.InnerException.StackTrace);

                if (ex.InnerException.InnerException != null)
                {
                    WEBeDoctorPharmacy.SS_Logger.log("Inner inner Error: " + ex.InnerException.InnerException.StackTrace);
                }

            
            }

            return null;
        }
    }

  private string GetTextFromXMLFile(string file)
  {
   StreamReader reader = new StreamReader(file);
   string ret = reader.ReadToEnd();
   reader.Close();
   return ret;
  }
  private bool AuthenticateRequest()
  {
        //Create new user/credential cache for authentication.
      String TestPostData = "Test";
      byte[] bytes = System.Text.Encoding.UTF8.GetBytes(TestPostData);
      CredentialCache ssUserPWD = new CredentialCache();
      ssUserPWD.Add(new Uri("https://messaging.surescripts.net/WebeDoctor4x/AuthenticatingXmlServer.aspx"), "Basic", new NetworkCredential("webemd2$ure$cript$", "e$cripswebe23*$%3"));
      string SSURL = "https://messaging.surescripts.net/WebeDoctor4x/AuthenticatingXmlServer.aspx";
      HttpWebRequest req = (HttpWebRequest)WebRequest.Create(SSURL);
      req.Credentials = ssUserPWD;
      req.Timeout = 3000;
      req.Method = "Post";
      req.ContentType = "text/xml";
      req.ContentLength = bytes.Length;
      using (Stream requestStream = req.GetRequestStream())
      {
          requestStream.Write(bytes, 0, bytes.Length);
      }
      //Response.Write("Authenticating....");

      HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

      //Response.Write("StatusCode: " + resp.StatusCode);

      if (resp.StatusCode == HttpStatusCode.OK)
      {
          return true;
      }
      return false;
   }
  private string EncodeUIDPwd(string MsgType)
    {
        string UID;
        string PWD ;

        if (MsgType == "DirDwnld")
        {
            UID = "webedoc";
            PWD = CalculateSHA1("ebe0700!");
            WEBeDoctorPharmacy.SS_Logger.log(PWD);
        }
        else{
            UID = "webemd2$ure$cript$";
            PWD = "e$cripswebe23*$%3";
        }
        string EncodedString = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(UID+":"+PWD));

        return EncodedString;
    }
  public static string CalculateSHA1(string text)
    {
        return BitConverter.ToString(SHA1Managed.Create().ComputeHash(Encoding.Default.GetBytes(text))).Replace("-", "");
    }

    private void DownloadDirectoryFile(string FileName,string FileType)
    {
        WebClient client = new WebClient();

        WEBeDoctorPharmacy.SS_Logger.log(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName);

        if (FileType == "full")
        {
            client.DownloadFile(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName,
                                @"d:\UserFiles\SSFiles\DirectoryDownloadFiles\Full\" + FileName);
        }
        else
        {
            client.DownloadFile(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName,
                                @"d:\UserFiles\SSFiles\DirectoryDownloadFiles\Nightly\" + FileName);
        }

    }


    protected void btnNote_Click(object sender, EventArgs e)
    {
        //Response.Redirect(EnvironmentUrl +"/practice/notes/display/medication_history.asp?visit_key=" + Session["visit_key"].ToString() + "&patient_id=" + Session["patient_id"].ToString() + "&show=" + Session["show"].ToString() + "&MsgType=" + Session["MsgType"].ToString());
    }
}
