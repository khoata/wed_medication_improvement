using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;

public partial class Default_Multiple : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Hashtable[] inputParams = ParseInputParams();
        HttpWebRequest Req;
        string Single_URL = "http://192.168.73.4/SureScriptInterface/Default.aspx";
        string PostData;

        WEBeDoctorPharmacy.SS_Logger.log("HASTABLE: " + inputParams.GetUpperBound(0));
        
        // Note: for some reason the inputParams.GetUpperBound(0) does not work.
        for (int i = 0; i < Request["request_messageid"].Split(',').GetUpperBound(0); i++)
        {
            //WEBeDoctorPharmacy.SS_Logger.log(" ***** IN LOOP 1***");
            
            try
            {
                PostData = ConstructPostData(inputParams[i]);
                WEBeDoctorPharmacy.SS_Logger.log(PostData);
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(PostData);
                Req = (HttpWebRequest)WebRequest.Create(Single_URL);
                Req.Timeout = 60000;
                Req.Method = "Post";
                Req.ContentLength = bytes.Length;
                Req.ContentType = "application/x-www-form-urlencoded";
                using (Stream requestStream = Req.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                WEBeDoctorPharmacy.SS_Logger.log("Default_Multiple: Processing Response");

                HttpWebResponse resp = (HttpWebResponse)Req.GetResponse();
                Response.Write(resp);
            }
            catch (Exception ex)
            {
                WEBeDoctorPharmacy.SS_Logger.log("Default_Multiple: Error in Creating Multiple Requests!");
                WEBeDoctorPharmacy.SS_Logger.log(ex.Message);
                WEBeDoctorPharmacy.SS_Logger.log(ex.StackTrace.ToString());
                Response.Write("Default Multiple Error creating Web Request! -" + ex.Message);
                //mMessage.UpdateStatus("Error", "Submission Unsuccessful");
            }
        }

        Response.Redirect("http://192.168.73.4/practice/notes/display/medication_history.asp?visit_key=&patient_id=&show=curr&MsgType=ReillResponse");


    }

    public string ConstructPostData(Hashtable inputParams)
    {
        string PostData ="";

        PostData = PostData + "NCPDPID=" + inputParams["NCPDPID"].ToString();
        PostData = PostData + "&Physician_ID=" + inputParams["Physician_ID"].ToString();
        PostData = PostData + "&Patient_ID=" + inputParams["Patient_ID"].ToString();
        PostData = PostData + "&selected_mid=" + inputParams["selected_mid"].ToString();
        PostData = PostData + "&Visit_Key=" + inputParams["Visit_Key"].ToString();
        PostData = PostData + "&Response_Type=" + inputParams["Response_Type"].ToString();
        PostData = PostData + "&Response_Reason_Code=" + inputParams["Response_Reason_Code"].ToString();
        PostData = PostData + "&Response_Note=" + HttpUtility.UrlEncode(inputParams["Response_Note"].ToString().Trim());
        PostData = PostData + "&request_messageid=" + inputParams["request_messageid"].ToString();
        PostData = PostData + "&Refills=" + inputParams["Refills"].ToString();
        PostData = PostData + "&MsgType=" + inputParams["MsgType"].ToString();
        if (inputParams.ContainsKey("Pharmacy_Requested_Mid") &&
            inputParams["Pharmacy_Requested_Mid"] != null)
            PostData = PostData + "&Pharmacy_Requested_Mid=" + inputParams["Pharmacy_Requested_Mid"].ToString();
        if (inputParams.ContainsKey("Prior_Authorization_Number") &&
            inputParams["Prior_Authorization_Number"] != null)
            PostData = PostData + "&Prior_Authorization_Number=" + inputParams["Prior_Authorization_Number"].ToString();

        PostData = PostData + "&epcs_twofactor_userid=" + Request["epcs_twofactor_userid"];
        PostData = PostData + "&epcs_twofactor_password=" + Request["epcs_twofactor_password"];
        PostData = PostData + "&epcs_twofactor_otpcode=" + Request["epcs_twofactor_otpcode"];
        PostData = PostData + "&is_sub=" + Request["is_sub"];
        PostData = PostData + "&ScheduleCode=" + Request["ScheduleCode"];
        PostData = PostData + "&DPSNumber=" + (string.IsNullOrEmpty(Request["DPSNumber"]) ? "" : Request["DPSNumber"]);
		PostData = PostData + "&DEANumber=" + (string.IsNullOrEmpty(Request["DEANumber"]) ? "" : Request["DEANumber"]);
		PostData = PostData + "&epcsAuditAuthor=" + (string.IsNullOrEmpty(Request["epcsAuditAuthor"]) ? "" : Request["epcsAuditAuthor"]);
        PostData = PostData + "&epcsAuditRole=" + (string.IsNullOrEmpty(Request["epcsAuditRole"]) ? "" : Request["epcsAuditRole"]);
		PostData = PostData + "&Response_Reason=" + (string.IsNullOrEmpty(Request["Response_Reason"]) ? "" : Request["Response_Reason"]);

        return PostData;
    }
    public Hashtable[] ParseInputParams()
    {
        string[] NCPDPID, Physician_ID, Patient_ID, Drug_ID, Visit_Key, Response_Type, Response_Reason_Code, Response_Note, request_messageid, Refills, MsgType, Pharmacy_Requested_Mid, Prior_Authorization_Number;
        Hashtable[] inputParams;

        WEBeDoctorPharmacy.SS_Logger.log("  ===dumping request parms=====");
        foreach (String parm in Request.Params.AllKeys)
        {
            WEBeDoctorPharmacy.SS_Logger.log("      " + parm + "=" + Request[parm]);
        }
        WEBeDoctorPharmacy.SS_Logger.log("  ===end dumping request parms=====");

        //inputArgs[0] = "0001074";
        NCPDPID = Request["NCPDPID"].Split(',');
        Physician_ID = Request["Physician_ID"].Split(',');
        Patient_ID = Request["Patient_ID"].Split(',');
        Drug_ID = Request["selected_mid"].Split(',');
        Visit_Key = Request["Visit_Key"].Split(',');
        Response_Type = Request["Response_Type"].Split(',');
        Response_Reason_Code = Request["Response_Reason_Code"].Split(',');
        Response_Note = Request["Response_Note"].Split(',');
        request_messageid = Request["request_messageid"].Split(',');
        Refills = Request["Refills"].Split(',');
        MsgType = Request["MsgType"].Split(',');
        Pharmacy_Requested_Mid = (!string.IsNullOrEmpty(Request["Pharmacy_Requested_Mid"]))
            ? Request["Pharmacy_Requested_Mid"].Split(',')
            : null;
        Prior_Authorization_Number = (!string.IsNullOrEmpty(Request["Prior_Authorization_Number"]))
            ? Request["Prior_Authorization_Number"].Split(',')
            : null;

        inputParams = new Hashtable[NCPDPID.GetUpperBound(0)];
        for (int i = 0; i < NCPDPID.GetUpperBound(0); i++)
        {
            inputParams[i] = new Hashtable();
            inputParams[i].Add("NCPDPID", NCPDPID[i]);
            inputParams[i].Add("Physician_ID", Physician_ID[i]);
            inputParams[i].Add("Patient_ID", Patient_ID[i]);
            inputParams[i].Add("selected_mid", Drug_ID[i]);
            inputParams[i].Add("Visit_Key", Visit_Key[i]);
            inputParams[i].Add("Response_Type", Response_Type[i]);
            inputParams[i].Add("Response_Reason_Code", Response_Reason_Code[i]);
            inputParams[i].Add("Response_Note", Response_Note[i]);
            inputParams[i].Add("request_messageid", request_messageid[i]);
            inputParams[i].Add("Refills", Refills[i]);
            inputParams[i].Add("MsgType", MsgType[i]);
            if (Pharmacy_Requested_Mid != null)
                inputParams[i].Add("Pharmacy_Requested_Mid", Pharmacy_Requested_Mid[i]);
            if (Prior_Authorization_Number != null)
                inputParams[i].Add("Prior_Authorization_Number", Prior_Authorization_Number[i]);
            //WEBeDoctorPharmacy.SS_Logger.log(" ***** IN LOOP 2***");
        }
        //WEBeDoctorPharmacy.SS_Logger.log(" ***** Number of Items in inputParams***" + inputParams[0].ContainsKey("NCPDPID"));
        return inputParams;
    }
}
