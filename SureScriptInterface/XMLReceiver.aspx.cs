using System;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;



public partial class XMLReceiver : System.Web.UI.Page
{
    protected string Single_URL = "http://192.168.73.4/SureScriptInterface/Default.aspx";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // if (Request.ContentType == "text/xml" && Request.ContentLength > 0)
            // {
                 string FileName = null;
                 String DrugID = null;

                 // get the input XML.
                 WEBeDoctorPharmacy.Message Msg = ReceiveRequestXML();
                 FileName = Msg.msgHeader.MessageID.Replace("-", "");
                 Msg.SynchronizePrescriberSPI();

                 //Update status 
                 WEBeDoctorPharmacy.SS_Logger.log("Entering Receiver----------------------");      
                 // send status message.
                 if (Msg.msgBody.RefReq != null)
                 {
                     // check for controlled substances.
                     //if (Msg.msgBody.RefReq.mMedication.DrugCode.ProductCode != null &&
                     //    Msg.msgBody.RefReq.mMedication.DrugCode.ProductCodeQualifier == "ND")
                     //{
                     //    string CSA_Schedule = Msg.CheckForControlledSubstance(Msg.msgBody.RefReq.mMedication.DrugCode.ProductCode.ToString());
                         
                         
                     //    if (CSA_Schedule != "0" )
                     //    {
                              
                     //    }
                     //}

                     if (Msg.msgBody.RefReq.mMedicationDispensed.DrugCoverageStatusCode != null)
                     {
                         foreach (string code in Msg.msgBody.RefReq.mMedicationDispensed.DrugCoverageStatusCode)
                         {
                             if (code == "CP")
                             {
                                 Msg.UpdateStatus(FileName, "Error", "Compounds items are not supported.");
                                 SendResponseMsg(Msg, 4);
                                 return;
                             }
                         }
                     }

                     // Parse XML and save into DB.
                     DrugID = Msg.Save(FileName,null);

                     if (DrugID == "")
                     {
                         Msg.UpdateStatus(FileName,"Error", "No matching Drug ID");
                         // Rreject message if drug id does not matach.
                         //SendResponseMsg(Msg, 5);
                     }
                     else
                     {
                         //int ValidationFlag = ValidateCorrelatedMsg(Msg, DrugID);

                         //if (ValidationFlag == 0)
                         //{
                         //    Msg.UpdateStatus(FileName,"OK", "Request received successfully.");
                         //}
                         //else if (ValidationFlag == 1)
                         //{
                         //    Msg.UpdateStatus(FileName,"Error", "Physician does not match.");
                         //}
                         //else if (ValidationFlag == 2)
                         //{
                         //    Msg.UpdateStatus(FileName,"Error", "Patient does not match.");
                         //}
                         //else if (ValidationFlag == 3)
                         //{
                         //    Msg.UpdateStatus(FileName,"Error", "Prescription does not match.");
                         //}

                         //SendResponseMsg(Msg, ValidationFlag);

                         // Do not reject any request, let doctors make decisions
                         Msg.UpdateStatus(FileName, "OK", "Request received successfully.");
                     }

                     // Confirm already receiving message successfully
                     SendResponseMsg(Msg, 0);
                 }
				 if (Msg.msgBody.CancelResp != null)
                 {
                     // Parse XML and save into DB.
                     DrugID = Msg.Save(FileName,null);

                     if (DrugID == "")
                     {
                         Msg.UpdateStatus(FileName,"Error", "No matching Cancel Request");
                     }
                     else
                     {
                         Msg.UpdateStatus(FileName, "OK", "Request received successfully.");
                         Msg.UpdateCancelStatus(Msg.msgBody.CancelResp.GetResponseStatus(), Msg.msgBody.CancelResp.GetResponseStatusDescription());
                     }

                     // Confirm already receiving message successfully
                     SendResponseMsg(Msg, 0);
                 }
                 if (Msg.msgBody.ChangeReq != null)
                 {
                     // Parse XML and save into DB.
                     DrugID = Msg.Save(FileName,null);

                     if (DrugID == "")
                     {
                         Msg.UpdateStatus(FileName,"Error", "No matching Drug ID");
                     }
                     else
                     {
                         // Do not reject any request, let doctors make decisions
                         Msg.UpdateStatus(FileName, "OK", "Request received successfully.");
                     }

                     // Confirm already receiving message successfully
                     SendResponseMsg(Msg, 0);
                 }
                 else
                 {
                     // Update unread status of corresponding message
                     Msg.UpdateUnreadStatusOfRelatedMessage(true);

                     if (Msg.msgBody.Status != null)
                     {
                         if (Msg.msgBody.Status.Code == "010")
                         {
                             Msg.UpdateStatus(Msg.msgHeader.RelatesToMessageID, "OK", "Submission Successful");
                             Msg.UpdateCancelStatus("Sent", "Cancel request sent to pharmacy and waiting for confirmation");
                         }
                         else if (Msg.msgBody.Status.Code == "000")
                         {
                             Msg.UpdateStatus(Msg.msgHeader.RelatesToMessageID, "Pending", "Submitted To Next Receiver.");
                             Msg.UpdateCancelStatus("Pending", "Submitted To Next Receiver");
                         }
                         else
                         {
                             Msg.UpdateStatus(Msg.msgHeader.RelatesToMessageID, "Error", Msg.msgBody.Status.Code.ToString());
                             Msg.UpdateCancelStatus("Error", Msg.msgBody.Status.Code.ToString());
                         }
                         SendResponseMsg(Msg, 0);
                     }
                     else if (Msg.msgBody.Verify != null)
                     {
                         if (Msg.msgBody.Verify.Status == null || Msg.msgBody.Verify.Status.Code == "010")
                         {
                             Msg.UpdateStatus(Msg.msgHeader.RelatesToMessageID, "Verified", "Receiver Acknowledged.");
                             Msg.UpdateCancelStatus("Sent", "Cancel request sent to pharmacy and waiting for confirmation");
                         }
                         else
                         {
                             Msg.UpdateStatus(Msg.msgHeader.RelatesToMessageID, "Error", Msg.msgBody.Verify.Status.Code.ToString());
                             Msg.UpdateCancelStatus("Error", Msg.msgBody.Status.Code.ToString());
                         }
                         SendResponseMsg(Msg, 0);
                     }
                     else if (Msg.msgBody.Error != null)
                     {
                         WEBeDoctorPharmacy.SS_Logger.log("Entered Error Block--------");
                         StringBuilder builder = new StringBuilder();
                         builder.Append(Msg.msgBody.Error.Code);
                         if (!string.IsNullOrEmpty(Msg.msgBody.Error.DescriptionCode))
                         {
                             builder.Append("-");
                             builder.Append(Msg.msgBody.Error.DescriptionCode);
                         }
                         if (!string.IsNullOrEmpty(Msg.msgBody.Error.Description))
                         {
                             builder.Append("-");
                             builder.Append(Msg.msgBody.Error.Description.Replace("'", ""));
                         }
                         Msg.UpdateStatus(Msg.msgHeader.RelatesToMessageID, "Error", builder.ToString());
                         Msg.UpdateCancelStatus("Error", builder.ToString());
                         SendResponseMsg(Msg, 0);
                     }
                 }


            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetSettledDups";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageId", OracleType.VarChar)).Value = Msg.msgHeader.MessageID;
            command.Parameters.Add(new OracleParameter("CurDups", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();

            int dupCount = 0;

            if (reader.Read())
            {
                dupCount = reader.GetInt32(0);
            }


            if (dupCount > 0)
            {
                WEBeDoctorPharmacy.SS_Logger.log("autodenying because of settled duplicate");
				WEBeDoctorPharmacy.SS_Logger.log(dupCount.ToString());
				WEBeDoctorPharmacy.SS_Logger.log(Msg.msgHeader.MessageID);

                WEBeDoctorPharmacy.SS_Logger.log( FileName );
                
                string PostData = "";

                PostData = PostData + "NCPDPID=" + Msg.msgBody.RefReq.mPharmacy.mPharmacyID.NCPDPID;
                PostData = PostData + "&Physician_ID=nomatter";
                PostData = PostData + "&Patient_ID=nomatter";
                PostData = PostData + "&selected_mid=" + DrugID;
                PostData = PostData + "&Visit_Key=nomatter";
                PostData = PostData + "&Response_Type=Denied";
                PostData = PostData + "&Response_Reason_Code=AP";
                PostData = PostData + "&Response_Note=Already Responded To";
                PostData = PostData + "&request_messageid=" + FileName;
                PostData = PostData + "&Refills=0";
                PostData = PostData + "&MsgType=RefillResponse";


                try
                {


                    WEBeDoctorPharmacy.SS_Logger.log(PostData);
                    byte[] bytes = System.Text.Encoding.ASCII.GetBytes(PostData);

                    HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(Single_URL);
                    Req.Timeout = 60000;
                    Req.Method = "Post";
                    Req.ContentLength = bytes.Length;
                    Req.ContentType = "application/x-www-form-urlencoded";
                    using (Stream requestStream = Req.GetRequestStream())
                    {
                        requestStream.Write(bytes, 0, bytes.Length);
                    }

                    HttpWebResponse resp = (HttpWebResponse)Req.GetResponse();
                }
                catch (Exception dontEvenKnowWhatToDoWithThis)
                {

                }














            }


        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message + ex.StackTrace);
        }


        
        

    }

    // Compares the received Message with the original NewRx and return the mismatch or return 0 if they all match.
    private int ValidateCorrelatedMsg(WEBeDoctorPharmacy.Message Msg, string DrugID)
    {
        string[] inputArgs = new string[8];

        if (Msg.msgHeader.PrescriberOrderNumber != null)
        {

            inputArgs = WEBeDoctorPharmacy.MessageFuncs.LoadNewRxParams(DrugID, Msg.msgHeader.PrescriberOrderNumber.ToString());

            WEBeDoctorPharmacy.Message NewRxMsg = new WEBeDoctorPharmacy.Message("NewRX", inputArgs);
            NewRxMsg.msgHeader.PrescriberOrderNumber = Msg.msgHeader.PrescriberOrderNumber.ToString();

            //check physician
            //if ((Msg.msgBody.RefReq.mPrescriber.mPrescriberID.SPI != NewRxMsg.msgBody.NewRq.mPrescriber.mPrescriberID.SPI)
            //    || (Msg.msgBody.RefReq.mPrescriber.mPrescriberName.FirstName != NewRxMsg.msgBody.NewRq.mPrescriber.mPrescriberName.FirstName)
            //    || (Msg.msgBody.RefReq.mPrescriber.mPrescriberName.LastName != NewRxMsg.msgBody.NewRq.mPrescriber.mPrescriberName.LastName))
            //    return 1;
            if ((Msg.msgBody.RefReq.mPrescriber.mPrescriberID.SPI != NewRxMsg.msgBody.NewRq.mPrescriber.mPrescriberID.SPI))
                return 1;
        }

        return 0;
    }

    private WEBeDoctorPharmacy.Message ReceiveRequestXML()
    {
        try
        {
            using (StreamReader stream = new StreamReader(Request.InputStream))
            {
                string content = stream.ReadToEnd();
                using (StringReader reader = new StringReader(content))
                {
                    XmlSerializer Y = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetNamespaceOfRxRoutingMessage());
                    WEBeDoctorPharmacy.Message Msg = (WEBeDoctorPharmacy.Message)Y.Deserialize(reader);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(content);
                    string fileName = Msg.msgHeader.MessageID.Replace("-", "");
                    xmlDoc.Save(@"d:\UserFiles\SSFiles\Response\" + fileName + ".xml");
                    return Msg;
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.StackTrace);
            return null;
        }
    }

    private void SendResponseMsg(WEBeDoctorPharmacy.Message Msg,int ValidationFlag)
    {

        if (Msg.msgBody.RefReq != null || Msg.msgBody.CancelResp != null || Msg.msgBody.ChangeReq != null || Msg.msgBody.Status != null || Msg.msgBody.Error != null || Msg.msgBody.Verify != null)
        {
            //Response.Write("Prescriber Name :" + Msg.msgBody.RefReq.mPrescriber.mClinicName.ToString() + "<BR>");
            //Response.Write("Patient Name :" + Msg.msgBody.RefReq.mPatient.mPatientName.LastName.ToString() + "," + Msg.msgBody.RefReq.mPatient.mPatientName.FirstName.ToString() + "<BR>");
            //Response.Write("Pharmacy Name :" + Msg.msgBody.RefReq.mPharmacy.mStoreName.ToString() + "<BR>");
            //Response.Write("Medication Name :" + Msg.msgBody.RefReq.mMedication.DrugDescription.ToString() + "<BR>";
            //Response.StatusCode = (int)HttpStatusCode.OK;
            // if (Response.IsClientConnected)
            // {
            string MessageID = CreateXMLEnvelop(Msg,ValidationFlag);
            if (MessageID != null)
            {
                FileStream FS = new FileStream(@"d:\UserFiles\SSFiles\Request\" + MessageID + ".xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                XmlDocument outputXML = new XmlDocument();
                outputXML.Load(FS);
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(outputXML.InnerXml);

                Response.Flush();
                Response.Clear();
                //Response.ContentType = "text/xml";
                Response.BinaryWrite(bytes);

            }
            //}
        }
    }

    protected string CreateXMLEnvelop(WEBeDoctorPharmacy.Message Msg,int ValidationFlag)
    {
        try
        {
            string[] inputArgs = new string[5];
            WEBeDoctorPharmacy.Message mMessage;

            if (ValidationFlag == 0)
            {
                inputArgs[0] = "";
                inputArgs[1] = "";
                inputArgs[2] = "";
                inputArgs[3] = "";
                inputArgs[4] = "";
                mMessage = new WEBeDoctorPharmacy.Message("Status", inputArgs);
            }
            else
            {
                inputArgs[0] = (string) ValidationFlag.ToString();
                inputArgs[1] = "";
                inputArgs[2] = "";
                inputArgs[3] = "";
                inputArgs[4] = "";
                mMessage = new WEBeDoctorPharmacy.Message("Error", inputArgs);
            }
            //12/20/2008 Removed these line since I'm adding the NS info in the .NET type Message itself.
            //mMessage.xmlns = new XmlSerializerNamespaces();
            //mMessage.xmlns.Add("", "http://www.surescripts.com/messaging");

            //Generate PON
            mMessage.msgHeader.PrescriberOrderNumber = Guid.NewGuid().ToString().Replace("-", "");
            //Set the header info equal to the input message header.
            mMessage.msgHeader.RelatesToMessageID = Msg.msgHeader.MessageID;
            mMessage.msgHeader.From = Msg.msgHeader.From;
            mMessage.msgHeader.To = Msg.msgHeader.To;
            mMessage.msgHeader.SentTime = Msg.msgHeader.SentTime;

            XmlSerializer Y = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetNamespaceOfRxRoutingMessage());
            TextWriter z = new StreamWriter(@"d:\UserFiles\SSFiles\Request\" + mMessage.msgHeader.MessageID + ".xml");
            Y.Serialize(z, mMessage);
            z.Close();

            //save message
            //mMessage.Save();

            return mMessage.msgHeader.MessageID.ToString();
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.StackTrace);
            return null;
        }
    }

    private void SendAutoDenialResponse(WEBeDoctorPharmacy.Message Msg)
    {

    }
}
