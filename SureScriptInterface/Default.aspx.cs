﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;
using System.Collections;
using WEBeDoctorPharmacy;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Text.RegularExpressions;
using EPCSSignature;

public partial class _Default : System.Web.UI.Page 
{

   string str_visit_Key = string.Empty;
   string str_Patient_ID = string.Empty;
   private string EnvironmentUrl = ConfigurationManager.AppSettings["EnvironmentURL"].ToString();

  protected void Page_Load(object sender, EventArgs e)
    {
        //PDR Work Start
        string PaymentNotes = string.Empty; 
        try
        {
            StringBuilder s = new StringBuilder();
            foreach (string key in Request.Form.Keys)
            {
                s.AppendLine(key + ": " + Request.Form[key] + "^");
            }
            string PostedValues = s.ToString();
            string PDRXML =  CallPDR(PostedValues);
           // Response.Write(s.ToString ()) ; 
            //Response.Write(Request["MsgType"].ToString() + "_____");
            //Response.Write(Request["Response_Note"].ToString() + "_____");

            PaymentNotes = GetPaymentNotes(PDRXML); 
            //if (!(string.IsNullOrWhiteSpace(PaymentNotes )))
            //{
            //    Response.Write (PaymentNotes) ; 
            //}
        }
        catch (Exception) { }
        //PDR Work End

        //if (IsPostBack)
        //{ 
        try
        {
            string epcs_twofactor_userid = Request["epcs_twofactor_userid"];
            string epcs_twofactor_password = Request["epcs_twofactor_password"];
            string epcs_twofactor_otpcode = Request["epcs_twofactor_otpcode"];
            string epcs_is_sub = Request["is_sub"];
            bool isEPCS = false;
            string scheduleCode = "";
            string dpsNumber = "";
            //EPCS: Authenticate Two-Factor
            if (epcs_is_sub == "Y" && (Request["Response_Type"] == null || !Request["Response_Type"].ToUpper().Contains("DENIED")))
            {
                isEPCS = true;
                scheduleCode = Request["ScheduleCode"];
                dpsNumber = string.IsNullOrEmpty(Request["DPSNumber"]) ? "" : Request["DPSNumber"];
                //string checkEPCSFields = CheckRequiredFieldsEPCS();
                //if (!string.IsNullOrEmpty(checkEPCSFields))
                //{
                //    Response.Write(checkEPCSFields);
                //    return;
                //}
                bool isAuthenticated = TwoFactorAuthenticate(epcs_twofactor_userid, epcs_twofactor_password, epcs_twofactor_otpcode);
                if (!isAuthenticated)
                {
                    //Response.Write("Authenticated Result: FAIL!<br/>");
                    //Response.Write("EPCS UserId: " + epcs_twofactor_userid + "<br/>");
                    //Response.Write("EPCS Password: " + epcs_twofactor_password + "<br/>");
                    //Response.Write("EPCS OTP Code: " + epcs_twofactor_otpcode + "<br/>");
                    Response.Write("Failed in signing with Two-Factor authentication!");
                    return;
                }
            }
            
            string epcs_approve = Request["epcs_approve"];
            if ("Y".Equals(epcs_approve) && "Y".Equals(epcs_is_sub))
            {
                string prescriberid = Request["prescriberid"];
                string is_epcs_access_control = Request["is_epcs_access_control"];
                string epcs = Request["epcs"];
                EPCSLogicalAccessControl("SUCCESS");
                Response.Redirect("http://192.168.73.4/practice/personal/epcs_access_control/approve_epcs.asp?prescriberid=" + prescriberid + "&is_epcs_access_control=" + is_epcs_access_control + "&epcs=" + epcs);
                return;
            }



            WEBeDoctorPharmacy.SS_Logger.log("Starting SS Integration--------");

            WEBeDoctorPharmacy.SS_Logger.log("  ===dumping request parms=====");
            foreach (String parm in Request.Params.AllKeys)
            {
                WEBeDoctorPharmacy.SS_Logger.log("      " + parm + "=" + Request[parm]);
            }
            WEBeDoctorPharmacy.SS_Logger.log("  ===end dumping request parms=====");



            string Visit_Key = Request["Visit_Key"];
            string Patient_ID = Request["Patient_ID"];
            string MsgType = Request["MsgType"].ToString();
            string SSURL;

            

            //Response.Write(Visit_Key);
            //Response.Write(Patient_ID);
            //Response.End();

            WEBeDoctorPharmacy.SS_Logger.log(" 1. Creating XMLEnvelope");

            WEBeDoctorPharmacy.Message mMessage = CreateXMLEnvelop(isEPCS, scheduleCode, dpsNumber, PaymentNotes);

            if (mMessage.msgHeader.MessageID != null)
            {
                //authenticate request
                //if (AuthenticateRequest())
                // {

                WEBeDoctorPharmacy.SS_Logger.log(" 2. Sending Request");
                FileStream FS = new FileStream(@"d:\UserFiles\SSFiles\Request\" + mMessage.msgHeader.MessageID + ".xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                XmlDocument inputXML = new XmlDocument();
                inputXML.Load(FS);
                if (MsgType == "DirDwnld" || MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber" )
                {
                    SSURL = ConfigurationManager.AppSettings["DSURL"].ToString();
                }
                else
                {
                    SSURL = ConfigurationManager.AppSettings["URL"].ToString();
                }
                // Send Request.
                //CredentialCache ssUserPWD = new CredentialCache();
                //ssUserPWD.Add(new Uri("https://staging.surescripts.net/WebeDoctorTest/AuthenticatingXmlServer.aspx"), "Digest", new NetworkCredential("WebeDoctorU$er", "r0tc06363w"));


                ArrayList whiteList = new ArrayList();
                whiteList.Add("/Message/Body/RefillResponse/Response");
                whiteList.Add("/Message/Body/RefillResponse/Response/Approved");
                whiteList.Add("/Message/Body/RefillResponse/Response/Denied");
                whiteList.Add("/Message/Body/RefillResponse/Response/DenialReason");
                whiteList.Add("/Message/Body/RefillResponse/Response/ApprovedWithChanges");
                whiteList.Add("/Message/Body/RefillResponse/Response/DeniedNewPrescriptionToFollow");
                whiteList.Add("/Message/Body/RxChangeResponse/Response");
                whiteList.Add("/Message/Body/RxChangeResponse/Response/Approved");
                whiteList.Add("/Message/Body/RxChangeResponse/Response/Denied");
                whiteList.Add("/Message/Body/RxChangeResponse/Response/DenialReason");
                whiteList.Add("/Message/Body/RxChangeResponse/Response/ApprovedWithChanges");
                whiteList.Add("/Message/Body/RxChangeResponse/Response/DeniedNewPrescriptionToFollow");

                SS_Logger.log("----Pruning Response----");
                SS_Logger.log(inputXML.InnerXml);
                inputXML = new XmlPruner().pruneXml(inputXML, whiteList);
                SS_Logger.log("----Finished Pruning Response----");
                SS_Logger.log(inputXML.InnerXml);
                
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(inputXML.InnerXml);
                //System.Text.Encoding.Convert(System.Text.Encoding.UTF8, System.Text.Encoding.ASCII, bytes);
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(SSURL);
                string AuthorizationCredentials = EncodeUIDPwd(MsgType);
                if (MsgType != "DirDwnld")
                {
                    req.Headers.Add("Authorization", "Basic " + AuthorizationCredentials);
                }
                //Response.Write(req.Headers);
                //Response.Write(SSURL);
                //req.Credentials = ssUserPWD;
                //bytes.ToString().Replace("?","");
                req.Timeout = 600000;
                req.Method = "Post";
                req.ContentLength = bytes.Length;
                req.ContentType = "text/xml";
                req.KeepAlive = false; 
                using (Stream requestStream = req.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                try
                {

                    WEBeDoctorPharmacy.SS_Logger.log(" 3. Processing Response");
                    
                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                    
                    
                    
                    // Save Response XML to file.
                    Guid FileNameGuid = System.Guid.NewGuid();
                    string FileName = "";

                    Stream responseStream = resp.GetResponseStream();
                    XmlDocument responseXML = new XmlDocument();

                    if (MsgType == "NO" &&
                        (MsgType == "DirDwnld" || MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber"))
                    {
                        WEBeDoctorPharmacy.SS_Logger.log("!!!!!directing response to log instead of processing it!!!!  Later code probably won't work.");
                        WEBeDoctorPharmacy.SS_Logger.log("=========");
                        WEBeDoctorPharmacy.SS_Logger.log("  Response Contents");
                        StringWriter sw = new StringWriter();
                        int b;
                        while ((b = responseStream.ReadByte()) != -1)
                        {
                            sw.Write((char)b);
                        }
                        WEBeDoctorPharmacy.SS_Logger.log(sw.ToString());
                        WEBeDoctorPharmacy.SS_Logger.log("=========");
                    }

                    WEBeDoctorPharmacy.SS_Logger.log(" 4. Unmarshalling XML from response");
                    responseXML.Load(responseStream);
                    FileName = FileNameGuid.ToString().Replace("-", "");

                    WEBeDoctorPharmacy.SS_Logger.log(" 5. Saving Response file");
                    responseXML.Save(@"d:\UserFiles\SSFiles\Response\" + FileName + ".xml");

                    WEBeDoctorPharmacy.SS_Logger.log(" 6. Reading Response file");
                    // Load Response XML into object
                    XmlSerializer Y = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetDefaultNamespace(MsgType));
                    TextReader z = new StreamReader(@"d:\UserFiles\SSFiles\Response\" + FileName + ".xml");
                    WEBeDoctorPharmacy.Message Msg = (WEBeDoctorPharmacy.Message)Y.Deserialize(z);
                    z.Close();


                    //StreamReader readStream = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    //Char[] read = new Char[512];
                    //// Reads 256 characters at a time.    
                    //int count = readStream.Read(read, 0, 512);
                    //while (count > 0)
                    //{
                    //    // Dumps the 256 characters on a string and displays the string to the console.
                    //    String str = new String(read, 0, count);
                    //    Response.Write(str);
                    //    count = readStream.Read(read, 0, 512);

                    //save the status of submission
                    if (Msg.msgBody.Status != null)
                    {
                        if (Msg.msgBody.Status.Code == "010")
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID,"OK", "Submission Successful");
                            if (epcs_is_sub == "Y")
                            {
                                EPCSFeaturesCalls(mMessage, Msg, "OK");
                            }

                            if (mMessage.msgBody.UpdatePrescriber != null)
                            {
                                Msg.UpdatePrescriberSPI(Request["Physician_ID"].ToString(), mMessage.msgBody.UpdatePrescriber.mPrescriber.mPrescriberID.DSSPI, mMessage.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel);

                                EPCSLogicalAccessControl("SUCCESS");
                            }

                            if (mMessage.msgBody.CancelReq != null)
                            {
                                Msg.UpdateCancelStatus("Sent", "Cancel request sent to pharmacy and waiting for confirmation");
                            }
                        }
                        else if (Msg.msgBody.Status.Code == "000")
                        {
                            if (mMessage.msgBody.UpdatePrescriber != null)
                            {
                                Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", "Submission Successful");
                                

                                Msg.UpdatePrescriberSPI(Request["Physician_ID"].ToString(), mMessage.msgBody.UpdatePrescriber.mPrescriber.mPrescriberID.DSSPI, mMessage.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel);

                                EPCSLogicalAccessControl("SUCCESS");
                            }
                            else
                            {
                                Msg.UpdateStatus(Msg.msgHeader.MessageID, "Pending", "Submitted To Next Receiver.");
                            }

                            if (epcs_is_sub == "Y")
                            {
                                EPCSFeaturesCalls(mMessage, Msg, "PENDING");                                   
                            }

                            if (mMessage.msgBody.CancelReq != null)
                            {
                                Msg.UpdateCancelStatus("Pending", "Submitted To Next Receiver");
                            }
                        }
                        else
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID,"Error", Msg.msgBody.Status.Code.ToString());
                            if (epcs_is_sub == "Y")
                            {
                                EPCSFeaturesCalls(mMessage, Msg, "ERROR");
                            }

                            if (mMessage.msgBody.CancelReq != null)
                            {
                                Msg.UpdateCancelStatus("Error", Msg.msgBody.Status.Code.ToString());
                            }
                        }
                    }
                    else if (Msg.msgBody.Error != null)
                    {
                        Msg.UpdateStatus(Msg.msgHeader.MessageID,"Error", Msg.msgBody.Error.Description.Replace("'", ""));
                        if (epcs_is_sub == "Y")
                        {
                            EPCSFeaturesCalls(mMessage, Msg, "ERROR");
                        }
                        if (mMessage.msgBody.UpdatePrescriber != null)
                        {
                            EPCSLogicalAccessControl("FAIL");
                        }

                        if (mMessage.msgBody.CancelReq != null)
                        {
                            Msg.UpdateCancelStatus("Error", Msg.msgBody.Error.Description.Replace("'", ""));
                        }
                    }

                    // DirectoryService Responses
                    // DirectoryDownload Response
                    else if (Msg.msgBody.DirDwnldResponse != null)
                    {
                        if (Msg.msgBody.DirDwnldResponse.URL != null)
                        {
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", Msg.msgBody.DirDwnldResponse.URL.ToString());
                            // download the file
                            DownloadDirectoryFile(Msg.msgBody.DirDwnldResponse.URL.ToString(), Request["style"].ToString());
                        }
                    }
                    else if (Msg.msgBody.AddPrescriberResp != null || Msg.msgBody.AddPrescriberLocationResponse != null)
                    {
                        DSPrescriber response = (Msg.msgBody.AddPrescriberResp != null)
                            ? Msg.msgBody.AddPrescriberResp
                            : (Msg.msgBody.AddPrescriberLocationResponse != null)
                               ? Msg.msgBody.AddPrescriberLocationResponse
                                : null;
                        if (response.mResponse != null && response.mResponse.ApprovedResp != null)
                        {
                            // SPI & ServiceLevel is required
                            // DEANumber never null, only prescribers who have NOT empty DEANumber can be sent to Surescripts
                            Msg.UpdateStatus(Msg.msgHeader.MessageID, "OK", "Physician Saved - SPI: " + response.mPrescriber.mPrescriberID.DSSPI);
                            Msg.UpdatePrescriberSPI(Request["Physician_ID"].ToString(), response.mPrescriber.mPrescriberID.DSSPI, response.mPrescriber.mDirectoryInfo.ServiceLevel);
                        }
                    }
                }
                catch (Exception ex)
                {
                    WEBeDoctorPharmacy.SS_Logger.log("!!!Exception in SS interface!");
                    WEBeDoctorPharmacy.SS_Logger.log(ex.Message);
                    WEBeDoctorPharmacy.SS_Logger.log(ex.StackTrace.ToString());
                    Response.Write("Error creating Web Request! -" + ex.Message );
                    //mMessage.UpdateStatus("Error", "Submission Unsuccessful");
                }

                finally
                {
                    if (MsgType != "DirDwnld" && MsgType != "AddPrescriber" && MsgType != "UpdatePrescriber" || MsgType == "RefillResponse")
                    {
                        //Response.Redirect("http://192.168.73.4/practice/notes/display/medication_history.asp?visit_key=" + Visit_Key + "&patient_id=" + Patient_ID + "&show=curr&MsgType=" + MsgType);
                    }

                    if (MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber")
                    {
						//throw new NotImplementedException();
                        string userid = Request.QueryString["userid"];
                        if (!(Request["serviceLevel_5"] == null || Request["serviceLevel_5"].ToString() == "N"))
                        {
                            Response.Redirect("http://192.168.73.4/admin/practice/ss_provider_admin.asp?section=ss_service_level&is_epcs=Y&userid=" + userid);
                        }
                        else
                        {
                            Response.Redirect("http://192.168.73.4/admin/practice/ss_provider_admin.asp?section=ss_service_level&is_epcs=N&userid=" + userid);
                        }
                    }

                    

                    
                }
                //Response.Write("Status: " + resp.StatusCode.ToString() + "<BR>ContentLength: " + resp.ContentLength.ToString() + "<BR>ContentType: " + resp.ContentType.ToString());
                //Receive Response.
                //byte[] buf = new byte[req.ContentLength];
                //buf = Request.BinaryRead((int)req.ContentLength);
                //string s = System.Text.Encoding.UTF8.GetString(buf);

                //StringReader sr = new StringReader(s);
                //DataSet ds = new DataSet();
                //ds.ReadXml(sr);

                //ds.WriteXml(AppDomain.CurrentDomain.BaseDirectory + "/Received/" +
                //System.DateTime.Now.ToFileTimeUtc().ToString() + ".xml");
                //}
                //else
                //{
                //    Response.Write("Error. Unable to Authenticate Request.");
                //}
            }
            else
            {
                Response.Write("Error creating the XML file!");
            }
            //}

            if (MsgType != "DirDwnld" && MsgType != "AddPrescriber" && MsgType != "UpdatePrescriber" || MsgType == "RefillResponse")
            {
                //Response.Redirect("http://192.168.73.4/practice/notes/display/medication_history.asp?visit_key=" + Visit_Key + "&patient_id=" + Patient_ID + "&show=curr&MsgType=" + MsgType);
            }

            if (MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber")
            {
                string userid = Request.QueryString["userid"];
                if (!(Request["serviceLevel_5"] == null || Request["serviceLevel_5"].ToString() == "N"))
                {
                    Response.Redirect("http://192.168.73.4/admin/practice/ss_provider_admin.asp?section=ss_service_level&is_epcs=Y&userid=" + userid);
                }
                else
                {
                    Response.Redirect("http://192.168.73.4/admin/practice/ss_provider_admin.asp?section=ss_service_level&is_epcs=N&userid=" + userid);
                }
            }
        }
        catch (Exception Ex1)
        {
            WEBeDoctorPharmacy.SS_Logger.log("!!!Exception in SS interface!!!");
            WEBeDoctorPharmacy.SS_Logger.log(Ex1.Message);
            WEBeDoctorPharmacy.SS_Logger.log(Ex1.StackTrace.ToString());
            Response.Write("Error! -" + Ex1.Message);
        }
    }

  protected WEBeDoctorPharmacy.Message CreateXMLEnvelop(bool isEPCS, string scheduleCode, string dpsNumber,string sPaymentNotes)
    {
        try
        {
			string[] benefitArgs = new string[11] { "N", null, null, null, null, null, null, null, null, null, null };
            string[] inputArgs = new string[12] { null, null, null, null, null, null, null, null, null, null, null, null };
            string FollowupFlag = "N";
            string RefillResponseContent;
            string ChangeResponseContent;

            //inputArgs[0] = PharmacyID.Text.ToString();
            //inputArgs[1] = DoctorID.Text.ToString();
            //inputArgs[2] = PatientID.Text.ToString();
            //inputArgs[3] = "304";
            //inputArgs[4] = VisitKey.Text.ToString();

            //inputArgs[0] = "0001074";
            //inputArgs[1] = "LASERTECH";
            //inputArgs[2] = "sithihou2001";
            //inputArgs[3] = "304";
            //inputArgs[4] = "2297SFarooquilenzgertrude";

            WEBeDoctorPharmacy.Message mMessage;

            string MsgType = Request["MsgType"].ToString();

            switch (MsgType)
            {
                case "NewRX":
                    //inputArgs[0] = "0001074";
                    inputArgs[0] = Request["NCPDPID"].ToString();
                    inputArgs[1] = Request["Physician_ID"].ToString();
                    inputArgs[2] = Request["Patient_ID"].ToString();
                    inputArgs[3] = Request["selected_mid"].ToString();
                    inputArgs[4] = Request["Visit_Key"].ToString();
                    inputArgs[5] = (Request["refres_messageid"] == null ? null : Request["refres_messageid"].ToString());
                    FollowupFlag = Request["Followup_Flag"].ToString();
                    inputArgs[7] = sPaymentNotes; 
					
					benefitArgs[0] = "Y";
                    benefitArgs[1] = Request["B_PayerId"].ToString();
                    benefitArgs[2] = Request["B_PayerName"].ToString();                    
                    benefitArgs[3] = Request["B_BINLocationNumber"].ToString();
					benefitArgs[4] = Request["B_ProcessorControlNumber"].ToString();
					benefitArgs[5] = Request["B_GroupId"].ToString();
					benefitArgs[6] = Request["B_GroupName"].ToString();
                    benefitArgs[7] = Request["B_CardHolderId"].ToString();
					benefitArgs[8] = Request["B_CardHolderFirstName"].ToString();
					benefitArgs[9] = Request["B_CardHolderLastName"].ToString();
					benefitArgs[10] = Request["B_MutuallyDefined"].ToString();
					
                    break;
                case "RefillResponse":
                    //inputArgs[0] = "0001074";
                    inputArgs[0] = Request["NCPDPID"].ToString();
                    inputArgs[1] = Request["Physician_ID"].ToString();
                    inputArgs[2] = Request["Patient_ID"].ToString();
                    inputArgs[3] = Request["selected_mid"].ToString();
                    inputArgs[4] = Request["Visit_Key"].ToString();
                    inputArgs[5] = Request["Response_Type"].ToString();
                    inputArgs[6] = Request["Response_Reason_Code"].ToString();
                    inputArgs[7] = Request["Response_Note"].ToString();
                    inputArgs[8] = Request["request_messageid"].ToString();
                    inputArgs[9] = Request["Refills"];
                    //inputArgs[10] = Request["Substitutions"];
                    
                    break;

				case "CancelRx":
					inputArgs[0] = Request["NCPDPID"].ToString();
					inputArgs[1] = Request["Physician_ID"].ToString();
					inputArgs[2] = Request["Patient_ID"].ToString();
					inputArgs[3] = Request["Selected_Mid"].ToString();
					inputArgs[4] = Request["Visit_Key"].ToString();
					inputArgs[5] = Request["NewRx_MessageId"].ToString();
					break;

                case "RxChangeResponse":
                    inputArgs[0] = Request["NCPDPID"].ToString();
                    inputArgs[1] = Request["Physician_ID"].ToString();
                    inputArgs[2] = Request["Patient_ID"].ToString();
                    inputArgs[3] = Request["selected_mid"].ToString();
                    inputArgs[4] = Request["Visit_Key"].ToString();
                    inputArgs[5] = Request["Response_Type"].ToString();
                    inputArgs[6] = Request["Response_Reason_Code"].ToString();
                    inputArgs[7] = Request["Response_Note"].ToString();
                    inputArgs[8] = Request["request_messageid"].ToString();
                    inputArgs[9] = Request["Refills"];
                    inputArgs[10] = Request["Pharmacy_Requested_Mid"];
                    inputArgs[11] = Request["Prior_Authorization_Number"];
                    break;

                case "DirDwnld":
                    
                    if( Request["style"] != null )
                    {
                        inputArgs[0] = Request["style"].ToString();
                    }
                    else
                    {
                        inputArgs[0] = "";
                    }
                    
                    break;

                case "AddPrescriber":
                case "UpdatePrescriber":
                    inputArgs[1] = Request["Physician_ID"].ToString();

                    int i = 0;

                    if (!(Request["serviceLevel_0"] == null || Request["serviceLevel_0"].ToString() == "N"))
                    {
                        i |= 1;
                    }

                    if (!(Request["serviceLevel_1"] == null || Request["serviceLevel_1"].ToString() == "N"))
                    {
                        i |= 2;
                    }
                    if (!(Request["serviceLevel_2"] == null || Request["serviceLevel_2"].ToString() == "N"))
                    {
                        i |= 4;
                    }
                    if (!(Request["serviceLevel_3"] == null || Request["serviceLevel_3"].ToString() == "N"))
                    {
                        i |= 8;
                    }
                    if (!(Request["serviceLevel_4"] == null || Request["serviceLevel_4"].ToString() == "N"))
                    {
                        i |= 16;
                    }
                    if (!(Request["serviceLevel_5"] == null || Request["serviceLevel_5"].ToString() == "N"))
                    {
                        i |= 2048;
                    }

                    inputArgs[2] = i.ToString();

                    break;
            }


            


            //Response.Write(MsgType+"|"+inputArgs[2].ToString() + "|" + inputArgs[3].ToString() + "|" + inputArgs[4].ToString() + "|" + inputArgs[5].ToString() + "|" + inputArgs[6].ToString() + "|" + inputArgs[7].ToString());

            //12/20/2008 Removed these line since I'm adding the NS info in the .NET type Message itself.
            //mMessage.xmlns = new XmlSerializerNamespaces();
            //mMessage.xmlns.Add("", "http://www.surescripts.com/messaging");

            //8/22/2009 kxu: Do clone/merge logic if it's a RefillResponse and RefillRequest msg file exists.
            if (MsgType == "RefillResponse" && File.Exists(@"d:\UserFiles\SSFiles\Response\" + inputArgs[8] + ".xml"))
            {
                RefillResponseContent = GetTextFromXMLFile(@"d:\UserFiles\SSFiles\Response\" + inputArgs[8] + ".xml").Replace("RefillRequest", "RefillResponse");

                WEBeDoctorPharmacy.SS_Logger.log(RefillResponseContent);

                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(RefillResponseContent);
                MemoryStream ms = new MemoryStream(bytes);
   

                // Deserializes RefillResponse XML Copied from RefillRequest
                XmlSerializer x = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetDefaultNamespace(MsgType));
                //TextWriter tw = new StreamWriter(@"d:\UserFiles\SSFiles\Request\" + 456 + ".xml");
                mMessage = (WEBeDoctorPharmacy.Message)x.Deserialize(ms);
                //x.Serialize(tw, mMessage);

                //figure out if we need approved or approvedwithChanges
                //if (inputArgs[5] != null && inputArgs[5] == "Approved")
                //{
                //    if (mMessage.msgBody.RefResp.mMedication.Refills != null
                //       && mMessage.msgBody.RefResp.mMedication.Refills.Quantity != null
                //       && mMessage.msgBody.RefResp.mMedication.Refills.Quantity != "0")
                //    {
                //        //they sent us a specific amount of refills and we are overriding it
                //        int oldRefills = Int32.Parse(mMessage.msgBody.RefResp.mMedication.Refills.Quantity);
                //        int newRefills = Int32.Parse(inputArgs[9]);

                //        if (oldRefills != newRefills - 1)
                //        {
                //            inputArgs[5] = "ApprovedWithChanges";
                //        }
                //    }
                //}


                mMessage.SynchronizePrescriberSPI();
                // Todo List:
                // 1.  Re-generate MessageID
                // 2.  Re-generate SentTime
                mMessage.SetMessageHeaderFromClone();
                // 3.  Construct Response Element
                // 4.  Update Refill Quantity, Subsitution and WrittenDate according to inputArgs values.
                mMessage.SetRefillResponseFromClone(inputArgs, isEPCS, dpsNumber);
                // 5.  Prescriber must have DEA number to send EPCS prescription.
                if (isEPCS)
                {
                    if (mMessage.msgBody.RefResp.mPrescriber.mPrescriberID == null)
                    {
                        mMessage.msgBody.RefResp.mPrescriber.mPrescriberID = new ID();
                    }

                    if (string.IsNullOrEmpty(mMessage.msgBody.RefResp.mPrescriber.mPrescriberID.DEANumber))
                    {
                        if (Request["DEANumber"] != null && !string.IsNullOrEmpty(Request["DEANumber"].ToString()))
                        {
                            mMessage.msgBody.RefResp.mPrescriber.mPrescriberID.DEANumber = Request["DEANumber"].ToString();
                        }
                        else
                        {
                            throw new Exception("Error: Prescriber must have DEA number to send EPCS prescription.");
                        }
                    }
                }
            }
            else if (MsgType == "RxChangeResponse" && File.Exists(@"d:\UserFiles\SSFiles\Response\" + inputArgs[8] + ".xml"))
            {
                ChangeResponseContent = GetTextFromXMLFile(@"d:\UserFiles\SSFiles\Response\" + inputArgs[8] + ".xml").Replace("RxChangeRequest", "RxChangeResponse");
                WEBeDoctorPharmacy.SS_Logger.log(ChangeResponseContent);
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(ChangeResponseContent);
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    // Deserializes RxChangeResponse XML
                    XmlSerializer x = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetDefaultNamespace(MsgType));
                    mMessage = (WEBeDoctorPharmacy.Message)x.Deserialize(ms);

                    mMessage.SynchronizePrescriberSPI();
                    // 1.  Re-generate MessageID
                    // 2.  Re-generate SentTime
                    mMessage.SetMessageHeaderFromClone();
                    // 4.  Update Refill Quantity and WrittenDate according to inputArgs values.
                    mMessage.SetChangeResponseFromClone(inputArgs, isEPCS, dpsNumber);
                    // 5.  Prescriber must have DEA number to send EPCS prescription.
                    if (isEPCS)
                    {
                        if (mMessage.msgBody.ChangeResp.mPrescriber.mPrescriberID == null)
                        {
                            mMessage.msgBody.ChangeResp.mPrescriber.mPrescriberID = new ID();
                        }

                        if (string.IsNullOrEmpty(mMessage.msgBody.ChangeResp.mPrescriber.mPrescriberID.DEANumber))
                        {
                            if (Request["DEANumber"] != null && !string.IsNullOrEmpty(Request["DEANumber"].ToString()))
                            {
                                mMessage.msgBody.ChangeResp.mPrescriber.mPrescriberID.DEANumber = Request["DEANumber"].ToString();
                            }
                            else
                            {
                                throw new Exception("Error: Prescriber must have DEA number to send EPCS prescription.");
                            }
                        }
                    }
                }
            }
            else
            {
                mMessage = new WEBeDoctorPharmacy.Message(MsgType, inputArgs, isEPCS, scheduleCode, dpsNumber, benefitArgs);
            }
            mMessage.ConstructHeaderFields();
            if (FollowupFlag == "Y")
            {
                MsgType = "NewRxFollowup";
            }
            mMessage.LoadCorrelatedMessageInfo(MsgType, inputArgs);

            //string relatedId = mMessage.msgHeader.RelatesToMessageID;

            //if (FollowupFlag == "Y")
            //{
            //    mMessage.msgHeader.RelatesToMessageID = null;
            //}

            XmlSerializer Y = new XmlSerializer(typeof(WEBeDoctorPharmacy.Message), WEBeDoctorPharmacy.Message.GetDefaultNamespace(MsgType));
            //XmlSerializerNamespaces NS = new XmlSerializerNamespaces();
            //NS.Add(string.Empty, "http://www.surescripts.com/messaging");
            TextWriter z = new StreamWriter(@"d:\UserFiles\SSFiles\Request\" + mMessage.msgHeader.MessageID + ".xml");
            Y.Serialize(z, mMessage);
            z.Close();

            //if (FollowupFlag == "Y")
            //{
            //    mMessage.msgHeader.RelatesToMessageID = relatedId;
            //}


            //save message
            string DrugID = mMessage.Save(mMessage.msgHeader.MessageID, sPaymentNotes);



            return mMessage;
        }
        catch(Exception ex)
        {
            Response.Write("Error: " + ex.StackTrace);
            WEBeDoctorPharmacy.SS_Logger.log("Error: " + ex.StackTrace);

            WEBeDoctorPharmacy.SS_Logger.log("Error: " + ex.Message);
            if (ex.InnerException != null)
            {
                WEBeDoctorPharmacy.SS_Logger.log("Inner Error: " + ex.InnerException.StackTrace);

                if (ex.InnerException.InnerException != null)
                {
                    WEBeDoctorPharmacy.SS_Logger.log("Inner inner Error: " + ex.InnerException.InnerException.StackTrace);
                }

            
            }

            return null;
        }
    }

  private string GetTextFromXMLFile(string file)
  {
   StreamReader reader = new StreamReader(file);
   string ret = reader.ReadToEnd();
   reader.Close();
   return ret;
  }
  private bool AuthenticateRequest()
  {
        //Create new user/credential cache for authentication.
      String TestPostData = "Test";
      byte[] bytes = System.Text.Encoding.UTF8.GetBytes(TestPostData);
      CredentialCache ssUserPWD = new CredentialCache();
      ssUserPWD.Add(new Uri("https://messaging.surescripts.net/WebeDoctor4x/AuthenticatingXmlServer.aspx"), "Basic", new NetworkCredential("webemd2$ure$cript$", "e$cripswebe23*$%3"));
      string SSURL = "https://messaging.surescripts.net/WebeDoctor4x/AuthenticatingXmlServer.aspx";
      HttpWebRequest req = (HttpWebRequest)WebRequest.Create(SSURL);
      req.Credentials = ssUserPWD;
      req.Timeout = 3000;
      req.Method = "Post";
      req.ContentType = "text/xml";
      req.ContentLength = bytes.Length;
      using (Stream requestStream = req.GetRequestStream())
      {
          requestStream.Write(bytes, 0, bytes.Length);
      }
      //Response.Write("Authenticating....");

      HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

      //Response.Write("StatusCode: " + resp.StatusCode);

      if (resp.StatusCode == HttpStatusCode.OK)
      {
          return true;
      }
      return false;
   }
  private string EncodeUIDPwd(string MsgType)
    {
        string UID;
        string PWD ;

        if (MsgType == "DirDwnld")
        {
            UID = "webedoc";
            PWD = CalculateSHA1("Cyb3rn3t!");
            WEBeDoctorPharmacy.SS_Logger.log(PWD);
        }
        else{
            UID = "WebeDoctorU$er";
            PWD = "K!TF#:I1T:4xm";
        }
        string EncodedString = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(UID+":"+PWD));

        return EncodedString;
    }
  public static string CalculateSHA1(string text)
    {
        return BitConverter.ToString(SHA1Managed.Create().ComputeHash(Encoding.Default.GetBytes(text))).Replace("-", "");
    }

    private void DownloadDirectoryFile(string FileName,string FileType)
    {
        WebClient client = new WebClient();

        WEBeDoctorPharmacy.SS_Logger.log(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName);

        string downloadFilePath = "";
        if (FileType == "full")
        {
            downloadFilePath = @"d:\UserFiles\SSFiles\DirectoryDownloadFiles\Full\" + FileName;
        }
        else
        {
            downloadFilePath = @"d:\UserFiles\SSFiles\DirectoryDownloadFiles\Nightly\" + FileName;
        }
        WEBeDoctorPharmacy.SS_Logger.log("=====" + DateTime.Now.ToString("M/dd/yyyy h:mm:ss tt") + ": Start downloading Pharmacy Directory ");
        client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
        client.DownloadFileAsync(new Uri(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName),
                               downloadFilePath, new string[2] { downloadFilePath, FileType });
        //client.DownloadFile(ConfigurationManager.AppSettings["DSDownloadURL"].ToString() + FileName, downloadFilePath);
        
    }

    private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
    {
        WEBeDoctorPharmacy.SS_Logger.log("=====" + DateTime.Now.ToString("M/dd/yyyy h:mm:ss tt") + ": Finish downloading Pharmacy Directory");
        WebClient client = sender as WebClient;
        string downloadFilePath;
        String FileType;
        try
        {
            string[] ut = (string[])e.UserState;
            downloadFilePath = ut[0];
            FileType = ut[1];
        }
        catch (Exception ex)
        {
            WEBeDoctorPharmacy.SS_Logger.log("File path error: " + (e.UserState == null ? "null" : e.UserState.ToString()));
            WEBeDoctorPharmacy.SS_Logger.log(ex.Message);
            WEBeDoctorPharmacy.SS_Logger.log(ex.StackTrace.ToString());
            return;
        }

        try
        {
            ArrayList inputFiles = WEBeDoctorPharmacy.ServiceUtils.unzipFile(downloadFilePath, @"d:\UserFiles\SSFiles\DirectoryDownloadFiles\Unzip\");
            if (inputFiles.Count > 0)                                                               
            {
                WEBeDoctorPharmacy.SS_Logger.log("=====Unzip Pharmacies Directory file: " + inputFiles[0].ToString());
                WEBeDoctorPharmacy.PharmacyService.LoadPharmaciesFromFile(inputFiles[0].ToString(), HttpRuntime.AppDomainAppPath + @"Config\MappedFields-Pharmacy.xml", FileType);
            }
            else
            {
                WEBeDoctorPharmacy.SS_Logger.log("=====Unzip Pharmacies Directory file: " + inputFiles.Count.ToString() + " file(s)");
            }
        }
        catch (Exception ex)
        {
            WEBeDoctorPharmacy.SS_Logger.log("Unhandled error when unzip and update Pharmacies: " + ex.Message);
            WEBeDoctorPharmacy.SS_Logger.log(ex.StackTrace.ToString());
            return;
        }
    }

    //EPCS: Authenticate
    private bool TwoFactorAuthenticate(string userid, string password, string otpcode)
    {
        //BEGIN: Skip this block
        if (otpcode == "123456")
        {
            return true;
        }

        if (otpcode == "654321")
        {
            return false;
        }

        if (otpcode == "111111")
        {
            throw new Exception("Exception for test only");
        }
        //END: Skip this block
		
		string responseResult = "";
		try
		{

        string urlFormat = "http://localhost:1455/TwoFactorAuthentication.svc/authenticate?userid={0}&otppwd={1}&otpcode={2}";

        string destinationURL = String.Format(urlFormat, userid, password, otpcode);
        WebRequest wrGETURL = WebRequest.Create(destinationURL);

        Stream objStream = wrGETURL.GetResponse().GetResponseStream();

        StreamReader objReader = new StreamReader(objStream);

        //Two cases:
        //  FAIL-&-Password Status: - OTP Status: 
        //  SUCCESS

        responseResult = objReader.ReadToEnd();
		WEBeDoctorPharmacy.SS_Logger.log("Two Factor Authentication Status: " + responseResult);
        //Response.Write("Full: " + responseResult + "<br/>");

        //<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">SUCCESS</string>
        //<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">FAIL-&-Password Status: - OTP Status:</string>
		
		}
		catch(Exception ex)
		{
			WEBeDoctorPharmacy.SS_Logger.log("Two Factor Authentication Error: " + ex.Message);
			return false;
		}

        string[] arrayResults = Regex.Split(responseResult, "-&amp;-");
        

        bool result = false;
        if (arrayResults.Length == 1)
        {
            if (arrayResults[0].Contains("SUCCESS"))
            {
                result = true;
            }
        }
        else if (arrayResults.Length == 2)
        {
            if (!arrayResults[0].Contains("FAIL"))
            {
                Response.Write("ERROR: Length: 2, Index 0: " + arrayResults[0] + ", Index 1: " + arrayResults[1] + "<br/>");
            }
        }
        else
        {
            Response.Write("ERROR: Length: " + arrayResults.Length);
        }

        return result;
    }

    private string CheckRequiredFieldsEPCS()
    {
        string result = "";
        // Check Prescriber Information:  Name, Phone

        // Check Patient Information: First Name, Last Name, Gender, DOB

        // Check Pharmacy Information: Name, Address, Phone

        // Check Drug Information: Drug description (Full Drug Name, Strength, Dosage form), quantity/Potency Unit Code Description,
        //                         Directions, Notes, Refills Value or PRN, Substitution or Dispensed as Written (DAW), Date Written
        return result;
    }

    private void EPCSFeaturesCalls(Message sentMessage, Message receivedMessage, string status)
    {
        //Event Type: Controlled Substance Prescription
        //Action: Order - CS NewRx, Denied - CS Refill Request, Denied with NewRx to Follow -  CS Refill Request, Approved - CS Refill Request
        //Author: session("user")
        //Role: physician
        //Detail: 
        //Result: SUCCESS/FAILED

        byte[] encryptedContent = null;
        byte[] encryptedKey = null;
        byte[] encryptedIV = null;
        byte[] signature = null;

        EPCS_Signature epcsSignature = CreateEPCSSignature();

        string epcsAuditLog_EventType = "Controlled Substance Prescription";
        string epcsAuditLog_Action = "empty";
        string epcsAuditLog_Author = "empty";
        string epcsAuditLog_Role = "empty";
        string epcsAuditLog_Detail = "empty";
        string epcsAuditLog_Result = "empty";

        if (sentMessage.msgBody.NewRq != null || sentMessage.msgBody.RefResp != null)
        {
            //EPCS Audit Log
            epcsAuditLog_Author = Request["epcsAuditAuthor"].ToString();
            epcsAuditLog_Role = Request["epcsAuditRole"].ToString();
        }

        if (sentMessage.msgBody.NewRq != null)
        {
            CoreMessage coreMessage = sentMessage.msgBody.NewRq;

            //EPCS Prescription
            if (epcsSignature != null)
            {
                string Medication_Description = coreMessage.mMedicationPrescribed.DrugDescription;
                string Medication_Quantity = coreMessage.mMedicationPrescribed.Quantity.Value;
                string Medication_RefillQuan = coreMessage.mMedicationPrescribed.Refills.Value;
                string Patient_FirstName = coreMessage.mPatient.mPatientName.FirstName;
                string Patient_LastName = coreMessage.mPatient.mPatientName.LastName;
                string Patient_Address = coreMessage.mPatient.mPatientAddress.Address1;
                string Patient_City = coreMessage.mPatient.mPatientAddress.City;
                string Patient_State = coreMessage.mPatient.mPatientAddress.State;
                string Patient_Zip = coreMessage.mPatient.mPatientAddress.Zip;
                string PrescriberAgent_FirstName = coreMessage.mPrescriber.mPrescriberName.FirstName;
                string PrescriberAgent_LastName = coreMessage.mPrescriber.mPrescriberName.LastName;
                string Prescriber_Address = coreMessage.mPrescriber.mPrescriberAddress.Address1;
                string Doctor_DEA = coreMessage.mPrescriber.mPrescriberID.DEANumber;

                string currentData = epcsSignature.CreateInput(Medication_Description, Medication_Quantity, Medication_RefillQuan,
                    Patient_FirstName, Patient_LastName, Patient_Address, Patient_City, Patient_State, Patient_Zip,
                    PrescriberAgent_FirstName, PrescriberAgent_LastName, Prescriber_Address, Doctor_DEA);

                byte[] Key, IV;

                encryptedContent = epcsSignature.EncryptBySymmetricKey(currentData, out Key, out IV);
                encryptedKey = epcsSignature.EncryptByAsymmetricKey(Key);
                encryptedIV = epcsSignature.EncryptByAsymmetricKey(IV);
                signature = epcsSignature.Sign(currentData);
            }
            else
            {
                encryptedContent = new byte[] { new byte() };
                encryptedKey = new byte[] { new byte() };
                encryptedIV = new byte[] { new byte() };
                signature = new byte[] { new byte() };
            }

            //EPCS Audit Log
            epcsAuditLog_Action = "Order - CS NewRx";
            epcsAuditLog_Detail = "- Prescriber: " + coreMessage.mPrescriber.mPrescriberName.FirstName + " " + coreMessage.mPrescriber.mPrescriberName.LastName + "<br/>" +
                                  "- Address: " + coreMessage.mPrescriber.mPrescriberAddress.Address1 + " " + coreMessage.mPrescriber.mPrescriberAddress.City + " " + coreMessage.mPrescriber.mPrescriberAddress.State + " " + coreMessage.mPrescriber.mPrescriberAddress.Zip + "<br/>" +
                                  "- Phone: " + coreMessage.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber + "<br/>" +
                                  "- DEA Number: " + coreMessage.mPrescriber.mPrescriberID.DEANumber + "<br/>" +
                                  "- Pharmacy: " + coreMessage.mPharmacy.mStoreName + "<br/>" +
                                  "- Patient: " + coreMessage.mPatient.mPatientName.FirstName + " " + coreMessage.mPatient.mPatientName.LastName + "<br/>" +
                                  "- Medication: " + coreMessage.mMedicationPrescribed.DrugDescription;

            if (status.ToUpper().Equals("OK"))
            {
                //EPCS Prescription
                receivedMessage.SaveEPCSPrescription(Request["selected_mid"].ToString(), Request["visit_key"].ToString(), Request["Physician_ID"].ToString(),
                                            Request["NCPDPID"].ToString(), Request["patient_id"].ToString(), "NEWRX--OK", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                epcsAuditLog_Result = "SUCCESS";
            }
            else if (status.ToUpper().Equals("PENDING"))
            {
                //EPCS Prescription
                receivedMessage.SaveEPCSPrescription(Request["selected_mid"].ToString(), Request["visit_key"].ToString(), Request["Physician_ID"].ToString(),
                                            Request["NCPDPID"].ToString(), Request["patient_id"].ToString(), "NEWRX--PENDING", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                //EPCS Audit Log
                epcsAuditLog_Result = "PENDING";
            }
            else if (status.ToUpper().Equals("ERROR"))
            {
                //EPCS Prescription
                receivedMessage.SaveEPCSPrescription(Request["selected_mid"].ToString(), Request["visit_key"].ToString(), Request["Physician_ID"].ToString(),
                                            Request["NCPDPID"].ToString(), Request["patient_id"].ToString(), "NEWRX--ERROR", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                //EPCS Audit Log
                epcsAuditLog_Result = "FAIL";
            }

            Message.SaveEPCSAuditLog(epcsAuditLog_EventType, epcsAuditLog_Action, epcsAuditLog_Author, epcsAuditLog_Role, epcsAuditLog_Detail, epcsAuditLog_Result);

        }
        else if (sentMessage.msgBody.RefResp != null)
        {
            CoreMessage coreMessage = sentMessage.msgBody.RefResp;

            //EPCS Prescription
            if (epcsSignature != null)
            {
                string Medication_Description = coreMessage.mMedicationPrescribed.DrugDescription;
                string Medication_Quantity = coreMessage.mMedicationPrescribed.Quantity.Value;
                string Medication_RefillQuan = coreMessage.mMedicationPrescribed.Refills.Value;
                string Patient_FirstName = coreMessage.mPatient.mPatientName.FirstName;
                string Patient_LastName = coreMessage.mPatient.mPatientName.LastName;
                string Patient_Address = coreMessage.mPatient.mPatientAddress.Address1;
                string Patient_City = coreMessage.mPatient.mPatientAddress.City;
                string Patient_State = coreMessage.mPatient.mPatientAddress.State;
                string Patient_Zip = coreMessage.mPatient.mPatientAddress.Zip;
                string PrescriberAgent_FirstName = coreMessage.mPrescriber.mPrescriberName.FirstName;
                string PrescriberAgent_LastName = coreMessage.mPrescriber.mPrescriberName.LastName;
                string Prescriber_Address = coreMessage.mPrescriber.mPrescriberAddress.Address1;
                string Doctor_DEA = coreMessage.mPrescriber.mPrescriberID.DEANumber;

                string currentData = epcsSignature.CreateInput(Medication_Description, Medication_Quantity, Medication_RefillQuan, 
                    Patient_FirstName, Patient_LastName, Patient_Address, Patient_City, Patient_State, Patient_Zip,
                    PrescriberAgent_FirstName, PrescriberAgent_LastName, Prescriber_Address, Doctor_DEA);

                byte[] Key, IV;

                encryptedContent = epcsSignature.EncryptBySymmetricKey(currentData, out Key, out IV);
                encryptedKey = epcsSignature.EncryptByAsymmetricKey(Key);
                encryptedIV = epcsSignature.EncryptByAsymmetricKey(IV);
                signature = epcsSignature.Sign(currentData);
            }
            else
            {
                encryptedContent = new byte[] { new byte() };
                encryptedKey = new byte[] { new byte() };
                encryptedIV = new byte[] { new byte() };
                signature = new byte[] { new byte() };
            }

            //EPCS Audit Log
            epcsAuditLog_Detail = "- Prescriber: " + coreMessage.mPrescriber.mPrescriberName.FirstName + " " + coreMessage.mPrescriber.mPrescriberName.LastName + "<br/>" +
                                  "- Address: " + coreMessage.mPrescriber.mPrescriberAddress.Address1 + " " + coreMessage.mPrescriber.mPrescriberAddress.City + " " + coreMessage.mPrescriber.mPrescriberAddress.State + " " + coreMessage.mPrescriber.mPrescriberAddress.Zip + "<br/>" +
                                  "- Phone: " + coreMessage.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber + "<br/>" +
                                  "- DEA Number: " + coreMessage.mPrescriber.mPrescriberID.DEANumber + "<br/>" +
                                  "- Pharmacy: " + coreMessage.mPharmacy.mStoreName + "<br/>" +
                                  "- Patient: " + coreMessage.mPatient.mPatientName.FirstName + " " + coreMessage.mPatient.mPatientName.LastName + "<br/>" +
                                  "- Medication: " + coreMessage.mMedicationPrescribed.DrugDescription;

            if (sentMessage.msgBody.RefResp.ResponseMsg.ApprovedResp != null)
            {
                epcsAuditLog_Action = "Approved - CS Refill Request";

                if (status.ToUpper().Equals("OK"))
                {
                    receivedMessage.SaveEPCSPrescription("", "", epcsAuditLog_Author,
                                            coreMessage.mPharmacy.mPharmacyID.NCPDPID, "", "APPROVED--OK", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                    epcsAuditLog_Result = "SUCCESS";
                }
                else if (status.ToUpper().Equals("PENDING"))
                {
                    receivedMessage.SaveEPCSPrescription("", "", epcsAuditLog_Author,
                                            coreMessage.mPharmacy.mPharmacyID.NCPDPID, "", "APPROVED--PENDING", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                    epcsAuditLog_Result = "PENDING";
                }
                else if (status.ToUpper().Equals("ERROR"))
                {
                    receivedMessage.SaveEPCSPrescription("", "", epcsAuditLog_Author,
                                            coreMessage.mPharmacy.mPharmacyID.NCPDPID, "", "APPROVED--ERROR", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                    epcsAuditLog_Result = "FAIL";
                }
            }
            else if (sentMessage.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp != null)
            {
                epcsAuditLog_Action = "Approved - CS Refill Request";

                if (status.ToUpper().Equals("OK"))
                {
                    receivedMessage.SaveEPCSPrescription("", "", epcsAuditLog_Author,
                                            coreMessage.mPharmacy.mPharmacyID.NCPDPID, "", "APPROVED--OK", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                    epcsAuditLog_Result = "SUCCESS";
                }
                else if (status.ToUpper().Equals("PENDING"))
                {
                    receivedMessage.SaveEPCSPrescription("", "", epcsAuditLog_Author,
                                            coreMessage.mPharmacy.mPharmacyID.NCPDPID, "", "APPROVED--PENDING", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                    epcsAuditLog_Result = "PENDING";
                }
                else if (status.ToUpper().Equals("ERROR"))
                {
                    receivedMessage.SaveEPCSPrescription("", "", epcsAuditLog_Author,
                                            coreMessage.mPharmacy.mPharmacyID.NCPDPID, "", "APPROVED--ERROR", receivedMessage.msgHeader.MessageID,
                                            encryptedContent, encryptedKey, encryptedIV, signature);

                    epcsAuditLog_Result = "FAIL";
                }
            }
            else if (sentMessage.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp != null)
            {
                epcsAuditLog_Action = "Denied with NewRx to Follow -  CS Refill Request";
                epcsAuditLog_Detail = epcsAuditLog_Detail + "<br/>- Reason of Deny: " + (string.IsNullOrEmpty(Request["Response_Note"]) ? "" : Request["Response_Note"]);

                if (status.ToUpper().Equals("OK"))
                {
                    epcsAuditLog_Result = "SUCCESS";
                }
                else if (status.ToUpper().Equals("PENDING"))
                {
                    epcsAuditLog_Result = "PENDING";
                }
                else if (status.ToUpper().Equals("ERROR"))
                {
                    epcsAuditLog_Result = "FAIL";
                }
            }
            else if (sentMessage.msgBody.RefResp.ResponseMsg.DeniedResp != null)
            {
                epcsAuditLog_Action = "Denied - CS Refill Request";
                epcsAuditLog_Detail = epcsAuditLog_Detail + "<br/>- Reason of Deny: " + (string.IsNullOrEmpty(Request["Response_Reason"]) ? "" : Request["Response_Reason"]);

                if (status.ToUpper().Equals("OK"))
                {
                    epcsAuditLog_Result = "SUCCESS";
                }
                else if (status.ToUpper().Equals("PENDING"))
                {
                    epcsAuditLog_Result = "PENDING";
                }
                else if (status.ToUpper().Equals("ERROR"))
                {
                    epcsAuditLog_Result = "FAIL";
                }
            }
            else
            {
                return;
            }

            Message.SaveEPCSAuditLog(epcsAuditLog_EventType, epcsAuditLog_Action, epcsAuditLog_Author, epcsAuditLog_Role, epcsAuditLog_Detail, epcsAuditLog_Result);
        }


    }

    private void EPCSLogicalAccessControl(string status)
    {
        string epcsAuditLog_EventType = "Logical Access Control";

        string epcsAuditLog_Action = "";
        string epcsAuditLog_Detail = "";

        if (Request["epcs_approve"] != null && Request["epcs_approve"].ToString().Equals("Y"))
        {
            if (Request["is_epcs_access_control"] != null && Request["is_epcs_access_control"].ToString().Equals("Y"))
            {
                epcsAuditLog_Action = "EPCS Access Control - First time Access";
            }
            else
            {
                epcsAuditLog_Action = "SureScripts EPCS Service Level - Approve With 2 FA";

                
                
                if (Request["epcs"] != null && Request["epcs"].ToString().Equals("Y-P"))
                {
                    epcsAuditLog_Detail = "<br/>- Status: Active - Approved";
                }
                else
                {
                    epcsAuditLog_Detail = "<br/>- Status: Inactive - Approved";
                }

                epcsAuditLog_Detail = epcsAuditLog_Detail + "<br/>" +
                                      "- Approved By: " + Request["epcsAuditDetail_ApprovedBy"].ToString() + "<br/>" +
                                      "- DEA Number: " + Request["epcsAuditDetail_ApproverDEA"].ToString();

            }
            
        }
        else if(Request["MsgType"].ToString().ToUpper().Equals("ADDPRESCRIBER"))
        {
            epcsAuditLog_Action = "SureScripts EPCS Service Level - Add Prescriber";

            epcsAuditLog_Detail = epcsAuditLog_Detail + "<br/>" +
                                  "- Added By: " + Request["epcsAuditDetail_AddedBy"].ToString() + ", Role: " + Request["epcsAuditDetail_Role"].ToString();
        }
        else if (Request["MsgType"].ToString().ToUpper().Equals("UPDATEPRESCRIBER"))
        {
            epcsAuditLog_Action = "SureScripts EPCS Service Level - Update Prescriber";
            if (Request["serviceLevel_5"] != null && Request["serviceLevel_5"].ToString().ToUpper().Equals("Y"))
            {
                epcsAuditLog_Detail = "<br/>- Action: Grant";
            }
            else
            {
                epcsAuditLog_Detail = "<br/>- Action: Revoke";
            }
            epcsAuditLog_Detail = epcsAuditLog_Detail + "<br/>" +
                                  "- Updated By: " + Request["epcsAuditDetail_UpdatedBy"].ToString() + ", Role: " + Request["epcsAuditDetail_Role"].ToString();
        }

        string epcsAuditLog_Author = Request["epcsAuditAuthor"].ToString();
        string epcsAuditLog_Role = Request["epcsAuditRole"].ToString();

        epcsAuditLog_Detail = "- Prescriber: " + Request["epcsAuditDetail_Prescriber"].ToString() + "<br/>" +
                              "- DEA Number: " + Request["epcsAuditDetail_DEA"].ToString() +
                              epcsAuditLog_Detail;

        if (!string.IsNullOrEmpty(Request["epcsAuditDetail_Practice"].ToString()))
        {
            epcsAuditLog_Detail = "- Practice: " + Request["epcsAuditDetail_Practice"].ToString() + "<br/>" +
                                  epcsAuditLog_Detail;
        }

        Message.SaveEPCSAuditLog(epcsAuditLog_EventType, epcsAuditLog_Action, epcsAuditLog_Author, epcsAuditLog_Role, epcsAuditLog_Detail, status);
    }

    private EPCS_Signature CreateEPCSSignature()
    {
        EPCS_Signature epcsSignature = null;
        try
        {
            string propertyPath = ConfigurationManager.AppSettings["OTPPropertyPath"];

            epcsSignature = new EPCS_Signature(propertyPath);
        }
        catch (Exception ex)
        {
            WEBeDoctorPharmacy.SS_Logger.log("Error in creating EPCS signature. Detail: " + ex.Message);
        }

        return epcsSignature;
    }

    #region PDR Methods

    public string CallPDR(string PostedValues)
    {
        //UtilityMethods.AppLogEntry_Pat("Post Values: " + PostValues);
        string str_Med_Id = string.Empty;
        string resXML = string.Empty;
        try
        {
            if (NotEmpty(PostedValues))
            {
                string[] sArr = PostedValues.Split('^');

                foreach (string s in sArr)
                {
                    if (s.Contains("selected_mid:"))
                    {
                        str_Med_Id = s.Replace("selected_mid:", "").Trim();
                    }
                }
            }
            else
            {
                return resXML;
            }

        }
        catch (Exception)
        {
            return resXML;
        }
                
        string docuser = string.Empty;
        DataTable PatCommAcess = new DataTable();
        
        PatCommAcess = PDRDAL.Check_PDR_Access(str_Med_Id);

        try
        {
            if ((PatCommAcess != null) && (PatCommAcess.Rows.Count > 0))
            {
                if (PatCommAcess.Rows[0]["patientcomm_access"].ToString() == "1")
                {
                    resXML = ProcessPDR(PatCommAcess);
                    return resXML;
                }
                else
                {
                    return resXML;
                }
            }
            else
            {
                return resXML;
            }
        }
        catch (Exception)
        {
            return resXML;
        }

    }

    public string  ProcessPDR(DataTable dtMedications)
    {
        DataTable dtPatInfo = new DataTable();
        string str_DrugName = string.Empty; 
        try
        {
            str_visit_Key = dtMedications.Rows[0]["visit_key"].ToString();
            str_DrugName = dtMedications.Rows[0]["drug_name"].ToString();
            //Request Attributes
            Patient_Comm_Structs.RequestAtt.source = ConfigurationManager.AppSettings["source"].ToString();
            Patient_Comm_Structs.RequestAtt.portalID = ConfigurationManager.AppSettings["portalID"].ToString();
            Patient_Comm_Structs.RequestAtt.pFormat = ConfigurationManager.AppSettings["pFormat"].ToString();
            Patient_Comm_Structs.RequestAtt.wFormat = ConfigurationManager.AppSettings["wFormat"].ToString();
            Patient_Comm_Structs.RequestAtt.output = ConfigurationManager.AppSettings["output"].ToString();

            //Physician Info
            Patient_Comm_Structs.PhysicianLoc.code = dtMedications.Rows[0]["org_code"].ToString();
            Patient_Comm_Structs.PhysicianLoc.name = dtMedications.Rows[0]["name"].ToString();
            Patient_Comm_Structs.PhysicianLoc.address = dtMedications.Rows[0]["org_address"].ToString();
            Patient_Comm_Structs.PhysicianLoc.city = dtMedications.Rows[0]["org_city"].ToString();
            Patient_Comm_Structs.PhysicianLoc.state = dtMedications.Rows[0]["org_state"].ToString();
            Patient_Comm_Structs.PhysicianLoc.zip = dtMedications.Rows[0]["org_zip"].ToString();
            Patient_Comm_Structs.PhysicianLoc.phone = dtMedications.Rows[0]["org_phone"].ToString();
            Patient_Comm_Structs.PhysicianLoc.fax = dtMedications.Rows[0]["org_fax"].ToString();

            //Patient Info
            Patient_Comm_Structs.PatientInfo.code = dtMedications.Rows[0]["pat_code"].ToString();
            Patient_Comm_Structs.PatientInfo.firstName = dtMedications.Rows[0]["patient_firstName"].ToString();
            Patient_Comm_Structs.PatientInfo.lastName = dtMedications.Rows[0]["patient_lastName"].ToString();
            Patient_Comm_Structs.PatientInfo.dob = dtMedications.Rows[0]["patient_dob"].ToString();
            if (dtMedications.Rows[0]["patient_gender"].ToString().ToLower() == "male")
              Patient_Comm_Structs.PatientInfo.gender = "M";
            else if (dtMedications.Rows[0]["patient_gender"].ToString().ToLower() == "female")
              Patient_Comm_Structs.PatientInfo.gender = "F";
            Patient_Comm_Structs.PatientInfo.address = dtMedications.Rows[0]["patient_address"].ToString();
            Patient_Comm_Structs.PatientInfo.city = dtMedications.Rows[0]["patient_city"].ToString();
            Patient_Comm_Structs.PatientInfo.state = dtMedications.Rows[0]["patient_state"].ToString();
            Patient_Comm_Structs.PatientInfo.zip = dtMedications.Rows[0]["patient_zip"].ToString();
            Patient_Comm_Structs.PatientInfo.phone = dtMedications.Rows[0]["patient_phone"].ToString();

            //Request Params
            Patient_Comm_Structs.RequestParams.rxNumber = dtMedications.Rows[0]["drug_id"].ToString();
            //Patient_Comm_Structs.RequestParams.productRxNorm = "15648";
            Patient_Comm_Structs.RequestParams.productNDC = dtMedications.Rows[0]["ndc_code"].ToString();

            //Immunizations                    
            DataTable dtImm = new DataTable();
            List<string> lstImmunizations = new List<string>();
            if (NotEmpty(str_visit_Key))
            {
                dtImm = PDRDAL.Get_Immunization_Info(str_visit_Key);
                if ((dtImm != null) && (dtImm.Rows.Count > 0))
                {
                    if (NotEmpty(dtImm.Rows[0]["cvx_code"].ToString()))
                        lstImmunizations.Add(dtImm.Rows[0]["cvx_code"].ToString());
                }
            }

            //Diagnosis
            List<List<string>> lstdiagnosis = new List<List<string>>();
            DataTable dtDiag = new DataTable();
            if (NotEmpty(str_visit_Key))
            {
                dtDiag = PDRDAL.Get_Diag_Info(str_visit_Key);
                if ((dtDiag != null) && (dtDiag.Rows.Count > 0))
                {
                    if (NotEmpty(dtDiag.Rows[0]["DIAG_CODE"].ToString()))
                    {
                        List<string> lstdiagnosis_detail = new List<string> { "codeSystem:ICD10CM", "value:" + dtDiag.Rows[0]["DIAG_CODE"].ToString(), "description:Diagnosis 1" };
                        lstdiagnosis.Add(lstdiagnosis_detail);
                    }
                }
            }

            //allergies                    
            DataTable dtAllergies = new DataTable();
            List<string> lstAllergies = new List<string>();
            if (NotEmpty(str_visit_Key))
            {
                dtAllergies = PDRDAL.Get_Allergy_Info(str_visit_Key);
                if ((dtAllergies != null) && (dtAllergies.Rows.Count > 0))
                {
                    if (NotEmpty(dtAllergies.Rows[0]["allergy_text"].ToString()))
                        lstAllergies.Add(dtAllergies.Rows[0]["allergy_text"].ToString());
                }
            }

            //Labs                    
            DataTable dtLabs = new DataTable();
            List<string> lstLabs = new List<string>();
            if (NotEmpty(str_visit_Key))
            {
                dtLabs = PDRDAL.Get_Lab_Info(str_visit_Key);
                if ((dtLabs != null) && (dtAllergies.Rows.Count > 0))
                {
                    if (NotEmpty(dtLabs.Rows[0]["lab_code"].ToString()))
                        lstLabs.Add(dtLabs.Rows[0]["lab_code"].ToString());
                }
            }

       

            //string reqXML = CreateRequestXML(Patient_Comm_Structs.RequestAtt, Patient_Comm_Structs.RequestParams, Patient_Comm_Structs.PhysicianLoc, Patient_Comm_Structs.PatientInfo, lstImmunizations, lstdiagnosis, lstLabs, lstAllergies);
            string reqXML = CreateRequestXML(Patient_Comm_Structs.RequestAtt, Patient_Comm_Structs.RequestParams, Patient_Comm_Structs.PhysicianLoc, Patient_Comm_Structs.PatientInfo, lstImmunizations, lstdiagnosis, lstLabs, lstAllergies);

            if (ConfigurationManager.AppSettings["save_request_xml"].ToString() =="true")
                File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_request_xml/" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "__" + DateTime.Now.Ticks + ".xml"), reqXML);
            string strPostData = reqXML;
            string result = PostData(strPostData);
            if (ConfigurationManager.AppSettings["save_response_xml"].ToString() == "true")
                File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_response_xml/" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "__" + DateTime.Now.Ticks + ".xml"), result);
            //Environment.Exit(0);

            //string reqXML = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_request_xml/002.xml"));
           
            //string resXML = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_response_xml/ResXMLwithPDF.xml"));

            string TransactionID = GeneratePDF(result);

            if (NotEmpty(TransactionID))
            {
                HyperLink hypLink = new HyperLink();
                string LinkPath = System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + TransactionID + "/" + TransactionID + ".pdf");
                hypLink.Text = "View and Print Physicians Desk Reference (PDR) PDF for " + str_DrugName; 
                //hypLink.NavigateUrl = "~/transactionid/" + TransactionID + "/" + TransactionID + ".pdf";
                hypLink.NavigateUrl = "javascript:showPDF(" + TransactionID + ")";               
                PanelLinks.Controls.Add(hypLink);
            }
            else
            {
                Label lbl = new Label();
                lbl.Text = "There is no content from PDR.";
                PanelLinks.Controls.Add(lbl);            
            }            

            btnNote.NavigateUrl = EnvironmentUrl + "/practice/notes/display/medication_history.asp?visit_key=" + str_visit_Key + "&patient_id=" + Patient_Comm_Structs.PatientInfo.code + "&show=curr&MsgType=NewRX";

            return result; 
        }
        catch (Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in ProcessPDR:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return string.Empty; 
        }

    }

    [System.Web.Services.WebMethod]
    public static void SendPrintConfirmation(string TrnasctionId)
    {
        
    }

    public string GetTransctionID(string xmldoc)
    {
        try
        {
            string transactionID = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmldoc);
            XmlNodeList elemList = doc.GetElementsByTagName("response");
            for (int i = 0; i < elemList.Count; i++)
            {
                transactionID = elemList[i].Attributes["transactionID"].Value;
                if (transactionID != string.Empty)
                    break;
            }

            return transactionID;
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }


    public string GetPaymentNotes(string xmldoc)
    {
        try
        {
            string PNotes = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmldoc);
            XmlNodeList elemList = doc.GetElementsByTagName("response");
            PNotes = elemList.ToString();

            XmlNodeList nl = doc.SelectNodes("response");
            XmlNode root = nl[0];
            XmlNode programs = root.ChildNodes[0];

            foreach (XmlNode xnode in programs.ChildNodes)
            {
                foreach (XmlNode node in xnode)
                {
                    string name = node.Name;
                    if (name == "paymentNotes")
                    {
                        PNotes = node.InnerText;
                        return PNotes;
                    }
                }
            }
            return string.Empty;
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }


    public string GeneratePDF(string xmldoc)
    {
        try
        {
            string transactionID = string.Empty;
            string programID = string.Empty;
            string FinalPDFPath = string.Empty;
            List<string> lstProgramIDs = new List<string>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmldoc);
            XmlNodeList elemList = doc.GetElementsByTagName("response");
            transactionID = elemList[0].Attributes["transactionID"].Value;

            //File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/patcomm_request_xml/" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Year + "__" + DateTime.Now.Ticks + ".xml"), reqXML);

            XmlNodeList nl = doc.SelectNodes("response");
            XmlNode root = nl[0];
            XmlNode programs = root.ChildNodes[0];

            foreach (XmlNode xnode in programs.ChildNodes)
            {
                programID = xnode.Attributes["id"].Value;
                lstProgramIDs.Add(programID); 

                foreach (XmlNode node in xnode)
                {
                    string name = node.Name;
                    if (name == "image")
                    {
                        string value = node.InnerText;

                        if (!(Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID))))
                            Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID));

                        File.WriteAllBytes(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID + "/" + programID + ".pdf"), Convert.FromBase64String(value));
                    }
                }
            }

            if (NotEmpty(programID))
            {
                FinalPDFPath = UtilityMethods.MergeMultiplePDF(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID + "/"), transactionID);


                //foreach (string element in lstProgramIDs)
                //{
                //    UtilityMethods.DeleteFile(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + transactionID + "/" + element + ".pdf"));    
                //}
                
                CreateConfirmationXML(transactionID, lstProgramIDs);
            }
            

            return FinalPDFPath;
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    public void CreateConfirmationXML(string TransctionId, List<string> ProgramIds)
    {
        string ConfirmXML = string.Empty;
        try
        {
            //XML root and declaration
            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(dec);            

            //confirmation
            XmlElement confirmation = doc.CreateElement("confirmation");
            confirmation.SetAttribute("transactionID", TransctionId);
            doc.AppendChild(confirmation);            

            //programs                        
            XmlElement programs = doc.CreateElement("programs");
            foreach (string element in ProgramIds)
            {
                XmlElement program = doc.CreateElement("program");
                program.SetAttribute("id", element);
                programs.AppendChild(program);
            }
            confirmation.AppendChild(programs);

            ConfirmXML = doc.OuterXml;

            File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + TransctionId + "/" + TransctionId + ".xml"), ConfirmXML);            
        }
        catch (System.Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in CreateConfirmationXML:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
        }
    }


    [System.Web.Services.WebMethod]
    public static void SendConfirmation(string value)
    {
        try
        {              
            string ConfimationXML = File.ReadAllText ( System.Web.HttpContext.Current.Server.MapPath("~/transactionid/" + value + "/" + value + ".xml"));
            string result = string.Empty;
            try
            {
                string sURL = ConfigurationManager.AppSettings["patient_comm_Web_service"].ToString();

                // prepare request
                HttpWebRequest wreq = (HttpWebRequest)WebRequest.Create(sURL);
                wreq.Method = "POST";
                wreq.ContentType = "application/x-www-form-urlencoded";
                wreq.ContentLength = ConfimationXML.Length;

                // post to server
                Stream sw = wreq.GetRequestStream();
                StreamWriter wr = new StreamWriter(sw);
                wr.Write(ConfimationXML);
                wr.Close();

                // get response
                HttpWebResponse httpResponse = (HttpWebResponse)wreq.GetResponse();
                Stream sr = httpResponse.GetResponseStream();
                StreamReader rd = new StreamReader(sr);

                result = rd.ReadToEnd();

                UtilityMethods.SavePDRPrintConfirmationResponse("TransactionID :" + value + " ---> " + result);

            }
            catch (System.Exception ex)
            {
                UtilityMethods.AppLogEntry_Pat("Error:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));               
            }
        }
        catch (Exception)
        {
            
            throw;
        }
    }

    public string PostData(string sData)
    {
        string result = string.Empty;
        try
        {
            string sURL = ConfigurationManager.AppSettings["patient_comm_Web_service"].ToString();

            // prepare request
            HttpWebRequest wreq = (HttpWebRequest)WebRequest.Create(sURL);
            wreq.Method = "POST";
            wreq.ContentType = "application/x-www-form-urlencoded";
            wreq.ContentLength = sData.Length;

            // post to server
            Stream sw = wreq.GetRequestStream();
            StreamWriter wr = new StreamWriter(sw);
            wr.Write(sData);
            wr.Close();

            // get response
            HttpWebResponse httpResponse = (HttpWebResponse)wreq.GetResponse();
            Stream sr = httpResponse.GetResponseStream();
            StreamReader rd = new StreamReader(sr);

            result = rd.ReadToEnd();

        }
        catch (System.Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return ex.Message.ToString();
        }
        return result;
    }

    public string CreateRequestXML(Patient_Comm_Structs.RequestAttribues ReqAttributes, Patient_Comm_Structs.RequestParameters ReqParams, Patient_Comm_Structs.PhysicianLocation PhysicianLoc, Patient_Comm_Structs.PatientInformation PatientInfo, List<string> lstImmunizations, List<List<string>> lstdiagnosis, List<string> lstLabs, List<string> lstAllergies)
    {
        string xmlOutput = string.Empty;
        try
        {
            //XML root and declaration
            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(dec);

            //Request Attributes
            XmlElement root = doc.CreateElement("request");
            if (!(string.IsNullOrEmpty(ReqAttributes.source))) { root.SetAttribute("source", ReqAttributes.source); }
            if (!(string.IsNullOrEmpty(ReqAttributes.portalID))) { root.SetAttribute("portalID", ReqAttributes.portalID); }
            if (!(string.IsNullOrEmpty(ReqAttributes.pFormat))) { root.SetAttribute("pFormat", ReqAttributes.pFormat); }
            if (!(string.IsNullOrEmpty(ReqAttributes.wFormat))) { root.SetAttribute("wFormat", ReqAttributes.wFormat); }
            if (!(string.IsNullOrEmpty(ReqAttributes.output))) { root.SetAttribute("output", ReqAttributes.output); }
            doc.AppendChild(root);

            //Request Parameters
            XmlElement rxNumber = doc.CreateElement("rxNumber");
            rxNumber.InnerText = ReqParams.rxNumber;
            root.AppendChild(rxNumber);

            XmlElement productRxNorm = doc.CreateElement("productRxNorm");
            productRxNorm.InnerText = ReqParams.productRxNorm;
            //root.AppendChild(productRxNorm);

            XmlElement productNDC = doc.CreateElement("productNDC");
            productNDC.InnerText = ReqParams.productNDC;
            root.AppendChild(productNDC);

            //PatientInfo
            XmlElement patient = doc.CreateElement("patient");
            //Paitent Code
            XmlElement patient_code = doc.CreateElement("code");
            patient_code.InnerText = PatientInfo.code;
            patient.AppendChild(patient_code);
            //patient_firstName
            XmlElement patient_firstName = doc.CreateElement("firstName");
            patient_firstName.InnerText = PatientInfo.firstName;
            patient.AppendChild(patient_firstName);
            //patient_lastName
            XmlElement patient_lastName = doc.CreateElement("lastName");
            patient_lastName.InnerText = PatientInfo.lastName;
            patient.AppendChild(patient_lastName);
            //patient_dob
            XmlElement patient_age = doc.CreateElement("dob");
            patient_age.InnerText = PatientInfo.dob;
            patient.AppendChild(patient_age);
            //patient_gender
            XmlElement patient_gender = doc.CreateElement("gender");
            patient_gender.InnerText = PatientInfo.gender;
            patient.AppendChild(patient_gender);
            //Paitent address
            XmlElement patient_address = doc.CreateElement("address");
            patient_address.InnerText = PatientInfo.address;
            patient.AppendChild(patient_address);
            //Paitent city
            XmlElement patient_city = doc.CreateElement("city");
            patient_city.InnerText = PatientInfo.city;
            patient.AppendChild(patient_city);
            //Paitent state
            XmlElement patient_state = doc.CreateElement("state");
            patient_state.InnerText = PatientInfo.state;
            patient.AppendChild(patient_state);
            //Paitent zip
            XmlElement patient_zip = doc.CreateElement("zip");
            patient_zip.InnerText = PatientInfo.zip;
            patient.AppendChild(patient_zip);
            //Paitent phone
            XmlElement patient_phone = doc.CreateElement("phone");
            patient_phone.InnerText = PatientInfo.phone;
            patient.AppendChild(patient_phone);

            root.AppendChild(patient);

            #region "diagnosis"
            //diagnosis
            XmlElement diagnosis = doc.CreateElement("diagnosis");
            foreach (List<string> element in lstdiagnosis)
            {
                XmlElement diagnosischild = doc.CreateElement("diagnosis");
                foreach (string arr in element)
                {
                    if (arr.ToString().Contains("codeSystem"))
                    {
                        diagnosischild.SetAttribute("codeSystem", arr.Replace("codeSystem:", ""));
                        diagnosis.AppendChild(diagnosischild);
                    }
                    else if (arr.ToString().Contains("value"))
                    {
                        diagnosischild.SetAttribute("value", arr.Replace("value:", ""));
                        diagnosis.AppendChild(diagnosischild);
                    }
                    else if (arr.ToString().Contains("description"))
                    {
                        diagnosischild.SetAttribute("description", arr.Replace("description:", ""));
                        diagnosis.AppendChild(diagnosischild);
                    }
                }

            }
            patient.AppendChild(diagnosis);
            #endregion

            #region "Immunization"
            //Labs
            XmlElement immunizations = doc.CreateElement("immunizations");
            foreach (string element in lstImmunizations)
            {
                XmlElement immunization = doc.CreateElement("immunization");
                immunization.SetAttribute("value", element);
                immunizations.AppendChild(immunization);
            }
            patient.AppendChild(immunizations);
            #endregion

            #region "labs"
            //Labs
            XmlElement labs = doc.CreateElement("labs");
            foreach (string element in lstLabs)
            {
                XmlElement lab = doc.CreateElement("lab");
                lab.SetAttribute("value", element);
                labs.AppendChild(lab);
            }
            patient.AppendChild(labs);
            #endregion

            #region "allergies"
            //Allergies
            XmlElement allergies = doc.CreateElement("allergies");
            foreach (string element in lstAllergies)
            {
                XmlElement allergy = doc.CreateElement("allergy");
                allergy.SetAttribute("value", element);
                allergies.AppendChild(allergy);
            }
            patient.AppendChild(allergies);
            #endregion

            //diagnosis
            //XmlElement diagnosis = doc.CreateElement("diagnosis");
            //foreach (List<string> element in lstdiagnosis)
            //{
            //    XmlElement diagnosischild = doc.CreateElement("diagnosis");
            //    foreach (string arr in element)
            //    {
            //        if (arr.ToString().Contains("codeSystem"))
            //        {
            //            diagnosischild.SetAttribute("codeSystem", arr.Replace("codeSystem:", ""));
            //            diagnosis.AppendChild(diagnosischild);
            //        }
            //        else if (arr.ToString().Contains("value"))
            //        {
            //            diagnosischild.SetAttribute("value", arr.Replace("value:", ""));
            //            diagnosis.AppendChild(diagnosischild);
            //        }
            //        else if (arr.ToString().Contains("description"))
            //        {
            //            diagnosischild.SetAttribute("description", arr.Replace("description:", ""));
            //            diagnosis.AppendChild(diagnosischild);
            //        }
            //    }

            //}
            //patient.AppendChild(diagnosis);

            //immunizations
            //XmlElement immunizations = doc.CreateElement("immunizations");
            //foreach (string element in lstImmunizations)
            //{
            //    XmlElement immunization = doc.CreateElement("immunization");
            //    immunization.SetAttribute("value", element);
            //    immunizations.AppendChild(immunization);
            //}
            //patient.AppendChild(immunizations);

            //Labs
            //XmlElement labs = doc.CreateElement("labs");
            //foreach (string element in lstLabs)
            //{
            //    XmlElement lab = doc.CreateElement("lab");
            //    lab.SetAttribute("value", element);
            //    labs.AppendChild(lab);
            //}
            //patient.AppendChild(labs);

            //Allergies
            //XmlElement allergies = doc.CreateElement("allergies");
            //foreach (string element in lstAllergies)
            //{
            //    XmlElement allergy = doc.CreateElement("allergy");
            //    allergy.SetAttribute("value", element);
            //    allergies.AppendChild(allergy);
            //}
            //patient.AppendChild(allergies);

            //financial
            XmlElement financial = doc.CreateElement("financial");

            //medicalBenefit
            XmlElement medicalBenefit = doc.CreateElement("medicalBenefit");
            financial.AppendChild(medicalBenefit);

            XmlElement medicalBenefit_primary = doc.CreateElement("primary");
            medicalBenefit.AppendChild(medicalBenefit_primary);

            XmlElement medicalBenefit_secondary = doc.CreateElement("secondary");
            medicalBenefit.AppendChild(medicalBenefit_secondary);

            //prescriptionBenefit
            XmlElement prescriptionBenefit = doc.CreateElement("prescriptionBenefit");
            financial.AppendChild(prescriptionBenefit);

            XmlElement prescriptionBenefit_primary = doc.CreateElement("primary");
            prescriptionBenefit.AppendChild(prescriptionBenefit_primary);

            XmlElement prescriptionBenefit_secondary = doc.CreateElement("secondary");
            prescriptionBenefit.AppendChild(prescriptionBenefit_secondary);

            //root.AppendChild(financial);


            //pharmacy
            XmlElement pharmacy = doc.CreateElement("pharmacy");
            //root.AppendChild(pharmacy);



            //prescriber
            XmlElement prescriber = doc.CreateElement("prescriber");
            // root.AppendChild(prescriber);

            //location
            XmlElement location = doc.CreateElement("location");

            XmlElement location_code = doc.CreateElement("code");
            if (ConfigurationManager.AppSettings["mode"].ToString() == "test")
                location_code.InnerText = "999999999";
            else
                location_code.InnerText = PhysicianLoc.code;
                
                location.AppendChild(location_code);

            XmlElement location_name = doc.CreateElement("name");
            location_name.InnerText = PhysicianLoc.name;
            location.AppendChild(location_name);

            XmlElement location_address = doc.CreateElement("address");
            location_address.InnerText = PhysicianLoc.address;
            location.AppendChild(location_address);

            XmlElement location_city = doc.CreateElement("city");
            location_city.InnerText = PhysicianLoc.city;
            location.AppendChild(location_city);

            XmlElement location_state = doc.CreateElement("state");
            location_state.InnerText = PhysicianLoc.state;
            location.AppendChild(location_state);

            XmlElement location_zip = doc.CreateElement("zip");
            location_zip.InnerText = PhysicianLoc.zip;
            location.AppendChild(location_zip);

            XmlElement location_phone = doc.CreateElement("phone");
            location_phone.InnerText = PhysicianLoc.phone;
            location.AppendChild(location_phone);

            XmlElement location_fax = doc.CreateElement("fax");
            location_fax.InnerText = PhysicianLoc.fax;
            location.AppendChild(location_fax);

            root.AppendChild(location);

            xmlOutput = doc.OuterXml;
        }
        catch (System.Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in CreateRequestXML:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return ex.Message.ToString();
        }
        return xmlOutput;
    }

    public string CreateDemoXML()
    {
        string xmlOutput = string.Empty;
        try
        {
            // Create the xml document containe
            XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);// Create the root element
            XmlElement root = doc.CreateElement("Library");
            doc.AppendChild(root);
            // Create Books
            // Note that to set the text inside the element,
            // you use .InnerText instead of .Value (which will throw an exception).
            // You use SetAttribute to set attribute
            XmlElement book = doc.CreateElement("Book");
            book.SetAttribute("BookType", "Hardcover");
            XmlElement title = doc.CreateElement("Title");
            title.InnerText = "Door Number Three";
            XmlElement author = doc.CreateElement("Author");
            author.InnerText = "O'Leary, Patrick";
            book.AppendChild(title);
            book.AppendChild(author);
            root.AppendChild(book);
            book = doc.CreateElement("Book");
            book.SetAttribute("BookType", "Paperback");
            title = doc.CreateElement("Title");
            title.InnerText = "Lord of Light";
            author = doc.CreateElement("Author");
            author.InnerText = "Zelanzy, Roger";
            book.AppendChild(title);
            book.AppendChild(author);
            root.AppendChild(book);
            xmlOutput = doc.OuterXml;
        }
        catch (System.Exception ex)
        {
            UtilityMethods.AppLogEntry_Pat("Error in CreateRequestXML:" + System.Environment.NewLine + UtilityMethods.BuildErrorDetails(ex));
            return ex.Message.ToString();
        }
        return xmlOutput;
    }

    public static bool NotEmpty(string Element)
    {
        if (!(string.IsNullOrEmpty(Element)))
            return true;
        else
            return false;

    }

    #endregion

    protected void btnBack_Click(object sender, EventArgs e)
    {
        string AddMedicationURL = "https://www.webedoctor2.com/practice/notes/display/medication_history.asp";

        //string strvalues = this.HiddenField1.Value.Replace("\n", "");
        //string[] sArr = strvalues.Split('&');

        //foreach (string s in sArr)
        //{
        //    if (s.Contains("visit_key"))
        //        Visit_Key = s;

        //    if (s.Contains("patient_id"))
        //        PatientId = s;
        //}

        Response.Redirect(AddMedicationURL + "?" + str_visit_Key + "&" + str_Patient_ID); 
    }
}
