CREATE OR REPLACE PACKAGE BODY MEDICAL.Wssinterface AS 
    PROCEDURE GetPatientInfo (ParamPatientID IN VARCHAR2, CurPatientInfo OUT Out_Cursor)
    IS
      PInfo OUT_CURSOR;
    BEGIN
         IF ParamPatientID IS NOT NULL THEN
             OPEN PInfo FOR
            SELECT UserID AS FileID,
                   trim(SSN) AS PatientID,
                   First_Name AS FirstName,
                   trim(Last_Name) AS LastName,
                   trim(Middle_Name) AS MiddleName,
                   trim(Prefix) AS Prefix,
                   trim(Suffix) AS Suffix,
                   SUBSTR(Gender,0,1) AS Gender,
                   Birth_Date AS DateOfBirth,
                   Street AS Address1,
                   City AS City,
                   STATE AS STATE,
                   Zip AS Zip,
                   Email AS Email,
                   Home_Phone AS Home_Phone,
                   Work_Phone AS Work_Phone,
                   Fax AS Fax,
                   Cell_Phone AS Cell_Phone,
                   Street2 AS Address2
            FROM   PATIENTTABLE
            WHERE  USERID = ParamPatientID;
         END IF;
         CurPatientInfo := PInfo;
    END GetPatientInfo;
    PROCEDURE GetPatientFromRefReq (ParamMessageId IN VARCHAR2, CurPatientInfo OUT Out_Cursor)
    IS
      PInfo OUT_CURSOR;
    BEGIN
         IF ParamMessageId IS NOT NULL THEN
             OPEN PInfo FOR
            SELECT 
                      PATIENTID,
                      PATIENTSSN,
                      PATIENTFIRSTNAME,
                      PATIENTLASTNAME,
                      PATIENTPREFIX,          
                      PATIENTGENDER,
                      PATIENTDOB,
                      PATIENTADDRESS,
                      PATIENTCITY,
                      PATIENTSTATE,
                      PATIENTZIP,
                      PATIENTEMAIL,
                      PATIENTPHONE,
                PATIENTPHONETYPE,
                PATIENTPHONE2,
                PATIENTPHONETYPE2,
                PATIENTPHONE3,
                PATIENTPHONETYPE3,
                PATIENTPHONE4,
                PATIENTPHONETYPE4,
                PATIENTPHONE5,
                PATIENTPHONETYPE5,
                PATIENTPHONE6,
                PATIENTPHONETYPE6,
                PATIENTPHONE7,
                PATIENTPHONETYPE7,
                PATIENTPHONE8,
                PATIENTPHONETYPE8,
                PATIENTMIDDLENAME
            FROM   SS_MESSAGE_CONTENT
            WHERE  messageid = paramMessageId;
         END IF;
         CurPatientInfo := PInfo;
    END GetPatientFromRefReq;
    PROCEDURE GetPharmacyInfo (ParamPharmacyID IN VARCHAR2, CurPharmacyInfo OUT Out_Cursor)
    IS
      PInfo Out_Cursor;
    BEGIN
         IF ParamPharmacyID IS NOT NULL OR ParamPharmacyID <> '' THEN
         OPEN PInfo FOR
         SELECT NCPDPID, 
                 StoreName, 
                AddressLine1 AS Address,
                City,
                STATE,
                Zip, 
                Email, 
                PhonePrimary AS PHONE,
                'TE' AS Qualifier,
                NPI,
                AddressLine2
         FROM    SS_PHARMACY                 
         WHERE    NCPDPID = ParamPharmacyID;
        END IF;
         CurPharmacyInfo := PInfo;
    END GetPharmacyInfo;
PROCEDURE GetSettledDups (ParamMessageId IN VARCHAR2, CurDups OUT Out_Cursor)
    IS
      PInfo Out_Cursor;
    BEGIN
    insert into jeff_log select sysdate, 'getting dups for ' || ParamMessageId from dual; commit;
         IF ParamMessageId IS NOT NULL THEN
  --insert into jeff_log select sysdate, 'mid: ' || parammessageid from dual;
         OPEN PInfo FOR
         SELECT count(*) theCount
         FROM    ss_message a
         WHERE
      a.messagetype = 'RefillRequest'
      and a.duplicate_key = (select c.duplicate_key from ss_message c where c.sender_messageid = ParammessageID)
      and exists (select * from ss_message b where b.relatedmessageid = a.sender_messageid and b.messagetype = 'RefillResponse');
        END IF;
         CurDups := PInfo;
    END GetSettledDups;
    PROCEDURE GetPharmacyFromRefReq (ParammessageID IN VARCHAR2, CurPharmacyInfo OUT Out_Cursor)
    IS
      PInfo Out_Cursor;
    BEGIN
         IF ParammessageID IS NOT NULL THEN
  --insert into jeff_log select sysdate, 'mid: ' || parammessageid from dual;
         OPEN PInfo FOR
         SELECT pharmacyid NCPDPID, 
                 pharmacyname StoreName, 
                pharmacyaddress AS Address,
                pharmacycity City,
                pharmacystate STATE,
                pharmacyzip Zip, 
                ' ' Email, 
                pharmacyphone AS PHONE,
                pharmacyphonetype AS Qualifier,
                pharmacyphone2 AS PHONE2,
                pharmacyphonetype2 AS Qualifier2,
                pharmacyphone3 AS PHONE3,
                pharmacyphonetype3 AS Qualifier3,
                pharmacyphone4 AS PHONE4,
                pharmacyphonetype4 AS Qualifier4,
                pharmacyphone5 AS PHONE5,
                pharmacyphonetype5 AS Qualifier5,
                pharmacyphone6 AS PHONE6,
                pharmacyphonetype6 AS Qualifier6,
                pharmacyphone7 AS PHONE7,
                pharmacyphonetype7 AS Qualifier7,
                pharmacyphone8 AS PHONE8,
                pharmacyphonetype8 AS Qualifier8
         FROM    ss_message_content                 
         WHERE    messageid = ParammessageID;
        END IF;
         CurPharmacyInfo := PInfo;
    END GetPharmacyFromRefReq;
    PROCEDURE GetPrescriberInfo (ParamPrescriberID IN VARCHAR2, CurPrescriberInfo OUT Out_Cursor)
    IS
      PRInfo Out_Cursor;
    BEGIN
         IF ParamPrescriberID IS NOT NULL THEN
            OPEN PRInfo FOR
            SELECT a.SPI AS SPI,
                   a.NPI AS NPI,
                   a.DEA_NO AS DEA,
                   b.ORGNAME AS FACILITYNAME,
                   a.LAST_NAME AS LASTNAME,
                   a.FIRST_NAME AS FIRSTNAME,
                   CASE WHEN UPPER(a.SPECIALITY) = 'GENERAL PRACTICE' THEN 'GP'
                    WHEN UPPER(a.SPECIALITY) = 'CARDIOVASCULAR SURGERY' THEN 'CTS'
                    WHEN UPPER(a.SPECIALITY) = 'OBSTETRICS' THEN 'CBS'
                    ELSE 'GP' END AS SPECIALTY,
                   COALESCE(a.STREET,b.ADDR1||' '||b.ADDR2) AS ADDRESS,
                   COALESCE(a.CITY,b.CITY) AS CITY,
                   COALESCE(a.STATE,b.STATE) AS STATE,
                   COALESCE(a.ZIP,b.ZIP) AS ZIP,
                   COALESCE(a.EMAIL,b.EMAIL) AS EMAIL,
                   COALESCE(a.PHONE,b.TEL) AS PHONE,
                   'TE' AS PHONEQUALIFIER,
            nvl(a.fax, '') as FAX,
            'FX' AS FAXQUALIFIER,
            a.STREET2 AS ADDRESS2
            FROM   DOCTORTABLE a
            JOIN   STREETREG b ON a.facility = b.streetadd
            WHERE DOCUSER = ParamPrescriberID;
         END IF;
         CurPrescriberInfo := PRInfo;
    END GetPrescriberInfo;
    PROCEDURE GetPrescriberFromRefReq (ParamMessageId IN VARCHAR2, CurPrescriberInfo OUT Out_Cursor)
    IS
      PRInfo Out_Cursor;
    BEGIN
         IF ParamMessageId IS NOT NULL THEN
            OPEN PRInfo FOR
        select
          a.prescriberid as SPI,
          a.prescribernpi as NPI,
          a.prescriberdea as DEA,
          a.prescriberclinicname as FACILITYNAME,
          a.prescriberagentlastname as lastname,
          a.prescriberagentfirstname firstname,
          a.prescriberspecialtycode AS SPECIALTY,
            a.prescriberspecialtyqual AS SPECIALTYQUAL,
            a.prescriberaddress AS ADDRESS,
            a.prescribercity AS CITY,
            a.prescriberstate AS STATE,
            a.prescriberzip AS ZIP,
            a.prescriberemail AS EMAIL,
            a.prescriberphone AS PHONE,
            a.prescriberphonetype AS PHONEQUALIFIER,
            a.prescriberphone2 as PHONE2,
            a.prescriberphonetype2 as QUAL2,
            a.prescriberphone3 as PHONE3,
            a.prescriberphonetype3 as QUAL3,
            a.prescriberphone4 as PHONE4,
            a.prescriberphonetype4 as QUAL4,
            a.prescriberphone5 as PHONE5,
            a.prescriberphonetype5 as QUAL5,
            a.prescriberphone6 as PHONE6,
            a.prescriberphonetype6 as QUAL6,
            a.prescriberphone7 as PHONE7,
            a.prescriberphonetype7 as QUAL7,
            a.prescriberphone8 as PHONE8,
            a.prescriberphonetype8 as QUAL8,
            a.PRESCRIBERAGENTMIDDLENAME as middlename
          from ss_message_content a
          where messageid = ParamMessageId;
         END IF;
         CurPrescriberInfo := PRInfo;
    END GetPrescriberFromRefReq;
  PROCEDURE SavePrescriberInfo (ParamPrescriberID IN VARCHAR2, ParamSPI IN VARCHAR2, ParamServiceLevel in VARCHAR2)
    IS
    BEGIN
         IF ParamPrescriberID IS NOT NULL THEN
        update doctortable set spi = ParamSPI, ss_service_level = ParamServiceLevel where docuser = ParamPrescriberID;
         END IF;
    END SavePrescriberInfo;
    PROCEDURE GetMedicationPrescribed (ParamPrescriptionID IN VARCHAR2, ParamVisitKey IN VARCHAR2, CurPrescription OUT Out_Cursor)
    IS
      MPInfo Out_Cursor;
    BEGIN
        IF ParamPrescriptionID IS NOT NULL THEN
           OPEN MPInfo FOR
           SELECT UPPER(a.DRUG_NAME||' '||a.FORMULATION) AS DRUGDESCRIPTION,
                     a.NDC_CODE AS PRODUCTCODE,
                  'ND' AS PRODUCTCODEQUALIFIER,
                     Get_Token(a.DISPENSE,1,' ') AS QUANTITY,
                  COALESCE(b.CODE,'C38046') AS QUANTITYQUALIFIER,
                  CASE WHEN a.ROUTE IS NOT NULL THEN a.DOSE||' '||a.FREQUENCY||'-'||a.ROUTE
                       ELSE a.DOSE||' '||a.FREQUENCY END AS DIRECTIONS,
                  a.NOTES AS NOTES,
                  CASE WHEN a.REFILL = 0 THEN 'R'
                       WHEN a.REFILL = 999 THEN 'PRN' 
                       ELSE 'R' END AS REFILL,
                  a.REFILL AS REFILLQUANTITY,
                  SUBSTITUTION AS SUBSTITUTION,
                  PRESCRIBED_DATE AS WRITTENDATE,
                  a.BRAND_GENERIC_CODE as BRANDGENERICCODE,
                  a.RXNORM_CODE as RXNORMCODE,
                  a.SYNONYM_TYPE_ID as SYNONYMTYPEID,
                  a.MEDICAL_TYPE as MEDICALTYPE
            FROM  NOTES_MEDICATIONS a
            LEFT OUTER JOIN  SS_DISPENSE b on TRIM(REGEXP_SUBSTR(a.DISPENSE,'\s.+')) = b.DESCRIPTION
            WHERE Drug_ID = ParamPrescriptionID;
        END IF;
        CurPrescription := MPInfo;
    END GetMedicationPrescribed;
    PROCEDURE GetMedFromRefReq (ParamMessageId IN VARCHAR2, ParamSubstitutionID IN VARCHAR2, CurPrescription OUT Out_Cursor)
    IS
      MPInfo Out_Cursor;
    BEGIN
        IF ParamMessageId IS NOT NULL THEN
           OPEN MPInfo FOR
          select
            medicationdescription as drugdescription,
            medicationcode as productcode,
            medicationcodetype as productcodequalifier,
            medicationquantity as quantity,
            medicationquantityqual as quantityqualifier,
            medicationdirections as directions,
            medicationnote as notes,
            medicationrefillqual as refill,
            medicationrefillquan as refillquantify,
            ParamSubstitutionID as substitution,
            medicationwrittendate as writtendate,
        medicationdosageform as dose,
        medicationstrength as str,
        medicationstrengthunits as units,
        medicationdrugdbcode as dbcode,
        medicationdrugdbcodequal as dbqual,
        medicationdayssupply as dayssupply,
        medicationlastfilldate as lastfill,
        medicationclinicalinfoqual as ciq,
        medicationprimaryqual as primqual,
        medicationprimaryvalue as primval,
        medicationsecondaryqual as secqual,
        medicationsecondaryvalue as secval
        from ss_message_content
          where messageid = parammessageid;
        END IF;
        CurPrescription := MPInfo;
    END GetMedFromRefReq;
    PROCEDURE GetSupervisorInfo (ParamSupervisorID IN VARCHAR2, CurSupervisorInfo OUT Out_Cursor)
    IS
      SPInfo Out_Cursor;
    BEGIN
         IF ParamSupervisorID IS NOT NULL THEN
             OPEN SPInfo FOR
            SELECT DOCUSER
            FROM DOCTORTABLE
            WHERE DOCUSER = ParamSupervisorID;
         END IF;
    END GetSupervisorInfo;
    PROCEDURE SaveMessage(
ParamMessageID IN VARCHAR2,ParamPRESCRIBERORDERNUMBER IN VARCHAR2,ParamRXREFERENCENUMBER IN VARCHAR2, 
ParamRelatedMessageId IN VARCHAR2, ParamPHARMACYID IN VARCHAR2,ParamPRESCRIBERID IN VARCHAR2,
ParamSUPERVISORID IN VARCHAR2,ParamDRUGID IN VARCHAR2, ParamMSGCONTENT IN VARCHAR2, ParamMESSAGETYPE IN VARCHAR2,
ParamRESPONSECODE IN VARCHAR2,ParamCHANGETYPE IN VARCHAR2,ParamPharmacyName IN VARCHAR2,ParamPharmacyAddress IN VARCHAR2,
ParamPharmacyCity IN VARCHAR2, ParamPharmacyState IN VARCHAR2, ParamPharmacyZip IN VARCHAR2,
ParamPharmacyPhone IN VARCHAR2,
ParamPharmacyPhoneType IN VARCHAR2, 
ParamPharmacyPhone2 IN VARCHAR2, 
ParamPharmacyPhoneType2 IN VARCHAR2, 
ParamPharmacyPhone3 IN VARCHAR2, 
ParamPharmacyPhoneType3 IN VARCHAR2, 
ParamPharmacyPhone4 IN VARCHAR2, 
ParamPharmacyPhoneType4 IN VARCHAR2,
ParamPharmacyPhone5 IN VARCHAR2, 
ParamPharmacyPhoneType5 IN VARCHAR2,
ParamPharmacyPhone6 IN VARCHAR2, 
ParamPharmacyPhoneType6 IN VARCHAR2,
ParamPharmacyPhone7 IN VARCHAR2, 
ParamPharmacyPhoneType7 IN VARCHAR2,
ParamPharmacyPhone8 IN VARCHAR2, 
ParamPharmacyPhoneType8 IN VARCHAR2,
paramPrescriberClinicName IN VARCHAR2, ParamPrescriberSpecialtyQual IN VARCHAR2, ParamPrescriberSpecialtyCode IN VARCHAR2,ParamPrescriberAgentLastName IN VARCHAR2, ParamPrescriberAgentFirstName IN VARCHAR2, ParamPrescriberAddress IN VARCHAR2,ParamPrescriberCity IN VARCHAR2,ParamPrescriberState IN VARCHAR2, ParamPrescriberZip IN VARCHAR2, ParamPrescriberEmail IN VARCHAR2, 
ParamPrescriberPhone IN VARCHAR2, ParamPrescriberPhoneType IN VARCHAR2,
ParamPrescriberPhone2 IN VARCHAR2, ParamPrescriberPhoneType2 IN VARCHAR2,
ParamPrescriberPhone3 IN VARCHAR2, ParamPrescriberPhoneType3 IN VARCHAR2,
ParamPrescriberPhone4 IN VARCHAR2, ParamPrescriberPhoneType4 IN VARCHAR2,
ParamPrescriberPhone5 IN VARCHAR2, ParamPrescriberPhoneType5 IN VARCHAR2,
ParamPrescriberPhone6 IN VARCHAR2, ParamPrescriberPhoneType6 IN VARCHAR2,
ParamPrescriberPhone7 IN VARCHAR2, ParamPrescriberPhoneType7 IN VARCHAR2,
ParamPrescriberPhone8 IN VARCHAR2, ParamPrescriberPhoneType8 IN VARCHAR2,
ParamPatientID IN VARCHAR2,ParamPatientSSN IN VARCHAR2,ParamPatientLastName IN VARCHAR2,ParamPatientFirstName IN VARCHAR2,ParamPatientPreFix IN VARCHAR2,ParamPatientGender IN VARCHAR2,ParamPatientDOB IN VARCHAR2,ParamPatientAddress IN VARCHAR2,ParamPatientAddress2 IN VARCHAR2,ParamPatientCity IN VARCHAR2,ParamPatientState IN VARCHAR2,ParamPatientZip IN VARCHAR2,ParamPatientEmail IN VARCHAR2,
ParamPatientPhone IN VARCHAR2,ParamPatientPhoneType IN VARCHAR2,
ParamPatientPhone2 IN VARCHAR2,ParamPatientPhoneType2 IN VARCHAR2,
ParamPatientPhone3 IN VARCHAR2,ParamPatientPhoneType3 IN VARCHAR2,
ParamPatientPhone4 IN VARCHAR2,ParamPatientPhoneType4 IN VARCHAR2,
ParamPatientPhone5 IN VARCHAR2,ParamPatientPhoneType5 IN VARCHAR2,
ParamPatientPhone6 IN VARCHAR2,ParamPatientPhoneType6 IN VARCHAR2,
ParamPatientPhone7 IN VARCHAR2,ParamPatientPhoneType7 IN VARCHAR2,
ParamPatientPhone8 IN VARCHAR2,ParamPatientPhoneType8 IN VARCHAR2,
ParamMedicationDescription IN VARCHAR2,ParamMedicationCode IN VARCHAR2,ParamMedicationCodeType IN VARCHAR2,ParamMedicationQuantityQual IN VARCHAR2,ParamMedicationQuantity IN VARCHAR2,ParamMedicationDirections IN VARCHAR2,ParamMedicationNote IN VARCHAR2,ParamMedicationRefillQual IN VARCHAR2,ParamMedicationRefillQuan IN VARCHAR2,ParamMedicationSubstitution IN VARCHAR2,ParamMedicationWrittenDate IN VARCHAR2, ParamPrescriberServiceLevel in varchar2, ParamSenderMessageID IN VARCHAR2, 
ParamNote IN VARCHAR2, ParamPatientMiddleName IN VARCHAR2, ParamPrescriberMiddleName IN VARCHAR2, 
ParamMedicationDosageForm IN VARCHAR2, ParamMedicationStrength IN VARCHAR2, ParamMedicationStrengthUnits IN VARCHAR2, ParamMedicationDrugDBCode IN VARCHAR2, ParamMedicationDrugDBCodeQual IN VARCHAR2,
ParamMedicationDaysSupply IN VARCHAR2, ParamMedicationLastFillDate IN VARCHAR2, ParamMedicationCliInfoQual IN VARCHAR2, ParamMedicationPrimaryQual IN VARCHAR2, ParamMedicationPrimaryVal IN VARCHAR2, ParamMedicationSecondaryQual IN VARCHAR2, ParamMedicationSecondaryVal IN VARCHAR2,
ParamMedDispenseDescription IN VARCHAR2,ParamMedDispenseCode IN VARCHAR2,ParamMedDispenseCodeType IN VARCHAR2,ParamMedDispenseQuantityQual IN VARCHAR2,ParamMedDispenseQuantity IN VARCHAR2,ParamMedDispenseDirections IN VARCHAR2,ParamMedDispenseNote IN VARCHAR2,ParamMedDispenseRefillQual IN VARCHAR2,ParamMedDispenseRefillQuan IN VARCHAR2,ParamMedDispenseSubstitution IN VARCHAR2,ParamMedDispenseWrittenDate IN VARCHAR2,
ParamMedDispenseDosageForm IN VARCHAR2, ParamMedDispenseStrength IN VARCHAR2, ParamMedDispenseStrengthUnits IN VARCHAR2, ParamMedDispenseDrugDBCode IN VARCHAR2, ParamMedDispenseDrugDBCodeQual IN VARCHAR2,
ParamMedDispenseDaysSupply IN VARCHAR2, ParamMedDispenseLastFillDate IN VARCHAR2, ParamMedDispenseCliInfoQual IN VARCHAR2, ParamMedDispensePrimaryQual IN VARCHAR2, ParamMedDispensePrimaryVal IN VARCHAR2, ParamMedDispenseSecondaryQual IN VARCHAR2, ParamMedDispenseSecondaryVal IN VARCHAR2,
ParamPrescriberNPI IN VARCHAR2, ParamPrescriberDEA IN VARCHAR2, ParamDEASCHEDULE IN VARCHAR2,  ParamDEASCHEDULEPRESCRIBE IN VARCHAR2,
ParamCooIncluded IN NUMBER,
ParamCooPayerMutuallydefined IN VARCHAR2, ParamCooPayerBinlocationNumber IN VARCHAR2, ParamCooPayerProcessorIdNumber IN VARCHAR2, ParamCooPayerId IN VARCHAR2, ParamCooPayerName IN VARCHAR2,
ParamCooCardholderId IN VARCHAR2, ParamCooCardholderFirstname IN VARCHAR2, ParamCooCardholderLastname IN VARCHAR2, ParamCooCardholderMiddlename IN VARCHAR2, ParamCooCardholderSuffix IN VARCHAR2, ParamCooCardholderPrefix IN VARCHAR2,
ParamCooGroupId IN VARCHAR2,
ParamMedicationDUE IN VARCHAR2,
ParamSaveFlag OUT VARCHAR2)
    IS
    Drug_ID SS_MESSAGE.Drugid%TYPE;
  v_code NUMBER;
   v_errm VARCHAR2(120);
    BEGIN
    IF ParamMESSAGETYPE = 'RefillRequest' THEN
    -- Get Correlated NewRX Message first for DrugID
     BEGIN
     SELECT DrugID INTO Drug_ID
     FROM     SS_MESSAGE join SS_MESSAGE_CONTENT on SS_MESSAGE.messageid = SS_MESSAGE_CONTENT.MESSAGEID
     WHERE  PRESCRIBERORDERNUMBER = ParamPRESCRIBERORDERNUMBER
     AND     MessageType = 'NewRX' and rownum < 2;
     EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
             SELECT DrugID INTO Drug_ID
             FROM     SS_MESSAGE join SS_MESSAGE_CONTENT on SS_MESSAGE.messageid = SS_MESSAGE_CONTENT.MESSAGEID
            WHERE upper(trim(nvl(PATIENTLASTNAME,''))) = upper(trim(nvl(ParamPatientLastName,'')))
                 and upper(trim(nvl(PATIENTFIRSTNAME,''))) = upper(trim(nvl(ParamPatientFirstName,'')))
                 and upper(trim(nvl(PATIENTGender,''))) = upper(trim(nvl(parampatientgender,'')))
                 and upper(trim(nvl(PATIENTDOB,''))) = upper(trim(nvl(parampatientdob,'')))
             and upper(trim(nvl(MEDICATIONCODE,''))) = upper(trim(nvl(ParamMedicationCode,'')))
       and messagetype = 'NewRX'
             and rownum < 2;            
            EXCEPTION    
                WHEN NO_DATA_FOUND THEN
                    ParamSaveFlag := ''; 
            END;
     END;
     INSERT INTO SS_MESSAGE(
             MessageID,
            PRESCRIBERORDERNUMBER,
            RXREFERENCENUMBER,
            PHARMACYID,
            PRESCRIBERID,
            DRUGID,
            MSGCONTENT,
            MESSAGETYPE,
            TIMESENT,
            REFRESPONSECODE,
			CHANGETYPE,
                  RelatedMessageId,
                  Sender_messageid,
                  NewRXNeeded,
                  Note)
            VALUES(
            ParamMessageID,
            ParamPRESCRIBERORDERNUMBER,
            ParamRXREFERENCENUMBER,
            ParamPHARMACYID,
            ParamPRESCRIBERID,
            DRUG_ID,
            ParamMSGCONTENT,
            ParamMESSAGETYPE,
            SYSDATE,
            ParamRESPONSECODE,
			ParamCHANGETYPE,
                  ParamRelatedMessageId,
                  ParamSenderMessageID,
                  'NO',
                  ParamNote);
        ParamSaveFlag := DRUG_ID;

	-- Change Request
    ELSIF ParamMESSAGETYPE = 'RxChangeRequest' THEN
    -- Get Correlated NewRX/RefillResponse Message first for DrugID
     BEGIN
     SELECT DrugID INTO Drug_ID
     FROM   SS_MESSAGE join SS_MESSAGE_CONTENT on SS_MESSAGE.messageid = SS_MESSAGE_CONTENT.MESSAGEID
     WHERE  (SS_MESSAGE.MESSAGEID = ParamRelatedMessageId  OR SS_MESSAGE.PRESCRIBERORDERNUMBER = ParamPRESCRIBERORDERNUMBER) AND
			(MessageType = 'NewRX' or MessageType = 'RefillResponse') and rownum < 2;
     EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
             SELECT DrugID INTO Drug_ID
             FROM     SS_MESSAGE join SS_MESSAGE_CONTENT on SS_MESSAGE.messageid = SS_MESSAGE_CONTENT.MESSAGEID
            WHERE upper(trim(nvl(PATIENTLASTNAME,''))) = upper(trim(nvl(ParamPatientLastName,'')))
                 and upper(trim(nvl(PATIENTFIRSTNAME,''))) = upper(trim(nvl(ParamPatientFirstName,'')))
                 and upper(trim(nvl(PATIENTGender,''))) = upper(trim(nvl(parampatientgender,'')))
                 and upper(trim(nvl(PATIENTDOB,''))) = upper(trim(nvl(parampatientdob,'')))
				 and upper(trim(nvl(MEDICATIONCODE,''))) = upper(trim(nvl(ParamMedicationCode,'')))
				 and (MessageType = 'NewRX' or MessageType = 'RefillResponse')
				 and rownum < 2;            
            EXCEPTION    
                WHEN NO_DATA_FOUND THEN
                    ParamSaveFlag := ''; 
            END;
     END;
     INSERT INTO SS_MESSAGE(
             MessageID,
            PRESCRIBERORDERNUMBER,
            RXREFERENCENUMBER,
            PHARMACYID,
            PRESCRIBERID,
            DRUGID,
            MSGCONTENT,
            MESSAGETYPE,
            TIMESENT,
            REFRESPONSECODE,
			CHANGETYPE,
                  RelatedMessageId,
                  Sender_messageid,
                  NewRXNeeded,
                  Note)
            VALUES(
            ParamMessageID,
            ParamPRESCRIBERORDERNUMBER,
            ParamRXREFERENCENUMBER,
            ParamPHARMACYID,
            ParamPRESCRIBERID,
            DRUG_ID,
            ParamMSGCONTENT,
            ParamMESSAGETYPE,
            SYSDATE,
            ParamRESPONSECODE,
			ParamCHANGETYPE,
                  ParamRelatedMessageId,
                  ParamSenderMessageID,
                  'NO',
                  ParamNote);
        ParamSaveFlag := DRUG_ID;

	-- Cancel Response
	ELSIF ParamMESSAGETYPE = 'CancelRxResponse' THEN
		BEGIN
			SELECT DrugID INTO Drug_ID
			FROM SS_MESSAGE
			WHERE (MESSAGEID = ParamRelatedMessageId  OR PRESCRIBERORDERNUMBER = ParamPRESCRIBERORDERNUMBER)
				  AND MessageType = 'CancelRx' AND rownum < 2;
		EXCEPTION
            WHEN NO_DATA_FOUND THEN
                ParamSaveFlag := '';
		END;

		INSERT INTO SS_MESSAGE(
            MessageID,
            PRESCRIBERORDERNUMBER,
            RXREFERENCENUMBER,
            PHARMACYID,
            PRESCRIBERID,
            DRUGID,
            MSGCONTENT,
            MESSAGETYPE,
            TIMESENT,
            REFRESPONSECODE,
			CHANGETYPE,
            RelatedMessageId,
            Sender_messageid,
            NewRXNeeded,
            Note,
			CANCELCODE,
			CANCELDESC)
        VALUES(
            ParamMessageID,
            ParamPRESCRIBERORDERNUMBER,
            ParamRXREFERENCENUMBER,
            ParamPHARMACYID,
            ParamPRESCRIBERID,
            DRUG_ID, -- Loaded from CancelRx
            ParamMSGCONTENT,
            ParamMESSAGETYPE,
            SYSDATE,
            '',
			'',
            ParamRelatedMessageId,
            ParamSenderMessageID,
            'NO',
            ParamNote,
			ParamRESPONSECODE,
			ParamNote);

        UPDATE SS_MESSAGE SET
            CANCELCODE = ParamRESPONSECODE,
            CANCELDESC = ParamNote
        WHERE MESSAGEID = ParamRelatedMessageId AND MessageType = 'CancelRx';

		ParamSaveFlag := DRUG_ID;
    ELSE
          INSERT INTO SS_MESSAGE(
             MessageID,
            PRESCRIBERORDERNUMBER,
            RXREFERENCENUMBER,
            PHARMACYID,
            PRESCRIBERID,
            DRUGID,
            MSGCONTENT,
            MESSAGETYPE,
            TIMESENT,
            REFRESPONSECODE,
			CHANGETYPE,
      RelatedMessageId,
      sender_messageid,
      NewRXNeeded,
      Note)
            VALUES(
            ParamMessageID,
            ParamPRESCRIBERORDERNUMBER,
            ParamRXREFERENCENUMBER,
            ParamPHARMACYID,
            ParamPRESCRIBERID,
            ParamDRUGID,
            ParamMSGCONTENT,
            ParamMESSAGETYPE,
            SYSDATE,
            ParamRESPONSECODE,
			ParamCHANGETYPE,
      ParamRelatedMessageId,
      ParamSenderMessageID,
            case when ParamResponseCode = 'DeniedNewPrescriptionToFollow' then 'YES' else 'NO' end,
            ParamNote);
        ParamSaveFlag := '';
    END IF;
  IF ParamMESSAGETYPE = 'NewRX' and not ParamRelatedMessageId is null THEN
    update ss_message set newrxneeded = 'NO' where messageid = ParamRelatedMessageId;
  end if;
  begin 
    --ParamSaveFlag := 'O';
    --3/14/2009 KXU Added new table to hold msg content for scenario where patient does not exist in system
    INSERT INTO SS_MESSAGE_CONTENT(
            MESSAGEID, 
            PHARMACYID, 
            PHARMACYNAME, 
            PHARMACYADDRESS, 
            PHARMACYCITY, 
            PHARMACYSTATE, 
            PHARMACYZIP, 
            PHARMACYPHONE, 
            PHARMACYPHONETYPE,
            PHARMACYPHONE2,
            PHARMACYPHONETYPE2, 
            PHARMACYPHONE3,
            PHARMACYPHONETYPE3,
            PHARMACYPHONE4,
            PHARMACYPHONETYPE4,
            PHARMACYPHONE5,
            PHARMACYPHONETYPE5,
            PHARMACYPHONE6,
            PHARMACYPHONETYPE6,
            PHARMACYPHONE7,
            PHARMACYPHONETYPE7,
            PHARMACYPHONE8,
            PHARMACYPHONETYPE8,
            PRESCRIBERCLINICNAME, 
            PRESCRIBERID, 
            PRESCRIBERSPECIALTYQUAL, 
            PRESCRIBERSPECIALTYCODE, 
            PRESCRIBERAGENTLASTNAME, 
            PRESCRIBERAGENTFIRSTNAME, 
            PRESCRIBERAGENTMIDDLENAME,
            PRESCRIBERADDRESS, 
            PRESCRIBERCITY, 
            PRESCRIBERSTATE, 
            PRESCRIBERZIP, 
            PRESCRIBEREMAIL, 
            PRESCRIBERPHONETYPE, 
            PRESCRIBERPHONETYPE2,
            PRESCRIBERPHONETYPE3,
            PRESCRIBERPHONETYPE4,
            PRESCRIBERPHONETYPE5,
            PRESCRIBERPHONETYPE6,
            PRESCRIBERPHONETYPE7,
            PRESCRIBERPHONETYPE8,
            PATIENTID, 
            PATIENTSSN, 
            PATIENTLASTNAME, 
            PATIENTFIRSTNAME, 
            PATIENTMIDDLENAME,
            PATIENTPREFIX, 
            PATIENTGENDER, 
            PATIENTDOB, 
            PATIENTADDRESS,
            PATIENTADDRESS2, 
            PATIENTCITY, 
            PATIENTSTATE, 
            PATIENTZIP, 
            PATIENTEMAIL, 
            PATIENTPHONE, 
            PATIENTPHONETYPE, 
            PATIENTPHONE2, 
            PATIENTPHONETYPE2,
            PATIENTPHONE3, 
            PATIENTPHONETYPE3,
            PATIENTPHONE4, 
            PATIENTPHONETYPE4,
            PATIENTPHONE5, 
            PATIENTPHONETYPE5,
            PATIENTPHONE6, 
            PATIENTPHONETYPE6,
            PATIENTPHONE7, 
            PATIENTPHONETYPE7,
            PATIENTPHONE8, 
            PATIENTPHONETYPE8,
            MEDICATIONDESCRIPTION, 
            MEDICATIONCODE, 
            MEDICATIONCODETYPE, 
            MEDICATIONQUANTITYQUAL, 
            MEDICATIONQUANTITY, 
            MEDICATIONDIRECTIONS, 
            MEDICATIONNOTE, 
            MEDICATIONREFILLQUAL, 
            MEDICATIONREFILLQUAN, 
            MEDICATIONSUBSTITUTION, 
            MEDICATIONWRITTENDATE, 
            PRESCRIBERPHONE,
            PRESCRIBERPHONE2,
            PRESCRIBERPHONE3,
            PRESCRIBERPHONE4,
            PRESCRIBERPHONE5,
            PRESCRIBERPHONE6,
            PRESCRIBERPHONE7,
            PRESCRIBERPHONE8,
            DATECREATED,
            MEDICATIONDOSAGEFORM,
            MEDICATIONSTRENGTH,
            MEDICATIONSTRENGTHUNITS,
            MEDICATIONDRUGDBCODE,
            MEDICATIONDRUGDBCODEQUAL,
            MEDICATIONDAYSSUPPLY,
            MEDICATIONLASTFILLDATE,
            MEDICATIONCLINICALINFOQUAL,
            MEDICATIONPRIMARYQUAL,
            MEDICATIONPRIMARYVALUE,
            MEDICATIONSECONDARYQUAL,
            MEDICATIONSECONDARYVALUE,
            MEDDISPENSEDESCRIPTION, 
            MEDDISPENSECODE, 
            MEDDISPENSECODETYPE, 
            MEDDISPENSEQUANTITYQUAL, 
            MEDDISPENSEQUANTITY, 
            MEDDISPENSEDIRECTIONS, 
            MEDDISPENSENOTE, 
            MEDDISPENSEREFILLQUAL, 
            MEDDISPENSEREFILLQUAN, 
            MEDDISPENSESUBSTITUTION, 
            MEDDISPENSEWRITTENDATE,
            MEDDISPENSEDOSAGEFORM,
            MEDDISPENSESTRENGTH,
            MEDDISPENSESTRENGTHUNITS,
            MEDDISPENSEDRUGDBCODE,
            MEDDISPENSEDRUGDBCODEQUAL,
            MEDDISPENSEDAYSSUPPLY,
            MEDDISPENSELASTFILLDATE,
            MEDDISPENSECLINICALINFOQUAL,
            MEDDISPENSEPRIMARYQUAL,
            MEDDISPENSEPRIMARYVALUE,
            MEDDISPENSESECONDARYQUAL,
            MEDDISPENSESECONDARYVALUE,
            PRESCRIBERNPI,
            PRESCRIBERDEA,
            DEASCHEDULE,
            DEASCHEDULEPRESCRIBE,
			PRESCRIBERSERVICELEVEL,
			COOINCLUDED,
			COOPAYERMUTUALLYDEFINED,
			COOPAYERBINLOCATIONNUMBER,
			COOPAYERPROCESSORIDNUMBER,
			COOPAYERID,
			COOPAYERNAME,
			COOCARDHOLDERID,
			COOCARDHOLDERFIRSTNAME,
			COOCARDHOLDERLASTNAME,
			COOCARDHOLDERMIDDLENAME,
			COOCARDHOLDERSUFFIX,
			COOCARDHOLDERPREFIX,
			COOGROUPID,
			MEDICATIONDUE
            )
            VALUES(
            ParamMESSAGEID, 
            ParamPHARMACYID, 
            ParamPHARMACYNAME, 
            ParamPHARMACYADDRESS, 
            ParamPHARMACYCITY, 
            ParamPHARMACYSTATE, 
            ParamPHARMACYZIP, 
            ParamPHARMACYPHONE, 
            ParamPHARMACYPHONETYPE,
            ParamPHARMACYPHONE2,
            ParamPHARMACYPHONETYPE2, 
            ParamPHARMACYPHONE3,
            ParamPHARMACYPHONETYPE3, 
            ParamPHARMACYPHONE4,
            ParamPHARMACYPHONETYPE4,
            ParamPHARMACYPHONE5,
            ParamPHARMACYPHONETYPE5,
            ParamPHARMACYPHONE6,
            ParamPHARMACYPHONETYPE6,
            ParamPHARMACYPHONE7,
            ParamPHARMACYPHONETYPE7,
            ParamPHARMACYPHONE8,
            ParamPHARMACYPHONETYPE8,
            ParamPRESCRIBERCLINICNAME, 
            ParamPRESCRIBERID, 
            ParamPRESCRIBERSPECIALTYQUAL, 
            ParamPRESCRIBERSPECIALTYCODE, 
            ParamPRESCRIBERAGENTLASTNAME, 
            ParamPRESCRIBERAGENTFIRSTNAME,
            ParamPRESCRIBERMIDDLENAME, 
            ParamPRESCRIBERADDRESS, 
            ParamPRESCRIBERCITY, 
            ParamPRESCRIBERSTATE, 
            ParamPRESCRIBERZIP, 
            ParamPRESCRIBEREMAIL, 
            ParamPRESCRIBERPHONETYPE, 
            ParamPRESCRIBERPHONETYPE2,
            ParamPRESCRIBERPHONETYPE3,
            ParamPRESCRIBERPHONETYPE4,
            ParamPRESCRIBERPHONETYPE5,
            ParamPRESCRIBERPHONETYPE6,
            ParamPRESCRIBERPHONETYPE7,
            ParamPRESCRIBERPHONETYPE8,
            ParamPATIENTID, 
            ParamPATIENTSSN, 
            ParamPATIENTLASTNAME, 
            ParamPATIENTFIRSTNAME,
            ParamPATIENTMIDDLENAME, 
            ParamPATIENTPREFIX, 
            ParamPATIENTGENDER, 
            ParamPATIENTDOB, 
            ParamPATIENTADDRESS, 
            ParamPATIENTADDRESS2,
            ParamPATIENTCITY, 
            ParamPATIENTSTATE, 
            ParamPATIENTZIP, 
            ParamPATIENTEMAIL, 
            ParamPATIENTPHONE, 
            ParamPATIENTPHONETYPE, 
            ParamPATIENTPHONE2, 
            ParamPATIENTPHONETYPE2,
            ParamPATIENTPHONE3, 
            ParamPATIENTPHONETYPE3,
            ParamPATIENTPHONE4, 
            ParamPATIENTPHONETYPE4,
            ParamPATIENTPHONE5, 
            ParamPATIENTPHONETYPE5,
            ParamPATIENTPHONE6, 
            ParamPATIENTPHONETYPE6,
            ParamPATIENTPHONE7, 
            ParamPATIENTPHONETYPE7,
            ParamPATIENTPHONE8, 
            ParamPATIENTPHONETYPE8,
            ParamMEDICATIONDESCRIPTION, 
            ParamMEDICATIONCODE, 
            ParamMEDICATIONCODETYPE, 
            ParamMEDICATIONQUANTITYQUAL, 
            ParamMEDICATIONQUANTITY, 
            ParamMEDICATIONDIRECTIONS, 
            ParamMEDICATIONNOTE, 
            ParamMEDICATIONREFILLQUAL, 
            ParamMEDICATIONREFILLQUAN, 
            ParamMEDICATIONSUBSTITUTION, 
            ParamMEDICATIONWRITTENDATE, 
            ParamPRESCRIBERPHONE,
            ParamPRESCRIBERPHONE2,
            ParamPRESCRIBERPHONE3,
            ParamPRESCRIBERPHONE4,
            ParamPRESCRIBERPHONE5,
            ParamPRESCRIBERPHONE6,
            ParamPRESCRIBERPHONE7,
            ParamPRESCRIBERPHONE8,
            SYSDATE,
            ParamMEDICATIONDOSAGEFORM,
            ParamMEDICATIONSTRENGTH,
            ParamMEDICATIONSTRENGTHUNITS,
            ParamMEDICATIONDRUGDBCODE,
            ParamMEDICATIONDRUGDBCODEQUAL,
            ParamMEDICATIONDAYSSUPPLY,
            ParamMEDICATIONLASTFILLDATE,
            ParamMEDICATIONCLIINFOQUAL,
            ParamMEDICATIONPRIMARYQUAL,
            ParamMEDICATIONPRIMARYVAL,
            ParamMEDICATIONSECONDARYQUAL,
            ParamMEDICATIONSECONDARYVAL,
            ParamMEDDISPENSEDESCRIPTION, 
            ParamMEDDISPENSECODE, 
            ParamMEDDISPENSECODETYPE, 
            ParamMEDDISPENSEQUANTITYQUAL, 
            ParamMEDDISPENSEQUANTITY, 
            ParamMEDDISPENSEDIRECTIONS, 
            ParamMEDDISPENSENOTE, 
            ParamMEDDISPENSEREFILLQUAL, 
            ParamMEDDISPENSEREFILLQUAN, 
            ParamMEDDISPENSESUBSTITUTION, 
            ParamMEDDISPENSEWRITTENDATE,
            ParamMEDDISPENSEDOSAGEFORM,
            ParamMEDDISPENSESTRENGTH,
            ParamMEDDISPENSESTRENGTHUNITS,
            ParamMEDDISPENSEDRUGDBCODE,
            ParamMEDDISPENSEDRUGDBCODEQUAL,
            ParamMEDDISPENSEDAYSSUPPLY,
            ParamMEDDISPENSELASTFILLDATE,
            ParamMEDDISPENSECLIINFOQUAL,
            ParamMEDDISPENSEPRIMARYQUAL,
            ParamMEDDISPENSEPRIMARYVAL,
            ParamMEDDISPENSESECONDARYQUAL,
            ParamMEDDISPENSESECONDARYVAL,
            ParamPRESCRIBERNPI,
            ParamPRESCRIBERDEA,
            ParamDEASCHEDULE,
            ParamDEASCHEDULEPRESCRIBE,
			ParamPrescriberServiceLevel,
			ParamCooIncluded,
			ParamCooPayerMutuallydefined,
			ParamCooPayerBinlocationNumber,
			ParamCooPayerProcessorIdNumber,
			ParamCooPayerId,
			ParamCooPayerName,
			ParamCooCardholderId,
			ParamCooCardholderFirstname,
			ParamCooCardholderLastname,
			ParamCooCardholderMiddlename,
			ParamCooCardholderSuffix,
			ParamCooCardholderPrefix,
			ParamCooGroupId,
			ParamMedicationDUE
            );
    EXCEPTION
      when OTHERS then
        v_code := SQLCODE;
        v_errm := substr(SQLERRM, 1,120);   
        rollback;
        insert into jeff_log select sysdate, v_code || ' ' || v_errm from dual; commit;
      raise;
  end;
    END SaveMessage;
    PROCEDURE SaveSurescriptsMedication(
		ParamID IN VARCHAR2,
        ParamMessageID IN VARCHAR2,
        ParamMedicationType IN VARCHAR2,
        ParamSSMedDescription IN VARCHAR2,
        ParamSSMedCode IN VARCHAR2,
        ParamSSMedCodeType IN VARCHAR2,
        ParamSSMedQuantityQual IN VARCHAR2,
        ParamSSMedQuantity IN VARCHAR2,
        ParamSSMedDirections IN VARCHAR2,
        ParamSSMedNote IN VARCHAR2,
        ParamSSMedRefillQual IN VARCHAR2,
        ParamSSMedRefillQuan IN VARCHAR2,
        ParamSSMedSubstitution IN VARCHAR2,
        ParamSSMedWrittenDate IN VARCHAR2,
        ParamSSMedDosageForm IN VARCHAR2,
        ParamSSMedStrength IN VARCHAR2,
        ParamSSMedStrengthUnits IN VARCHAR2,
        ParamSSMedDrugDBCode IN VARCHAR2,
        ParamSSMedDrugDBCodeQual IN VARCHAR2,
        ParamSSMedDaysSupply IN VARCHAR2,
        ParamSSMedLastFillDate IN VARCHAR2,
        ParamSSMedCliInfoQual IN VARCHAR2,
        ParamSSMedPrimaryQual IN VARCHAR2,
        ParamSSMedPrimaryVal IN VARCHAR2,
        ParamSSMedSecondaryQual IN VARCHAR2,
        ParamSSMedSecondaryVal IN VARCHAR2,
		ParamSSMedRawContent IN VARCHAR2)
    IS
    v_code NUMBER;
    v_errm VARCHAR2(120);
    BEGIN
        INSERT INTO SS_MESSAGE_MEDICATION(
			ID,
            MESSAGEID,
            MEDICATIONTYPE,
            MEDICATIONDESCRIPTION,
            MEDICATIONCODE,
            MEDICATIONCODETYPE,
            MEDICATIONQUANTITYQUAL,
            MEDICATIONQUANTITY,
            MEDICATIONDIRECTIONS,
            MEDICATIONNOTE,
            MEDICATIONREFILLQUAL,
            MEDICATIONREFILLQUAN,
            MEDICATIONSUBSTITUTION,
            MEDICATIONWRITTENDATE,
            MEDICATIONDOSAGEFORM,
            MEDICATIONSTRENGTH,
            MEDICATIONSTRENGTHUNITS,
            MEDICATIONDRUGDBCODE,
            MEDICATIONDRUGDBCODEQUAL,
            MEDICATIONDAYSSUPPLY,
			MEDICATIONLASTFILLDATE,
            MEDICATIONCLINICALINFOQUAL,
            MEDICATIONPRIMARYQUAL,
            MEDICATIONPRIMARYVALUE,
            MEDICATIONSECONDARYQUAL,
            MEDICATIONSECONDARYVALUE,
			RAWCONTENT)
        VALUES(
			ParamID,
            ParamMessageID,
            ParamMedicationType,
            ParamSSMedDescription,
            ParamSSMedCode,
            ParamSSMedCodeType,
            ParamSSMedQuantityQual,
            ParamSSMedQuantity,
            ParamSSMedDirections,
            ParamSSMedNote,
            ParamSSMedRefillQual,
            ParamSSMedRefillQuan,
            ParamSSMedSubstitution,
            ParamSSMedWrittenDate,
            ParamSSMedDosageForm,
            ParamSSMedStrength,
            ParamSSMedStrengthUnits,
            ParamSSMedDrugDBCode,
            ParamSSMedDrugDBCodeQual,
            ParamSSMedDaysSupply,
            ParamSSMedLastFillDate,
            ParamSSMedCliInfoQual,
            ParamSSMedPrimaryQual,
            ParamSSMedPrimaryVal,
            ParamSSMedSecondaryQual,
            ParamSSMedSecondaryVal,
			ParamSSMedRawContent);

    EXCEPTION
      when OTHERS then
        v_code := SQLCODE;
        v_errm := substr(SQLERRM, 1,120);   
        rollback;
        insert into jeff_log select sysdate, v_code || ' ' || v_errm from dual; commit;
      raise;
    END SaveSurescriptsMedication;
    PROCEDURE UpdateStatus(ParamMessageID IN VARCHAR2, ParamStatusCode IN VARCHAR2, ParamStatusDesc IN VARCHAR2)
    IS    
    BEGIN
    UPDATE SS_MESSAGE
    SET    Status = ParamStatusCode,
        StatusDesc = ParamStatusDesc
    WHERE    MessageID = ParamMessageID;
    END UpdateStatus;
    PROCEDURE UpdateCancelStatus(ParamCancelReqID IN VARCHAR2, ParamStatusCode IN VARCHAR2, ParamStatusDesc IN VARCHAR2)
    IS    
    BEGIN
    UPDATE SS_MESSAGE
    SET CANCELCODE = ParamStatusCode,
        CANCELDESC = ParamStatusDesc
    WHERE MessageID = ParamCancelReqID;
    END UpdateCancelStatus;
    PROCEDURE UpdateUnreadStatus(ParamMessageID IN VARCHAR2, ParamIsUnread IN CHAR)
    IS    
    BEGIN
    UPDATE SS_MESSAGE
    SET    Is_Unread = ParamIsUnread
    WHERE    MessageID = ParamMessageID;
    END UpdateUnreadStatus;
    PROCEDURE LoadCorrelatedMessageInfo(ParamMsgType IN VARCHAR2, ParamDrugID IN VARCHAR2, CurCorrelatedMessageInfo OUT Out_Cursor)
    IS
      MInfo Out_Cursor;
    BEGIN
    --IF ParamMsgType IS NOT NULL AND ParamDrugID <> '' THEN
    OPEN MInfo FOR
        SELECT     COALESCE(RXREFERENCENUMBER,'') AS RXREFERENCENUMBER,
            COALESCE(PRESCRIBERORDERNUMBER,'') AS PRESCRIBERORDERNUMBER,
            COALESCE(SENDER_MESSAGEID,'') AS SENDER_MESSAGEID
        FROM    SS_MESSAGE
        WHERE    DRUGID = ParamDrugID AND
            MESSAGETYPE = ParamMsgType;
    --END IF;    
    CurCorrelatedMessageInfo := MInfo;
    END LoadCorrelatedMessageInfo;
    PROCEDURE LoadCorrelatedMessage(ParamMessageID IN VARCHAR2, CurCorrelatedMessageInfo OUT Out_Cursor)
    IS
      MInfo Out_Cursor;
    BEGIN
    --IF ParamMsgType IS NOT NULL AND ParamDrugID <> '' THEN
    OPEN MInfo FOR
        SELECT     COALESCE(a.RXREFERENCENUMBER,'') AS RXREFERENCENUMBER,
            COALESCE(c.PRESCRIBERORDERNUMBER,'') AS PRESCRIBERORDERNUMBER,
            COALESCE(a.SENDER_MESSAGEID,'') AS SENDER_MESSAGEID,
      COALESCE(b.MEDICATIONSUBSTITUTION,'') AS SUBSTITUTION
        FROM SS_MESSAGE a
      left join ss_message c on c.messagetype = 'NewRX' and c.prescriberordernumber = a.prescriberordernumber, ss_message_content b
        WHERE    a.messageid = parammessageid and b.messageid = a.messageid;
    --END IF;    
    CurCorrelatedMessageInfo := MInfo;
    END LoadCorrelatedMessage;
    PROCEDURE LoadCorrelatedNewRxParams(ParamDrugID IN VARCHAR2, ParamPRESCRIBERORDERNUMBER IN VARCHAR2, CurNewRxParams OUT Out_Cursor)
    IS
      NInfo Out_Cursor;
    BEGIN
    OPEN NInfo FOR
        SELECT     b.PHARMACYID AS PHARMACY_ID,
            a.PRESCRIBED_BY AS PRESCRIBER_ID,
            a.PATIENT_ID AS PATIENT_ID,
            b.DRUGID AS DRUG_ID,
            a.VISIT_KEY AS VISIT_KEY                    
        FROM     NOTES_MEDICATIONS a
        JOIN    SS_MESSAGE b ON a.drug_id = b.DRUGID
        WHERE a.Drug_ID = ParamDrugID AND b.PRESCRIBERORDERNUMBER = ParamPRESCRIBERORDERNUMBER AND b.MESSAGETYPE = 'NewRX';
    CurNewRxParams := NInfo;
    END LoadCorrelatedNewRxParams;
    PROCEDURE CheckForControlledSubstance(ParamNDCCode IN VARCHAR2, ParamCSASCHEDULE OUT VARCHAR2)
    IS
    BEGIN
           SELECT CSA_SCHEDULE INTO ParamCSASCHEDULE
        FROM    NDC_DRUGS
        WHERE     NDC_CODE = ParamNDCCode;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            ParamCSASCHEDULE := '0';     
    END CheckForControlledSubstance;
    PROCEDURE SavePharmacy (ParamNCPDPID IN VARCHAR2, 
        ParamStoreNumber IN VARCHAR2, 
        ParamStoreName IN VARCHAR2, 
        ParamAddressLine1 IN VARCHAR2, 
        ParamAddressLine2 IN VARCHAR2, 
        ParamCity IN VARCHAR2, 
        ParamState IN VARCHAR2, 
        ParamZip IN VARCHAR2, 
        ParamPhonePrimary IN VARCHAR2, 
        ParamFax IN VARCHAR2, 
        ParamEmail IN VARCHAR2, 
        ParamPhoneAlt1 IN VARCHAR2, 
        ParamPhoneAlt1Qualifier IN VARCHAR2, 
        ParamPhoneAlt2 IN VARCHAR2, 
        ParamPhoneAlt2Qualifier IN VARCHAR2, 
        ParamPhoneAlt3 IN VARCHAR2, 
        ParamPhoneAlt3Qualifier IN VARCHAR2, 
        ParamPhoneAlt4 IN VARCHAR2, 
        ParamPhoneAlt4Qualifier IN VARCHAR2, 
        ParamPhoneAlt5 IN VARCHAR2, 
        ParamPhoneAlt5Qualifier IN VARCHAR2, 
        ParamActiveStartTime IN VARCHAR2, 
        ParamActiveEndTime IN VARCHAR2, 
        ParamServiceLevel IN VARCHAR2, 
        ParamPartnerAccount IN VARCHAR2, 
        ParamLastModifiedDate IN VARCHAR2, 
        ParamCrossStreet IN VARCHAR2, 
        ParamRecordChange IN VARCHAR2, 
        ParamOldServiceLevel IN VARCHAR2, 
        ParamTextServiceLevel IN VARCHAR2, 
        ParamTextServiceLevelChange IN VARCHAR2, 
        ParamVersion IN VARCHAR2, 
        ParamNPI IN VARCHAR2, 
        ParamSpecialtyType1 IN VARCHAR2, 
        ParamSpecialtyType2 IN VARCHAR2, 
        ParamSpecialtyType3 IN VARCHAR2, 
        ParamSpecialtyType4 IN VARCHAR2, 
        ParamFileID IN VARCHAR2, 
        ParamStateLicenseNumber IN VARCHAR2, 
        ParamMedicareNumber IN VARCHAR2, 
        ParamMedicaidNumber IN VARCHAR2, 
        ParamPPONumber IN VARCHAR2, 
        ParamPayerID IN VARCHAR2, 
        ParamBINLocationNumber IN VARCHAR2, 
        ParamDEANumber IN VARCHAR2, 
        ParamHIN IN VARCHAR2, 
        ParamSecondaryCoverage IN VARCHAR2, 
        ParamNAICCode IN VARCHAR2, 
        ParamPromotionNumber IN VARCHAR2, 
        ParamSocialSecurity IN VARCHAR2, 
        ParamPriorAuthorization IN VARCHAR2, 
        ParamMutuallyDefined IN VARCHAR2,
        ParamIsActive IN VARCHAR2,
        ParamErrorMsg OUT VARCHAR2
    )
    IS 
    v_code NUMBER;
    v_errm VARCHAR2(120);
    BEGIN
        BEGIN
        MERGE INTO SS_PHARMACY 
            USING DUAL
            ON (NCPDPID = ParamNCPDPID)
            WHEN MATCHED THEN UPDATE SET 
                STORENUMBER = ParamStoreNumber,
                STORENAME = ParamStoreName,
                ADDRESSLINE1 = ParamAddressLine1,
                ADDRESSLINE2 = ParamAddressLine2,
                CITY = ParamCity,
                STATE = ParamState,
                ZIP = ParamZip,
                PHONEPRIMARY = ParamPhonePrimary,
                FAX = ParamFax,
                EMAIL = ParamEmail,
                PHONEALT1 = ParamPhoneAlt1,
                PHONEALT1QUALIFIER = ParamPhoneAlt1Qualifier,
                PHONEALT2 = ParamPhoneAlt2,
                PHONEALT2QUALIFIER = ParamPhoneAlt2Qualifier,
                PHONEALT3 = ParamPhoneAlt3,
                PHONEALT3QUALIFIER = ParamPhoneAlt3Qualifier,
                PHONEALT4 = ParamPhoneAlt4,
                PHONEALT4QUALIFIER = ParamPhoneAlt4Qualifier,
                PHONEALT5 = ParamPhoneAlt5,
                PHONEALT5QUALIFIER = ParamPhoneAlt5Qualifier,
                ACTIVESTARTTIME = ParamActiveStartTime,
                ACTIVEENDTIME = ParamActiveEndTime,
                SERVICELEVEL = ParamServiceLevel,
                PARTNERACCOUNT = ParamPartnerAccount,
                LASTMODIFIEDDATE = CAST(TO_TIMESTAMP(ParamLastModifiedDate, 'YYYY-MM-DD"T"HH24:MI:SS.FF1"Z"') AS DATE),
                CROSSSTREET = ParamCrossStreet,
                RECORDCHANGE = ParamRecordChange,
                OLDSERVICELEVEL = ParamOldServiceLevel,
                TEXTSERVICELEVEL = ParamTextServiceLevel,
                TEXTSERVICELEVELCHANGE = ParamTextServiceLevelChange,
                VERSION = ParamVersion,
                NPI = ParamNPI,
                SPECIALTYTYPE1 = ParamSpecialtyType1,
                SPECIALTYTYPE2 = ParamSpecialtyType2,
                SPECIALTYTYPE3 = ParamSpecialtyType3,
                SPECIALTYTYPE4 = ParamSpecialtyType4,
                FILEID = ParamFileID,
                STATELICENSENUMBER = ParamStateLicenseNumber,
                MEDICARENUMBER = ParamMedicareNumber,
                MEDICAIDNUMBER = ParamMedicaidNumber,
                PPONUMBER = ParamPPONumber,
                PAYERID = ParamPayerID,
                BINLOCATIONNUMBER = ParamBINLocationNumber,
                DEANUMBER = ParamDEANumber,
                HIN = ParamHIN,
                SECONDARYCOVERAGE = ParamSecondaryCoverage,
                NAICCODE = ParamNAICCode,
                PROMOTIONNUMBER = ParamPromotionNumber,
                SOCIALSECURITY = ParamSocialSecurity,
                PRIORAUTHORIZATION = ParamPriorAuthorization,
                MUTUALLYDEFINED = ParamMutuallyDefined,
                ISACTIVE = ParamIsActive
            WHEN NOT MATCHED THEN INSERT (
                NCPDPID,
                STORENUMBER,
                STORENAME,
                ADDRESSLINE1,
                ADDRESSLINE2,
                CITY,
                STATE,
                ZIP,
                PHONEPRIMARY,
                FAX,
                EMAIL,
                PHONEALT1,
                PHONEALT1QUALIFIER,
                PHONEALT2,
                PHONEALT2QUALIFIER,
                PHONEALT3,
                PHONEALT3QUALIFIER,
                PHONEALT4,
                PHONEALT4QUALIFIER,
                PHONEALT5,
                PHONEALT5QUALIFIER,
                ACTIVESTARTTIME,
                ACTIVEENDTIME,
                SERVICELEVEL,
                PARTNERACCOUNT,
                LASTMODIFIEDDATE,
                CROSSSTREET,
                RECORDCHANGE,
                OLDSERVICELEVEL,
                TEXTSERVICELEVEL,
                TEXTSERVICELEVELCHANGE,
                VERSION,
                NPI,
                SPECIALTYTYPE1,
                SPECIALTYTYPE2,
                SPECIALTYTYPE3,
                SPECIALTYTYPE4,
                FILEID,
                STATELICENSENUMBER,
                MEDICARENUMBER,
                MEDICAIDNUMBER,
                PPONUMBER,
                PAYERID,
                BINLOCATIONNUMBER,
                DEANUMBER,
                HIN,
                SECONDARYCOVERAGE,
                NAICCODE,
                PROMOTIONNUMBER,
                SOCIALSECURITY,
                PRIORAUTHORIZATION,
                MUTUALLYDEFINED,
                ISACTIVE
            )
            VALUES (
                ParamNCPDPID,
                ParamStoreNumber,
                ParamStoreName,
                ParamAddressLine1,
                ParamAddressLine2,
                ParamCity,
                ParamState,
                ParamZip,
                ParamPhonePrimary,
                ParamFax,
                ParamEmail,
                ParamPhoneAlt1,
                ParamPhoneAlt1Qualifier,
                ParamPhoneAlt2,
                ParamPhoneAlt2Qualifier,
                ParamPhoneAlt3,
                ParamPhoneAlt3Qualifier,
                ParamPhoneAlt4,
                ParamPhoneAlt4Qualifier,
                ParamPhoneAlt5,
                ParamPhoneAlt5Qualifier,
                ParamActiveStartTime,
                ParamActiveEndTime,
                ParamServiceLevel,
                ParamPartnerAccount,
                CAST(TO_TIMESTAMP(ParamLastModifiedDate, 'YYYY-MM-DD"T"HH24:MI:SS.FF1"Z"') AS DATE),
                ParamCrossStreet,
                ParamRecordChange,
                ParamOldServiceLevel,
                ParamTextServiceLevel,
                ParamTextServiceLevelChange,
                ParamVersion,
                ParamNPI,
                ParamSpecialtyType1,
                ParamSpecialtyType2,
                ParamSpecialtyType3,
                ParamSpecialtyType4,
                ParamFileID,
                ParamStateLicenseNumber,
                ParamMedicareNumber,
                ParamMedicaidNumber,
                ParamPPONumber,
                ParamPayerID,
                ParamBINLocationNumber,
                ParamDEANumber,
                ParamHIN,
                ParamSecondaryCoverage,
                ParamNAICCode,
                ParamPromotionNumber,
                ParamSocialSecurity,
                ParamPriorAuthorization,
                ParamMutuallyDefined,
                ParamIsActive
            );
        EXCEPTION
            WHEN OTHERS THEN
            BEGIN
                ParamErrorMsg := SQLCODE || ' ' || SQLERRM;
            END;
        END;    
    END SavePharmacy;
    PROCEDURE GetPrescriberByNPI(ParamNPI IN VARCHAR2, CurPrescriberInfo OUT Out_Cursor)
    IS
      PRInfo Out_Cursor;
    BEGIN
         IF ParamNPI IS NOT NULL THEN
            OPEN PRInfo FOR
            SELECT a.DOCUSER AS PrescriberID,
                   a.SPI AS SPI,
                   a.NPI AS NPI,
                   a.DEA_NO AS DEA,
                   b.ORGNAME AS FACILITYNAME,
                   a.LAST_NAME AS LASTNAME,
                   a.FIRST_NAME AS FIRSTNAME,
                   COALESCE(a.STREET,b.ADDR1||' '||b.ADDR2) AS ADDRESS,
                   COALESCE(a.CITY,b.CITY) AS CITY,
                   COALESCE(a.STATE,b.STATE) AS STATE,
                   COALESCE(a.ZIP,b.ZIP) AS ZIP,
                   COALESCE(a.EMAIL,b.EMAIL) AS EMAIL,
                   COALESCE(a.PHONE,b.TEL) AS PHONE,
                   'TE' AS PHONEQUALIFIER,
            nvl(a.fax, '') as FAX,
            'FX' AS FAXQUALIFIER
            FROM   DOCTORTABLE a
            JOIN   STREETREG b ON a.facility = b.streetadd
            WHERE a.NPI = ParamNPI;
         END IF;
         CurPrescriberInfo := PRInfo;
    END GetPrescriberByNPI;
    PROCEDURE GetSSMessageMedication(ParamID IN VARCHAR2, CurMedicationInfo OUT Out_Cursor)
    IS
      MDInfo Out_Cursor;
    BEGIN
         IF ParamID IS NOT NULL THEN
            OPEN MDInfo FOR
            SELECT MEDICATIONDESCRIPTION,
				MEDICATIONCODE,
				MEDICATIONCODETYPE,
				MEDICATIONQUANTITYQUAL,
				MEDICATIONQUANTITY,
				MEDICATIONDIRECTIONS,
				MEDICATIONNOTE,
				MEDICATIONREFILLQUAL,
				MEDICATIONREFILLQUAN,
				MEDICATIONSUBSTITUTION,
				MEDICATIONWRITTENDATE,
				MEDICATIONDOSAGEFORM,
				MEDICATIONSTRENGTH,
				MEDICATIONSTRENGTHUNITS,
				MEDICATIONDRUGDBCODE,
				MEDICATIONDRUGDBCODEQUAL,
				MEDICATIONLASTFILLDATE,
				MEDICATIONDAYSSUPPLY,
				MEDICATIONCLINICALINFOQUAL,
				MEDICATIONPRIMARYQUAL,
				MEDICATIONPRIMARYVALUE,
				MEDICATIONSECONDARYQUAL,
				MEDICATIONSECONDARYVALUE,
				RAWCONTENT
            FROM SS_MESSAGE_MEDICATION
            WHERE ID = ParamID;
         END IF;
         CurMedicationInfo := MDInfo;
    END GetSSMessageMedication;
END Wssinterface;
/
