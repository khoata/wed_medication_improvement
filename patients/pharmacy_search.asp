<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->
<!--#include file="../../library/ss_servicelevel.inc.asp"-->

<html>
<head>
<title>Pharmacy Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>

<script language="javascript">
function setpharmacy(p_code,p_name)
{
	//getFrameByName(top,"leftNoteFrameBottom").document.frm2.p_code.value = p_code;
	//getFrameByName(top,"leftNoteFrameBottom").document.frm2.pharmacy_name.value = p_name;
	
    window.opener.document.register.ncpdpid.value = p_code;
    window.opener.document.register.preferred_pharmacy.value = p_name;
	self.close();
	
}

</script>


</head>

<body>
<%set RS = server.createobject("adodb.recordset")
    pharmacy_type=trim(Request.form("pharmacy_type"))
	pharmacy_name=ucase(trim(Request.form("pharmacy_name")))	
	city_name=ucase(trim(Request.form("city_name")))
	zip=ucase(trim(Request.form("zip")))
	PHONEPRIMARY=ucase(trim(Request.form("PHONEPRIMARY")))
    chkEPCSOnly = ucase(trim(Request.form("chkEPCSOnly")))
    chkCancelRxOnly = ucase(trim(Request.form("chkCancelRxOnly")))
    chkChangeRxOnly = ucase(trim(Request.form("chkChangeRxOnly")))

%>


<form action="<%=Request.ServerVariables("script_name")%>" method="post" name="f1">

<div class="titleBox" align="center">Pharmacy Lookup</div>
<div class="titleBox">
 Pharmacy Type: <select name="pharmacy_type">
    <option value="" >All</option>
    <option value="Retail" <% if pharmacy_type = "Retail" then %> selected="selected" <% end if %>>Retail</option>
    <option value="MailOrder" <% if pharmacy_type = "MailOrder" then %> selected="selected" <% end if %>>Mail Order</option>
    <option value="Specialty" <% if pharmacy_type = "Specialty" then %> selected="selected" <% end if %>>Specialty</option>
    <option value="LongTermCare" <% if pharmacy_type = "LongTermCare" then %> selected="selected" <% end if %>>Long Term Care</option>
 </select>&nbsp;&nbsp;
 Pharmacy Name: <input type=text size=15 maxlength=15 name="pharmacy_name" value="<%=pharmacy_name%>"> &nbsp;&nbsp;
 City: <input type=text size=15 maxlength=15 name="city_name" value="<%=city_name%>"> &nbsp;&nbsp;
 ZIP: <input type=text size=10 maxlength=5 name="zip" value="<%=zip%>"> &nbsp;&nbsp;
 Phone: <input type=text size=10 maxlength=10 name="PHONEPRIMARY" value="<%=PHONEPRIMARY%>"> &nbsp;&nbsp;
 <input type="checkbox" name="chkEPCSOnly" value="true" <% if chkEPCSOnly = "TRUE" then response.write("checked") end if %> />EPCS Enabled &nbsp;&nbsp;
 <input type="checkbox" name="chkCancelRxOnly" value="true" <% if chkCancelRxOnly = "TRUE" then response.write("checked") end if %> />Cancel Enabled &nbsp;&nbsp;
 <input type="checkbox" name="chkChangeRxOnly" value="true" <% if chkChangeRxOnly = "TRUE" then response.write("checked") end if %> />Change Enabled &nbsp;&nbsp;
 <input type="submit" value="Search" id=button1 name=button1>

</div><br>


<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="3" border="0" class="sched" width="100%">
        <tr bgcolor="#3366CC">
          <td class="w">Pharmacy Name</td>
          <td class="w">Address</td>
          <td class="w">Phone</td>
          <td class="w">Fax</td>
          <td class="w">Pharmacy Type</td>
        </tr>


<%
if  pharmacy_type<>"" or pharmacy_name<>"" or zip<>"" or city_name<>"" or PHONEPRIMARY <>"" then


strSQL="select * from ss_pharmacy where 1=1 And isactive = '1' "

if pharmacy_name<>"" then
  encoded_pharmacy_name = ReplaceStr(pharmacy_name, "'", "''", 0)
	strSQL= strSQL &" And upper(storename) like upper('"&encoded_pharmacy_name&"%')"
end if
if city_name<>"" then
  encoded_city_name = ReplaceStr(city_name, "'", "''", 0)
	strSQL= strSQL &" And upper(city) like upper('"&encoded_city_name&"%')"
end if
if zip<>"" then
  encoded_zip = ReplaceStr(zip, "'", "''", 0)
	strSQL= strSQL &" And (upper(zip) like upper('"&encoded_zip&"%'))"
end if
if pharmacy_type<>"" then
  encoded_pharmacy_type = ReplaceStr(pharmacy_type, "'", "''", 0)
  strSQL= strSQL & " And (specialtytype1 = '"&encoded_pharmacy_type&"' or specialtytype2 = '"&encoded_pharmacy_type&"' or specialtytype3 = '"&encoded_pharmacy_type&"' or specialtytype4 = '"&encoded_pharmacy_type&"') "
end if
if PHONEPRIMARY <>"" then
  encoded_PHONEPRIMARY = ReplaceStr(PHONEPRIMARY, "'", "''", 0)
	strSQL= strSQL &" And upper(PHONEPRIMARY) like upper('"&encoded_PHONEPRIMARY&"%')"
end if
if chkEPCSOnly = "TRUE" then
    strSQL= strSQL &" And bitand(cast(nvl(servicelevel,'0') as varchar2(20)) ,  2048) = 2048"
end if
if chkCancelRxOnly = "TRUE" then
    strSQL= strSQL &" And bitand(cast(nvl(servicelevel,'0') as varchar2(20)) ,  16) = 16"
end if
if chkChangeRxOnly = "TRUE" then
    strSQL= strSQL &" And bitand(cast(nvl(servicelevel,'0') as varchar2(20)) ,  4) = 4"
end if

strSQL= strSQL &" order by storename"

RS.Open strSQL,CONN
i=0
if not (rs.EOF or rs.BOF) then
	while not RS.EOF
	storename=trim(replace(rs("storename"),"'",""))
    slevel = rs("servicelevel")
    if slevel <> "" then
        servicelevel = CInt(slevel)
        if ( servicelevel And SERVICELEVEL_NEWRX ) = SERVICELEVEL_NEWRX then
%>
      <tr> 
          <td>
              <a href="#" onclick="setpharmacy('<%=rs("NCPDPID")%>','<%=storename%>')"><%=RS("storename")%></a>
              <% if ( servicelevel And SERVICELEVEL_EPCS ) = SERVICELEVEL_EPCS then %>
                  <img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" />
              <% end if %>
          </td>
          <td><%=RS("ADDRESSLINE1")%>&nbsp;<%=RS("CITY")%>&nbsp;<%=RS("STATE")%>&nbsp;-&nbsp;<%=RS("ZIP")%></td>
          <td><%=RS("PHONEPRIMARY")%></td>
          <td><%=RS("fax")%></td>
          <td><%=RS("specialtytype1") %><% if RS("specialtytype2")<>"" then %>, <%=RS("specialtytype2") %><% end if %><% if RS("specialtytype3")<>"" then %>, <%=RS("specialtytype3") %><% end if %><% if RS("specialtytype4")<>"" then %>, <%=RS("specialtytype4") %><% end if %></td>
      </tr>
<%	    end if
    end if
    i=i+1
	RS.movenext
	wend
%>
<%else
	Response.Write "<tr><td>No records found</td></tr>"
end if
%>
 </table>
    </td>
  </tr>
</table>

<% end if %>

</form>
<img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /> Controlled Substance
</body>

</html>