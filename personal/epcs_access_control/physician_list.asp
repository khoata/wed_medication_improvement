<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<%
    dim rs
    set rs = server.createobject("adodb.recordset")

    dim epcs_updated
    epcs_updated = Request.QueryString("epcs_updated")

    If epcs_updated = "Y" then
	Response.Write "<script>parent.opener.location.reload(true);</script>"
	Response.Write "<script>self.close();</script>"
	Response.End
    end if

    ' epcs_access of user
    dim epcs_access
    strSQL = "SELECT epcs_access " &_
					"FROM user_pwd a " &_
			        "WHERE lower(a.userid)='" & Session("user") & "'"
    rs.Open strSQL,CONN
	if not rs.EOF then
		epcs_access = rs("epcs_access")
	end if
	rs.Close

    epcs_access = epcs_access & ""
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<%=Application("Styles")%>
<script type="text/javascript" src="/library/expand_collapse.js"></script>
<script type="text/javascript" src="../../../library/jquery-1.10.2.min.js"></script>
<title>EPCS Access Control</title>
</head>

<script language="javascript" type="text/javascript">
    function checkValid() {
        var identrustId = $("#epcs_twofactor_userid").val() || "";

        if (identrustId.length == 0) {
            alert("Please setting Identrust User ID");
            return false;
        }

        var identrustPassword = $("#epcs_twofactor_password").val() || "";

        if (identrustPassword.length == 0) {
            alert("Please input password");
            return false;
        }

        var identrustOptCode = $("#epcs_twofactor_otpcode").val() || "";

        if (identrustOptCode.length == 0) {
            alert("Please input OTP code");
            return false;
        }

        return true;
    }

    $(document).ready(function () {
        $(".approve_epcs").click(function () {
            var prescriberid = $(this).closest('tr').children('td:eq(0)').text();
            var epcs = $(this).closest('tr').children('td:eq(1)').text();
            var prescribername = $(this).closest('tr').children('td:eq(2)').text();
            var epcs_status = $(this).closest('tr').children('td:eq(3)').text();

            popScrollWindow('approve_sign_two_factor.asp?' +
            'prescriberid=' + prescriberid +
			'&prescribername=' + prescribername +
            '&epcs_status=' + epcs_status +
            '&epcs=' + epcs +
            '&userid=' + '<%=Session("user") %>', '620', '300');
        });
    });    
</script>
<body>
<%
if epcs_access = "" then
%>
You can't access this page. You don't have epcs access control.
<%
Response.End
end if
 %>
<%
if epcs_access = "P" then
queryStr = "select epcs, bitand(cast(nvl(ss_service_level,'0') as varchar2(20)), 2048) as ss_service_level, epcs_userid " & _
            " from doctortable " & _
            " where docuser = '" & Session("user") & "'"
rs.Open queryStr, CONN
if not rs.EOF then
	identrust_userid = rs("epcs_userid")
end if
%>
<div class="titleBox">EPCS access control</div><br />
This is for the first time you access this page.

<form id="frmTwoFactor" action="http://192.168.73.4/surescriptInterface/default.aspx" method="post" onsubmit="return checkValid()">
<input type="hidden" name="prescriberid" value="<%=Session("user") %>" />
<input type="hidden" name="is_sub" value="Y" />
<input type="hidden" name="epcs_approve" value="Y" />
<input type="hidden" name="is_epcs_access_control" value="Y" />

<%
'--START: Add to EPCS_Audit_Log
set rsEPCS = server.CreateObject("ADODB.Recordset")
strSQLEPCSAudit = "SELECT userid, role FROM acc WHERE upper(streetadd)=upper('"&session("pid")&"') and upper(userid) = upper('" & session("user") & "')"
rsEPCS.open strSQLEPCSAudit,con
if not (rsEPCS.eof or rsEPCS.bof) then
    epcsAuditAuthor = rsEPCS("userid")
    epcsAuditRole = rsEPCS("role")
end if
rsEPCS.close

set rsEPCS2 = server.CreateObject("ADODB.Recordset")
strSQL = "SELECT * FROM DOCTORTABLE WHERE upper(DOCUSER) = upper('" & session("user") & "')"
rsEPCS.open strSQL,con
if not (rsEPCS.eof or rsEPCS.bof) then
    strSQL2 = "SELECT orgname,streetadd FROM streetreg WHERE acc_status='V' and upper(streetadd) = upper('" & rsEPCS("Facility") & "')"
    rsEPCS2.open strSQL2,con
    if not (rsEPCS2.eof or rsEPCS2.bof) then
        epcsAuditDetail_Practice = rsEPCS2("orgname")
        epcsAuditDetail_Prescriber = rsEPCS("First_Name") & " " & rsEPCS("Last_Name")
        epcsAuditDetail_DEA = rsEPCS("DEA_NO")
    end if 
    rsEPCS2.close 
end if
rsEPCS.close
'--END

%>
<input type="hidden" name="epcsAuditAuthor" value="<%=epcsAuditAuthor %>" />
<input type="hidden" name="epcsAuditRole" value="<%=epcsAuditRole %>" />
<input type="hidden" name="epcsAuditDetail_Practice" value="<%=epcsAuditDetail_Practice %>" />
<input type="hidden" name="epcsAuditDetail_Prescriber" value="<%=epcsAuditDetail_Prescriber %>" />
<input type="hidden" name="epcsAuditDetail_DEA" value="<%=epcsAuditDetail_DEA %>" />


<table cellspacing = "2" cellpadding = "1" width="50%" border="1">
<tr>
	<td valign="top">
        

		<div class="titleBox">Sign with Two-Factor authentication</div>
		<input type="text" id="epcs_twofactor_userid" name="epcs_twofactor_userid" value="<%=identrust_userid %>" style="display: none;"/>
		<table cellspacing = "2" cellpadding = "1" width="100%" border="0">
			<tr>
				<td>
					Password:
				</td>
				<td>
					<input type="password" id="epcs_twofactor_password" name="epcs_twofactor_password"/>
					(Your password via IdenTrust)
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					OTP Code:
				</td>
				<td>
					<input type="text" id="epcs_twofactor_otpcode" name="epcs_twofactor_otpcode"/>
					(Get this code from your OTP token device)
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4">
					<p>By completing the two-factor authentication protocol at this time, you are legally signing to accept the EPCS Access Control role to approve EPCS Service Level of others in your practice. 
					</p>
				</td>
			</tr>
		</table>
	</td>
</tr>

</table>
<br />
<table cellspacing = "2" cellpadding = "1" width="50%" border="0">
<tr>
    <td>
        <center>
        <input type="submit" value="Sign + Approve" />
        <input type="button" value="Cancel" onclick="document.location='../../home.asp'" />
        </center>
    </td>
</tr>
</table>
</form>
<%
rs.Close
 %>
<%
else
queryStr = "select t2.last_name || ', ' || t2.first_name as doctorname, t2.docuser, t2.epcs, case t2.epcs " & _
            " when 'Y' then 'Active' when 'Y-P' then 'Active' else 'Inactive' end as epcs_status, " & _
            " bitand(cast(nvl(t2.ss_service_level,'0') as varchar2(20)), 2048) as ss_service_level " &_

            " from user_pwd t1 " & _
            " inner join doctortable t2 on t1.userid = t2.docuser " & _
            " inner join acc t3 on t1.userid = t3.userid " & _
            " inner join streetreg t4 on t3.streetadd = t4.streetadd " & _
            " where t4.streetadd = '" & Session("pid") & "'"
rs.Open queryStr, CONN
%>
<div class="titleBox">EPCS Service Level</div><br />

<table cellspacing="0" cellpadding="3" border="0" width="50%">
    <tr bgcolor="#3366CC">
        <td class="w" style="display:none;">Prescriber Id</td>
        <td class="w" style="display:none;">EPCS Status</td>
        <td class="w">Prescriber Name</td>
        <td class="w">EPCS Status</td>
        <td class="w"></td>
    </tr>
    <%
    while not rs.EOF
    %>
        <tr>
            <td style="display:none;"><%=rs("docuser")%></td>
            <td style="display:none;"><%=rs("epcs")%></td>
            <td><%=rs("doctorname")%></td>
            <td><%=rs("epcs_status")%></td>
            <td>
                <%
                if rs("epcs") = "Y-P" or rs("epcs") = "N-P" then
                 %>
                 <a href="#"><img class="approve_epcs" alt="approve" border="0" src="/images/global/button_approve.png" /></a>
                 <%
                 end if
                  %>
	        </td>
        </tr>
    <%
        rs.movenext
    wend

    rs.Close
    %>
    <tr>
        <td colspan="6" height="5" bgcolor="#6699FF"><img alt="spacer" src="/images/spacer.gif" height="3" /></td>
    </tr>
</table>
<%
end if
 %>
</body>

</html>