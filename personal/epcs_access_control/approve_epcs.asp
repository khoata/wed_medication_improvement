<%@ Language=VBScript %>

<!--#include file="../../../library/connection.inc.asp"-->

<%
Dim prescriberid
prescriberid= Request.QueryString("prescriberid")

dim epcs_access_control
epcs_access_control = Request.QueryString("is_epcs_access_control")

dim epcs
epcs = Request.QueryString("epcs")

set rs = server.CreateObject("adodb.recordset")

' epcs_access of user
strSQL = "SELECT epcs_access " &_
				"FROM user_pwd a " &_
			    "WHERE lower(a.userid)='" & Session("user") & "'"
rs.Open strSQL,CONN
if not rs.EOF then
	epcs_access = rs("epcs_access")
end if
rs.Close

epcs_access = epcs_access & ""

if prescriberid <> Session("user") then
    if epcs_access <> "Y" then
        Response.Redirect "physician_list.asp?epcs_updated=Y"
    end if
end if

if epcs_access_control = "Y" then
    rs.Open " update user_pwd set epcs_access = 'Y' where userid = '" & prescriberid & "'" , CONN

    if rs.State =1 then rs.Close
end if

if epcs = "Y-P" then
    rs.Open " update doctortable set epcs = 'Y' where docuser = '" & prescriberid & "'" , CONN
elseif epcs = "N-P" then
    rs.Open " update doctortable set epcs = 'N' where docuser = '" & prescriberid & "'" , CONN
end if

if rs.State =1 then rs.Close
set rs= nothing

if epcs_access_control = "Y" then
    Response.Redirect "physician_list.asp"
else
    Response.Redirect "physician_list.asp?epcs_updated=Y"
end if


%>