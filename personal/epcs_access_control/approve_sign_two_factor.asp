<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->

<%
    dim rs
    set rs = server.createobject("adodb.recordset")

    dim prescriberName
    prescriberName = Request.QueryString("prescribername")

    dim epcs_status
    epcs_status =  Request.QueryString("epcs_status")

    dim epcs
    epcs =  Request.QueryString("epcs")

    dim userid
    userid =  Request.QueryString("userid")

    dim prescriberid
    prescriberid =  Request.QueryString("prescriberid")

%>

<html>
<head>
<title>EPCS Approve</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<%=Application("Styles")%>
</head>
<script type="text/javascript" src="../../../library/jquery-1.10.2.min.js"></script>
<script type = "text/javascript">
    function checkValid() {
        var identrustId = $("#epcs_twofactor_userid").val() || "";

        if (identrustId.length == 0) {
            alert("Please setting Identrust User ID");
            return false;
        }

        var identrustPassword = $("#epcs_twofactor_password").val() || "";

        if (identrustPassword.length == 0) {
            alert("Please input password");
            return false;
        }

        var identrustOptCode = $("#epcs_twofactor_otpcode").val() || "";

        if (identrustOptCode.length == 0) {
            alert("Please input OTP code");
            return false;
        }

        return true;
    };
</script>

<body>
<h3 style="display: inline;">Prescriber Name: <%=prescriberName %></h3><br />
<h3 style="display: inline;">EPCS service status: <%=epcs_status %></h3>
<div class="titleBox_HL" style="background-color: #FFF; font-weight: normal" />

<form id="frmTwoFactor" action="http://192.168.73.4/surescriptInterface/default.aspx" method="post" onsubmit="return checkValid()" >
<input type="hidden" name="prescriberid" value="<%=prescriberid %>" />
<input type="hidden" name="is_sub" value="Y" />
<input type="hidden" name="epcs_approve" value="Y" />

<%
'--START: Add to EPCS_Audit_Log
set rsEPCS = server.CreateObject("ADODB.Recordset")
strSQLEPCSAudit = "SELECT userid, role FROM acc WHERE upper(streetadd)=upper('"&session("pid")&"') and upper(userid) = upper('" & session("user") & "')"
rsEPCS.open strSQLEPCSAudit,con
if not (rsEPCS.eof or rsEPCS.bof) then
    epcsAuditAuthor = rsEPCS("userid")
    epcsAuditRole = rsEPCS("role")
end if
rsEPCS.close

set rsEPCS2 = server.CreateObject("ADODB.Recordset")
strSQL = "SELECT * FROM DOCTORTABLE WHERE upper(DOCUSER) = upper('" & session("user") & "')"
rsEPCS.open strSQL,con
if not (rsEPCS.eof or rsEPCS.bof) then
    strSQL2 = "SELECT orgname,streetadd FROM streetreg WHERE acc_status='V' and upper(streetadd) = upper('" & rsEPCS("Facility") & "')"
    rsEPCS2.open strSQL2,con
    if not (rsEPCS2.eof or rsEPCS2.bof) then
        epcsAuditDetail_Practice = rsEPCS2("orgname")
        epcsAuditDetail_ApprovedBy = rsEPCS("First_Name") & " " & rsEPCS("Last_Name")
        epcsAuditDetail_ApproverDEA = rsEPCS("DEA_NO")
    end if 
    rsEPCS2.close 
end if
rsEPCS.close

strSQL = "SELECT * FROM DOCTORTABLE WHERE upper(DOCUSER) = upper('" & prescriberid & "')"
rsEPCS.open strSQL,con
if not (rsEPCS.eof or rsEPCS.bof) then
    epcsAuditDetail_Prescriber = rsEPCS("First_Name") & " " & rsEPCS("Last_Name")
    epcsAuditDetail_DEA = rsEPCS("DEA_NO")
end if
rsEPCS.close
'--END

%>
<input type="hidden" name="epcsAuditAuthor" value="<%=epcsAuditAuthor %>" />
<input type="hidden" name="epcsAuditRole" value="<%=epcsAuditRole %>" />

<input type="hidden" name="epcsAuditDetail_Practice" value="<%=epcsAuditDetail_Practice %>" />
<input type="hidden" name="epcsAuditDetail_Prescriber" value="<%=epcsAuditDetail_Prescriber %>" />
<input type="hidden" name="epcsAuditDetail_DEA" value="<%=epcsAuditDetail_DEA %>" />
<input type="hidden" name="epcsAuditDetail_ApprovedBy" value="<%=epcsAuditDetail_ApprovedBy %>" />
<input type="hidden" name="epcsAuditDetail_ApproverDEA" value="<%=epcsAuditDetail_ApproverDEA %>" />

<input type="hidden" name="epcs" value="<%=epcs %>" />
<% 
	strSQL = "select epcs_userid from doctortable where docuser='" & userid & "'"
	rs.Open strSQL, con
	if not rs.EOF then
		popup_identrust_userid = rs("epcs_userid")
	end if
	rs.Close
%>

<table cellspacing = "2" cellpadding = "1" width="100%" border="1">
<tr>
	<td valign="top">
        

		<div class="titleBox">Sign with Two-Factor authentication</div>
		<input type="text" id="epcs_twofactor_userid" name="epcs_twofactor_userid" value="<%=popup_identrust_userid %>" style="display: none;"/>
		<table cellspacing = "2" cellpadding = "1" width="100%" border="0">
			<tr>
				<td>
					<span>P</span>assword:
				</td>
				<td>
					<input type="password" id="epcs_twofactor_password" name="epcs_twofactor_password"/>
					(Your password via IdenTrust)
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					OTP Code:
				</td>
				<td>
					<input type="text" id="epcs_twofactor_otpcode" name="epcs_twofactor_otpcode"/>
					(Get this code from your OTP token device)
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4">
					<p>By completing the two-factor authentication protocol at this time,
					you are legally signing to approve the EPCS status of this practitioner.
					</p>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>

<br />

<center>
<input type="submit" value="Sign + Approve" id="btnSign" />
<input type="button" value="Cancel" onclick="javascript:window.close();" id="btnCancel" />
</center>

</form>
</body>
</html>