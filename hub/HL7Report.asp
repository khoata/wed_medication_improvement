<% Response.Buffer = false %>
<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/adovbs.inc" -->
<!--#include file="../../library/functions.inc.asp"-->
<!--#include file="../../library/paginate.inc.asp"-->
<%

if Session("type") <> "practice" then
	Response.Redirect("/")
end if
if session("role") <> "physician" and session("role") <> "rn" and session("role") <> "nurse" and left(session("role"),5) <> "admin" then
	Response.Redirect("/")
end if

' Get all the search criteria
' Do not clean the session if it's a refresh
'if Request.QueryString("SessionRefresh") <> "no" and Request.QueryString("index") = "" then
	'Hari-----start modified for new logic per Anwere's request
	'if session("dt_schedule") <> Request("dt") then
		'if isdate(Request("dt")) then
			'session("dt_schedule") = Request("dt")
		'else
			'session("dt_schedule") = ""
		'end if
	'end if
	'Hari-----end

	if session("first_name") <> ucase(Request("first_name")) then
		session("first_name") = ucase(Request("first_name"))
	end if
	if session("last_name") <> ucase(Request("last_name")) then
		session("last_name") = ucase(Request("last_name"))
	end if
	if session("ssn") <> Request("ssn")then
		session("ssn") = Request("ssn")
	end if
	if session("icd_code") <> Request("icd_code") then
		session("icd_code") = Request("icd_code")
	end if
'end if

'--Added by Hari for Visit Note
if Request("dt") <> "" and request("reset") <> 1 then
	session("dt_schedule") = Request("dt")
end if

if request("reset") = 1  then
   session("dt_schedule") = ""
end if
'----


if session("docid")="" then
	if Request("docid") <> "" then
		session("docid") = Request("docid")
	else
		session("docid") = session("user")
	end if
end if


' Date put into search criteria
if trim(session("dt_schedule")) <> "" then
	condition_date = "and visit.visit_date=to_date(trim('"&SQLFixUp(session("dt_schedule"))&"'),'mm/dd/yyyy')"
	dt_msg = session("dt_schedule")
else
	if session("dt_set") = 0 or request("reset") = 1 then
	  condition_date = "and visit.visit_date = to_date('"&date()&"','mm/dd/yyyy')"
	  dt_msg = date()
	  session("dt_schedule") = dt_msg
	  session("dt_set") = 1
	end if
end if

'--Added by Hari for Visit Note
If trim(Request("dt")) = "" and request("reset") <> 1 and Request.form("searchtype")=1 then
	condition_date=""
	dt_msg=""
	session("dt_schedule")=""
end if
'--------------

' First Name put into search criteria
if session("first_name") <> "" then
	'patient_table = ", patienttable"
	'patient_condition = "and visit.patient_id=patienttable.userid"
	condition_firstname = "and upper(patienttable.first_name) like '"&SQLFixUp(session("first_name"))&"%'"
else
	condition_firstname = ""
end if

' Last Name put into Search criteria
if session("last_name") <> "" then
	'patient_table = ", patienttable"
	'patient_condition = "and visit.patient_id=patienttable.userid"
	condition_lastname = "and upper(patienttable.last_name) like '"&SQLFixUp(session("last_name"))&"%'"
else
	condition_lastname = ""
end if

' Patient's SSN put into search criteria
if session("ssn") <> "" then
	'patient_table = ", patienttable"
	'patient_condition = "and visit.patient_id=patienttable.userid"
	condition_ssn = "and patienttable.ssn like '"&SQLFixUp(session("ssn"))&"%'"
else
	condition_ssn = ""
end if

' ICD code put int search criteria
if session("icd_code") <> "" then
	icd_table = ", assessplan_new"
	icd_condition = " and assessplan_new.visit_key = visit.visit_key and assessplan_new.icdcode = '"&session("icd_code")&"'"
end if

if Request("hl7type") <> "" then
	session("hl7type") = Request("hl7type")
end if

'immunization put into search criteria
if session("hl7type") = "Immunization" then
	'immu_table = ",notes_immunization"
	'immu_condition = " and notes_immunization.visit_key = visit.visit_key "
	immu_condition = " and exists (select 1 from notes_immunization where notes_immunization.visit_key = visit.visit_key) "
end if

' lab result put into search criteria
if session("hl7type") = "Lab Results" then
	'lab_table = ",labcorp_orders, LABCORP_ORDER_DATA"
	lab_condition = " and exists (select 1 from labcorp_orders, LABCORP_ORDER_DATA where labcorp_orders.visit_key = visit.visit_key and LABCORP_ORDER_DATA.order_id = labcorp_orders.order_id and LABCORP_ORDER_DATA.Status='Ready') "
end if

%>

<script language="JavaScript">
    var listPage = 1;

    function CheckItem(Appoint_Date, Patient_ID, Visit_ID, Doctor_ID) {
        document.frm2.Appoint_Date.value = Appoint_Date;
        document.frm2.Patient_ID.value = Patient_ID;
        document.frm2.Visit_ID.value = Visit_ID;
        document.frm2.Doctor_ID.value = Doctor_ID;

    }
</script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script language="JavaScript" src="../../library/calendar.js"></script>
<script language="javascript">

    function openNote() {

        var value = document.getElementById('visit_hidden').value;

        if (value != "" && value != null) {
            document.location.replace('/practice/notes/display/?visit_key=' + value);
        } else {
            alert("Please select a patient.")
        }
    }

</script>
</head>
<body>
<%
dim res
set res=server.createobject("adodb.recordset")
dim res1
set res1=server.CreateObject("adodb.recordset")
dim res55
set res55 = server.CreateObject("ADODB.RecordSet")

res.open "select first_name,last_name,docuser,doc_location from doctortable where docuser = '"&session("user")&"' union all " &_
			 " select first_name,last_name, docuser,doc_location from doctortable where docuser in(select empid from docassign where docid='" & session("user") &"' and role='physician') order by last_name asc",con
%>
<div class="titleBox">
<table border="0" cellpadding="0" cellspacing="0">
<form action="HL7Report.asp" method="post" name="frm">
	<tr>
		<td class="st">Doctor&nbsp;</td>
		<td><select size="1" name="docid" onChange="document.frm.submit();">
<%
if Request.form("docid") <> "" then
	session("docid") = Request.Form("docid")
end if
while not res.eof
	if res("first_name") <> "" and res("last_name") <> "" then
		doc_name = res("last_name")&", "&res("first_name")
		If trim(res("doc_location")) <> "" then
			doc_name_display= trim(res("last_name")&", "&res("first_name")&"-"&res("doc_location"))
		else
			doc_name_display=doc_name
		end if
	else
		doc_name = res("docuser")&""
	end if
	if session("docid") = res("docuser") then
		selected_doctor = doc_name
%>
	      <option value="<%=res("docuser")%>" selected><%=ucase(doc_name_display)%></option>
<%	else%>
          <option value="<%=res("docuser")%>"><%=ucase(doc_name_display)%></option>
<% 
	end if
	res.movenext
wend
res.Close %></select></td>
		<td class="st">&nbsp;Date&nbsp;</td>
		<td><input type="text" name="dt" value="<%=session("dt_schedule")%>" size="10" maxlength="10" onFocus="check_date(this);"></td>
		<td><a href="#" onClick="getCalendarFor(document.frm.dt); return false;"> <img src="../../images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></td>
		<td class="st">&nbsp;Last Name&nbsp;</td>
		<td><input type="text" name="last_name" size="10" maxlength="10" value="<%=session("last_name")%>"></td>
				<td class="st">&nbsp;First Name&nbsp;</td>
		<td><input type="text" name="first_name" size="10" maxlength="10" value="<%=session("first_name")%>"></td>
		<td class="st">&nbsp;SSN&nbsp;</td>
		<td><input type="text" name="ssn" size="10" maxlength="10" value="<%=session("ssn")%>"></td>
		<td class="st">Type&nbsp;</td>
		<td><select size="1" name="hl7type">
			<option <% if session("hl7type") = "Immunization" then %> selected <% end if %> >Immunization</option>
			<option <% if session("hl7type") = "Lab Results" then %> selected <% end if %> >Lab Results</option>
		</td>
		<% if session("role") = "physician" then %>
		<td class="st">&nbsp;ICD&nbsp;</td>
		<td><input type="text" name="icd_code" size="10" maxlength="10" value="<%=session("icd_code")%>"></td>
		<% end if %>
		<input type="hidden" name="searchtype" value=1>
		<td><input type="submit" name="submit_button" value="Search"></td>
    </tr>
</form>
</table>
</div>
<script language="javascript">
    var session_date = '<%=session("dt_schedule")%>';
    function check_date(that) {
        if (that.value != session_date) {
            document.frm.submit();
        } else {
            return true;
        }

    }
</script>



<p>
<div class="titleBox_HL">Click on the Green Dot to View HL7 Message</div>
<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<form name="frm2">
	<input type="hidden" name="Appoint_Date">
	<input type="hidden" name="Patient_ID">
	<input type="hidden" name="Visit_ID">
	<input type="hidden" name="Doctor_ID">
<tr>
	<td width="100%">
<%
dim res3
set res3 = server.CreateObject("ADODB.RecordSet")

set cmd = server.CreateObject("adodb.command")
cmd.ActiveConnection = CONN	


if Request("submit_button") <> "" then
	session("page") = 1
	iPageCurrent = 1
end if

res.CursorLocation = 3

res.PageSize = iPageSize
res.CacheSize = iPageSize

'added fix to sort properly - 5/2 vibol

'sqlQuery = "select linenum, proccode, procname from (select rownum as linenum, proccode, procname from (select rownum as linenum, icdcode as proccode, icdname as procname from icdtable " & sqlWhere & " order by icdcode)) where linenum between " & iCurrentRecord+1 & " and " & iSearchRecord

iCurrentRecord = (iPageCurrent - 1) * iPageSize
iSearchRecord = iCurrentRecord + iPageSize

'session("docid") = session("user")

sqlFrom = " apptrequest, confappt, visit, patienttable, resources " & patient_table & icd_table & immu_table & lab_table

sqlWhere = " apptrequest.appreqno = confappt.apptreqno and " & _
           " apptrequest.docid = confappt.docid and " &_
           " apptrequest.status = 1 and " &_
           " apptrequest.patid = visit.patient_id and " &_
           " visit.doctor_id = confappt.docid and " &_
           " visit.doctor_id = '"&session("docid")&"' and " &_
           " visit.visit_id = confappt.visitid and " &_
           " patienttable.userid = visit.patient_id and " &_
           " apptrequest.resource_id = resources.resource_id(+) " &_
		   " "&patient_condition&" "&condition_date&" "&condition_firstname&" "&condition_lastname&" "&condition_ssn&" "&icd_condition&immu_condition&lab_condition


sqlSort = " order by visit_date desc,  visit_time_new asc "
sqlVisitTimeSort = "to_date(replace(substr(visit.visit_time,0,11),'-',''),'HH:MI:SS AM') as visit_time_new,"

if isdate(session("schedule_dt")) then
	sqlSort = " order by visit_date asc,  visit_time_new asc "
	sqlVisitTimeSort = "to_date(replace(substr(visit.visit_time,0,11),'-',''),'HH:MI:SS AM') as visit_time_new,"
else
	'sqlSort = " order by visit_date asc "
end if

sqlQuery = "select linenum, resource_name, closed, reason, to_char(visit_date,'MM/DD/YYYY') as visit_date, visit_time, visit_key, patient_id, doctor_id, first_name, last_name, uploaded,global from " &_
		        "(select rownum as linenum,resource_name,closed, reason, visit_date, visit_time, visit_key, patient_id, doctor_id, first_name, last_name,uploaded, global from " &_
		             "(select " &_
						  "resources.resource_name as resource_name, " &_
						  "visit.closed as closed, " &_
		                  "patienttable.first_name as first_name, " &_
		                  "patienttable.last_name as last_name, " &_
		                  "apptrequest.reason as reason," &_
		                  "visit.visit_date as visit_date," &_
		                  "visit.visit_time as visit_time," &_
		                  sqlVisitTimeSort &_
                          "visit.visit_key as visit_key," &_
                          "visit.patient_id as patient_id," &_
                          "visit.doctor_id as doctor_id, " &_
                          "visit.global as global,uploadedtosftp AS uploaded  " &_
                           " from " & sqlFrom &_
		             " where " & sqlWhere & " " & sqlSort & ")" &_
		         ") " &_
		   "where linenum between  " & iCurrentRecord+1 & " and " & iSearchRecord


    session("account") = ""
    res55.open "Select IMMHL7_Rights.acc_id from   IMMHL7_Rights  INNER JOIN doctortable  on doctortable.facility = IMMHL7_Rights.acc_id and doctortable.docuser='"&trim(session("docid"))&"'",con
     If Not res55.EOF Then
     if (Trim(res55("acc_id")) <> "") then session("account") = res55("acc_id") end if
     End If
    res55.close


'res1.open "select first_name,last_name,middle_name from patienttable where userid='"&res("patient_id")&"'",con
'patname=ucase(res1("last_name")&", "&res1("first_name"))
'if (res1("middle_name") <> "") then patname = patname & " " & left(trim(res1("middle_name")),1)&"."
'res1.close

'Response.write sqlQuery & "<p>"

res.CursorLocation = 3

res.open sqlQuery,con

iPageCount = res.PageCount

sqlQuery = "select count(*) as count from (select visit.visit_key from " & sqlFrom & " where " & sqlWhere & ")"

set res2 = conn.execute(sqlQuery)
RecordCount = cint(res2("count").value)

if RecordCount < iPageSize and RecordCount > 0 then
	iPageCount = 1
else
	iPageCount = ceiling(cint(res2("count").Value) / iPageSize)
end if

res2.close

If iPageCurrent > iPageCount Then iPageCurrent = iPageCount
If iPageCurrent < 1 Then iPageCurrent = 1

If iPageCount = 0 Then
	Response.Write "No scheduled visits found with search criteria."
Else
    'do not set recordset page
	'res.AbsolutePage = iPageCurrent

%>
		<table border="0" cellpadding="4" cellspacing="0" width="100%" class="sched">
			<tr>
				<td class="w" width="5" height="1" bgcolor="#3366CC" align="left">&nbsp;</td>
				<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Patient Name</b></td>
				<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Physician</b></td>
				<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Date</b></td>
				<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Time</b></td>
				<td class="w" height="1" bgcolor="#3366CC" align="left" width="20%"><b>Reason for Visit</b></td>
				<td class="w" height="1" bgcolor="#3366CC" align="center"><b>Immunization Record</b></td>
                <% if session("account") <> "" then  %>
                <td class="w" height="1" bgcolor="#3366CC" align="center"><b>Immunization Uploaded</b></td>
                <% end if %>
				<td class="w" height="1" bgcolor="#3366CC" align="center"><b>Lab Results</b></td>
			</tr>
<%
iRecordsShown = 0

res1.open "select first_name,last_name,middle_name,credential from doctortable where docuser='"&trim(session("docid"))&"'",con
docnamefull=res1("first_name")&" "&res1("middle_name")&" "&res1("last_name")&", "&res1("credential")
res1.close

do while iRecordsShown < iPageSize And not res.eof
uploadstatus = res("uploaded")
patname=ucase(res("last_name")&", "&res("first_name"))

session("patname")=patname

if iRecordsShown mod 2 = 0 then
	bgcolor = "#EEEEEE"
else
	bgcolor = "#FFFFFF"
end if

PostOp = res("global")

' check for superbill and claim
'call CHECK_SP_CLAIM_status(res("visit_key"))
session("uploaded") = res("uploaded")

%>
		<tr bgcolor="<%=bgcolor%>">
			<td width="5"><input type="radio" name="visit" value="<%=res("visit_key")%>" onclick="document.getElementById('visit_hidden').value=this.value; openNote();"></td>
			<td class="mt"><b><%=UCase(patname)%></b></td>
			<td class="mt"><%=docnamefull%></td>
			<td class="mt"><%=res("visit_date")%></td>
			<td class="mt"><%=formatTimeSlot(res("visit_time"))%></td>
			<td class="mt"><b><%=ucase(res("reason"))%>&nbsp;</td>
			<td class="mt" align="center"><b><% if session("hl7type") = "Immunization" then %><a href="/GenerateImmunizationHL7/GenerateImmunizationHL7.aspx?visit_key=<%=res("visit_key")%>" target="_blank" onmouseover="window.status='View HL7 Message';return true" onmouseout="window.status='';return true" ><img src="/images/global/status_green.gif" border="0" WIDTH="15" HEIGHT="15"></a><% else %> <img src="/images/global/status_hollow.gif" border="0" WIDTH="15" HEIGHT="15"> <% end if %></b></td>            
            <% if session("account") <> "" then  %>
             <td class="mt" align="center"><b><% if session("uploaded") = "1" then %><a href="/GenerateImmunizationHL7/GenerateImmunizationHL7.aspx?visit_key=<%=res("visit_key")%>" target="_blank" onmouseover="window.status='View HL7 Message';return true" onmouseout="window.status='';return true" ><img src="/images/global/status_green.gif" border="0" WIDTH="15" HEIGHT="15"></a><% else %> <img src="/images/global/status_hollow.gif" border="0" WIDTH="15" HEIGHT="15"> <% end if %></b></td>            
           <% end if %>
			<td class="mt" align="center"><b><% if session("hl7type") = "Lab Results" then %> <a href="/HL7Client/Default.aspx?visit_key=<%=res("visit_key")%>&HL7_Type=Lab" target="_blank" onmouseover="window.status='View HL7 Message';return true" onmouseout="window.status='';return true" ><img src="/images/global/status_green.gif" border="0" WIDTH="15" HEIGHT="15"></a><% else %> <img src="/images/global/status_hollow.gif" border="0" WIDTH="15" HEIGHT="15"> <% end if %></b></td>
		</tr>


<%
iRecordsShown = iRecordsShown + 1
res.movenext

loop

res.close
%>

</table>
<br>
<input type="hidden" id="visit_hidden" name="visit_hidden">
<!--<input type="button" onclick="openNote();" name="visit_notes" id="visit_notes" value="View Visit Notes">-->
</form>

<p>
<div class="titleBox">
<table border="0" cellspacing="0" cellpadding="0" width="98%">
<form action="HL7Report.asp" method="get" id="form1" name="form1">
<input type="hidden" name="dt" value="<%=session("dt_schedule")%>">
<input type="hidden" name="first_name" value="<%=session("first_name")%>">
<input type="hidden" name="last_name" value="<%=session("last_name")%>">
<input type="hidden" name="ssn" value="<%=session("ssn")%>">
<tr>
<td><b>Pages:
<%

If iPageCurrent > 1 Then
	%>
	<a href="HL7Report.asp?dt=<%=session("dt_schedule")%>&amp;first_name=<%=session("first_name")%>&amp;last_name=<%=session("last_name")%>&amp;ssn=<%=session("ssn")%>&amp;page=<%= iPageCurrent - 1 %>">[&lt;&lt; Prev]</a>
	<%
End If
%>
<input name="page" value="<%=iPageCurrent%>" size="3" maxlength="5" type="text" style="font-size: 10px"> of <% Response.Write iPageCount %>
<%
If iPageCurrent < iPageCount Then
	%>
	<a href="HL7Report.asp?dt=<%=session("dt_schedule")%>&amp;first_name=<%=session("first_name")%>&amp;last_name=<%=session("last_name")%>&amp;ssn=<%=session("ssn")%>&amp;page=<%= iPageCurrent + 1 %>">[Next &gt;&gt;]</a>
	<%
End If

' END RUNTIME CODE
%></b>
</td>
</form>
<form action="HL7Report.asp" method="get" id="form2" name="form2">
<input type="hidden" name="dt" value="<%=session("dt_schedule")%>">
<input type="hidden" name="first_name" value="<%=session("first_name")%>">
<input type="hidden" name="last_name" value="<%=session("last_name")%>">
<input type="hidden" name="ssn" value="<%=session("ssn")%>">
<td align="right">
<b>Displaying <input name="ips" style="font-size:10px" type="text" size="2" maxlength="10" value="<%=iPageSize%>"> entries per page.</b>
</td>
</tr>
</form>
</table>
</div>
<p>
<p>
<b>Legend:</b>&nbsp;&nbsp;<img src="../../images/global/status_green.gif" border="0" WIDTH="15" HEIGHT="15">&nbsp;&nbsp;Created&nbsp;&nbsp;<img src="../../images/global/status_hollow.gif" border="0" WIDTH="15" HEIGHT="15">&nbsp;&nbsp;Not Created
<p>

<% end if %>
</body>
</html>
<%
	set Cmd.ActiveConnection=nothing
	set Cmd=nothing
%>

