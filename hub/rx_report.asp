<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->


<%
 server.ScriptTimeout=3000
 Response.Buffer = false 
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script language="JavaScript" src="../../../library/calendar.js"></script>
<title>Rx</title>
<style type="text/css">
    .unactive
	{
		z-index: -10 !important;
		display: none;
	}
</style>
<script LANGUAGE="JavaScript" SRC="/library/OverLibNew/overlib.js"></script>
<script src="../../library/jquery-1.10.2.min.js"></script>
<script type="text/javascript"  language="javascript">
	function showIllegalChanges(changeDetail) {
        $("#epcs_illegalChanges_Detail").html(changeDetail);
        var medprintHeight = $(document).height();
        $("#epcs_illegalChanges").height(medprintHeight);
        $("#epcs_illegalChanges").removeClass("unactive");
    }
    function closeIllegalChanges() {
        $("#epcs_illegalChanges").addClass("unactive");
    }
    function CheckDates() {
        var valuefrom = document.getElementById("dtfrom").value;
        var valueto = document.getElementById("dtto").value;


        if ((valuefrom == "") & (valueto == "")) {
            return true;
        }
        else {
            if (valuefrom == "" | valueto == "") {
                alert("Please enter both dates.")
                return false;
            }
        }

    }
</script>
</head>


<body>
<%
set RS = server.createobject("adodb.recordset")

rx_type = trim(Request.Form("rx_type"))
last_name=trim(Request.Form("last_name"))
first_name=trim(Request.Form("first_name"))
dtfrom=trim(Request.Form("dtfrom"))
dtto=trim(Request.Form("dtto"))
emrLink=trim(Request.Form("emrLink"))

If rx_type="" then rx_type="RefillRequest"

strSQL="select is_unread, drugid,messageType,timesent, b.status, b.statusdesc, a.PRESCRIBERAGENTLASTNAME  ||', '|| a.PRESCRIBERAGENTFIRSTNAME as doc_name, a.PRESCRIBERID,  "&_
	"	a.PRESCRIBERCLINICNAME, a.PATIENTID, a.PATIENTSSN, a.PATIENTLASTNAME ||', '||a.PATIENTFIRSTNAME as pat_name, a.MEDICATIONDESCRIPTION, a.pharmacyname, "&_
	"	a.messageID, b.RELATEDMESSAGEID, b.duplicate_key, b.cancelcode, b.canceldesc " &_
	"	from SS_MESSAGE_CONTENT a, SS_MESSAGE b,doctortable c  "&_
	"	where a.MESSAGEID=b.MESSAGEID   "&_
	"	and a.PRESCRIBERID=c.SPI "&_
	" and c.FACILITY='" & session("pid") & "' and ( messagetype='" & rx_type & "'"

'If rx_type="RefillRequest" then
	'strSQL=strSQL&" and b.is_latest = 1 and not exists ( select * from ss_message quicktest where quicktest.pharmacyid = b.pharmacyid  "
	'strSQL = strSQL & " and quicktest.RXREFERENCENUMBER = b.RXREFERENCENUMBER and quicktest.messagetype = 'RefillResponse' "
	'strSQL = strSQL & " and (quicktest.relatedmessageid is null or quicktest.relatedmessageid = b.sender_messageid)) or messageType = 'RefillResponse' and refresponsecode = 'DeniedNewPrescriptionToFollow' and newrxneeded = 'YES'"
'End if

If rx_type="RefillRequest" then
	strSQL=strSQL&" and b.is_latest = 1 and not exists ( select * from ss_message quicktest where quicktest.pharmacyid = b.pharmacyid  "
	strSQL = strSQL & " and quicktest.RXREFERENCENUMBER = b.RXREFERENCENUMBER and quicktest.messagetype = 'RefillResponse' "
	strSQL = strSQL & " and (quicktest.relatedmessageid is null or quicktest.relatedmessageid = b.sender_messageid))"
End if

If rx_type="RxChangeRequest" then
	strSQL=strSQL&" and not exists ( select * from ss_message quicktest where quicktest.pharmacyid = b.pharmacyid  "
	strSQL = strSQL & " and (b.RXREFERENCENUMBER is null  or quicktest.RXREFERENCENUMBER = b.RXREFERENCENUMBER) and quicktest.messagetype = 'RxChangeResponse' "
	strSQL = strSQL & " and (quicktest.relatedmessageid is null or quicktest.relatedmessageid = b.sender_messageid))"
End if


strSQL = strSQL & ") "

If last_name<>"" then
	strSQL=strSQL&" and upper(a.PATIENTLASTNAME) like '" & ucase(last_name) & "%'"
end if
If first_name <>"" then
	strSQL=strSQL&" and upper(a.PATIENTFIRSTNAME) like '" & ucase(first_name) & "%'"
end if

if emrLink <> "" then
    if emrLink = "No" then 
        strsql = strsql & " and drugid is null "
    else
        strsql = strsql & " and drugid is not null "
    end if
end if


if dtfrom <>"" and dtto <>""  then	
	dtfrom_1	= "to_date('"& dtfrom &"','mm/dd/yyyy')"
	dtto_1	= "to_date('"& DateAdd("d", 1, dtto  )  &"','mm/dd/yyyy')"
	date_clause=" and timesent between "& dtfrom_1 &" and "& dtto_1

elseif  Request.querystring("ft")=1 then
    dtfrom=Date()-7
    dtto=Date()
    dtfrom_1	= "to_date('"& dtfrom &"','mm/dd/yyyy')"
	dtto_1	= "to_date('"& DateAdd("d", 1, dtto  ) &"','mm/dd/yyyy')"
    date_clause=" and timesent between "& dtfrom_1 &" and "& dtto_1

end if


strSQL= strSQL & date_clause

strSQL=strSQL&" order by is_unread, timesent desc,pat_name,MEDICATIONDESCRIPTION"
'Response.Write(strSQL)

'Response.Write strSQL
'Response.end
%>
<form action="<%=Request.ServerVariables("script_name")%>" method="post" name="f1">
<div class="titleBox">Search >&nbsp;&nbsp;Status:
<%if rx_type="RefillRequest" then 
	sel1=" selected"
 elseif rx_type="NewRX" then 
	sel2=" selected"
 elseif rx_type="RefillResponse" then 
	sel3=" selected"
 elseif rx_type="CancelRx" then 
	sel4=" selected"
 elseif rx_type="RxChangeRequest" then 
	sel5=" selected"
 elseif rx_type="RxChangeResponse" then 
	sel6=" selected"
 end if
%>

<select name="rx_type">
	<option value="RefillRequest" <%=sel1%>>Open Refill</option>
	<option value="RefillResponse" <%=sel3%>>Closed Refill</option>
	<option value="NewRX" <%=sel2%>>New Rx</option>	
    <option value="CancelRx" <%=sel4%>>Cancel Rx</option>
    <option value="RxChangeRequest" <%=sel5%>>Open Change</option>
    <option value="RxChangeResponse" <%=sel6%>>Closed Change</option>
</select>
&nbsp;&nbsp;Last Name:<input type="text" size="10" maxlength="10" name="last_name" value="<%=last_name%>">
&nbsp;&nbsp;First Name:<input type="text" size="10" maxlength="10" name="first_name" value="<%=first_name%>">

&nbsp;&nbsp;Date From&nbsp;<input type=text size=9 maxlength=10 name=dtfrom id="dtfrom" value="<%=dtfrom%>">
<a href="#" onClick="getCalendarFor(document.f1.dtfrom); return false;"> <img src="/images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></a>
&nbsp;&nbsp;Date To&nbsp;<input type=text size=9 maxlength=10 name=dtto id="dtto" value="<%=dtto%>">
<a href="#" onClick="getCalendarFor(document.f1.dtto); return false;"> <img src="/images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></a>
&nbsp;&nbsp;Linked To EMR&nbsp;<select name=emrLink >
                    <option value="" <% if emrLink <> "No" and emrLink <> "Yes" then response.write " selected " %>/>
                    <option value="No" <% if emrLink = "No" then response.write " selected " %>/>No
                    <option value="Yes" <% if emrLink = "Yes" then response.write " selected " %>/>Yes
                    </select>




&nbsp;<input type="image" src="/images/global/icon_search.gif" border="0" width="18" height="18" id="image1" name="image1" onclick="return CheckDates();">


</div><br>
</form>


<div >Prescriptions Ordered Electronically
</div>

<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="3" border="0" class="sched" width="100%">
        <tr bgcolor="#3366CC">
          <td class="w">Unread</td>
          <td colspan="2" class="w">Patient Name</td>
          <td class="w">Doctor Name</td>          
          <td class="w">Drug-Formulation</td>
          <td class="w">Pharmacy</td>
          <td class="w">Date</td>
          <td class="w">Type</td>
          <td class="w">Status</td>
          <td class="w">Linked<br />To EMR</td>
		  <% if rx_type="RefillResponse" then %>
            <td class="w">Detail for EPCS</td>
          <% end if %>
        </tr>
<%
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	while not RS.EOF

	If rx_type="RefillRequest" then	  '--only for open refills
	%>	
			<tr>
              <%if RS("is_unread")="1" then  %>
                <td align="center"><span style="color: Red">*</span></td>
              <%else%>
                <td>&nbsp;</td>
              <%end if%>
			  <td colspan="2"><strong><%=RS("pat_name")%></strong></td>
			  <td><%=RS("doc_name")%></td>
	          <td><a href="#" onclick="popScrollWindow('../notes/display/msg_summary.asp?messageId=<%=rs("messageID")%>','500','400');"><b><%=RS("MEDICATIONDESCRIPTION")%></b></a></td>
	          <td><%=RS("pharmacyname")%></td>
	          <td><%=RS("timesent")%></td>
	          <%if RS("messagetype")="RefillRequest" then  %>
	          <td><a href='#' onclick="popScrollWindow('../notes/display/msg_rf_response_new.asp?drug_id=<%=rs("drugid")%>&visit_key=<%=visit_key%>&patient_id=<%if Not(IsNull(rs("patientid"))) then%><%=Escape(rs("patientid")) %><%end if%>&messageId=<%=rs("messageID")%>&duplicate_key=<%=Replace( "" & rs("duplicate_key"), "'", "")%>','850','750');" ><%=RS("messagetype")%></a></td>
	          <%else%>
	          <td><%=RS("messagetype")%></td>
	          <%end if%>
	          <td><a href="#" onclick="popScrollWindow('../notes/display/message_history.asp?messageId=<%=rs("messageID")%>&drug_id=<%=rs("drugid")%>','750','500');"><b><%=RS("status")%></b></a></td>
	          <td><% if not isnull(rs("drugid")) and rs("drugid") <> "" then %>
						Yes
					 <%else%>
						<a href="#" onclick="popScrollWindow('LinkToEMR.asp?message_id=<%=rs("messageID")%>','950','500');"><b>No</b></a>
					 <%end if %>	 
			   </td>		
	        </tr>
	<%
	
	    if not isnull( rs("duplicate_key")) then
            
            set dups = server.createobject("adodb.recordset")

            strSQL="select drugid,messageType,timesent, b.status, b.statusdesc, a.PRESCRIBERAGENTLASTNAME  ||', '|| a.PRESCRIBERAGENTFIRSTNAME as doc_name, a.PRESCRIBERID,  "&_
            "	a.PRESCRIBERCLINICNAME, a.PATIENTID, a.PATIENTSSN, a.PATIENTLASTNAME ||', '||a.PATIENTFIRSTNAME as pat_name, a.MEDICATIONDESCRIPTION, a.pharmacyname, "&_
            "	a.messageID, b.duplicate_key " &_
            "	from SS_MESSAGE_CONTENT a, SS_MESSAGE b,doctortable c  "&_
            "	where a.MESSAGEID=b.MESSAGEID " &_
            "	and a.PRESCRIBERID=c.SPI "&_
            " and b.messageid in (" & rs("duplicate_key") & ") " & _
            " order by timesent desc,pat_name,MEDICATIONDESCRIPTION"
        	
            dups.Open strSQL,CONN

            while not dups.EOF
%>
			<tr>
              <td>&nbsp;</td>
			  <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			  <td><%=dups("pat_name")%></td>
			  <td><%=dups("doc_name")%></td>
	          <td><a href="#" onclick="popScrollWindow('../notes/display/msg_summary.asp?messageId=<%=dups("messageID")%>','500','400');"><b><%=dups("MEDICATIONDESCRIPTION")%></b></a></td>
	          <td><%=dups("pharmacyname")%></td>
	          <td><%=dups("timesent")%></td>
	          <td><%=dups("messagetype")%></td>
	          <td><a href="#" onclick="popScrollWindow('../notes/display/message_history.asp?drug_id=<%=dups("drugid")%>','750','500');"><b><%=dups("status")%></b></a></td>	
	          <td><% if not isnull(dups("drugid")) and dups("drugid") <> "" then Response.Write "Yes" else response.Write "No" end if %></td>	
	        </tr>
<%

    	        dups.MoveNext
            wend
        end if
	
	 end if    '--end of open refills 
 
   If rx_type="RefillResponse" or rx_type="NewRX" or rx_type="CancelRx" or rx_type="RxChangeRequest" or rx_type="RxChangeResponse" then	  '--other refills
 %>
	<tr>
              <%if RS("is_unread")="1" then  %>
                <td align="center"><span style="color: Red">*</span></td>
              <%else%>
                <td>&nbsp;</td>
              <%end if%>
			  <td colspan="2"><%=RS("pat_name")%></td>
			  <td><%=RS("doc_name")%></td>
              <%
                  chg_rsp_medication = RS("MEDICATIONDESCRIPTION")
                  if RS("messagetype")="RxChangeResponse" and (isnull(chg_rsp_medication) or chg_rsp_medication = "") then
                      sqlChg = "SELECT MEDICATIONDESCRIPTION " &_
                               "FROM SS_MESSAGE_CONTENT " &_
                               "WHERE MESSAGEID = '" & rs("RELATEDMESSAGEID") & "'"
                    
                      set rsChg = server.CreateObject("ADODB.Recordset")
                      rsChg.open sqlChg,con
                    
                      if not (rsChg.EOF or rsChg.BOF) then
                          chg_rsp_medication = rsChg("MEDICATIONDESCRIPTION")
                      end if
                  end if
              %>
	          <td><a href="#" onclick="popScrollWindow('../notes/display/msg_summary.asp?messageId=<%=rs("messageID")%>','500','400');"><b><%=chg_rsp_medication%></b></a></td>
	          <td><%=RS("pharmacyname")%></td>
	          <td><%=RS("timesent")%></td>
	          <%if RS("messagetype")="RxChangeRequest" then  %>
	          <td><a href='#' onclick="popScrollWindow('../notes/display/msg_chg_response.asp?drug_id=<%=rs("drugid")%>&visit_key=<%=visit_key%>&patient_id=<%if Not(IsNull(rs("patientid"))) then%><%=Escape(rs("patientid")) %><%end if%>&messageId=<%=rs("messageID")%>&duplicate_key=<%=Replace( "" & rs("duplicate_key"), "'", "")%>','850','750');" ><%=RS("messagetype")%></a></td>
	          <%else%>
	          <td><%=RS("messagetype")%></td>
	          <%end if%>
              <%
                  if RS("messagetype")="CancelRx" then
                      popupTitle = "Description"
                      if RS("cancelcode") = "Approved" or RS("cancelcode") = "Denied" then
                          popupTitle = "Notes From Pharmacy"
                      end if
              %>
                  <td><a onMouseOver="return overlib('<%=RS("canceldesc")%>',LEFT,STICKY,CAPTION,'<%=popupTitle%>');" onmouseout="return nd();"><%=RS("cancelcode")%></a></td>
              <%else%>
                  <td><a href="#" onclick="popScrollWindow('../notes/display/message_history.asp?messageId=<%=rs("messageID")%>&drug_id=<%=rs("drugid")%>','750','500');"><b><%=RS("status")%></b></a></td>	
              <%end if%>
	          <td><% if not isnull(rs("drugid")) and rs("drugid") <> "" then Response.Write "Yes" else response.Write "No" end if %></td>	
			  <% if rx_type="RefillResponse" then %>
                <td>
                    <%
                    '1. Check if this message is recorded in EPCS_PRESCRIPTIONS
                    sqlEPCS = "SELECT SS_MESSAGE_ID, STATUS, CREATED_DATE " &_
                              "FROM EPCS_PRESCRIPTIONS " &_
                              "WHERE SS_MESSAGE_ID = '" & rs("messageID") & "'"
                    
                    set rsEPCS = server.CreateObject("ADODB.Recordset")
                    rsEPCS.open sqlEPCS,con
                    
                    if not (rsEPCS.EOF or rsEPCS.BOF) then
                        response.Write("<b>" & rsEPCS("Status") & "</b>")
                        checkChangeResult = FUNC_CHECKCHANGES(rs("messageID"))
                        if checkChangeResult <> "VALID" then
                            response.Write(" <span style=""color: red; text-decoration: underline; cursor: pointer;"" onclick=""showIllegalChanges('" & checkChangeResult & "');"">Changed!</span>")
                        end if
                        response.Write("<br/>signed on " & rsEPCS("Created_Date"))
                    end if
                    %>
                </td>
              <% end if %>
	 </tr>     
<%        
   End if '--end of other refills     
        
RS.movenext
  wend
  RS.Close
  set RS = nothing
%>
<%else
	Response.Write "<tr><td>No records found</td></tr>"
end if
%>
 </table>
    </td>
  </tr>
</table>
</form>
<br><br>

<div id="epcs_illegalChanges" class="unactive" style="height: 100%; width: calc(100% - 15px); z-index: 100; position: absolute; top: 0;">
    <div style="width: 450px; height: 500px; background-color: White; margin: auto; border: 3px solid #ddd; position: relative; top: 70px;">
        <div style="width: 435px; height: 30px; position: absolute; top: 0; background-color: #3366CC; padding-top: 15px; padding-left: 15px;">
            <span style="font-weight: bold; color: #FFF;">ILLEGALLY CHANGES</span>
        </div>
        <div id="epcs_illegalChanges_Detail" style="width: 420px; height: 395px; position: absolute; top: 50px; padding-left: 15px; padding-right: 15px; overflow-y: auto; line-height: 2; font-size: 14px;">
        </div>
        <div style="width: 100%; height: 50px; position: absolute; bottom: 0;">
            <input type="button" value="Close" style="font-size: 15px; margin-left: 198px; margin-top: 10px" onclick="closeIllegalChanges();" />
        </div>
    </div>
</div>

</body>
</html>

<%
FUNCTION FUNC_CHECKCHANGES(ssMessageId)
    Set oXMLHTTP = CreateObject("MSXML2.XMLHTTP")
    Set oXMLDoc = CreateObject("MSXML2.DOMDocument")
    Server.ScriptTimeout=10000
    dim url: url = "http://192.168.73.4/surescriptInterface/EPCS_Signature_Page.aspx?runType=verify&ssMessageId="&ssMessageId
    call oXMLHTTP.open("GET",url,false)
    call oXMLHTTP.setRequestHeader("Content-Type","application/x-www-form-urlencoded")

    call oXMLHTTP.send()

    FUNC_CHECKCHANGES = oXMLHTTP.responseText
END FUNCTION

%>