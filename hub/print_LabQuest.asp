<!--#include file="../../library/connOLEDB.inc.asp"-->

<%
set RS = server.createobject("adodb.recordset")
set RSAOE = server.createobject("adodb.recordset")

order_id=trim(Request("order_id"))
isprint=trim(Request("print"))

strSQL="Select * from LABCORP_ORDERS where ORDER_ID=" & order_id & " order by order_id desc"

RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	patient_id=rs("PATIENT_ID")
	doctor_id=rs("DOCTOR_ID")
	coll_date=rs("date_ordered")
	BILL_TYPE= rs("BILL_TYPE")
	
	If BILL_TYPE="T" then 
		Bill_code="Third Party"
	elseif BILL_TYPE="P" then 
		Bill_code="Patient"
	else
		Bill_code="Client"
	end if
	barcode = rs("who_ordered") & "-" & rs("order_id")		
	imgSrc = "http://www.bcgen.com/demo/linear-dbgs.aspx?D=" & barcode & "&H=2&S=0&CC=F"
	who_ordered = rs("who_ordered")
	psc_hold = rs("psc_hold")
	'info1=rs("Notes")
	'labcorp_test_num=rs("LABCORP_TEST_NUMBER")
	'labcorp_test_name=rs("LABCORP_TEST_NAME")
	'cpt_code= RS("CPT_CODEs")
	'Z_SEGMENT=rs("Z_SEGMENT")
	'SOURCE_SPECIMEN=rs("SOURCE_SPECIMEN")
end if
rs.Close

strSQL="Select * from LABCORP_ORDER_DATA where ORDER_ID="&order_id
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	InternalNotes = ""
	ReportNotes = ""
	ICD_code = ""
	while not rs.EOF 
		Test_cnt=0
		If (RS("ICD_1")<>"" and (InStr(ICD_code,RS("ICD_1")) = 0)) then ICD_code = ICD_code & RS("ICD_1") & ", "
		If (RS("ICD_2")<>"" and (InStr(ICD_code,RS("ICD_2")) = 0)) then ICD_code = ICD_code & RS("ICD_2") & ", " 
		If (RS("ICD_3")<>"" and (InStr(ICD_code,RS("ICD_3")) = 0)) then ICD_code = ICD_code & RS("ICD_3") & ", " 
		If (RS("ICD_4")<>"" and (InStr(ICD_code,RS("ICD_4")) = 0)) then ICD_code = ICD_code & RS("ICD_4") & ", " 
		
		notes = RS("Notes")
		if (len(notes) > 0) then
			notes = split(RS("Notes"),"#")
		
			for i=0 to ubound(notes)
				if Mid(notes(i),1,1) = "I" then
					InternalNotes = InternalNotes & Mid(notes(i),2,len(notes(i))) & "<BR>"
				else 
					ReportNotes = ReportNotes & Mid(notes(i),2,len(notes(i))) & "<BR>"
				end if
			next
		end if
		Test_cnt = Test_cnt + 1
		RS.MoveNext
	wend
end if
RS.Close

strSQL= "select orgname,addr1,city,state,country,zip, tel as phone from streetreg where streetadd='"&session("pid")&"'"
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	client_name=rs("orgname")
	client_Add1= rs("addr1")
	client_Add2= rs("city")&" "&rs("state")&" "&rs("zip")
	client_phone=rs("phone")	
end if
rs.Close

strSQL= "select * from Patienttable where USERID='"&patient_id&"'"
'Response.Write strSQL & "<BR>"
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	pat_ssn= RS("SSN")
	'-----
	first_name = RS("FIRST_NAME")
	last_name = RS("LAST_NAME")
    if (len(last_name) > 22) then last_name = mid(last_name,1, 22)
    if (len(first_name) > 22) then first_name = mid(first_name,1, 22)
    pat_name = last_name & "," & first_name
    if (len(pat_name) > 24) then pat_name = Mid(pat_name,1,24)
	'-----
	'pat_name = RS("LAST_NAME") & "," & RS("FIRST_NAME")
	if ucase(RS("GENDER"))="MALE" then 
		Pat_sex="M"
	elseif ucase(RS("GENDER"))="FEMALE" then
		Pat_sex="F"
	end if
	pat_dob=RS("BIRTH_DATE")
	pat_phone=RS("HOME_PHONE")
	pat_chartid=RS("userid")
	Pri_Ins= rs("PRIMARY_INSURANCE")
	sec_ins= rs("SECONDARY_INSURANCE")
	tri_ins= rs("TERTIARY_INSURANCE")
	pat_address1 = rs("STREET")
	pat_address2 = rs("CITY") & ", " & rs("STATE") & "-" & rs("ZIP")		
	'-----

	first_name = RS("contact_first_person")
	last_name = RS("contact_last_person")
	if (len(last_name) > 22) then last_name = Mid(last_name,1, 22)
	if (len(first_name) > 22) then first_name = Mid(first_name,1, 22)			

	pat_gua_name = last_name & "," & first_name
	if (len(pat_gua_name) > 24) then pat_gua_name = Mid(pat_gua_name,1,24)
	'-----
	'pat_gua_name = rs("contact_last_person") & "," & rs("contact_first_person")
	pat_gua_address1 = rs("street_contact_person")
	pat_gua_address2 = rs("city_contact_person") & "," & rs("state_contact_person") & "-" & rs("zip_contact_person")
	pat_gua_phone = rs("phone_contact_person")
end if
rs.Close

strSQL= "select * from patient_insurance where patient_id='"&patient_id&"' order by INS_order"
RS.Open strSQL,CONN
if not( rs.eof or rs.bof) then
	i=0
	redim Grp(10)
	redim id(10)
	redim Ins_name(10)
	redim Emp_Name(10)
	redim work_comp(10)
	 
		while not rs.eof
		  Ins_name(i)=RS("INSURANCE_PROGRAM_NAME")
		  Grp(i)= rs("policy_group")
		  id(i)= rs("insurance_id")
		  Emp_Name(i) = RS("EMPLOYERS_NAME")
		  work_comp(i)= RS("WORK_COMP")	  
		  i=i+1
		  rs.movenext
		 wend
else
	i=0
	redim Grp(10)
	redim id(10)
	for i=0 to 9
		Grp(i)=""
		id(i)=""
		i=i+1
	next
end if
rs.close




strSQL= "select last_name||','||first_name as doc_name,NPI,UPIN,LABCORP_ACC_ID from doctortable where docuser = '"& doctor_id &"'"
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	doc_name		= RS("doc_name")
	npi				= RS("npi")
	upin			= RS("upin")
	LABCORP_ACC_ID	= RS("LABCORP_ACC_ID")	
end if
rs.Close

if (BILL_TYPE="T") then 
	If Pri_Ins <> "" then 
		strSQL= "select * from patient_insurance where ins_order=1 and patient_id='"&patient_id&"' order by INS_order"
		'Response.Write strSQL
		RS.Open strSQL,CONN 	    
		if not (rs.EOF or rs.BOF) then
			P_INSURANCE_PROGRAM_NAME = RS("INSURANCE_PROGRAM_NAME")
			P_INSURANCE_ID	=RS("INSURANCE_ID")
			P_POLICY_GROUP	=RS("POLICY_GROUP")
			'-----

			INS_NAME_Str = RS("Insurance_Name")
            dim ins_name_array 
            ins_name_array = split(INS_NAME_Str,",")
            last_name = trim(ins_name_array(0))
            first_name = trim(ins_name_array(1))
			if (len(last_name) > 22) then last_name = Mid(last_name,1, 22)
			if (len(first_name) > 22) then first_name = Mid(first_name,1, 22)
			P_INS_NAME = last_name & "," & first_name
			if (len(P_INS_NAME) > 24) then P_INS_NAME = Mid(P_INS_NAME,1,24)
			'-----
			'P_INS_NAME = RS("Insurance_Name")
			
			P_RELATION_PATIENT = RS("RELATION_PATIENT")	
			if (P_RELATION_PATIENT = "child") then
				P_RELATION_PATIENT = "Other"
			end if			
			P_STREET	=RS("STREET")
			P_CITY	=RS("CITY")
			P_STATE	=RS("STATE")
			P_ZIP	=RS("ZIP")
			P_PH_NO	=RS("PH_NO")	
			P_EMPLOYERS_NAME	=RS("EMPLOYERS_NAME")			
			'WORK_COMP		= RS("WORK_COMP")
		end if
		rs.Close  
	   
		'get labcorp payer code and address...
		strSQL= "select a.* from LABCORP_CARRIER a, Patienttable b where a.facility_id = '" & session("pid") & "' and upper(trim(PRIMARY_INSURANCE))= upper(trim(Ins_Name)) and b.userid='"&patient_id&"' "
		RS.Open strSQL,CONN 	    
		if not (rs.EOF or rs.BOF) then
			P_LC_CARRIER_CODE			= RS("LC_CARRIER_CODE")
			P_LC_PLAN_TYPE			= RS("LC_PLAN_TYPE")	
			P_lq_billing_id			= RS("lq_billing_id")
			P_lq_toplab_client_mnemonic = RS("lq_toplab_client_mnemonic")
			P_lq_fax_no				= RS("lq_fax_no")
			P_lq_Third_Party_HMO_Flag = RS("lq_Third_Party_HMO_Flag")
			P_lq_Billing_Type			= RS("lq_Billing_Type")
			P_lq_Master_Mnemonic		= RS("lq_Master_Mnemonic")
			
			P_INS_ADD1	= RS("ADD1")
			P_INS_ADD2	= RS("ADD2")
			P_INS_CITY	= RS("CITY")
			P_INS_STATE	= RS("STATE")
			P_INS_ZIP		= RS("ZIP")
			P_INS_PHONE	= RS("PHONE")
			P_INS_address1 = ADD1 &" "&ADD2
			P_INS_address2 = CITY&" "&STATE&" "&ZIP
		end if
	   rs.Close 
	end if    
	
	If Sec_Ins <> "" then 
		strSQL= "select * from patient_insurance where ins_order=2 and patient_id='"&patient_id&"'"
		'Response.Write strSQL
		RS.Open strSQL,CONN 	    
		if not (rs.EOF or rs.BOF) then
			S_INSURANCE_PROGRAM_NAME=RS("INSURANCE_PROGRAM_NAME")
			S_INSURANCE_ID	=RS("INSURANCE_ID")
			S_POLICY_GROUP	=RS("POLICY_GROUP")
			'-----
			INS_NAME_Str = RS("Insurance_Name")
            ins_name_array = split(INS_NAME_Str,",")
            last_name = trim(ins_name_array(0))
            first_name = trim(ins_name_array(1))
			if (len(last_name) > 22) then last_name = Mid(last_name,1, 22)
			if (len(first_name) > 22) then first_name = Mid(first_name,1, 22)
			S_INS_NAME = last_name & "," & first_name
			if (len(S_INS_NAME) > 24) then S_INS_NAME = Mid(S_INS_NAME,1,24)
			'-----
			'S_INS_NAME = RS("Insurance_Name")
			S_RELATION_PATIENT = RS("RELATION_PATIENT")	
			S_STREET	=RS("STREET")
			S_CITY	=RS("CITY")
			S_STATE	=RS("STATE")
			S_ZIP		=RS("ZIP")
			S_PH_NO	=RS("PH_NO")	
			S_EMPLOYERS_NAME	=RS("EMPLOYERS_NAME")			
			'WORK_COMP		= RS("WORK_COMP")
		end if
		rs.Close
	   
		'get labcorp payer code and address...
		strSQL= "select a.* from LABCORP_CARRIER a, Patienttable b where a.facility_id = '" & session("pid") & "' and upper(trim(SECONDARY_INSURANCE))= upper(trim(Ins_Name)) and b.userid='"&patient_id&"' "
		RS.Open strSQL,CONN 	    
		if not (rs.EOF or rs.BOF) then
			S_LC_CARRIER_CODE			= RS("LC_CARRIER_CODE")
			S_LC_PLAN_TYPE			= RS("LC_PLAN_TYPE")	
			S_lq_billing_id			= RS("lq_billing_id")
			S_lq_toplab_client_mnemonic = RS("lq_toplab_client_mnemonic")
			S_lq_fax_no				= RS("lq_fax_no")
			S_lq_Third_Party_HMO_Flag = RS("lq_Third_Party_HMO_Flag")
			S_lq_Billing_Type			= RS("lq_Billing_Type")
			S_lq_Master_Mnemonic		= RS("lq_Master_Mnemonic")
			
			S_INS_ADD1	= RS("ADD1")
			S_INS_ADD2	= RS("ADD2")
			S_INS_CITY	= RS("CITY")
			S_INS_STATE	= RS("STATE")
			S_INS_ZIP		= RS("ZIP")
			S_INS_PHONE	= RS("PHONE")
			S_INS_address1 = ADD1 &" "&ADD2
			S_INS_address2 = CITY&" "&STATE&" "&ZIP
		end if
		rs.Close   
	end if
end if

If (BILL_TYPE="P") then 
	strSQL= "select * from patienttable where userid ='"&patient_id&"'"
	RS.Open strSQL,CONN 	
		    
	if not (rs.EOF or rs.BOF) then
		'-----
		first_name = RS("FIRST_NAME")
		last_name = RS("LAST_NAME")
		if (len(last_name) > 22) then last_name = Mid(last_name,1, 22)
		if (len(first_name) > 22) then first_name = Mid(first_name,1, 22)			
		P_INS_NAME = last_name & "," & first_name
		if (len(P_INS_NAME) > 24) then P_INS_NAME = Mid(P_INS_NAME,1,24)
		'-----
		'P_INS_NAME = RS("last_name")&", "&RS("first_name")	
		P_RELATION_PATIENT = "Self"	
		P_STREET= RS("STREET")
		P_CITY	= RS("CITY")
		P_STATE	= RS("STATE")
		P_ZIP	= RS("ZIP")
	end if
	rs.Close 
End if
%>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Print Lab Corp E-Req</title>

<style>div.break {page-break-before:always}</style>

</head>
<style type="text/css">
	thead	{ display: table-header-group; } 
	tfoot	{ display: table-footer-group; }
	div		{ height: 62px; border: 0; overflow:hidden;}


</style>

<%if isprint = "0" then %>
<body>
<%else%>
<body onload="window.print()";>
<%end if%>

<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1" height="695">
<thead>
<tr>
	<th width="100%" height="96">
		<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber2" height="90">
		<tr>
		    <td width="30%" align ="center">
				<img src="/images/global/WEBeDoctor.bmp" alt="WebEDoctor" width="200" height="85" /> 
		    </td>
			<td width="33%" valign="center" align="center">
				<div>
					<img src=<%=imgSrc%>/>
				</div>
			</td>
			<td width="33%" align="center" valign="Middle">
				Quest Diagnostics Incorporated
				<BR>
				<%if (psc_hold) then%>
					<font face="Arial" size="6"><B>PSC HOLD ORDER</B></font>
				<%else%>
					<font face="Arial" size="14">e</font>
				<%end if%>
				<BR>
				Bill To : <%=Bill_code%>
			</td>
		</tr>
		</table>
    </th>
</tr>
<tr>
	<th width="100%" height="15"></th>
</tr>
<tr>
	<th width="100%" height="134">
		<table width="100%">
		<tr>
			<td width="33%" height="134" valign="top">
				<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber3">
				<tr>
					<td width="100%" colspan="2"><i><b><font face="Arial">Client Information:</font></b></i></td>
				</tr>
				<tr>
					<td width="18%"><b><font face="Arial" size="2">Client #:<%=who_ordered%></font></b></td>
				</tr>
				<tr>
					<td width="82%"><%=client_name%>&nbsp;</td>
				</tr>
				<tr>
					<td width="82%"><%=client_Add1%>&nbsp;</td>
				</tr>
				<tr>
					<td width="82%"><%=client_Add2%>&nbsp;</td>
				</tr>
				<tr>
				    <td width="82%"><%=client_phone%>&nbsp;</td>
				</tr>
				</table>
			</td>
			<td width="33%" height="134" bgcolor="#CCCCCC" align="center">
				FOR LAB USE ONLY			
			</td>
			<td width="33%" height="134" align="right" valign="top">
				<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber3">
				<tr>
					<td width="100%" colspan="2"><i><b><font face="Arial">Patient Information:</font></b></i></td>
				</tr>
				<tr>
					<td width="82%"><%=pat_name%>&nbsp;</td>
				</tr>
				<tr>
					<td width="82%"><%=pat_address1%>&nbsp;</td>
				</tr>
				<tr>
					<td width="82%"><%=pat_address2%>&nbsp;</td>
				</tr>
				<tr>
					<td width="82%"><%=pat_phone%>&nbsp;</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</th>
</tr>
</thead>
<tbody>
<tr>
	<td width="100%" height="19">&nbsp;</td>
</tr>
<tr>
	<td width="100%" height="149">
		<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
		<tr>
			<td width="50%">
				Collection Date : <%=formatdatetime(coll_date,2)%> Time : <%=formatdatetime(coll_date,3)%>
				<BR>
				Lab Reference # : <%=order_id%>
			</td>
			<td width="50%">
				Pat ID # : <%=pat_chartid%> Sex : <%=pat_sex%>
				<BR>
				DOB : <%=pat_dob%>
			</td>
		</tr>
		<tr>
			<td width="50%" valign="top">
				NPI : <%=npi%>
				<BR>
				Ref Physician Provider ID : <%=doc_name%>
				<HR>
				Bill Type : <%=Bill_code%>
				<br>
				<%if (BILL_TYPE ="T") then%>
					Reponsible Party (1) :
					<BR>
					<%=P_INS_NAME%>
					<BR>
					<%=P_STREET%>
					<BR>
					<%=P_CITY%>,<%=P_STATE%>,<%=P_ZIP%>
					<BR>
					Relation : <%=P_RELATION_PATIENT%>
					<br>
					Reponsible Party (2) :
					<BR>
					<%=S_ins_name%>
					<BR>
					<%=S_STREET%>
					<BR>
					<%=S_CITY%>,<%=S_STATE%>,<%=S_ZIP%>
					<BR>
					Relation : <%=S_RELATION_PATIENT%>
				<%end if%>
				<%if (BILL_TYPE ="P") then%>
					Reponsible Party (1) :
					<BR>
					<%=pat_name%>
					<BR>
					<%=pat_address1%>
					<BR>
					<%=pat_address2%>
					<BR>
					Relation : Self
					<br>
					Reponsible Party (2) :
					<BR>
					<BR>
					<BR>
					<BR>
				<%end if%>
			</td>
			<td width="50%">
				<%if (BILL_TYPE ="T") then%>
					<table width="100%">
					<tr>
						<td colspan="2">
							Quest Bill Code : <%=P_lq_billing_id%>
						</td>
					</tr>
					<tr>
						<td>
							Priimary Carrier : 
						</td>
						<td>
							<%=P_INSURANCE_PROGRAM_NAME%>
							
						</td>
					</tr>
					<tr>
						<td>
							<%=P_INS_ADD1%>
						</td>
						<td>
							Group # : <%=P_POLICY_GROUP%>
						</td>
					</tr>
					<tr>
						<td>
							<%=P_INS_CITY%>,<%=P_INS_STATE%>,<%=P_INS_ZIP%>
						</td>
						<td>
							Insurance # : <%=P_INSURANCE_ID%>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<HR>
							Quest Bill Code : <%=S_lq_billing_id%>
						</td>
					</tr>
					<tr>
						<td>
							Secondary Carrier : 
						</td>
						<td>
							<%=S_INSURANCE_PROGRAM_NAME%>
						</td>
					</tr>
					<tr>
						<td>
							<%=S_INS_ADD1%>
						</td>
						<td>
							Group # : <%=S_POLICY_GROUP%>
						</td>
					</tr>
					<tr>
						<td>
							<%=S_INS_CITY%>,<%=S_INS_STATE%>,<%=S_INS_ZIP%>
						</td>
						<td>
							Insurance # : <%=S_INSURANCE_ID%>
						</td>
					</tr>
					</table>
				<%end if%>
				<HR>
				<%if (BILL_TYPE ="T") then%>
					<table width="100%">
					<tr>
						<td colspan="2">
							Guarantor : <%=pat_gua_name%>
						</td>
					</tr>
					<tr>
						<td>
							<%=pat_gua_address1%>
							<br>
							<%=pat_gua_address2%>
						</td>
						<td valign="top">
							Phone :<%=pat_gua_phone%>
						</td>
					</tr>
					</table>
				<%end if%>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				ICD Diagnosis Code(s) : <%=icd_code%>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		&nbsp;
	</td>
</tr>
<tr>
	<td width="100%" height="23" bgcolor="#000000" align="center"><font face="Arial" color="#FFFFFF"><B>ProMes/Tests</B></font></td>
</tr>
<tr>
	<td width="100%">
		<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber5">
		<tr>
			<!--<td width="100%"><b><%=labcorp_test_num%>&nbsp;-&nbsp;<%=labcorp_test_name%></b></td>-->
	        <td width="100%">
				<%
				strSQL="Select * from LABCORP_ORDER_DATA where ORDER_ID="&order_id
				RS.Open strSQL,CONN
				if not (rs.EOF or rs.BOF) then
					while not rs.EOF 
						id_no = RS("ID_NO")
						labcorp_test_info = RS("LABCORP_TEST_NUMBER") & "-" & RS("LABCORP_TEST_NAME") & "<br>"
						cpt_code = cpt_code & RS("CPT_CODEs") & "<br>"
						Test_cnt = Test_cnt + 1	
						strSQLAOE = "Select AOE_QUESTION_DESC,AOE_RESPONSE from LABCORP_AOE where ORDER_ID=" & order_id & " and id_no = " & id_no & " and labcorp_test_number = '" & RS("LABCORP_TEST_NUMBER") & "' order by aoe_question_desc"
						RSAOE.Open strSQLAOE,CONN
						if not (RSAOE.EOF or RSAOE.BOF) then
							AOE = ""
							while not RSAOE.EOF 
								AOE = AOE & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   " & RSAOE("AOE_QUESTION_DESC") & " : " & RSAOE("AOE_RESPONSE") & "<BR>"
								RSAOE.MoveNext
							wend
						end if
						RSAOE.Close ()
						%>
						<b><%=labcorp_test_info%></b>
						<%=AOE%>
						<%
						RS.MoveNext
					wend
				end if
				RS.Close
				%>	        
	        </td>
		  </tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber5">
		<%if (InternalNotes <> "") then%>
	    <tr>
			<td width="25%" valign="top">INTERNAL COMMENTS:</td>
			<td width="75%"><%=InternalNotes%></td>			
		</tr>
		<%end if%>
		<%if (ReportNotes <> "") then%>
		<tr>
			<td width="25%" valign="top">RESULT COMMENTS:</td>
			<td width="75%"><%=ReportNotes%></td>			
		</tr>
		<%end if%>
		</table>
	</td>
</tr>
<tr>
	<td width="100%" align="center">
		<B>End of Requisition # : <%=order_id%></B>
    </td>
</tr>
</tbody>
</table>
</body>
</html>