<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script language="JavaScript" src="../../../library/calendar.js"></script>
<title>Path Lab orders</title>
</head>

<body>
<%
set RS = server.createobject("adodb.recordset")
set RS1 = server.createobject("adodb.recordset")

status=trim(Request.Form("status"))
dtfrom=trim(Request.Form("dtfrom"))
dtto=trim(Request.Form("dtto"))
last_name =	trim(Request.Form("last_name"))
first_name	=trim(Request.Form("first_name"))

if dtfrom="" or dtto="" then 
    dtfrom=Date()-7
    dtto=Date()
end if

if status="New" then sel1=" selected"
if status="Viewed" then sel2=" selected"

strSQL="select distinct b.FIRST_NAME||' '||b.LAST_NAME as Pat_Name,c.FIRST_NAME||' '||c.LAST_NAME as doc_name, a.ORDER_ID, a.DATE_ORDERED, a.LAST_UPDATED,a.path_test_name,A.REPORT_STATUS "&_
	   "from path_ORDERS a, patienttable b, doctortable C "&_
	   "	where a.chart_id = b.chartId "&_
	   "	and a.DOCTOR_ID=c.DOCUSER "&_
	  "	and a.facility_id= '" & session("pid") & "'"	

If last_name<>"" then
	strSQL=strSQL&" and upper(b.LAST_NAME) like '" & ucase(last_name) & "%'"
end if
If first_name <>"" then
	strSQL=strSQL&" and upper(b.FIRST_NAME) like '" & ucase(first_name) & "%'"
end if

if dtfrom <>"" and dtto <>"" then
	'strSQL = strSQL&" and to_char(a.LAST_UPDATED,'mm/dd/yyyy') between '"&dtfrom&"' and '"&dtto&"' "
    dtfrom_1	= "to_date('"& dtfrom &"','mm/dd/yyyy')"
	dtto_1	= "to_date('"& DateAdd("d", 1, dtto)  &"','mm/dd/yyyy')"
    strSQL = strSQL&" and a.LAST_UPDATED between "& dtfrom_1 &" and "& dtto_1     	
end if

if status <>"" and status <>"All" then
	strSQL= strSQL &" And a.REPORT_STATUS in ('"&status&"')"
end if

strSQL= strSQL & " order by  a.LAST_UPDATED desc,Pat_Name"

	
'Response.Write strSQL

%>

<form action="<%=Request.ServerVariables("script_name")%>" method="post" name="f1">
<div class="titleBox">Search >&nbsp;&nbsp;&nbsp;&nbsp; 
Status: <select name="status">
	<option value="All">All</option>
	<option value="New" <%=sel1%>>New</option>
	<option value="Viewed" <%=sel2%>>Viewed</option>
</select>&nbsp;
Last Name:<input type="text" size="12" maxlength="12" name="last_name" value="<%=last_name%>">
First Name: <input type="text" size="12" maxlength="12" name="first_name" value="<%=first_name%>">
&nbsp;&nbsp;From Date Ordered:&nbsp;<input type=text size=9 maxlength=10 name=dtfrom value="<%=dtfrom%>">
<a href="#" onClick="getCalendarFor(document.f1.dtfrom); return false;"> <img src="/images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></a>
&nbsp;&nbsp;To Date Ordered:&nbsp;<input type=text size=9 maxlength=10 name=dtto value="<%=dtto%>">
<a href="#" onClick="getCalendarFor(document.f1.dtto); return false;"> <img src="/images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></a>
&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="/images/global/icon_search.gif" border="0" WIDTH="18" HEIGHT="18" id=image1 name=image1>
</div><br>
</form>


<div>
	The following orders have received results, please click on the test name to view/print the results.
</div>

<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="3" border="0" class="sched" width="100%">
        <tr bgcolor="#3366CC">
          <td class="w">Patient Name</td>
          <td class="w">Doctor Name</td>
          <td class="w">Date Ordered</td>
          <td class="w">Last Updated</td>
          <td class="w">Test Name</td>
          <td class="w">Status</td>     
        </tr>
<%

If strSQL <>"" then
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	while not RS.EOF
%>
        <tr>
		  <td><%=RS("pat_name")%></td>
		  <td><%=RS("doc_name")%></td>
          <td><%=RS("DATE_ORDERED")%></td>
          <td><%=RS("LAST_UPDATED")%></td>
          <td>
			 <a href="#" onclick="popScrollWindowMax('/practice/notes/display/pathlab_results.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"> <%=RS("path_TEST_NAME")%> </a><br>
          </td>  
          <td>
			<a href="#" title="Click here to change status" onclick="popScrollWindowMax('path_status.asp?order_id=<%=RS("ORDER_ID")%>&r_status=<%=RS("REPORT_STATUS")%>','50','50')"> <%=RS("REPORT_STATUS")%> </a>
           </td>
        </tr>
<%	RS.movenext
	wend
%>
<%else
	Response.Write "<tr><td>No records found</td></tr>"
end if

End if
%>
 </table>
    </td>
  </tr>
</table>
</form>
<br><br>

</body>
</html>