<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script language="JavaScript" src="../../../library/calendar.js"></script>
<title>Patient Notes/Reminders</title>
</head>

<body>
<%
set RS = server.createobject("adodb.recordset")
set RS1 = server.createobject("adodb.recordset")
set RSDoctor = server.createobject("adodb.recordset")

test_status=trim(Request.Form("test_status"))
docuser=trim(Request.Form("docuser"))
search_order=trim(Request.Form("search_order"))
date_ordered_f=trim(Request.Form("date_ordered_f"))
date_ordered_t=trim(Request.Form("date_ordered_t"))
test_name=trim(Request.Form("test_name"))
patient_first_name=trim(Request.Form("patient_first_name"))
patient_last_name=trim(Request.Form("patient_last_name"))
patient_id=trim(Request.Form("patient_id"))

if date_ordered_f="" or date_ordered_t="" then 
    date_ordered_f=Date()-7
    date_ordered_t=Date()
end if

'strSQL="select b.FIRST_NAME||' '||b.LAST_NAME as Pat_Name,c.FIRST_NAME||' '||c.LAST_NAME as doc_name, "&_
		'" a.ORDER_ID, d.LABCORP_TEST_NUMBER, d.LABCORP_TEST_NAME, a.DATE_ORDERED, a.LAST_UPDATED, LAB_RESULTS, d.NOTES, a.STATUS "&_
		'" from LABCORP_ORDERS a, patienttable b, Doctortable C,LABCORP_ORDER_DATA D "&_
		'" where a.PATIENT_ID = b.USERID  "&_
		'" and a.DOCTOR_ID=c.DOCUSER and a.ORDER_ID=d.ORDER_ID "&_
		'" and a.facility_id= '" & session("pid") & "' order by a.DATE_ORDERED desc"

        T = Request.querystring("ft")

if T=1 then
        date_ordered=Date()
        'Response.Write (date_ordered) 
end if

If test_status <>"" then

	strSQL="select distinct b.FIRST_NAME||' '||b.LAST_NAME as Pat_Name,a.patient_id as useridval,c.FIRST_NAME||' '||c.LAST_NAME as doc_name, " &_
		" a.ORDER_ID as order_id, a.DATE_ORDERED, a.LAST_UPDATED,  d.STATUS,a.transaction_id,a.message_control_id,a.bill_type,d.viewed,p.Order_id as PDFOrderID " &_
		" from LABCORP_ORDERS a, patienttable b, Doctortable C,LABCORP_ORDER_DATA D ,PDFTABLE p " &_
		" where a.PATIENT_ID = b.USERID   and a.Order_id=p.ORDER_ID(+) " &_
		" and a.DOCTOR_ID=c.DOCUSER and a.ORDER_ID=d.ORDER_ID " &_
		" and labcorp_acc_id is not null  and a.facility_id= '" & session("pid") & "'"
	
	if (test_status = "Sent") then
		strSQL = strSQL & " and d.status in ('Sent','Ready','New1') and a.transaction_id is null and a.message_control_id is null "
	else	
		strSQL = strSQL & " and d.status='" & test_status & "' and a.transaction_id is null and a.message_control_id is null"
	end if
	
	if (search_order <> "") then
		strSQL= strSQL & " and a.order_id = " & search_order 
	end if 
	if (patient_first_name <> "") then
		strSQL= strSQL & " and upper(b.first_name) like upper('" & patient_first_name & "%') "
	end if 
	if (patient_last_name <> "") then
		strSQL= strSQL & " and upper(b.last_name) like upper('" & patient_last_name & "%') "
	end if 
	if (patient_id <> "") then
		strSQL= strSQL & " and upper(b.userid) like upper('" & patient_id & "%') "
	end if 
	if (docuser <> "") then
		'strSQL= strSQL & " and upper(who_ordered) like upper('" & docuser & "%') "
	end if

	if date_ordered_f <> "" and  date_ordered_t <> "" then
        dtfrom_1	= "to_date('"& date_ordered_f &"','mm/dd/yyyy')"
	    dtto_1	= "to_date('"& DateAdd("d", 1, date_ordered_t  )  &"','mm/dd/yyyy')"
		strSQL = strSQL & " and a.date_ordered  between "& dtfrom_1 &" and "& dtto_1		
	end if
    
	if (test_name <> "") then
		strSQL = strSQL & " and upper(labcorp_test_name) like upper('%" & test_name & "%') "		
	end if
	if test_status="Sent" then sel2=" selected"
	if test_status="Ready" then sel3=" selected"		
	
	strSQL= strSQL & " order by order_id desc, a.DATE_ORDERED desc"
	'Response.Write strSQL
end if

%>
<form action="<%=Request.ServerVariables("script_name")%>" method="post" name="f1">
<div class="titleBox"><font color=red>Labcorp</font> Search &gt;&nbsp;&nbsp;Status:
<select name="test_status">
	<option value>Select Search Type</option>
	<option value="Sent" <%=sel2%>>Labs Ordered</option>
	<option value="Ready" <%=sel3%>>Results</option>	
</select>
Doctor Name
<%
strSQLDoctor = "select distinct docuser,first_name, last_name from doctortable where facility = '" & session("pid") & "' order by first_name,last_name"
RSDoctor.Open strSQLDoctor,CONN
RSDoctor.MoveFirst 
if not (RSDoctor.EOF or RSDoctor.BOF) then
%>
<select name="docuser">
<%
while not RSDoctor.EOF
%>
	<option value="<%=RSDoctor("docuser")%>" <%if (docuser = RSDoctor("docuser")) then Response.Write " selected "%>><%=RSDoctor("first_name")%>&nbsp;<%=RSDoctor("last_name")%></option>
<%
	RSDoctor.MoveNext
wend
%>
</select>
<%
end if
'--------------------
%>
Order ID
<input type="text" name="search_order" size="20" value="<%=trim(Request("search_order"))%>">
<br />
From Date Ordered
<input type="text" name="date_ordered_f" size="12" value="<%=date_ordered_f%>">
<a href="#" onClick="getCalendarFor(document.f1.date_ordered_f); return false;"> <img src="/images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></a>
To Date Ordered
<input type="text" name="date_ordered_t" size="12" value="<%=date_ordered_t%>">
<a href="#" onClick="getCalendarFor(document.f1.date_ordered_t); return false;"> <img src="/images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></a>
Test Name
<input type="text" name="test_name" size="20" value="<%=trim(Request("test_name"))%>">
<br>
Patient First Name
<input type="text" name="patient_first_name" size="20" value="<%=trim(Request("patient_first_name"))%>">
Patient Last Name
<input type="text" name="patient_last_name" size="20" value="<%=trim(Request("patient_last_name"))%>">
Patient ID
<input type="text" name="patient_id" size="20" value="<%=Request("patient_id")%>">
&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="/images/global/icon_search.gif" border="0" WIDTH="18" HEIGHT="18" id="image1" name="image1">
</div><br>
</form>

<div>
<%If test_status="Sent" then%>
		The following Lab Tests have been ordered, please click on the test name to print the Req.	
<%end if%>
<%if test_status="Ready" then%>
		The following Orders have received the results, please click on the test name to view/print the results.
<%end if%>

<%if test_status = "" then%>
		Please select the Search Type from the dropdown above.		
<%End if%>
</div>

<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="3" border="0" class="sched" width="100%">
        <tr bgcolor="#3366CC">
		  <td class="w">Order ID</td>
          <td class="w">Patient Name</td>
          <td class="w">Doctor Name</td>
          <td class="w">Date Ordered</td>
          <td class="w">Last Updated</td>
          <td class="w">Test Name</td>
          <td class="w">Bill</td>
          <td class="w">Notes</td>
          <td class="w">Status</td>
          <td class="w">Viewed</td>
        </tr>
<%
'Response.Write strSQL

If strSQL <>"" then
RS.Open strSQL,CONN
if not (RS.EOF or RS.BOF) then
	while not RS.EOF
%>
        <tr>
		  <td><%=RS("order_id")%></td>
		  <td><%=RS("pat_name")%></td>
		  <td><%=RS("doc_name")%></td>
          <td><%=RS("DATE_ORDERED")%></td>
          <td>&nbsp;<%=RS("LAST_UPDATED")%></td>
          <td>
          <%
          Notes_display=""
          strSQLData = "select Notes,LABCORP_TEST_NAME,LABCORP_TEST_NUMBER,status,ORCNUMBER from LABCORP_ORDER_DATA where ORDER_ID="&RS("ORDER_ID")&" order by LABCORP_TEST_NAME "
          'Response.Write strSQLData & "<BR>"
          RS1.Open strSQLData,CONN
          while not rs1.EOF
          %>
			<%if test_status="Ready" then%>
              <% if rs("PDFOrderID")<> "" then %>
               <a href="../../../ConvertToPDF/Default.aspx?OrderID=<%=RS("ORDER_ID")%>">
              <img src="../../ConvertToPDF/img/pdf16.png"></a> 
              <% end if %> 
					<a href="#" onclick="popScrollWindowMax('/practice/notes/display/labcorp_results.asp?order_id=<%=RS("ORDER_ID")%>&amp;test_number=<%=RS1("LABCORP_TEST_NUMBER")%>','700','500')"><font color="<%=font_color%>"><%=RS1("LABCORP_TEST_NAME")%></font></a><br>	
			<%End if%>				
			<%If test_status="Sent" then %>			
					<a href="#" onclick="popScrollWindowMax('print_labcorp_label.asp?order_id=<%=RS("ORDER_ID")%>','100','50')">
						<img src="/images/global/labels-icon.gif" alt="Print Label" border="0"></a> &nbsp;
			 <% if rs("PDFOrderID")<> "" then %>
               <a href="../../../ConvertToPDF/Default.aspx?OrderID=<%=RS("ORDER_ID")%>">
              <img src="../../ConvertToPDF/img/pdf16.png"></a>
              <% end if %>
					<a href="#" onclick="popScrollWindowMax('print_LabTest.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"><%=RS1("LABCORP_TEST_NAME")%></a><br>
			<%end if%>
			<%	RS1.MoveNext
				wend
				rs1.close
			%>
          </td>       
          <td>
          <%=RS("BILL_TYPE")%>
          </td>
          <td><%=Notes_display%>&nbsp;</td>
           <td><%=RS("Status")%></td>
           <td>&nbsp;<%=RS("viewed")%></td>
        </tr>
<%
	RS.movenext
	wend
%>
<%else
	Response.Write "<tr><td>No records found</td></tr>"
end if

End if
%>
 </table>
    </td>
  </tr>
</table>
</form>
<br><br>

</body>
</html>