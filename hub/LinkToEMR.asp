<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->
<%

if Request("SubmitFlag") = "1" then

	drug_id = Request("medi_sel")
	message_id = Request("messageID")
	
	dim CMD
	set CMD = server.CreateObject("ADODB.Command")
	CMD.ActiveConnection = CONN
	
	CMD.CommandText = "Update SS_Message Set DrugID = '"&drug_id&"' where messageid = '"&message_id&"'"
	CMD.Execute 

	Response.Write "<script>window.close();window.opener.location.reload(true);</script>"
else
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
</head>
<body>
<div class="titleBox">Medications > Link RefillRequest to EMR</div>
<p>
Columns highlighted in <span style="background-color: #FFFFCC">yellow</span> indicate closest match.
<p>
<!--<form action="medication_send_rx.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>" method="post" name="medprint" id="medprint">-->

<form action="LinkToEMR.asp" method="post" name="medprint" id="medprint">

<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">

  <tr bgcolor="#3366CC">
    <td></td>
    <td><font color="white"><b>Date</td>
    <td><font color="white"><b>Patient</td>
    <td><font color="white"><b>Prescription</td>
    <td><font color="white"><b>Note to Pharmacist</td>
    <!--<td><font color="white"><b>Current Order Status</td> -->
    
  </tr>

<%
set res1 = server.CreateObject("ADODB.Recordset")
set rs = server.CreateObject("ADODB.Recordset")

strSQL="select a.MessageID,a.PATIENTLASTNAME, a.PATIENTFIRSTNAME, a.PATIENTGENDER, a.PATIENTDOB, a.MEDICATIONDESCRIPTION, a.MEDICATIONnote,  c.docuser as physician_id, a.medicationwrittendate, a.medicationcode  "&_
	   "From SS_MESSAGE_CONTENT a, doctortable c, ss_message d " &_
	   "where a.messageid = d.messageid and d.prescriberid = c.SPI  and a.messageid = '"&Request("message_id")&"'"
'Response.Write strSQL
rs.open strSQL,con	   
if not (rs.eof or rs.BOF) then	
	physician_id = rs("physician_id")
	patient_lastname = rs("patientlastname")
	patient_firstname = rs("patientfirstname")
	medication_date = rs("medicationwrittendate")
	ndc_code = rs("medicationcode")
	Patient_dob = rs("PATIENTDOB")
 
%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top"></td>
		<td valign="top"><%=medication_date%></td>
		<td valign="top"><%=patient_lastname& ", " & patient_firstname%></td>
         <td valign="top"><b><%=rs("MEDICATIONDESCRIPTION")%></b></td>         
	     <td class="st" valign="top"><%=rs("MEDICATIONnote")%></td>	    
	</tr>
<%
end if
rs.Close 
%>
<tr bgcolor="#3366CC">
    <td colspan=5><font color="white">The following medications have been identified as possible match, please select the appropriate medication below to link to the current RefillRequest. </td>       
</tr>
<%
if ndc_code <> "" then
	ndc_clause = " AND a.NDC_Code = '"& ndc_code &"'"
end if

sqlQuery = "select a.ndc_code,a.prescribed_date,a.drug_name, a.formulation, a.dispense, a.frequency,a.substitution, a.drug_id, a.refill,a.route,a.notes,b.last_name,b.first_name,to_char(b.birth_date,'mm/dd/yyyy') as dob " &_
		   "from notes_medications a, patienttable b " &_
		   "where a.patient_id = b.userid and a.prescribed_by = '"&physician_id&"' and soundex(b.last_Name) = soundex('"&patient_lastname&"') and soundex(b.first_name) = soundex('"&patient_firstname&"') " & ndc_clause &_
		   "and upper(a.status) = 'CURRENT' " &_
		   "union " &_
		   "select a.ndc_code,a.prescribed_date,a.drug_name, a.formulation, a.dispense, a.frequency,a.substitution, a.drug_id, a.refill,a.route,a.notes,b.last_name,b.first_name,to_char(b.birth_date,'mm/dd/yyyy') as dob " &_
		   "from notes_medications a, patienttable b " &_
		   "where a.patient_id = b.userid and a.prescribed_by = '"&physician_id&"' and (b.last_Name = '"&patient_lastname&"' or b.first_name = '"&patient_firstname&"') and to_char(b.birth_date,'yyyymmdd') = '"& patient_dob &"' " & ndc_clause &_
		   "and upper(a.status) = 'CURRENT' " &_
		   "order by prescribed_date "
'Response.Write sqlQuery		   
res1.open sqlQuery,con
if not res1.eof then	
%>
<%
	cnt = 1
	while not res1.eof
			if cnt mod 2 = 0 then
				bgcolor = "#FFFFFF"
			else
				bgcolor = "#EFEFEF"
			end if

		
%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top"><input type="radio" name="medi_sel" value="<%=res1("drug_id")%>">
        <td valign="top"><b><%=res1("prescribed_date")%></td>
        <td valign="top"><b><%=res1("last_name") & ", " & res1("first_name")%><br><b>DOB:</b><%=res1("dob")%></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b> - 
          <%=res1("formulation")%><%if Is_CS="Y" then%><img alt="controlled substance,can not be sent electronacally" src="/images/practice/red_star.gif" border="0"><%end if%> <br>
	      <% if trim(res1("dispense").value) <> "" then %>
	      <b>Dispense:</b> <%=res1("dispense")%><br>
	      <% end if %>
	      <b>Refill:</b> <%If cint(res1("refill"))=999 then Response.Write "PRN" else Response.Write res1("refill") end if%>
	      <% if res1("frequency") <> "" then %>
	      <br><b>Directions:</b> <%=res1("frequency")%>-<%=PCase(res1("route"))%>
	      <% end if%>
	    </td>
	    <td class="st" valign="top"><%=res1("notes")%></td>   
	</tr>
<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close 
%>

<%
else
%>
	<tr>
		<td colspan="8">No matching medication found.</td>
	</tr>
<%
end if
%>
</table>


<p>
<div class="titleBox_HL2">
<input type="hidden" name="messageID" value="<%=Request("message_id")%>">
<input type="hidden" name="SubmitFlag" value="1">
<br><p>
<input type="submit" value="Link" id=s1 name=s2>
<input type="button" value="Manual Match" onclick="window.location='LinkToEMR_FullSearch.asp?last_name=<%=patient_lastname%>&first_name=<%=patient_firstname%>&physician_id=<%=physician_id%>&message_id=<%=Request("message_id")%>'">
<input type="button" onclick="window.close();" value="Cancel" id=button4 name=button4>
</form>
</BODY>
</HTML>
<% end if %>
