<%if (session("user") = "") then
	session("user") = "Patient Login " & UCase(Request("patient_id"))
end if%>
<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->

<%
set RS = Server.CreateObject("ADODB.RecordSet")
set RS1 = Server.CreateObject("ADODB.RecordSet")
set CMD = Server.CreateObject("ADODB.Command")

id_no  =trim(request("id_no"))


RS.open "select * from ECHO_TST where id_no ='"&id_no&"'",CONN
if not (rs.EOF or rs.BOF) then
    PAT_NAME		=RS("PAT_NAME")
	MRN				=RS("MRN")
	STUDY_DATE		=RS("STUDY_DATE")
	DOB				=RS("DOB")
	GENDER			=RS("GENDER")
	RESTING_HR    =	RS("RESTING_HR")
	TARGET_HR     = RS("TARGET_HR")
	MAX_BP        = RS("MAX_BP")
	RESTING_BP    = RS("RESTING_BP")
	MAX_HR        = RS("MAX_HR")
	TOTAL_ET      = RS("TOTAL_ET")
	HR_ACHIEVED   = RS("HR_ACHIEVED")
	METS_ACHIEVED = RS("METS_ACHIEVED")
	TST_IMP		  = RS("TST_IMP")
    REASON_TER   = RS("REASON_TER")
    Protocol  = RS("Protocol")
Rs.Close
end if

doctor_id = session("user")
strSQL="select b.orgname,a.first_name,a.last_name,a.middle_name,a.credential,a.street,a.city,a.state,a.zip,a.wphone,a.ext,a.fax,a.email,a.provider_licno,a.provider_licnostate,a.dea_no,a.speciality from  doctortable a,streetreg b where a.FACILITY  = b.streetadd and a.docuser='"& doctor_id &"'"

'Response.Write strSQL

rs.open strSQL,CONN

doc_name=rs("first_name")&" "&rs("last_name")
credential = rs("credential")
org_name=rs("orgname")

report_header="Treadmill Stress Test Report"

%>

<html>

<head>
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Print Report</title>
<style>
<!--
 p.MsoNormal
	{mso-style-parent:"";
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";
	margin-left:0in; margin-right:0in; margin-top:0in}
 table.MsoNormalTable
	{mso-style-parent:"";
	font-size:10.0pt;
	font-family:"Times New Roman"}
-->
</style>
</head>

<body onload="window.print();">

<p class="MsoNormal" style="margin-left:1.0in" align="center"><b>
<span style="font-size:14.0pt;font-family:Arial"><%'=org_name%></span></b></p>
<div style="border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .75pt;padding:0in 0in 1.0pt 0in">
  <p class="MsoNormal" align="center" style="text-align: center; border: medium none; padding: 0in">
  <b><span style="font-family:Arial"><%=report_header%></span></b></div>
<p class="MsoNormal"><b><span style="font-family:Arial">&nbsp;</span></b></p>

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="613" style="width: 459.8pt; border-collapse: collapse; border: medium none; margin-left: 5.4pt" height="47">
  <tr style="height: 16.5pt">
    <td width="227" colspan="2" valign="top" style="width: 170.0pt; height: 12; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <b><font size="2">Patient Name:</font></b></span><span style="font-size:8.0pt;font-family:Arial"><font size="2"><%=PAT_NAME%></font></span></td>
    <td width="193" valign="top" style="width: 144.5pt; height: 12; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">MRN:<%=MRN%>
    </font>
    </span></td>
    <td width="194" valign="top" style="width: 145.3pt; height: 12; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Study
    Date:<%=STUDY_DATE%> </font> </span></td>
  </tr>
  <tr style="height: 7.9pt">
    <td width="113" valign="top" style="width: 85.0pt; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">DOB:<%=DOB%>
    </font>
    </span></td>
    <td colspan=3 valign="top" style="width: 85.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Gender:<%=GENDER%> </font> </span></td>
        
  </tr>
 </table>
  
<p class="MsoNormal"><b><u>
<span style="text-decoration: none; font-size: 10.0pt; font-family: Arial">&nbsp;</span></u></b></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="612" style="width: 459.0pt; border-collapse: collapse; border: medium none; margin-left: 5.4pt" height="23">
   <tr style="height: 14.25pt">
    <td width="144" valign="top" style="width: 1.5in; height: 11; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Protocol:</font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 11; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Protocol%></font></span></td>
  </tr>    
    
    <tr style="height: 14.25pt">
    <td width="144" valign="top" style="width: 1.5in; height: 11; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Resting Heart Rate:</font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 11; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=RESTING_HR%></font></span></td>
  </tr>
  <tr style="height: 8.25pt">
    <td width="144" valign="top" style="width: 1.5in; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">RV
    Target Heart Rate:</font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=TARGET_HR%></font></span></td>
  </tr>
  <tr style="height: 13.0pt">
    <td width="144" valign="top" style="width: 1.5in; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Max BP: </font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=MAX_BP%></font></span></td>
  </tr>
  <tr style="height: 13.0pt">
    <td width="144" valign="top" style="width: 1.5in; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Resting BP: </font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=RESTING_BP%></font></span></td>
  </tr>
  <tr style="height: 13.0pt">
    <td width="144" valign="top" style="width: 1.5in; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Max Heart Rate: </font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=MAX_HR%></font></span></td>
  </tr>
  <tr style="height: 13.0pt">
    <td width="144" valign="top" style="width: 1.5in; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Total Exercise Time: </font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=TOTAL_ET%></font></span></td>
  </tr>
  <tr style="height: 13.0pt">
    <td width="144" valign="top" style="width: 1.5in; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">%of Heart Rate Achieved: </font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=HR_ACHIEVED%></font></span></td>
  </tr>
  <tr style="height: 13.0pt">
    <td width="144" valign="top" style="width: 1.5in; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">METS Achieved: </font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=METS_ACHIEVED%></font></span></td>
  </tr>
  
</table>


<p class="MsoNormal"><span style="font-size:8.0pt;font-family:Arial">&nbsp;</span></p>
<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:Arial">
Impression:</b>&nbsp;<%=TST_IMP%></span></p>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">&nbsp;</span></p>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></p>

<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">
  <b> Reason for Termination:</b>&nbsp;<%=REASON_TER%> 
</span></p>


<!--
<%session("authentication") = "1"%>
<p class="MsoNormal" style="margin-left:3.0in">
<span style="font-size:10.0pt;
font-family:Arial">
<img src="/practice/customization/signature/display_signature.asp">
</span></p>-->

<p class="MsoNormal" style="margin-left:3.0in">
<span style="font-size:10.0pt;font-family:Arial">&nbsp;<%'=org_name%></span></p>

</body>

</html>