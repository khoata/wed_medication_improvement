<%
' Scheduling frame interface
'
' Developer: Vibol Hou - 9/19/01
'
' Notes: three part row-based frameset
'
' Changelog:
'

if Session("type") <> "patient" and Session("type") <> "practice" then
	Response.Redirect("/")
end if

'if session("lastURL") <> "" then
'	mainFrame = getScript(session("lastURL"))
'	if len(mainFrame) <= 0 then
'		mainFrame = "list.asp?reset=1"
'	end if
'else
	'mainFrame = "tickets.asp?type=all"
'end if

%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<frameset rows="25,*" frameborder="NO" border="0" framespacing="0" topmargin="0" leftmargin="0"  marginwidth="0" marginheight="0"> 
  <frame noresize name="headerFrame" scrolling="NO" noresize src="header.asp">
  <frame noresize name="bodyFrame" scrolling="YES" src="<%=mainFrame%>">
</frameset>
<noframes>
<body bgcolor="#FFFFFF" text="#000000">
Internet Explorer 5+ is required to access WEBeDoctor Services. 
</body>
</noframes> 
</html>
