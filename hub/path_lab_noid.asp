<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script language="JavaScript" src="../../../library/calendar.js"></script>
<title>Path Lab orders</title>
</head>

<body>
<%
set RS = server.createobject("adodb.recordset")
set RS1 = server.createobject("adodb.recordset")

	  
strSQL=	"select distinct patient_name as Pat_Name,'Virginia Lee' doc_name, a.ORDER_ID, a.DATE_ORDERED, a.LAST_UPDATED,a.path_test_name "&_
		" from path_ORDERS a "&_
		" where chart_id is null and a.facility_id='" & session("pid") & "'"&_
		" order by Pat_Name, DATE_ORDERED desc "

'Response.Write strSQL

%>
<form action="<%=Request.ServerVariables("script_name")%>" method="post" name="f1">

</form>

<div>
	<b>Other Labs</b> - The following orders have received results, please click on the test name to view/print the results.
</div>

<div>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<b> <a href="path_lab_noid.asp">Results without Patient ChartID</a>
</div>

<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="3" border="0" class="sched" width="100%">
        <tr bgcolor="#3366CC">
          <td class="w">Patient Name</td>
          <td class="w">Doctor Name</td>
          <td class="w">Date Ordered</td>
          <td class="w">Last Updated</td>
          <td class="w">Test Name</td>        
        </tr>
<%

If strSQL <>"" then
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	while not RS.EOF
%>
        <tr>
		  <td><%=RS("pat_name")%></td>
		  <td><%=RS("doc_name")%></td>
          <td><%=RS("DATE_ORDERED")%></td>
          <td><%=RS("LAST_UPDATED")%></td>
          <td>
			 <a href="#" onclick="popScrollWindowMax('/practice/notes/display/pathlab_results.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"> <%=RS("path_TEST_NAME")%> </a><br>
          </td>  
        </tr>
<%	RS.movenext
	wend
%>
<%else
	Response.Write "<tr><td>No records found</td></tr>"
end if

End if
%>
 </table>
    </td>
  </tr>
</table>
</form>
<br><br>

</body>
</html>