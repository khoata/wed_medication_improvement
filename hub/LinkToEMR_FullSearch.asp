<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->
<%

if Request("SubmitFlag") = "1" then

	drug_id = Request("medi_sel")
	message_id = Request("message_ID")
	
	dim CMD
	set CMD = server.CreateObject("ADODB.Command")
	CMD.ActiveConnection = CONN
	
	CMD.CommandText = "Update SS_Message Set DrugID = '"&drug_id&"' where messageid = '"&message_id&"'"
	CMD.Execute 

	Response.Write "<script>window.close();window.opener.location.reload(true);</script>"
else
	last_name = Request("last_name")
	first_name = Request("first_name")
	medication = Request("medication")
	physician_id = Request("physician_id")
	message_id = Request("message_id")
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
</head>
<body>
<div class="titleBox">Medications &gt; Link RefillRequest to EMR</div>
<p>
Columns highlighted in <span style="background-color: #FFFFCC">yellow</span> indicate closest match.
<p>
<!--<form action="medication_send_rx.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>" method="post" name="medprint" id="medprint">-->

<form action="LinkToEMR_FullSearch.asp" method="post" name="fullsearch" id="fullsearch">
<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">
  <tr>
	<td colspan="5"><div class="titleBox">Search &gt;
	&nbsp;&nbsp;Last Name:<input type="text" size="10" maxlength="10" name="last_name" value="<%=last_name%>">
	&nbsp;&nbsp;First Name:<input type="text" size="10" maxlength="10" name="first_name" value="<%=first_name%>">
	&nbsp;&nbsp;Medication:<input type="text" size="10" maxlength="10" name="medication" value="<%=medication%>">
	&nbsp;<input type="image" src="/images/global/icon_search.gif" border="0" WIDTH="18" HEIGHT="18" id="image1" name="image1"></div>
	</td>
  </tr>
  <tr bgcolor="#3366CC">
    <td></td>
    <td><font color="white"><b>Date</td>
    <td><font color="white"><b>Patient</td>
    <td><font color="white"><b>Prescription</td>
    <td><font color="white"><b>Note to Pharmacist</td>
    <td><font color="white"><b>Last Visit</td>
    <!--<td><font color="white"><b>Current Order Status</td> -->
    
  </tr>
<tr bgcolor="#3366CC">
    <td colspan="6"><font color="white">The following medications have been identified as possible match, please select the appropriate medication below to link to the current RefillRequest. </td>       
</tr>
<%
dim res1
Set res1 = Server.CreateObject("ADODB.RecordSet")
dim res2
Set res2 = Server.CreateObject("ADODB.RecordSet")

if first_name <> "" then
	first_name_clause = " AND upper(b.first_name) like upper('"&first_name&"%')"
end if

if last_name <> "" then
	last_name_clause = " AND upper(b.last_name) like upper('"&last_name&"%')"
end if

if medication <> "" then
	medication_clause = " AND upper(a.drug_name) like upper('"&medication&"%')"
end if

sqlQuery = "select a.patient_id,a.ndc_code,a.prescribed_date,a.drug_name, a.formulation, a.dispense, a.frequency,a.substitution, a.drug_id, a.refill,a.route,a.notes,b.last_name,b.first_name,to_char(b.birth_date,'mm/dd/yyyy') as dob " &_
		   "from notes_medications a, patienttable b " &_
		   "where a.patient_id = b.userid and a.prescribed_by = '"&physician_id&"'" & first_name_clause & last_name_clause & medication_clause &_
		   "and upper(a.status) = 'CURRENT' "
'Response.Write sqlQuery		   
res1.open sqlQuery,con
if not res1.eof then	
%>
<%
	cnt = 1
	while not res1.eof
			if cnt mod 2 = 0 then
				bgcolor = "#FFFFFF"
			else
				bgcolor = "#EFEFEF"
			end if

			res2.Open "select visit_date,visit_key from visit where patient_id = '"&res1("patient_id")&"' and rownum < 2 order by visit_date desc",CONN
			if not res2.EOF then
				visit_date = res2("visit_date")
				visit_key = res2("visit_key")
			end if
			res2.Close
%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top"><input type="radio" name="medi_sel" value="<%=res1("drug_id")%>">
        <td valign="top"><b><%=res1("prescribed_date")%></td>
        <td valign="top"><b><%=res1("last_name") & ", " & res1("first_name")%><br><b>DOB:</b><%=res1("dob")%></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b> - 
          <%=res1("formulation")%><%if Is_CS="Y" then%><img alt="controlled substance,can not be sent electronacally" src="/images/practice/red_star.gif" border="0"><%end if%> <br>
	      <% if trim(res1("dispense").value) <> "" then %>
	      <b>Dispense:</b> <%=res1("dispense")%><br>
	      <% end if %>
	      <b>Refill:</b> <%If cint(res1("refill"))=999 then Response.Write "PRN" else Response.Write res1("refill") end if%>
	      <% if res1("frequency") <> "" then %>
	      <br><b>Directions:</b> <%=res1("frequency")%>-<%=PCase(res1("route"))%>
	      <% end if%>
	    </td>
	    <td class="st" valign="top"><%=res1("notes")%></td>   
	    <td class="st" valign="top"><a href="#" onclick="popScrollWindow('/practice/notes/display/?visit_key=<%=visit_key%>',800,500);"><%=visit_date%></a></td>
	</tr>
<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close 
%>

<%
else
%>
	<tr>
		<td colspan="8">No matching medication found.</td>
	</tr>
<%
end if
%>
</table>


<p>
<div class="titleBox_HL2">
<input type="hidden" name="physician_id" value="<%=physician_id%>">
<input type="hidden" name="message_ID" value="<%=Request("message_id")%>">
<input type="hidden" name="SubmitFlag" value="0">
<br><p>
<input type="button" value="Link" onclick="document.fullsearch.SubmitFlag.value=1;document.fullsearch.submit();" id="s1" name="s2">
<input type="button" value="Back To System Match" onclick="window.location='LinkToEMR.asp?message_id=<%=Request("message_id")%>'">
<input type="button" onclick="window.close();" value="Cancel" id="button4" name="button4">
</form>
</body>
</html>
<% end if %>
