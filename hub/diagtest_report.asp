<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script language="JavaScript" src="../../../library/calendar.js"></script>
<title>Diagnostic Tests</title>
</head>

<body>
<%
set RS = server.createobject("adodb.recordset")

search_type =trim(Request.Form("search_type"))
last_name =	trim(Request.Form("last_name"))
first_name	=trim(Request.Form("first_name"))

strSQL="select b.FIRST_NAME,b.LAST_NAME,b.HOME_PHONE,b.birth_date,"&_
		" a.test_type, a.STUDY_DATE, a.DIAGNOSIS,a.VISIT_KEY,a.PATIENT_ID "&_
		" from RTE_ECHO a, patienttable b where a.PATIENT_ID = b.USERID and b.facility_id= '" & session("pid") & "'"

If last_name<>"" then
	strSQL=strSQL&" and upper(b.LAST_NAME) like '" & ucase(last_name) & "%'"
end if
If first_name <>"" then
	strSQL=strSQL&" and upper(b.FIRST_NAME) like '" & ucase(first_name) & "%'"
end if

'Response.Write strSQL

%>
<form action="<%=Request.ServerVariables("script_name")%>" method="post" name="f1">
<div class="titleBox">Search &gt;&nbsp;&nbsp;&nbsp;&nbsp;Test Type: 
<select name="search_type">
	 <option value="Echo"> Echo Test</option>
</select>
&nbsp;&nbsp;Last Name:<input type="text" size="15" maxlength="12" name="last_name" value="<%=last_name%>">
&nbsp;&nbsp;First Name:<input type="text" size="15" maxlength="12" name="first_name" value="<%=first_name%>">

&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="/images/global/icon_search.gif" border="0" WIDTH="18" HEIGHT="18" id="image1" name="image1">
</div><br>
</form>


<div>Diagnostic Tests Report
</div>

<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="3" border="0" class="sched" width="100%">
        <tr bgcolor="#3366CC">
          <td class="w">Patient Name</td>
          <td class="w">Birth Date</td>
          <td class="w">Contact#</td>
          <td class="w">Test Type</td>          
          <td class="w">Diagnosis</td>
          <td class="w">Study Date</td>
          <td class="w">Print</td>
		</tr>
<%
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	while not RS.EOF
%>
        <tr>
		  <td><%=RS("first_name")%>&nbsp;<%=RS("Last_name")%></td>
		  <td><%=RS("birth_date")%></td>
          <td><%=RS("home_phone")%></td>
          <td>Echo Test</td>
          <td><%=RS("DIAGNOSIS")%></td>
          <td><%=RS("STUDY_DATE")%></td>
          <td><input type="image" src="/images/global/print.gif" border="0" onclick="window.open('print_echotest.asp?visit_key=<%=rs("visit_key")%>&amp;patient_id=<%=rs("patient_id")%>&amp;test_type=<%=RS("TEST_TYPE")%>','aaaa','width=700,height=500,menubar=1,scrollbars=1,resizable=0')"></td>
		</tr>
<%	RS.movenext
	wend
%>
<%else
	Response.Write "<tr><td>No records found</td></tr>"
end if
%>
 </table>
    </td>
  </tr>
</table>
</form>
<br><br>

</body>

</html>