<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script language="JavaScript" src="../../../library/calendar.js"></script>
<title>Patient Notes/Reminders</title>
</head>

<body>
<%
set RS = server.createobject("adodb.recordset")
set RS1 = server.createobject("adodb.recordset")
set RSDoctor = server.createobject("adodb.recordset")

test_status=trim(Request.Form("test_status"))
docuser=trim(Request.Form("docuser"))
search_order=trim(Request.Form("search_order"))
date_ordered=trim(Request.Form("date_ordered"))
test_name=trim(Request.Form("test_name"))
patient_first_name=trim(Request.Form("patient_first_name"))
patient_last_name=trim(Request.Form("patient_last_name"))
patient_id=trim(Request.Form("patient_id"))

'dtfrom=trim(Request.Form("dtfrom"))
'dtto=trim(Request.Form("dtto"))

'strSQL="select b.FIRST_NAME||' '||b.LAST_NAME as Pat_Name,c.FIRST_NAME||' '||c.LAST_NAME as doc_name, "&_
		'" a.ORDER_ID, d.LABCORP_TEST_NUMBER, d.LABCORP_TEST_NAME, a.DATE_ORDERED, a.LAST_UPDATED, LAB_RESULTS, d.NOTES, a.STATUS "&_
		'" from LABCORP_ORDERS a, patienttable b, Doctortable C,LABCORP_ORDER_DATA D "&_
		'" where a.PATIENT_ID = b.USERID  "&_
		'" and a.DOCTOR_ID=c.DOCUSER and a.ORDER_ID=d.ORDER_ID "&_
		'" and a.facility_id= '" & session("pid") & "' order by a.DATE_ORDERED desc"

If test_status <>"" then

	strSQL="select distinct b.FIRST_NAME||' '||b.LAST_NAME as Pat_Name,a.patient_id as useridval,c.FIRST_NAME||' '||c.LAST_NAME as doc_name, " &_
		" a.ORDER_ID as order_id, a.DATE_ORDERED, a.LAST_UPDATED,  d.STATUS,a.transaction_id,a.message_control_id,a.bill_type " &_
		" from LABCORP_ORDERS a, patienttable b, Doctortable C,LABCORP_ORDER_DATA D " &_
		" where a.PATIENT_ID = b.USERID  " &_
		" and a.DOCTOR_ID=c.DOCUSER and a.ORDER_ID=d.ORDER_ID " &_
		" and a.facility_id= '" & session("pid") & "'"
	if (test_status = "Result_PatientID_notfound") then
		strSQL = "select distinct patient_id as Pat_Name, patient_id as doc_name, a.patient_id as useridval, a.ORDER_ID as order_id, a.DATE_ORDERED, a.LAST_UPDATED, d.STATUS,a.transaction_id,a.message_control_id,a.bill_type " &_
		" from LABCORP_ORDERS a,LABCORP_ORDER_DATA D " &_
		" where a.ORDER_ID=d.ORDER_ID " &_
		" and a.facility_id= '" & session("pid") & "'"
	end if 
	
	if (test_status = "Sent") then
		strSQL = strSQL & " and d.status in ('Sent','Ready','New1') "
	else	
		strSQL = strSQL & " and d.status='" & test_status & "'"
	end if
	
	if (search_order <> "") then
		strSQL= strSQL & " and a.order_id = " & search_order 
	end if 
	if (patient_first_name <> "") then
		strSQL= strSQL & " and upper(b.first_name) like upper('" & patient_first_name & "%') "
	end if 
	if (patient_last_name <> "") then
		strSQL= strSQL & " and upper(b.last_name) like upper('" & patient_last_name & "%') "
	end if 
	if (patient_id <> "") then
		strSQL= strSQL & " and upper(b.userid) like upper('" & patient_id & "%') "
	end if 
	if (docuser <> "") then
		'strSQL= strSQL & " and upper(who_ordered) like upper('" & docuser & "%') "
	end if
	if (date_ordered <> "") then
		strSQL = strSQL & " and a.date_ordered like to_date('" & date_ordered & "','MM/DD/YYYY') "		
	end if
	if (test_name <> "") then
		strSQL = strSQL & " and upper(labcorp_test_name) like upper('%" & test_name & "%') "		
	end if
	if test_status="Sent" then sel2=" selected"
	if test_status="Ready" then sel3=" selected"
	if test_status="Error" then sel4=" selected"
	if test_status="Result_Error" then sel5=" selected"
	if test_status="Result_PatientID_notfound" then sel6=" selected"	
	
	strSQL= strSQL & " order by order_id desc, a.DATE_ORDERED desc"
	'Response.Write strSQL
end if

%>
<form action="<%=Request.ServerVariables("script_name")%>" method="post" name="f1">
<div class="titleBox">Search &gt;&nbsp;&nbsp;Status:
<select name="test_status">
	<option value>Select Search Type</option>
	<option value="Sent" <%=sel2%>>Labs Ordered</option>
	<option value="Ready" <%=sel3%>>Results</option>
	<option value="Error" <%=sel4%>>Labs Ordered Error</option>
	<option value="Result_Error" <%=sel5%>>Result Error</option>
	<option value="Result_PatientID_notfound" <%=sel6%>>Result Patient ID Not Found</option>
</select>
Doctor Name
<%
strSQLDoctor = "select * from doctortable where facility = '" & session("pid") & "' order by first_name,last_name"
RSDoctor.Open strSQLDoctor,CONN
RSDoctor.MoveFirst 
if not (RSDoctor.EOF or RSDoctor.BOF) then
%>
<select name="docuser">
<%
while not RSDoctor.EOF
%>
	<option value="<%=RSDoctor("docuser")%>" <%if (docuser = RSDoctor("docuser")) then Response.Write " selected "%>><%=RSDoctor("first_name")%>&nbsp;<%=RSDoctor("last_name")%></option>
<%
	RSDoctor.MoveNext
wend
%>
</select>
<%
end if
'--------------------
%>
Order ID
<input type="text" name="search_order" size="20" value="<%=trim(Request("search_order"))%>">
Date Ordered
<input type="text" name="date_ordered" size="20" value="<%=trim(Request("date_ordered"))%>">
Test Name
<input type="text" name="test_name" size="20" value="<%=trim(Request("test_name"))%>">
<br>
Patient First Name
<input type="text" name="patient_first_name" size="20" value="<%=trim(Request("patient_first_name"))%>">
Patient Last Name
<input type="text" name="patient_last_name" size="20" value="<%=trim(Request("patient_last_name"))%>">
Patient ID
<input type="text" name="patient_id" size="20" value="<%=Request("patient_id")%>">
&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="/images/global/icon_search.gif" border="0" WIDTH="18" HEIGHT="18" id="image1" name="image1">
</div><br>
</form>

<div>
<%If test_status="Sent" then%>
		The following Lab Tests have been ordered, please click on the test name to print the Req.	
<%end if%>
<%if test_status="Ready" then%>
		The following Orders have received the results, please click on the test name to view/print the results.
<%end if%>
<%if test_status="Error" then%>
		The following Orders were not Sent &amp; have Error message, please click on the test name to view the details.
<%end if%>
<%if test_status="Result_Error" then%>
		The following Results were received &amp; have Error message, please click on the test name to view the details.
<%end if%>
<%if test_status = "" then%>
		Please select the Search Type from the dropdown above.		
<%End if%>
</div>

<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="3" border="0" class="sched" width="100%">
        <tr bgcolor="#3366CC">
		  <td class="w">Order ID</td>
          <td class="w">Patient Name</td>
          <td class="w">Doctor Name</td>
          <td class="w">Date Ordered</td>
          <td class="w">Last Updated</td>
          <td class="w">Test Name</td>
          <td class="w">Bill</td>
          <td class="w">Notes</td>
          <td class="w">Status</td>
        </tr>
<%
'Response.Write strSQL

If strSQL <>"" then
RS.Open strSQL,CONN
if not (RS.EOF or RS.BOF) then
	while not RS.EOF
%>
        <tr>
		  <td>
			<%If RS("transaction_id") <> "" then %>
				<a href="labquest_order_edit.asp?order_id=<%=RS("ORDER_ID")%>"> <%=RS("order_id")%> </a>
			<%else%>
				<%=RS("order_id")%> 
			<%end if%>
		  </td>
		  <td><%=RS("pat_name")%></td>
		  <td><%=RS("doc_name")%></td>
          <td><%=RS("DATE_ORDERED")%></td>
          <td>&nbsp;<%=RS("LAST_UPDATED")%></td>
          <td>
          <%
          Notes_display=""
          strSQLData = "select Notes,LABCORP_TEST_NAME,LABCORP_TEST_NUMBER,status,ORCNUMBER from LABCORP_ORDER_DATA where ORDER_ID="&RS("ORDER_ID")&" order by LABCORP_TEST_NAME "
          'Response.Write strSQLData & "<BR>"
          RS1.Open strSQLData,CONN
          while not rs1.EOF
          %>
			<%if test_status="Ready" then%>
					
					<a href="../../labquestresult2/displaypdf.aspx?order_id=<%=RS("ORDER_ID")%>&amp;orcnumber=<%=RS1("ORCNUMBER")%>"><%=RS1("LABCORP_TEST_NAME")%></a><br>
					
			<%End if%>	
			
			<%If test_status="Sent" then %>          
				<%If RS("transaction_id") <> "" then %>       
					<a href="#" onclick="popScrollWindowMax('print_labquest_label.asp?order_id=<%=RS("ORDER_ID")%>','100','50')">
						<img src="/images/global/labels-icon.gif" alt="Print Label" border="0" /> 
					</a>   
					<a href="#" onclick="popScrollWindowMax('print_LabQuest.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"><%=RS1("LABCORP_TEST_NAME")%></a><br>
				<%else%>
					<a href="#" onclick="popScrollWindowMax('print_LabTest.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"><%=RS1("LABCORP_TEST_NAME")%></a><br>
				<%end if%>
			<%end if%>
			
			<%If test_status="Error" then %>          
				<%If RS("transaction_id") <> "" then %> 
					<a href="#" onclick="popScrollWindowMax('print_LabQuest.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"><%=RS1("LABCORP_TEST_NAME")%></a><br>
				<%else%>
					<a href="#" onclick="popScrollWindowMax('print_LabTest.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"><%=RS1("LABCORP_TEST_NAME")%></a><br>
				<%end if%>
			<%end if%>
			
			<%If test_status="Result Error" then %>          
				<%If RS("transaction_id") <> "" then %> 
					<a href="#" onclick="popScrollWindowMax('print_LabQuest.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"><%=RS1("LABCORP_TEST_NAME")%></a><br>
				<%else%>
					<a href="#" onclick="popScrollWindowMax('print_LabTest.asp?order_id=<%=RS("ORDER_ID")%>','700','500')"><%=RS1("LABCORP_TEST_NAME")%></a><br>
				<%end if%>
			<%end if%>			
			<%If test_status="Result_PatientID_notfound" then %>        
				
				<!--	<a href="../../labquestresult2/displaypdf.aspx?order_id=<%=RS("ORDER_ID")%>" onclick="popScrollWindowMax('../../labquestresult2/displaypdf.aspx?order_id=<%=RS("ORDER_ID")%>','700','500')"><font color="<%=font_color%>"><%=RS1("LABCORP_TEST_NAME")%></font></a><br>-->
				<a href="../../labquestresult2/displaypdf.aspx?order_id=<%=RS("ORDER_ID")%>&amp;orcnumber=<%=RS1("ORCNUMBER")%>"><%=RS1("LABCORP_TEST_NAME")%></a><br>
				
			<%end if%>		
			
			<%
				i = INSTR(RS1("Notes"),"#")
				if (i>0) then
					notes = split(RS1("Notes"),"#")		
					for i=0 to ubound(notes)
						Notes_display = Notes_display & mid(notes(i),2,len(notes(i)))
					next
				else 
					Notes_display = Notes_display + RS1("Notes")
				end if
				Notes_display = Notes_display & "<BR>"
				RS1.MoveNext
				wend
				rs1.close
				If test_status="Result_PatientID_notfound" then
				If RS("transaction_id") <> "" then%>
					<a href="#" onclick="popScrollWindowMax('/practice/notes/display/labquest_patient_search.asp?order_id=<%=RS("ORDER_ID")%>&amp;patient_id=<%=Mid(RS("useridval"),1,5)%>','1000','600')"><font color="<%=font_color%>">Search Patient</font></a><br>	
				<%end if
			end if
			%>
          </td>       
          <td>
          <%=RS("BILL_TYPE")%>
          </td>
          <td>
			<%=Notes_display%> &nbsp;			
		   </td>
           <td><%=RS("Status")%></td>
        </tr>
<%
	RS.movenext
	wend
%>
<%else
	Response.Write "<tr><td>No records found</td></tr>"
end if

End if
%>
 </table>
    </td>
  </tr>
</table>
</form>
<br><br>

</body>
</html>