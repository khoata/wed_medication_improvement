<%if (session("user") = "") then
	session("user") = "Patient Login " & UCase(Request("patient_id"))
end if%>
<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->


<%
set RS = Server.CreateObject("ADODB.RecordSet")
set RS1 = Server.CreateObject("ADODB.RecordSet")
set CMD = Server.CreateObject("ADODB.Command")

PATIENT_ID  =trim(request("PATIENT_ID"))
VISIT_KEY	=trim(request("VISIT_KEY"))
test_type=trim(request("test_type"))

'Response.Write PATIENT_ID&"<br>"
'Response.Write VISIT_KEY

RS.open "select * from RTE_ECHO where test_type='"&test_type&"'and patient_id='"&PATIENT_ID&"' and visit_key= '"&visit_key&"'",CONN
if not (rs.EOF or rs.BOF) then
    action_type="UPDATE"
    PAT_NAME		=RS("PAT_NAME")
	MRN				=RS("MRN")
	STUDY_DATE		=RS("STUDY_DATE")
	DOB				=RS("DOB")
	GENDER			=RS("GENDER")
	HEIGHT			=RS("HEIGHT")
	WEIGHT			=RS("WEIGHT")
	BSA				=RS("BSA")
	DIAGNOSIS		=RS("DIAGNOSIS")
	TECHNICIAN		=RS("TECHNICIAN")
	REASON			=RS("REASON")
	LVESD			=RS("LVESD")
	LVEDD			=RS("LVEDD")
	IVS				=RS("IVS")
	PW				=RS("PW")
	RV_D			=RS("RV_D")
	LA				=RS("LA")
	AO				=RS("AO")
	RVSP			=RS("RVSP")
	LVEF			=RS("LVEF")
	STRESS_LVEF	    =RS("STRESS_LVEF")
	IMPRESSIONS		= RS("IMPRESSIONS")

	TECH_QUALITY	=RS("TECH_QUALITY")
	RV_FUNCTION		=RS("RV_FUNCTION")
	RV_Text			=RS("RV_Text")
	if RV_Text<>"" then RV_FUNCTION= RV_FUNCTION&","&RV_Text
	LV_FUNCTION		=RS("LV_FUNCTION")
	LV_Text			 =RS("LV_Text")
	if LV_Text<>"" then LV_FUNCTION= LV_FUNCTION&","&LV_Text

	PERI_EFF		=RS("PERI_EFF") &" "&RS("PERI_TEXT")
	PLEU_EFF		=RS("PLEU_EFF") &" "&RS("PLEU_TEXT")
	INTR_MASS		=RS("INTR_MASS")
	INTR_MASS_TEXT	=RS("INTR_MASS_TEXT")
	INTR_THRO		=RS("INTR_THRO") &" "&RS("INTR_THRO_TEXT")
	SPON_ECHO		=RS("SPON_ECHO") &" "&RS("SPON_ECHO_TEXT")
	VALV_VEG		=RS("VALV_VEG") &" "&RS("VALV_VEG_TEXT")
	MITR_PRO		=RS("MITR_PRO") &" "&RS("MITR_PRO_TEXT")
	VALVE_STEN		=RS("VALVE_STEN") &" "&RS("VALVE_STEN_TEXT")
	PFO				=RS("PFO")  &" "&RS("PFO_TEXT")
	ASD				=RS("ASD") &" "&RS("ASD_TEXT")
	VSD				=RS("VSD") &" "&RS("VSD_TEXT")
	MITRAL_TEXT		=RS("MITRAL_TEXT")
	Tricuspid_TEXT	=RS("Tricuspid_TEXT")
	Plumonary_TEXT	=RS("Plumonary_TEXT")
	Aortic_TEXT		=RS("Aortic_TEXT")
	
	'----resting echo - start
	NORMAL			=RS("NORMAL")
	If NORMAL <> "" then
		if Instr(NORMAL,"IVSeptum") <> 0 then IVSeptum=IVSeptum&",Normal"
		if Instr(NORMAL,"Anterior") <> 0 then Anterior=Anterior&",Normal"
		if Instr(NORMAL,"Apex") <> 0 then Apex=Apex&",Normal"
		if Instr(NORMAL,"Lateral") <> 0 then Lateral=Lateral&",Normal"
		if Instr(NORMAL,"Inferior") <> 0 then Inferior=Inferior&",Normal"
		if Instr(NORMAL,"Posterior") <> 0 then Posterior=Posterior&",Normal"
		if Instr(NORMAL,"Global") <> 0 then Global_r=Global_r&",Normal"
	end if

	HYPOK			=RS("HYPOK")
	If HYPOK <> "" then
		if Instr(HYPOK,"IVSeptum") <> 0 then IVSeptum=IVSeptum&",Hypokinetic"
		if Instr(HYPOK,"Anterior") <> 0 then Anterior=Anterior&",Hypokinetic"
		if Instr(HYPOK,"Apex") <> 0 then Apex=Apex&",Hypokinetic"
		if Instr(HYPOK,"Lateral") <> 0 then Lateral=Lateral&",Hypokinetic"
		if Instr(HYPOK,"Inferior") <> 0 then Inferior=Inferior&",Hypokinetic"
		if Instr(HYPOK,"Posterior") <> 0 then Posterior=Posterior&",Hypokinetic"
		if Instr(HYPOK,"Global") <> 0 then Global_r=Global_r&",Hypokinetic"
	end if
	AKINETIC			=RS("AKINETIC")
	If AKINETIC <> "" then
		if Instr(AKINETIC,"IVSeptum") <> 0 then IVSeptum=IVSeptum&",Akinetic"
		if Instr(AKINETIC,"Anterior") <> 0 then Anterior=Anterior&",Akinetic"
		if Instr(AKINETIC,"Apex") <> 0 then Apex=Apex&",Akinetic"
		if Instr(AKINETIC,"Lateral") <> 0 then Lateral=Lateral&",Akinetic"
		if Instr(AKINETIC,"Inferior") <> 0 then Inferior=Inferior&",Akinetic"
		if Instr(AKINETIC,"Posterior") <> 0 then Posterior=Posterior&",Akinetic"
		if Instr(AKINETIC,"Global") <> 0 then Global_r=Global_r&",Akinetic"
	end if
	DYSKINETIC			=RS("DYSKINETIC")
	If DYSKINETIC <> "" then
		if Instr(DYSKINETIC,"IVSeptum") <> 0 then IVSeptum=IVSeptum&",Dyskinetic"
		if Instr(DYSKINETIC,"Anterior") <> 0 then Anterior=Anterior&",Dyskinetic"
		if Instr(DYSKINETIC,"Apex") <> 0 then Apex=Apex&",Dyskinetic"
		if Instr(DYSKINETIC,"Lateral") <> 0 then Lateral=Lateral&",Dyskinetic"
		if Instr(DYSKINETIC,"Inferior") <> 0 then Inferior=Inferior&",Dyskinetic"
		if Instr(DYSKINETIC,"Posterior") <> 0 then Posterior=Posterior&",Dyskinetic"
		if Instr(DYSKINETIC,"Global") <> 0 then Global_r=Global_r&",Dyskinetic"
	end if

	If IVSeptum <>"" then IVSeptum=right(IVSeptum,len(IVSeptum)-1)
	If Anterior<>"" then Anterior=right(Anterior,len(Anterior)-1)
	If Apex<>"" then Apex=right(Apex,len(Apex)-1)
	If Lateral<>"" then Lateral=right(Lateral,len(Lateral)-1)
	If Inferior <>"" then Inferior=right(Inferior,len(Inferior)-1)
	If Posterior<>"" then Posterior=right(Posterior,len(Posterior)-1)
	If Global_r<>"" then Global_r=right(Global_r,len(Global_r)-1)
	'---end of stress echo

	STRESS_NORMAL			=RS("STRESS_NORMAL")
	If STRESS_NORMAL <> "" then
		if Instr(STRESS_NORMAL,"IVSeptum") <> 0 then s_IVSeptum=s_IVSeptum&",Normal"
		if Instr(STRESS_NORMAL,"Anterior") <> 0 then s_Anterior=s_Anterior&",Normal"
		if Instr(STRESS_NORMAL,"Apex") <> 0 then s_Apex=s_Apex&",Normal"
		if Instr(STRESS_NORMAL,"Lateral") <> 0 then s_Lateral=s_Lateral&",Normal"
		if Instr(STRESS_NORMAL,"Inferior") <> 0 then s_Inferior=s_Inferior&",Normal"
		if Instr(STRESS_NORMAL,"Posterior") <> 0 then s_Posterior=s_Posterior&",Normal"
		if Instr(STRESS_NORMAL,"Global") <> 0 then s_Global_r=s_Global_r&",Normal"
	end if

	STRESS_HYPOK			=RS("STRESS_HYPOK")
	If STRESS_HYPOK <> "" then
		if Instr(STRESS_HYPOK,"IVSeptum") <> 0 then s_IVSeptum=s_IVSeptum&",Hypokinetic"
		if Instr(STRESS_HYPOK,"Anterior") <> 0 then s_Anterior=s_Anterior&",Hypokinetic"
		if Instr(STRESS_HYPOK,"Apex") <> 0 then s_Apex=s_Apex&",Hypokinetic"
		if Instr(STRESS_HYPOK,"Lateral") <> 0 then s_Lateral=s_Lateral&",Hypokinetic"
		if Instr(STRESS_HYPOK,"Inferior") <> 0 then s_Inferior=s_Inferior&",Hypokinetic"
		if Instr(STRESS_HYPOK,"Posterior") <> 0 then s_Posterior=s_Posterior&",Hypokinetic"
		if Instr(STRESS_HYPOK,"Global") <> 0 then s_Global_r=s_Global_r&",Hypokinetic"
	end if
	STRESS_AKINETIC			=RS("STRESS_AKINETIC")
	If STRESS_AKINETIC <> "" then
		if Instr(STRESS_AKINETIC,"IVSeptum") <> 0 then s_IVSeptum=s_IVSeptum&",Akinetic"
		if Instr(STRESS_AKINETIC,"Anterior") <> 0 then s_Anterior=s_Anterior&",Akinetic"
		if Instr(STRESS_AKINETIC,"Apex") <> 0 then s_Apex=s_Apex&",Akinetic"
		if Instr(STRESS_AKINETIC,"Lateral") <> 0 then s_Lateral=s_Lateral&",Akinetic"
		if Instr(STRESS_AKINETIC,"Inferior") <> 0 then s_Inferior=s_Inferior&",Akinetic"
		if Instr(STRESS_AKINETIC,"Posterior") <> 0 then s_Posterior=s_Posterior&",Akinetic"
		if Instr(STRESS_AKINETIC,"Global") <> 0 then s_Global_r=s_Global_r&",Akinetic"
	end if
	STRESS_DYSKINETIC			=RS("STRESS_DYSKINETIC")
	If STRESS_DYSKINETIC <> "" then
		if Instr(STRESS_DYSKINETIC,"IVSeptum") <> 0 then s_IVSeptum=s_IVSeptum&",Dyskinetic"
		if Instr(STRESS_DYSKINETIC,"Anterior") <> 0 then s_Anterior=s_Anterior&",Dyskinetic"
		if Instr(STRESS_DYSKINETIC,"Apex") <> 0 then s_Apex=s_Apex&",Dyskinetic"
		if Instr(STRESS_DYSKINETIC,"Lateral") <> 0 then s_Lateral=s_Lateral&",Dyskinetic"
		if Instr(STRESS_DYSKINETIC,"Inferior") <> 0 then s_Inferior=s_Inferior&",Dyskinetic"
		if Instr(STRESS_DYSKINETIC,"Posterior") <> 0 then s_Posterior=s_Posterior&",Dyskinetic"
		if Instr(STRESS_DYSKINETIC,"Global") <> 0 then s_Global_r=s_Global_r&",Dyskinetic"
	end if

	If s_IVSeptum<>"" then s_IVSeptum=right(s_IVSeptum,len(s_IVSeptum)-1)
	If s_Anterior<>"" then s_Anterior=right(s_Anterior,len(s_Anterior)-1)
	if s_Apex <>"" then s_Apex=right(s_Apex,len(s_Apex)-1)
	If s_Lateral<>"" then s_Lateral=right(s_Lateral,len(s_Lateral)-1)
	If s_Inferior <>"" then s_Inferior=right(s_Inferior,len(s_Inferior)-1)
	If s_Posterior<>"" then s_Posterior=right(s_Posterior,len(s_Posterior)-1)
	If s_Global_r<>"" then s_Global_r=right(s_Global_r,len(s_Global_r)-1)





	RS1.open "select * from RTE_DOPPLER where DOPPLER='VMax' and patient_id='"&PATIENT_ID&"' and visit_key= '"&visit_key&"'",CONN
	MITRAL1	=rs1("MITRAL")
	AORTIC1	=rs1("AORTIC")
	LVOT1	=rs1("LVOT")
	PULMONIC1=rs1("PULMONIC")
	TRICUSPID1=rs1("TRICUSPID")
	RS1.Close

	RS1.open "select * from RTE_DOPPLER where DOPPLER='Peak' and patient_id='"&PATIENT_ID&"' and visit_key= '"&visit_key&"'",CONN
	MITRAL2	=rs1("MITRAL")
	AORTIC2	=rs1("AORTIC")
	LVOT2	=rs1("LVOT")
	PULMONIC2=rs1("PULMONIC")
	TRICUSPID2=rs1("TRICUSPID")
	RS1.Close

	RS1.open "select * from RTE_DOPPLER where DOPPLER='Mean' and patient_id='"&PATIENT_ID&"' and visit_key= '"&visit_key&"'",CONN
	MITRAL3	=rs1("MITRAL")
	AORTIC3	=rs1("AORTIC")
	LVOT3	=rs1("LVOT")
	PULMONIC3=rs1("PULMONIC")
	TRICUSPID3=rs1("TRICUSPID")
	RS1.Close

	RS1.open "select * from RTE_DOPPLER where DOPPLER='Valve' and patient_id='"&PATIENT_ID&"' and visit_key= '"&visit_key&"'",CONN
	MITRAL4	=rs1("MITRAL")
	AORTIC4	=rs1("AORTIC")
	LVOT4	=rs1("LVOT")
	PULMONIC4=rs1("PULMONIC")
	TRICUSPID4=rs1("TRICUSPID")
	RS1.Close

	RS1.open "select * from RTE_DOPPLER where DOPPLER='Insufficiency' and patient_id='"&PATIENT_ID&"' and visit_key= '"&visit_key&"'",CONN
	MITRAL5	=rs1("MITRAL")
	AORTIC5	=rs1("AORTIC")
	LVOT5	=rs1("LVOT")
	PULMONIC5=rs1("PULMONIC")
	TRICUSPID5=rs1("TRICUSPID")
	RS1.Close

	RS1.open "select * from RTE_DOPPLER where DOPPLER='Time' and patient_id='"&PATIENT_ID&"' and visit_key= '"&visit_key&"'",CONN
	MITRAL6	=rs1("MITRAL")
	AORTIC6	=rs1("AORTIC")
	LVOT6	=rs1("LVOT")
	PULMONIC6=rs1("PULMONIC")
	TRICUSPID6=rs1("TRICUSPID")
	RS1.Close

	RS1.open "select * from RTE_DOPPLER where DOPPLER='Pattern' and patient_id='"&PATIENT_ID&"' and visit_key= '"&visit_key&"'",CONN
	MITRAL7	=rs1("MITRAL")
	AORTIC7	=rs1("AORTIC")
	LVOT7	=rs1("LVOT")
	PULMONIC7=rs1("PULMONIC")
	TRICUSPID7=rs1("TRICUSPID")
	RS1.Close

Rs.Close
end if

'doctor_id= "jsherman"
doctor_id = session("user")
strSQL="select b.orgname,a.first_name,a.last_name,a.middle_name,a.credential,a.street,a.city,a.state,a.zip,a.wphone,a.ext,a.fax,a.email,a.provider_licno,a.provider_licnostate,a.dea_no,a.speciality from  doctortable a,streetreg b where a.FACILITY  = b.streetadd and a.docuser='"& doctor_id &"'"

'Response.Write strSQL


rs.open strSQL,CONN

doc_name=rs("first_name")&" "&rs("last_name")
credential = rs("credential")
org_name=rs("orgname")

If test_type="RESTING" then
	report_header="Resting Echocardiogram Report"
elseif test_type="STRESS" then
	report_header="Stress Echocardiogram Report"
end if
%>

<html>

<head>
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Print Report</title>
<style>
<!--
 p.MsoNormal
	{mso-style-parent:"";
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";
	margin-left:0in; margin-right:0in; margin-top:0in}
 table.MsoNormalTable
	{mso-style-parent:"";
	font-size:10.0pt;
	font-family:"Times New Roman"}
-->
</style>
</head>

<body onload="window.print();">

<p class="MsoNormal" style="margin-left:1.0in" align="center"><b>
<span style="font-size:14.0pt;font-family:Arial"><%'=org_name%></span></b></p>
<div style="border:none;border-bottom:solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .75pt;padding:0in 0in 1.0pt 0in">
  <p class="MsoNormal" align="center" style="text-align: center; border: medium none; padding: 0in">
  <b><span style="font-family:Arial"><%=report_header%></span></b></div>
<p class="MsoNormal"><b><span style="font-family:Arial">&nbsp;</span></b></p>

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="613" style="width: 459.8pt; border-collapse: collapse; border: medium none; margin-left: 5.4pt" height="47">
  <tr style="height: 16.5pt">
    <td width="227" colspan="2" valign="top" style="width: 170.0pt; height: 12; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <b><font size="2">Patient Name:</font></b></span><span style="font-size:8.0pt;font-family:Arial"><font size="2"><%=PAT_NAME%></font></span></td>
    <td width="193" valign="top" style="width: 144.5pt; height: 12; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">MRN:<%=MRN%>
    </font>
    </span></td>
    <td width="194" valign="top" style="width: 145.3pt; height: 12; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Study
    Date:<%=STUDY_DATE%> </font> </span></td>
  </tr>
  <tr style="height: 7.9pt">
    <td width="113" valign="top" style="width: 85.0pt; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">DOB:<%=DOB%>
    </font>
    </span></td>
    <td width="113" valign="top" style="width: 85.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Gender:<%=GENDER%> </font> </span></td>
    <td width="193" valign="top" style="width: 144.5pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Ht:<%=HEIGHT%>
    </font>
    </span></td>
    <td width="194" valign="top" style="width: 145.3pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" style="margin-left:2.1pt">
    <span style="font-family:Arial"><font size="2">BSA:<%=BSA%> </font> </span></td>
  </tr>
  <tr style="height: 13.65pt">
    <td width="227" colspan="2" valign="top" style="width: 459.8pt; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Diagnosis:<%=DIAGNOSIS%> </font> </span></td>
    <td width="386" colspan="2" valign="top" style="width: 459.8pt; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Indication For Study:<%=REASON%> </font> </span></td>    
  </tr>
  <tr style="height: 8.15pt">
    <td width="227" colspan="2" valign="top" style="width: 170.0pt; height: 4; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Technician: <%=TECHNICIAN%></font></span></td>
    <td width="386" colspan="2" valign="top" style="width: 289.8pt; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Technical Quality: <%=TECH_QUALITY%></font></span></td>
  </tr>
  </table>
<p class="MsoNormal"><b><u>
<span style="text-decoration: none; font-size: 10.0pt; font-family: Arial">&nbsp;</span></u></b></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="612" style="width: 459.0pt; border-collapse: collapse; border: medium none; margin-left: 5.4pt" height="23">
  <tr style="height: 14.25pt">
    <td width="144" valign="top" style="width: 1.5in; height: 11; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><b><span style="font-family:Arial">
    <font size="2">Findings:</font></span></b></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 11; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">&nbsp;</font></span></td>
  </tr>
  <tr style="height: 8.25pt">
    <td width="144" valign="top" style="width: 1.5in; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">RV
    Systolic Function:</font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=RV_FUNCTION%></font></span></td>
  </tr>
  <tr style="height: 13.0pt">
    <td width="144" valign="top" style="width: 1.5in; height: 7; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">LV Systolic Function: </font></span></td>
    <td width="468" valign="top" style="width: 351.0pt; height: 7; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=LV_FUNCTION%></font></span></td>
  </tr>
</table>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">&nbsp;</span></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="613" style="width: 459.75pt; border-collapse: collapse; border: medium none; margin-left: 5.4pt" height="1">
  <tr style="height: 16.5pt">
    <td width="114" valign="top" style="width: 85.15pt; height: 10; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <b><font size="2">LV Wall Motion:</font></b></span></td>
    <td valign="top" style="width: 204; height: 10; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in" align="center">
    <p class="MsoNormal"><span style="font-family:Arial; font-weight:700">
    <font size="2">
    <% if test_type="STRESS" then %> &nbsp;Resting</font></span><font size="2" face="Arial">
    <% end if%> </font>
    </td>
    <td valign="top" style="width: 278; height: 10; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in" align="center">
		<span style="font-family: Arial; font-weight: 700">
    <% if test_type="STRESS" then %>
    <font size="2">Stress</font></span><font face="Arial">
    <% end if%> </font>
    </td>
  </tr>
  <tr style="height: 11.05pt">
    <td width="114" valign="top" style="width: 85.15pt; height: 3; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">IVSeptum</font></span></td>
    <td valign="top" style="width: 204; height: 3; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=IVSeptum%></font></span></td>
    <td valign="top" style="width: 278; height: 3; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <font face="Arial" size="2"><%=s_IVSeptum%></font>&nbsp;</td>
  </tr>
  <tr style="height: 7.35pt">
    <td width="114" valign="top" style="width: 85.15pt; height: 2; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Anterior</font></span></td>
    <td valign="top" style="width: 204; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Anterior%></font></span></td>
    <td valign="top" style="width: 278; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <font face="Arial" size="2"><%=s_Anterior%></font>&nbsp;</td>
  </tr>
  <tr style="height: 14.25pt">
    <td width="114" valign="top" style="width: 85.15pt; height: 4; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Apex </font> </span></td>
    <td valign="top" style="width: 204; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Apex%></font></span></td>
    <td valign="top" style="width: 278; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <font face="Arial" size="2"><%=s_Apex%></font>&nbsp;</td>
  </tr>
  <tr>
    <td width="114" valign="top" style="width: 85.15pt; height: 2; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Lateral</font></span></td>
    <td valign="top" style="width: 204; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Lateral%></font></span></td>
    <td valign="top" style="width: 278; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <font face="Arial" size="2"><%=s_Lateral%></font>&nbsp;</td>
  </tr>
  <tr>
    <td width="114" valign="top" style="width: 85.15pt; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Inferior</font></span></td>
    <td valign="top" style="width: 204; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Inferior%></font></span></td>
    <td valign="top" style="width: 278; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <font face="Arial" size="2"><%=s_Inferior%></font>&nbsp;</td>
  </tr>
  <tr>
    <td width="114" valign="top" style="width: 85.15pt; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Posterior</font></span></td>
    <td valign="top" style="width: 204; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Posterior%></font></span></td>
    <td valign="top" style="width: 278; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <font face="Arial" size="2"><%=s_Posterior%></font>&nbsp;</td>
  </tr>
  <tr style="height: 11.9pt">
    <td width="114" valign="top" style="width: 85.15pt; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Global</font></span></td>
    <td valign="top" style="width: 204; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Global_r%></font></span></td>
    <td valign="top" style="width: 278; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <font face="Arial" size="2"><%=s_Global_r%></font>&nbsp;</td>
  </tr>
</table>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">&nbsp;</span></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="613" style="width: 459.75pt; border-collapse: collapse; border: medium none; margin-left: 5.4pt" height="26">
  <tr style="height: 8.9pt">
    <td width="613" colspan="4" valign="top" style="width: 459.75pt; height: 5; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><b><span style="font-family:Arial">
    <font size="2">Dimensions:</font></span></b></td>
  </tr>
  <tr style="height: 15.1pt">
    <td width="136" valign="top" style="width: 102.15pt; height: 10; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">LVESD
    (2.3-3.9 cm)</font></span></td>
    <td width="148" valign="top" style="width: 110.7pt; height: 10; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=LVESD%></font></span></td>
    <td width="136" valign="top" style="width: 102.15pt; height: 10; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">LA
    (1.9-4.0 cm)</font></span></td>
    <td width="193" valign="top" style="width: 144.75pt; height: 10; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=LA%></font></span></td>
  </tr>
  <tr style="height: 10.05pt">
    <td width="136" valign="top" style="width: 102.15pt; height: 4; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">LVEDD
    (3.5-5.7 cm)</font></span></td>
    <td width="148" valign="top" style="width: 110.7pt; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=LVEDD%></font></span></td>
    <td width="136" valign="top" style="width: 102.15pt; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">AO
    (2.0-4.0 cm)</font></span></td>
    <td width="193" valign="top" style="width: 144.75pt; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=AO%></font></span></td>
  </tr>
  <tr style="height: 6.35pt">
    <td width="136" valign="top" style="width: 102.15pt; height: 2; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">IVS&nbsp;&nbsp;
    (0.6-1.1 cm) </font> </span></td>
    <td width="148" valign="top" style="width: 110.7pt; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=IVS%></font></span></td>
    <td width="136" valign="top" style="width: 102.15pt; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">RVSP
    (&lt;35 mm Hg)</font></span></td>
    <td width="193" valign="top" style="width: 144.75pt; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=RVSP%></font></span></td>
  </tr>
  
  <tr style="height: 15.95pt">
    <td width="136" valign="top" style="width: 102.15pt; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">PW&nbsp;&nbsp;
    (0.6-1.1 cm)</font></span></td>
    <td width="148" valign="top" style="width: 110.7pt; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=PW%></font></span></td>

    <% if test_type="STRESS" then %>
    <td width="150"  valign="top" style="width: 102.15pt; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Rest LVEF:&nbsp;<%=LVEF%></font></span></td>
    <td width="170" valign="top" style="width: 144.75pt; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
	<font size="2">Stress LVEF:&nbsp; <%=STRESS_LVEF%></font></span>
	</td>
	<%else%>
	<td width="150"  valign="top" style="width: 102.15pt; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Rest LVEF:</font></span></td>
    <td width="170" valign="top" style="width: 144.75pt; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
	<font size="2">&nbsp;<%=LVEF%></font></span></td>
    <% end if%>
    
    <tr style="height: 6.35pt">
    <td width="136" valign="top" style="width: 102.15pt; height: 2; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">RV</font> </span></td>
    <td width="148" valign="top" style="width: 110.7pt; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=RV_D%></font></span></td>
    <td width="136" valign="top" style="width: 102.15pt; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">&nbsp; </font></span></td>
    <td width="193" valign="top" style="width: 144.75pt; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">&nbsp; </font></span></td>
  </tr>
    

  </tr>
</table>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">&nbsp;</span></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="width: 613; border-collapse: collapse; border: medium none; margin-left: 5.4pt" height="10">
  <tr style="height: 11.85pt">
    <td valign="top" style="width: 358; height: 6; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Pericardial Effusion: </font> </span></td>
    <td valign="top" style="width: 231; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=PERI_EFF%></font></span></td>

	<td valign="top" style="width: 324; height: 6; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Mitral Valve Prolapse:</font></span></td>
    <td valign="top" style="width: 313; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=MITR_PRO%></font></span></td>


  </tr>

  <tr style="height: 8.45pt">
    <td valign="top" style="width: 358; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Pleural Effusion: </font> </span></td>
    <td valign="top" style="width: 231; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=PLEU_EFF%></font></span></td>

	<td valign="top" style="width: 324; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Valve
    Stenosis: </font> </span></td>
    <td valign="top" style="width: 313; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=VALVE_STEN%></font></span></td>


  </tr>
  <tr style="height: 15.25pt">
    <td valign="top" style="width: 358; height: 2; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Intracardiac Mass: </font> </span></td>
    <td valign="top" style="width: 231; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=INTR_MASS%>&nbsp;&nbsp;&nbsp;&nbsp;<%=INTR_MASS_Text%></font></span></td>

	<td valign="top" style="width: 324; height: 2; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">PFO:
    </font> </span></td>
    <td valign="top" style="width: 313; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=PFO%></font></span></td>

  </tr>
  <tr style="height: 9.55pt">
    <td valign="top" style="width: 358; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Intracardiac Thrombus: </font> </span></td>
    <td valign="top" style="width: 231; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=INTR_THRO%></font></span></td>

	<td valign="top" style="width: 324; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">ASD:
    </font> </span></td>
    <td valign="top" style="width: 313; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=ASD%></font></span></td>

  </tr>
  <tr style="height: 6.05pt">
    <td valign="top" style="width: 358; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Spontaneous Echo Contrast: </font> </span></td>
    <td valign="top" style="width: 231; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=SPON_ECHO%></font></span></td>

  <td valign="top" style="width: 324; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">VSD:
    </font> </span></td>
    <td valign="top" style="width: 313; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=VSD%></font></span></td>


  </tr>
  
   <tr style="height: 9.3pt">
    <td valign="top" style="width: 358; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Valvular Vegetation: </font> </span></td>
    <td valign="top" style="width: 231; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=VALV_VEG%></font></span></td>

  <td valign="top" style="width: 324; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal">&nbsp;</td>
    <td valign="top" style="width: 313; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal">&nbsp;</td>

  </tr> 
  
  
  <tr style="height: 6.05pt">
    <td valign="top" style="width: 358; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Mitral Value: </font> </span></td>
    <td valign="top" style="width: 231; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=MITRAL_TEXT%></font></span></td>

  <td valign="top" style="width: 324; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Tricuspid Value:
    </font> </span></td>
    <td valign="top" style="width: 313; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Tricuspid_TEXT%></font></span></td>


  </tr>
  
  <tr style="height: 6.05pt">
    <td valign="top" style="width: 358; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Plumonary Value: </font> </span></td>
    <td valign="top" style="width: 231; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Plumonary_TEXT%></font></span></td>

  <td valign="top" style="width: 324; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Aortic Value:
    </font> </span></td>
    <td valign="top" style="width: 313; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2"><%=Aortic_TEXT%></font></span></td>


  </tr>
</table>

<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">&nbsp;</span></p>
<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="613" style="width: 459.75pt; border-collapse: collapse; border: medium none; margin-left: 5.4pt" height="48">
  <tr style="height: 11.35pt">
    <td width="212" valign="top" style="width: 158.9pt; height: 13; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><b><span style="font-family:Arial">
    <font size="2">Doppler:</font></span></b></td>
    <td valign="top" style="width: 80; height: 13; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><b><span style="font-family:Arial">
    <font size="2">Mitral</font></span></b></td>
    <td valign="top" style="width: 80; height: 13; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><b><span style="font-family:Arial">
    <font size="2">Aortic</font></span></b></td>
    <td valign="top" style="width: 81; height: 13; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><b><span style="font-family:Arial">
    <font size="2">LVOT</font></span></b></td>
    <td valign="top" style="width: 81; height: 13; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><b><span style="font-family:Arial">
    <font size="2">Pulmonic</font></span></b></td>
    <td valign="top" style="width: 81; height: 13; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><b><span style="font-family:Arial">
    <font size="2">Tricuspid </font> </span></b></td>
  </tr>
  <tr style="height: 8.7pt">
    <td width="212" valign="top" style="width: 158.9pt; height: 3; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">VMax&nbsp;&nbsp;
    (m/s)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </font> </span></td>
    <td valign="top" style="width: 80; height: 3; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=MITRAL1%></font></span></td>
    <td valign="top" style="width: 80; height: 3; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=AORTIC1%></font></span></td>
    <td valign="top" style="width: 81; height: 3; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=LVOT1%></font></span></td>
    <td valign="top" style="width: 81; height: 3; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=PULMONIC1%></font></span></td>
    <td valign="top" style="width: 81; height: 3; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=TRICUSPID1%></font></span></td>
  </tr>
  <tr style="height: 15.05pt">
    <td width="212" valign="top" style="width: 158.9pt; height: 6; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Peak
    Gradient (mm Hg)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </font> </span></td>
    <td valign="top" style="width: 80; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=MITRAL2%></font></span></td>
    <td valign="top" style="width: 80; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=AORTIC2%></font></span></td>
    <td valign="top" style="width: 81; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=LVOT2%></font></span></td>
    <td valign="top" style="width: 81; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=PULMONIC2%></font></span></td>
    <td valign="top" style="width: 81; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=TRICUSPID2%></font></span></td>
  </tr>
  <tr style="height: 8.95pt">
    <td width="212" valign="top" style="width: 158.9pt; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Mean
    Gradient (mm Hg)&nbsp;&nbsp;&nbsp;&nbsp; </font> </span></td>
    <td valign="top" style="width: 80; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=MITRAL3%></font></span></td>
    <td valign="top" style="width: 80; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=AORTIC3%></font></span></td>
    <td valign="top" style="width: 81; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=LVOT3%></font></span></td>
    <td valign="top" style="width: 81; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=PULMONIC3%></font></span></td>
    <td valign="top" style="width: 81; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=TRICUSPID3%></font></span></td>
  </tr>
  <tr style="height: 9.55pt">
    <td width="212" valign="top" style="width: 158.9pt; height: 1; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">Est.
    Valve Area (cm2)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </font> </span></td>
    <td valign="top" style="width: 80; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=MITRAL4%></font></span></td>
    <td valign="top" style="width: 80; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=AORTIC4%></font></span></td>
    <td valign="top" style="width: 81; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=LVOT4%></font></span></td>
    <td valign="top" style="width: 81; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=PULMONIC4%></font></span></td>
    <td valign="top" style="width: 81; height: 1; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=TRICUSPID4%></font></span></td>
  </tr>
  <tr style="height: 15.85pt">
    <td width="212" valign="top" style="width: 158.9pt; height: 4; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial">
    <font size="2">Insufficiency(1+,2+,3+,4+)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </font> </span></td>
    <td valign="top" style="width: 80; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=MITRAL5%></font></span></td>
    <td valign="top" style="width: 80; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=AORTIC5%></font></span></td>
    <td valign="top" style="width: 81; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=LVOT5%></font></span></td>
    <td valign="top" style="width: 81; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=PULMONIC5%></font></span></td>
    <td valign="top" style="width: 81; height: 4; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=TRICUSPID5%></font></span></td>
  </tr>
  <tr style="height: 15.15pt">
    <td width="212" valign="top" style="width: 158.9pt; height: 6; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">P1/2
    Time (msec)</font></span></td>
    <td valign="top" style="width: 80; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=MITRAL6%></font></span></td>
    <td valign="top" style="width: 80; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=AORTIC6%></font></span></td>
    <td valign="top" style="width: 81; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=LVOT6%></font></span></td>
    <td valign="top" style="width: 81; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=PULMONIC6%></font></span></td>
    <td valign="top" style="width: 81; height: 6; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=TRICUSPID6%></font></span></td>
  </tr>
  <tr style="height: 11.1pt">
    <td width="212" valign="top" style="width: 158.9pt; height: 2; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal"><span style="font-family:Arial"><font size="2">E:A Ratio/Diastolic
    Filling Pattern</font></span></td>
    <td valign="top" style="width: 80; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=MITRAL7%></font></span></td>
    <td valign="top" style="width: 80; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=AORTIC7%></font></span></td>
    <td valign="top" style="width: 81; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=LVOT7%></font></span></td>
    <td valign="top" style="width: 81; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=PULMONIC7%></font></span></td>
    <td valign="top" style="width: 81; height: 2; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0in; padding-bottom: 0in">
    <p class="MsoNormal" align="center"><span style="font-family:Arial">
    <font size="2"><%=TRICUSPID7%></font></span></td>
  </tr>
</table>
<p class="MsoNormal"><span style="font-size:8.0pt;font-family:Arial">&nbsp;</span></p>
<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:Arial">
Impressions/Conclusions:</b>&nbsp;<%=IMPRESSIONS%></span></p>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">&nbsp;</span></p>
<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></p>

<p class="MsoNormal"><span style="font-size:10.0pt;font-family:Arial">&nbsp;</span></p>


<!--
<%session("authentication") = "1"%>
<p class="MsoNormal" style="margin-left:3.0in">
<span style="font-size:10.0pt;
font-family:Arial">
<img src="/practice/customization/signature/display_signature.asp">
</span></p>-->

<p class="MsoNormal" style="margin-left:3.0in">
<span style="font-size:10.0pt;font-family:Arial">&nbsp;<%=org_name%></span></p>

</body>

</html>