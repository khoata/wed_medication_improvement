<!--#include file="../../library/functions.inc.asp"-->
<!--#include file="../../library/connection.inc.asp"-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script>
    function Open_Fax_Window() {
        popScrollWindow('/practice/hub/RefreshSessionState.asp', 850, 700);
    }
</script>
</head>
<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
<table cellspacing="0" cellpadding="0" border="0" width="100%" bgcolor="#6699FF">
	<tr>
		<td height="25" style="color: #ffffff">&nbsp;&nbsp;<b>
			<a onmouseover="window.status='View Diagnostic Reports'; return true;" onmouseout="window.status=''; return true;" href="diagtest_report.asp"  target="bodyFrame" class="nav">Diag Report</a>
				| <a onmouseover="window.status='View Labs Ordered for Labcorp'; return true;" onmouseout="window.status=''; return true;" href="labcorp_report.asp?ft=1"  target="bodyFrame" class="nav">Labcorp</a>
			
				| <a onmouseover="window.status='View Labs Ordered for Quest'; return true;" onmouseout="window.status=''; return true;" href="labquest_report.asp?ft=1"  target="bodyFrame" class="nav">Quest</a>
			<%if (session("pid") = "CEN1275") then%>
			| <a onmouseover="window.status='View Labs Ordered for Millennium'; return true;" onmouseout="window.status=''; return true;" href="/MillenniumLabOrder/Millennium_Report.aspx?facility=<%=session("pid")%>"  target="bodyFrame" class="nav"><b>Millennium</b></a>
            <%end if%>
			
			<% if (session("pid") = "KEN1000" or session("pid") = "VIR1232" or session("pid") = "ADV1250" or session("pid") = "THU1206")then %><B> 
			| <a onmouseover="window.status='View other lab orders'; return true;" onmouseout="window.status=''; return true;" href="path_lab.asp"  target="bodyFrame" class="nav">Other Labs</a></b> 
			<% end if %>
			<%if (Session("pid") = "MAG1179") then%>
            | <a onmouseover="window.status='View Labs Ordered for Lab NorthShore'; return true;" onmouseout="window.status=''; return true;" href="/practice/notes/display/NorthShoreLab/NorthShore_Report.asp?ft=1"  target="bodyFrame" class="nav">NorthShore</a>
            <%end if%>
			| <a onmouseover="window.status='View elcetronic prescritions'; return true;" onmouseout="window.status=''; return true;" href="rx_report.asp?ft=1"  target="bodyFrame" class="nav">Rx Hub</a>
			| <a onmouseover="window.status='Work with CCR'; return true;" onmouseout="window.status=''; return true;" href="https://www.webedoctor.com/CCR/CCR_Hub.aspx?FacilityID=<%=session("pid")%>" target="bodyFrame" class="nav">CCR/CCD</a></b>
			| <a onmouseover="window.status='HL7 Hub'; return true;" onmouseout="window.status=''; return true;" href="hl7report.asp" target="bodyFrame" class="nav"><b>HL7</a></b>
			| <b><a onmouseover="window.status='HL7 Hub'; return true;" onmouseout="window.status=''; return true;" href="https://www.webedoctor2.com/ImportLabResults/Import.aspx?pid=<%=session("pid")%>" target="bodyFrame" class="nav">Import Lab Results</a></b>
            <%if (Session("pid") = "MAG1179") then%>			
            |<b> <a onmouseover="window.status='Export Immunization Records'; return true;" onmouseout="window.status=''; return true;" href="https://www.webedoctor.com/GenerateImmunizationHL7/GenerateBatchHL7Immunization.aspx" target="bodyFrame" class="nav">Export Immunization Records</a></b>
            <%end if%>

			<%if (Session("user") = "kenxphy") then%>
			<!--	| <a onmouseover="window.status='Usernames'; return true;" onmouseout="window.status=''; return true;" href="list_username.asp" target="bodyFrame" class="nav"><b>USERNAMES</a></b>-->
			<%end if%>	
			
                                    <%
                   dim status1
                   status = "" %>

                                               <% set RS9 = Server.CreateObject("ADODB.Recordset")
                            RS9.Open "SELECT ACC_ID  FROM FaxSending_Rights WHERE ACC_ID = '" & SQLFixUp(session("pid")) & "' ", CONN
                        %>
                   
                   <%while not RS9.EOF%>
                                                <%if RS9("ACC_ID") = session("pid") then
                                                              status1="selected"
                                                       else
                                                              status1=""
                                                       end if                                   
                                                %>                                                
                          <%    
                                                RS9.MoveNext
                                                wend
                                                set rs9=nothing                                                   
                          %>  
                                     
                   <% if status1 <> "" then %>
                     |<b> <a onmouseover="window.status='Fax Visit Note'; return true;" onmouseout="window.status=''; return true;" href="#" onclick="Open_Fax_Window();" class="nav" class="nav">Send Fax</b></a>
                   <%  end if  %>
			
			
		</td>
	</tr>
</table>
</body>
</html>
