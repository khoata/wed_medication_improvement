
<!--#include file="../../library/connOLEDB.inc.asp"-->

<%
set RS = server.createobject("adodb.recordset")

order_id=trim(Request("order_id"))

strSQL="Select * from LABCORP_ORDERS where ORDER_ID="&order_id

RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	patient_id=rs("PATIENT_ID")
	doctor_id=rs("DOCTOR_ID")
	coll_date=rs("date_ordered")
	BILL_TYPE= rs("BILL_TYPE")
	
	If BILL_TYPE="T" then 
		Bill_code="Third Party"
	elseif BILL_TYPE="P" then 
		Bill_code="Patient"
	else
		Bill_code="Client"
	end if
				
	'info1=rs("Notes")
	'labcorp_test_num=rs("LABCORP_TEST_NUMBER")
	'labcorp_test_name=rs("LABCORP_TEST_NAME")
	'cpt_code= RS("CPT_CODEs")
	'Z_SEGMENT=rs("Z_SEGMENT")
	'SOURCE_SPECIMEN=rs("SOURCE_SPECIMEN")
end if
rs.Close

strSQL="Select * from LABCORP_ORDER_DATA where ORDER_ID="&order_id
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	info1=""
	labcorp_test_info=""
	cpt_code=""	
	Z_SEGMENT=""
	SOURCE_SPECIMEN=""
	Test_cnt=0
				
	while not rs.EOF
        If (RS("ICD_1")<>"" and (InStr(ICD_code,RS("ICD_1")) = 0)) then ICD_code = ICD_code & RS("ICD_1") & ", "
		If (RS("ICD_2")<>"" and (InStr(ICD_code,RS("ICD_2")) = 0)) then ICD_code = ICD_code & RS("ICD_2") & ", " 
		If (RS("ICD_3")<>"" and (InStr(ICD_code,RS("ICD_3")) = 0)) then ICD_code = ICD_code & RS("ICD_3") & ", " 
		If (RS("ICD_4")<>"" and (InStr(ICD_code,RS("ICD_4")) = 0)) then ICD_code = ICD_code & RS("ICD_4") & ", "

		if trim(rs("Notes"))<>"" then
			info1=info1&rs("Notes")&"<br>"
		end if
		labcorp_test_info = labcorp_test_info&rs("LABCORP_TEST_NUMBER")&"-"&rs("LABCORP_TEST_NAME")&"<br>"
		cpt_code=cpt_code&RS("CPT_CODEs")&"<br>"
		If trim(rs("SOURCE_SPECIMEN"))<> trim(SOURCE_SPECIMEN) then 
			SOURCE_SPECIMEN=SOURCE_SPECIMEN&rs("SOURCE_SPECIMEN")&" "
		End if
		Z_SEGMENT=Z_SEGMENT&rs("Z_SEGMENT")&","	
		Test_cnt=Test_cnt+1
	rs.MoveNext
	wend
end if
rs.Close

labcorp_test_info =left(labcorp_test_info,len(labcorp_test_info)-4)
SOURCE_SPECIMEN=trim(SOURCE_SPECIMEN)

If Z_SEGMENT<>"" and Instr(Z_SEGMENT,"ZCI")<>0 then
	strSQL="Select * from LABCORP_ZCI where ORDER_ID="&order_id
	RS.Open strSQL,CONN
	
	WEIGHT=rs("WEIGHT")
	WEIGHT_UNITS=rs("WEIGHT_UNITS")
	URINE_VOLUME=rs("URINE_VOLUME")
	VOLUME_UNITS=rs("VOLUME_UNITS")
	FASTING=rs("FASTING")
	weight = rs("weight")
	If weight <>"" then weight= weight&" Lbs"	
	rs.Close
end if

If Z_SEGMENT<>"" and Instr(Z_SEGMENT,"ZCY")<>0 then
	strSQL="Select * from LABCORP_ZCY where ORDER_ID="&order_id
	RS.Open strSQL,CONN
	
	CERVICAL		= rs("CERVICAL")
	ENDOCERVICAL	= rs("ENDOCERVICAL")
	LABIA_VULVA		= rs("LABIA_VULVA")
	VAGINAL			= rs("VAGINAL")
	ENDOMETRIAL		= rs("ENDOMETRIAL")
	SWAB_SPATULA	= rs("SWAB_SPATULA")
	BRUSH_SPATULA	= rs("BRUSH_SPATULA")
	SPATULA_ALONE	= rs("SPATULA_ALONE")
	BRUSH_ALONE		= rs("BRUSH_ALONE")
	BROOM_ALONE		= rs("BROOM_ALONE")
	OTHER_COLLECTION= rs("OTHER_COLLECTION")
	LMP_MENO_DATE	= rs("LMP_MENO_DATE")
	PREVIOUS_TREATMENT= rs("PREVIOUS_TREATMENT")
	HYST				= rs("HYST")
	CONIZA				= rs("CONIZA")
	COLP_BX				= rs("COLP_BX")
	LASER_VAP				= rs("LASER_VAP")
	CYRO			= rs("CYRO")
	RADIATION		= rs("RADIATION")
	DATES_RESULTS	= rs("DATES_RESULTS")
	PREGNANT		= rs("PREGNANT")
	LACTATING		= rs("LACTATING")
	ORAL_CONTRACEPTIVES= rs("ORAL_CONTRACEPTIVES")
	MENOPAUSAL		= rs("MENOPAUSAL")
	ESTRO_RX		= rs("ESTRO_RX")
	PMP_BLEEDING	= rs("PMP_BLEEDING")
	POST_PART		= rs("POST_PART")
	IUD				= rs("IUD")
	ALL_OTHER_PAT	= rs("ALL_OTHER_PAT")
	NEGATIVE		= rs("NEGATIVE")
	ATYPICAL		= rs("ATYPICAL")
	DYSPLASIA		= rs("DYSPLASIA")
	CA_IN_SITU		= rs("CA_IN_SITU")
	INVASIVE		= rs("INVASIVE")
	OTHER_PREVIOUS_INFO=rs("OTHER_PREVIOUS_INFO")
	
	If CERVICAL="Y" then Gyn_Source="Cervical"
	if ENDOCERVICAL ="Y" then Gyn_Source=Gyn_Source&"/Endocervical"
	if LABIA_VULVA ="Y" then Gyn_Source=Gyn_Source&"/Labia-Vulva"
	if VAGINAL ="Y" then Gyn_Source=Gyn_Source&"/Vaginal"
	if ENDOMETRIAL ="Y" then Gyn_Source=Gyn_Source&"/Endometrial"	
	
	If SWAB_SPATULA="Y" then Coll_tech="Swab-Spatula"
	if BRUSH_SPATULA ="Y" then Coll_tech=Coll_tech&"/Brush-Spatula"
	if SPATULA_ALONE ="Y" then Coll_tech=Coll_tech&"/Spatula-Alone"
	if BRUSH_ALONE ="Y" then Coll_tech=Coll_tech&"/Brush-Alone"
	if BROOM_ALONE ="Y" then Coll_tech=Coll_tech&"/Broom-Alone"
	if OTHER_COLLECTION ="Y" then Coll_tech=Coll_tech&"/Other collection technique"
	
	
	If PREVIOUS_TREATMENT="Y" then Prv_tret = "None"
	if HYST="Y" then Prv_tret = Prv_tret&"/Hyst"
	if CONIZA="Y" then Prv_tret =Prv_tret&"/Coniza"
	if COLP_BX ="Y" then Prv_tret =Prv_tret&"/Colp-BX"
	if LASER_VAP ="Y" then Prv_tret =Prv_tret&"/Laser-Vap"
	if CYRO ="Y" then Prv_tret =Prv_tret&"/Cyro"	
	if RADIATION ="Y" then Prv_tret =Prv_tret&"/Radiation"
	if DATES_RESULTS <> "" then Prv_tret =Prv_tret&"/"&DATES_RESULTS	

	
	If NEGATIVE ="Y" then Prv_Cyto ="Negative"
	if ATYPICAL ="Y" then Prv_Cyto =Prv_Cyto&"/Atypical"	
	if DYSPLASIA ="Y" then Prv_Cyto =Prv_Cyto&"/Dysplasia"	
	if CA_IN_SITU ="Y" then Prv_Cyto =Prv_Cyto&"/Ca-In-Situ"		
	if INVASIVE ="Y" then Prv_Cyto =Prv_Cyto&"/Invasive"	
	if OTHER_PREVIOUS_INFO ="Y" then Prv_Cyto =Prv_Cyto&"/Other Previous Information"
		
	
	If PREGNANT ="Y" then Othet_Pat_info ="Pregnant"
	if LACTATING ="Y" then Othet_Pat_info = Othet_Pat_info&"/Lactating"
	if ORAL_CONTRACEPTIVES ="Y" then Othet_Pat_info = Othet_Pat_info&"/Oral Contraceptives"
	if MENOPAUSAL ="Y" then Othet_Pat_info = Othet_Pat_info&"/Menopausal"
	if ESTRO_RX ="Y" then Othet_Pat_info = Othet_Pat_info&"/Estro-RX"
	if PMP_BLEEDING ="Y" then Othet_Pat_info = Othet_Pat_info&"/PMP-Bleeding"
	if POST_PART ="Y" then Othet_Pat_info = Othet_Pat_info&"/Post-Part"
	if IUD ="Y" then Othet_Pat_info = Othet_Pat_info&"/IUD"
	if ALL_OTHER_PAT="Y" then Othet_Pat_info = Othet_Pat_info&"/All-Other-Pat"	
			
	rs.Close
end if

If Z_SEGMENT<>"" and Instr(Z_SEGMENT,"ZBL")<>0 then
	strSQL="Select * from LABCORP_ZBL where ORDER_ID="&order_id
	RS.Open strSQL,CONN	
	
	RACE= RS("RACE")
	HISPANIC=RS("HISPANIC")
	BLOOD_LEAD_TYPE=RS("BLOOD_LEAD_TYPE")
	BLOOD_LEAD_PURPOSE=RS("BLOOD_LEAD_PURPOSE")
	
	if RACE="1" then RACE="White/Caucasian"
	if RACE="2" then RACE="Black"
	if RACE="3" then RACE="American Indian"
	if RACE="4" then RACE="Asian"
	if RACE="5" then RACE="Other"
	if RACE="9" then RACE="Unknown/Not Indicated"
	
	If HISPANIC="1" then HISPANIC="Yes"
	If HISPANIC="2" then HISPANIC="No"
	If HISPANIC="3" then HISPANIC="Unknown"
	
	If BLOOD_LEAD_TYPE="U" then BLOOD_LEAD_TYPE="Urine"
	If BLOOD_LEAD_TYPE="V" then BLOOD_LEAD_TYPE="Venous"
	If BLOOD_LEAD_TYPE="F" then BLOOD_LEAD_TYPE="Finger stick"
	
	If BLOOD_LEAD_PURPOSE="I" then BLOOD_LEAD_PURPOSE="Initial"
	If BLOOD_LEAD_PURPOSE="R" then BLOOD_LEAD_PURPOSE="Repeat"
	If BLOOD_LEAD_PURPOSE="F" then BLOOD_LEAD_PURPOSE="Follow-up"
	
		
	rs.Close
end if

If Z_SEGMENT<>"" and Instr(Z_SEGMENT,"ZSA")<>0 then
	strSQL="Select * from LABCORP_ZSA where ORDER_ID="&order_id
	RS.Open strSQL,CONN	
	
	INSULIN_DEPENDENT		=RS("INSULIN_DEPENDENT")
	GESTATION_AGE_WEEKS		=RS("GESTATION_AGE_WEEKS")
	GESTATIONAL_AGE_DAYS	=RS("GESTATIONAL_AGE_DAYS")
	GESTATIONAL_AGE_DECIMAL=RS("GESTATIONAL_AGE_DECIMAL")
	GESTATIONAL_AGE_DATE	=RS("GESTATIONAL_AGE_DATE")
	METHOD_BY_LMP			=RS("METHOD_BY_LMP")
	LMP_DATE				=RS("LMP_DATE")
	METHOD_BY_ULTRASOUND	=RS("METHOD_BY_ULTRASOUND")
	ULTRASOUND_DATE			=RS("ULTRASOUND_DATE")
	METHOD_BY_EST_DOD		=RS("METHOD_BY_EST_DOD")
	EST_DOD					=RS("EST_DOD")
	TYPE_PREGNANCY			=RS("TYPE_PREGNANCY")
	ROUTINE_SCREENING		=RS("ROUTINE_SCREENING")
	NEURAL_TUBE_DEFECTS		=RS("NEURAL_TUBE_DEFECTS")
	MATERNAL_AGE			=RS("MATERNAL_AGE")
	DOWN_SYNDROME			=RS("DOWN_SYNDROME")
	CYSTIC_FIBROSIS			=RS("CYSTIC_FIBROSIS")
	OTHER_INDICATIONS		=RS("OTHER_INDICATIONS")
	AFP_INFO				=RS("AFP_INFO")
	ELEVATED				=RS("ELEVATED")
	EARLY_GA				=RS("EARLY_GA")
	HEMOLYZED				=RS("HEMOLYZED")
	
	WEIGHT					=rs("WEIGHT")	
	
	If TYPE_PREGNANCY="M" then TYPE_PREGNANCY="Multiple"
	If TYPE_PREGNANCY="S" then TYPE_PREGNANCY="Single"
	If TYPE_PREGNANCY="T" then TYPE_PREGNANCY="Twins"	
	
	set RS1 = server.createobject("adodb.recordset")
	rs1.open "select ETHNICITY from Patienttable where userid='"& patient_id &"'",CONN
	if not (rs1.eof or rs1.BOF) then
		Race=rs1("ETHNICITY")
		if RACE="C" then RACE="Caucasian"
		if RACE="B" then RACE="Black"
		if RACE="I" then RACE="American Indian or Native American"
		if RACE="A" then RACE="Asian"
		if RACE="H" then RACE="Hispanic"
		if RACE="O" then RACE="Other"
		If RACE="" then RACE="Not Indicated"
	end if
	rs1.close	
	rs.Close
end if


strSQL= "select orgname,addr1,city,state,country,zip, tel as phone from streetreg where streetadd='"&session("pid")&"'"
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	client_name=rs("orgname")
	client_Add1= rs("addr1")
	client_Add2= rs("city")&" "&rs("state")&" "&rs("zip")
	client_phone=rs("phone")	
end if
rs.Close

strSQL= "select * from Patienttable where USERID='"&patient_id&"'"
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	pat_ssn= RS("SSN")
	pat_name=RS("LAST_NAME")&" "&RS("FIRST_NAME")&" "&RS("MIDDLE_NAME")
	if ucase(RS("GENDER"))="MALE" then 
		Pat_sex="M"
	elseif ucase(RS("GENDER"))="FEMALE" then
		Pat_sex="F"
	end if
	pat_dob=RS("BIRTH_DATE")
	pat_phone=RS("HOME_PHONE")
	pat_chartid=RS("chartID")
	Pri_Ins= rs("PRIMARY_INSURANCE")
	sec_ins= rs("SECONDARY_INSURANCE")
	tri_ins= rs("TERTIARY_INSURANCE")
	pat_address=rs("STREET")&" "&rs("CITY")&" "&rs("STATE")&"-"&rs("ZIP")		
end if
rs.Close


strSQL= "select last_name||' '||first_name as doc_name,NPI,LABCORP_ACC_ID from doctortable where docuser = '"& doctor_id &"'"
RS.Open strSQL,CONN
if not (rs.EOF or rs.BOF) then
	doc_name		= RS("doc_name")
	npi				=RS("npi")
	LABCORP_ACC_ID	=RS("LABCORP_ACC_ID")	
end if
rs.Close

'findout #of pages to print.....
If Test_cnt > 3 then 
	If Z_SEGMENT <> "" and Instr(Z_SEGMENT,"ZSA") <> 0 then
		Test_ZSA="Yes"
	end if	
end if

%>

<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Print Labcorp E-Req</title>

<style>div.break {page-break-before:always}</style>

<style type="text/css">

thead { display: table-header-group; } 
tfoot { display: table-footer-group; }

</style>

</head>

<body onload="window.print();">

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1" height="695">
  <tr>
    <td width="100%" height="96">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber2" height="90">
      <tr>
        <td width="34%" height="90">&nbsp;</td>
        <td width="33%" height="90" valign="top"><font face="Arial" size="4">Labcorp<br> EREQ</font><p><font face="Arial" size="4">WEBeDoctor</font></td>
        <td width="33%" height="90">&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>

  <tr>
    <td width="100%" height="134">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber3" height="115">
      <tr>
        <td width="50%" colspan="2" height="18"><i><b><font face="Arial">Client Information:</font></b></i></td>
        <td width="50%" colspan="2" height="18"><b><i><font face="Arial">
        Physician Information:</font></i></b></td>
      </tr>
      <tr>
        <td width="18%" height="19"><b><font face="Arial" size="2">Account Number:</font></b></td>
        <td width="32%" height="19"><%=LABCORP_ACC_ID%>&nbsp;</td>
        <td width="17%" height="19"><b><font face="Arial" size="2">Physician Name:</font></b></td>
        <td width="33%" height="19"><%=doc_name%></td>
      </tr>
      <tr>
        <td width="18%" height="16"><b><font face="Arial" size="2">Name:</font></b></td>
        <td width="32%" height="16"><%=client_name%></td>
        <td width="17%" height="16"><b><font face="Arial" size="2">Physician NPI:</font></b></td>
        <td width="33%" height="16"><%=npi%></td>
      </tr>
      <tr>
        <td width="18%" height="19"><b><font face="Arial" size="2">Address:</font></b></td>
        <td width="32%" height="19"><%=client_Add1%>&nbsp;</td>
        <td width="17%" height="19"><b><font face="Arial" size="2">Physician ID:</font></b></td>
        <td width="33%" height="19"><%=doctor_id%></td>
      </tr>
      <tr>
        <td width="18%" height="19"><b><font face="Arial" size="2">City, State, Zip:</font></b></td>
        <td width="32%" height="19"><%=client_Add2%>&nbsp;</td>
        <td width="17%" height="19">&nbsp;</td>
        <td width="33%" height="19">&nbsp;</td>
      </tr>
      <tr>
        <td width="18%" height="19"><b><font face="Arial" size="2">Phone Number:</font></b></td>
        <td width="32%" height="19"><%=client_phone%>&nbsp;</td>
        <td width="17%" height="19">&nbsp;</td>
        <td width="33%" height="19">&nbsp;</td>
      </tr>
    </table>
    </td> 
  </tr>
  
  <tr>
    <td width="100%" height="149">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
      <tr>
        <td width="100%" colspan="5"><i><b><font face="Arial">Patient Information:</font></b></i></td>
      </tr>
      <tr>
        <td ><b><font face="Arial" size="2">Req/Ctrl#(ACC):</font></b>&nbsp;<%=order_id%></td>
        <td  colspan="2"><font face="Arial" size="2"><b>Alternate Ctrl # (CD-) :</b></font></td>
        <td  colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td ><b><font face="Arial" size="2">Name:</font></b>&nbsp;<%=pat_name%></td>
        <td ><font face="Arial" size="2"><b>Sex:</b>&nbsp;<%=pat_sex%></font></td>
        <td ><font face="Arial" size="2"><b>Date of Birth:</b></font>&nbsp;<%=pat_dob%></td>
        <td ><font face="Arial" size="2"><b>SSN:</b></font>&nbsp;xxxxx<%=right(pat_ssn,4)%></td>
        <td >&nbsp;</td>
      </tr>
      <tr>
		<td  colspan="5"><b><font face="Arial" size="2">Address:</font></b>&nbsp;<%=pat_address%> <b><font face="Arial" size="2">Phone:</font></b>&nbsp;<%=pat_phone%></td>
	 </tr>
      <tr>
        <td ><b><font face="Arial" size="2">Coll Date:</font></b>&nbsp;<%=formatdatetime(coll_date,2)%></td>
        <td ><b><font face="Arial" size="2">Coll Time:</font></b>&nbsp;<%=formatdatetime(coll_date,3)%></td>
        <td ><font face="Arial" size="2"><b>Fasting:</b>&nbsp;<%=FASTING%></font></td>      
        <td ><font face="Arial" size="2"><b>Total Volume:</b>&nbsp;<%=URINE_VOLUME%>&nbsp;<%=VOLUME_UNITS%></font></td>
        <td ><font face="Arial" size="2"><b>Weight:</b>&nbsp;<%=Weight%></font></td>
      </tr>
      <tr>
        <td  colspan="2"><b><font face="Arial" size="2">Patient ID:</font></b>&nbsp;<%=patient_id%></td>
        <td  colspan="2"><font face="Arial" size="2"><b>Alt ID (PID:)</b>&nbsp;<%=pat_chartid%></font></td> 
        <td >&nbsp;</td>       
      </tr>
      <tr>
        <td ><b><font face="Arial" size="2">Bill Code:</font></b>&nbsp;<%=Bill_code%></td>
        <td colspan="4" rowspan="3" valign="top"><font face="Arial" size="2"><b>Clin Info 1:</b></font>&nbsp;<%=info1%></td>
      </tr>

    </table>
    </td>
  </tr>
  
  <% if SOURCE_SPECIMEN<>"" then %>
	<tr><td width="100%" height="5">&nbsp;</td></tr>
    <tr>
    <td width="100%" height="38">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber5">
      <tr>
        <td width="100%"><i><b><font face="Arial">Micro/Histo Information:</font></b></i></td>
      </tr>
      <tr>
        <td width="100%"><b>Source:</b>&nbsp;&nbsp;<%=SOURCE_SPECIMEN%></b></td>
      </tr>
    </table>
    </td>
  </tr>
  <%end if%>
  
  <% if Instr(Z_SEGMENT,"ZCY")<>0 then %>
	<tr><td width="100%" height="5">&nbsp;</td></tr>
	<tr>
    <td width="100%">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
      <tr>
        <td width="100%" colspan="5"><i><b><font face="Arial">Cytology Information:</font></b></i></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Gyn Source:</font></b>&nbsp;<%=Gyn_Source%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Date LMP/Menopause:</b>&nbsp;<%=LMP_MENO_DATE%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Collection Technique:</font></b>&nbsp;<%=Coll_tech%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Previous Treatment:</b>&nbsp;<%=Prv_tret%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Previous Cytology:</font></b>&nbsp;<%=Prv_Cyto%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Other Patient Information:</b>&nbsp;<%=Othet_Pat_info%></font></td>
      </tr>
    </table>
    </td>
  </tr>	
  <%end if%>    
  
  <% if Instr(Z_SEGMENT,"ZBL")<>0 then %>
	<tr><td width="100%" height="5">&nbsp;</td></tr>
	<tr>
    <td width="100%">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
      <tr>
        <td width="100%" colspan="5"><i><b><font face="Arial">Blood Lead Information:</font></b></i></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Race:</font></b>&nbsp;<%=RACE%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Hispanic:</b>&nbsp;<%=HISPANIC%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Blood Lead Type:</font></b>&nbsp;<%=BLOOD_LEAD_TYPE%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Blood Lead Purpose:</b>&nbsp;<%=BLOOD_LEAD_PURPOSE%></font></td>
      </tr>
    </table>
    </td>
  </tr>	
  <%end if%>    
  
<% If Test_cnt >= 3 or Test_ZSA="Yes" then %>	
	<tr><td width="100%"><div class="break"></div></td></tr>
		
	<table border=0 align="center" width="100%">
	<thead>
		<tr>
			<th width=100% align=left >
				 <tr><td width="100%">
				  <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
				    <tr>
				      <td width="50%"><b><font face="Arial" size="2">Account#:</font><%=LABCORP_ACC_ID%></b></td>
				      <td width="50%" colspan="4"><font face="Arial" size="2"><b>Patient Name:</b>&nbsp;<%=pat_name%></font></td>
				    </tr>
				    <tr>
				      <td width="50%"><b><font face="Arial" size="2">Date of Specimen:</font></b>&nbsp;<%=formatdatetime(coll_date,2)%></td>
				      <td width="50%" colspan="4"><font face="Arial" size="2"><b>Patient ID:</b>&nbsp;<%=patient_id%></font></td>
				    </tr>
				    <tr>
				      <td width="50%"><b><font face="Arial" size="2">Req/Crtl#:</font></b>&nbsp;<%=order_id%></td>
				      <td width="50%" colspan="4"><font face="Arial" size="2"><b>Alt Pat ID:</b>&nbsp;<%=pat_chartid%></font></td>
				    </tr>
				  </table>
				  </td>
				</tr>
			</th>
		</tr>
	</thead> 
	<tbody> 
	<tr> 
	
	<td width="100%">
	<table width="100%">		
<%end if%>
  
<% if Instr(Z_SEGMENT,"ZSA")<>0 then %>
	<tr><td width="100%" height="5">&nbsp;</td></tr>
	<tr>
    <td width="100%">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
      <tr>
        <td width="100%" colspan="5"><i><b><font face="Arial">AFP Information:</font></b></i></td>
      </tr>
      <tr>
			<td width="100%" colspan="5"><b><font face="Arial" size="2">Gest Age:</font></b>
			    <%if GESTATION_AGE_WEEKS<>"" then Response.Write GESTATION_AGE_WEEKS&" Weeks" %>&nbsp;&nbsp;&nbsp;
			    <%if GESTATIONAL_AGE_DAYS<>"" then Response.Write GESTATION_AGE_DAYS&" Days" %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    <%If GESTATIONAL_AGE_DECIMAL<>"" then Response.Write "Decimal Form:"&GESTATIONAL_AGE_DECIMAL&" on "&GESTATIONAL_AGE_DATE %>
			</td>        
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">By LMP:</font></b>&nbsp;<%=METHOD_BY_LMP%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Date:</b>&nbsp;<%=LMP_DATE%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">By US:</font></b>&nbsp;<%=METHOD_BY_ULTRASOUND%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Date:</b>&nbsp;<%=ULTRASOUND_DATE%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">By EDD:</font></b>&nbsp;<%=METHOD_BY_EST_DOD%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Date:</b>&nbsp;<%=EST_DOD%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Patient Weight:</font></b>&nbsp;<%=WEIGHT%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Type of Pregnancy:</b>&nbsp;<%=TYPE_PREGNANCY%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Insulin Dependent:</font></b>&nbsp;<%=INSULIN_DEPENDENT%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Race:</b>&nbsp;<%=RACE%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Routine Screening:</font></b>&nbsp;<%=ROUTINE_SCREENING%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Prev. Neural Tube Defect:</b>&nbsp;<%=NEURAL_TUBE_DEFECTS%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Advanced Maternal Age:</font></b>&nbsp;<%=MATERNAL_AGE%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>History of Down Syndrome:</b>&nbsp;<%=DOWN_SYNDROME%></font></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">History of Cystic Fibrosis:</font></b>&nbsp;<%=CYSTIC_FIBROSIS%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Other Indications:</b>&nbsp;<%=OTHER_INDICATIONS%></font></td>
      </tr>
      <tr>
        <td width="100%" colspan="5"><b><font face="Arial" size="2">Comment:</font></b>&nbsp;<%=AFP_INFO%></td>
      </tr>
      <tr>
        <td width="100%" colspan="5"><b><font face="Arial" size="2">Reason for Repeat:</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<b>Elevated:</b>&nbsp;<%=ELEVATED%>&nbsp;&nbsp;&nbsp;<b>Early GA:</b>&nbsp;<%=EARLY_GA%>&nbsp;&nbsp;&nbsp;<b>Hemolyzed:</b>&nbsp;<%=HEMOLYZED%></td>
        
      </tr>
            
    </table>
    </td>
  </tr>	
  <%end if%> 
  
  
<% if BILL_TYPE="T" or BILL_TYPE="P" then %>
	<%If Pri_Ins <> "" then 
		strSQL= "select * from patient_insurance where ins_order=1 and patient_id='"&patient_id&"' order by INS_order"
		'Response.Write strSQL
		RS.Open strSQL,CONN 	    
		if not (rs.EOF or rs.BOF) then
			INSURANCE_PROGRAM_NAME=RS("INSURANCE_PROGRAM_NAME")
			INSURANCE_ID	=RS("INSURANCE_ID")
			POLICY_GROUP	=RS("POLICY_GROUP")
			P_ins_name = RS("Insurance_Name")
			RELATION_PATIENT = RS("RELATION_PATIENT")	
			STREET_p	=RS("STREET")
			CITY_p	=RS("CITY")
			STATE_p	=RS("STATE")
			ZIP_p	=RS("ZIP")
			PH_NO	=RS("PH_NO")	
			EMPLOYERS_NAME	=RS("EMPLOYERS_NAME")			
			WORK_COMP		= RS("WORK_COMP")
			if WORK_COMP="Y" then 
				WORK_COMP="Yes"
			else
				WORK_COMP="No"
			end if
	   end if
	   rs.Close  
	   
	   'get labcorp payer code and address...
	   strSQL= "select a.* from LABCORP_CARRIER a, Patienttable b where upper(trim(PRIMARY_INSURANCE))= upper(trim(Ins_Name)) and b.userid='"&patient_id&"' "
	   'Response.Write strSQL
	   RS.Open strSQL,CONN 	    
		if not (rs.EOF or rs.BOF) then
			LC_CARRIER_CODE	=RS("LC_CARRIER_CODE")
			LC_PLAN_TYPE	=RS("LC_PLAN_TYPE")	
			ADD1	=  RS("ADD1")
			ADD2	= RS("ADD2")
			CITY	= RS("CITY")
			STATE	= RS("STATE")
			ZIP		= RS("ZIP")
			PHONE	= RS("PHONE")
			Ins_address= ADD1 &" "&ADD2&" "&CITY&" "&STATE&" "&ZIP
		end if
	   rs.Close 	   
	   
	   if Instr(INSURANCE_PROGRAM_NAME,"Medicare") <> 0 then  '--'for medicare only
			Medicare_id=INSURANCE_ID
			Medicaid_id=""
			subscriber_id=""
	   elseif Instr(INSURANCE_PROGRAM_NAME,"Medicaid") <> 0 then  '--'for Medicaid only
			Medicare_id=""
			Medicaid_id=INSURANCE_ID
			subscriber_id=""
	   else     	'--for other insurance types
			Medicare_id=""
			Medicaid_id=""
			subscriber_id=INSURANCE_ID
	   end if  
	            
	end if    
	    
	    
	%>  
  
	<tr><td width="100%" height="5">&nbsp;</td></tr>
	<tr>
    <td width="100%">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
      <tr>
        <td width="100%" colspan="5"><i><b><font face="Arial">Primary Billing:</font></b></i></td>
      </tr>
      
      <tr>
        <td colspan="2"><b><font face="Arial" size="2">Medicare#:</font></b>&nbsp;<%=Medicare_id%>&nbsp;&nbsp;</td>
        <td colspan="3"><b><font face="Arial" size="2" >Medicaid/HMO#: </font></b>&nbsp;<%=Medicaid_id%>&nbsp;&nbsp;
		<font face="Arial" size="2"><b>ABN?</b>&nbsp;<%'=%></font>&nbsp;&nbsp;
			<font face="Arial" size="2"><b>Worker�s Comp?</b><%=work_comp%></font>
        </td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Insurance Co Name:</font></b>&nbsp;<%=INSURANCE_PROGRAM_NAME%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Payor/Carrier Code:</b>&nbsp;<%=LC_CARRIER_CODE%></font></td>
      </tr>      
	  <tr>
        <td width="50%"><b><font face="Arial" size="2">Subscriber/Member #:</font></b>&nbsp;<%=subscriber_id%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Group Number:</b>&nbsp;<%=POLICY_GROUP%></font></td>
      </tr>       
      <tr>
        <td width="100%" colspan="5"><b><font face="Arial" size="2">Insurance Address:</font></b>&nbsp;<%=Ins_address%></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Physician�s Provider #:</font></b>&nbsp;<%'=%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Employer�s Name or Number:</b>&nbsp;<%=EMPLOYERS_NAME%></font></td>
      </tr>    
            
    </table>
    </td>
  </tr>	
  
  
  
  <%If Sec_Ins <> "" then 
		strSQL= "select * from patient_insurance where ins_order=2 and patient_id='"&patient_id&"'"
		'Response.Write strSQL
		RS.Open strSQL,CONN 	    
		if not (rs.EOF or rs.BOF) then
			INSURANCE_PROGRAM_NAME=RS("INSURANCE_PROGRAM_NAME")
			INSURANCE_ID	=RS("INSURANCE_ID")
			POLICY_GROUP	=RS("POLICY_GROUP")
			RELATION_PATIENT = RS("RELATION_PATIENT")	
			STREET	=RS("STREET")
			CITY	=RS("CITY")
			STATE	=RS("STATE")
			ZIP		=RS("ZIP")
			PH_NO	=RS("PH_NO")	
			EMPLOYERS_NAME	=RS("EMPLOYERS_NAME")			
			WORK_COMP		= RS("WORK_COMP")
			if WORK_COMP="Y" then 
				WORK_COMP="Yes"
			else
				WORK_COMP="No"
			end if
	   end if
	   rs.Close
	   
	   'get labcorp payer code and address...
	   strSQL= "select a.* from LABCORP_CARRIER a, Patienttable b where upper(trim(SECONDARY_INSURANCE))= upper(trim(Ins_Name)) and b.userid='"&patient_id&"' "
	   RS.Open strSQL,CONN 	    
		if not (rs.EOF or rs.BOF) then
			LC_CARRIER_CODE	=RS("LC_CARRIER_CODE")
			LC_PLAN_TYPE	=RS("LC_PLAN_TYPE")	
			ADD1	=  RS("ADD1")
			ADD2	= RS("ADD2")
			CITY	= RS("CITY")
			STATE	= RS("STATE")
			ZIP		= RS("ZIP")
			PHONE	= RS("PHONE")
			Ins_address= ADD1 &" "&ADD2&" "&CITY&" "&STATE&" "&ZIP
		end if
	   rs.Close   
	   
	  if Instr(INSURANCE_PROGRAM_NAME,"Medicare") <> 0 then  '--'for medicare only
			Medicare_id=INSURANCE_ID
			Medicaid_id=""
			subscriber_id=""
	   elseif Instr(INSURANCE_PROGRAM_NAME,"Medicaid") <> 0 then  '--'for Medicaid only
			Medicare_id=""
			Medicaid_id=INSURANCE_ID
			subscriber_id=""
	   else     	'--for other insurance types
			Medicare_id=""
			Medicaid_id=""
			subscriber_id=INSURANCE_ID
	   end if  
	    
	%>  
  
	<tr><td width="100%" height="5">&nbsp;</td></tr>
	<tr>
    <td width="100%">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
      <tr>
        <td width="100%" colspan="5"><i><b><font face="Arial">Secondary Billing:</font></b></i></td>
      </tr>
      
      <tr>
        <td colspan="2"><b><font face="Arial" size="2">Medicare#:</font></b>&nbsp;<%=Medicare_id%>&nbsp;&nbsp;</td>
        <td colspan="3"><b><font face="Arial" size="2" >Medicaid/HMO#: </font></b>&nbsp;<%=Medicaid_id%>&nbsp;&nbsp;
		<font face="Arial" size="2"><b>ABN?</b>&nbsp;<%'=%></font>&nbsp;&nbsp;
			<font face="Arial" size="2"><b>Worker�s Comp?</b><%=work_comp%></font>
        </td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Insurance Co Name:</font></b>&nbsp;<%=INSURANCE_PROGRAM_NAME%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Payor/Carrier Code:</b>&nbsp;<%=LC_CARRIER_CODE%></font></td>
      </tr>      
	  <tr>
        <td width="50%"><b><font face="Arial" size="2">Subscriber/Member #:</font></b>&nbsp;<%=subscriber_id%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Group Number:</b>&nbsp;<%=POLICY_GROUP%></font></td>
      </tr>       
      <tr>
        <td width="100%" colspan="5"><b><font face="Arial" size="2">Insurance Address:</font></b>&nbsp;<%=Ins_address%></td>
      </tr>
      <tr>
        <td width="50%"><b><font face="Arial" size="2">Physician�s Provider #:</font></b>&nbsp;<%'=%></td>
        <td width="50%" colspan="4"><font face="Arial" size="2"><b>Employer�s Name or Number:</b>&nbsp;<%=EMPLOYERS_NAME%></font></td>
      </tr>    
            
    </table>
    </td>
  </tr>	
<%end if%>
 
  <tr><td width="100%" height="5">&nbsp;</td></tr>
	<tr>
    <td width="100%">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
      <tr>
        <td width="100%" colspan="5"><i><b><font face="Arial">Responsible Party/ Insured's Information:</font></b></i></td>
      </tr>
      
      <tr>
        <td ><b><font face="Arial" size="2">Name:</font></b>&nbsp;<%=P_ins_name%></td>
        <td ><b><font face="Arial" size="2" >Relationship:</b>&nbsp;<%=RELATION_PATIENT%>&nbsp;</td>
      </tr>
      <tr>
        <td width="100%" colspan="5" ><b><font face="Arial" size="2">Address:</font></b>&nbsp;<%=Street_p%>&nbsp;,<%=City_p%>&nbsp;<%=state_p%>-<%=zip_p%></td>
      </tr>            
    </table>
    </td>
  </tr>	
  
<%end if%>  

<% If BILL_TYPE="P" or BILL_TYPE="T" then %>
 <%		strSQL= "select * from patienttable where userid ='"&patient_id&"'"
		RS.Open strSQL,CONN 	
		    
		if not (rs.EOF or rs.BOF) then
			P_name = RS("last_name")&", "&RS("first_name")
			RELATION_PATIENT = "Self"	
			STREET_p	=RS("STREET")
			CITY_p	=RS("CITY")
			STATE_p	=RS("STATE")
			ZIP_p	=RS("ZIP")
			
			g_name=RS("contact_last_person")&", "&RS("contact_first_person")
			g_relation = RS("contact_person_relation")
			g_address = RS("street_contact_person")&" "&RS("city_contact_person")&" "&RS("state_contact_person")&"-"&RS("zip_contact_person")	
			g_home_phone= RS("phone_contact_person")	
	   end if
	   rs.Close 
 %>

 <tr><td width="100%" height="5">&nbsp;</td></tr>
	<tr>
    <td width="100%">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber4">
      <tr>
        <td width="100%" colspan="5"><i><b><font face="Arial">Guarantor Information:</font></b></i></td>
      </tr>
      
      <tr>
        <td ><b><font face="Arial" size="2">Name:</font></b>&nbsp;<%=g_name%></td>
        <td ><b><font face="Arial" size="2" >Relation ship:</b>&nbsp;<%=g_relation%>&nbsp;</td>
      </tr>
      <tr>
        <td width="100%" colspan="5" ><b><font face="Arial" size="2">Address:</font></b><%=g_address%><b><font face="Arial" size="2">&nbsp;&nbsp;Phone:</font></b><%=g_home_phone%></td>
      </tr>            
    </table>
    </td>
  </tr>   
<%End if%> 
   
  <tr>
    <td width="100%" height="5">&nbsp;</td>
  </tr>
  <tr>
    <td width="100%">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber5">
      <tr>
        <td width="100%"><i><b><font face="Arial">Tests Ordered:</font></b></i></td>
      </tr>
      <tr>
        <!--<td width="100%"><b><%=labcorp_test_num%>&nbsp;-&nbsp;<%=labcorp_test_name%></b></td>-->
        <td width="100%"><b><%=labcorp_test_info%></b>&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  
  <tr>
    <td width="100%" height="5">&nbsp;</td>
  </tr>
  <tr>
    <td width="100%" height="48">
    <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber6">
      <tr>
        <td width="100%"><i><b><font face="Arial">Diagnosis Codes:</font></b></i></td>
      </tr>
      <tr>
        <td width="100%"><b><%=icd_code%></b>&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  
  <tr>
    <td width="100%" height="5">&nbsp;</td>
  </tr>  
  
  <tr>
    <td width="100%" height="120"><font face="Arial" size="2">Authorization - 
    Please sign and Date<br>
    I hereby authorize the release of medical information related to the 
    services described hereon and authorize payment directly to Laboratory 
    Corporation of America.<br>
    <br>
    ____________________________________ ________________ <br>
    Patient Signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    Date<br>
    <br>
    <br>
    ____________________________________ ________________ <br>
    Physician Signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    Date</font><br>
&nbsp;</td>
  </tr>
</table>

<% If Test_cnt >= 3 or Test_ZSA="Yes" then %>	

<!--To print the header on each page. Do not change this -->
</td>
</tr> 
</tbody> 
</table>
<!--Do not change this -->

<%end if%>

</body>

</html>