<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/functions.inc.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%

visit_key	= Request("visit_key")
patient_id  = Request("patient_id")
other_meds = Session("other_meds")


dim auditObj
set auditObj = new clsAudit

'call auditObj.LogActivity("Accessing MEDICATION NOSS","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)


dim patientNotesObj
set rs1 = server.CreateObject("adodb.recordset")
set patientNotesObj = new clsPatientNotes
patientNotesObj.SetVisitKey(visit_key)

'if patientNotesObj.Closed = true then
'  Response.Write "<h1>Note has already been closed.</h1>"
'  Response.End
'end if

if session("show") = "new" then
  showval = 0
else
  showval = 1
end if

if Request.Form("submit_button") <> "" then

  dim CMD

  drug_name =  trim(Request.Form("nm_med"))
  formulation = trim(Request.Form("formulation"))
  if formulation = "" then
	formulation = "NA"
	end if

  'formulation = AscEncode(formulation)
  'formulation = ChrEncode(formulation)
  dispense = Request.Form("dispense") & " " & Request.Form("dispense_units")
  dosage = Request.Form("dosage") & " " & Request.Form("dosage_units")
  frequency = trim(Request.Form("freq"))
  route = Request.Form("route")
  refill = Request.Form("refill")
  prescribed_by = session("user")
  notes = trim(Request.Form("notes"))
  multum_drug_id = Request.Form("multum_drug_id")
  multum_brand_code = Request.Form("multum_brand_code")
  stn=trim(Request.Form("stn"))
  start_date = Request.Form("start_date")
  if start_date = "" then
	start_date = date()
  end if
  sub_id = Request.Form("sub_id")
  brand_code = Request.Form("brand_code")
  main_multum_drug_code = Request.Form("main_multum_drug_code")

  set CMD = Server.CreateObject("ADODB.Command")
  CMD.ActiveConnection = CONN

  CONN.BeginTrans

  sqlQuery = "SELECT notes_medications_drug_id.nextval as drug_id FROM DUAL"
  CMD.CommandText = sqlQuery
  set RSSeq = CMD.Execute

  drug_id = RSSeq("drug_id").value
  RSSeq.Close
  set RSSeq = nothing

	if (other_meds = "Current") then
		med_status = "Current"
	else
		med_status = "O Current"	
	end if

  ndc_code=""
  
  sqlQuery = "INSERT INTO notes_medications " &_
  "(drug_id,visit_key,drug_name,formulation,dispense,dose,frequency,route,refill,REFILL_REMAINING,prescribed_by,prescribed_date,patient_id,notes,iscurrent,multum_drug_id,multum_brand_code,SUBSTITUTION,ndc_code,ss_flag,status) " &_
  "VALUES " &_
  "('" & drug_id & "','" & visit_key & "','" & sqlFixUp(drug_name) & "','" & trim(sqlFixUp(formulation)) & "','" & sqlFixUp(dispense) & "','" & sqlFixUp(dosage) & "','" & sqlFixUp(frequency) & "','" & sqlFixUp(route) & "','" & sqlFixUp(refill) & "','" & sqlFixUp(refill) & "','" & prescribed_by & "',to_date('" & start_date & "','MM/DD/YYYY'),'" & patient_id & "','" & sqlFixUp(notes) &"','"& showval &"','"& multum_drug_id &"','"& multum_brand_code &"','"& sub_id &"','"& ndc_code &"','0','"& med_status &"')"

  CMD.CommandText = sqlQuery
  CMD.Execute

  call auditObj.LogActivity("Created a new record into Medication Notes","",Cstr(drug_id)+","+Cstr(drug_name)+","+Cstr(formulation)+","+Cstr(dispense)+","+Cstr(dosage)+","+Cstr(frequency)+","+Cstr(route)+","+Cstr(refill)+","+Cstr(prescribed_by)+","+Cstr(notes)+","+Cstr(showval)+","+Cstr(multum_drug_id)+","+Cstr(multum_brand_code)+","+Cstr(sub_id)+","+Cstr(ndc_code),Cstr(patient_id),Cstr(visit_key),SYSDATE)


  CONN.CommitTrans

  %>
  <script language="javascript">
    parent.rightNoteFrame.location.reload(true);
    document.location='/practice/notes/add/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&msg=Medication Added.';
  </script>
  <%

  Response.End

end if


set RSVitals = Server.CreateObject("ADODB.Recordset")
RSVitals.CursorLocation = 3
sqlQuery = "SELECT DISTINCT take FROM notes_vitals WHERE visit_key='" & visit_key & "'"
RSVitals.Open sqlQuery, CONN, 3, 4
VCount = RSVitals.RecordCount
RSVitals.Close

if VCount > 0 then
  sqlQuery = "SELECT * FROM notes_vitals WHERE visit_key='" & visit_key & "' AND vital_type='W' ORDER BY take DESC"
  RSVitals.Open sqlQuery, CONN, 3, 4
  set RSVitals.ActiveConnection = nothing

  dim VCounter

  if RSVitals.RecordCount > 0 then
    weight = RSVitals("vital_data1") & "kgs (" & Round(RSVitals("vital_data1").Value / .4536,2) & "lbs)"
  end if

end if

if weight = "" then
  weight = "Not recorded yet."
end if

Set DoctorDataSet = GetDoctorData(visit_key)
DOCTOR_NAME = DoctorDataSet("Last_Name")&", "&DoctorDataSet("First_Name")
DoctorDataSet.Close
Set DoctorDataSet = nothing

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script language="javascript">
function validateForm(that) {
	setDosage();
	//setFreq(document.frm2.freq_select);
	//setRoute(document.frm2.route_select);
	
	//alert(document.frm2.dosage_units_select.selectedIndex);
	

  if (that.nm_med.value == "") {
    alert("Please input a medication to prescribe.");
    that.nm_med.focus();
    return false;
  }
    
  if (that.formulation.value == "") {
    alert("Please describe the medication's formulation.");
    that.formulation.focus();
    return false;
  }
  
  var re = /^\d+$/;
  
  
  /* if (!that.dispense.value.match(re)) {
    alert("Please enter an appropriate amount of drug to dispense.");
    that.dispense.focus();
    return false;
  } */

  if (that.dispense.value == "") {
      alert("Please enter the dispense quantity");
      that.dispense.focus();
      return false;
  }


  if (that.dispense_units.selectedIndex <= 0) {
      alert("Please select an appropriate dispense unit.");
      that.dispense_units.focus();
      return false;
  }

  if (alltrim(that.dosage.value) == '') {
      alert("Please enter an appropriate dosage.");
      that.dosage.focus();
      return false;
  }

  if (that.dosage_units_select.selectedIndex == 0) {
      alert("Please enter an appropriate dosage unit.");
      that.dosage_units_select.focus();
      return false;
  }
  if (that.dosage_units_select.selectedIndex == 21 && that.dosage_units.value == '') {
      alert("Please enter Other Dosage Unit.");
      that.dosage_units_select.selectedIndex = 21;
      that.dosage_units.focus();
      return false;
  }
  
  
  var units = /^[\w\(\)\d]+$/;
  
  if (that.freq.value == "") {
	setFreq(document.frm2.freq_select);
  }
  if (that.route.value == "") {
	setRoute(document.frm2.route_select);
  }
  

  if (!that.dosage_units.value != "" && that.showval.value==0 ) {
    alert("Please select an appropriate dosage unit: " + that.dosage_units.value);
    return false;
  }

  if (!that.freq.value != "") {
    alert("Please select the frequency of dosage: " + that.freq.value);
    return false;
  }

if (!that.route.value != "") {
    alert("Please select the route of administering dosage: " + that.route.value);
    return false;
}

if (that.dispense.value != "" && !isNumber()) {
    return false;
  }
  

//  if (!that.route.value != "") {
//    alert("Please select the route of administering dosage: " + that.route.value);
//    return false;
//  }

	/*
  var startDate = /^\d{2}\/\d{2}\/\d{4}$/;
  if (that.start_date.value == "") {
	alert("Please set the Medication Start Date.");
	that.start_date.focus();
	return false;
	}
  else
  {
	if (!that.start_date.value.match(startDate))
	{
		alert("Please enter the start date in correct format(MM/DD/YYYY)");
		return false;
	}
  }
  return true; */
    if (!validateSpecialChars(that)) {
        return false;
    }

  //return confirm("Please confirm that this medication is NOT a controlled substance.");
}

function setDosage() {

  for (var i = 0; i < document.frm2.dosage_units_select.length; i++) {
    if (document.frm2.dosage_units_select[i].selected == true && document.frm2.dosage_units_select[i].text == 'Other') {
      document.getElementById('dosage_tr').style.display = '';
      //document.getElementById('dosage_units').value = '';
      document.getElementById('dosage_units').focus();

      return true;
    }
    if (document.frm2.dosage_units_select[i].selected == true && document.frm2.dosage_units_select[i].text != 'Other') {
      document.getElementById('dosage_tr').style.display = 'none';
      document.getElementById('dosage_units').value = document.frm2.dosage_units_select[i].text;
      return true;
    }
  }

}

function setFreq123() {

  for (var i = 0; i < document.frm2.freq_select.length; i++) {
    if (document.frm2.freq_select[i].selected == true && document.frm2.freq_select[i].text == 'Other') {
      document.getElementById('freq_tr').style.display = '';
      document.getElementById('freq').value = '';
      document.getElementById('freq').focus();

      return true;
    }
    if (document.frm2.freq_select[i].selected == true && document.frm2.freq_select[i].text != 'Other') {
      document.getElementById('freq_tr').style.display = 'none';
      document.getElementById('freq').value = document.frm2.freq_select[i].text;
      return true;
    }
  }

}

function setFreq(that) {

  for (var i = 0; i < that.length; i++) {
    if (that[i].selected == true && that[i].text == 'Other') {
      document.getElementById('freq_tr').style.display = '';
      document.getElementById('freq').value = '';
      document.getElementById('freq').focus();

      return true;
    }
    if (that[i].selected == true && that[i].text != 'Other') {
      document.getElementById('freq_tr').style.display = 'none';
      document.getElementById('freq').value = that[i].text;
      return true;
    }
  }

}


function setRoute123() {

  for (var i = 0; i < document.frm2.route_select.length; i++) {
    if (document.frm2.route_select[i].selected == true && document.frm2.route_select[i].text == 'Other') {
      document.getElementById('route_tr').style.display = '';
      document.getElementById('route').value = '';
      document.getElementById('route').focus();

      return true;
    }
    if (document.frm2.route_select[i].selected == true && document.frm2.route_select[i].text != 'Other') {
      document.getElementById('route_tr').style.display = 'none';
      document.getElementById('route').value = document.frm2.route_select[i].text;
      return true;
    }
  }

}



function setRoute(that) {

  for (var i = 0; i < that.length; i++) {
    if (that[i].selected == true && that[i].text == 'Other') {
      document.getElementById('route_tr').style.display = '';
      document.getElementById('route').value = '';
      document.getElementById('route').focus();

      return true;
    }
    if (that[i].selected == true && that[i].text != 'Other') {
      document.getElementById('route_tr').style.display = 'none';
      document.getElementById('route').value = that[i].text;
      return true;
    }
  }

}

function setDate(frmObj, d) {

  var month, day, year;
  re = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;

  if (frmObj.value.match(re)) {

    var now = new Date(frmObj.value);

    if (d == 0) {
      now.setTime(now.getTime() - 24 * 60 * 60 * 1000);
      month = now.getMonth() + 1;
      day = now.getDate();
      year = now.getYear();

      if (year < 1000)
        year = year + 1900;

      frmObj.value = month + "/" + day + "/" + year;
    } else {
      now.setTime(now.getTime() + 24 * 60 * 60 * 1000);
      month = now.getMonth() + 1;
      day = now.getDate();
      year = now.getYear();
      if (year < 1000)
        year = year + 1900;
      frmObj.value = month + "/" + day + "/" + year;
    }


  } else {
    var today = new Date();
    month = today.getMonth() + 1;
    day = today.getDate();
    year = today.getYear();
    if (year < 1000)
      year = year + 1900;
    frmObj.value = month + "/" + day + "/" + year;

  }
}

function checklength(i)
{
var txt;
	txt=document.frm2.notes.value;
	n=txt.length;
if (n>i)
{
	alert('Only 210 characters are allowed for this field');
	document.frm2.notes.focus();
	document.frm2.notes.value=txt;
return;
}
	text1=document.frm2.notes.value;
}


function fx_number3(){
			var str = document.frm2.dispense.value;
			var valid;
			var val='exp1';

			switch (val){
			case "exp1":
				valid = test1(str);
				break;
			case "exp2":
				valid = test2(str);
				break;
			case "exp3":
				valid = test3(str);
				break;
			default:
				valid = false;
			}
			function test1(str) {
				str = alltrim(str);
				return /^[-+]?[0-9]+(\.[0-9]+)?$/.test(str);
			}
			function test2(str) {
				str = alltrim(str);
				return /^[-+]?\d+(\.\d+)?$/.test(str);
			}
			function test3(str) {
				str = alltrim(str);
				return /^[-+]?\d{3,5}(\.\d{1,3})?$/.test(str);
			}
			//document.frm2.dispense.value = (valid)?"Valid":"Invalid";

			if (document.frm2.dispense.value !='' &&  valid==false)
			{
				alert("Invalid Number");
				document.frm2.dispense.focus();
			}
		}

function isNumber() {
    var val = alltrim(document.frm2.dispense.value);
	if (val != "") {
		var num = Number(val);
		if (isNaN(num) || val.match(/^(\.|\d)+$/g) == null) {
		    alert("Invalid Number");
		    document.frm2.dispense.focus();
		} else if (val.indexOf(".") != -1 && val.length > 11) {
		    alert("Dispense must not be longer than 10 digits.");
		    document.frm2.dispense.focus();
		} else if (val.indexOf(".") == -1 && val.length > 10) {
		    alert("Dispense must not be longer than 10 digits.");
		    document.frm2.dispense.focus();
		} else if (num <= 0) {
		    alert("Dispense must not be zero");
		    document.frm2.dispense.focus();
		} else {
		    document.frm2.dispense.value = num.toFixed(decimalPlaces(val));
		    return true;
		}
    }
    return false;
}

function decimalPlaces(numberStr) {
    // toFixed produces a fixed representation accurate to 20 decimal places
    // without an exponent.
    // The ^-?\d*\. strips off any sign, integer portion, and decimal point
    // leaving only the decimal fraction.
    // The 0+$ strips off any trailing zeroes.
    return (numberStr).replace(/^-?\d*\.?|0+$/g, '').length
}

function alltrim(str) {
			return str.replace(/^\s+|\s+$/g, '');
		}

function validateSpecialChars(that) {
	if (that.nm_med.value != "" && hasSpecialChars(that.nm_med.value)) {
		alert("Illegal characters (#, ') in Medication Name.");
		that.nm_med.focus();
		return false;
	}

	if (that.formulation.value != "" && hasSpecialChars(that.formulation.value)) {
		alert("Illegal characters (#, ') in Formulation.");
		that.formulation.focus();
		return false;
	}

    if (alltrim(that.dosage.value) != '' && hasSpecialChars(that.dosage.value)) {
		alert("Illegal characters (#, ') in Dosage.");
		that.dosage.focus();
		return false;
	}
	if (that.dosage_units.value != '' && hasSpecialChars(that.dosage_units.value)) {
		alert("Illegal characters (#, ') in Other Dosage.");
		that.dosage_units.focus();
		return false;
	}
	if (that.freq.value != "" && hasSpecialChars(that.freq.value)) {
		alert("Illegal characters (#, ') in Frequency");
		return false;
	}

	if (that.route.value != "" && hasSpecialChars(that.route.value)) {
		alert("Illegal characters (#, ') in Route");
		return false;
	}
	if (that.notes.value != "" && hasSpecialChars(that.notes.value)) {
		alert("Illegal characters (#, ') in Notes.");
		that.formulation.focus();
		return false;
	}
	return true;
}

function hasSpecialChars(inputStr) {
	var specialChars = ["'", "#"];
	for (var i = 0; i < specialChars.length; i++) {
		if (inputStr.indexOf(specialChars[i]) != -1) {
		    return true;
		}
	}
	return false;
}

</script>
</head>
<body>
<div class="titleBox">Add New Medication (Free Text)</div>
<p>
<% if Request("msg") <> "" then %>
<span style="color: green"><b><%=Request("msg")%></b></span>
<p>
<% end if %>
Fields in <b>BOLD</b> are required.<br>Medications added here cannot be submitted to SureScript.
<p>
<form name="frm2" id="frm2" action="medicationNOSS.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>" method="post" onsubmit="return validateForm(this);">
<table>
  <tr>
	<input type="hidden" name="multum_drug_id">
	<input type="hidden" name="multum_brand_code">
	<input type="hidden" name="showval" value=<%=showval%>>
	<input type="hidden" name="stn">
	<input type="hidden" name="brand_code">
	<input type="hidden" name="main_multum_drug_code">

    <td align="right"><b>Patient Weight</td>
    <td><%=weight%></td>
  </tr>
  <tr>
    <td align="right"><b><a href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/medication.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'); return false;">Drug Name</a></td>
    <td><input maxlength="100" name="nm_med" size="20" value="<%=drug_name%>"> </td>
  </tr>
  <tr>
    <td align="right"><b>Formulation</b></td>
    <td><input name="formulation" value="<%=forumlation%>" size="25" maxlength="100"></td>
  </tr>
  <tr>
    <td align="right"><b>Dose</b></td>
    <td>
      <input name="dosage" value="<%=dosage%>" size="5" maxlength="9">
      <select id="dosage_units_select" name="dosage_units_select" onchange="setDosage();">
        <option></option>
        <option value="Ampoule(s)">Ampoule(s)</option>
        <option value="Application(s)">Application(s)</option>
        <option value="Applicator(s)">Applicator(s)</option>
        <option value="Applicatorful(s)">Applicatorful(s)</option>
        <option value="Bag(s)">Bag(s)</option>
        <option value="Bar(s)">Bar(s)</option>
        <option value="Bead(s)">Bead(s)</option>
        <option value="Blister(s)">Blister(s)</option>
        <option value="Block(s)">Block(s)</option>
        <option value="Bolus(es)">Bolus(es)</option>
        <option value="Bottle(s)">Bottle(s)</option>
        <option value="Box(es)">Box(es)</option>
        <option value="Can(s)">Can(s)</option>
        <option value="Canister(s)">Canister(s)</option>
        <option value="Caplet(s)">Caplet(s)</option>
        <option value="Capsule(s)">Capsule(s)</option>
        <option value="Carton(s)">Carton(s)</option>
        <option value="Cartridge(s)">Cartridge(s)</option>
        <option value="Case(s)">Case(s)</option>
        <option value="Cassette(s)">Cassette(s)</option>
        <option value="Container(s)">Container(s)</option>
        <option value="Cylinder(s)">Cylinder(s)</option>
        <option value="Device(s)">Device(s)</option>
        <option value="Disk(s)">Disk(s)</option>
        <option value="Dose Pack(s)">Dose Pack(s)</option>
        <option value="Drop(s)">Drop(s)</option>
        <option value="Dual Pack(s)">Dual Pack(s)</option>
        <option value="Each">Each</option>
        <option value="Film(s)">Film(s)</option>
        <option value="Fluid Ounce(s)">Fluid Ounce(s)</option>
        <option value="French(es)">French(es)</option>
        <option value="Gallon(s)">Gallon(s)</option>
        <option value="Gram(s)">Gram(s)</option>
        <option value="Gum(s)">Gum(s)</option>
        <option value="Implant(s)">Implant(s)</option>
        <option value="Inhalation(s)">Inhalation(s)</option>
        <option value="Inhaler(s)">Inhaler(s)</option>
        <option value="Inhaler Refill(s)">Inhaler Refill(s)</option>
        <option value="Insert(s)">Insert(s)</option>
        <option value="Intravenous Bag(s)">Intravenous Bag(s)</option>
        <option value="Kilogram(s)">Kilogram(s)</option>
        <option value="Kit(s)">Kit(s)</option>
        <option value="Lancet(s)">Lancet(s)</option>
        <option value="Liter(s)">Liter(s)</option>
        <option value="Lozenge(s)">Lozenge(s)</option>
        <option value="Micrograms">Micrograms</option>
        <option value="Milliequivalent(s)">Milliequivalent(s)</option>
        <option value="Milligrams">Milligrams</option>
        <option value="Milliliters">Milliliters</option>
        <option value="Millimeter(s)">Millimeter(s)</option>
        <option value="Nebule(s)">Nebule(s)</option>
        <option value="Needle Free Injection">Needle Free Injection</option>
        <option value="Ocular System(s)">Ocular System(s)</option>
        <option value="Ounce(s)">Ounce(s)</option>
        <option value="Package(s)">Package(s)</option>
        <option value="Packet(s)">Packet(s)</option>
        <option value="Pad(s)">Pad(s)</option>
        <option value="Paper(s)">Paper(s)</option>
        <option value="Pastille(s)">Pastille(s)</option>
        <option value="Patch(es)">Patch(es)</option>
        <option value="Pen Needle(s)">Pen Needle(s)</option>
        <option value="Pint(s)">Pint(s)</option>
        <option value="Pouch(es)">Pouch(es)</option>
        <option value="Pound(s)">Pound(s)</option>
        <option value="Pre-filled Pen Syringe">Pre-filled Pen Syringe</option>
        <option value="Puff(s)">Puff(s)</option>
        <option value="Pump(s)">Pump(s)</option>
        <option value="Quart(s)">Quart(s)</option>
        <option value="Ring(s)">Ring(s)</option>
        <option value="Sachet(s)">Sachet(s)</option>
        <option value="Scoopful(s)">Scoopful(s)</option>
        <option value="Sponge(s)">Sponge(s)</option>
        <option value="Spray(s)">Spray(s)</option>
        <option value="Stick(s)">Stick(s)</option>
        <option value="Strip(s)">Strip(s)</option>
        <option value="Suppository(s)">Suppository(s)</option>
        <option value="Swab(s)">Swab(s)</option>
        <option value="Syringe(s)">Syringe(s)</option>
        <option value="Tablespoon(s)">Tablespoon(s)</option>
        <option value="Tablet(s)">Tablet(s)</option>
        <option value="Tabminder(s)">Tabminder(s)</option>
        <option value="Tampon(s)">Tampon(s)</option>
        <option value="Teaspoon(s)">Teaspoon(s)</option>
        <option value="Tray(s)">Tray(s)</option>
        <option value="Troche(s)">Troche(s)</option>
        <option value="Tube(s)">Tube(s)</option>
        <option value="Unit(s)">Unit(s)</option>
        <option value="Vaginal Ring">Vaginal Ring</option>
        <option value="Vial(s)">Vial(s)</option>
        <option value="Wafer(s)">Wafer(s)</option>
        <option value="Other">Other</option>
      </select>
    </td>
  </tr>
  <tr id="dosage_tr" style="display: none;">
    <td class="st" align="right"><i>other dosage unit</td>
    <td><input type="text" id="dosage_units" name="dosage_units" maxlength="15"></td>
  </tr>

  <tr>
    <td align="right"><b>Frequency</td>
    <td>
      <select name="freq_select" id="freq_select" onchange="setFreq(this);">
        <option>
        <option selected>Once Daily
        <option>Twice Daily
        <option>Three Times Daily
        <option>Four Times Daily
        <option>Five Times Daily
        <option>Six Times Daily
        <option>Every Other Day
        <option>7:00am and 4:00pm
        <option>At Bedtime
        <option>Once Weekly
        <option>Once Monthly
        <option>Twice Weekly
        <option>Three Times Weekly
        <option>As Needed
        <option>Every Morning
        <option>Every Night
        <option>Every Morning and Night
        <option>
        <option>Every 0.25 Hours
        <option>Every 0.50 Hours
        <option>Every 1 Hours
        <%
          for i = 2 to 24
        %>
        <option>Every <%=i%> Hours
        <%
          next
        %>
        <option>
        <option>Other
      </select>
    </td>
  </tr>
  <tr id="freq_tr" style="display: none;">
    <td class="st" align="right"><i>other freq.</td>
    <td><input type="text" id="freq" name="freq" maxlength=70></td>
  </tr>

  <tr>
    <td align="right"><b>Route</td>
    <td>
      <select name="route_select" id="route_select" onchange="setRoute(this);">
        <option>
      <%
        dim RSRoute
        set RSRoute = Server.CreateObject("ADODB.Recordset")
        RSRoute.CursorLocation = 3
        sqlQuery = "SELECT * FROM multum_route ORDER BY route_description"
        RSRoute.Open sqlQuery, CONN, 3, 4
        set RSRoute.ActiveConnection = nothing

        while not RSRoute.EOF
      %>
        <option value="<%=RSRoute("route_abbr")%>" <% if ucase(RSRoute("route_description")) = "ORAL" then %> selected <% end if %> ><%=PCase(RSRoute("route_description"))%>
      <%
          RSRoute.MoveNext
        wend
        RSRoute.close

      %>
        <option>Other
      </select>
    </td>
  </tr>
  <tr id="route_tr" style="display: none;">
    <td class="st" align="right"><i>other route</td>
    <td><input type="text" id="route" name="route" maxlength=25></td>
  </tr>
  <tr>
    <td align="right"><b>Refill Allowed</td>
    <td>
      <select name="refill" SIZE="1" onchange="document.frm2.submit_button.focus();">
        <%
          for i = 0 to 99
        %>
        <option><%=i%>
        <%
          next
        %>
        <option value=999>PRN</option>
      </select> times
    </td>
  </tr>
  <tr>
    <td align="right"><b>Dispense</b></td>
    <td>
      <input name="dispense" value="<%=dispense%>" size="6" maxlength=11 onblur="isNumber();">
      <select name="dispense_units">
      <option>  
        <%
					set RSDSP = Server.CreateObject("ADODB.Recordset")
					RSDSP.open "select * from SS_DISPENSE where inuse = 1 order by description", con
					while not RSDSP.EOF
    	%>
					<option value="<%=RSDSP("description")%>"><%=RSDSP("description")%></option>
		<%
					RSDSP.movenext
					wend
				RSDSP.close
		%>
      </select>
    </td>
  </tr>

  <tr>
    <td align="right">Note to Pharmacist</td>
    <td>
      <textarea name="notes" cols="15" rows="5" onblur="checklength(210)" ></textarea>
    </td>
  </tr>

  <tr>
    <td align="right"><b>Substitution Permitted?</td>
    <td>
      <select name="SUB_ID">
		<%
		set RS1 = Server.CreateObject("ADODB.Recordset")
		RS1.open "select * from SS_SUBSTITUTIONS where sub_id in (0,1)", con
		while not rs1.eof
		%>
			<option value="<%=RS1("SUB_ID")%>"><%=RS1("DESCRIPTION")%></option>
		<%
		 RS1.movenext
		wend
		RS1.close
		%>
	 </select>
    </td>
  </tr>

  <tr><td align="right">Prescribed Date</td>
	<td>
		<table border="0">
		<tr>
			<td><input type=text name=start_date id="dt" size=10 value="<%=date()%>" readonly></td>
			<td>&nbsp;<!--<a href="#"><img onClick="setDate(document.getElementById('dt'),1);" alt="Up" src="/images/practice/date_up_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a><br><img src="/images/spacer.gif" WIDTH="1" HEIGHT="1"><br><a href="#"><img onClick="setDate(document.getElementById('dt'),0);" alt="Down" src="/images/practice/date_down_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a>--></td>
		</tr>
		</table>
		(MM/DD/YYYY)
	</td>
	</td></tr>
 <!-- <tr>
    <td align="right"><b>Provider</td>
    <td><%=DOCTOR_NAME%></td>
  </tr>-->
</table>
<p>
<input type="submit" value="Submit" id="submit_button" name="submit_button">
<input type="button" value="Cancel" onclick="parent.leftNoteFrameTop.toggleTop(); parent.rightNoteFrame.location.replace('/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>')" id=button1 name=button1>
</form>

<font size=1>* PRN:As Needed</font>

</body>
</html>

<%
Function AscEncode(str)
    Dim i
    Dim sAscii

    sAscii = ""
    For i = 1 To Len(str)
        sAscii = sAscii + CStr(Hex(Asc(Mid(str, i, 1))))
    Next

    AscEncode = sAscii
End Function


Function ChrEncode(str)
    Dim i
    Dim sStr

    sStr = ""
    For i = 1 To Len(str) Step 2
        sStr = sStr + Chr(CLng("&H" & Mid(str, i, 2)))
    Next

    ChrEncode = sStr
End Function


%>