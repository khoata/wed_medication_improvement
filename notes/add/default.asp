<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->
<!--#include file="../include/templatedesign.asp"-->


<%

section = Request.QueryString("section")
pid = Request.QueryString("pid")
visit_key = Request.QueryString("visit_key")
facility_id = session("pid")
show_all = Request.QueryString("show_all")
dim auditObj
set auditObj = new clsAudit

dim patientNotesObj
set patientNotesObj = new clsPatientNotes
patientNotesObj.SetVisitKey(visit_key)
patient_id = patientNotesObj.ID
call auditObj.LogActivity("Accessing EMR","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)

if patientNotesObj.Closed = true and section <> "Reports" and section <> "Procedure" and section <> "Surgicalx" and section <> "PMH" and section <> "FH" then
  Response.Write "<h1>Note has already been closed.</h1>"
  Response.End
end if

template_type = patientNotesObj.SectionToTemplateType(section)
set RS = Server.CreateObject("ADODB.Recordset")
set RS_images = Server.CreateObject("ADODB.Recordset")
sqlQueryImages = "SELECT image_id,image_name FROM notes_template_images order by image_name asc"
RS_images.Open sqlQueryImages, CONN
image_list_url = "<BR>"
while not rs_images.EOF
	image_name = RS_images("image_name")
	dim str
	str = "parent.frames.rightNoteFrame.location.replace(""/template_images/image_update.aspx?visit_key=" & visit_key & "&template_type=" & template_type & "&image_id= " & RS_images("image_id") & """)"
	image_list_url = image_list_url & "<a href='#' onClick='" & str & "'> " & image_name & "</a><BR>"	
	
	rs_images.MoveNext ()
wend
facility_id_list = "'" & session("pid") & "'"
facility_id_list = facility_id_list & ",'SYSTEM'"

if show_all <> "" then
  sqlQuery = "SELECT * FROM NOTES_TEMPLATE_INDEX WHERE TEMPLATE_TYPE='" & template_type & "' AND FACILITY_ID IN (" & facility_id_list & ") AND SEX IN ('B','" & PatientNotesObj.Gender & "') ORDER BY template_name ASC"
else
  sqlQuery = "SELECT a.* FROM NOTES_TEMPLATE_INDEX a, NOTES_SOAP_QUICKLIST b WHERE TO_CHAR(a.TEMPLATE_ID) = b.ID_1 and b.SECTION = '" & section & "' and a.TEMPLATE_TYPE='" & template_type & "' AND a.FACILITY_ID IN (" & facility_id_list & ") AND SEX IN ('B','" & patientNotesObj.Gender & "') and b.user_id = '" & session("user") & "' ORDER BY a.template_name ASC"
end if
RS.Open sqlQuery, CONN

'Response.Write sqlQuery

%>
<html>
<head>

<%
'--upload progressbar -start
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = "PID=" & UploadProgress.CreateProgressID()
barref = "framebar.asp?to=10&" & PID
'--end
%>

<SCRIPT LANGUAGE="JavaScript">
function ShowProgress()
{
  strAppVersion = navigator.appVersion;
  if (document.upload_frm.blob.value != "")
  {
    if (strAppVersion.indexOf('MSIE') != -1 && strAppVersion.substr(strAppVersion.indexOf('MSIE')+5,1) > 4)
    {
      winstyle = "dialogWidth=385px; dialogHeight:140px; center:yes";
      window.showModelessDialog('<% = barref %>&b=IE',null,winstyle);
    }
    else
    {
      window.open('<% = barref %>&b=NN','','width=375,height=115', true);
    }
  }
  return true;
}
</SCRIPT>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>

<script type="text/javascript">
    <!--#include virtual= "/ASPSpellCheck/ASPSpellInclude.inc"-->
    <%
    dim myLink, strSpellURL
    set myLink = new AspSpellLink
    myLink.fields="__FCKEDITOR__"
    strSpellURL = myLink.url
    set myLink=nothing
    %>
    FCKAspSpellCheck = {};  FCKAspSpellCheck.URI = '<%=strSpellURL%>' ;
</script>

<script language="javascript">
function isSelected() {

  for (var i = 1; i < document.getElementById("template_id").length; i++) {
    if (document.getElementById("template_id").options[i].selected == true) {
      return true;
    }
  }

  alert("Please select a template to proceed with.");
  return false;

}

function gettemplate_id() {

  var e = document.getElementById("template_id");
           
          
return e.options[e.selectedIndex].value;
}



function showDiv(target) {

  if (target) {
    document.getElementById('template').style.display = 'none';
    document.getElementById('quicktext').style.display = 'none';
    document.getElementById('quickpen').style.display = 'none';
    document.getElementById('upload').style.display = 'none';
    document.getElementById('images').style.display = 'none';    
    document.getElementById('quicktext_tab').className = 'titleBox';
    document.getElementById('template_tab').className = 'titleBox';
    document.getElementById('quickpen_tab').className = 'titleBox';
    document.getElementById('upload_tab').className = 'titleBox';
    document.getElementById('images_tab').className = 'titleBox';
    document.getElementById(target).style.display = '';
    document.getElementById(target + '_tab').className = 'titleBox_HL';
    if (target == "template")
		document.getElementById('search_word').focus();
  }

  return true;

}

function clear_form()
{

	document.getElementById('quickpen_tab').className='titleBox_HL';
	document.getElementById('template_tab').className = 'titleBox';
	document.getElementById('quicktext_tab').className = 'titleBox';
	document.getElementById('template').style.display = 'none';
	document.getElementById('quicktext').style.display = 'none';
	document.getElementById('quickpen').style.display = '';
	document.getElementById('comments').focus();
	window.location.reload();
}
function submit_form(type,mode)
{
	if (type == 'pen')
	{
		document.pen_frm.samplingrate.value = document.iSign1.samplingrate;
		document.pen_frm.signature.value = document.iSign1.signature;
	}

	document.pen_frm.submit();

}


function file_size()
{
	var oas = new ActiveXObject("Scripting.FileSystemObject");
	var d = document.upload_frm.blob.value;
 if (d != ''){
	var e = oas.getFile(d);
	var f = e.size;
	//alert(f + " bytes");

	if (f>=30000)
	{	alert('File is too large to upload.  File size must be no bigger than 3 MB per file');
		return false;
	}else{return true;}

 }else{return false;}
}

function Checkfiles()
{
	var fup = document.getElementById('blob');
	var fileName = fup.value;
	var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
	//ext == "JPEG" || ext == "jpeg" || ext == "jpg"|| ext == "doc" || ext == "gif" || ext == "pdf"
	if(ext == "doc" || ext == "pdf")
	{
		return true;
	} 
	else
	{
		alert("Upload gif,jpeg,jpg,doc,pdf files only");
		fup.focus();
		return false;
	}
}

function getWords(wordString) {

	return wordString.split();

}

function inArray(hayStack, searchArrayName) {

	for (var i = 0; i < searchArrayName.length; i++) {
		if (hayStack.search(searchArrayName[i]) != -1) {
			return true;
		}
	}

	return false;

}

function sortByName(a, b) {

	if (a[1] < b[1])
		return -1
	if (a[1] > b[1])
		return 1
	if (a[1] == b[1])
		return 0

}

function refreshList() {

	searchWords = document.getElementById('search_word').value.toLowerCase().replace(/^\s+(.*)\s+$/,"$1");

	if (searchWords.length > 0) {

		foundList = new Array();
		var searchArray = getWords(searchWords);

		for (var i = 0; i < templateList.length; i++) {
			if (inArray(templateList[i][1].toLowerCase(),searchArray)) {
				foundList.push(new Array(templateList[i][0],templateList[i][1]));
			}
		}

		foundList.sort(sortByName);

		if (timerID != null)
		  clearTimeout(timerID);

		timerID = setTimeout("addOptions(foundList)",200);

	} else {
		addOptions(templateList);
	}

}

function addOptions(arrayName) {

	targetSelect = document.getElementById('template_id');

	for (var j = 0; j < 10; j++)
		for (var i = 0; i < targetSelect.options.length; i++) {
			targetSelect.options[i] = null;
		}
	var option1 = new Option("Select Template","Select Template");
		targetSelect.options.add(option1);
	for (var i = 0; i < arrayName.length; i++) {
		var option = new Option(arrayName[i][1],arrayName[i][0]);
		targetSelect.options.add(option);
	}

}

var timerRunning = false;
var timerID = null;
var templateList = new Array();
var foundList = new Array();

<%
while not RS.EOF

if RS("facility_id") = "SYSTEM" then
%>templateList.push(new Array("<%=RS("template_id")%>","<%=JSFixUp(RS("template_name"))%>"));
<%
else
%>templateList.push(new Array("<%=RS("template_id")%>","<%=JSFixUp(RS("template_name"))%>*"));
<%
end if

RS.MoveNext
wend
RS.Close
%>

</script>
</head>
<body onload="highlightAccessKeys(); document.getElementById('search_word').focus(); refreshList();">
<div class="titleBox">Visit Notes &gt; Add Note &gt; <%=section%></div>
<br><img src="/images/spacer.gif" height="3" width="5"><br>
<%' if not patientNotesObj.Closed and section <> "Reports" and section <> "Procedure" and section <> "Surgical" and section <> "PMH" then %>
<div class="titleBox_HL" id="template_tab" onclick="showDiv('template');" style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Template</font></div>
<div class="titleBox" id="quicktext_tab" onclick="if (showDiv('quicktext')); " style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Text</font></div>
<div class="titleBox" id="quickpen_tab" onclick="if (showDiv('quickpen')){ document.getElementById('comments').focus(); };" style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Pen</font></div>
<div class="titleBox" id="upload_tab" onclick="showDiv('upload');" style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Upload</font></div>
<div class="titleBox" id="images_tab" onclick="showDiv('images');" style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Images</font></div>
<%' else %>
<!--<div class="titleBox" id="template_tab" style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Template</font></div>
<div class="titleBox" id="quicktext_tab" style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Text</font></div>
<div class="titleBox" id="quickpen_tab" style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Pen</font></div>
<div class="titleBox_HL" id="upload_tab" onclick="showDiv('upload');" style="display: inline;cursor: pointer; cursor: hand;"><font class="st">Upload</font></div>-->
<%' end if %>
<br><img src="/images/spacer.gif" height="3" width="5"><P>
<div id="template" <% if patientNotesObj.Closed and section <> "Reports" and section <> "Procedure" and section <> "Surgical" and section <> "PMH" then %> style="display:none;" <% end if %> >
<%
if show_all <> "" then
%>
<b>Viewing All Templates</b>
<%
else
%>
<b>Viewing QuickList</b>
<%
end if
%>
<p>
<form action="template.asp" method="get" name="add_select_template" id="add_select_template">
<input id="search_word" type="text" onkeyup="refreshList();" style="width: 265px">
<select size="15" style="width: 265px" name="template_id" id="template_id" accesskey='t'>


</select>
<p>
<span style="font-size: 10px">An asterisk (*) denotes a customized template.</span>
</p>
<input type="hidden" name="section" value="<%=section%>">
<input type="hidden" name="template_type" value="<%=template_type%>">
<input type="hidden" name="visit_key" value="<%=visit_key%>">
<% dim designtype

designtype= GetTemplateDesignbyUserID(session("user"))
if designtype <> "" then
	designtype = cint(designtype)
else
	designtype = 0
end if
%>

<%'= designtype %>

 <%if designtype=0 or designtype=1 then %>
<button name="select" value="1" onclick="if (isSelected()) { document.add_select_template.submit(); }; return false;" accesskey='s'>Select Template</button>
<% end if %>
   
<% if  designtype=2 Then %>
<button name="selectnew" value="1" onclick="if (isSelected()) { parent.rightNoteFrame.location.href = '/practice/notes/Templates/Template_detail.aspx?visit_key=<%=visit_key%>&amp;template_id='+ gettemplate_id() +'&amp;facility_id=<%= session("pid") %>&amp;user_id=<%= session("user") %>'; }; return false;" accesskey='s'>Select Template</button>
<% end if %>

<% if show_all <> "" then %>
<button onclick="document.location.replace('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=<%=section%>'); return false;" id="button1" name="button1" accesskey='v'>View QuickList</button>
<% else %>
<button onclick="document.location.replace('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=<%=section%>&amp;show_all=1'); return false;" accesskey='v' id=button4 name=button4>View All</button>
<% end if %>
<p>
<button onclick="parent.leftNoteFrameTop.toggleTop(); return false;"  accesskey='c' id=button5 name=button5>Cancel</button>
</form>
</div>

<div style="color:red">New Template Design Available.  Go to the <a href="/practice/admin/customization/soap/design.asp?loc=notesdefault&visit_key=<%=visit_key%>&section=<%=section%>" target="rightNoteFrame">Customization section</a> to change your EMR to the new template format</div>

<div id="quicktext" style="display:none;">
<%'--find the browser type
If InStr(1, Request.ServerVariables("HTTP_USER_AGENT"), "MSIE") then
	b_type="IE"
elseif InStr(1, Request.ServerVariables("HTTP_USER_AGENT"), "Firefox") then
	b_type="FF"
else
	b_type="Non-IE"
end if
%>

<form action="custom_note.asp" method="post" name="add_custom_note" id="add_custom_note">
To add your own notes to the <%=section%> section, type them here:
<p>
<!-- #INCLUDE virtual="/fckeditor/fckeditor.asp" -->

<%if b_type="IE" then
	Dim sBasePath
	Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.BasePath	= "/fckeditor/"
	'oFCKeditor.Value	= "<b>ASP<font color='#BD0202'>Spell</font>Check</b> FCK Editor Pllugin- Spellcheck HTML content with seeamless FCK toolbar integration."
	oFCKeditor.ToolbarSet = "Basic"
	oFCKeditor.Create "custom_note"
else
%>
	<textarea rows="10" name="custom_note" cols="30"></textarea>
<%
end if
%>

<p>
<input type="hidden" name="section" value="<%=section%>">
<input type="hidden" name="template_type" value="<%=template_type%>">
<input type="hidden" name="visit_key" value="<%=visit_key%>">
<button onclick="if (document.add_custom_note.custom_note.value.length >= 0) { document.add_custom_note.submit() } else { alert('Please enter a note.'); }; return false;" name="add" value="1" accesskey='a'>Add</button>
<button onclick="parent.leftNoteFrameTop.toggleTop(); return false;" id="button3" name="button3" accesskey='c'>Cancel</button>
</form>
</div>


<div id="quickpen" style="display:none;">
<form name="pen_frm" id="pen_frm" method="post" action="insert_file.asp?input_type=pen">
<input type="hidden" name="section" value="<%=section%>">
<input type="hidden" name="template_type" value="<%=template_type%>">
<input type="hidden" name="visit_key" value="<%=visit_key%>">
<input name="signature" type="hidden">
<input name="samplingrate" type="hidden">

<b>Title:</b>&nbsp;<input type="text" name="comments" size="30" maxlength="id="comments" accesskey='p'>
<p>
<object id="Microsoft_Licensed_Class_Manager_1_0" classid="clsid:5220cb21-c88d-11cf-b347-00aa00a28331x" width="0" height="0"><param NAME="LPKPath" VALUE="/isign/iSign.lpk"></object>
<p>
<object id="iSign1" style="WIDTH: 265px; COLOR: black; HEIGHT: 265px; WRITINGGUIDE: NO" name="iSign1"    codebase="../../../../../../../../../../../iSign/Cab/iSign.cab#version=2,0,0,1038" classid="clsid:93E5204A-4344-4381-8912-8A7091E0FAE5" data="data:application/x-oleobject;base64,SiDlk0RDgUOJEopwkeD65QADAAAXNAAA2BMAABMA////ABMAAAAAAAMAAgAAABMAAAD/AAMAAgAAAAMAAgAAAAMA7gIAAAMAAAAAAAMAAgAAAAgAAgAAAAAA" width="450" height="192" border=0 VIEWASTEXT>
<PARAM NAME="WritingGuide" value="no">
<PARAM Name="InkColor" value="green">
</object><p>
<p>
<p>
<input type="button" value="Submit" onclick="javascript:submit_form('pen','<%=session("mode")%>')" id=button2 name=button2> <input name="clear_btn" type="button" value="Clear" onclick="javascript:clear_form()" accesskey='c'> <button onclick="parent.leftNoteFrameTop.toggleTop(); return false;" id="button3" name="button3" accesskey='c'>Cancel</button>

</form>
</div>

<div id="upload" style="display:none;" >
<form name="upload_frm" id="upload_frm" method="post" action="insert_file.asp?<%=PID%>&input_type=file" ENCTYPE="multipart/form-data" OnSubmit="return ShowProgress();">
<input type="hidden" name="section" value="<%=section%>">
<input type="hidden" name="template_type" value="<%=template_type%>">
<input type="hidden" name="visit_key" value="<%=visit_key%>">

<p><font color=red size=1>Note: File size must be no bigger than 3 MB per file. Please convert the files to PDF format prior to upload for faster file upload.</font></p>

<p><b>File:</b> <br><input TYPE="file" NAME="blob"><p>
<p><b>Folder:</b><select name="folder_name">
					<option value=""></option>
                    <option value="ABI">ABI</option>
					<option value="Anesthesiology">Anesthesiology</option>
					<option value="Authorizations">Authorizations</option>
					<option value="Approvals">Approvals</option>
                    <option value="Allergy">Allergy</option>
                    <option value="AdvanceDirective">Advance Directive</option>
                    <option value="Arbitration">Arbitration</option>
                    <option value="Billing">Billing</option>
					<option value="Cardiology">Cardiology</option>
					<option value="ClinicNotes">Clinic Notes</option>
					<option value="Consents">Consents</option>
					<option value="Co-pay">Co-pay</option>
					<option value="Correspondence">Correspondence</option>
					<option value="CosmeticDiagrams">Cosmetic Treatment Diagrams</option>
					<option value="Consult">Consult</option>
					<option value="Cosmetic">Cosmetic</option>
					<option value="Cultures">Cultures</option>
					<option value="Dermatology">Dermatology</option>
					<option value="Denials">Denials</option>
                    <option value="Dental">Dental</option>
					<option value="Disability">Disability</option>
                    <option value="Development">Development</option>
					<option value="DME">DME</option>
					<option value="DOX Billing">DOX Billing</option>
					<option value="DOX Clinical Notes">DOX Clinical Notes</option>
					<option value="DOX Clinical Summaries">DOX Clinical Summaries</option>
					<option value="DOX Order">DOX Order</option>
					<option value="DOX Referrals">DOX Referrals</option>
					<option value="EKG">EKG</option>
					<option value="ENT">ENT</option>
                    <option value="Eligibility">Eligibility</option>
					<option value="Emergency Room">Emergency Room</option>					
					<option value="Endocrinology">Endocrinology</option>
					<option value="ExternalPhotos">External Photos</option>
					<option value="EyeDiagram">Eye Diagram</option>
                    <option value="Epworth">Epworth</option>
					<option value="FMLA">FMLA/Others</option>
					<option value="Gastroenterology">Gastroenterology</option>
					<option value="Gynecology">Gynecology</option>
					<option value="Hematology">Hematology</option>
                    <option value="HealthHistory">Health History</option>
					<option value="HomeHealth">Home Health</option>					
					<option value="Hospital Records">Hospital Records</option>
					<option value="HP Consult">H&P Consult</option>
                    <option value="HIPAA">HIPAA</option>
					<option value="Insurance">Insurance</option>
                    <option value="Infectious Disease">Infectious Disease</option>                    
                    <option value="InternalMedicine">Internal Medicine</option>
                    <option value="Immunization">Immunization</option>
					<option value="Labs">Labs</option> 
					<option value="LabOrder">Lab Order</option>
					<option value="Microbiology">Microbiology</option>
                    <option value="MedicalRecords">Medical Records</option>
                    <option value="MedicalRelease">Medical Release</option>
                    <option value="Medication">Medication</option>
					<option value="Nephrology">Nephrology</option>
					<option value="Neurology">Neurology</option>
                    <option value="Office Notes">Office Notes</option>
					<option value="Oncology">Oncology</option>
					<option value="Ophthalmology">Ophthalmology</option>
					<option value="OperativeReport">Operative Report</option>
					<option value="OperativeDiagram">Operative Diagram</option>
					<option value="Orthopedic">Orthopedic</option>
					<option value="Others">Others</option>
                    <option value="OSA">OSA</option>
                    <option value="OBGYN">OBGYN</option>
                    <option value="PADT">PADT</option>
                    <option value="PatientFolder">Patient Folder</option>
					<option value="Pathology">Pathology</option>
					<option value="PatientIntakeInfo">Patient Intake Info</option>
					<option value="PainManagement">Pain Management</option>
                    <option value="PartialExam">Partial Exam</option>
					<option value="Photomedex">Photomedex</option>
					<option value="PhysicalTherapy">Physical Therapy</option>
					<option value="PhysicianOrders">Physician Orders</option>
                    <option value="PhysicalMedicine">Physical Medicine</option>
					<option value="Pictures">Pictures</option>
					<option value="Podiatry">Podiatry</option>
					<option value="Protime">Protime</option>
					<option value="Pre-op">Pre-op</option>
                    <option value="ProgressNotes">Progress Notes</option>
					<option value="PSG">PSG</option>
					<option value="PKU">PKU</option>					
					<option value="Psychiatry">Psychiatry</option>
					<option value="Pulmonology">Pulmonology</option>
                    <option value="Physical">Physical</option>
					<option value="Quote">Quote</option>
					<option value="Radiology">Radiology</option>
                    <option value="Registration">Registration</option>
					<option value="Referral Notes">Referral Notes</option>
					<option value="Rheumatology">Rheumatology</option>
					<option value="Rx">Rx</option>
                    <option value="SchoolPhysical">SchoolPhysical</option>
					<option value="Sleep Notes">Sleep Notes</option>
					<option value="SpecialTest">Special Test</option>
					<option value="SpeechandHearing">Speech and Hearing</option>
					<option value="Split Night">Split Night</option>
					<option value="Surgery">Surgery</option>
                    <option value="SOAP">SOAP</option>
                    <option value="SpecialistNote">Specialist Note</option>
                    <option value="Superbill">Superbill</option>
					<option value="Titration Study">Titration Study</option>
					<option value="Urology">Urology</option>
					<option value="UrgentCare">Urgent Care</option>
                    <option value="VascularStudies">VascularStudies</option>
					<option value="WeightManagement">Weight Management</option>
                    <option value="WellChildsExam">Well Childs Exam</option>
					<option value="WorkersComp">Workers Comp</option>
					<option value="WoundCare">Wound Care</option>
					<option value="X-Ray">X-Ray</option>
				 </select>
<p><b>Description: (optional)</b> <br>
<textarea rows="3" cols="25" name="comment"></textarea><br>
(200 characters max)
<p>
<input type="submit" value="Submit" id=submit1 name=submit1 >
<input type="reset" value="Clear" accesskey='c' id=reset1 name=reset1>
<button onclick="parent.leftNoteFrameTop.toggleTop(); return false;" id="button3" name="button3" accesskey='c'>Cancel</button>
</form>
</div>

<div id="images" style="display:none;">
<form action="/template_images/image_update.aspx?image_id=image_id" method="post" name="image_update" id="image_update">
<input type="hidden" name="section" value="<%=section%>">
<input type="hidden" name="template_type" value="<%=template_type%>">
<input type="hidden" name="visit_key" value="<%=visit_key%>">
<b>Choose Image Name:</b>&nbsp;
<%=image_list_url%>
<p>
<p>

<%
if conn.state=1 then
    conn.close()
end if
 %>
<button onclick="parent.leftNoteFrameTop.toggleTop(); return false;" id="button3" name="button3">Cancel</button>
</form>
</div>

</body>
</html>