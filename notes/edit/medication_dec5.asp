<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/functions.inc.asp"-->
<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id")

'dim patientNotesObj
'set patientNotesObj = new clsPatientNotes
'patientNotesObj.SetVisitKey(visit_key)

'if patientNotesObj.Closed = true then
'  Response.Write "<h1>Note has already been closed.</h1>"
'  Response.End
'end if

if Request.Form("submit_button") <> "" and drug_id <> "" then


  dim CMD
  
  set CMD = Server.CreateObject("ADODB.Command")

  CMD.ActiveConnection = CONN


  drug_name =  Request.Form("nm_med")
  formulation = Request.Form("formulation")
  dispense = Request.Form("dispense") & " " & Request.Form("dispense_units")
  dosage = Request.Form("dosage") & " " & Request.Form("dosage_units")
  frequency = Request.Form("freq")
  route = Request.Form("route")
  refill = Request.Form("refill")
  prescribed_by = session("user")
  notes = Request.Form("notes")
  start_date = Request.Form("start_date")
   
  CONN.BeginTrans

  sqlQuery = "UPDATE notes_medications SET drug_name='" & sqlFixUp(drug_name) & "',formulation='" & sqlFixUp(formulation) & "',dispense='" & sqlFixUp(dispense) & "',dose='" & sqlFixUp(dosage) & "',frequency='" & sqlFixUp(frequency) & "',route='" & sqlFixUp(route) & "',refill='" & sqlFixUp(refill) & "',notes='" & sqlFixUp(notes) & "',prescribed_date=to_date('"&start_date&"','MM/DD/YYYY')  where drug_id='" & drug_id & "' and patient_id='" & patient_id & "'"
    
  CMD.CommandText = sqlQuery
  CMD.Execute 

  CONN.CommitTrans
  
  %>
  <script language="javascript">
    parent.rightNoteFrame.location.reload(true);
    document.location='/practice/notes/edit/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=<%=drug_id%>&msg=Changes Saved.';
  </script>
  <%
  
  Response.End
  
else

  dim RSDrug
  set RSDrug = Server.CreateObject("ADODB.Recordset")
  
  RSDrug.CursorLocation = 3
  sqlQuery = "SELECT * FROM notes_medications WHERE drug_id='" & drug_id & "'"
  
  RSDrug.Open sqlQuery, CONN, 3, 4
  
  'visit_key = RSDrug("visit_key")
  drug_name =  RSDrug("drug_name")
  formulation = RSDrug("formulation")
  dispense_ar = split(trim(RSDrug("dispense")), " ")
  if ubound(dispense_ar) > 0 then
    dispense = dispense_ar(0)
    dispense_units = dispense_ar(1)
  else
    dispense = trim(RSDrug("dispense"))
  end if
  dosage_ar = split(trim(RSDrug("dose")), " ")
  if ubound(dosage_ar) > 0 then
    dosage = dosage_ar(0)
    dosage_units = dosage_ar(1)
  else
    dosage = trim(RSDrug("dose"))
  end if
  frequency = RSDrug("frequency")
  route = RSDrug("route")
  refill = RSDrug("refill")
  notes = RSDrug("notes")
  start_date = RSDrug("prescribed_date")
end if


set RSVitals = Server.CreateObject("ADODB.Recordset")
RSVitals.CursorLocation = 3
sqlQuery = "SELECT DISTINCT take FROM notes_vitals WHERE visit_key='" & visit_key & "'"
RSVitals.Open sqlQuery, CONN, 3, 4
VCount = RSVitals.RecordCount
RSVitals.Close

if VCount > 0 then
  sqlQuery = "SELECT * FROM notes_vitals WHERE visit_key='" & visit_key & "' AND vital_type='W' ORDER BY take DESC"
  RSVitals.Open sqlQuery, CONN, 3, 4
  set RSVitals.ActiveConnection = nothing
  
  dim VCounter
  
  if RSVitals.RecordCount > 0 then
    weight = RSVitals("vital_data1") & "kgs (" & Round(RSVitals("vital_data1").Value / .4536,2) & "lbs)"
  end if
  
end if

if weight = "" then
  weight = "Not recorded yet."
end if

Set DoctorDataSet = GetDoctorData(visit_key)
DOCTOR_NAME = DoctorDataSet("Last_Name")&", "&DoctorDataSet("First_Name")
DoctorDataSet.Close
Set DoctorDataSet = nothing

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script language="javascript">
function validateForm(that) {

  if (that.nm_med.value == "") {
    alert("Please select a medication to prescribe.");
    that.nm_med.focus();
    return false;
  }
    
  if (that.formulation.value == "") {
    alert("Please describe the medication's formulation.");
    that.formulation.focus();
    return false;
  }
  
  var re = /^\d+$/;
  
  /*
  if (!that.dispense.value.match(re)) {
    alert("Please enter an appropriate amount of drug to dispense.");
    that.dispense.focus();
    return false;
  }

  if (that.dispense_units.selectedIndex <= 0) {
    alert("Please select an appropriate dispense unit.");
    that.dispense_units.focus();
    return false;
  }

  if (!that.dosage.value.match(re)) {
    alert("Please enter an appropriate dosage.");
    that.dosage.focus();
    return false;
  }
  */

  if (that.dosage_units.selectedIndex <= 0) {
    alert("Please enter an appropriate dosage unit.");
    that.dosage_units.focus();
    return false;
  }

  if (that.freq.selectedIndex == 0 || that.freq.selectedIndex == 13) {
    alert("Please select the frequency of dosage.");
    that.freq.focus();
    return false;
  }
  

  if (that.route.selectedIndex <= 0) {
    alert("Please select the route of administering dosage.");
    that.route.focus();
    return false;
  }
  
  var startDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
  if (that.start_date.value == "") {
	alert("Please set the Medication Start Date.");
	that.start_date.focus();
	return false;
	}
  else
  {
	if (!that.start_date.value.match(startDate))
	{
		alert("Please enter the start date in correct format(MM/DD/YYYY)");
		return false;
	}
  }
  return true;

}

function setDosage(that) {
  
  for (var i = 0; i < that.length; i++) {
    
    if (that[i].selected == true && that[i].text == 'Other') {
      
      document.getElementById('dosage_tr').style.display = '';
      document.getElementById('dosage_units').value = '';
      document.getElementById('dosage_units').focus();
      
      return true;
    } 
    if (that[i].selected == true && that[i].text != 'Other') {
      document.getElementById('dosage_tr').style.display = 'none';
      document.getElementById('dosage_units').value = that[i].text;
      return true;
    }
  } 
  
}

function setFreq(that) {

  for (var i = 0; i < that.length; i++) {
    if (that[i].selected == true && that[i].text == 'Other') {
      document.getElementById('freq_tr').style.display = '';
      document.getElementById('freq').value = '';
      document.getElementById('freq').focus();
      
      return true;
    } 
    if (that[i].selected == true && that[i].text != 'Other') {
      document.getElementById('freq_tr').style.display = 'none';
      document.getElementById('freq').value = that[i].text;
      return true;
    }
  } 
  
}

function setRoute(that) {

  for (var i = 0; i < that.length; i++) {
    if (that[i].selected == true && that[i].text == 'Other') {
      document.getElementById('route_tr').style.display = '';
      document.getElementById('route').value = '';
      document.getElementById('route').focus();
      
      return true;
    } 
    if (that[i].selected == true && that[i].text != 'Other') {
      document.getElementById('route_tr').style.display = 'none';
      document.getElementById('route').value = that[i].text;
      return true;
    }
  } 
  
}

function setInitRoute(that) {
  var inlist = false;
  
  if (document.getElementById('route').value.length > 0) {
    
    for (var i = 0; i < that.length; i++) {
      if (that[i].text == document.getElementById('route').value) {
        that[i].selected = true;     
        inlist = true;
        
      } 
    }
    
    if (inlist == false) {
    
      for (var i = 0; i < that.length; i++) {
        if (that[i].text == 'Other') {
          that[i].selected = true;     
        } 
      }    
      document.getElementById('route_tr').style.display = '';
    }
  }
  
}

function setInitFreq(that) {
  var inlist = false;
  
  if (document.getElementById('freq').value.length > 0) {
    
    for (var i = 0; i < that.length; i++) {
      if (that[i].text == document.getElementById('freq').value) {
        that[i].selected = true;     
        inlist = true;
        
      } 
    }
    
    if (inlist == false) {
    
      for (var i = 0; i < that.length; i++) {
        if (that[i].text == 'Other') {
          that[i].selected = true;     
        } 
      }    
      document.getElementById('freq_tr').style.display = '';
    }
  }
  
}
function setInitDosage(that) {
  var inlist = false;
  
  if (document.getElementById('dosage_units').value.length > 0) {
    
    for (var i = 0; i < that.length; i++) {
      if (that[i].text == document.getElementById('dosage_units').value) {
        that[i].selected = true;     
        inlist = true;
        
      } 
    }
    
    if (inlist == false) {
    
      for (var i = 0; i < that.length; i++) {
        if (that[i].text == 'Other') {
          that[i].selected = true;     
        } 
      }    
      document.getElementById('dosage_tr').style.display = '';
    }
  }
  
}

function initSelect() {

  setInitRoute(document.getElementById('route_select'));
  setInitFreq(document.getElementById('freq_select'));
  setInitDosage(document.getElementById('dosage_units_select'));

}
function setDate(frmObj, d) {

  var month, day, year;
  re = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
  
  if (frmObj.value.match(re)) {

    var now = new Date(frmObj.value);

    if (d == 0) {
      now.setTime(now.getTime() - 24 * 60 * 60 * 1000);
      month = now.getMonth() + 1;
      day = now.getDate();
      year = now.getYear();
      
      if (year < 1000)
        year = year + 1900;
      
      frmObj.value = month + "/" + day + "/" + year;
    } else {
      now.setTime(now.getTime() + 24 * 60 * 60 * 1000);
      month = now.getMonth() + 1;
      day = now.getDate();
      year = now.getYear();
      if (year < 1000)
        year = year + 1900;      
      frmObj.value = month + "/" + day + "/" + year;
    }
      
  
  } else {
    var today = new Date();
    month = today.getMonth() + 1;
    day = today.getDate();
    year = today.getYear();
    if (year < 1000)
      year = year + 1900;   
    frmObj.value = month + "/" + day + "/" + year;

  }
}
</script>
</head>
<body onload="initSelect()">
<div class="titleBox">Edit Medication</div>
<p>
<% if Request("msg") <> "" then %>
<span style="color: green"><b><%=Request("msg")%></b></span>
<p>
<% end if %>
Fields in <b>BOLD</b> are required.
<p>
<form name="frm2" id="frm2" action="medication.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>&amp;drug_id=<%=drug_id%>" method="post" onsubmit="return validateForm(this)">
<table>
  <tr>
    <td align="right"><b>Patient Weight</td>
    <td><%=weight%></td>
  </tr>
  <tr>
    <td align="right"><b>Drug Name</td>
    <td><input maxlength="100" name="nm_med" size="20" value="<%=drug_name%>" onchange="checkvalue()"> <a href="#" onclick="parent.rightNoteFrame.location='/practice/notes/display/medication_search.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'"><img border="0" height="18" src="/images/global/icon_search.gif"></a></td>
  </tr>
  <tr>
    <td align="right"><b>Formulation</td>
    <td><input name="formulation" value="<%=formulation%>" size="25"></td>
  </tr>
  <tr>
    <td align="right"><b>Dose</td>
    <td>
      <input name="dosage" value="<%=dosage%>" size="5">
      <select id="dosage_units_select" name="dosage_units_select" onchange="setDosage(this);">
      <option>
      <% if dosage_units = "Milligrams" then %><option selected><% else %><option><% end if %>Milligrams
      <% if dosage_units = "Micrograms" then %><option selected><% else %><option><% end if %>Micrograms
      <% if dosage_units = "Milliliters" then %><option selected><% else %><option><% end if %>Milliliters
      <% if dosage_units = "Tablet(s)" then %><option selected><% else %><option><% end if %>Tablet(s)
      <% if dosage_units = "Capsule(s)" then %><option selected><% else %><option><% end if %>Capsule(s)
      <% if dosage_units = "Tablespoon(s)" then %><option selected><% else %><option><% end if %>Tablespoon(s)
      <% if dosage_units = "Teaspoon(s)" then %><option selected><% else %><option><% end if %>Teaspoon(s)
      <% if dosage_units = "Drop(s)" then %><option selected><% else %><option><% end if %>Drop(s)
      <% if dosage_units = "Puff(s)" then %><option selected><% else %><option><% end if %>Puff(s)
      <% if dosage_units = "Inhalation(s)" then %><option selected><% else %><option><% end if %>Inhalation(s)
      <% if dosage_units = "Suppository(s)" then %><option selected><% else %><option><% end if %>Suppository(s)
      <% if dosage_units = "Application(s)" then %><option selected><% else %><option><% end if %>Application(s)
      <% if dosage_units = "Troche(s)" then %><option selected><% else %><option><% end if %>Troche(s)
      <% if dosage_units = "Lozenge(s)" then %><option selected><% else %><option><% end if %>Lozenge(s)
      <% if dosage_units = "Pastille(s)" then %><option selected><% else %><option><% end if %>Pastille(s)
      <% if dosage_units = "Patch(es)" then %><option selected><% else %><option><% end if %>Patch(es)
      <% if dosage_units = "Ampoule(s)" then %><option selected><% else %><option><% end if %>Ampoule(s)
      <% if dosage_units = "Unit(s)" then %><option selected><% else %><option><% end if %>Unit(s)
      <option>Other
      </select>
    </td>
  </tr>
  <tr id="dosage_tr" style="display: none;">
    <td class="st" align="right"><i>other unit</td>
    <td><input type="text" id="dosage_units" name="dosage_units" value="<%=dosage_units%>"></td>
  </tr>

  <tr>
    <td align="right"><b>Frequency</td>
    <td>
      <select name="freq_select" id="freq_select" onchange="setFreq(this);">
        <option>
        <% if frequency = "Once Daily" then %><option selected><% else %><option><% end if %>Once Daily
        <% if frequency = "Twice Daily" then %><option selected><% else %><option><% end if %>Twice Daily
        <% if frequency = "Three Times Daily" then %><option selected><% else %><option><% end if %>Three Times Daily
        <% if frequency = "Four Times Daily" then %><option selected><% else %><option><% end if %>Four Times Daily
        <% if frequency = "Five Times Daily" then %><option selected><% else %><option><% end if %>Five Times Daily
        <% if frequency = "Six Times Daily" then %><option selected><% else %><option><% end if %>Six Times Daily
        <% if frequency = "Every Other Day" then %><option selected><% else %><option><% end if %>Every Other Day
        <% if frequency = "7:00am and 4:00pm" then %><option selected><% else %><option><% end if %>7:00am and 4:00pm
        <% if frequency = "At Bedtime" then %><option selected><% else %><option><% end if %>At Bedtime
        <% if frequency = "Once Weekly" then %><option selected><% else %><option><% end if %>Once Weekly
        <% if frequency = "Twice Weekly" then %><option selected><% else %><option><% end if %>Twice Weekly
        <% if frequency = "Three Times Weekly" then %><option selected><% else %><option><% end if %>Three Times Weekly
        <% if frequency = "As Needed" then %><option selected><% else %><option><% end if %>As Needed
        <option>
        <% if frequency = "Every 0.25 Hours" then %><option selected><% else %><option><% end if %>Every 0.25 Hours
        <% if frequency = "Every 0.50 Hours" then %><option selected><% else %><option><% end if %>Every 0.50 Hours
        <% if frequency = "Every 1 Hours" then %><option selected><% else %><option><% end if %>Every 1 Hours
        <%
          for i = 2 to 24
            if frequency = "Every " & i & " Hours" then
        %>
        <option selected>Every <%=i%> Hours
        <%
            else
        %>
        <option>Every <%=i%> Hours
        <%
            end if
          next
        %>
        <option>Other
      </select>
    </td>
  </tr>
  <tr id="freq_tr" style="display: none;">
    <td class="st" align="right"><i>other freq.</td>
    <td><input type="text" id="freq" name="freq" value="<%=frequency%>"></td>
  </tr>
  <tr>
    <td align="right"><b>Route</td>
    <td>
      <select name="route_select" id="route_select" onchange="setRoute(this);">
        <option>
      <%
        dim RSRoute
        set RSRoute = Server.CreateObject("ADODB.Recordset")
        RSRoute.CursorLocation = 3
        sqlQuery = "SELECT * FROM multum_route ORDER BY route_description"
        RSRoute.Open sqlQuery, CONN, 3, 4
        set RSRoute.ActiveConnection = nothing
        
        while not RSRoute.EOF 
          if RSRoute("route_abbr") = route then
      %>
        <option selected value="<%=RSRoute("route_abbr")%>"><%=PCase(RSRoute("route_description"))%>
      <%
          else
      %>
        <option value="<%=RSRoute("route_abbr")%>"><%=PCase(RSRoute("route_description"))%>
      <%
          end if
          RSRoute.MoveNext
        wend
      %>
        <option>Other
      </select>
    </td>
  </tr>
  <tr id="route_tr" style="display: none;">
    <td class="st" align="right"><i>other route</td>
    <td><input type="text" id="route" name="route" value="<%=route%>"></td>
  </tr>  
  <tr>
    <td align="right"><b>Refill</td>
    <td>
      <select name="refill" size="1" onchange="document.frm2.button1.focus();">
        <%
          for i = 0 to 30
            if cint(refill) = i then
        %>
        <option selected><%=i%>
        <%
            else
        %>
        <option><%=i%>
        <%
            end if
          next
        %>
        <% if refill = "60" then %><option selected><% else %><option><% end if %>60
        <% if refill = "90" then %><option selected><% else %><option><% end if %>90
        <% if refill = "120" then %><option selected><% else %><option><% end if %>120
      </select> times
    </td>
  </tr>
  <tr>
    <td align="right">Dispense</td>
    <td>
      <input name="dispense" value="<%=dispense%>" size="5">
      <select name="dispense_units">
      <option>
      <% if dispense_units = "Milligrams" then %><option selected><% else %><option><% end if %>Milligrams
      <% if dispense_units = "Micrograms" then %><option selected><% else %><option><% end if %>Micrograms
      <% if dispense_units = "Milliliters" then %><option selected><% else %><option><% end if %>Milliliters
      <% if dispense_units = "Grams" then %><option selected><% else %><option><% end if %>Grams
      <% if dispense_units = "Ounces" then %><option selected><% else %><option><% end if %>Ounces
      <% if dispense_units = "Tablets" then %><option selected><% else %><option><% end if %>Tablets
      <% if dispense_units = "Capsules" then %><option selected><% else %><option><% end if %>Capsules
      <% if dispense_units = "Suppositories" then %><option selected><% else %><option><% end if %>Suppositories
      <% if dispense_units = "Inhalers" then %><option selected><% else %><option><% end if %>Inhalers
      <% if dispense_units = "Applicators" then %><option selected><% else %><option><% end if %>Applicators
      <% if dispense_units = "Ampoules" then %><option selected><% else %><option><% end if %>Ampoules
      <% if dispense_units = "Lozenges" then %><option selected><% else %><option><% end if %>Lozenges
      <% if dispense_units = "Troches" then %><option selected><% else %><option><% end if %>Troches
      <% if dispense_units = "Pastilles" then %><option selected><% else %><option><% end if %>Pastilles
      <% if dispense_units = "Cartridges" then %><option selected><% else %><option><% end if %>Cartridges
      <% if dispense_units = "Packs" then %><option selected><% else %><option><% end if %>Packs
      <% if dispense_units = "Kits" then %><option selected><% else %><option><% end if %>Kits
      <% if dispense_units = "Patches" then %><option selected><% else %><option><% end if %>Patches
      <% if dispense_units = "Units" then %><option selected><% else %><option><% end if %>Units
      </select>
    </td>
  </tr>
 
  <tr>
    <td align="right"><b>Notes to Patient</td>
    <td>
      <textarea name="notes" cols="15" rows="5"><%=notes%></textarea>
    </td>
  </tr>  
 <!-- <tr>
    <td align="right"><b>Provider</td>
    <td><%=DOCTOR_NAME%></td>
  </tr>-->
  <tr><td align="right"><b>Start Date</td>
	<td>
		<table border="0">
		<tr>
			<td><input type=text name=start_date id="dt" size=10 value="<%=start_date%>"></td>
			<td><a href="#"><img onClick="setDate(document.getElementById('dt'),1);" alt="Up" src="/images/practice/date_up_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a><br><img src="/images/spacer.gif" WIDTH="1" HEIGHT="1"><br><a href="#"><img onClick="setDate(document.getElementById('dt'),0);" alt="Down" src="/images/practice/date_down_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a></td>
		</tr>
		</table>
		(MM/DD/YYYY)
	</td>
	</td></tr>
</table>
<p>
<input type="submit" value="Submit" id="submit_button" name="submit_button">
<input type="button" value="Cancel" onclick="parent.leftNoteFrameTop.toggleTop(); parent.rightNoteFrame.location.replace('/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>')" id="reset1" name="reset1">
</form>

</body>
</html>