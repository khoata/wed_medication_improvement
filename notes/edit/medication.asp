<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../../../library/clsEPCSAuditLog.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/functions.inc.asp"-->

<%
'dim auditObj
'set auditObj = new clsAudit

dim epcsAuditLogObj
set epcsAuditLogObj = new clsEPCSAuditLog

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id")
other_meds = Session("other_meds")

dim patientNotesObj
set patientNotesObj = new clsPatientNotes
patientNotesObj.SetVisitKey(visit_key)

patient_id = patientNotesObj.ID

'call auditObj.LogActivity("Accessing MEDICATION EDIT Page","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)


'dim patientNotesObj
'set patientNotesObj = new clsPatientNotes
'patientNotesObj.SetVisitKey(visit_key)

'if patientNotesObj.Closed = true then
 ' Response.Write "<h1>Note has already been closed.</h1>"
  'Response.End
'end if

if Request.Form("submit_button") <> "" and drug_id <> "" then


  dim CMD

  set CMD = Server.CreateObject("ADODB.Command")
  set CMD1 = Server.CreateObject("ADODB.Command")
  set CMD2 = Server.CreateObject("ADODB.Command")

  CMD.ActiveConnection = CONN
  CMD1.ActiveConnection = CONN

  err_msg = ""

  drug_name =  unescape(trim(Request.Form("nm_med")))
  formulation = unescape(trim(Request.Form("formulation")))
  dispense = trim(Request.Form("dispense"))
  dispense_units = trim(Request("dispense_units"))
  dosage = trim(Request.Form("dosage")) & " " & trim(Request.Form("dosage_units"))
  frequency = trim(Request.Form("freq"))
  route = trim(Request.Form("route"))
  refill = trim(Request.Form("refill"))
  if (refill = "") then
	refill = 0
  end if
  prescribed_by = session("user")
  notes = trim(Request.Form("notes"))
  start_date = trim(Request.Form("start_date"))
  sub_id = trim(Request("sub_id"))
  medical_type = trim(Request("medical_type"))

  Dim numRegEx
  Set numRegEx = New RegExp
  numRegEx.Pattern = "^(\.|\d)+$"
  dispenseRes = numRegEx.Test(dispense)

  if drug_name = "" then err_msg = err_msg & "Please select a medication to prescribe.<br/>"
  if formulation = "" and medical_type <> "SP" then err_msg = err_msg & "Please describe the medication's formulation.<br/>"
  if dispense  = "" then
    err_msg = err_msg & "Please enter the dispense quantity.<br/>"
  elseif not IsNumeric(dispense) or not dispenseRes then
    err_msg = err_msg & "Invalid dispense quantity.<br/>"
  elseif InStr(1, dispense, ".") <> 0 and Len(dispense) > 11 then
    err_msg = err_msg & "Dispense quantity can not be longer than 10 digits.<br/>"
  elseif InStr(1, dispense, ".") = 0 and Len(dispense) > 10 then
    err_msg = err_msg & "Dispense quantity can not be longer than 10 digits.<br/>"
  else
    dispenseDec = CDBl(dispense)
    if dispenseDec <= 0 then err_msg = err_msg & "Dispense quantity must not be zero.<br/>"
  end if
  if dispense_units = "" then err_msg = err_msg & "Please select an appropriate dispense unit.<br/>"
  if trim(Request("dosage")) = "" or trim(Request("dosage_units")) = ""  then err_msg = err_msg & "Please enter an appropriate dosage.<br/>"
  'if frequency = "" then err_msg = err_msg & "Please select the frequency of dosage.<br/>"
  'if route = "" then err_msg = err_msg & "Please select the route of administering dosage.<br/>"
  if notes <> "" and Len(notes) > 210 then err_msg = err_msg & "Notes can not be longer than 210 characters.<br/>"

if err_msg = "" then
  dispense = CDBl(dispense) & " " & trim(Request("dispense_units"))
  date_mod=date()

  CONN.BeginTrans

	sqlQuery = "SELECT drug_name,formulation,dispense,dose,frequency,route,refill,REFILL_REMAINING,notes,ndc_code,substitution,notes,prescribed_date FROM notes_medications WHERE drug_id='" & drug_id & "' and patient_id='" & patient_id & "'"
    CMD1.CommandText = sqlQuery
    dim RSSeq
    set RSSeq = Server.CreateObject("ADODB.Recordset")
    RSSeq = CMD1.Execute

  sqlQuery = "UPDATE notes_medications SET drug_name='" & sqlFixUp(drug_name) & "',formulation='" & sqlFixUp(formulation) & "',dispense='" & sqlFixUp(dispense) & "',dose='" & sqlFixUp(dosage) & "',frequency='" & sqlFixUp(frequency) & "',route='" & sqlFixUp(route) & "',refill='" & sqlFixUp(refill) & "',REFILL_REMAINING='" & sqlFixUp(refill) & "',notes='" & sqlFixUp(notes) & "',prescribed_date=to_date('"&start_date&"','MM/DD/YYYY'),modified_date=to_date('" & date_mod & "','MM/DD/YYYY'), SUBSTITUTION='"& sub_id &"'  where drug_id='" & drug_id & "' and patient_id='" & patient_id & "'"

  CMD.CommandText = sqlQuery
  CMD.Execute

  'CONN.CommitTrans

  'call auditObj.LogActivity("Modified a Medication with name - "+RSSeq("drug_name").value,RSSeq("formulation").value+","+Cstr(RSSeq("dispense").value)+","+Cstr(RSSeq("dose").value)+","+RSSeq("frequency").value+","+RSSeq("route").value+","+Cstr(RSSeq("refill").value)+","+Cstr(RSSeq("REFILL_REMAINING").value)+","+RSSeq("notes").value,Cstr(drug_name)+","+Cstr(formulation)+","+Cstr(dispense)+","+Cstr(dosage)+","+Cstr(frequency)+","+Cstr(route)+","+Cstr(refill)+","+Cstr(refill)+","+Cstr(notes),Cstr(patient_id),Cstr(visit_key),SYSDATE)

'Hari Krishnan
' create string
''' Logdata =  " from drug:" & RSSeq("drug_name").value & ",formulation:" & RSSeq("formulation").value & ",dispense:"& Cstr(RSSeq("dispense").value) & ",dose:"& Cstr(RSSeq("dose").value) & ",frequency:" & RSSeq("frequency").value & ",route:" & RSSeq("route").value &",refill"+Cstr(RSSeq("refill").value) &  " To drug: " & drug_name &  ",formulation:" &   formulation & ",dispense:" & dispense  & ",dispenseunits:" & dispense_units & ",dosage:" & dosage & ",frequency:" &   frequency & ",route:" & route & ",refill:" & refill

	OldLogdata = " From drug: " & RSSeq("drug_name").value & ", formulation: " & RSSeq("formulation").value & ", dispense: "& Cstr(RSSeq("dispense").value) & ", dose: "& Cstr(RSSeq("dose").value) & ", frequency: " & RSSeq("frequency").value & ", route: " & RSSeq("route").value &", refill: "+Cstr(RSSeq("refill").value)  & " "
	NewLogdata =  " Modified To drug: " & drug_name &  ", formulation: " &   formulation & " ,dispense: " & dispense  & " , dispenseunits: " & dispense_units & " , dosage: " & dosage & " , frequency: " &   frequency & " , route: " & route & ", refill: " & refill


  'call auditObj.LogActivity("Modified a Medication with name - "+RSSeq("drug_name").value,RSSeq("formulation").value+","+Cstr(RSSeq("dispense").value)+","+Cstr(RSSeq("dose").value)+","+RSSeq("frequency").value+","+RSSeq("route").value+","+Cstr(RSSeq("refill").value)+","+Cstr(RSSeq("REFILL_REMAINING").value)+","+RSSeq("notes").value,Cstr(drug_name)+","+Cstr(formulation)+","+Cstr(dispense)+","+Cstr(dosage)+","+Cstr(frequency)+","+Cstr(route)+","+Cstr(refill)+","+Cstr(refill)+","+Cstr(notes),Cstr(patient_id),Cstr(visit_key),SYSDATE)
set cmd = server.CreateObject("ADODB.command")
cmd.ActiveConnection = CONN
 	strSQL = "INSERT INTO audit_log(USERNAME,  SECTION_NAME, SUB_SECTION, VISIT_KEY, ACTION, FIELD,  OLD_VALUE, NEW_VALUE , TIME_STAMP , IP_ADDR, USER_ROLE, PATIENT_ID, PATIENT_LAST_NAME, PATIENT_FIRST_NAME, VISIT_DATE, VISIT_TIME) " &_
	" SELECT B.DOCTOR_ID,'','',B.visit_key, 'Physician Modified  medication'  , '','" & OldLogdata  & "','" & NewLogdata & "', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'),'','Physician', B.PATIENT_ID, A.last_name, A.first_name,  VISIT_DATE,VISIT_TIME  FROM PATIENTTABLE A, VISIT B WHERE A.USERID = B.patient_id AND B.visit_key = '" &  visit_key & "'"
'Response.Write strSQL
		cmd.CommandText = strSQL
		cmd.Execute
		
	'---START: Add to EPCS_Prescriptions
    set rs = server.CreateObject("ADODB.Recordset")
    strSQL = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and ndc_code = '" & RSSeq("ndc_code") & "'"
    rs.open strSQL,con
    if not (rs.eof or rs.bof) then
        'set cmd = server.CreateObject("ADODB.command")
        'cmd.ActiveConnection = CONN
	    'strSQL = "INSERT INTO EPCS_PRESCRIPTIONS(Drug_Id, Visit_Key, Patient_Id, Doctor_Id, Status, Created_Date) values ('" & drug_id & "','" & visit_key & "','" & patient_id & "','" & session("docid") & "', 'New', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'))"
        'cmd.CommandText = strSQL
	    'cmd.Execute

        '---START: Add to EPCS_AUDIT_LOG
        
        drugNameChange = ""
        if RSSeq("drug_name").value <> drug_name then drugNameChange = "- Drug Name: From " & RSSeq("drug_name").value & " to " & drug_name & "<br/>" end if
        formulationChange = ""
        if RSSeq("formulation").value <> formulation then formulationChange = "- Formulation: From " & RSSeq("formulation").value & " to " & formulation & "<br/>" end if
        dispenseChange = ""
        if RSSeq("dispense").value <> dispense then dispenseChange = "- Dispense: From " & RSSeq("dispense").value & " to " & dispense & "<br/>" end if
        dosageChange = ""
        if RSSeq("dose").value <> dosage then dosageChange = "- Dosage: From " & RSSeq("dose").value & " to " & dosage & "<br/>" end if
        frequencyChange = ""
        if RSSeq("frequency").value <> frequency then frequencyChange = "- Frequency: From " & RSSeq("frequency").value & " to " & frequency & "<br/>" end if
        durationChange = ""
        'if RSSeq("frequency").value <> frequency then frequencyChange = "- Frequency: From " & RSSeq("frequency").value & " to " & frequency & "<br/>" end if
        routeChange = ""
        if RSSeq("route").value <> route then routeChange = "- Route: From " & RSSeq("route").value & " to " & route & "<br/>" end if
        refillChange = ""
        if trim(Cstr(RSSeq("refill").value)) <> trim(refill) then refillChange = "- Refill: From " & Cstr(RSSeq("refill").value) & " to " & refill & "<br/>" end if
        noteChange = ""
        if RSSeq("notes").value <> notes then noteChange = "- Note: From " & RSSeq("notes").value & " to " & notes & "<br/>" end if
        substitutionChange = ""
        if RSSeq("substitution").value <> sub_id then
            oldSubText = ""
            if RSSeq("substitution").value = 0 then
                oldSubText  = "allowed"
            else
                oldSubText = "not allowed"
            end if

            newSubText = ""
            if sub_id = 0 then
                newSubText  = "allowed"
            else
                newSubText = "not allowed"
            end if

            substitutionChange = "- Substitution: From " & oldSubText & " to " & newSubText & "<br/>" 
        end if
        recordedDateChange = ""
        if Cstr(RSSeq("prescribed_date").value) <> Cstr(start_date) then recordedDateChange = "- Recorded Date: From " & Cstr(RSSeq("prescribed_date").value) & " to " & Cstr(start_date) & "<br/>" end if

        detailAuditLog = drugNameChange & formulationChange & dispenseChange & dosageChange & frequencyChange & durationChange & routeChange & refillChange & noteChange & substitutionChange & recordedDateChange
            
        call epcsAuditLogObj.LogEPCSActivity("Controlled Substance Prescription","Edit CS Medication",detailAuditLog,"SUCCESS")
        '---END

    end if
    rs.close

    '---End
	
    sqlQuery = "UPDATE notes_medications SET drug_name='" & sqlFixUp(drug_name) & "',formulation='" & sqlFixUp(formulation) & "',dispense='" & sqlFixUp(dispense) & "',dose='" & sqlFixUp(dosage) & "',frequency='" & sqlFixUp(frequency) & "',route='" & sqlFixUp(route) & "',refill='" & sqlFixUp(refill) & "',REFILL_REMAINING='" & sqlFixUp(refill) & "',notes='" & sqlFixUp(notes) & "',prescribed_date=to_date('"&start_date&"','MM/DD/YYYY'),modified_date=to_date('" & date_mod & "','MM/DD/YYYY'), SUBSTITUTION='"& sub_id &"'  where drug_id='" & drug_id & "' and patient_id='" & patient_id & "'"
  
    CMD.CommandText = sqlQuery
    CMD.Execute 

    CONN.CommitTrans

end if
msg = "Changes Saved."
if err_msg <> "" then
  msg = ""
  err_msg = "There is error in input data. Please try again."
end if

  %>
  <script language="javascript">
      parent.rightNoteFrame.location.reload(true);
      document.location = '/practice/notes/edit/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=<%=drug_id%>&msg=<%=msg%>&errmsg=<%=err_msg%>';
  </script>
  <%

  Response.End

else

  dim RSDrug
  set RSDrug = Server.CreateObject("ADODB.Recordset")

  RSDrug.CursorLocation = 3
  sqlQuery = "SELECT * FROM notes_medications WHERE drug_id='" & drug_id & "'"

  RSDrug.Open sqlQuery, CONN, 3, 4

  'visit_key = RSDrug("visit_key")
  drug_name =  RSDrug("drug_name")
  formulation = RSDrug("formulation")
  dispense_ar = split(trim(RSDrug("dispense")), " ", 2)
  if ubound(dispense_ar) > 0 then
    dispense = dispense_ar(0)
    dispense_units = dispense_ar(1)
  else
    dispense = trim(RSDrug("dispense"))
  end if
  dosage_ar = split(trim(RSDrug("dose")), " ", 2)
  if ubound(dosage_ar) > 0 then
    dosage = dosage_ar(0)
    dosage_units = dosage_ar(1)
  else
    dosage = trim(RSDrug("dose"))
  end if
  frequency = RSDrug("frequency")
  route = RSDrug("route")
  refill = RSDrug("refill")
  notes = RSDrug("notes")
  start_date = RSDrug("prescribed_date")
  substitution = RSDrug("substitution")
  ss_flag = RSDrug("ss_flag")
  medical_type = RSDrug("medical_type")
  
  'EPCS
  ndc_code = RSDrug("ndc_code")
  dim RSEPCS
  set RSEPCS = Server.CreateObject("ADODB.Recordset")

  sqlQuery = "SELECT csa_schedule from ndc_drugs  where ndc_code = '" & ndc_code & "' and csa_schedule in (2,3,4,5)"
  RSEPCS.Open sqlQuery, CONN
  
  if not (RSEPCS.eof or RSEPCS.bof) then
    Is_CS="Y"
    if RSEPCS("csa_schedule") = 2 then
        CS2 = "Y"
    else
        CS2 = "N"
    end if
  else
    Is_CS="N"
  end if
  RSEPCS.close 

  
end if


set RSVitals = Server.CreateObject("ADODB.Recordset")
RSVitals.CursorLocation = 3
sqlQuery = "SELECT DISTINCT take FROM notes_vitals WHERE visit_key='" & visit_key & "'"
RSVitals.Open sqlQuery, CONN, 3, 4
VCount = RSVitals.RecordCount
RSVitals.Close

if VCount > 0 then
  sqlQuery = "SELECT * FROM notes_vitals WHERE visit_key='" & visit_key & "' AND vital_type='W' ORDER BY take DESC"
  RSVitals.Open sqlQuery, CONN, 3, 4
  set RSVitals.ActiveConnection = nothing

  dim VCounter

  if RSVitals.RecordCount > 0 then
    weight = RSVitals("vital_data1") & "kgs (" & Round(RSVitals("vital_data1").Value / .4536,2) & "lbs)"
  end if

end if

if weight = "" then
  weight = "Not recorded yet."
end if

Set DoctorDataSet = GetDoctorData(visit_key)
DOCTOR_NAME = DoctorDataSet("Last_Name")&", "&DoctorDataSet("First_Name")
DoctorDataSet.Close
Set DoctorDataSet = nothing

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script language="javascript">
    function validateForm(that) {
        setDosage();

        if (that.nm_med.value == "") {
            alert("Please select a medication to prescribe.");
            that.nm_med.focus();
            return false;
        }

        if (that.formulation.value == "" && that.medical_type.value != "SP") {
            alert("Please describe the medication's formulation.");
            that.formulation.focus();
            return false;
        }

        if (alltrim(that.dosage.value) == '') {
            alert("Please enter an appropriate dosage.");
            that.dosage.focus();
            return false;
        }

        if (that.dosage_units_select.selectedIndex == 0) {
            alert("Please enter an appropriate dosage unit.");
            that.dosage_units_select.focus();
            return false;
        }
        
        if (that.dosage_units_select.options[that.dosage_units_select.selectedIndex].value == 'Other' && that.dosage_units.value == '' ) {
            alert("Please enter Other Dosage Unit.");
            that.dosage_units.focus();
            return false;
        }

        if (that.freq.value == "") {
            setFreq(document.frm2.freq_select);
        }

        if (!that.freq.value != "") {
            alert("Please select the frequency of dosage: " + that.dosage_units.value);
            that.freq.focus();
            return false;
        }
		
        if (that.route.value == "") {
            setRoute(document.frm2.route_select);
        }
        
        if (that.route.value == "" && that.medical_type.value != "SP") {
            alert("Please select an appropriate route");
            that.route.focus();
            return false;
        }

        var re = /^\d+$/;

        /*
        if (!that.dispense.value.match(re)) {
          alert("Please enter an appropriate amount of drug to dispense.");
          that.dispense.focus();
          return false;
        }

        if (that.dispense_units.selectedIndex <= 0) {
          alert("Please select an appropriate dispense unit.");
          that.dispense_units.focus();
          return false;
        }*/

        if (that.dispense.value == "") {
            alert("Please enter the dispense quantity");
            that.dispense.focus();
            return false;
        }

        if (that.dispense_units.selectedIndex <= 0) {
            alert("Please select an appropriate dispense unit.");
            that.dispense_units.focus();
            return false;
        }

        /*var startDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (that.start_date.value == "") {
            alert("Please set the Medication Start Date.");
            that.start_date.focus();
            return false;
        }
        else {
            if (!that.start_date.value.match(startDate)) {
                alert("Please enter the start date in correct format(MM/DD/YYYY)");
                return false;
            }
        }*/

        if (that.notes.value != "" && that.notes.value.length > 210) {
            that.notes.focus();
            alert("Notes cannot be longer than 210 characters.");
            return false;
        }

        if (!isNumber()) {
            return false;
        }

        if (!validateSpecialChars(that)) {
            return false;
        }

        return true;

    }

    function setDosage() {

        for (var i = 0; i < document.frm2.dosage_units_select.length; i++) {
            if (document.frm2.dosage_units_select[i].selected == true && document.frm2.dosage_units_select[i].text == 'Other') {
                document.getElementById('dosage_tr').style.display = '';
                //document.getElementById('dosage_units').value = '';
                document.getElementById('dosage_units').focus();

                return true;
            }
            if (document.frm2.dosage_units_select[i].selected == true && document.frm2.dosage_units_select[i].text != 'Other') {
                document.getElementById('dosage_tr').style.display = 'none';
                document.getElementById('dosage_units').value = document.frm2.dosage_units_select[i].text;
                return true;
            }
        }

    }

    function setFreq(that) {

        for (var i = 0; i < that.length; i++) {
            if (that[i].selected == true && that[i].text == 'Other') {
                document.getElementById('freq_tr').style.display = '';
                document.getElementById('freq').value = '';
                document.getElementById('freq').focus();

                return true;
            }
            if (that[i].selected == true && that[i].text != 'Other') {
                document.getElementById('freq_tr').style.display = 'none';
                document.getElementById('freq').value = that[i].text;
                return true;
            }
        }

    }

    function setRoute(that) {

        for (var i = 0; i < that.length; i++) {
            if (that[i].selected == true && that[i].text == 'Other') {
                document.getElementById('route_tr').style.display = '';
                document.getElementById('route').value = '';
                document.getElementById('route').focus();

                return true;
            }
            if (that[i].selected == true && that[i].text != 'Other') {
                document.getElementById('route_tr').style.display = 'none';
                document.getElementById('route').value = that[i].text;
                return true;
            }
        }

    }

    function setInitRoute(that) {
        var inlist = false;

        if (document.getElementById('route').value.length > 0) {

            for (var i = 0; i < that.length; i++) {
                if (that[i].text == document.getElementById('route').value) {
                    that[i].selected = true;
                    inlist = true;

                }
            }

            if (inlist == false) {

                for (var i = 0; i < that.length; i++) {
                    if (that[i].text == 'Other') {
                        that[i].selected = true;
                    }
                }
                document.getElementById('route_tr').style.display = '';
            }
        }

    }

    function setInitFreq(that) {
        var inlist = false;

        if (document.getElementById('freq').value.length > 0) {

            for (var i = 0; i < that.length; i++) {
                if (that[i].text == document.getElementById('freq').value) {
                    that[i].selected = true;
                    inlist = true;

                }
            }

            if (inlist == false) {

                for (var i = 0; i < that.length; i++) {
                    if (that[i].text == 'Other') {
                        that[i].selected = true;
                    }
                }
                document.getElementById('freq_tr').style.display = '';
            }
        }

    }
    function setInitDosage(that) {
        var inlist = false;

        if (document.getElementById('dosage_units').value.length > 0) {

            for (var i = 0; i < that.length; i++) {
                if (that[i].text == document.getElementById('dosage_units').value) {
                    that[i].selected = true;
                    inlist = true;

                }
            }

            if (inlist == false) {

                for (var i = 0; i < that.length; i++) {
                    if (that[i].text == 'Other') {
                        that[i].selected = true;
                    }
                }
                document.getElementById('dosage_tr').style.display = '';
            }
        }

    }

    function initSelect() {

        setInitRoute(document.getElementById('route_select'));
        setInitFreq(document.getElementById('freq_select'));
        setInitDosage(document.getElementById('dosage_units_select'));

    }
    function setDate(frmObj, d) {

        var month, day, year;
        re = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;

        if (frmObj.value.match(re)) {

            var now = new Date(frmObj.value);

            if (d == 0) {
                now.setTime(now.getTime() - 24 * 60 * 60 * 1000);
                month = now.getMonth() + 1;
                day = now.getDate();
                year = now.getYear();

                if (year < 1000)
                    year = year + 1900;

                frmObj.value = month + "/" + day + "/" + year;
            } else {
                now.setTime(now.getTime() + 24 * 60 * 60 * 1000);
                month = now.getMonth() + 1;
                day = now.getDate();
                year = now.getYear();
                if (year < 1000)
                    year = year + 1900;
                frmObj.value = month + "/" + day + "/" + year;
            }


        } else {
            var today = new Date();
            month = today.getMonth() + 1;
            day = today.getDate();
            year = today.getYear();
            if (year < 1000)
                year = year + 1900;
            frmObj.value = month + "/" + day + "/" + year;

        }
    }


    function checklength(i) {
        var txt;
        txt = document.frm2.notes.value;
        n = txt.length;
        if (n > i) {
            alert('Only 210 characters are allowed for this field');
            document.frm2.notes.value = text1;
            return;
        }
        text1 = document.frm2.notes.value;
    }

    function isNumber() {
        var val = alltrim(document.frm2.dispense.value);
        if (val != "") {
            var num = Number(val);
            if (isNaN(num) || val.match(/^(\.|\d)+$/g) == null) {
                alert("Invalid Number");
                document.frm2.dispense.focus();
            } else if (val.indexOf(".") != -1 && val.length > 11) {
                alert("Dispense must not be longer than 10 digits.");
                document.frm2.dispense.focus();
            } else if (val.indexOf(".") == -1 && val.length > 10) {
                alert("Dispense must not be longer than 10 digits.");
                document.frm2.dispense.focus();
            } else if (num <= 0) {
                alert("Dispense must not be zero");
                document.frm2.dispense.focus();
            } else {
                document.frm2.dispense.value = num.toFixed(decimalPlaces(val));
                return true;
            }
        }
        return false;
    }

    function decimalPlaces(numberStr) {
        // toFixed produces a fixed representation accurate to 20 decimal places
        // without an exponent.
        // The ^-?\d*\. strips off any sign, integer portion, and decimal point
        // leaving only the decimal fraction.
        // The 0+$ strips off any trailing zeroes.
        return (numberStr).replace(/^-?\d*\.?|0+$/g, '').length
    }

    function alltrim(str) {
        return str.replace(/^\s+|\s+$/g, '');
    }

    function validateSpecialChars(that) {
        if (that.nm_med.value != "" && hasSpecialChars(that.nm_med.value)) {
            alert("Illegal characters (#, ') in Medication Name.");
            that.nm_med.focus();
            return false;
        }

        if (that.formulation.value != "" && hasSpecialChars(that.formulation.value)) {
            alert("Illegal characters (#, ') in Formulation.");
            that.formulation.focus();
            return false;
        }

        if (alltrim(that.dosage.value) != '' && hasSpecialChars(that.dosage.value)) {
            alert("Illegal characters (#, ') in Dosage.");
            that.dosage.focus();
            return false;
        }
        if (that.dosage_units.value != '' && hasSpecialChars(that.dosage_units.value)) {
            alert("Illegal characters (#, ') in Other Dosage.");
            that.dosage_units.focus();
            return false;
        }
        if (that.freq.value != "" && hasSpecialChars(that.freq.value)) {
            alert("Illegal characters (#, ') in Frequency");
            return false;
        }

        if (that.route.value != "" && hasSpecialChars(that.route.value)) {
            alert("Illegal characters (#, ') in Route");
            return false;
        }
        if (that.notes.value != "" && hasSpecialChars(that.notes.value)) {
            alert("Illegal characters (#, ') in Notes.");
            that.formulation.focus();
            return false;
        }
        return true;
    }

    function hasSpecialChars(inputStr) {
        var specialChars = ["'", "#"];
        for (var i = 0; i < specialChars.length; i++) {
            if (inputStr.indexOf(specialChars[i]) != -1) {
                return true;
            }
        }
        return false;
    }
</script>
</head>
<body onload="initSelect()">
<div class="titleBox">Edit Medication</div>
<p>
<% if Request("msg") <> "" then %>
<span style="color: green"><b><%=Request("msg")%></b></span>
<p>
<% end if %>
Fields in <b>BOLD</b> are required.
<p>
<form name="frm2" id="frm2" action="medication.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>&amp;drug_id=<%=drug_id%>" method="post" onsubmit="return validateForm(this)">
<table>
<% if Request("errmsg")<> "" then %>
  <tr>
    <td colspan="2" style="color: red"><%=Request("errmsg")%></td>
  </tr>
<% end if %>
  <tr>
    <input type="hidden" name="medical_type" value="<%=medical_type%>">

    <td align="right"><b>Patient Weight</td>
    <td><%=HTMLEncode(weight)%></td>
  </tr>
  <tr>
    <td align="right">
        <% if Is_CS = "Y" then %>
            <span style="float: left;">
				<img id="epcsIcon" alt="Controlled Substance" src="/images/practice/red_star.gif" border="0"/>
			</span>
        <% end if %>
        <b>Drug Name</b>
    </td>
    <td><input maxlength="100" name="nm_med" size="20" value="<%=HTMLEncode(drug_name)%>" onchange="checkvalue()" readonly> <a href="#" onclick="parent.rightNoteFrame.location='/practice/notes/display/medication_search.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'"><img border="0" height="18" src="/images/global/icon_search.gif"></a></td>
  </tr>
  <tr>
    <td align="right"><b>Formulation</td>
    <td><input name="formulation" value="<%=HTMLEncode(formulation)%>" size="25" maxlength="100" readonly></td>
  </tr>
  <tr>
    <td align="right"><b>Dose</td>
    <td>
      <input name="dosage" value="<%=HTMLEncode(dosage)%>" size="5" maxlength="9">
      <select id="dosage_units_select" name="dosage_units_select" onchange="setDosage(this);">
        <option>
        <% if dosage_units = "Ampoule(s)" then %><option selected value="Ampoule(s)"><% else %><option value="Ampoule(s)"><% end if %>Ampoule(s)</option>
        <% if dosage_units = "Application(s)" then %><option selected value="Application(s)"><% else %><option value="Application(s)"><% end if %>Application(s)</option>
        <% if dosage_units = "Applicator(s)" then %><option selected value="Applicator(s)"><% else %><option value="Applicator(s)"><% end if %>Applicator(s)</option>
        <% if dosage_units = "Applicatorful(s)" then %><option selected value="Applicatorful(s)"><% else %><option value="Applicatorful(s)"><% end if %>Applicatorful(s)</option>
        <% if dosage_units = "Bag(s)" then %><option selected value="Bag(s)"><% else %><option value="Bag(s)"><% end if %>Bag(s)</option>
        <% if dosage_units = "Bar(s)" then %><option selected value="Bar(s)"><% else %><option value="Bar(s)"><% end if %>Bar(s)</option>
        <% if dosage_units = "Bead(s)" then %><option selected value="Bead(s)"><% else %><option value="Bead(s)"><% end if %>Bead(s)</option>
        <% if dosage_units = "Blister(s)" then %><option selected value="Blister(s)"><% else %><option value="Blister(s)"><% end if %>Blister(s)</option>
        <% if dosage_units = "Block(s)" then %><option selected value="Block(s)"><% else %><option value="Block(s)"><% end if %>Block(s)</option>
        <% if dosage_units = "Bolus(es)" then %><option selected value="Bolus(es)"><% else %><option value="Bolus(es)"><% end if %>Bolus(es)</option>
        <% if dosage_units = "Bottle(s)" then %><option selected value="Bottle(s)"><% else %><option value="Bottle(s)"><% end if %>Bottle(s)</option>
        <% if dosage_units = "Box(es)" then %><option selected value="Box(es)"><% else %><option value="Box(es)"><% end if %>Box(es)</option>
        <% if dosage_units = "Can(s)" then %><option selected value="Can(s)"><% else %><option value="Can(s)"><% end if %>Can(s)</option>
        <% if dosage_units = "Canister(s)" then %><option selected value="Canister(s)"><% else %><option value="Canister(s)"><% end if %>Canister(s)</option>
        <% if dosage_units = "Caplet(s)" then %><option selected value="Caplet(s)"><% else %><option value="Caplet(s)"><% end if %>Caplet(s)</option>
        <% if dosage_units = "Capsule(s)" then %><option selected value="Capsule(s)"><% else %><option value="Capsule(s)"><% end if %>Capsule(s)</option>
        <% if dosage_units = "Carton(s)" then %><option selected value="Carton(s)"><% else %><option value="Carton(s)"><% end if %>Carton(s)</option>
        <% if dosage_units = "Cartridge(s)" then %><option selected value="Cartridge(s)"><% else %><option value="Cartridge(s)"><% end if %>Cartridge(s)</option>
        <% if dosage_units = "Case(s)" then %><option selected value="Case(s)"><% else %><option value="Case(s)"><% end if %>Case(s)</option>
        <% if dosage_units = "Cassette(s)" then %><option selected value="Cassette(s)"><% else %><option value="Cassette(s)"><% end if %>Cassette(s)</option>
        <% if dosage_units = "Container(s)" then %><option selected value="Container(s)"><% else %><option value="Container(s)"><% end if %>Container(s)</option>
        <% if dosage_units = "Cylinder(s)" then %><option selected value="Cylinder(s)"><% else %><option value="Cylinder(s)"><% end if %>Cylinder(s)</option>
        <% if dosage_units = "Device(s)" then %><option selected value="Device(s)"><% else %><option value="Device(s)"><% end if %>Device(s)</option>
        <% if dosage_units = "Disk(s)" then %><option selected value="Disk(s)"><% else %><option value="Disk(s)"><% end if %>Disk(s)</option>
        <% if dosage_units = "Dose Pack(s)" then %><option selected value="Dose Pack(s)"><% else %><option value="Dose Pack(s)"><% end if %>Dose Pack(s)</option>
        <% if dosage_units = "Drop(s)" then %><option selected value="Drop(s)"><% else %><option value="Drop(s)"><% end if %>Drop(s)</option>
        <% if dosage_units = "Dual Pack(s)" then %><option selected value="Dual Pack(s)"><% else %><option value="Dual Pack(s)"><% end if %>Dual Pack(s)</option>
        <% if dosage_units = "Each" then %><option selected value="Each"><% else %><option value="Each"><% end if %>Each</option>
        <% if dosage_units = "Film(s)" then %><option selected value="Film(s)"><% else %><option value="Film(s)"><% end if %>Film(s)</option>
        <% if dosage_units = "Fluid Ounce(s)" then %><option selected value="Fluid Ounce(s)"><% else %><option value="Fluid Ounce(s)"><% end if %>Fluid Ounce(s)</option>
        <% if dosage_units = "French(es)" then %><option selected value="French(es)"><% else %><option value="French(es)"><% end if %>French(es)</option>
        <% if dosage_units = "Gallon(s)" then %><option selected value="Gallon(s)"><% else %><option value="Gallon(s)"><% end if %>Gallon(s)</option>
        <% if dosage_units = "Gram(s)" then %><option selected value="Gram(s)"><% else %><option value="Gram(s)"><% end if %>Gram(s)</option>
        <% if dosage_units = "Gum(s)" then %><option selected value="Gum(s)"><% else %><option value="Gum(s)"><% end if %>Gum(s)</option>
        <% if dosage_units = "Implant(s)" then %><option selected value="Implant(s)"><% else %><option value="Implant(s)"><% end if %>Implant(s)</option>
        <% if dosage_units = "Inhalation(s)" then %><option selected value="Inhalation(s)"><% else %><option value="Inhalation(s)"><% end if %>Inhalation(s)</option>
        <% if dosage_units = "Inhaler(s)" then %><option selected value="Inhaler(s)"><% else %><option value="Inhaler(s)"><% end if %>Inhaler(s)</option>
        <% if dosage_units = "Inhaler Refill(s)" then %><option selected value="Inhaler Refill(s)"><% else %><option value="Inhaler Refill(s)"><% end if %>Inhaler Refill(s)</option>
        <% if dosage_units = "Insert(s)" then %><option selected value="Insert(s)"><% else %><option value="Insert(s)"><% end if %>Insert(s)</option>
        <% if dosage_units = "Intravenous Bag(s)" then %><option selected value="Intravenous Bag(s)"><% else %><option value="Intravenous Bag(s)"><% end if %>Intravenous Bag(s)</option>
        <% if dosage_units = "Kilogram(s)" then %><option selected value="Kilogram(s)"><% else %><option value="Kilogram(s)"><% end if %>Kilogram(s)</option>
        <% if dosage_units = "Kit(s)" then %><option selected value="Kit(s)"><% else %><option value="Kit(s)"><% end if %>Kit(s)</option>
        <% if dosage_units = "Lancet(s)" then %><option selected value="Lancet(s)"><% else %><option value="Lancet(s)"><% end if %>Lancet(s)</option>
        <% if dosage_units = "Liter(s)" then %><option selected value="Liter(s)"><% else %><option value="Liter(s)"><% end if %>Liter(s)</option>
        <% if dosage_units = "Lozenge(s)" then %><option selected value="Lozenge(s)"><% else %><option value="Lozenge(s)"><% end if %>Lozenge(s)</option>
        <% if dosage_units = "Micrograms" then %><option selected value="Micrograms"><% else %><option value="Micrograms"><% end if %>Micrograms</option>
        <% if dosage_units = "Milliequivalent(s)" then %><option selected value="Milliequivalent(s)"><% else %><option value="Milliequivalent(s)"><% end if %>Milliequivalent(s)</option>
        <% if dosage_units = "Milligrams" then %><option selected value="Milligrams"><% else %><option value="Milligrams"><% end if %>Milligrams</option>
        <% if dosage_units = "Milliliters" then %><option selected value="Milliliters"><% else %><option value="Milliliters"><% end if %>Milliliters</option>
        <% if dosage_units = "Millimeter(s)" then %><option selected value="Millimeter(s)"><% else %><option value="Millimeter(s)"><% end if %>Millimeter(s)</option>
        <% if dosage_units = "Nebule(s)" then %><option selected value="Nebule(s)"><% else %><option value="Nebule(s)"><% end if %>Nebule(s)</option>
        <% if dosage_units = "Needle Free Injection" then %><option selected value="Needle Free Injection"><% else %><option value="Needle Free Injection"><% end if %>Needle Free Injection</option>
        <% if dosage_units = "Ocular System(s)" then %><option selected value="Ocular System(s)"><% else %><option value="Ocular System(s)"><% end if %>Ocular System(s)</option>
        <% if dosage_units = "Ounce(s)" then %><option selected value="Ounce(s)"><% else %><option value="Ounce(s)"><% end if %>Ounce(s)</option>
        <% if dosage_units = "Package(s)" then %><option selected value="Package(s)"><% else %><option value="Package(s)"><% end if %>Package(s)</option>
        <% if dosage_units = "Packet(s)" then %><option selected value="Packet(s)"><% else %><option value="Packet(s)"><% end if %>Packet(s)</option>
        <% if dosage_units = "Pad(s)" then %><option selected value="Pad(s)"><% else %><option value="Pad(s)"><% end if %>Pad(s)</option>
        <% if dosage_units = "Paper(s)" then %><option selected value="Paper(s)"><% else %><option value="Paper(s)"><% end if %>Paper(s)</option>
        <% if dosage_units = "Pastille(s)" then %><option selected value="Pastille(s)"><% else %><option value="Pastille(s)"><% end if %>Pastille(s)</option>
        <% if dosage_units = "Patch(es)" then %><option selected value="Patch(es)"><% else %><option value="Patch(es)"><% end if %>Patch(es)</option>
        <% if dosage_units = "Pen Needle(s)" then %><option selected value="Pen Needle(s)"><% else %><option value="Pen Needle(s)"><% end if %>Pen Needle(s)</option>
        <% if dosage_units = "Pint(s)" then %><option selected value="Pint(s)"><% else %><option value="Pint(s)"><% end if %>Pint(s)</option>
        <% if dosage_units = "Pouch(es)" then %><option selected value="Pouch(es)"><% else %><option value="Pouch(es)"><% end if %>Pouch(es)</option>
        <% if dosage_units = "Pound(s)" then %><option selected value="Pound(s)"><% else %><option value="Pound(s)"><% end if %>Pound(s)</option>
        <% if dosage_units = "Pre-filled Pen Syringe" then %><option selected value="Pre-filled Pen Syringe"><% else %><option value="Pre-filled Pen Syringe"><% end if %>Pre-filled Pen Syringe</option>
        <% if dosage_units = "Puff(s)" then %><option selected value="Puff(s)"><% else %><option value="Puff(s)"><% end if %>Puff(s)</option>
        <% if dosage_units = "Pump(s)" then %><option selected value="Pump(s)"><% else %><option value="Pump(s)"><% end if %>Pump(s)</option>
        <% if dosage_units = "Quart(s)" then %><option selected value="Quart(s)"><% else %><option value="Quart(s)"><% end if %>Quart(s)</option>
        <% if dosage_units = "Ring(s)" then %><option selected value="Ring(s)"><% else %><option value="Ring(s)"><% end if %>Ring(s)</option>
        <% if dosage_units = "Sachet(s)" then %><option selected value="Sachet(s)"><% else %><option value="Sachet(s)"><% end if %>Sachet(s)</option>
        <% if dosage_units = "Scoopful(s)" then %><option selected value="Scoopful(s)"><% else %><option value="Scoopful(s)"><% end if %>Scoopful(s)</option>
        <% if dosage_units = "Sponge(s)" then %><option selected value="Sponge(s)"><% else %><option value="Sponge(s)"><% end if %>Sponge(s)</option>
        <% if dosage_units = "Spray(s)" then %><option selected value="Spray(s)"><% else %><option value="Spray(s)"><% end if %>Spray(s)</option>
        <% if dosage_units = "Stick(s)" then %><option selected value="Stick(s)"><% else %><option value="Stick(s)"><% end if %>Stick(s)</option>
        <% if dosage_units = "Strip(s)" then %><option selected value="Strip(s)"><% else %><option value="Strip(s)"><% end if %>Strip(s)</option>
        <% if dosage_units = "Suppository(s)" then %><option selected value="Suppository(s)"><% else %><option value="Suppository(s)"><% end if %>Suppository(s)</option>
        <% if dosage_units = "Swab(s)" then %><option selected value="Swab(s)"><% else %><option value="Swab(s)"><% end if %>Swab(s)</option>
        <% if dosage_units = "Syringe(s)" then %><option selected value="Syringe(s)"><% else %><option value="Syringe(s)"><% end if %>Syringe(s)</option>
        <% if dosage_units = "Tablespoon(s)" then %><option selected value="Tablespoon(s)"><% else %><option value="Tablespoon(s)"><% end if %>Tablespoon(s)</option>
        <% if dosage_units = "Tablet(s)" then %><option selected value="Tablet(s)"><% else %><option value="Tablet(s)"><% end if %>Tablet(s)</option>
        <% if dosage_units = "Tabminder(s)" then %><option selected value="Tabminder(s)"><% else %><option value="Tabminder(s)"><% end if %>Tabminder(s)</option>
        <% if dosage_units = "Tampon(s)" then %><option selected value="Tampon(s)"><% else %><option value="Tampon(s)"><% end if %>Tampon(s)</option>
        <% if dosage_units = "Teaspoon(s)" then %><option selected value="Teaspoon(s)"><% else %><option value="Teaspoon(s)"><% end if %>Teaspoon(s)</option>
        <% if dosage_units = "Tray(s)" then %><option selected value="Tray(s)"><% else %><option value="Tray(s)"><% end if %>Tray(s)</option>
        <% if dosage_units = "Troche(s)" then %><option selected value="Troche(s)"><% else %><option value="Troche(s)"><% end if %>Troche(s)</option>
        <% if dosage_units = "Tube(s)" then %><option selected value="Tube(s)"><% else %><option value="Tube(s)"><% end if %>Tube(s)</option>
        <% if dosage_units = "Unit(s)" then %><option selected value="Unit(s)"><% else %><option value="Unit(s)"><% end if %>Unit(s)</option>
        <% if dosage_units = "Vaginal Ring" then %><option selected value="Vaginal Ring"><% else %><option value="Vaginal Ring"><% end if %>Vaginal Ring</option>
        <% if dosage_units = "Vial(s)" then %><option selected value="Vial(s)"><% else %><option value="Vial(s)"><% end if %>Vial(s)</option>
        <% if dosage_units = "Wafer(s)" then %><option selected value="Wafer(s)"><% else %><option value="Wafer(s)"><% end if %>Wafer(s)</option>
        <option value="Other">Other</option>
      </select>
    </td>
  </tr>
  <tr id="dosage_tr" style="display: none;">
    <td class="st" align="right"><i>other unit</td>
    <td><input type="text" id="dosage_units" name="dosage_units" value="<%=HTMLEncode(dosage_units)%>" maxlength=15></td>
  </tr>

  <tr>
    <td align="right"><b>Frequency</td>
    <td>
      <select name="freq_select" id="freq_select" onchange="setFreq(this);">
        <option>
        <% if frequency = "Once Daily" then %><option selected><% else %><option><% end if %>Once Daily
        <% if frequency = "Twice Daily" then %><option selected><% else %><option><% end if %>Twice Daily
        <% if frequency = "Three Times Daily" then %><option selected><% else %><option><% end if %>Three Times Daily
        <% if frequency = "Four Times Daily" then %><option selected><% else %><option><% end if %>Four Times Daily
        <% if frequency = "Five Times Daily" then %><option selected><% else %><option><% end if %>Five Times Daily
        <% if frequency = "Six Times Daily" then %><option selected><% else %><option><% end if %>Six Times Daily
        <% if frequency = "Every Other Day" then %><option selected><% else %><option><% end if %>Every Other Day
        <% if frequency = "7:00am and 4:00pm" then %><option selected><% else %><option><% end if %>7:00am and 4:00pm
        <% if frequency = "At Bedtime" then %><option selected><% else %><option><% end if %>At Bedtime
        <% if frequency = "Once Weekly" then %><option selected><% else %><option><% end if %>Once Weekly
        <% if frequency = "Twice Weekly" then %><option selected><% else %><option><% end if %>Twice Weekly
        <% if frequency = "Three Times Weekly" then %><option selected><% else %><option><% end if %>Three Times Weekly
        <% if frequency = "As Needed" then %><option selected><% else %><option><% end if %>As Needed
        <option>
        <% if frequency = "Every 0.25 Hours" then %><option selected><% else %><option><% end if %>Every 0.25 Hours
        <% if frequency = "Every 0.50 Hours" then %><option selected><% else %><option><% end if %>Every 0.50 Hours
        <% if frequency = "Every 1 Hours" then %><option selected><% else %><option><% end if %>Every 1 Hours
        <%
          for i = 2 to 24
            if frequency = "Every " & i & " Hours" then
        %>
        <option selected>Every <%=i%> Hours
        <%
            else
        %>
        <option>Every <%=i%> Hours
        <%
            end if
          next
        %>
        <option>Other
      </select>
    </td>
  </tr>
  <tr id="freq_tr" style="display: none;">
    <td class="st" align="right"><i>other freq.</td>
    <td><input type="text" id="freq" name="freq" value="<%=HTMLEncode(frequency)%>" maxlength=70></td>
  </tr>
  <tr>
    <td align="right"><b>Route</td>
    <td>
      <select name="route_select" id="route_select" onchange="setRoute(this);">
        <option>
      <%
        dim RSRoute
        set RSRoute = Server.CreateObject("ADODB.Recordset")
        RSRoute.CursorLocation = 3
        sqlQuery = "SELECT * FROM multum_route ORDER BY route_description"
        RSRoute.Open sqlQuery, CONN, 3, 4
        set RSRoute.ActiveConnection = nothing

        while not RSRoute.EOF
          if RSRoute("route_abbr") = route then
      %>
        <option selected value="<%=RSRoute("route_abbr")%>"><%=PCase(RSRoute("route_description"))%>
      <%
          else
      %>
        <option value="<%=RSRoute("route_abbr")%>"><%=PCase(RSRoute("route_description"))%>
      <%
          end if
          RSRoute.MoveNext
        wend
      %>
        <option>Other
      </select>
    </td>
  </tr>
  <tr id="route_tr" style="display: none;">
    <td class="st" align="right"><i>other route</td>
    <td><input type="text" id="route" name="route" value="<%=HTMLEncode(route)%>" maxlength=25></td>
  </tr>
  <tr>
    <td></td>
    <td><span style="font-size: 0.8em">Route NOT required for medical supply</span></td>
  </tr>
  <% if (other_meds = "Current") then %>
  <tr>
    <td align="right"><b>Refill</td>
    <td>
      <% if Is_CS = "Y" and CS2 = "Y" then %>
             <select name="refill" size="1" onchange="document.frm2.button1.focus();" disabled="disabled">
                <option>0</option>
             </select>
      <% else %>
              <select name="refill" size="1" onchange="document.frm2.button1.focus();">
                    <%
                      for i = 0 to 99
                        if cint(refill) = i then
                    %>
                            <option selected><%=i%></option>
                    <%
                        else
                    %>
                            <option><%=i%></option>
                    <%
                        end if
                      next
                    %>

                    <% if Is_CS = "Y" and CS2 = "N" then%>
                            <option disabled="disabled">PRN</option>
                    <% else %>
                            <% if refill = "999" then %><option selected value=999> <%else %><option ><% end if %>PRN</option>
                    <% end if %>                 
              </select>
      <% end if %> 
      times
    </td>
  </tr>
  <% end if%>
  <tr>
    <td align="right"><b>Dispense<b /></td>
    <td>
      <input name="dispense" value="<%=HTMLEncode(dispense)%>" size="5" maxlength=11 onblur="isNumber();">
      <select name="dispense_units">
      <option>
        <%
					set RSDSP = Server.CreateObject("ADODB.Recordset")
					RSDSP.open "select * from SS_DISPENSE where inuse = 1 order by description", con
					while not RSDSP.EOF
		%>
					<option <% if dispense_units = RSDSP("description") then %>selected<% end if %> value="<%=RSDSP("description")%>"><%=RSDSP("description")%></option>
		<%
					RSDSP.movenext
					wend
					RSDSP.close
		%>

      </select>
    </td>
  </tr>

  <tr>
    <td align="right">Note to Pharmacist</td>
    <td>
      <textarea name="notes" cols="15" rows="5" onblur="checklength(210)"><%=HTMLEncode(notes)%></textarea>
    </td>
  </tr>
  <% if ss_flag then %>
  <tr>
    <td></td>
    <td><b>IMPORTANT:</b> Do NOT add any formulation, dose, frequency or dispense information to notes to pharmacist</td>
  </tr>
  <% end if %>

    <tr>
	    <td align="right"><b>Substitution Permitted?</b></td>
	    <td>
		    <select name="SUB_ID" style='width: 200px;'>
    <%
                set RSsubs = Server.CreateObject("ADODB.Recordset")
			    RSsubs.open "select * from SS_SUBSTITUTIONS where sub_id in (0,1)", con
			    while not RSsubs.eof
    %>
			    <option value="<%=RSsubs("SUB_ID")%>" <% if substitution = CStr(RSsubs("SUB_ID")) then %> selected <% end if %> ><%=RSsubs("DESCRIPTION")%></option>
    <%
			    RSsubs.movenext
			    wend
			    RSsubs.close
    %>
		    </select>
	    </td>
    </tr>
 <!-- <tr>
    <td align="right"><b>Provider</td>
    <td><%=DOCTOR_NAME%></td>
  </tr>-->
  <% if (other_meds = "Current") then %>
	<td align="right"><% if session("show") = "current" then %> Recorded Date <% else %> Prescribed Date <% end if %></td>
  <% else %>
  <tr><td align="right"><b>Prescribed Date</td>
  <%end if %>
	<td>
		<table border="0">
		<tr>
			<% if (other_meds = "Current") then %>
			<td><input type=text name=start_date id="dt" size=10  readonly value="<%=start_date%>"></td>
			<td>&nbsp;<!--<a href="#"><img onClick="setDate(document.getElementById('dt'),1);" alt="Up" src="/images/practice/date_up_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a><br><img src="/images/spacer.gif" WIDTH="1" HEIGHT="1"><br><a href="#"><img onClick="setDate(document.getElementById('dt'),0);" alt="Down" src="/images/practice/date_down_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a>--></td>
			<%else%>
			<td><input type=text name=start_date id="Text1" size=10 value="<%=start_date%>"></td>
			<td>&nbsp;<a href="#"><img onClick="setDate(document.getElementById('dt'),1);" alt="Up" src="/images/practice/date_up_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a><br><img src="/images/spacer.gif" WIDTH="1" HEIGHT="1"><br><a href="#"><img onClick="setDate(document.getElementById('dt'),0);" alt="Down" src="/images/practice/date_down_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a></td>
			<%end if%>
		</tr>
		</table>
		(MM/DD/YYYY)
	</td>
	</td></tr>
</table>
<p>
<input type="submit" value="Submit" id="submit_button" name="submit_button">
<input type="button" value="Cancel" onclick="parent.leftNoteFrameTop.toggleTop(); parent.rightNoteFrame.location.replace('/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>')" id="reset1" name="reset1">
</form>

<% if Is_CS = "Y" and CS2 = "Y" then %>
        <font size="1" id="epcsPRNDescription">* No more than a single dispense is allowed for CS II.</font>    
<% elseif Is_CS = "Y" and CS2 = "N" then%>
        <font size="1" id="Font1">* PRN is not allowed for controlled substance</font>
<% else %>
        <font size="1" id="Font2">* PRN:As Needed</font>
<% end if %>
<br />
<% if Is_CS = "Y" then %>
    <img alt="Controlled Substance" src="/images/practice/red_star.gif" border="0"/> Controlled Substance.
<% end if %>

</body>
</html>