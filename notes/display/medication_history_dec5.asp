<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id_hidden")
show = Request("show")
dim patientObj
set patientObj = new clsPatientNotes
patientObj.SetVisitKey(visit_key)

if Request("delete") <> "" then

  dim CMD
  set CMD = Server.CreateObject("ADODB.Command")
  
  CMD.ActiveConnection = CONN
  
  sqlQuery = "DELETE FROM notes_medications WHERE drug_id='" & drug_id & "'"
  
  CMD.CommandText = sqlQuery
  CMD.Execute
  
  'Response.Redirect("/practice/notes/display/immunization_history.asp?visit_key=" & visit_key & "&patient_id=" & patient_id)

end if


if Request("stop") <> "" then
  set CMD = Server.CreateObject("ADODB.Command")
  
  CMD.ActiveConnection = CONN
  
  sqlQuery = "UPDATE notes_medications SET status='Stopped', stop_date = to_date('" & date() & "','MM/DD/YYYY') WHERE drug_id='" & drug_id & "'"
  
  CMD.CommandText = sqlQuery
  CMD.Execute
  
  'Response.Redirect("/practice/notes/display/immunization_history.asp?visit_key=" & visit_key & "&patient_id=" & patient_id)

end if

if Request("start") <> "" then

  set CMD = Server.CreateObject("ADODB.Command")
  
  CMD.ActiveConnection = CONN
  
  sqlQuery = "UPDATE notes_medications SET status='Current', stop_date =  NULL, start_date = to_date('" & date() & "','MM/DD/YYYY') WHERE drug_id='" & drug_id & "'"
  
  CMD.CommandText = sqlQuery
  CMD.Execute
  
  'Response.Redirect("/practice/notes/display/immunization_history.asp?visit_key=" & visit_key & "&patient_id=" & patient_id)

end if

if Request("refill") <> "" then

	set CMD = server.CreateObject("ADODB.Command")
	CMD.ActiveConnection = CONN
	
	refill_date = CalculateLocalTime(now()) 
	
	if NOT isnull(session("TimeZone")) AND session("TimeZone") <> "" then
		refill_date = refill_date & "(" & session("TimeZone") & ")"
	end if
	
	sqlQuery = "Update notes_medications set notes = notes || 'Refilled on '||'"&refill_date&"'||';',refill=refill+1 WHERE drug_id='"& drug_id &"'"
	CMD.CommandText = sqlQuery
	CMD.Execute
	
end if
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script language="javascript">
function editDrug() {

  var value = document.getElementById('drug_id_hidden').value;

  if (value != "" && value != null) {
    getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=' + value);
  } else {
    alert("Please select a prescription to edit.")
  }
}

function printmedhx()
{
var all;
all = 0;

if (confirm('Do you want to print All medications history?')) 
{ 
	all = 1;
}
	popScrollWindow('/practice/notes/display/med_hx_print.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&all='+all,700,400);  
}


</script>
</head>
<body>
<div class="titleBox">Medications</div>
<p>

<form action="medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>" method="post" name="medhist" id="medhist">
<input type="hidden" name="drug_id_hidden" id="drug_id_hidden" value="">

<p>
<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'); return false;" value="New" id=button1 name=button1>&nbsp;
<input type="submit" name="refill" onclick="return confirm('Are you sure you wish to refill this prescription?');" value="Refill" id=button1 name=button1>&nbsp;
<input type="button" onclick="editDrug();" value="Edit" id=button2 name=button2>&nbsp;
<input type="submit" name="stop" value="Stop Medication" onclick="return confirm('Are you sure you wish to stop this prescription?');" >&nbsp;
<input type="submit" name="start" value="(Re)start Medication" onclick="return confirm('Are you sure you wish to (re)start this prescription?');" >&nbsp;
<!--<input type="button" onclick="document.location='/practice/notes/display/interactions.asp?drug_id_list='+document.getElementById('multum_drug_id_list').value+'&drug_name_list='+document.getElementById('multum_drug_name_list').value;" value="Drug Interactions">-->
<input type="button" onclick="popScrollWindow('http://www.subscriberx.com/iqhealth/Inter.asp?'+escape(String(document.getElementById('multum_query_string').value)),'800','600');" value="Multum Drug Interactions" id=button5 name=button5>&nbsp;
<input type="button" onclick="document.location='/practice/notes/display/medication_printrx.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'" value="Print Rx..." id=button3 name=button3>&nbsp;
<input type="button" onclick="document.location='/practice/notes/display/medication_emailrx.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'" value="Email Rx..." id=button6 name=button6>&nbsp;
<input type="button" onclick="printmedhx();" value="Print Med Hx" id=button8 name=button8>&nbsp;
<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').toggleTop();document.location='/practice/notes/display/note.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>';" value="Cancel" id=button4 name=button4>&nbsp;
<p>

<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">

  <tr bgcolor="#3366CC">
    <td colspan="1"><font color="white"><b>Date Prescribed</td>
    <td colspan="1"><font color="white"><b>Date Started</td>
    <td colspan="1"><font color="white"><b>Date Stopped</td>
    <td><font color="white"><b>Prescription</td>
    <td><font color="white"><b>Notes</td>
    <td><font color="white"><b>Status</td>
  </tr>


<%

set res1 = server.CreateObject("ADODB.Recordset")

if show = "new" or session("show") = "new" then
	sqlQuery = "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and patient_id='" & patient_id & "' and iscurrent = 0 and visit_key='" & visit_key & "' order by status,drug_name"
	session("show") = "new"
end if
if show = "current" or session("show") = "current" then
	sqlQuery = "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and patient_id='" & patient_id & "' and a.prescribed_date <= (select visit_date from visit where visit_key = '"&visit_key&"') order by status,drug_name"
	session("show") = "current"
end if

'Response.Write sqlQuery

res1.open sqlQuery,con
if not res1.eof then

	cnt = 1
	while not res1.eof
		
		notes = ""
		
		if cnt mod 2 = 0 then
			bgcolor = "#FFFFFF"
		else
			bgcolor = "#EFEFEF"
		end if
		
		if not isnull(res1("multum_drug_id")) and res1("status") <> "Stopped" then
			multum_drug_id_list =  res1("multum_drug_id") & "|" & multum_drug_id_list
			multum_drug_name_list =  res1("drug_name") & "|" & multum_drug_name_list
			
			if not isnull(res1("multum_brand_code")) then
				multum_query_string = multum_query_string & "&drugname=" & res1("drug_name") & ";drugid=" & res1("multum_brand_code") & " "
			end if
		end if
		
		if res1("notes") <> "" then
			notes = replace(res1("notes"),";","<br>")
		end if
			
%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top"><input type="radio" name="mid" value="<%=res1("drug_id")%>" onclick="document.getElementById('drug_id_hidden').value=this.value">
			<b><%=res1("prescribed_date")%></td>
		<td valign="top"><b><%=res1("start_date")%></td>
        <td valign="top"><b><%=res1("stop_date")%></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b> - 
          <%=res1("formulation")%><br>
	      <b>Dose:</b> <%=res1("dose")%><br>
	      <b>Route:</b> <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %><br>
	      <% if trim(res1("dispense").value) <> "" then %>
	      <b>Dispense:</b> <%=res1("dispense")%><br>
	      <% end if %>
	      <b>Refill:</b> <%=res1("refill")%>
	      <% if res1("frequency") <> "" then %>
	      <br><b>Frequency:</b> <%=res1("frequency")%>
	      <% end if %>
	    </td>
	    <td class="st" valign="top"><%=notes%></td>
	    <td valign="top"><b><%
			If res1("status")="Current" then 
				Response.Write "<font color=green>"&"Current"
			else
				'Response.Write patientObj.VisitDate & " - " & res1("stop_date1")
				'if CompareDates(FormatDateTime(patientObj.VisitDate,2),FormatDateTime(res1("stop_date"),2)) = 0 then
				'	Response.Write "<font color=green>"&"Current"
				'else
					Response.Write res1("status")
				'Response.Write patientObj.VisitDate
				'end if
			end if
			%></td>
	</tr>
	<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close 

else
%>
	<tr>
		<td colspan="8">No medications found on record.</td>
	</tr>
<%
end if
%>
</table>
<input type=hidden name="multum_drug_id_list" value="<%=multum_drug_id_list%>">
<input type=hidden name="multum_drug_name_list" value="<%=multum_drug_name_list%>">
<input type=hidden name="multum_query_string" value="<%=multum_query_string%>">

<p>
<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'); return false;" value="New" id=button1 name=button1>
<input type="submit" name="refill" onclick="return confirm('Are you sure you wish to refill this prescription?');" value="Refill" id=button1 name=button1>
<input type="button" onclick="editDrug();" value="Edit" id=button2 name=button2>
<input type="submit" name="stop" value="Stop Medication" onclick="return confirm('Are you sure you wish to stop this prescription?');" >
<input type="submit" name="start" value="(Re)start Medication" onclick="return confirm('Are you sure you wish to (re)start this prescription?');" >
<!--<input type="button" onclick="document.location='/practice/notes/display/interactions.asp?drug_id_list='+document.getElementById('multum_drug_id_list').value+'&drug_name_list='+document.getElementById('multum_drug_name_list').value;" value="Drug Interactions">-->
<input type="button" onclick="popScrollWindow('http://www.subscriberx.com/iqhealth/Inter.asp?'+escape(String(document.getElementById('multum_query_string').value)),'800','600');" value="Multum Drug Interactions" id=button5 name=button5>
<input type="button" onclick="document.location='/practice/notes/display/medication_printrx.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'" value="Print Rx..." id=button3 name=button3>
<input type="button" onclick="document.location='/practice/notes/display/medication_emailrx.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'" value="Email Rx..." id=button6 name=button6>
<input type="button" onclick="printmedhx();" value="Print Med Hx" id=button8 name=button8>&nbsp;
<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').toggleTop();document.location='/practice/notes/display/note.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>';" value="Cancel" id=button4 name=button4>
<p>



</form>
</BODY>
</HTML>
