<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_list = Request("list")

if request("sig") = "true" then
	show_ds = 1	
end if
if request("necessary") = "true" then
	show_mn = 1
end if

%>
<html>
<head>
<title>Medications &gt; Print Prescription</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
</head>

<body onload="window.focus();window.print();">

<div class="titleBox" style="background-color: #FFF; width: 550px; font-weight: normal; align: center; vertical-align: middle;">

<table width="100%">
<%

set res = server.CreateObject("adodb.recordset")
set res1 = server.CreateObject("adodb.recordset")

strsql="select doctor_id from visit where visit_key='"&visit_key&"'"
res.open strsql,CONN
doc_id= res("doctor_id")
res.Close

strsql="select b.orgname,a.first_name,a.last_name,a.middle_name,a.credential,a.street,a.city,a.state,a.zip,a.wphone,a.ext,a.fax,a.email,a.provider_licno,a.provider_licnostate,a.dea_no,a.dps_no,a.speciality from  doctortable a,streetreg b where a.FACILITY  = b.streetadd and a.docuser='"&doc_id&"'"
res.open strsql,CONN

name=res("first_name")&" "&res("middle_name")&" "&res("last_name")&", "&res("credential")
credential = res("credential")

dim res2
set res2 = Server.CreateObject("ADODB.Recordset")

ph_id=request("pharmacy")

if ph_id="" then 
	res2.open " select PharmacyID from ss_message where DRUGID ='"& drug_list &"'", con
		if not (res2.EOF or res2.BOF) then ph_id = res2("PharmacyID")
	res2.Close
end if

res2.open " select * from SS_PHARMACY where NCPDPID ='"& ph_id &"'", con

%>
<tr>
<td valign="top"><img src="/images/practice/rx.gif" border="0" align="left" WIDTH="46" HEIGHT="48"></td>
<td align="left" width="50%" valign="top">

<%
if not res2.EOF then
%>
<b><%=res2("STORENAME")%></b><br>
<%=res2("ADDRESSLINE1")%><br>
<%=res2("city")&", "&res2("state")&"  "&res2("zip")%><br>
<% if res2("PHONEPRIMARY") <> "" then %>
Tel: <%=res2("PHONEPRIMARY")%><br>
<% end if %>
<% if res2("fax") <> "" then %>
Fax: <%=res2("fax")%><br>
<% end if %>
<%
end if
%>
</td>
<td align="right" width="50%" valign="top">
<%=res("orgname")%> <br> 
<% if session("pid") <> "JON1176" then %> <b><%=name%></b><br><% end if %>
<%=res("street")%><br>
<%=res("city")&", "&res("state")&"  "&res("zip")%><br>
Tel: <%=res("wphone")%><br>
<% if res("fax") <> "" then %>
Fax: <%=res("fax")%><br>
<% end if %>
DEA#&nbsp;<%=res("dea_no")%><br>
<% if res("dps_no")<>"" then Response.Write "DPS# "&res("dps_no")&"<br>" %>
License#&nbsp;<%=res("provider_licno")%>
</td>
</tr>
</table>

<%
res.close
res1.open "select first_name,middle_name,last_name,birth_date,street,city,state,zip,primary_insurance,patient_employer,ssn from patienttable where userid='"&patient_id&"'",CONN
%>
<p>
<div class="titleBox_HL" style="background-color: #FFF; font-weight: normal">
<b>Patient</b>
<p>
<%=res1("first_name")&" "&res1("last_name")%><br>
<%=res1("street")%><br>
<%=res1("city")&", "&res1("state")&" "&res1("zip")%><br>
<b>DOB:</b> <%=res1("birth_date")%>
</div>

<%
res1.close
%>
<p>
<%
res1.open "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and drug_id in (" & drug_list & ")",CONN
if not res1.eof then
  while not res1.eof
%>
<div class="titleBox_HL2" style="background-color: #FFF; font-weight: normal">
<img src="/images/practice/rx.gif" border="0" HEIGHT="12" align="left"> <b><%=res1("drug_name")%></b><p>
Formulation: <%=res1("formulation")%><br>
<% if trim(res1("dispense").value) <> "" then %>
Dispense: <%=res1("dispense")%><br>
<% end if %>
Dose: <%=res1("dose")%><br>
Frequency: <%=res1("frequency")%><br>
Route: <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %><br>
Refill: <%=res1("refill")%><p>

<% if not isnull(request("oldMessageId")) and request("oldMessageId") <> "undefined" then  %>
    <strong>Reference Number: <%= request("oldMessageId") %></strong>
<% end if %>


<%=res1("notes")%>
</div>
<p>
<%
    res1.MoveNext
  wend
end if
%>
<p>
<table height="100">
<tr>
<td valign="middle">
<b><span style="font-size: 24px">X</span></b>
</td>
<td valign="middle">
<%
if show_ds = 1 then
	session("authentication") = "1"
%>
<img src="/practice/customization/signature/display_signature.asp">
<% 
else 
%>
__________________________________
<% end if %>
</td>
<td valign="middle">
,<%=credential%> <b>Date</b>: <%=date%>
</td>
<tr><td valign="middle">Dispense as Written </td></tr>

</tr>
</table>


<p>



<table height="100">
<tr>
<td valign="middle">
 <b><span style="font-size: 24px">X</span></b>
</td>
<td valign="middle">
<%
if show_mn = 1 then
	session("authentication") = "1"
%>
<img src="/practice/customization/signature/display_signature.asp">
<% 
else 
%>
__________________________________
<% end if %>
</td>
<td valign="middle">
,<%=credential%>
</td>
</tr>
<tr><td valign="middle">Substitution  Permitted</td></tr>

</table>

</div>

</body>
</html>
