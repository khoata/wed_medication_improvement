<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../../../library/clsEPCSAuditLog.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%
dim epcsAuditLogObj
set epcsAuditLogObj = new clsEPCSAuditLog

dim auditObj
set auditObj = new clsAudit

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_list = Request("list")
call auditObj.LogActivity("Accessing Medications Print page","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)

if request("sig") = "true" then
	show_ds = 1
end if
if request("necessary") = "true" then
	show_mn = 1
end if

%>

<%
'EPCS - PRINT
epcsPrintType = "EPCS-NO"

set rsEPCS = server.CreateObject("adodb.recordset")
set rsEPCS2 = server.CreateObject("adodb.recordset")

'1. Check if Prescriber has EPCS
sqlEPCS = "select * from doctortable where docuser='" & session("docid") & "' and EPCS = 'Y' and bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  2048) = 2048"
rsEPCS.Open sqlEPCS, con
if not (rsEPCS.EOF or rsEPCS.BOF) then 

    '2. Check if printing request contains multiple drugs
    arrDrug = Split(drug_list,",")
    countDrug = UBound(arrDrug) + 1
    if countDrug > 1 then
        for each d in arrDrug
            sqlEPCS2 =  "SELECT * " &_
                        "FROM NDC_DRUGS ndc " &_
                        "INNER JOIN (select * from NOTES_MEDICATIONS WHERE drug_id = " & d & ") drug " &_
                        "ON drug.NDC_CODE = ndc.NDC_CODE AND ndc.CSA_SCHEDULE in (2,3,4,5)"
            rsEPCS2.Open sqlEPCS2, con
            if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                rsEPCS2.Close
                response.Write("Controlled substance must be printed for dispensing in one prescription separately!")
                response.End
            end if
            rsEPCS2.Close
        next        
    else
        
        '3. Check if drug is controlled substance
         sqlEPCS2 = "SELECT * " &_
                    "FROM NDC_DRUGS ndc " &_
                    "INNER JOIN (select * from NOTES_MEDICATIONS WHERE drug_id = " & drug_list & ") drug " &_
                    "ON drug.NDC_CODE = ndc.NDC_CODE AND ndc.CSA_SCHEDULE in (2,3,4,5)"
         rsEPCS2.Open sqlEPCS2, con
         if not (rsEPCS2.EOF or rsEPCS2.BOF) then
             epcsSchedule = rsEPCS2("CSA_SCHEDULE")
             rsEPCS2.Close
             '4. Determine which soft of printing
             sqlEPCS2 = "SELECT VISIT_KEY, STATUS, NCPDPID, CREATED_DATE FROM (SELECT * FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = " & drug_list & " ORDER BY CREATED_DATE DESC) WHERE rownum = 1"
             rsEPCS2.Open sqlEPCS2, con
             if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                visit_key_newest = rsEPCS2("VISIT_KEY")
                status_newest = rsEPCS2("STATUS")
                ncpdpid_newest = rsEPCS2("NCPDPID")
                createdDate_newest = rsEPCS2("CREATED_DATE")
                rsEPCS2.Close

                sqlEPCS2 = "SELECT VISIT_ID FROM VISIT WHERE VISIT_KEY = '" & visit_key & "'"

                rsEPCS2.open sqlEPCS2,con
                if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                    visit_id_current = CLng(rsEPCS2("VISIT_ID"))
                end if
                rsEPCS2.close

                sqlEPCS2 = "SELECT VISIT_ID FROM VISIT WHERE VISIT_KEY = '" & visit_key_newest & "'"

                rsEPCS2.open sqlEPCS2,con
                if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                    visit_id_newest = CLng(rsEPCS2("VISIT_ID"))
                end if
                rsEPCS2.close

                if visit_id_current < visit_id_newest then
                    '5. Print
                    response.Write("Can not print controlled substance which was added, printed or ordered in newer encounter!")
                    response.End
                elseif visit_id_current > visit_id_newest then
                    '5. Print
                    epcsPrintType = "EPCS-DISPENSE"
                else
                    if UCase(status_newest) = "PRINTED" then
                        '5. Print

                        'Check if last error newrx is in the same encounter of printed status
                        sqlEPCS2 = "SELECT NCPDPID, Created_Date FROM ( SELECT * FROM EPCS_PRESCRIPTIONS WHERE UPPER(STATUS) = 'NEWRX--ERROR' and VISIT_KEY = '" & visit_key_newest & "' and DRUG_ID = " & drug_list & " ORDER BY CREATED_DATE DESC) WHERE rownum = 1"

                        rsEPCS2.open sqlEPCS2,con
                        if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                            epcsPrintType = "EPCS-DISPENSE-FAILED"
                            epcsLastErrorNewRx_NCPDPID = rsEPCS2("NCPDPID")
                            epcsLastErrorNewRx_CreatedDate = rsEPCS2("Created_Date")
                        else
                            epcsPrintType = "EPCS-DISPENSE"
                        end if
                        rsEPCS2.close
                        
                    elseif UCase(status_newest) = "NEW" then
                        '5. Print
                        epcsPrintType = "EPCS-DISPENSE"
                    elseif UCase(status_newest) = "NEWRX--ERROR" then
                        '5. Print
                        epcsPrintType = "EPCS-FAILED"
                        epcsNCPDPID = ncpdpid_newest
                        epcsCreatedDate = createdDate_newest
                    elseif UCase(status_newest) = "NEWRX--OK" or UCase(status_newest) = "NEWRX--PENDING" or UCase(status_newest) = "NEWRX--VERIFIED" then
                        '5. Print
                        epcsPrintType = "EPCS-COPY"
                    end if
                end if
             else
                rsEPCS2.Close
             end if               
         else
            rsEPCS2.Close 
         end if         
    end if
end if
rsEPCS.Close

'Add to EPCS_PRESCRIPTIONS
if epcsPrintType <> "EPCS-NO" then
    if epcsPrintType = "EPCS-DISPENSE" or epcsPrintType = "EPCS-FAILED" or epcsPrintType = "EPCS-DISPENSE-FAILED" then
        set cmdEPCS = server.CreateObject("ADODB.command")
        cmdEPCS.ActiveConnection = CONN
	    sqlEPCS = "INSERT INTO EPCS_PRESCRIPTIONS(Drug_Id, Visit_Key, Patient_Id, Doctor_Id, Status, Created_Date) values (" & drug_list & ",'" & visit_key & "','" & patient_id & "','" & session("docid") & "', 'Printed', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'))"
        cmdEPCS.CommandText = sqlEPCS
	    cmdEPCS.Execute
    end if
end if

%>
<html>
<head>
<title>Medications &gt; Print Prescription</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
</head>

<body onload="window.focus();window.print();">

<div class="titleBox" style="background-color: #FFF; width: 550px; font-weight: normal; align: center; vertical-align: middle;">

<table width="100%">
<%

set res = server.CreateObject("adodb.recordset")
set res1 = server.CreateObject("adodb.recordset")

strsql="select doctor_id from visit where visit_key='"&visit_key&"'"
res.open strsql,CONN
doc_id= res("doctor_id")
res.Close

strsql="select b.orgname,a.first_name,a.last_name,a.middle_name,a.credential,a.street,a.city,a.state,a.zip,a.wphone,a.ext,a.fax,a.email,a.provider_licno,a.provider_licnostate,a.dea_no,a.dps_no,a.extra,a.speciality, a.npi from  doctortable a,streetreg b where a.FACILITY  = b.streetadd and a.docuser='"&doc_id&"'"
res.open strsql,CONN

name=res("first_name")&" "&res("middle_name")&" "&res("last_name")&", "&res("credential")
epcsAuditPrescriber = name
credential = res("credential")

dim res2
set res2 = Server.CreateObject("ADODB.Recordset")

res2.open "select * from pharmreg where id='"&SQLFixUp(request("pharmacy"))&"' and pid='"&session("pid")&"' order by org_name asc", con

%>

<%
 
 visit_key = Request("visit_key")
 
'dim cmd
set cmd = server.CreateObject("ADODB.command")
cmd.ActiveConnection = CONN
 	strSQL = "INSERT INTO audit_log(USERNAME,  SECTION_NAME, SUB_SECTION, VISIT_KEY, ACTION, FIELD,  OLD_VALUE, NEW_VALUE , TIME_STAMP , IP_ADDR, USER_ROLE, PATIENT_ID, PATIENT_LAST_NAME, PATIENT_FIRST_NAME, VISIT_DATE, VISIT_TIME) " &_
	" SELECT B.DOCTOR_ID, '','',B.visit_key,'Physician printing  medication details', '','','', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'),'','Physician', B.PATIENT_ID, A.last_name, A.first_name,  VISIT_DATE,VISIT_TIME  FROM PATIENTTABLE A, VISIT B WHERE A.USERID = B.patient_id AND B.visit_key = '" &  visit_key & "'"
'Response.Write strSQL
		cmd.CommandText = strSQL
		cmd.Execute 
'cmd.close
  
 
%>

<tr>
<td valign="top"><img src="/images/practice/rx.gif" border="0" align="left" WIDTH="46" HEIGHT="48"></td>
<td align="left" width="50%" valign="top">

<%
if not res2.EOF then
%>
<b><%=res2("org_name")%></b><br>
<% if res2("first_name") <> "" then %>
ATTN: <%=res2("first_name")%><br>
<% end if %>
<%=res2("address")%><br>
<%=res2("city")&", "&res2("state")&"  "&res2("zip")%><br>
<% if res2("tel") <> "" then %>
Tel: <%=res2("tel")%><br>
<% end if %>
<% if res2("fax") <> "" then %>
Fax: <%=res2("fax")%><br>
<% end if %>
<%
end if
%>
</td>
<td align="right" width="50%" valign="top">
<%=res("orgname")%> <br>
<% if session("pid") <> "JON1176" then %> <b><%=name%></b><br><% end if %>
<% if res("extra")<>"" then Response.Write "Collaborating Phy: "&res("extra")&"<br>" %>
<%=res("street")%><br>
<%=res("city")&", "&res("state")&"  "&res("zip")%><br>
Tel: <%=res("wphone")%><br>
<% if res("fax") <> "" then %>
Fax: <%=res("fax")%><br>
<% end if %>
DEA#&nbsp;<%=res("dea_no")%><br>
<% if res("dps_no") <> "" then Response.Write "DPS# "&res("dps_no")&"<br>" %>
<% if res("npi") <> "" then Response.Write "NPI# "&res("npi")&"<br>" %>
License#&nbsp;<%=res("provider_licno")%>
</td>
</tr>
</table>

<%
res.close
res1.open "select first_name,middle_name,last_name,birth_date,HOME_PHONE,street,city,state,zip,primary_insurance,patient_employer,ssn from patienttable where userid='"&patient_id&"'",CONN
%>
<p>
<%
if epcsPrintType <> "EPCS-NO" then
    if epcsPrintType = "EPCS-DISPENSE" then
        
    elseif epcsPrintType = "EPCS-FAILED" or epcsPrintType = "EPCS-DISPENSE-FAILED" then
        
        if epcsPrintType = "EPCS-DISPENSE-FAILED" then
            tempNCPDPID = epcsLastErrorNewRx_NCPDPID
            tempCreatedDate = epcsLastErrorNewRx_CreatedDate
        else
            tempNCPDPID = epcsNCPDPID
            tempCreatedDate = epcsCreatedDate
        end if

        sqlEPCS = "select * from SS_PHARMACY where NCPDPID = '" & tempNCPDPID & "'"
        rsEPCS.Open sqlEPCS, con
        if not (rsEPCS.EOF or rsEPCS.BOF) then
            response.Write("<b>The prescription was originally transmitted electronically to " & rsEPCS("storename") & " on " & tempCreatedDate & " and that transmission failed.</b>")
        end if
        rsEPCS.Close
    elseif epcsPrintType = "EPCS-COPY" then
        response.Write("<span stype=""color: red; font-weight: bold;"">Copy only - not valid for dispensing!</span>")
    end if

    epcsAuditPatient = res1("first_name") & " " & res1("last_name")

end if
%>
<div class="titleBox_HL" style="background-color: #FFF; font-weight: normal">
<b>Patient</b>
<p>
<%=res1("first_name")&" "&res1("last_name")%><br>
<%=res1("street")%><br>
<%=res1("city")&", "&res1("state")&" "&res1("zip")%><br>
<b>Phone:</b> <%=res1("HOME_PHONE")%><br>
<b>DOB:</b> <%=res1("birth_date")%>
</div>

<%
res1.close
%>
<p>
<%
res1.open "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and drug_id in (" & drug_list & ")",CONN
if not res1.eof then
  while not res1.eof
	epcsAuditMedication = res1("drug_name")
%>
<div class="titleBox_HL2" style="background-color: #FFF; font-weight: normal">
<img src="/images/practice/rx.gif" border="0" HEIGHT="12" align="left"> <b><%=res1("drug_name")%></b><p>

<%
if epcsPrintType <> "EPCS-NO" then
    Select Case epcsSchedule
        Case 2
            response.Write("Schedule: II")
        Case 3
            response.Write("Schedule: III")
        Case 4
            response.Write("Schedule: IV")
        Case 5
            response.Write("Schedule: V")
    End Select
    response.Write("<br/>")
end if
%>


Formulation: <%=res1("formulation")%><br>
<% if trim(res1("dispense").value) <> "" then %>
Dispense: <%=res1("dispense")%><br>
<% end if %>
Dose: <%=res1("dose")%><br>
Frequency: <%=res1("frequency")%><br>
Route: <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %><br>
Refill: <%=res1("refill")%><p>
<%
if res1("notes") <> "" then
    response.Write("Note: " & res1("notes"))
end if
%>
</div>
<p>
<%
    res1.MoveNext
  wend
end if
%>

<p>
<%
if epcsPrintType <> "EPCS-COPY" then
    %>
    <table height="100">
    <tr>
    <td valign="left">
    <span style="font-size: 24px">X</span>
    <%
    if show_mn = 1 then
	    session("authentication") = "1"
    %>
    <img src="/practice/customization/signature/display_signature.asp">
    <%
    else
    %>
    _____________
    <% end if %>
    ,<%=credential%> 
    <span style="font-size: 24px">X</span>
    <%
    if show_ds = 1 then
	    session("authentication") = "1"
    %>
    <img src="/practice/customization/signature/display_signature.asp">
    <%
    else
    %>
    _____________
    <% end if %>
    ,<%=credential%>
    </td></tr>
    <tr><td valign="middle">
    Substitution  Permitted&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dispense as Written
    <br><%=date()%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=date()%>
    </td></tr>
    </table>
    <%
end if
%>


<%
if epcsPrintType <> "EPCS-NO" then
    if epcsPrintType = "EPCS-DISPENSE" then
        response.Write("<b>* Must manually sign for dispensing!</b>")
    elseif epcsPrintType = "EPCS-FAILED" then
        response.Write("<b>* Must manually sign for dispensing!</b>")
    elseif epcsPrintType = "EPCS-COPY" then
        
    end if

    
    '---START: Add to EPCS_AUDIT_LOG
    detailAuditLog = ""
    detailAuditLog = "- Prescriber: " & epcsAuditPrescriber & "<br/>" &_
                      "- Patient: " & epcsAuditPatient & "<br/>" &_
                    "- Medication: " & epcsAuditMedication & "<br/>"

    if epcsPrintType = "EPCS-DISPENSE" then
        detailAuditLog = detailAuditLog & "- Type: Print for Dispensing"
    elseif epcsPrintType = "EPCS-FAILED" or epcsPrintType = "EPCS-DISPENSE-FAILED" then
        detailAuditLog = detailAuditLog & "- Type: Print for Dispensing of failed transmission"
    elseif epcsPrintType = "EPCS-COPY" then
        detailAuditLog = detailAuditLog & "- Type: Print for copy only"
    end if

    if detailAuditLog <> "" then
        call epcsAuditLogObj.LogEPCSActivity("Controlled Substance Prescription","Print CS Prescription",detailAuditLog,"SUCCESS")
    end if
    '---END
end if
%>

</div>

</body>
</html>
