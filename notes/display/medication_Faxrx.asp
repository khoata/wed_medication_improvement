<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id_hidden")

dim auditObj
set auditObj = new clsAudit

call auditObj.LogActivity("Accessing Medications Print page","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script language="javascript">
function editDrug() {

  var value = document.getElementById('drug_id_hidden').value;

  if (value != "" && value != null) {
    parent.leftNoteFrame.location='/practice/notes/edit/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=' + value;
  } else {
    alert("Please select a prescription to edit.")
  }
}

function printPrescription() {

  var list = "";

  for (var i = 0; i < document.medprint.elements.length; i++) {
    if (document.medprint.elements[i].type == "checkbox" && document.medprint.elements[i].name == "selected_mid") {
      if (document.medprint.elements[i].checked) {
        list = list + "'" + document.medprint.elements[i].value + "',";
      }
    }
  }

  list = escape(list.substr(0,list.length-1));

  pharmacy = document.getElementById('pharmacy').selectedIndex;

  if (pharmacy != 0) {
    pharmacy = document.getElementById('pharmacy').options[document.getElementById('pharmacy').selectedIndex].value;
  }

  if (list.length > 0)

	<%if session("pid")="ACC1280" then %>
		presWin = window.open('/practice/notes/display/medication_printrx_form_ny1.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&list=' + list + '&pharmacy=' + pharmacy + '&sig=' + document.getElementById('sig').checked + '&necessary=' + document.getElementById("necessary").checked,'prescription<%=visit_key%>','toolbar=no,menubar=no,scrollbars=yes,statusbar=no,width=640,height=400');
    <%else%>
		presWin = window.open('/practice/notes/display/medication_printrx_form.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&list=' + list + '&pharmacy=' + pharmacy + '&sig=' + document.getElementById('sig').checked + '&necessary=' + document.getElementById("necessary").checked,'prescription<%=visit_key%>','toolbar=no,menubar=no,scrollbars=yes,statusbar=no,width=640,height=400');
    <%end if%>
  else
    alert("Please select at least one medication to include in this prescription");
}

function FaxPrescription() {

  var list = "";

  for (var i = 0; i < document.medprint.elements.length; i++) {
    if (document.medprint.elements[i].type == "checkbox" && document.medprint.elements[i].name == "selected_mid") {
      if (document.medprint.elements[i].checked) {
        list = list + "'" + document.medprint.elements[i].value + "',";
      }
    }
  }

  list = escape(list.substr(0,list.length-1));

  pharmacy = document.getElementById('pharmacy').selectedIndex;

  if (pharmacy != 0) {
    pharmacy = document.getElementById('pharmacy').options[document.getElementById('pharmacy').selectedIndex].value;
  }

  if (list.length > 0)
    presWin = window.open('/practice/notes/display/medication_printrx_form_FX.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&list=' + list + '&pharmacy=' + pharmacy + '&sig=' + document.getElementById('sig').checked + '&necessary=' + document.getElementById("necessary").checked,'prescription<%=visit_key%>','toolbar=no,menubar=no,scrollbars=yes,statusbar=no,width=800,height=400');
  else
    alert("Please select at least one medication to include in this prescription");
}

</script>
</head>
	<body>

<div>
<div class="titleBox">Medications > Print Prescription</div>
<p>
Columns highlighted in <span style="background-color: #FFFFCC">yellow</span> indicate medication prescribed during this visit.
<p>
<form action="medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>" method="post" name="medprint" id="medprint">

<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">

  <tr bgcolor="#3366CC">
    <td></td>
    <td><font color="white"><b>Date</td>
    <td><font color="white"><b>Prescription</td>
    <td><font color="white"><b>Notes</td>
  </tr>


<%

set res1 = server.CreateObject("ADODB.Recordset")

sqlQuery = "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and status='Current' and patient_id='" & patient_id & "'"

res1.open sqlQuery,con
if not res1.eof then

	cnt = 1
	while not res1.eof

		if res1("visit_key") = visit_key then
			bgcolor = "#FFFFCC"
		else
			if cnt mod 2 = 0 then
				bgcolor = "#FFFFFF"
			else
				bgcolor = "#EFEFEF"
			end if
		end if
%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top"><input type="checkbox" name="selected_mid" value="<%=res1("drug_id")%>">
        <td valign="top"><b><%=res1("prescribed_date")%></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b> -
          <%=res1("formulation")%><br>
	      <b>Dose:</b> <%=res1("dose")%><br>
	      <b>Route:</b> <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %><br>
	      <% if trim(res1("dispense").value) <> "" then %>
	      <b>Dispense:</b> <%=res1("dispense")%><br>
	      <% end if %>
	      <b>Refill:</b> <%=res1("refill")%>
	      <% if res1("frequency") <> "" then %>
	      <br><b>Frequency:</b> <%=res1("frequency")%>
	      <% end if %>
	    </td>
	    <td class="st" valign="top"><%=res1("notes")%></td>
	</tr>
	<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close

else
%>
	<tr>
		<td colspan="8">No medications found on record.</td>
	</tr>
<%
end if
%>
</table>
<p>
<div class="titleBox_HL2">
Address this prescription to:
<select name="pharmacy" id="pharmacy">
<option value="0">None</option>
<%

dim res
set res = Server.CreateObject("ADODB.Recordset")

res.open "select * from pharmreg where pid='"&session("pid")&"' order by org_name asc", con

while not res.eof
%>
<option value="<%=res("id")%>"><%=res("org_name")%> (<%=res("address")%>, <%=res("city")&", "&res("state")&"  "&res("zip")%>)</option>
<%
  res.movenext
wend

res.close

%>
</select>
</div>
<p>
<%

res.Open "select count(*) as count from doctor_signature_info where doctor_id='" & session("user") & "'", con

dim have_sig
have_sig = cint(res("count").Value)

if have_sig > 0 then
%>
<div class="titleBox_HL2">
<b>Signature on file</b>
<p>
<input type="checkbox" value="1" name="sig" id="sig"> Dispense as Written - No Substitution<br>
<input type="checkbox" value="1" name="necessary" id="necessary"> Substitution Permitted?
</div>
<%
else
%>
<div class="titleBox_HL2">
We do not have a signature on file.  If you'd like to sign this prescription with a digitized signature, visit the Customization section to create one.
</div>
<input type="hidden" value="1" name="sig" id="sig">
<input type="hidden" value="1" name="necessary" id="necessary">
<%
end if
%>

<p>

<input type="button" onclick="FaxPrescription();" value="Fax Prescription" id=button1 name=button1>


<%if session("pid")="GRE1214" then %>
		<input type="button" onclick="document.location='/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>';" value="Cancel" id=button4 name=button4>
<%else%>
		<input type="button" onclick="document.location='/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>';" value="Cancel" id=button4 name=button4>
<%end if%>

</form>
</BODY>
</HTML>
