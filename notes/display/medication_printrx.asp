<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id_hidden")

dim auditObj
set auditObj = new clsAudit

call auditObj.LogActivity("Accessing Medications Print page","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script type="text/javascript" src="/library/jquery.min.js"></script>
<script language="javascript">
function editDrug() {

  var value = document.getElementById('drug_id_hidden').value;

  if (value != "" && value != null) {
    parent.leftNoteFrame.location='/practice/notes/edit/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=' + value;
  } else {
    alert("Please select a prescription to edit.")
  }
}

function printPrescription() {

  var list = "";

  for (var i = 0; i < document.medprint.elements.length; i++) {
    if (document.medprint.elements[i].type == "checkbox" && document.medprint.elements[i].name == "selected_mid") {
      if (document.medprint.elements[i].checked) {
        list = list + "'" + document.medprint.elements[i].value + "',";
      }
    }
  }

  list = escape(list.substr(0,list.length-1));

  pharmacy = document.getElementById('pharmacy').selectedIndex;

  if (pharmacy != 0) {
    pharmacy = document.getElementById('pharmacy').options[document.getElementById('pharmacy').selectedIndex].value;
  }

  if (list.length > 0)

	<%if session("pid")="ACC1280" then %>
		presWin = window.open('/practice/notes/display/medication_printrx_form_ny1.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&list=' + list + '&pharmacy=' + pharmacy + '&sig=' + document.getElementById('sig').checked + '&necessary=' + document.getElementById("necessary").checked,'prescription<%=visit_key%>','toolbar=no,menubar=no,scrollbars=yes,statusbar=no,width=640,height=400');
    <%else%>
		presWin = window.open('/practice/notes/display/medication_printrx_form.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&list=' + list + '&pharmacy=' + pharmacy + '&sig=' + document.getElementById('sig').checked + '&necessary=' + document.getElementById("necessary").checked,'prescription<%=visit_key%>','toolbar=no,menubar=no,scrollbars=yes,statusbar=no,width=640,height=400');
    <%end if%>
  else
    alert("Please select at least one medication to include in this prescription");
}

var countEPCSChecked = 0;

function CheckMultiplePrint(obj, isEPCS) {
    if (document.medprint.doctorHasEPCS.value == "Y"){

        if(isEPCS == "Y"){
            if(obj.checked == true) {
                countEPCSChecked = countEPCSChecked + 1;   
            }
            else {
                countEPCSChecked = countEPCSChecked - 1;
            }
        }

        if (obj.checked == true){
            var countChecked = 0;
            $(":checkbox").each(function(){
                if($(this)[0].checked == true)
                {
                    countChecked = countChecked + 1;
                }
            });

            if(countEPCSChecked > 1 || (countEPCSChecked == 1 && countChecked > 1))
            {
//                alert("This is controlled substance. You are only allowed to print it for dispensing as a single prescription! ")
                alert("Controlled substance must be printed for dispensing in one prescription separately!")

                if(isEPCS == "Y") countEPCSChecked = countEPCSChecked - 1;
                return false;
            }

            var epcsObj = $(obj).parent().parent().find('.epcsStatus').html();

            if ( epcsObj.length == 0) return true;

            var status = epcsObj;

            if(status == "READY")
            {
                var epcsAlert = $(obj).parent().parent().find('.epcsAlert').html();

                if (epcsAlert.length == 0) {
                    if(isEPCS == "Y") countEPCSChecked = countEPCSChecked - 1;
                    return false;
                }

                if(epcsAlert == "Y")
                {
                    var printForDispense = confirm("After printing this controlled substance prescription for dispense, you will not be able to order it electronically in this encounter.");
                    if(printForDispense == false)
                    {
                        if(isEPCS == "Y") countEPCSChecked = countEPCSChecked - 1;
                        return false;
                    }
                }
            }
            else if (status == "ORDERED"){
                alert("This controlled substance drug was transmissed successfully in this encounter. You can print it for copy only in the same encounter.");
            }
            else if (status == "PENDING"){
                alert("This controlled substance drug was transmissed but still in pending status. You can print it for copy only in the same encounter.");
            }
			else if (status == "VERIFIED"){
                alert("This controlled substance drug was transmissed but still in VERIFIED status. You can print it for copy only in the same encounter.");
            }
            else{
                if(status == "NO"){
                    alert("Can not print controlled substance which was added, printed or ordered in newer encounter!");
                }

                if(isEPCS == "Y") countEPCSChecked = countEPCSChecked - 1;
                return false;
            }
        }
    }
    return true;
}

</script>
</head>
<body>
<div class="titleBox">Medications > Print Prescription</div>
<p>
Columns highlighted in <span style="background-color: #FFFFCC">yellow</span> indicate medication prescribed during this visit.
<p>
<form action="medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>" method="post" name="medprint" id="medprint">

<table id="tbl_Print" border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">

  <tr bgcolor="#3366CC">
    <td width="2%"></td>
    <td width="6%"><font color="white"><b>Date</td>
    <td width="40%"><font color="white"><b>Prescription</td>
    <td width="26%"><font color="white"><b>Detail for EPCS</td>
    <td width="26%"><font color="white"><b>Notes</td>
  </tr>


<%

set res1 = server.CreateObject("ADODB.Recordset")

sqlQuery = "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and status='Current' and patient_id='" & patient_id & "'"

res1.open sqlQuery,con
if not res1.eof then

	cnt = 1
	while not res1.eof

		if res1("visit_key") = visit_key then
			bgcolor = "#FFFFCC"
		else
			if cnt mod 2 = 0 then
				bgcolor = "#FFFFFF"
			else
				bgcolor = "#EFEFEF"
			end if
		end if
		
        set rs = server.CreateObject("ADODB.Recordset")

        sqlQuery = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and upper(brand_name) = '" & ucase(res1("drug_name")) &"'"

		rs.open sqlQuery,con
		if not (rs.eof or rs.bof) then
			Is_CS="Y"
		else
			Is_CS="N"
		end if
		rs.close
%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top"><input class="chkCheckBox" type="checkbox" name="selected_mid" value="<%=res1("drug_id")%>" onclick="return CheckMultiplePrint(this, '<%=Is_CS %>');" /></td>
        <td valign="top"><b><%=res1("prescribed_date")%></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b> -
          <%=res1("formulation")%><%if Is_CS="Y" then%><img alt="controlled substance" src="/images/practice/red_star.gif" border="0" /><%end if%><br>
	      <b>Dose:</b> <%=res1("dose")%><br>
	      <b>Route:</b> <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %><br>
	      <% if trim(res1("dispense").value) <> "" then %>
	      <b>Dispense:</b> <%=res1("dispense")%><br>
	      <% end if %>
	      <b>Refill:</b> <%=res1("refill")%>
	      <% if res1("frequency") <> "" then %>
	      <br><b>Frequency:</b> <%=res1("frequency")%>
	      <% end if %>
	    </td>
		<td>
            <%
                sqlEPCS = "SELECT v.VISIT_KEY, VISIT_DATE, VISIT_TIME " &_
                         "FROM VISIT v " &_
                         "INNER JOIN (SELECT VISIT_KEY FROM (SELECT * FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC) WHERE rownum = 1) p " &_
                         "ON v.VISIT_KEY = p.VISIT_KEY "

                set rsEPCS = server.CreateObject("ADODB.Recordset")
                rsEPCS.open sqlEPCS,con

                if not (rsEPCS.EOF or rsEPCS.BOF) then

                    set rsEPCS2 = server.CreateObject("ADODB.Recordset")

                    sqlEPCS2 = "SELECT Status, Created_Date, ss_message_id FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC"
                    
                    rsEPCS2.open sqlEPCS2,con
                    while not rsEPCS2.EOF
                        if UCase(rsEPCS2("Status")) <> "NEW" then
                            response.Write("- <b>" & rsEPCS2("Status") & "</b> ")
                            if UCase(rsEPCS2("Status")) <> "PRINTED" then
                                response.Write("signed on " & rsEPCS2("Created_Date"))
                                'checkChangeResult = FUNC_CHECKCHANGES(rsEPCS2("ss_message_id"))
                                'if checkChangeResult <> "VALID" then
                                '    response.Write(" <span style=""color: red; text-decoration: underline; cursor: pointer;"" onclick=""showIllegalChanges('" & checkChangeResult & "');"">Changed!</span>")
                                'end if
                                response.Write("<br/>")
                            else
                                response.Write("on " & rsEPCS2("Created_Date") & "<br/>")
                            end if
                        end if
                        rsEPCS2.movenext
                    wend
                    rsEPCS2.close

                    response.Write("- <b>Encounter:</b> " & rsEPCS("VISIT_DATE") & " " & rsEPCS("VISIT_TIME"))
                    if rsEPCS("VISIT_KEY") = visit_key then
                        response.Write(" (Current)")
                    end if

                    sqlEPCS2 = "SELECT VISIT_ID FROM VISIT WHERE VISIT_KEY = '" & visit_key & "'"

                    rsEPCS2.open sqlEPCS2,con
                    if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                        visit_id_current = CLng(rsEPCS2("VISIT_ID"))
                    end if
                    rsEPCS2.close

                    sqlEPCS2 = "SELECT VISIT_ID FROM VISIT WHERE VISIT_KEY = '" & rsEPCS("VISIT_KEY") & "'"

                    rsEPCS2.open sqlEPCS2,con
                    if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                        visit_id_newest = CLng(rsEPCS2("VISIT_ID"))
                    end if
                    rsEPCS2.close

                    if visit_id_current < visit_id_newest then
                        %>
                        <span class="epcsStatus" style="display: none;">NO</span>
                        <%
                    elseif visit_id_current > visit_id_newest then
                        %>
                        <span class="epcsStatus" style="display: none;">READY</span>
                        <span class="epcsAlert" style="display: none;">Y</span>
                        <%
                    else
                        sqlEPCS2 = "SELECT STATUS FROM (SELECT * FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC) WHERE rownum = 1"

                        rsEPCS2.open sqlEPCS2,con
                        if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                            if UCase(rsEPCS2("STATUS")) = "NEWRX--OK" then
                                %>
                                <span class="epcsStatus" style="display: none;">ORDERED</span>
                                <% 
                            elseif UCase(rsEPCS2("STATUS")) = "NEWRX--PENDING" then
                                %>
                                <span class="epcsStatus" style="display: none;">PENDING</span>
                                <%
							elseif UCase(rsEPCS2("STATUS")) = "NEWRX--VERIFIED" then
                                %>
                                <span class="epcsStatus" style="display: none;">VERIFIED</span>
                                <%
                            else
                                %>
                                <span class="epcsStatus" style="display: none;">READY</span>
                                <%

                                if UCase(rsEPCS2("STATUS")) = "PRINTED" then
                                    %>
                                    <span class="epcsAlert" style="display: none;">N</span>
                                    <%
                                else
                                    %>
                                    <span class="epcsAlert" style="display: none;">Y</span>
                                    <%
                                end if
                            end if
                        end if
                        rsEPCS2.close
                    end if

                end if
                rsEPCS.close
            %>
        </td>
	    <td class="st" valign="top"><%=res1("notes")%></td>
	</tr>
	<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close

else
%>
	<tr>
		<td colspan="8">No medications found on record.</td>
	</tr>
<%
end if
%>
</table>
<p>
<div class="titleBox_HL2">
Address this prescription to:
<select name="pharmacy" id="pharmacy">
<option value="0">None</option>
<%

dim res
set res = Server.CreateObject("ADODB.Recordset")

res.open "select * from pharmreg where pid='"&session("pid")&"' order by org_name asc", con

while not res.eof
%>
<option value="<%=res("id")%>"><%=res("org_name")%> (<%=res("address")%>, <%=res("city")&", "&res("state")&"  "&res("zip")%>)</option>
<%
  res.movenext
wend

res.close

%>
</select>
</div>
<p>
<%

res.Open "select count(*) as count from doctor_signature_info where doctor_id='" & session("user") & "'", con

dim have_sig
have_sig = cint(res("count").Value)

if have_sig > 0 then
%>
<div class="titleBox_HL2">
<b>Signature on file</b>
<p>
<input type="checkbox" value="1" name="sig" id="sig"> Dispense as Written - No Substitution<br>
<input type="checkbox" value="1" name="necessary" id="necessary"> Substitution Permitted?
</div>
<%
else
%>
<div class="titleBox_HL2">
You are allowed to select only one controlled substance as a single prescription per print.
</div>
<p>
<div class="titleBox_HL2">
We do not have a signature on file.  If you'd like to sign this prescription with a digitized signature, visit the Customization section to create one.
</div>
<input type="hidden" value="1" name="sig" id="sig">
<input type="hidden" value="1" name="necessary" id="necessary">
<%
end if
%>

<p>
<input type="button" onclick="printPrescription();" value="Print Prescription">

<%if session("pid")="GRE1214" then %>
		<input type="button" onclick="document.location='/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>';" value="Cancel" id=button4 name=button4>
<%else%>
		<input type="button" onclick="document.location='/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>';" value="Cancel" id=button4 name=button4>
<%end if%>
<br/>
<img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /> Controlled substance

<%
strSQL = "select * from doctortable where docuser='" & session("docid") & "' and EPCS = 'Y' and bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  2048) = 2048"
rs.Open strSQL, con
%>
<input type="hidden" name="doctorHasEPCS" value="<%if not rs.EOF then %>Y<% else %>N<%end if %>" />       
<%
rs.Close
%>
</form>
</BODY>
</HTML>

<%
FUNCTION FUNC_CHECKCHANGES(ssMessageId)
    Set oXMLHTTP = CreateObject("MSXML2.XMLHTTP")
    Set oXMLDoc = CreateObject("MSXML2.DOMDocument")
    Server.ScriptTimeout=10000
    dim url: url = "http://192.168.72.8/surescriptInterface/EPCS_Signature_Page.aspx?runType=verify&ssMessageId="&ssMessageId
    call oXMLHTTP.open("GET",url,false)
    call oXMLHTTP.setRequestHeader("Content-Type","application/x-www-form-urlencoded")

    call oXMLHTTP.send()

    FUNC_CHECKCHANGES = oXMLHTTP.responseText
END FUNCTION

%>
