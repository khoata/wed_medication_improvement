<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")

dim res
dim res1
set res = server.CreateObject("adodb.recordset")
set res1 = server.CreateObject("adodb.recordset")

if Request("drug_name") <>"" then
	dname=request("drug_name")
	strength=request("strength")
else
	dname=""
	strength=""
end if

%>

<html>
<head>
<title>[W] - Medication Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
</head>
<body onLoad="document.frm.drug_name.focus();">
<form name=frm action="medication_search.asp" method=get>
<div class="titleBox">Quick Drug Search > <span class="st">Drug Name</span> <input type="text" name="drug_name" size="20" value="<%=Request("drug_name")%>"> <input type=submit name="submit" value="Search"></div>
<p>
<input type=hidden name=visit_key value="<%=visit_key%>">
<input type=hidden name=patient_id value="<%=patient_id%>">
<input type=hidden name=type value=1>
<input type=hidden name=operation value=<%=Request("operation")%>>
<input type=hidden name=by value=<%=session("by")%>>
<input type=hidden name=type value="yes">
</form>

<%

name = Request("drug_name")
strength = Request("strength")

if name <> "" then
%>

<!--<div style="display: block; position; normal; height: 100%; width: 100%; overflow: scroll;">-->
<table width="100%" cellspacing="1" cellpadding="3" border="0" bgcolor="#CCCCCC">
<%
	iPageSize = 25 ' You could easily allow users to change this

	If Request.QueryString("page") = "" Then
		iPageCurrent = 1
	Else
		iPageCurrent = CInt(Request.QueryString("page"))
	End If

	res.CursorLocation = 3
	res.PageSize = iPageSize
	res.CacheSize = iPageSize

	res.Open "select distinct " &_
					"a.brand_description as brand_name," &_
					"g.route_abbr," &_
					"g.route_description," &_
					"b.product_strength_description as strength," &_
					"c.drug_id as multum_drug_id," &_
					"a.brand_code as brand_code," &_
					"e.dose_from_description as dose_form," &_
					"f.drug_name " &_
				"from ndc_brand_name a, multum_product_strength b, ndc_main_multum_drug_code c, ndc_core_description d, multum_dose_form e, multum_drug_id f, multum_route g " &_
				"where " &_
					"a.brand_code = d.brand_code " &_
					"and d.main_multum_drug_code = c.main_multum_drug_code " &_
					"and b.product_strength_code = c.product_strength_code " &_
					"and c.principal_route_code = g.route_code " &_
					"and c.drug_id = f.drug_id " &_
					"and c.dose_form_code = e.dose_form_code " &_
					"and a.brand_description like initcap('"&name&"%') " &_
					"and b.product_strength_description like '"&strength&"%' " &_
					"order by brand_name asc",CONN

	iPageCount = res.PageCount

	If iPageCurrent > iPageCount Then iPageCurrent = iPageCount
	If iPageCurrent < 1 Then iPageCurrent = 1

	If iPageCount = 0 Then
		response.Write "<tr bgcolor=#FFFFFF><td colspan=10><b>No medications found with search criteria.</td></tr>"
	Else
		res.AbsolutePage = iPageCurrent
%>
	<tr bgcolor="#DEE8FD">
		<td><b>Drug Name</b></td>
		<td><b>Formulation</b></td>
		<td><b>Route</b></td>
	</tr>
<%
	iRecordsShown = 0
	
	do while iRecordsShown < iPageSize And not res.eof
		
		dose_form = res("dose_form")
	
		if iRecordsShown mod 2 = 0 then
			bgcolor = "#FFFFFF"
		else
			bgcolor = "#EFEFEF"
		end if
	
  %>
	<tr bgcolor="<%=bgcolor%>">
		<td><b><a href="#" onclick="setValue('<%=res("brand_name")%>','<%=res("strength")%>&nbsp;<%=res("dose_form")%>','<%=res("route_abbr")%>','<%=res("dose_form")%>','<%=res("multum_drug_id")%>','<%=res("brand_code")%>')"><%=res("brand_name")%></a></b> <span class="st"><i>(<%=res("drug_name")%>)</i></span></td>
		<td class="st"><%=res("strength")%>&nbsp;<%=res("dose_form")%></td>
		<td class="st"><%=res("route_description")%></td>
	</tr>	
<%
	iRecordsShown = iRecordsShown + 1
	res.movenext
loop

%>
</table>
<!--</div>-->

<table width="100%" cellspacing="1" cellpadding="3" border="0" bgcolor="#CCCCCC">
	<tr>
		<td bgcolor="#FFFFFF">

<%
' Close DB objects and free variables
res.Close
Set res = Nothing
'con.Close
'Set con = Nothing
%>
	Page <B><%= iPageCurrent %></B> of <B><%= iPageCount %>
	<%
	' Spacing
	response.Write " - " & vbCrLf

' Show "previous" and "next" page links which pass the page to view
' and any parameters needed to rebuild the query.  You could just as
' easily use a form but you'll need to change the lines that read
' the info back in at the top of the script.
If iPageCurrent > 1 Then
	%>
	<a href="medication_search.asp?drug_name=<%=name%>&strength=<%=strength%>&page=<%= iPageCurrent - 1 %>&order=<%= Server.URLEncode(strOrderBy) %>">&lt;&lt;</a>&nbsp;
	<%
End If

' You can also show page numbers:
if iPageCount-iPageCurrent < 10  then
	For I = 1 to iPageCount
		If I = iPageCurrent Then
		%>
		<%= I %>
		<%
		Else
		%>
		<a href="medication_search.asp?drug_name=<%=name%>&strength=<%=strength%>&page=<%= I %>&order=<%= Server.URLEncode(strOrderBy) %>"><%= I %></a>
		<%
		End If
	Next 'I
else
	For I = iPageCurrent To iPageCurrent+10
		If I = iPageCurrent Then
		%>
		<%= I %>
		<%
		Else
		%>
		<a href="medication_search.asp?drug_name=<%=name%>&strength=<%=strength%>&page=<%= I %>&order=<%= Server.URLEncode(strOrderBy) %>"><%= I %></a>
		<%
		End If
	Next 'I
end if
	

If iPageCurrent < iPageCount Then
	%>
	&nbsp;<a href="medication_search.asp?drug_name=<%=name%>&strength=<%=strength%>&page=<%= iPageCurrent + 1 %>&order=<%= Server.URLEncode(strOrderBy) %>">&gt;&gt;</a>
	<%
End If

' END RUNTIME CODE
end if
%>


		</td>
	</tr>		
</table>

<%
end if
%>


<script language="javascript">
function setValue(Drug_Name, Formulation, Route, Form, Multum_Drug_ID,Multum_Brand_Code)
{	
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.nm_med.value = Drug_Name;
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.formulation.value = Formulation;
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.multum_drug_id.value = Multum_Drug_ID;
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.multum_brand_code.value = Multum_Brand_Code;
	
	for (var i = 0; i < getFrameByName(top,"leftNoteFrameBottom").document.frm2.route_select.length; i++) {
	  if (getFrameByName(top,"leftNoteFrameBottom").document.frm2.route_select[i].value == Route) {
	    getFrameByName(top,"leftNoteFrameBottom").document.frm2.route_select[i].selected = true;
	  }
	}
	
	getFrameByName(top,"leftNoteFrameBottom").focus();
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.dosage.focus();
	
	re = new RegExp(Form.toUpperCase(), "i");
	//alert(re);
	
	for (var i = 0; i < getFrameByName(top,"leftNoteFrameBottom").document.frm2.dosage_units_select.length; i++) {
	  var a=  (getFrameByName(top,"leftNoteFrameBottom").document.frm2.dosage_units_select[i].text).toUpperCase()
	  if (a.match(re)) {
		//alert('hello');
	    getFrameByName(top,"leftNoteFrameBottom").document.frm2.dosage_units_select[i].selected = true;
	  }
	}
	getFrameByName(top,"leftNoteFrameBottom").setDosage(getFrameByName(top,"leftNoteFrameBottom").document.getElementById('dosage_units_select'));
	document.location = '/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>';
}
</script>



</body>
</html>