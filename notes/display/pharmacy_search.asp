<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../../../library/ss_servicelevel.inc.asp"-->

<%
'--get the preferred pharmacy status
set RS = server.createobject("adodb.recordset")

patient_id= request("patient_id")
strSQL="select PREFERRED_PHARMACY, NCPDPID from patienttable where PREFERRED_PHARMACY is not null and NCPDPID is not null and userid= '"& patient_id &"%'"
RS.Open strSQL,CONN

If not(rs.EOF or rs.BOF) then
	pref_phar="Yes"
else
	pref_phar="No"
end if
rs.Close

%>

<html>
<head>
<title>Pharmacy Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>

 <script language="javascript">
    function setpharmacy(p_code, p_name, p_type)
    {
        var pref_phar,p_id;
	
        pref_phar='<%=pref_phar%>'
        p_id=document.f1.patient_id.value;	

        window.opener.document.medprint.NCPDPID.value = p_code;
        window.opener.document.medprint.ph_name.value = p_name;
        document.f1.action = "pharmacy_pref_save.asp?p_code=" + p_code + "&patient_id=" + p_id + "&p_type=" + p_type;
        document.f1.submit();
    }
</script>

</head>


<body onload="document.getElementById('nameField').focus();">
<%
    pharmacy_type=trim(Request.form("pharmacy_type"))
	pharmacy_name=ucase(trim(Request.form("pharmacy_name")))	
	city_name=ucase(trim(Request.form("city_name")))
	zip=ucase(trim(Request.form("zip")))
	PHONEPRIMARY=ucase(trim(Request.form("PHONEPRIMARY")))
	chkEPCSOnly = ucase(trim(Request.form("chkEPCSOnly")))
    chkCancelRxOnly = ucase(trim(Request.form("chkCancelRxOnly")))
    chkChangeRxOnly = ucase(trim(Request.form("chkChangeRxOnly")))
%>


<form action="<%=Request.ServerVariables("script_name")%>" method="post" name="f1">

<input type="hidden" name="patient_id" value="<%=patient_id%>">
<div>
<%
strSQL="select * "
strSQL= strSQL&" from patienttable pt "
strSQL= strSQL&" left join (select ncpdpid retailncpdpid,storename retailstorename,addressline1 retailaddressline1,city retailcity,state retailstate,zip retailzip, servicelevel retailservicelevel from ss_pharmacy) retail on pt.recentretailphar = retail.retailncpdpid "
strSQL= strSQL&" left join (select ncpdpid mailncpdpid,storename mailstorename,addressline1 mailaddressline1,city mailcity,state mailstate,zip mailzip, servicelevel mailservicelevel from ss_pharmacy) mail on pt.recentmailorderphar = mail.mailncpdpid "
strSQL= strSQL&" left join (select ncpdpid specncpdpid,storename specstorename,addressline1 specaddressline1,city speccity,state specstate,zip speczip, servicelevel specservicelevel from ss_pharmacy) spec on pt.recentspecialtyphar = spec.specncpdpid "
strSQL= strSQL&" left join (select ncpdpid ltcncpdpid,storename ltcstorename,addressline1 ltcaddressline1,city ltccity,state ltcstate,zip ltczip, servicelevel ltcservicelevel from ss_pharmacy) ltc on pt.recentlongtermcarephar = ltc.ltcncpdpid "
strSQL= strSQL&" where userid = '"& patient_id &"' "
RS.Open strSQL,CONN


	

%>
    <div><b>Recently</b></div>
    <div><ul>
    <% 
        If not(rs.EOF or rs.BOF) then 
            if rs("retailstorename") <> "" then
                retailstorename=trim(replace(rs("retailstorename"),"'",""))
            end if
            if rs("mailstorename") <> "" then
                mailstorename=trim(replace(rs("mailstorename"),"'",""))
            end if
            if rs("specstorename") <> "" then
                specstorename=trim(replace(rs("specstorename"),"'",""))
            end if
            if rs("ltcstorename") <> "" then
                ltcstorename=trim(replace(rs("ltcstorename"),"'",""))
            end if
    %>
        <li>Retail: <% if rs("retailncpdpid") <> "" then %><a href="#" onclick="setpharmacy('<%=rs("retailncpdpid")%>','<%=retailstorename%> - <%=RS("retailaddressline1")%>&nbsp;<%=RS("retailcity")%>&nbsp;<%=RS("retailstate")%>&nbsp;-&nbsp;<%=RS("retailzip")%>', '')"><%=RS("retailstorename")%></a><% end if %>
        <% if ( rs("retailservicelevel") And SERVICELEVEL_EPCS ) = SERVICELEVEL_EPCS then %><img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /><% end if %></li>
        <li>Mail Order: <% if rs("mailncpdpid") <> "" then %><a href="#" onclick="setpharmacy('<%=rs("mailncpdpid")%>','<%=mailstorename%> - <%=RS("mailaddressline1")%>&nbsp;<%=RS("mailcity")%>&nbsp;<%=RS("mailstate")%>&nbsp;-&nbsp;<%=RS("mailzip")%>', '')"><%=RS("mailstorename")%></a><% end if %>
        <% if ( rs("mailservicelevel") And SERVICELEVEL_EPCS ) = SERVICELEVEL_EPCS then %><img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /><% end if %></li>
        <li>Specialty: <% if rs("specncpdpid") <> "" then %><a href="#" onclick="setpharmacy('<%=rs("specncpdpid")%>','<%=specstorename%> - <%=RS("specaddressline1")%>&nbsp;<%=RS("speccity")%>&nbsp;<%=RS("specstate")%>&nbsp;-&nbsp;<%=RS("speczip")%>', '')"><%=RS("specstorename")%></a><% end if %>
        <% if ( rs("specservicelevel") And SERVICELEVEL_EPCS ) = SERVICELEVEL_EPCS then %><img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /><% end if %></li>
        <li>Long Term Care: <% if rs("ltcncpdpid") <> "" then %><a href="#" onclick="setpharmacy('<%=rs("ltcncpdpid")%>','<%=ltcstorename%> - <%=RS("ltcaddressline1")%>&nbsp;<%=RS("ltccity")%>&nbsp;<%=RS("ltcstate")%>&nbsp;-&nbsp;<%=RS("ltczip")%>', '')"><%=RS("ltcstorename")%></a><% end if %>
        <% if ( rs("ltcservicelevel") And SERVICELEVEL_EPCS ) = SERVICELEVEL_EPCS then %><img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /><% end if %></li>
    <% 
        else
    %>
        <li>Retail: </li>
        <li>Mail Order: </li>
        <li>Specialty: </li>
        <li>Long Term Care: </li>
    <%
        end if
        rs.Close 
    %>
    </ul></div>
</div>
<div class="titleBox" align="center">Pharmacy Lookup</div>
<div class="titleBox">
 Pharmacy Type: <select name="pharmacy_type">
    <option value="" >All</option>
    <option value="Retail" <% if pharmacy_type = "Retail" then %> selected="selected" <% end if %>>Retail</option>
    <option value="MailOrder" <% if pharmacy_type = "MailOrder" then %> selected="selected" <% end if %>>Mail Order</option>
    <option value="Specialty" <% if pharmacy_type = "Specialty" then %> selected="selected" <% end if %>>Specialty</option>
    <option value="LongTermCare" <% if pharmacy_type = "LongTermCare" then %> selected="selected" <% end if %>>Long Term Care</option>
 </select>&nbsp;&nbsp;
 Pharmacy Name: <input type=text size=15 maxlength=15 id="nameField" name="pharmacy_name" value="<%=pharmacy_name%>"> &nbsp;&nbsp;
 City: <input type=text size=15 maxlength=15 name="city_name" value="<%=city_name%>"> &nbsp;&nbsp;
 ZIP: <input type=text size=10 maxlength=5 name="zip" value="<%=zip%>"> &nbsp;&nbsp;
 Phone: <input type=text size=10 maxlength=10 name="PHONEPRIMARY" value="<%=PHONEPRIMARY%>"> 
 <input type="checkbox" name="chkEPCSOnly" value="true" <% if chkEPCSOnly = "TRUE" then response.write("checked") end if %> />EPCS Enabled &nbsp;&nbsp;
 <input type="checkbox" name="chkCancelRxOnly" value="true" <% if chkCancelRxOnly = "TRUE" then response.write("checked") end if %> />Cancel Enabled &nbsp;&nbsp;
 <input type="checkbox" name="chkChangeRxOnly" value="true" <% if chkChangeRxOnly = "TRUE" then response.write("checked") end if %> />Change Enabled &nbsp;&nbsp;
 <input type="submit" value="Search" id=button1 name=button1>

</div><br>


<p>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
  <tr>
    <td>
      <table cellspacing="0" cellpadding="3" border="0" class="sched" width="100%">
        <tr bgcolor="#3366CC">
          <td class="w">Pharmacy Name</td>
          <td class="w">Address</td>
          <td class="w">Phone</td>
          <td class="w">Pharmacy Type</td>
        </tr>


<%
if  pharmacy_type<>"" or pharmacy_name<>"" or zip<>"" or city_name<>"" or PHONEPRIMARY <>"" then


strSQL="select * from ss_pharmacy where 1=1 And isactive = '1' "

if pharmacy_name<>"" then
  encoded_pharmacy_name = ReplaceStr(pharmacy_name, "'", "''", 0)
	strSQL= strSQL &" And upper(storename) like upper('"&encoded_pharmacy_name&"%')"
end if
if city_name<>"" then
  encoded_city_name = ReplaceStr(city_name, "'", "''", 0)
	strSQL= strSQL &" And upper(city) like upper('"&encoded_city_name&"%')"
end if
if zip<>"" then
  encoded_zip = ReplaceStr(zip, "'", "''", 0)
	strSQL= strSQL &" And (upper(zip) like upper('"&encoded_zip&"%'))"
end if
if pharmacy_type<>"" then
  encoded_pharmacy_type = ReplaceStr(pharmacy_type, "'", "''", 0)
  strSQL= strSQL & " And (specialtytype1 = '"&encoded_pharmacy_type&"' or specialtytype2 = '"&encoded_pharmacy_type&"' or specialtytype3 = '"&encoded_pharmacy_type&"' or specialtytype4 = '"&encoded_pharmacy_type&"') "
end if
if PHONEPRIMARY <>"" then
  encoded_PHONEPRIMARY = ReplaceStr(PHONEPRIMARY, "'", "''", 0)
	strSQL= strSQL &" And upper(PHONEPRIMARY) like upper('"&encoded_PHONEPRIMARY&"%')"
end if
if chkEPCSOnly = "TRUE" then
    strSQL= strSQL &" And bitand(cast(nvl(servicelevel,'0') as varchar2(20)) ,  2048) = 2048"
end if
if chkCancelRxOnly = "TRUE" then
    strSQL= strSQL &" And bitand(cast(nvl(servicelevel,'0') as varchar2(20)) ,  16) = 16"
end if
if chkChangeRxOnly = "TRUE" then
    strSQL= strSQL &" And bitand(cast(nvl(servicelevel,'0') as varchar2(20)) ,  4) = 4"
end if


strSQL= strSQL &" order by storename"

RS.Open strSQL,CONN
i=0
displayedPharmacies = 0
if not (rs.EOF or rs.BOF) then
	while not RS.EOF
	storename=trim(replace(rs("storename"),"'",""))
    slevel = rs("servicelevel")
    if slevel <> "" then
        servicelevel = CLng(slevel)
        if ( servicelevel And SERVICELEVEL_NEWRX ) = SERVICELEVEL_NEWRX then
            displayedPharmacies = displayedPharmacies + 1
%>
      <tr> 
		  <td>
            <a href="#" onclick="setpharmacy('<%=rs("NCPDPID")%>','<%=storename%> - <%=RS("ADDRESSLINE1")%>&nbsp;<%=RS("CITY")%>&nbsp;<%=RS("STATE")%>&nbsp;-&nbsp;<%=RS("ZIP")%>&nbsp;-&nbsp;PH:&nbsp;<%=RS("PHONEPRIMARY")%>', '<%=pharmacy_type %>')"><%=RS("storename")%></a>
            <% if ( servicelevel And SERVICELEVEL_EPCS ) = SERVICELEVEL_EPCS then %>
                <img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" />
            <% end if %>
          </td>
          <td><%=RS("ADDRESSLINE1")%>&nbsp;<%=RS("CITY")%>&nbsp;<%=RS("STATE")%>&nbsp;-&nbsp;<%=RS("ZIP")%></td>
		  <td><%=RS("PHONEPRIMARY")%></td>
          <td><%=RS("specialtytype1") %><% if RS("specialtytype2")<>"" then %>, <%=RS("specialtytype2") %><% end if %><% if RS("specialtytype3")<>"" then %>, <%=RS("specialtytype3") %><% end if %><% if RS("specialtytype4")<>"" then %>, <%=RS("specialtytype4") %><% end if %></td>
      </tr>
<%	    end if
    end if
    i=i+1
	RS.movenext
	wend
end if
%>
<%
if (displayedPharmacies = 0) then
	Response.Write "<tr><td>No records found</td></tr>"
end if
%>
 </table>
    </td>
  </tr>
</table>

<br/>


<% end if %>

</form>
<img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /> Controlled Substance
</body>

</html>