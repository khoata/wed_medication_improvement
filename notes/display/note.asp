<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/templatedesign.asp"-->
<script type="text/javascript" src="../../../library/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="../../../library/htmltooltip.js"></script>
<style type="text/css">
div.htmltooltip{
position: absolute; /*leave this and next 3 values alone*/
z-index: 1000;
left: -1000px;
top: -1000px;
background: #3366cc;
border: 10px black;
color: white;
padding: 10px;
width: 500px; /*width of tooltip*/
}
.com1 { background:url(../../../library/images/com_ico1.png) no-repeat right 0; padding:0px 20px 0 0;}
.com2 { padding:0px 20px 0 0;}
</style>

<%
    
visit_key = Request("visit_key")
template_id = Request("template_id")
patient_id = Request("patient_id")

dim patientNotesObj
set patientNotesObj = new clsPatientNotes
patientNotesObj.SetVisitKey(visit_key)
patient_id = patientNotesObj.ID

'Response.Buffer = false

dim hlcolor
hlcolor = "#FFCC66"

function substitute_text(text, patient_name, patient_gender)


  patient_gender = lcase(left(patient_gender,1))

  text = ReplaceStr(text, "<NAME>", patient_name, 0)
  text = ReplaceStr(text, "{name}", patient_name, 0)
  text = ReplaceStr(text, "{NAME}", patient_name, 0)

  if patient_gender = "m" then
    text = ReplaceStr(text, "<S/HE>", "He", 0)
    text = ReplaceStr(text, "$he/she", "he", 0)
    text = ReplaceStr(text, "$He/She", "He", 0)
    text = ReplaceStr(text, "$his/her", "his", 0)
    text = ReplaceStr(text, "$His/Her", "His", 0)

    text = ReplaceStr(text, "{he/she}", "he", 0)
    text = ReplaceStr(text, "{He/She}", "He", 0)
    text = ReplaceStr(text, "{his/her}", "his", 0)
    text = ReplaceStr(text, "{His/Her}", "His", 0)
  elseif patient_gender = "f" then
    text = ReplaceStr(text, "<S/HE>", "She", 0)
    text = ReplaceStr(text, "$he/she", "she", 0)
    text = ReplaceStr(text, "$He/She", "She", 0)
    text = ReplaceStr(text, "$his/her", "her", 0)
    text = ReplaceStr(text, "$His/Her", "Her", 0)

    text = ReplaceStr(text, "{he/she}", "she", 0)
    text = ReplaceStr(text, "{He/She}", "She", 0)
    text = ReplaceStr(text, "{his/her}", "her", 0)
    text = ReplaceStr(text, "{His/Her}", "Her", 0)
  else
    text = ReplaceStr(text, "<S/HE>", "Patient", 0)
    text = ReplaceStr(text, "$he/she", "patient", 0)
    text = ReplaceStr(text, "$He/She", "Patient", 0)
    text = ReplaceStr(text, "$his/her", "patient", 0)
    text = ReplaceStr(text, "$His/Her", "Patient", 0)

    text = ReplaceStr(text, "{he/she}", "patient", 0)
    text = ReplaceStr(text, "{He/She}", "Patient", 0)
    text = ReplaceStr(text, "{his/her}", "patient", 0)
    text = ReplaceStr(text, "{His/Her}", "Patient", 0)
  end if

  text = ReplaceStr(text, "  ", " ", 0)
  text = ReplaceStr(text, "  ", " ", 0)
  text = ReplaceStr(text, " , ", ", ", 0)
  text = ReplaceStr(text, ";;", ";", 0)
  text = ReplaceStr(text, "..", ".", 0)
  text = ReplaceStr(text, "..", ".", 0)
  text = ReplaceStr(text, " ; ", "; ", 0)
  text = ReplaceStr(text, " ;", "; ", 0)
  text = ReplaceStr(text, "; ;", ";", 0)
  text = ReplaceStr(text, " .", ".", 0)
  text = ReplaceStr(text, " )", ")", 0)

  substitute_text = text

end function

function fix_text(byval text)

  dim new_text
  new_text = text
  continue = true

  do
    if left(new_text,2) = vbCrLf then
      new_text = right(new_text, len(new_text) - 2)
    else
      continue = false
    end if
  loop while continue = true

  new_text =Replace(new_text,"<br />", "")
  new_text = ReplaceStr(new_text, vbCrLf, "<br>" , 0)
  'new_text = ReplaceStr(new_text, vbCrLf, vbCrLf, 0)
  new_text = Replace(new_text, "<br> <br>", "<br>")
  fix_text = new_text
 

end function

designtype= GetTemplateDesignbyUserID(session("user"))
if designtype <> "" then
	designtype = cint(designtype)
else
	designtype = 0
end if

function setPopupHtml(template_id)
      dim strpopup
      strpopup=""""
      if (designtype=2) Then
          strpopup=";editlinkClick('" & visit_key &"', '"& template_id & "','" &  session("pid") & "','"+session("user")& "' );"""
      end if 
  setPopupHtml=strpopup   
end function

function setTemplateHtml()
dim str


setTemplateHtml= str
end function
gotoanchor = Request.QueryString("goto")

if Request.Form("type") <> "" then
%>
<!--#include file="../../notes/add/include/assessment_plan_delete.asp"-->
<%
end if
%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="../themes/cupertino/jquery.ui.all.css" />
<script src="../scripts/jquery-1.9.1.js"></script>
<script src="../scripts/jquery-ui-1.10.3.custom.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<!-- New Highlight.js file which is sourche Files --Starts Here
<script type='text/javascript' src='jquery-1.9.1.js'></script>
<script src="jquery.highlight-4.js" type="text/javascript"></script>
<!-- CSS Property for Highlight -- Stats Here
  <style type='text/css'>
	.highlight {
	background-color: #fff34d;
	-moz-border-radius: 5px; /* FF1+ */
	-webkit-border-radius: 5px; /* Saf3-4 */
	border-radius: 5px; /* Opera 10.5, IE 9, Saf5, Chrome */
	-moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* FF3.5+ */
	-webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Saf3.0+, Chrome */
	box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Opera 10.5+, IE 9.0 */
}

.highlight {
	padding:1px 4px;
	margin:0 -4px;
}
  </style>

Hightlight End-->
<%=Application("Styles")%>

<script type ="text/javascript">
function ActionConfirm(ActionType,Code,Type,Previous_Visit_Key)
{
  if ( ActionType == 'Delete' )
  {
	if (confirm("Are you sure you want to delete this code?"))
	{
		document.frm.type.value = Type;
		document.frm.code.value = Code;
		document.frm.actiontype.value = ActionType;
		document.frm.submit();

	}
  }
  else
  {
  		document.frm.type.value = Type;
		document.frm.code.value = Code;
		document.frm.actiontype.value = ActionType;
		document.frm.previous_visit_key.value = Previous_Visit_Key;
		document.frm.submit();
  }
}

function delete_cognitive(a)
{

    if (confirm('Are you sure you want to this status?'))
    {
        window.open('delete_cognitive.asp?id='+a,'a125','width=100,height=100');

    }
}


function showData() {
<% if gotoanchor <> "" then %>
  document.location.hash = '#<%=gotoanchor%>';
<% end if %>
}

function gotoURL(URL,thisone)
{
	visit_key = thisone.options[thisone.selectedIndex].value;
	full_URL = URL.concat('?visit_key=',visit_key);
	parent.location = '/practice/notes/display/default.asp?visit_key='+visit_key;
}

function delete_file(a)
{
if (confirm('Are you sure you want to delete this file?'))
	{
		window.open('delete_file.asp?file_id='+a,'a123','width=100,height=100');

	}
}

function delete_ss(a)
{
if (confirm('Are you sure you want to delete this file?'))
	{
		window.open('delete_ssfile.asp?id_no='+a,'a123','width=100,height=100');

	}
}

function delete_excuse(a)
{
if (confirm('Are you sure you want to delete this excuse letter?'))
	{
		window.open('ex_form_delete.asp?id_no='+a,'a123','width=100,height=100');

	}
}

function delete_echo(a,b,c)
{

if (confirm('Are you sure you want to delete this report?'))
	{
		window.open('echotest_delete.asp?visit_key='+a+'&patient_id='+b+'&test_type='+c,'a123','width=100,height=100');

	}
}


function delete_opth(a)
{

if (confirm('Are you sure you want to delete this prescription?'))
	{
		window.open('opth_delete.asp?pres_id='+a,'a123','width=100,height=100');

	}
}

function delete_opth_sd(a)
{

if (confirm('Are you sure you want to delete this examination?'))
	{
		window.open('opth_sd_delete.asp?pres_id='+a,'a123','width=100,height=100');

	}
}

function delete_opth_ee(a)
{

if (confirm('Are you sure you want to delete this examination?'))
	{
		window.open('opth_ee_delete.asp?exam_id='+a,'a125','width=100,height=100');

	}
}

function delete_lc_order(a)
{

if (confirm('Are you sure you want to delete this Incomplete Labcorp Order(s)?'))
	{
		window.open('labcorp_order_delete.asp?order_id='+a,'a129','width=100,height=100');

	}
}
    
function delete_patexam(a)
{

if (confirm('Are you sure you want to delete the Exam?'))
	{
		window.open('patexam_delete.asp?id_no='+a ,'a92','width=100,height=100');

	}
}

function delete_paperlab(a)
{

    if (confirm('Are you sure you want to delete the labs ordered on paper?'))
    {
        window.open('paperlab_delete.asp?id_no='+a ,'a91','width=100,height=100');

    }
}

function delete_mlab(a)
{

    if (confirm('Are you sure you want to delete the lab results?'))
    {
        window.open('delete_mlab.asp?id_no='+a ,'a90','width=100,height=100');

    }
}


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function editlinkClick(visit_key,template_id,facility_id,user_id) {
       
         $("#dialog").dialog({
          
        autoOpen: true,
        modal: true,
        height: 645,
        draggable: true,
        position: 'top',
        closeOnEscape: true,
        width: 515,
        buttons: {
        Done : function() {parent.leftNoteFrameTop.toggleTop();
          $( this ).dialog( "close" );
           
        }},
        open: function (ev, ui) { 
            $("#template_frame").attr('src', '/practice/notes/Templates/Template_popup.aspx?visit_key=' + visit_key+ '&template_id='+ template_id  +'&facility_id='+ facility_id +'&user_id='+ user_id );
           $('#dialog').css('overflow', 'hidden');
                    },
        close: function() {
        parent.leftNoteFrameTop.toggleTop();
       $("#dialog").dialog('destroy');
      }              
                    
       
          });
       
       
        
       
    }
    
  
   

function delete_smoke(a)
{

    if (confirm('Are you sure you want to delete Smaking Status?'))
    {
        window.open('smoke_delete.asp?visit_key='+a ,'s9','width=100,height=100');

    }
}


</script>

</head>

<body onload="showData();" width="100%">

<div ID="overDiv" STYLE="position:absolute; visibility:hide;"></div>
<script LANGUAGE="JavaScript" SRC="/library/overlib.js"></script>
<%

' Get patient and visit information for visit_key

visit_key = Request("visit_key")

dim patientObj
set patientObj = new clsPatientNotes
patientObj.SetVisitKey(visit_key)
patient_id = patientObj.ID
patient_gender = patientObj.Gender
patient_name = patientObj.Name
note_closed = patientObj.Closed

if note_closed = true then
	nc_status=1
else
	nc_status=0
end if

if visit_key <> "" then

set RS = Server.CreateObject("ADODB.Recordset")
set RSNoteData = Server.CreateObject("ADODB.Recordset")

if session("role") <> "physician" AND session("role") <> "nurse" then
	ReadOnly = True
else
	ReadOnly = False
end if

sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+) AND A.VISIT_KEY='" & visit_key & "' ORDER BY CUSTOM_NOTE ASC"
RSNoteData.CursorLocation = 3
RSNoteData.Open sqlQuery, CONN

'Response.Write sqlQuery

dim RSPastMedicalHx
set RSPastMedicalHx = server.CreateObject("ADODB.RecordSet")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type, d.visit_date FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B, VISIT C, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')D WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+)  AND C.VISIT_KEY = A.VISIT_KEY and C.visit_date <= D.visit_date and A.template_type = '7' and C.patient_id = '"&patient_id&"' ORDER BY C.VISIT_DATE DESC"
RSPastMedicalHx.CursorLocation = 3
RSPastMedicalHx.Open sqlQuery, CONN

dim RSSocialHx
set RSSocialHx = server.CreateObject("ADODB.RecordSet")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type, d.visit_date FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B, VISIT C, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')D WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+)  AND C.VISIT_KEY = A.VISIT_KEY and C.visit_date <= D.visit_date and A.template_type = '14' and C.patient_id = '"&patient_id&"' ORDER BY C.VISIT_DATE DESC"
RSSocialHx.CursorLocation = 3
RSSocialHx.Open sqlQuery, CONN

dim RSSurgicalHx
set RSSurgicalHx = server.CreateObject("ADODB.RecordSet")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type, d.visit_date FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B, VISIT C, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')D WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+)  AND C.VISIT_KEY = A.VISIT_KEY and C.visit_date <= D.visit_date and A.template_type = '12' and C.patient_id = '"&patient_id&"' ORDER BY C.VISIT_DATE DESC"
RSSurgicalHx.CursorLocation = 3
RSSurgicalHx.Open sqlQuery, CONN

dim RSFamilyHx
set RSFamilyHx = server.CreateObject("ADODB.RecordSet")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type, d.visit_date FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B, VISIT C, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')D WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+)  AND C.VISIT_KEY = A.VISIT_KEY and C.visit_date <= D.visit_date and A.template_type = '13' and C.patient_id = '"&patient_id&"' ORDER BY C.VISIT_DATE DESC"
RSFamilyHx.CursorLocation = 3
RSFamilyHx.Open sqlQuery, CONN

Dim RSFileData
Set RSFileData = server.CreateObject("ADODB.RecordSet")
sqlQuery = "Select * from Notes_Files_Ref where visit_key = '"&visit_key&"'"
RSFileData.CursorLocation = 3
RSFileData.Open sqlQuery, CONN

Dim RSNoteAmend
Set RSNoteAmend = server.CreateObject("ADODB.RecordSet")
sqlQuery = "Select na.*, dt.first_name  || ' ' ||   dt.last_name || ' ' || dt.credential  as name from NOTES_AMEND na, DOCTORTABLE dt where  NA.DOC_ID= DT.DOCUSER and  visit_key ='"&visit_key&"' order by 2 "
RSNoteAmend.CursorLocation = 3
RSNoteAmend.Open sqlQuery, CONN

Dim RSPatExams
Set RSPatExams = server.CreateObject("ADODB.RecordSet")
sqlQuery = "Select * from PATIENT_EXAMS where visit_key = '"&visit_key&"' order by EXAM_CATEGORY"
RSPatExams.CursorLocation = 3
RSPatExams.Open sqlQuery, CONN

Dim RSUploadData
set RSUploadData = server.CreateObject("ADODB.RecordSet")
'sqlQuery = "Select * from Notes_Upload_Ref where visit_key = '"&visit_key&"'"
'sqlQuery = "select DISTINCT a.* from Notes_Upload_Ref a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c where a.visit_key = b.visit_key and b.visit_date <= c.visit_date and b.patient_id='" & patient_id & "' order by folder_name"
sqlQuery = "select DISTINCT * from Notes_Upload_Ref where visit_key  in (select distinct visit_key from visit where patient_id ='"& patient_id &"') order by folder_name"
'Response.Write sqlQuery
RSUploadData.CursorLocation = 3
RSUploadData.Open sqlQuery, CONN

Dim RSNotes
set RSNotes = server.CreateObject("ADODB.RecordSet")
'strSQL = "select * from EVENTS where patient_id = '"& patient_id &"' and status<>'C' and FACILITY_ID='"& session("pid") &"' "  
strSQL = "select * from EVENTS where patient_id = '"& patient_id &"' and FACILITY_ID='"& session("pid") &"' order by submit_date desc"
RSNotes.CursorLocation = 3
RSNotes.Open strSQL, CONN

Dim RSLabcorp
Set RSLabcorp = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,LABCORP_TEST_NAME,LABCORP_TEST_NUMBER,date_ordered,last_updated,transaction_id,status,p.Order_id as PDFOrderID from LABCORP_ORDER_DATA a, LABCORP_ORDERS b,PDFTABLE p where a.order_id=b.order_id and lab_name ='LABCORP' and visit_key = '"&visit_key&"' and a.Order_id=p.ORDER_ID(+)"
RSLabcorp.CursorLocation = 3
RSLabcorp.Open sqlQuery, CONN

Dim RSLabquestOrders
Set RSLabquestOrders = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,LABCORP_TEST_NAME,LABCORP_TEST_NUMBER,date_ordered,last_updated,transaction_id,status from LABCORP_ORDER_DATA a, LABCORP_ORDERS b where a.order_id=b.order_id and lab_name is null and b.facility_id is not null  and visit_key = '"&visit_key&"'"
RSLabquestOrders.CursorLocation = 3
RSLabquestOrders.Open sqlQuery, CONN


Dim RSLabCorpOrders
Set RSLabCorpOrders = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,LABCORP_TEST_NAME,LABCORP_TEST_NUMBER,date_ordered,last_updated,transaction_id,status from LABCORP_ORDER_DATA a, LABCORP_ORDERS b where a.order_id=b.order_id and lab_name ='LABCORP' and  visit_key = '"&visit_key&"' order by order_id"
RSLabCorpOrders.CursorLocation = 3
RSLabCorpOrders.Open sqlQuery, CONN


Dim RSLabquest
Set RSLabquest = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,LABCORP_TEST_NAME,LABCORP_TEST_NUMBER,date_ordered,last_updated,transaction_id,status from LABCORP_ORDER_DATA a, LABCORP_ORDERS b where a.order_id=b.order_id and a.status='Ready' and b.facility_id is not null and b.patient_id = '"& patient_id &"'"
RSLabquest.CursorLocation = 3
RSLabquest.Open sqlQuery, CONN
'Response.Write sqlQuery

Dim RSLabManual
Set RSLabManual = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,LABCORP_TEST_NAME,LABCORP_TEST_NUMBER,date_ordered,last_updated,transaction_id,status from LABCORP_ORDER_DATA a, LABCORP_ORDERS b where a.order_id=b.order_id and a.status='Ready' and b.facility_id is null and b.patient_id = '"& patient_id &"'"
RSLabManual.CursorLocation = 3
RSLabManual.Open sqlQuery, CONN
'Response.Write sqlQuery

Dim RSLabImported
Set RSLabImported = server.CreateObject("ADODB.RecordSet")
sqlQuery = "Select master_id, is_pdf_included,created_date,is_comment_saved from LABRESULTS_MASTER A where IS_ASSOCIATED=1 and mrnid ='"& patient_id &"' and FACILITYID='"& session("pid") &"' order by 1 "
RSLabImported.CursorLocation = 3
RSLabImported.Open sqlQuery, CONN
'Response.Write sqlQuery

Dim RSMillenniumOrders
Set RSMillenniumOrders = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,TEST_NAME,TEST_NUMBER,date_ordered,last_updated,transaction_id,status from MILLENNIUM_ORDER_DATA a, MILLENNIUM_ORDERS b where a.order_id=b.order_id and (a.DELETEDFLAG IS NULL OR a.DELETEDFLAG <> 1) and visit_key = '"&visit_key&"'"
RSMillenniumOrders.CursorLocation = 3
RSMillenniumOrders.Open sqlQuery, CONN

Dim RSMillenniumResults
Set RSMillenniumResults = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,TEST_NAME,TEST_NUMBER,date_ordered,last_updated,transaction_id,status from MILLENNIUM_ORDER_DATA a, MILLENNIUM_ORDERS b where a.order_id=b.order_id and (a.DELETEDFLAG IS NULL OR a.DELETEDFLAG <> 1)and a.STATUS = 'Ready' and visit_key = '"&visit_key&"'"
RSMillenniumResults.CursorLocation = 3
RSMillenniumResults.Open sqlQuery, CONN

Dim RSCPLSEOrders
Set RSCPLSEOrders = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,TEST_NAME,TEST_NUMBER,date_ordered,last_updated,transaction_id,status from CPLSE_ORDER_DATA a, CPLSE_ORDERS b where a.order_id=b.order_id and (a.DELETEDFLAG IS NULL OR a.DELETEDFLAG <> 1) and a.status in ('Sent') and visit_key = '"&visit_key&"'  order by 1 desc"
RSCPLSEOrders.CursorLocation = 3
RSCPLSEOrders.Open sqlQuery, CONN

Dim RSProPathOrders
Set RSProPathOrders = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,TEST_NAME,TEST_NUMBER,date_ordered,last_updated,transaction_id,status from PROPATH_ORDER_DATA a, PROPATH_ORDERS b where a.order_id=b.order_id and (a.DELETEDFLAG IS NULL OR a.DELETEDFLAG <> 1) and a.status in ('Sent') and visit_key = '"&visit_key&"'"
RSProPathOrders.CursorLocation = 3
RSProPathOrders.Open sqlQuery, CONN

Dim RSMiracaOrders
Set RSMiracaOrders = server.CreateObject("ADODB.RecordSet")
sqlquery = "select a.order_id,test_name,test_number,date_ordered,last_updated,transaction_id,status from miraca_order_data a,miraca_orders b where a.order_id=b.order_id and (a.deletedflag is null or a.deletedflag <> 1) and a.status in ('Sent') and visit_key = '"&visit_key&"' order by 1"
RSMiracaOrders.CursorLocation = 3
RSMiracaOrders.Open sqlQuery, CONN

Dim RSMiracaResults
Set RSMiracaResults = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select distinct  a.ORDER_ID,TEST_NAME,TEST_NUMBER,date_ordered,last_updated,transaction_id,status, b.is_pdf_included, a.IS_UPDATED,a.UPDATE_VERSION, TO_CHAR(a.CREATED_DATE, 'MM/DD/YYYY HH:MI:SS AM') as REC_DATE from MIRACA_ORDER_DATA a, MIRACA_ORDERS b where a.order_id=b.order_id and (a.DELETEDFLAG IS NULL OR a.DELETEDFLAG <> 1) and a.STATUS = 'Ready' and b.patient_id ='"&patient_id&"' and date_ordered is not null  order by a.order_id,a.update_version"
RSMiracaResults.CursorLocation = 3
RSMiracaResults.Open sqlQuery, CONN

Dim RSCPLSEResults
Set RSCPLSEResults = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,TEST_NAME,TEST_NUMBER,date_ordered,last_updated,transaction_id,status from CPLSe_ORDER_DATA a, CPLSE_ORDERS b where a.order_id=b.order_id and (a.DELETEDFLAG IS NULL OR a.DELETEDFLAG <> 1)and a.STATUS = 'Ready' and patient_id = '"&patient_id&"'"
RSCPLSEResults.CursorLocation = 3
RSCPLSEResults.Open sqlQuery, CONN

Dim RSProPathResults
Set RSProPathResults = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select distinct a.ORDER_ID,TEST_NAME,TEST_NUMBER,date_ordered,last_updated,transaction_id,status from PROPATH_ORDER_DATA a, PROPATH_ORDERS b where a.order_id=b.order_id and (a.DELETEDFLAG IS NULL OR a.DELETEDFLAG <> 1) and a.STATUS = 'Ready' and b.patient_id = '"&patient_id&"'"
RSProPathResults.CursorLocation = 3
RSProPathResults.Open sqlQuery, CONN


Dim RSNorthShoreOrders
Set RSNorthShoreOrders = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,a.ORDER_DATA_ID,TEST_DESC,ORDER_CODE,date_ordered,last_updated,transaction_id,status,a.sendingfacility_id from NORTHSHORE_ORDER_DATA a, NORTHSHORE_ORDERS b where a.order_id=b.order_id and status = 'Sent'and b.visit_key = '"&visit_key&"' order by last_updated desc"
RSNorthShoreOrders.CursorLocation = 3
RSNorthShoreOrders.Open sqlQuery, CONN

Dim RSNorthShoreResults
Set RSNorthShoreResults = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select a.ORDER_ID,a.ORDER_DATA_ID,TEST_DESC,ORDER_CODE,date_ordered,last_updated,transaction_id,status,a.sendingfacility_id from NORTHSHORE_ORDER_DATA a, NORTHSHORE_ORDERS b where a.order_id=b.order_id and status = 'Ready'and b.visit_key = '"&visit_key&"' order by last_updated desc"
RSNorthShoreResults.CursorLocation = 3
RSNorthShoreResults.Open sqlQuery, CONN

Dim RSpaper
Set RSpaper = server.CreateObject("ADODB.RecordSet")
sqlQuery = "select * from paper_labs where  patient_id = '"& patient_id &"' order by order_id"
RSpaper.CursorLocation = 3
RSpaper.Open sqlQuery, CONN


Dim RSDiagTests
set RSDiagTests = server.CreateObject("ADODB.RecordSet")
strSQL = "select STUDY_DATE,visit_key,test_type from RTE_ECHO where patient_id = '"& patient_id &"' and to_date(STUDY_DATE,'mm/dd/yyyy') <= (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')"
RSDiagTests.CursorLocation = 3
RSDiagTests.Open strSQL, CONN

Dim RSDiagTST
set RSDiagTST = server.CreateObject("ADODB.RecordSet")
strSQL = "select STUDY_DATE,visit_key,id_no from ECHO_TST where patient_id = '"& patient_id &"' and to_date(STUDY_DATE,'mm/dd/yyyy') <= (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')"
RSDiagTST.CursorLocation = 3
RSDiagTST.Open strSQL, CONN

Dim RsNU
set RsNU = server.CreateObject("ADODB.RecordSet")
strSQL = "select distinct id_no, test_type from NEURO_MASTER where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"'"
RsNU.CursorLocation = 3
RsNU.Open strSQL, CONN

Dim RSssForm
set RSssForm = server.CreateObject("ADODB.RecordSet")
strSQL = "select id_no, visit_date from NOTES_SSFORM where form_type='SS' and patient_id = '"& patient_id &"' "
RSssForm.CursorLocation = 3
RSssForm.Open strSQL, CONN

Dim RSAEForm
set RSAEForm = server.CreateObject("ADODB.RecordSet")
strSQL = "select id_no, visit_date from NOTES_SSFORM where form_type='AE' and patient_id = '"& patient_id &"' "
RSAEForm.CursorLocation = 3
RSAEForm.Open strSQL, CONN

Dim RSExForm
set RSExForm = server.CreateObject("ADODB.RecordSet")
strSQL = "select id_no, visit_date from NOTES_ExFORM where type='Excuse Letter' and patient_id = '"& patient_id &"' "
RSExForm.CursorLocation = 3
RSExForm.Open strSQL, CONN

Dim RSRefForm
set RSRefForm = server.CreateObject("ADODB.RecordSet")
strSQL = "select id_no, visit_date,ex_type from NOTES_ExFORM where type = 'Referral Letter' and patient_id = '"& patient_id &"' "
RSRefForm.CursorLocation = 3
RSRefForm.Open strSQL, CONN


Dim RSPain
set RSPain = server.CreateObject("ADODB.RecordSet")
strSQL = "select pain_m  from visit where patient_id  =  '"& patient_id &"' and  pain_m is not null order by visit_date desc"
RSPain.CursorLocation = 3
RSPain.Open strSQL, CONN

Set PatientDataSet = GetPatientData(Request("Visit_Key"))
Set DoctorDataSet = GetDoctorData(Request("Visit_Key"))
DOCTOR_NAME = DoctorDataSet("Last_Name")&", "&DoctorDataSet("First_Name")
DoctorDataSet.Close
Set DoctorDataSet = nothing

if Response.IsClientConnected = false then Response.End

%>
<%
	sum_VisitKey = Request("visit_key")
	set sum_adoDb = server.CreateObject("ADODB.Recordset")
	sum_sqlQuery = "SELECT * FROM CDS_SETTING"
	sum_adoDb.open sum_sqlQuery,con

	sum_countEvidence = 0
	if not sum_adoDb.eof then
		while not sum_adoDb.eof
			if (sum_adoDb("CDS_ENABLE") = "Y" And UCase(session("role")) = "PHYSICIAN") Or (sum_adoDb("CDS_ENABLE_NURSE") = "Y" And UCase(session("role")) = "NURSE") then
				set sum_adoDb2 = server.CreateObject("ADODB.Recordset")
				sum_cdsQuery = Replace (sum_adoDb("CDS_QUERY"), "{0}", visit_key )
				sum_adoDb2.open sum_cdsQuery,con
				if not sum_adoDb2.eof then
					if sum_adoDb2("CDS_SHOW") = "TRUE" then
						sum_countEvidence = sum_countEvidence + 1
					end if
				end if
			end if
			sum_adoDb.movenext
		wend
	end if

	set sum_adoDbRef = server.CreateObject("ADODB.Recordset")
	sum_sqlRefQuery = "SELECT * FROM CDS_SETTING_REF"
	sum_adoDbRef.open sum_sqlRefQuery,con

	sum_countReference = 0
	if not sum_adoDbRef.eof then
		while not sum_adoDbRef.eof
			if (sum_adoDbRef("CDS_ENABLE") = "Y" And UCase(session("role")) = "PHYSICIAN") Or (sum_adoDbRef("CDS_ENABLE_NURSE") = "Y" And UCase(session("role")) = "NURSE") then
				set sum_adoDbRef2 = server.CreateObject("ADODB.Recordset")
				sum_cdsRefQuery = Replace (sum_adoDbRef("CDS_QUERY"), "{0}", sum_VisitKey )
				sum_adoDbRef2.open sum_cdsRefQuery,con
				if not sum_adoDbRef2.eof then
					sum_countReference = sum_countReference + 1
				end if
			end if
			sum_adoDbRef.movenext
		wend
	end if
%>    
<div style="float: right;">
	<a href="/practice/notes/display/getDDX.asp?visit_key=<%=sum_VisitKey %>">
		There are <%=sum_countEvidence %> alert(s) and <%=sum_countReference %> reference resource(s)
	</a>
</div>
<script type="text/javascript" language="JavaScript" src="find5.js"> </script>

<div class="titleBox_HL">VISIT NOTE</div>
<!-- Search Highlight Texbox and Javascript Starts Here
<div class="searchBox">Search<input type="text" id="text-search" value="Search Content" onfocus="this.value=''"></div>
<script type="text/javascript">
	$(function () {
		$('#text-search').bind('keyup change', function (ev) {
			// pull in the new value
			var searchTerm = $(this).val();

			// remove any old highlighted terms
			$('body').removeHighlight();

			// disable highlighting if empty
			if (searchTerm) {
				// highlight the new term
				$('body').highlight(searchTerm);
			}
		});
	});
</script>
 Search Highlight Texbox and Javascript End Here-->
<div id="left" style="float: left; margin-bottom: 3em; width: 49.5%;">
<div class="titleBox">SUBJECTIVE</div>
<div id="dialog" style="display:none;height:610px; width:500px; overflow:hidden;"  title= "VISIT NOTE">
     
    <iframe id="template_frame" src="" style="height:600px; width:500px; overflow:hidden; border:none" frameBorder="0"  scrolling="no" ></iframe>
</div>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="cc" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/cc.asp?visit_key=<%=visit_key%>')"><% end if %><b>CC:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%

'apptrequest, confappt

sqlQuery = "SELECT reason_visit,cl_ins,doctor_id,MED_REVIEW_WHO, MED_REVIEW_DATE FROM visit WHERE visit_key='" & visit_key & "'"

set RSCC = CONN.Execute(sqlQuery)

if not RSCC.EOF then
  doctor_id= RSCC("doctor_id").value
  reason_visit = RSCC("reason_visit").value
  MED_REVIEW_WHO = RSCC("MED_REVIEW_WHO").value
  MED_REVIEW_DATE = RSCC("MED_REVIEW_DATE").value
  cl_ins  = RSCC("cl_ins").value  
  if reason_visit = "" then reason_visit = "No chief complaint noted."
else
  reason_visit = "No chief complaint noted."
end if

set RSCC = nothing

%>
<p>
<b><%=reason_visit%></b>
<p>


<%'--HPI - Previous visit
If session("pid")="VIR1232" then '---only for Account

set RShpi = Server.CreateObject("ADODB.Recordset")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+) AND A.template_type=2 and A.VISIT_KEY =(select visit_key from (select visit_key from visit where visit_date < (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') and patient_id = '"& patient_id &"' order by visit_date desc ) where rownum <=1 )"
'Response.Write sqlQuery
RShpi.Open sqlQuery, CONN
If not (RShpi.eof or RShpi.bof) then
%>
<p>
<b><u>HPI:(Previous Visit)</u></b><BR>
<%
while not RShpi.EOF
  if RShpi("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RShpi("sentence_text"))    
    Response.Write "<p>"
  else ' not free text
    if RShpi("description_text") <> "" and RShpi("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RShpi("description_text"), patient_name, patient_gender) & "</B></P>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RShpi("template_id") & "'>" & fix_text(RShpi("sentence_text")) & "</div>"
    Response.Write "<p>"
  end if
  RShpi.MoveNext
wend
end if	
end if '--end of previous HPI
%>


<%'--HPI - current visit
RSNoteData.Filter = "template_type = '2'"
RSFileData.Filter = "template_type = '2'"

if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h2" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=HPI')"><% end if %><b>HPI:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if Response.IsClientConnected = false then Response.End
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSNoteData("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
      '/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
      '/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "
    end if
    Response.Write "<p>"
  end if
  RSNoteData.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if
%>


<%'--smoking status  
'sqlQuery = "SELECT smoke,smoke_start,smoke_end, visit_date FROM visit WHERE smoke is not null and patient_id ='" & patient_id & "' and visit_key= '" & visit_key & "'"
'sqlQuery = "select  v.smoke,v.smoke_start,v.smoke_end, v.visit_date,s.code_system_name, s.CONCEPT_NAME_CODE from visit v inner join smoking_status s on V.SMOKE = S.SMOKEID where v.visit_key = '" & visit_key & "'"
sqlQuery = "select  v.smoke,v.smoke_start,v.smoke_end, v.visit_date,s.code_system_name, s.CONCEPT_NAME_CODE from visit v inner join smoking_status s on V.SMOKE = S.SMOKEID where patient_id ='" & patient_id & "'"


set RSCC = CONN.Execute(sqlQuery)
If not(RSCC.eof or RSCC.bof) then
%>
<%
if note_closed = false and ReadOnly = false then %><a name="smoke" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/smoke.asp?visit_key=<%=visit_key%>')"><% end if %><b>Smoking Status:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<p>
<%
	if not RSCC.EOF then
		while not RSCC.EOF
			smoke= RSCC("smoke")
			smoke_start= RSCC("smoke_start")
			smoke_end =  RSCC("smoke_end")
			code_system_name =  RSCC("code_system_name")
			concept_name_code =  RSCC("concept_name_code")
			If smoke="1" then
				smoke="Current every day smoker"
			elseif smoke="2" then
				smoke="Current some day smoker"
			elseif smoke="3" then
				smoke="Former smoker"
			elseif smoke="4" then
				smoke="Never smoker"
			elseif smoke="5" then
				smoke="Smoker, current status unknown"
			elseif smoke="9" then
				smoke="Unknown if ever smoked"
			elseif smoke="7" then
				smoke="Heavy tobacco smoker"
		   elseif smoke="8" then
				smoke="Light tobacco smoker"
			else
				smoke=smoke
			end if
		  Response.Write smoke&", [SNOMED-CT: " & concept_name_code  & "]"
		  if isnull(smoke_start) and isnull(smoke_end) then response.Write "<br>"
		  if smoke_start <>"" then response.Write ", Start:"& smoke_start
		  if smoke_end <>"" then response.Write ", End:"& smoke_end&"<br>"
		  if smoke_start <> "" and isnull(smoke_end) then response.Write "<br>"
		  RSCC.movenext
		wend
	end if
	set RSCC = nothing
end if
%>

<%
RSFileData.Filter = "template_type = '7'"
if RSPastMedicalHx.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h7" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=PMH')"><% end if %><b>Past Medical Hx:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSPastMedicalHx.RecordCount > 0 then
RSPastMedicalHx.MoveFirst
while not RSPastMedicalHx.EOF
  if Response.IsClientConnected = false then Response.End
  if RSPastMedicalHx("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSPastMedicalHx("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSPastMedicalHx("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSPastMedicalHx("soap_id") & "&template_type=" & RSPastMedicalHx("template_type") & "')" & setPopupHtml( RSPastMedicalHx("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSPastMedicalHx("description_text") <> "" and RSPastMedicalHx("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSPastMedicalHx("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSPastMedicalHx("template_id") & "'>" & fix_text(RSPastMedicalHx("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSPastMedicalHx("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSPastMedicalHx("template_id") & "&template_type=" & RSPastMedicalHx("template_type") & "')" & setPopupHtml( RSPastMedicalHx("template_id")) &">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSPastMedicalHx.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if


RSFileData.Filter = "template_type = '13'"

if RSFamilyHx.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h13" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=FH')"><% end if %><b>Family Hx:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSFamilyHx.RecordCount > 0 then
RSFamilyHx.MoveFirst
while not RSFamilyHx.EOF
  if Response.IsClientConnected = false then Response.End
  if RSFamilyHx("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSFamilyHx("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSFamilyHx("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSFamilyHx("soap_id") & "&template_type=" & RSFamilyHx("template_type") & "')" & setPopupHtml( RSFamilyHx("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSFamilyHx("description_text") <> "" and RSFamilyHx("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSFamilyHx("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSFamilyHx("template_id") & "'>" & fix_text(RSFamilyHx("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSFamilyHx("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSFamilyHx("template_id") & "&template_type=" & RSFamilyHx("template_type") & "')" & setPopupHtml( RSFamilyHx("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSFamilyHx.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if


	'''''''''' Problems Start ''''''''''''''''
set RSProblem = Server.CreateObject("ADODB.Recordset")
RSProblem.CursorLocation = 3
sqlQuery = " select a.PROBLEM_ID,a.VISIT_KEY,a.DIAG_CODE,b.icdname as DIAG_DESC,a.STATUS,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_ONSET,'mm/dd/yyyy') as date_onset,to_char(a.DATE_RESOLVED,'mm/dd/yyyy') as date_resolved,a.NOTES,problem_source from notes_ProblemList a, icdtable b where a.DIAG_CODE(+) = b.icdcode AND  a.visit_key='"& visit_key &"' union all select a.PROBLEM_ID ,a.VISIT_KEY,a.DIAG_CODE,c.term as DIAG_DESC,a.STATUS,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_ONSET,'mm/dd/yyyy') as date_onset,to_char(a.DATE_RESOLVED,'mm/dd/yyyy') as date_resolved,a.NOTES,problem_source from notes_ProblemList a , (select CONCEPTID, MAX(TERM) as TERM from SNOMED_FOR_CCD GROUP BY CONCEPTID) c where a.DIAG_CODE (+) = c.conceptid AND a.visit_key='" & visit_key & "'"
RSProblem.Open sqlQuery, CONN, 3, 4

if not RSProblem.EOF then

%>
<p>
	<% if note_closed = false and ReadOnly = false then %><a name="problems" href=""><% end if %><b>Problems:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<br>
<%

while not RSProblem.EOF
  if Response.IsClientConnected = false then Response.End
  Response.Write "<b><span style='color: #FF0000'>" & RSProblem("diag_desc") & "</span></b>"
 
  if RSProblem("diag_code") <> "" then
	Response.Write ", [SNOMED-CT: " & RSProblem("diag_code") & "]"
  end if
	 if RSProblem("date_onset") <> "" then
	Response.Write ", " & RSProblem("date_onset")
  end if
	if RSProblem("status") <> "" then
	Response.Write ", " & RSProblem("status")
  end if
  Response.Write "<br>"
  RSProblem.MoveNext
wend

end if
RSProblem.Close
'''''''''' Problems End ''''''''''''''''




set RSAllergy = Server.CreateObject("ADODB.Recordset")
RSAllergy.CursorLocation = 3
'sqlQuery = "SELECT * FROM notes_allergy WHERE visit_key='" & visit_key & "'"
sqlQuery = "SELECT DISTINCT a.ALLERGY_ID,a.VISIT_KEY,a.RECORDED_BY,a.ALLERGY_TEXT,a.STATUS,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_ONSET,'mm/dd/yyyy') as date_onset,to_char(a.DATE_RESOLVED,'mm/dd/yyyy') as date_resolved,a.APPROVED_BY,a.NOTES FROM notes_allergy a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c WHERE a.visit_key = b.visit_key and b.visit_date <= c.visit_date and a.patient_id='" & patient_id & "'"
'Response.Write sqlQuery
RSAllergy.Open sqlQuery, CONN, 3, 4

if not RSAllergy.EOF then

%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="allergy" href="/practice/notes/display/allergy_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>"><% end if %><b>Allergies:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<br>
<%

while not RSAllergy.EOF
  if Response.IsClientConnected = false then Response.End
  Response.Write "<b><span style='color: #FF0000'>" & RSAllergy("allergy_text") & "</span></b>"
  if RSAllergy("date_onset") <> "" then
    Response.Write " - onset: " & RSAllergy("date_onset")
  end if
  if RSAllergy("date_resolved") <> "" then
    Response.Write " - resolved: " & RSAllergy("date_resolved")
  end if
  if RSAllergy("notes") <> "" then
    Response.Write " - Reaction: " & RSAllergy("notes")
  end if
  Response.Write "<br>"
  RSAllergy.MoveNext
wend

end if
RSAllergy.Close

 '---- allergies from other sources
sqlQuery = "SELECT DISTINCT a.ALLERGY_ID,a.VISIT_KEY,a.RECORDED_BY,a.ALLERGY_TEXT,a.STATUS,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_ONSET,'mm/dd/yyyy') as date_onset,to_char(a.DATE_RESOLVED,'mm/dd/yyyy') as date_resolved,a.APPROVED_BY,a.NOTES FROM notes_allergy a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c WHERE a.visit_key = b.visit_key and b.visit_date <= c.visit_date and allergy_source='Other' and a.patient_id='" & patient_id & "'"
'Response.Write sqlQuery
RSAllergy.Open sqlQuery, CONN, 3, 4

if not RSAllergy.EOF then

%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="allergy" href="/practice/notes/display/allergy_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>"><% end if %><b>Allergies from Other Source:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<br>
<%

while not RSAllergy.EOF
  if Response.IsClientConnected = false then Response.End
  Response.Write "<b><span style='color: #FF0000'>" & RSAllergy("allergy_text") & "</span></b>"
  if RSAllergy("date_onset") <> "" then
    Response.Write " - onset: " & RSAllergy("date_onset")
  end if
  if RSAllergy("date_resolved") <> "" then
    Response.Write " - resolved: " & RSAllergy("date_resolved")
  end if
  if RSAllergy("notes") <> "" then
    Response.Write " - Reaction: " & RSAllergy("notes")
  end if
  Response.Write "<br>"
  RSAllergy.MoveNext
wend

end if
RSAllergy.Close
    

set RSImm = Server.CreateObject("ADODB.Recordset")
RSImm.CursorLocation = 3
sqlQuery =" select distinct   ni.*, inv.MANUFACTURER,TO_CHAR(inv.date_expires, 'mm/dd/yyyy') as ExpirationDate  from  notes_immunization_inventory inv, doctortable dt ,notes_immunization ni, NOTES_IMMU_MVXCODES man, NOTES_IMMU_MVXCODES mvx     where MVX.MANUFACTURER_NAME = INV.MANUFACTURER and man.manufacturer_name =   INV.MANUFACTURER  and inv.lot_number = ni.LOT and inv.lot_type = ni.imm_type and inv.FACILITY_ID = dt.FACILITY  and  ni.visit_key ='" & visit_key & "'    union  select distinct  ni.*, '' as MANUFACTURER_1, '' as ExpirationDate from  doctortable dt ,notes_immunization ni   where  ni.visit_key ='" & visit_key & "'  and ni.imm_id not in ( select distinct  ni.imm_id from  notes_immunization_inventory inv, doctortable dt ,notes_immunization ni, NOTES_IMMU_MVXCODES man where man.manufacturer_name =  INV.MANUFACTURER  and  inv.lot_number = ni.LOT and inv.lot_type = ni.imm_type and inv.FACILITY_ID = dt.FACILITY  and  ni.visit_key ='" & visit_key & "')"

'Response.Write sqlQuery
'Response.End
RSImm.Open sqlQuery, CONN, 3, 4



VCount = RSImm.RecordCount

if VCount > 0 then

%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="imm" href="/practice/notes/display/immunization_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>"><% end if %><b>Immunizations:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<br />
<%

  while not RSImm.EOF
    if Response.IsClientConnected = false then Response.End  
       
       Response.Write "<a rel=""htmltooltip"" <b> " & RSImm("date_administered") & "</b> - " & RSImm("imm_type") & "</a><br/>"       
       Response.Write "<div class=""htmltooltip""><p>Vaccine Administered: " & RSImm("imm_type") &"</p> "
       Response.Write "<p>Administration Date: " & RSImm("DATE_ADMINISTERED") &"</p> "
       Response.Write "<p>Administration Time: " & RSImm("ADMINISTRATION_TIME") &"</p> "
       Response.Write "<p>Administered Amount: " & RSImm("VOLUME") &"</p> "
       Response.Write "<p>Substance Lot Number: " & RSImm("lot") &"</p> "
       Response.Write "<p>Substance Expiration Date: " & RSImm("ExpirationDate") &"</p> "
       Response.Write "<p>Substance Manufacturer Name: " & RSImm("MANUFACTURER") &"</p> "
       Response.Write "<p>Route: " & RSImm("ROUTE") &"</p> "
       Response.Write "<p>Administration Site: " & RSImm("SITE") &"</p> "
       Response.Write "</div>"  
             
       RSImm.MoveNext
  wend

end if
RSImm.Close
%>
<p>
<% if note_closed = false and ReadOnly = false then %>
<a name="meds" href="/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&show=current&other_meds=Current"><% end if %><b>Current Medications:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<br>
<%
set RSMeds = Server.CreateObject("ADODB.Recordset")
RSMeds.CursorLocation = 3
sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status in ('Current', 'O Reconciled') AND a.prescribed_date <= c.visit_date" &_
		   " UNION " &_
		   "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status='Stopped' AND a.prescribed_date <= c.visit_date and a.stop_date > c.visit_date "
'sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status='Current' UNION SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"&patient_id&"' AND visit_key = '"&visit_key&"') c WHERE   a.prescribed_date <= c.visit_date AND a.stop_date > c.visit_date AND a.patient_id='"&patient_id&"' AND a.status='Stopped'"
'sqlQuery = "select DISTINCT a.* from notes_medications a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c where a.visit_key = b.visit_key and b.visit_date <= c.visit_date and a.patient_id='" & patient_id & "' and a.status='Current'"
'Response.Write sqlQuery
RSMeds.Open sqlQuery, CONN, 3, 4

if RSMeds.RecordCount > 0 then
while not RSMeds.EOF
  if Response.IsClientConnected = false then Response.End
%>
<b><%=RSMeds("drug_name")%></b> - <%=RSMeds("formulation")%> (<%=RSMeds("dose")&"/"&RSMeds("frequency")%>)<br>
<%
  RSMeds.MoveNext
wend
else
	Response.Write "No Medication."
end if
RSMeds.Close

if MED_REVIEW_WHO<>"" then 
    Response.Write "Medication reviewed by:"&MED_REVIEW_WHO&" on " &MED_REVIEW_DATE

end if
%>

<%
'sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status='O Current' AND a.prescribed_date <= c.visit_date"
sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status in ('O Current','O Reconciled')" 
'Response.Write sqlQuery

RSMeds.Open sqlQuery, CONN, 3, 4
if RSMeds.RecordCount > 0 then
%>
<p>
<a name="meds" href="/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&show=current&other_meds=Other"><b><u>Meds from other sources:</u></b></a>
<br>
<%
while not RSMeds.EOF
%>
 <b><%=RSMeds("drug_name")%></b> - <%=RSMeds("formulation")%> (<%=RSMeds("dose")&"/"&RSMeds("frequency")%>)<br>
<%
RSMeds.MoveNext
wend
end if
RSMeds.Close
%>




<%
sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status='Stopped' AND a.prescribed_date <= c.visit_date and a.stop_date <= c.visit_date"
'sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.prescribed_date <= c.visit_date AND a.stop_date <= c.visit_date AND a.patient_id='" & patient_id & "' AND a.status<>'Current'"
RSMeds.Open sqlQuery, CONN, 3, 4
if RSMeds.RecordCount > 0 then

%>
<p>
<font color="red"><u><b>Past Medications:</b></u></font>
<br>
<%

while not RSMeds.EOF
  if Response.IsClientConnected = false then Response.End
%>
<font color="red"><b><%=RSMeds("drug_name")%></b> - <%=RSMeds("formulation")%> (<%=RSMeds("dose")&"/"&RSMeds("frequency")%>)</font><br>
<%
  RSMeds.MoveNext
wend

end if

'---opth prescriptions --start
set RSOpth = Server.CreateObject("ADODB.Recordset")
sqlQuery = "SELECT * FROM OPTH_PRESCRIPTIONS where visit_key = '"& visit_key &"'"
RSOpth.Open sqlQuery, CONN, 3, 4
if not (RSOpth.eof or RSOpth.bof) then

	%>
	<p>
	<u><b>Opth Prescriptions:</b></u>
	<br>
	<%while not RSOpth.EOF	%>
		<%If RSOpth("pres_type")="CL" then%>
				<a href="#" onclick="window.open('opth_printrx.asp?pres_id=<%=RSOpth("pres_id")%>','aaaa','width=700,height=500,menubar=0,scrollbars=1,resizable=0')">Contact Lens</a>;<%=RSOpth("PRES_RECORD_DATE")%>;<%=RSOpth("PRES_TS")%>
					- <a href="#" onclick="delete_opth('<%=RSOpth("pres_id")%>')"><font class="st" color=red>delete</font></a><br>

		<%elseIf RSOpth("pres_type")="EG" then%>
				<a href="#" onclick="window.open('opth_printrx.asp?pres_id=<%=RSOpth("pres_id")%>','aaaa','width=700,height=500,menubar=0,scrollbars=1,resizable=0')">Eyeglass</a>;<%=RSOpth("PRES_RECORD_DATE")%>;<%=RSOpth("PRES_TS")%>
					- <a href="#" onclick="delete_opth('<%=RSOpth("pres_id")%>')"><font class="st" color=red>delete</font></a><br>

		<%end if%>
		<% od_1=RSOpth("OD_1")
		   od_2=RSOpth("OD_2")
		   od_3=RSOpth("OD_3")
		   od_4=RSOpth("OD_4")
		   od_5=RSOpth("OD_5")
		   od_6=RSOpth("OD_6")
		   od_7=RSOpth("OD_7")

		   os_1=RSOpth("OS_1")
		   os_2=RSOpth("OS_2")
		   os_3=RSOpth("OS_3")
		   os_4=RSOpth("OS_4")
		   os_5=RSOpth("OS_5")
		   oS_6=RSOpth("OS_6")
		   os_7=RSOpth("OS_7")

		If RSOpth("pres_type") = "CL" then
			r1="PWR(Sphere)"
			r2="BC"
			r3="DIA"
			r4="CYL"
			r5="AXIS"
			r6="ADD"
			r7="COLOR"
		elseif RSOpth("pres_type")="EG" then
			r1="SPH"
			r2="CYL"
			r3="AXIS"
			r4="ADD"
			r5="PRISM"
			r6="PD"
			r7="NEAR PD"
		end if
     %>
<table>
<tr>
    <td>&nbsp;</td>
    <td><b>OD(Right)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td><b>OS(Left)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>

  <tr>
    <td><%=r1%></td>
    <td><%=od_1%></td>
	<td><%=os_1%></td>
  </tr>
  <tr>
    <td><%=r2%></td>
    <td><%=od_2%></td>
	<td><%=os_2%></td>
  </tr>
  <tr>
    <td><%=r3%></td>
    <td><%=od_3%></td>
	<td><%=os_3%></td>
  </tr>
  <tr>
    <td><%=r4%></td>
    <td><%=od_4%></td>
	<td><%=os_4%></td>
  </tr>
  <tr>
    <td><%=r5%></td>
    <td><%=od_5%></td>
	<td><%=os_5%></td>
  </tr>
  <tr>
    <td><%=r6%></td>
    <td><%=od_6%></td>
	<td><%=os_6%></td>
  </tr>
  <tr>
    <td><%=r7%></td>
    <td><%=od_7%></td>
	<td><%=os_7%></td>
  </tr>
</table>
<%	  
	  if RSOpth("pres_notes")<>"" then  Response.Write "Note:"&RSOpth("pres_notes")&"<br>"
	  if RSOpth("pres_notes_other")<>"" then  Response.Write "Other Note:"&RSOpth("pres_notes_other")&"<br>"
	  RSOpth.MoveNext
	wend

end if
RSOpth.close

'---opth prescriptions --End

RSFileData.Filter = "template_type = '14'"
if RSSocialHx.RecordCount > 0 or RSFileData.RecordCount > 0 then

%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h14" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=PSH')"><% end if %><b>Social Hx:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSSocialHx.RecordCount > 0 then
RSSocialHx.MoveFirst
while not RSSocialHx.EOF
  if Response.IsClientConnected = false then Response.End
  if RSSocialHx("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSSocialHx("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSSocialHx("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSSocialHx("soap_id") & "&template_type=" & RSSocialHx("template_type") & "')"  & setPopupHtml( RSSocialHx("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSSocialHx("description_text") <> "" and RSSocialHx("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSSocialHx("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSSocialHx("template_id") & "'>" & fix_text(RSSocialHx("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSSocialHx("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSSocialHx("template_id") & "&template_type=" & RSSocialHx("template_type") & "')" & setPopupHtml( RSSocialHx("template_id")) &  ">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSSocialHx.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if
%>

<%
RSNoteData.Filter = "template_type = '10'"
RSFileData.Filter = "template_type = '10'"

if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then

%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h10" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=Habits')"><% end if %><b>Habits:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if Response.IsClientConnected = false then Response.End
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSNoteData("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) &">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSNoteData.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if


RSFileData.Filter = "template_type = '12'"

if RSSurgicalHx.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h12" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=Surgical')"><% end if %><b>Surgical Hx:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSSurgicalHx.RecordCount > 0 then
RSSurgicalHx.MoveFirst
while not RSSurgicalHx.EOF
  if Response.IsClientConnected = false then Response.End
  if RSSurgicalHx("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSSurgicalHx("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSSurgicalHx("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSSurgicalHx("soap_id") & "&template_type=" & RSSurgicalHx("template_type") & "')"  & setPopupHtml( RSSurgicalHx("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSSurgicalHx("description_text") <> "" and RSSurgicalHx("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSSurgicalHx("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSSurgicalHx("template_id") & "'>" & fix_text(RSSurgicalHx("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSSurgicalHx("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSSurgicalHx("template_id") & "&template_type=" & RSSurgicalHx("template_type") & "')" & setPopupHtml( RSSurgicalHx("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSSurgicalHx.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if

RSNoteData.Filter = "template_type = '3'"
RSFileData.Filter = "template_type = '3'"

if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h3" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=ROS')"><% end if %><b>ROS:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if Response.IsClientConnected = false then Response.End
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSNoteData("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & RSNoteData("description_text") & "</B><BR>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSNoteData.MoveNext
wend

end if

'--Well Check
RSNoteData.Filter = "template_type = '20'"
RSFileData.Filter = "template_type = '20'"

if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<%if note_closed = false and ReadOnly = false then %><a name="h2" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=WC')"><% end if %><b>Well Checks:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if Response.IsClientConnected = false then Response.End
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSNoteData("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
      '/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""javascript:void(0);"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')"  & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
      '/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "
    end if
    Response.Write "<p>"
  end if
  RSNoteData.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
	if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if

set RSSubSpec = Server.CreateObject("ADODB.Recordset")
RSSubSpec.CursorLocation = 3
'sqlQuery = "SELECT * FROM notes_allergy WHERE visit_key='" & visit_key & "'"
'sqlQuery = "SELECT DISTINCT a.ID,a.VISIT_KEY,a.RECORDED_BY,a.SPECIALTY_TYPE,a.SPECIALIST_NAME,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_STARTED,'mm/dd/yyyy') as date_started,to_char(a.DATE_ENDED,'mm/dd/yyyy') as date_ended,a.APPROVED_BY,a.NOTES FROM notes_subspecialists a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c WHERE a.visit_key = b.visit_key  and b.visit_date <= c.visit_date and a.patient_id='" & patient_id & "'"
sqlQuery = "    select *   from notes_subspecialists  where visit_key ='" & visit_key & "'"
'Response.Write sqlQuery
RSSubSpec.Open sqlQuery, CONN, 3, 4

if not RSSubSpec.EOF then

%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="subspec" href="/practice/notes/display/SubSpecialist_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>"><% end if %><b>Sub Specialists:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<br>
<%

while not RSSubSpec.EOF
  if Response.IsClientConnected = false then Response.End
  'Response.Write "<b><span>" & RSSubSpec("Specialty_Type") & " : " & RSSubSpec("Specialist_Name") & "</span></b>"
	Response.Write "<b><span>" & RSSubSpec("Specialist_Name") & "</span></b>"
  'if RSSubSpec("date_started") <> "" then
   ' Response.Write " - started: " & RSSubSpec("date_started")
  'end if
  'if RSSubSpec("date_ended") <> "" then
   ' Response.Write " - ended: " & RSSubSpec("date_ended")
  'end if
  if RSSubSpec("notes") <> "" then
	'Response.Write ", " & RSSubSpec("notes")
  end if
  Response.Write "<br>"
  RSSubSpec.MoveNext
wend

end if

RSSubSpec.Close




    set RSSubSpec = Server.CreateObject("ADODB.Recordset")
RSSubSpec.CursorLocation = 3
'sqlQuery = "SELECT * FROM notes_allergy WHERE visit_key='" & visit_key & "'"
'sqlQuery = "SELECT DISTINCT a.ID,a.VISIT_KEY,a.RECORDED_BY,a.SPECIALTY_TYPE,a.SPECIALIST_NAME,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_STARTED,'mm/dd/yyyy') as date_started,to_char(a.DATE_ENDED,'mm/dd/yyyy') as date_ended,a.APPROVED_BY,a.NOTES FROM notes_subspecialists a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c WHERE a.visit_key = b.visit_key  and b.visit_date <= c.visit_date and a.patient_id='" & patient_id & "'"
sqlQuery = "    select id, term, status   from COGNITIVESTATUS  where visit_key ='" & visit_key & "'"
'Response.Write sqlQuery
RSSubSpec.Open sqlQuery, CONN, 3, 4

if not RSSubSpec.EOF then

%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="subspec" href="#"><% end if %><b>Functional and Cognitive Status:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<br>
<%

while not RSSubSpec.EOF
  if Response.IsClientConnected = false then Response.End
  
	Response.Write "<b><span>" & RSSubSpec("term") & "</span></b>"
  
  if RSSubSpec("status") <> "" then
	Response.Write ", " & RSSubSpec("status") & "  - " %>
    <a href="#" onclick="delete_cognitive('<%=RSSubSpec("id")%>')"><font class="st" color=red>delete</font></a>
    <%
  end if
  Response.Write "<br>"
  RSSubSpec.MoveNext
wend

end if

RSSubSpec.Close


    
%>
<p>







<div class="titleBox">OBJECTIVE</div>
<%
'---Visual Acuity -start
set RSOpth = Server.CreateObject("ADODB.Recordset")
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='Visual Acuity' and visit_key = '"& visit_key &"' order by DPLY_ORD "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Visual Acuity:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")		
		Response.Write "<br>"		
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Visual Acuity - end
%>

<%'---external exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='EE' and visit_key = '"& visit_key &"' order by DPLY_ORD"
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>External Examination:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")
		If RSOpth("OD_TEXT")<>"" then Response.Write ";OD-"&RSOpth("OD_TEXT")
		If RSOpth("OS_TEXT")<>"" then Response.Write ";OS-"&RSOpth("OS_TEXT")
		If RSOpth("EXAM_NOTES")<>"" then Response.Write ";Note-"&RSOpth("EXAM_NOTES")
		Response.Write "<br>"
	RSOpth.MoveNext		
	wend		
end if
RSOpth.close

'--external exam -end
%>


<%
'---Slit-Lamp exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='SL' and visit_key = '"& visit_key &"' order by DPLY_ORD"
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Slit-Lamp Examination:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")
		If RSOpth("OD_TEXT")<>"" then Response.Write ";OD-"&RSOpth("OD_TEXT")
		If RSOpth("OS_TEXT")<>"" then Response.Write ";OS-"&RSOpth("OS_TEXT")
		If RSOpth("EXAM_NOTES")<>"" then Response.Write " "&RSOpth("EXAM_NOTES")
		Response.Write "<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Slit-Lamp exam -end
%>

<%'---Dilated Fundus -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='Dilated Fundus' and visit_key = '"& visit_key &"' order by DPLY_ORD"
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Dilated Fundus Exam:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "
		If RSOpth("OD_TEXT")<>"" then Response.Write ";OD-"&RSOpth("OD_TEXT")
		If RSOpth("OS_TEXT")<>"" then Response.Write ";OS-"&RSOpth("OS_TEXT")
		If RSOpth("EXAM_notes")<>"" then Response.Write ";"&RSOpth("EXAM_notes")
		Response.Write "<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Dilated Fundus -end
%>

<%
'---Intraocular Pressure -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='Intraocular Pressure' and visit_key = '"& visit_key &"' order by DPLY_ORD "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Intraocular Pressure:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")		
		Response.Write "<br>"		
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Intraocular Pressure - end
%>

<%'---HRT exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='HRT' and visit_key = '"& visit_key &"' "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>HRT Test:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")&" OD-"&RSOpth("OD_TEXT")&";OS- "&RSOpth("OS_TEXT")&" "&RSOpth("exam_notes")&"<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--HRT exam -end
%>

<%'---Visual Fields -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='Visual Fields' and visit_key = '"& visit_key &"' order by DPLY_ORD "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Visual Fields Test:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")
		If RSOpth("OD_TEXT") <>"" then Response.Write " OD-"&RSOpth("OD_TEXT")&";"
		If RSOpth("OS_TEXT") <>"" then Response.Write " OS- "&RSOpth("OS_TEXT")&";"
		If RSOpth("exam_notes") <>"" then Response.Write RSOpth("exam_notes")
		Response.Write "<br>"		
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Visual Fields -end
%>

<%'---Gonioscopic exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='GE' and visit_key = '"& visit_key &"' order by DPLY_ORD"
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Gonioscopic Examination:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")
		If RSOpth("OD_TEXT") <>"" then Response.Write " OD-"&RSOpth("OD_TEXT")&";"
		If RSOpth("OS_TEXT") <>"" then Response.Write " OS- "&RSOpth("OS_TEXT")&";"
		If RSOpth("exam_notes") <>"" then Response.Write RSOpth("exam_notes")
		Response.Write "<br>"		
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Gonioscopic exam -end
%>

<%'---vsp exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='VSP' and visit_key = '"& visit_key &"' "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>VSP Examination:</b></u>
					- <a href="#" onclick="delete_opth_ee('<%=RSOpth("exam_id")%>')"><font class="st" color=red>delete</font></a><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_NOTES")&"<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--vsp exam -end
%>


<%
set RSVitals = Server.CreateObject("ADODB.Recordset")
RSVitals.CursorLocation = 3
sqlQuery = "SELECT DISTINCT take FROM notes_vitals WHERE visit_key='" & visit_key & "'"
RSVitals.Open sqlQuery, CONN, 3, 4
VCount = RSVitals.RecordCount
RSVitals.Close

if VCount > 0 then

%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="vitals" href="#"><% end if %><b>Vital Signs:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<br>
<%
  sqlQuery = "SELECT * FROM notes_vitals WHERE visit_key='" & visit_key & "' ORDER BY take ASC, vital_type ASC"
  RSVitals.Open sqlQuery, CONN, 3, 4
  set RSVitals.ActiveConnection = nothing

  dim VCounter, invalidVital
  invalidVital = false

  for VCounter = 1 to VCount
    if Response.IsClientConnected = false then Response.End
    'reset Ophal flag
    Ophal_Started = 0
    IP_Stared = 0
    MR_Started = 0
    P_Started = 0

    RSVitals.Filter = "take = '" & VCounter & "'"
    RSVitals.Sort = " Vital_ID ASC"
    if RSVitals.RecordCount > 0 then

      if RSVitals("status") = "I" then
        Response.Write "<s>"
        invalidVital = true
      end if
      Response.Write ("<br><b>Take " & VCounter & "</b>: ")
      Response.Write "" & RSVitals("datestamp") & " - <BR>"

      RSVitals.MoveFirst
      while not RSVitals.EOF
        select case RSVitals("vital_type").Value
          case "T":
            Response.Write "<b>T:</b>" & RSVitals("vital_data1")&";"
          case "HR":
            Response.Write "<b>HR:</b>" & RSVitals("vital_data1")&";"
          case "BP":
            Response.Write "<b>BP:</b>" & RSVitals("vital_data1") & "/" & RSVitals("vital_data2")
            if RSVitals("vital_data3").value <> "" then Response.Write " " & RSVitals("vital_data3").value
            if RSVitals("vital_data3").value <> "" and RSVitals("vital_data4").value <> "" then Response.Write ","
            if RSVitals("vital_data4").value <> "" then Response.Write " " & RSVitals("vital_data4").value&";"
          case "RR":
            Response.Write "<b>RR:</b>" & RSVitals("vital_data1")
          case "W":
            Response.Write "<b>Weight:</b>" & RSVitals("vital_data1") & "kgs (" & Round(RSVitals("vital_data1").Value / .4536,2) & "lbs)"&";"
          case "H":
            Response.Write "<b>Height:</b>" & RSVitals("vital_data1") & "cm (" & Round(RSVitals("vital_data1").Value * .3937,2) & "in)"&";"
          case "HC":
            Response.Write "<b>Head:</b>" & RSVitals("vital_data1") & "cm (" & Round(RSVitals("vital_data1").Value * .3937,2) & "in)"&";"
          case "BFP":
            Response.Write "<b>Body Fat%:</b>" & RSVitals("vital_data1")&";"
          case "BMI":
            Response.Write "<b>Body Mass Index:</b>" & RSVitals("vital_data1")&";"
          case "O2 SAT":
            Response.Write "<b>O2 SAT:</b>" & RSVitals("vital_data1")&";"
          case "Shoe Size":
            Response.Write "Shoe Size:" & RSVitals("vital_data1")&";"
          'case "VaSC_RDist","VaSC RPH","VaSC RNear","VaCC RDist","VaCC RPH","VaCC RNear","VaSC LDist","VaSC LPH","VaSC LNear","VaCC LDist","VaCC LPH","VaCC LNear":
		'	Response.Write RSVitals("Vital_Type")& ": 20/" & RSVitals("vital_data1")
		  case "VaSC RDist","VaSC RPH","VaSC RNear","VaCC RDist","VaCC RPH","VaCC RNear","VaSC LDist","VaSC LPH","VaSC LNear","VaCC LDist","VaCC LPH","VaCC LNear":
			'Response.Write RSVitals("Vital_Type")& ": 20/" & RSVitals("vital_data1")
			if Ophal_Started <> 1 then
				set RSVitalVisual = server.CreateObject("ADODB.RecordSet")
				Set RSVitalVisual = RSVitals.Clone
				Response.Write "<table width='70%' border=0 cellspacing=0 cellpadding=0>"
				Response.Write "<tr><td colspan=3><u><b>Visual Acuity</b></u></td></tr>"

				Ophal_Type_Str = "VaSC RDist;VaSC LDist*VaSC RPH;VaSC LPH*VaSC RNear;VaSC LNear*VaCC RDist;VaCC LDist*VaCC RPH;VaCC LPH*VaCC RNear;VaCC LNear"
				'.write "<tr><td colspan=2>VaSC</td></tr>"
				Ophal_Type = split(Ophal_Type_Str,"*")
				For i = 0 to UBound(Ophal_Type)
					Response.Write "<tr><td>" & left(Ophal_Type(i),4) & ":</td>"
					Ophal_SubType = split(Ophal_Type(i),";")
					For k = 0 to UBound(Ophal_SubType)
						'RSVitalVisual.MoveFirst
						RSVitalVisual.Filter = " Vital_Type = '"& Ophal_SubType(k) &"' and take = '" & VCounter & "'"
						if not RSVitalVisual.EOF  Then
							Response.write "<td>"&right(RSVitalVisual("Vital_Type"),len(RSVitalVisual("Vital_Type"))-4)& " = 20/" & RSVitalVisual("vital_data1") & "</td>"
						end if
					Next
					Response.Write "</tr>"
				Next

				Response.Write "</table>"

				' Short circuit if Ophal info has already been proccessed.
				Ophal_Started = 1
				RSVitalVisual.Close
				Set RSVitalVisual = Nothing
			end if
		  case "Method","R Ta","R Tc","L Ta","L Tc":
			if IP_Started <> 1 then
				set RSVitalIP = server.CreateObject("ADODB.RecordSet")
				Set RSVitalIP = RSVitals.Clone

				Response.Write "<table width='70%' border=0 cellspacing=0 cellpadding=0>"
				if IP_Flag <> 1 then
					Response.Write "<tr><td colspan=3><u><b>Intraocular Pressure</b></u></td></tr>"
				end if

				RSVitalIP.MoveFirst
				RSVitalIP.Filter = " Vital_Type = 'Method' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "<td>Method="&RSVitalIP("vital_data1")&"<td></tr>"
				end if

				RSVitalIP.Filter = " Vital_Type = 'R Ta' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "<tr><td>"&right(RSVitalIP("Vital_Type"),2)&":R="&RSVitalIP("vital_data1")&";&nbsp;"
				end if

				'RSVitalIP.MoveFirst
				RSVitalIP.Filter = " Vital_Type = 'L Ta' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "L="&RSVitalIP("vital_data1")&"<td></tr>"
				end if

				RSVitalIP.MoveFirst
				RSVitalIP.Filter = " Vital_Type = 'R Tc' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "<tr><td>"&right(RSVitalIP("Vital_Type"),2)&":</td><td>&nbsp;R="&RSVitalIP("vital_data1")&"<td>"
				end if
				'RSVitalIP.MoveFirst
				RSVitalIP.Filter = " Vital_Type = 'L Tc' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "<td>&nbsp;&nbsp;L="&RSVitalIP("vital_data1")&"<td></tr>"
				end if
				Response.Write "</table>"
			end if
			IP_Started = 1
			IP_Flag = 1
          case "R MR OD","R MR OS","R MR OD 20","R MR OS 20","L MR OD","L MR OS","L MR OD 20","L MR OS 20":
			'Response.Write RSVitals("Vital_Type") & ": 20/" & RSVitals("vital_data1")
			if MR_Started <> 1 Then
				set RSVitalMR = server.CreateObject("ADODB.RecordSet")
				Set RSVitalMR = RSVitals.Clone

				Response.Write "<table width='70%' border=0 cellspacing=0 cellpadding=0>"
				if IP_Flag <> 1 then
					Response.Write "<tr><td colspan=3><u><b>Intraocular Pressure</b></u></td></tr>"
				end if
				'RSVitalMR.MoveFirst
				RSVitalMR.Filter = " Vital_Type = 'R MR OD' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "<tr><td>"&left(RSVitalMR("Vital_Type"),4)&" ("&right(RSVitalMR("vital_type"),2)&"):</td><td>"&RSVitalMR("vital_data1")
				end if
				RSVitalMR.Filter = " Vital_Type = 'R MR OD 20' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "  "&right(RSVitalMR("Vital_Type"),2)&"/"&RSVitalMR("vital_data1")&"<td>"
				end if
				'RSVitalMR.MoveFirst
				RSVitalMR.Filter = " Vital_Type = 'R MR OS' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "<td>"&left(RSVitalMR("Vital_Type"),4)&" ("&right(RSVitalMR("vital_type"),2)&"):</td><td>"&RSVitalMR("vital_data1")
				end if
				RSVitalMR.Filter = " Vital_Type = 'R MR OS 20' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "  "&right(RSVitalMR("Vital_Type"),2)&"/"&RSVitalMR("vital_data1")&"<td></tr>"
				end if

				RSVitalMR.Filter = " Vital_Type = 'L MR OD' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "<tr><td>"&left(RSVitalMR("Vital_Type"),4)&" ("&right(RSVitalMR("vital_type"),2)&"):</td><td>"&RSVitalMR("vital_data1")
				end if
				RSVitalMR.Filter = " Vital_Type = 'L MR OD 20' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "  "&right(RSVitalMR("Vital_Type"),2)&"/"&RSVitalMR("vital_data1")&"<td>"
				end if
				'RSVitalMR.MoveFirst
				RSVitalMR.Filter = " Vital_Type = 'L MR OS' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "<td>"&left(RSVitalMR("Vital_Type"),4)&" ("&right(RSVitalMR("vital_type"),2)&"):</td><td>"&RSVitalMR("vital_data1")
				end if
				RSVitalMR.Filter = " Vital_Type = 'L MR OS 20' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "  "&right(RSVitalMR("Vital_Type"),2)&"/"&RSVitalMR("vital_data1")&"<td></tr>"
				end if
				Response.Write "</table>"
			End if
			MR_Started = 1
			IP_Flag = 1
		  case "R P OD","R P OS","L P OD","L P OS":
			if P_Started <> 1 Then
				set RSVitalP = server.CreateObject("ADODB.RecordSet")
				Set RSVitalP = RSVitals.Clone

				Response.Write "<table width='70%' border=0 cellspacing=0 cellpadding=0>"
				if IP_Flag <> 1 then
					Response.Write "<tr><td colspan=3><u><b>Intraocular Pressure</b></u></td></tr>"
				end if
				'RSVitalP.MoveFirst
				RSVitalP.Filter = " Vital_Type = 'R P OD' and take = '" & VCounter & "'"
				if not RSVitalP.EOF Then
					Response.Write "<tr><td>"&left(RSVitalP("Vital_Type"),4)&" ("&right(RSVitalP("vital_type"),2)&"):</td><td>"&RSVitalP("vital_data1")&"</td>"
				end if
				'RSVitalP.MoveFirst
				RSVitalP.Filter = " Vital_Type = 'R P OS' and take = '" & VCounter & "'"
				if not RSVitalP.EOF Then
					Response.Write "<td>"&left(RSVitalP("Vital_Type"),4)&" ("&right(RSVitalP("vital_type"),2)&"):</td><td>"&RSVitalP("vital_data1")& "</td></tr>"
				end if

				RSVitalP.Filter = " Vital_Type = 'L P OD' and take = '" & VCounter & "'"
				if not RSVitalP.EOF Then
					Response.Write "<tr><td>"&left(RSVitalP("Vital_Type"),4)&" ("&right(RSVitalP("vital_type"),2)&"):</td><td>"&RSVitalP("vital_data1")&"</td>"
				end if
				'RSVitalP.MoveFirst
				RSVitalP.Filter = " Vital_Type = 'L P OS' and take = '" & VCounter & "'"
				if not RSVitalP.EOF Then
					Response.Write "<td>"&left(RSVitalP("Vital_Type"),4)&" ("&right(RSVitalP("vital_type"),2)&"):</td><td>"&RSVitalP("vital_data1")&"</td></tr>"
				end if
				Response.Write "</table>"
			End if
			P_Started = 1
			IP_Flag = 1
          case else
			Response.Write RSVitals("Vital_Type")& ":" & RSVitals("vital_data1")&";&nbsp;"

        end select
        'Response.Write ";&nbsp;"
        RSVitals.MoveNext
      wend
      if invalidVital = true then
        Response.Write "</s><br>"
      else
        if note_closed = false and ReadOnly = false then
          Response.Write " - <span class='st'><a href=""#"" onclick=""if (confirm('Do you want to mark vital sign take #" & VCount & " invalid?')) { parent.updateNoteFrame.location.replace('/practice/notes/edit/vital_signs.asp?visit_key=" & visit_key & "&take=" & VCounter &"&action=delete') }"">invalidate</a></span><br>"
        end if
      end if
      invalidVital = false
    end if
    IP_Flag = 0
  next

end if

RSNoteData.Filter = "template_type = '1'"
RSFileData.Filter = "template_type = '1'"

if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h1" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=Exam')"><% end if %><b>Exams:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if Response.IsClientConnected = false then Response.End
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSNoteData("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSNoteData.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if

%>

<%
if RSPain.RecordCount > 0 then
RSPain.MoveFirst
%>
<b>Pain Management:</b><br>
<%

While not RSPain.EOF
pain_m = RSPain("pain_m")
response.Write pain_m&"<br>"
RSPain.MoveNext
wend
end if
%>
</div>



<div id="right" style="float: right; margin-bottom: 3em; width: 49.5%;">
<div class="titleBox">ASSESSMENT</div>

<%'--Assessment - Previous visit
set RSAss = Server.CreateObject("ADODB.Recordset")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+) AND A.template_type=18 and A.VISIT_KEY =(select visit_key from (select visit_key from visit where visit_date < (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') and patient_id = '"& patient_id &"' order by visit_date desc ) where rownum <=1 )"
'Response.Write sqlQuery
RSAss.Open sqlQuery, CONN
If not (RSAss.eof or RSAss.bof) then
%>
<p>
<b><u>Assessment:(Previous Visit)</u></b><BR>
<%
	RSAss.MoveFirst
	while not RSAss.EOF
	  if RSAss("custom_note") = "1" then ' free text
	    Response.Write "" & fix_text(RSAss("sentence_text"))
	    'if note_closed = false and ReadOnly = false then
	      'Response.Write " <span style='font-size: 8px'><a name='s" & RSAss("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSAss("soap_id") & "&template_type=" & RSAss("template_type") & "')"">edit</a></span>"
	    'end if
	    Response.Write "<p>"
	  else ' not free text
	    if RSAss("description_text") <> "" and RSAss("description_text") <> vbCrLf then
	      Response.Write "<P><B>" & substitute_text(RSAss("description_text"), patient_name, patient_gender) & "</B>"
	    else
	      Response.Write "" ' Clear
	    end if
	    Response.Write "<div style='display: inline;' id='id_" & RSAss("template_id") & "'>" & fix_text(RSAss("sentence_text")) & "</div>"
	    if note_closed = false and ReadOnly = false then
	      Response.Write " <span style='font-size: 8px'><a name='s" & RSAss("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSAss("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
	    end if
	    Response.Write "<p>"  end if
	  RSAss.MoveNext
	wend
end if
%>


<%'--Assessment - current visit
RSNoteData.Filter = "template_type = '18'"
RSFileData.Filter = "template_type = '18'"

if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h5" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=Assessment')"><% end if %><b>Assessment:(New)</b><BR><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "" & fix_text(RSNoteData("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSNoteData.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if

RSNoteData.Filter = "template_type = '5'"
RSFileData.Filter = "template_type = '5'"

if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h5" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=Reports')"><% end if %><b>Lab/Test Reports:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    'Response.Write "" & fix_text(RSNoteData("sentence_text"))
    Response.Write "" & RSNoteData("sentence_text")
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "</p>"  
  end if
  RSNoteData.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if

RSNoteData.Filter = "template_type = '6'"
RSFileData.Filter = "template_type = '6'"

if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h6" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=Procedure')"><% end if %><b>Procedure Reports:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "" & fix_text(RSNoteData("sentence_text"))
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSNoteData.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if
%>
<li><a name="ssf" href="/practice/notes/display/clinical_summary.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>" target="rightNoteFrame"><b>Clinical Summary</b></a>

<p>
<div class="titleBox">PLAN</div>
<%

set RSMeds = Server.CreateObject("ADODB.Recordset")
RSMeds.CursorLocation = 3
sqlQuery = "select * from notes_medications where patient_id='" & patient_id & "' and visit_key='" & visit_key & "' and status = 'Current' "
RSMeds.Open sqlQuery, CONN, 3, 4

if RSMeds.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="meds" href="/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&show=new&other_meds=Current"><% end if %><b>New Medications:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<p>
<%

while not RSMeds.EOF
%>
<b><%=RSMeds("drug_name")%></b> - <%=RSMeds("formulation")%> (<%=RSMeds("dose")&"/"&RSMeds("frequency")%>)<br>
<%
  RSMeds.MoveNext
wend

end if

RSNoteData.Filter = "template_type = '4'"
RSFileData.Filter = "template_type = '4'"


if RSNoteData.RecordCount > 0 or RSFileData.RecordCount > 0 then
%>
<p>
<% if note_closed = false and ReadOnly = false then %><a name="h4" href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/default.asp?visit_key=<%=visit_key%>&amp;section=Plan')"><% end if %><b>Plan of Care:</b><% if note_closed = false and ReadOnly = false then %></a><% end if %>
<%
end if

if RSNoteData.RecordCount > 0 then
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<br>" & fix_text(RSNoteData("sentence_text"))
    if note_closed = false and ReadOnly = false  then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/custom_note.asp?visit_key=" & visit_key & "&soap_id=" & RSNoteData("soap_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
    if note_closed = false and ReadOnly = false  then
      Response.Write " <span style='font-size: 8px'><a name='s" & RSNoteData("soap_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/template.asp?visit_key=" & visit_key & "&template_id=" & RSNoteData("template_id") & "&template_type=" & RSNoteData("template_type") & "')" & setPopupHtml( RSNoteData("template_id")) & ">edit</a></span>"
    end if
    Response.Write "<p>"  end if
  RSNoteData.MoveNext
wend
end if

' File Info
if RSFileData.RecordCount > 0 then
Response.Write "<P>"
RSFileData.MoveFirst
if not RSFileData.EOF then
  Response.Write "Pen/Images "
end if
While not RSFileData.EOF
    if Response.IsClientConnected = false then Response.End
	Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/display/ink_pad.asp?file_id="&RSFileData("FILE_ID")&"&visit_key="&visit_key&"&section=HPI&comments="&RSFileData("comments")&"')""><img src='/images/practice/wordpad.gif' border=0>&nbsp;"&RSFileData("comments")&"</a></span>"
	RSFileData.MoveNext
	if not RSFileData.EOF then
		Response.Write ", "
	end if
wend
Response.Write ("<BR>")
end if

%>

<p>
<div class="titleBox">CODING</div>
<form name="frm" method="post" action="note.asp">
<input type="hidden" name="type">
<input type="hidden" name="code">
<input type="hidden" name="actiontype">
<input type="hidden" name="previous_visit_key">
<input type="hidden" name="visit_key" value="<%=visit_key%>">
<%
	Dim res
	Set res = server.CreateObject("ADODB.RecordSet")

	' previous visit icd code
res.Open "select b.icdname as asses,b.icdcode as code,a.visit_key as previous_visit_key from assessplan_new a,icdtable b where a.icdcode = b.icdcode and a.visit_key=(select visit_key from (select visit_key from visit where visit_date < (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') and patient_id = '"& patient_id &"' order by visit_date desc ) where rownum <=1 ) " &_
		" UNION " &_
		" select b.term as asses,b.conceptid as code,a.visit_key as previous_visit_key from assessplan_new a,snomed_for_ccd b where a.icdcode = b.conceptid and rownum <= 1 and a.visit_key=(select visit_key from (select visit_key from visit where visit_date < (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') and patient_id = '"& patient_id &"' order by visit_date desc ) where rownum <=1 ) ",CONN
if not res.EOF then
%>
<p>
<a name="icd" href="#"><b>Diagnosis Codes Used In Previous Visit:</b></a>
<p>
<% while not res.eof %>

<b><%=res("code")%></b> - <%=ucase(res("asses"))%>
<%
if note_closed = false and ReadOnly = false  then
%>
 - <a href="#" onclick="javascript:ActionConfirm('Add','<%=res("code")%>','Diagnosis','<%=res("previous_visit_key")%>')"><font class="st">Add</font></a>
<%
end if
%>
<br>
<%
		res.movenext
		wend
%>

<%
end if

res.Close

	' current visit icd code
res.Open "select b.icdname as asses,b.icdcode as code " &_ 
				" from assessplan_new a,icdtable b " &_
				" where a.icdcode = b.icdcode and a.visit_key='"& visit_key &"' " &_
				" union " &_
				" SELECT list1.asses, list1.code FROM (  select trim(b.term) as asses,b.conceptid as code from assessplan_new a,snomed_for_ccd b where a.icdcode = b.conceptid and a.visit_key='"& visit_key &"') list1 INNER JOIN ( select MAX(length(trim(b.term))) as length_asses, b.conceptid as code from assessplan_new a,snomed_for_ccd b where a.icdcode = b.conceptid and a.visit_key='"& visit_key &"' group by b.conceptid ) list2 ON length(list1.asses) = list2.length_asses and list1.code = list2.code ",CONN
	
if not res.eof then
%>
<p>
<% if note_closed = false and ReadOnly = false  then %><a name="icd" href="/practice/notes/add/assess_plan_search.asp?visit_key=<%=visit_key%>&amp;section=ASS&amp;code_type=ICD"><% end if %><b>Diagnosis Codes(New):</b><% if note_closed = false and ReadOnly = false  then %></a><% end if %>
<p>
<% while not res.eof %>

<b><%=res("code")%></b> - <%=ucase(res("asses"))%>
<%
if note_closed = false and ReadOnly = false  then
%>
 - <a href="#" onclick="javascript:ActionConfirm('Delete','<%=res("code")%>','Diagnosis','')"><font class="st">delete</font></a>
<%
end if
%>
<br>
<%
		res.movenext
		wend
%>

<%
end if

res.Close

Dim res1
set res1 = server.CreateObject("ADODB.RecordSet")
' previous CPT codes
res.Open "select * from assessprocordered where visitkey=(select visit_key from (select visit_key from visit where visit_date < (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') and patient_id = '"& patient_id &"' order by visit_date desc ) where rownum <=1 ) ",con
if not res.EOF then
%>
<p>
<a href="#"><b>Procedure Codes Used In Previous Visit:</b></a>
<p>
<%			while not res.EOF
			if ucase(res("code_type")) = ucase("snomed") then
				res1.Open "select term as procname from snomed_for_ccd where conceptid='"&res("cpt_code")&"' and active = 1 and rownum <=1",con
			elseif ucase(res("code_type")) <> ucase("hcpcs") then
				res1.Open "select procname from cpt_list where proccode='"&res("cpt_code")&"'",con
            else
				res1.Open "select hcpsname as procname from hcpstable where hcpscode='"&res("cpt_code")&"'",con
			end if
    
			'if left(ucase(res1("procname")),6) <> "OFFICE" then
			if not res1.EOF then
				ProcName = ucase(res1("procname"))
			else
				ProcName = "THIS CODE IS NO LONGER AVAILABLE."
			end if
%>

<b><%=res("cpt_code")%></b> - <%=ProcName%>
<%
if note_closed = false and ReadOnly = false  then
%>
 - <a href="#" onclick="javascript:ActionConfirm('Add','<%=res("cpt_code")%>','Procedure','<%=res("visitkey")%>')"><font class="st">Add</font></a>
<%
end if
%>
<br>
<%
			'end if
			res.MoveNext
			res1.Close
			wend
   end if
   res.Close

' current cpt codes
res.Open "select * from assessprocordered where visitkey='"&visit_key&"' order by cpt_code",con
if not res.EOF then
%>
<p>
<% if note_closed = false and ReadOnly = false  then %><a href="/practice/notes/add/assess_plan_search.asp?visit_key=<%=visit_key%>&amp;section=ASS&amp;code_type=CPT"><% end if %><b>Procedure Codes(New):</b><% if note_closed = false and ReadOnly = false  then %></a><% end if %>
<p>
<%
	while not res.EOF		
           if ucase(res("code_type")) = ucase("snomed") then
				res1.Open "select term as procname from snomed_for_ccd where conceptid='"&res("cpt_code")&"' and active = 1 and rownum <=1",con
			elseif ucase(res("code_type")) <> ucase("hcpcs") then
				res1.Open "select procname from cpt_list where proccode='"&res("cpt_code")&"'",con
            else
				res1.Open "select hcpsname as procname from hcpstable where hcpscode='"&res("cpt_code")&"'",con
			end if

			'if left(ucase(res1("procname")),6) <> "OFFICE" then
			if not res1.EOF then
				ProcName = ucase(res1("procname"))
			else
				ProcName = "THIS CODE IS NO LONGER AVAILABLE."
			end if
%>

<b><%=res("cpt_code")%></b> - <%=ProcName%>
<%
if note_closed = false and ReadOnly = false  then
%>
 - <a href="#" onclick="javascript:ActionConfirm('Delete','<%=res("cpt_code")%>','Procedure','')"><font class="st">delete</font></a>
<%
end if
%>
<br>
<%
			'end if
			res.MoveNext
			res1.Close

			wend
   end if
   res.Close

%>

<p>
<div class="titleBox">FILES</div>
<p>
<%

' Upload file info
if RSUploadData.RecordCount > 0 then
RSUploadData.MoveFirst
While not RSUploadData.EOF
	if Response.IsClientConnected = false then Response.End
	if UCase(RSUploadData("File_Type")) = "GIF" or UCase(RSUploadData("File_Type")) = "JPG" then
		IconFile = "/images/global/imageIcon.gif"
			ActionFile = "/practice/notes/display/display_file.asp"
	elseif UCase(RSUploadData("File_Type")) = "DOC" then
		IconFile = "/images/global/wordIcon.gif"
		ActionFile = "/practice/notes/display/display_file.asp"
	elseif UCase(RSUploadData("File_Type")) = "PDF" then
		IconFile = "/images/global/pdfIcon.gif"
		ActionFile = "/practice/notes/display/display_file.asp"
	elseif UCase(RSUploadData("File_Type")) = "ZIP" then
		IconFile = "/images/global/zipIcon.gif"
		ActionFile = "/practice/notes/display/display_file.asp"
	else
		IconFile = "/images/practice/wordpad.gif"
		ActionFile = "/practice/notes/display/display_file.asp"
	end if

	if RSUploadData("comments") <> "" then
		nd = "onMouseOver=""drs('"&RSUploadData("comments")&"'); return true;"" onMouseOut=""nd(); return true;"""
	else
		nd = ""
	end if

	If RSUploadData("FOLDER_NAME")<>"" then
		If FOLDER_NAME<>RSUploadData("FOLDER_NAME") then
			FOLDER_NAME = RSUploadData("FOLDER_NAME")
			IconFile = "/images/global/folder_image.gif"
			ActionFile = "/practice/notes/display/display_folder_file.asp"
			Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href='#' " & nd & " onclick='javascript:popScrollWindowMax("&("""")&ActionFile&"?nc_status="&nc_status&"&file_id="&RSUploadData("FOLDER_NAME")&"&patient_id="&patient_id&"&section=HPI"&("""")&",800,400);'><img src='"&IconFile&"' border=0>&nbsp;"&RSUploadData("FOLDER_NAME")&"</a></span>"&"<br>"
		end if
	else
		Response.Write "<span style='padding-top: 2px; padding-bottom: 2px' onmouseover=""this.style.backgroundColor='" & hlcolor & "'"" onmouseout=""this.style.backgroundColor='#FFFFFF'""><a href='#' " & nd & " onclick='javascript:popScrollWindowMax("&("""")&ActionFile&"?nc_status="&nc_status&"&file_id="&RSUploadData("FILE_ID")&"&visit_key="&RSUploadData("Visit_Key")&"&section=HPI"&("""")&",800,400);'><img src='"&IconFile&"' border=0>&nbsp;"&RSUploadData("File_Name")&"</a></span>"
		if (RSUploadData("Comments") = "Referral Letter") then
			%>
			<a name="refletter" href="/practice/notes/display/ref_letter_edit.asp?visit_key=<%=visit_key%>&file_id=<%=RSUploadData("FILE_ID")%>" target="rightNoteFrame"><font class="st">edit</font></a>
			<%
		end if
		%>
		- <a href="#" onclick="delete_file('<%=RSUploadData("FILE_ID")%>')"><font class="st" color=red>delete</font></a><br>
		<%
	end if
	
	RSUploadData.MoveNext

	'if not RSUploadData.EOF then
		'Response.Write " <BR>"
	'end if
wend
end if

%>
</form>

<%
if RSDiagTests.RecordCount > 0 then
RSDiagTests.MoveFirst

Response.Write "<b>Diagnostic Reports:</b><br>"

'Response.Write "Echo Test: "
While not RSDiagTests.EOF
	If RSDiagTests("TEST_TYPE")="RESTING" then Response.Write "Resting Echo -"
	If RSDiagTests("TEST_TYPE")="STRESS" then Response.Write "Stress Echo -"
%>
  <a href="#" onclick="window.open('/practice/hub/print_echotest.asp?visit_key=<%=RSDiagTests("visit_key")%>&amp;patient_id=<%=patient_id%>&amp;test_type=<%=RSDiagTests("TEST_TYPE")%>','aaaa','width=700,height=500,menubar=1,scrollbars=1,resizable=0')">

<%	Response.Write RSDiagTests("STUDY_DATE")&";</a>" %>

<% 'if note_closed = false and ReadOnly = false  then %>
	<!--- <a href="#" onclick="delete_echo('<%=RSDiagTests("visit_key")%>','<%=patient_id%>','<%=RSDiagTests("TEST_TYPE")%>')"><font class="st" color=red>delete</font></a>-->
<%'end if%>

<%	Response.Write "<br>"
	RSDiagTests.MoveNext
wend
end if

While not RSDiagTST.EOF
  Response.Write "Treadmill Stress Test -"
%>
  <a href="#" onclick="window.open('/practice/hub/print_echotst.asp?id_no=<%=RSDiagTST("id_no")%>','t1','width=700,height=500,menubar=1,scrollbars=1,resizable=0')">

<%	Response.Write RSDiagTST("STUDY_DATE")&"</a>" %>

<%	Response.Write "<br>"
	RSDiagTST.MoveNext
wend

%>

<%
While not RsNU.EOF
  Response.Write "Nerve Conduction Study -"
%>
  <a href="#" onclick="window.open('neuro_master_print.asp?id_no=<%=RsNU("id_no")%>','t1','width=700,height=500,menubar=1,scrollbars=1,resizable=0')">

<%	Response.Write RsNU("test_type")&"</a>" %>

<%	Response.Write "<br>"
	RsNU.MoveNext
wend

%>



<%
'--ss form
if RSssForm.RecordCount > 0 then
RSssForm.MoveFirst

Response.Write "<b>SS Form:</b><br>"
While not RSssForm.EOF	
%>
	Visit Date - <a href="#" onclick="window.open('ss_form_print.asp?id_no=<%=RSssForm("id_no")%>','ss1','width=700,height=500,menubar=0,scrollbars=1,resizable=0')">

<%	Response.Write RSssForm("VISIT_DATE")&";</a>" %>
	- <a href="#" onclick="delete_ss('<%=RSssForm("id_no")%>')"><font class="st" color=red>delete</font></a><br>
<%	Response.Write "<br>"
	RSssForm.MoveNext
wend
end if
'--end of ss form
%>

<%
'--Annual exam form
if RSAEForm.RecordCount > 0 then
RSAEForm.MoveFirst

Response.Write "<b>SOAP Note:</b><br>"
While not RSAEForm.EOF	
%>
	Visit Date - <a href="#" onclick="window.open('ss_form_print.asp?id_no=<%=RSAEForm("id_no")%>','ss1','width=700,height=500,menubar=0,scrollbars=1,resizable=0')">

<%	Response.Write RSAEForm("VISIT_DATE")&";</a>" %>
	- <a href="#" onclick="delete_ss('<%=RSAEForm("id_no")%>')"><font class="st" color=red>delete</font></a><br>
<%	Response.Write "<br>"
	RSAEForm.MoveNext
wend
end if
'--end of Annual exam form
%>

<%
'--Excuse form
if RSExForm.RecordCount > 0 then
RSExForm.MoveFirst

Response.Write "<b>Excuse Letter:</b><br>"
While not RSExForm.EOF	
%>
 Date - <a href="#" onclick="window.open('ex_form_print.asp?id_no=<%=RSExForm("id_no")%>','ss1','width=700,height=500,menubar=0,scrollbars=1,resizable=0')">

<%	Response.Write RSExForm("VISIT_DATE")&";</a>" %>
	- <a href="#" onclick="delete_excuse('<%=RSExForm("id_no")%>')"><font class="st" color=red>delete</font></a><br>
<%	Response.Write "<br>"
	RSExForm.MoveNext
wend
end if
'--end of excuse form

'--Referral form
if RSRefForm.RecordCount > 0 then
RSRefForm.MoveFirst

Response.Write "<b>Referral Letter:</b><br>"
While not RSRefForm.EOF	
%>
	Date - <a href="#" onclick="window.open('patient_referral_print.asp?id_no=<%=RSRefForm("id_no")%>','ss1','width=700,height=500,menubar=0,scrollbars=1,resizable=0')">

<%	Response.Write RSRefForm("VISIT_DATE") & ":" & RSRefForm("EX_TYPE") & "</a>" %>
	- <a name="refletter" href="/practice/notes/display/patient_referral_save.asp?action_type=DELETE&visit_key=<%=visit_key%>&file_id=<%=RSRefForm("id_no")%>" target="rightNoteFrame"><font class="st" color=red>delete</font></a><br>
<%	Response.Write "<br>"	
	RSRefForm.MoveNext
wend
end if
'--end of Referral form
%>

<p>
<div class="titleBox">Other</div>
<p>

<%
if RSNotes.RecordCount > 0 then
RSNotes.MoveFirst
%>
	<p>
	<div class="titleBox">Note/Reminder</div>
	<p>
<%

While not RSNotes.EOF
rem_status=RSNotes("status")
if rem_status="C" then 
    rem_status="closed"
    Response.Write RSNotes("subject") & "<font size=1 color=red>(closed)</font>"
else
    Response.Write RSNotes("subject")
end if
Response.Write " <span style='font-size: 9px'><a name='n" & RSNotes("event_id") & "' href=""#"" onclick=""getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/tickets/update_note.asp?page_from=EMR&id=" & RSNotes("event_id")& "')"">update</a></span>"
Response.Write "<br>"

RSNotes.MoveNext
wend
end if
%>



<%
if RSNoteAmend.RecordCount > 0 then
RSNoteAmend.MoveFirst
%>
	<p>
	<div class="titleBox">Amendments</div>
	<p>
<%
    amendmentnumber = 1
While not RSNoteAmend.EOF
Response.Write "<b>Amendment #"&amendmentnumber&":</b><br>"
Response.Write RSNoteAmend("notes_amend")
Response.Write "<b><br>Recorded By: </b>"&RSNoteAmend("name")&" on "&" "&CalculateLocalTime(RSNoteAmend("date_amend"))&"("&session("TimeZone")&")"&"<br>"
if (RSNoteAmend("status") <> "") then
Response.Write "<b>Amendment Status: </b>"&RSNoteAmend("status")&"<br>"
end if 
Response.Write "<br>"
RSNoteAmend.MoveNext
amendmentnumber = amendmentnumber +1
wend
end if

%>



<%
if RSPatExams.RecordCount > 0 then
RSPatExams.MoveFirst
%>
	<p>
	<div class="titleBox">Exams Ordered</div>
	<p>
<%
While not RSPatExams.EOF
	Response.Write RSPatExams("EXAM_CATEGORY")&" - "
%>
	 <a href="#" onclick="window.open('pat_exams_print.asp?id_no=<%=RSPatExams("id_no")%>','aaaa','width=700,height=500,resizable=1,scrollbars=1')"><%=RSPatExams("EXAM_NAME")%></a>
	-<a href="#" onclick="delete_patexam('<%=RSPatExams("id_no")%>')"><font class="st" color=red>delete</font></a> <br>


<%
	RSPatExams.MoveNext
wend
end if
%>


<%
if RSLabcorp.RecordCount > 0 then
RSLabcorp.MoveFirst
%>
	<p>
	<div class="titleBox">LabCorp Test Results</div>
	<p>
<%
While not RSLabcorp.EOF
if (RSLabcorp("status") = "Ready") then
%>
<font color="green">Ready</font>
<%
else
%>
<font color="red">Sent</font>
<%

end if

if (RSLabcorp("transaction_id") <> "") then
%>
<a href="../../../labquestresult2/displaypdf.aspx?order_id=<%=RSLabcorp("ORDER_ID")%>"><%=RSLabcorp("LABCORP_TEST_NAME")%> - 
<%if (RSLabcorp("status") = "Ready") then%>
<%=RSLabcorp("last_updated")%>
<%else%>
<%=RSLabcorp("date_ordered")%>
<%end if%>
</a><br>
</font>
<%
else
%>
<a href="#" onclick="popScrollWindowMax('labcorp_results.asp?order_id=<%=RSLabcorp("ORDER_ID")%>&test_number=<%=RSLabcorp("LABCORP_TEST_NUMBER")%>','700','500')"><%=RSLabcorp("LABCORP_TEST_NAME")%> - <%=formatdatetime(RSLabcorp("date_ordered"),2)%></a>
         <%if (RSLabcorp("PDFOrderID") <> "") then%>
           <a href="../../../ConvertToPDF/Default.aspx?OrderID=<%=RSLabcorp("PDFOrderID")  %>"> <img src="../../../ConvertToPDF/img/pdf16.png" /></a>
        <% END IF %>
        <br>
<%
end if
RSLabcorp.MoveNext
wend
end if
%>


<%
if RSLabquest.RecordCount > 0 then
RSLabquest.MoveFirst
%>
	<p>
	<div class="titleBox">Lab Quest Results</div>
	<p>
<%
While not RSLabquest.EOF
%>
	<a href="../../../labquestresult2/displaypdf.aspx?order_id=<%=RSLabquest("ORDER_ID")%>"><%=RSLabquest("LABCORP_TEST_NAME")%> - <%=RSLabquest("last_updated")%></a><br>

<%
RSLabquest.MoveNext
wend
end if
%>

<%
if RSLabquestOrders.RecordCount > 0 then
RSLabquestOrders.MoveFirst
%>
	<p>
	<div class="titleBox">LabQuest Test Orders</div>
	<p>
<%
While not RSLabquestOrders.EOF
%>
<font color="green">Order:</font>
<a href="#" onclick="popScrollWindowMax('/practice/Hub/print_LabQuest.asp?order_id=<%=RSLabquestOrders("ORDER_ID")%>&print=0','800','600')"><%=RSLabquestOrders("ORDER_ID")%> - <%=RSLabquestOrders("LABCORP_TEST_NAME")%> - <%=RSLabquestOrders("date_ordered")%></a><br>

<%
RSLabquestOrders.MoveNext
wend
end if
%>

<%
if RSLabCorpOrders.RecordCount > 0 then
RSLabCorpOrders.MoveFirst
%>
	<p>
	<div class="titleBox">LabCorp Test Orders</div>
	<p>
<%
While not RSLabCorpOrders.EOF
%>
<font  color="green" class="st">Order:</font>
        <%if RSLabCorpOrders("status") = "New_Lab" then%>
            <a href="labcorp_incmpl.asp?order_id=<%=RSLabCorpOrders("ORDER_ID")%>"> <font class="st" color=red>(Incomplete) </font></a>&nbsp;
            <a href="#" onclick="delete_lc_order('<%=RSLabCorpOrders("ORDER_ID")%>')"><font class="st" color=red>delete</a></font>
         <% end if%>
<font class="st"><a href="#" onclick="popScrollWindowMax('/practice/Hub/print_LabCorp.asp?order_id=<%=RSLabCorpOrders("ORDER_ID")%>&print=0','800','600')"><%=RSLabCorpOrders("ORDER_ID")%> - <%=RSLabCorpOrders("LABCORP_TEST_NAME")%> - <%=formatdatetime(RSLabCorpOrders("date_ordered"),2)%></a><br></font>   
<%
RSLabCorpOrders.MoveNext
wend
end if
%>

<%
if RSMillenniumOrders.RecordCount > 0 then
RSMillenniumOrders.MoveFirst
%>
	<p>
	<div class="titleBox">Millennium Lab Orders</div>
	</p>
<%
While not RSMillenniumOrders.EOF
%>
<font color="green">Order:</font>
<a href="#" onclick="popScrollWindowMax('/MillenniumLabOrder/MillenniumLab_Requsition.aspx?order_id=<%=RSMillenniumOrders("ORDER_ID")%>&print=0','800','600')"><%=RSMillenniumOrders("ORDER_ID")%> - <%=RSMillenniumOrders("TEST_NAME")%> - <%=RSMillenniumOrders("date_ordered")%></a><br>

<%
RSMillenniumOrders.MoveNext
wend
end if
%>

<%
if RSCPLSEOrders.RecordCount > 0 then
RSCPLSEOrders.MoveFirst
%>
	<p>
	<div class="titleBox">CPLSE Lab Orders</div>
	</p>
<%
While not RSCPLSEOrders.EOF
%>
<font color="green">Order:</font>
<a href="#" onclick="popScrollWindowMax('/CPLSELabOrder/CPLSELab_Requsition.aspx?order_id=<%=RSCPLSEOrders("ORDER_ID")%>&print=0','800','600')"><%=RSCPLSEOrders("ORDER_ID")%> - <%=RSCPLSEOrders("TEST_NAME")%> - <%=RSCPLSEOrders("date_ordered")%></a><br>

<%
RSCPLSEOrders.MoveNext
wend
end if
%>


        
<%
if RSProPathOrders.RecordCount > 0 then
RSProPathOrders.MoveFirst
%>
	<p>
	<div class="titleBox">ProPath Lab Orders</div>
	</p>
<%
While not RSProPathOrders.EOF
%>
<font color="green">Order:</font>
<a href="#" onclick="popScrollWindowMax('/propathLabOrder/propathLab_Requsition.aspx?order_id=<%=RSProPathOrders("ORDER_ID")%>&print=0','800','600')"><%=RSProPathOrders("ORDER_ID")%> - <%=RSProPathOrders("TEST_NAME")%> - <%=RSProPathOrders("date_ordered")%></a><br>

<%
RSProPathOrders.MoveNext
wend
end if
%>


    
<%
if RSMiracaOrders.RecordCount > 0 then
RSMiracaOrders.MoveFirst
%>
	<p>
	<div class="titleBox">Miraca Lab Orders</div>
	</p>
<%
While not RSMiracaOrders.EOF
%>
<font color="green">Order:</font>
<a href="#" onclick="popScrollWindowMax('/miracalaborder/Requsition.aspx?order_id=<%=RSMiracaOrders("ORDER_ID")%>&pid=<%=session("pid")%>&print=0','800','600')"><%=RSMiracaOrders("ORDER_ID")%> - <%=RSMiracaOrders("TEST_NAME")%> - <%=RSMiracaOrders("date_ordered")%></a><br>

<%
RSMiracaOrders.MoveNext
wend
end if
%>




<%
if RSMiracaResults.RecordCount > 0 then
RSMiracaResults.MoveFirst
%>
	<p>
		<div class="titleBox">Miraca Lab Results</div>
	<p>
<%
    amendmentnumber = 1
While not RSMiracaResults.EOF
    dim uv
    uv = RSMiracaResults("UPDATE_VERSION")
    if ( uv > 0) then 
        Response.Write  " >> Update #"& uv&":" 
    else 
        Response.Write  "Result #"& amendmentnumber&": Order ID: " & RSMiracaResults("ORDER_ID")&" - "
        amendmentnumber = amendmentnumber +1
    end if 
%>

<a href="#" onclick="popScrollWindowMax('/miracalaborder/viewresult.aspx?mid=<%=RSMiracaResults("ORDER_ID")%>&amp;uv=<%=RSMiracaResults("UPDATE_VERSION")%>','800','600')"><font color="<%=font_color%>">Report</font></a>
    
<% if RSMiracaResults("is_pdf_included")=1 then %>
&nbsp;
<a href="/MiracaLabOrder/DisplayPDF.aspx?order_id=<%=RSMiracaResults("order_id")%>&amp;pid=<%=session("pid")%>"><font color="<%=font_color%>">PDF</font></a>
<% end if %>
- Received Date: <%=RSMiracaResults("rec_date")%> 
<br/>

<%      
RSMiracaResults.MoveNext
wend
%>

<%
end if
%>





<%
if RSNorthShoreOrders.RecordCount > 0 then
RSNorthShoreOrders.MoveFirst
%>
	<p>
	<div class="titleBox">NorthShore Lab Orders</div>
	<p>
<%
If not(RSNorthShoreOrders.EOF or RSNorthShoreOrders.BOF) then                     
While not RSNorthShoreOrders.EOF
if (RSNorthShoreOrders("status") = "Sent") then
%>
<font color="red">Order</font>
<a href="#" onclick="popScrollWindowMax('/practice/notes/display/NorthShoreLab/NorthShore_Print_LabTest.asp?order_id=<%=RSNorthShoreOrders("ORDER_ID")%>&order_data_id=<%=RSNorthShoreOrders("ORDER_DATA_ID")%>&test_number=<%=RSNorthShoreOrders("ORDER_CODE")%>&sendingFacilityId=<%=RSNorthShoreOrders("SENDINGFACILITY_ID") %>','700','500')"><%=RSNorthShoreOrders("TEST_DESC")%> - <%=RSNorthShoreOrders("date_ordered")%></a><br>
<%
end if
RSNorthShoreOrders.MoveNext
wend
end if
end if
%>
<%
if RSNorthShoreResults.RecordCount > 0 then
RSNorthShoreResults.MoveFirst
%>
	<p>
	<div class="titleBox">NorthShore Lab Results</div>
	<p>
<%
If not(RSNorthShoreResults.EOF or RSNorthShoreResults.BOF) then                     
While not RSNorthShoreResults.EOF
if (RSNorthShoreResults("status") = "Ready") then
%>
<font color="green">Result</font>
<a href="#" onclick="popScrollWindowMax('/practice/notes/display/NorthShoreLab/NorthShore_Results.asp?order_id=<%=RSNorthShoreResults("ORDER_ID")%>&order_data_id=<%=RSNorthShoreResults("ORDER_DATA_ID")%>&test_number=<%=RSNorthShoreResults("ORDER_CODE")%>&sendingFacilityId=<%=RSNorthShoreResults("SENDINGFACILITY_ID") %>','700','500')"><%=RSNorthShoreResults("TEST_DESC")%> - <%=RSNorthShoreResults("date_ordered")%></a><br>
<%
end if
RSNorthShoreResults.MoveNext
wend
end if
end if
%>



<%'----for Pathology orders only
If (doctor_id ="vleephy" or  doctor_id ="tsoares" or  doctor_id ="tfaleye" )then

Dim RSpathLab
Set RSpathLab = server.CreateObject("ADODB.RecordSet")

sqlQuery= "select chartid,visit_date from patienttable a, visit b where a.USERID=b.PATIENT_ID and b.patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"'"
RSpathLab.Open sqlQuery, CONN
chart_id=RSpathLab("chartid")
visit_date=RSpathLab("visit_date")
RSpathLab.Close

sqlQuery = "select distinct PATH_TEST_NAME,ORDER_ID, date_ordered from path_orders where doctor_id= '"& doctor_id &"' and chart_id = '"&chart_id&"'"
RSpathLab.CursorLocation = 3
RSpathLab.Open sqlQuery, CONN

	if RSpathLab.RecordCount > 0 then
	RSpathLab.MoveFirst
	%>
		<p>
		<div class="titleBox">Pathology Test Results</div>
		<p>
	<%
	While not RSpathLab.EOF
	%>
	<a href="#" onclick="popScrollWindowMax('pathlab_results.asp?order_id=<%=RSpathLab("ORDER_ID")%>','700','500')"><%=RSpathLab("PATH_TEST_NAME")%></a>&nbsp;-&nbsp;<%=RSpathLab("date_ordered")%><br>
	<%
	RSpathLab.MoveNext
	wend
	end if
	set RSpathLab=nothing
end if

'---end of pathlab orders
%>


<%'----for Dynacare results only
If (doctor_id ="thurowphy" )then

Dim RSDyna
Set RSDyna = server.CreateObject("ADODB.RecordSet")

sqlQuery= "select chartid,visit_date from patienttable a, visit b where a.USERID=b.PATIENT_ID and b.patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"'"
RSDyna.Open sqlQuery, CONN
chart_id=RSDyna("chartid")
visit_date=RSDyna("visit_date")
RSDyna.Close

sqlQuery = "select distinct PATH_TEST_NAME,ORDER_ID, visit_date from path_orders where doctor_id= '"& doctor_id &"' and chart_id = '"&chart_id&"' order by visit_date desc"
RSDyna.CursorLocation = 3
RSDyna.Open sqlQuery, CONN

	if RSDyna.RecordCount > 0 then
	RSDyna.MoveFirst
	%>
		<p>
		<div class="titleBox">Dynacare Lab Results</div>
		<p>
	<%
	While not RSDyna.EOF
	%>
	<a href="#" onclick="popScrollWindowMax('pathlab_results.asp?order_id=<%=RSDyna("ORDER_ID")%>','700','500')"><%=RSDyna("PATH_TEST_NAME")%></a>&nbsp;-&nbsp;<%=formatdatetime(RSDyna("visit_date"),2)%><br>
	<%
	RSDyna.MoveNext
	wend
	end if
	set RSDyna=nothing
end if
'---end of dynacare orders
%>


<%
'----for Millennium Lab Results only
if RSMillenniumResults.RecordCount > 0 then
RSMillenniumResults.MoveFirst
%>
	<p>
	<div class="titleBox">Millennium Lab Results</div>
	<p>
<%
While not RSMillenniumResults.EOF
if (RSMillenniumResults("status") = "Ready") then
%>
<font color="green">Ready</font>
<%
end if

%>
    <a href="../../../MillenniumLabOrder/DisplayPDF.aspx?order_id=<%=RSMillenniumResults("ORDER_ID")%>"><%=RSMillenniumResults("TEST_NAME")%> - 
    <%if (RSMillenniumResults("status") = "Ready") then%>
        <%=RSMillenniumResults("last_updated")%>
    <%end if%>
    </a><br />
<%
RSMillenniumResults.MoveNext
wend
end if
'---end of Millennium Lab Results
%>

<%
if RSLabManual.RecordCount > 0 then
RSLabManual.MoveFirst
%>
	<p>
	<div class="titleBox">Manual Lab Results</div>
	<p>
<%
While not RSLabManual.EOF
%>
    <a href="#" onclick="popScrollWindowMax('/practice/notes/display/labcorp_results.asp?order_id=<%=RSLabManual("ORDER_ID")%>&amp;test_number=<%=RSLabManual("LABCORP_TEST_NUMBER")%>','700','500')"><font color="<%=font_color%>"><%=RSLabManual("LABCORP_TEST_NAME")%></font></a>
         - <a href="#" onclick="delete_mlab('<%=RSLabManual("order_id")%>')"><font class="st" color=red>delete</font></a><br>  
<%
RSLabManual.MoveNext
wend
end if
%>


<%
if RSLabImported.RecordCount > 0 then
RSLabImported.MoveFirst
%>
	<p>
	<div class="titleBox">Imported Lab Results</div>
	<p>
<%
    amendmentnumber = 1
While not RSLabImported.EOF
If session("pid") <> "VIR1232" then
    Response.Write  "Result #"& amendmentnumber&":" 
    %>
    
<%
end if       
%>

<%If session("pid") <> "VIR1232" then %>
<a href="#" onclick="popScrollWindowMax('/ImportLabResults/viewconsolidatedreport.aspx?mid=<%=RSLabImported("MASTER_ID")%>&amp;pid=<%=session("pid")%>','800','600')"><font color="<%=font_color%>"><%=RSLabImported("MASTER_ID")%></font></a>
  - Received Date: <%=RSLabImported("created_date")%> 
   <% if RSLabImported("is_comment_saved")=1 then %>
        <a href="#" onclick="popScrollWindowMax('/ImportLabResults/Comments.aspx?mid=<%=RSLabImported("MASTER_ID")%>','730','360')" class="com1"><font color="<%=font_color%>">Comments</font></a>
    <% else %>
        <a href="#" onclick="popScrollWindowMax('/ImportLabResults/Comments.aspx?mid=<%=RSLabImported("MASTER_ID")%>','730','360')" ><font color="<%=font_color%>">Comments</font></a>
    <% end if %> 
<% end if %>
<% if RSLabImported("is_pdf_included")=1 then %>
PDF Report:
<a href="/ImportLabResults/displaypdf.aspx?mid=<%=RSLabImported("MASTER_ID")%>&amp;pid=<%=session("pid")%>"><font color="<%=font_color%>">PDF</font></a>
  - Received Date: <%=RSLabImported("created_date")%>
  -
    <% if RSLabImported("is_comment_saved")=1 then %>
        <a href="#" onclick="popScrollWindowMax('/ImportLabResults/Comments.aspx?mid=<%=RSLabImported("MASTER_ID")%>','730','360')" class="com1"><font color="<%=font_color%>">Comments</font></a>
    <% else %>
        <a href="#" onclick="popScrollWindowMax('/ImportLabResults/Comments.aspx?mid=<%=RSLabImported("MASTER_ID")%>','730','360')" ><font color="<%=font_color%>">Comments</font></a>
    <% end if %>
<% end if %>

<br/>
<%
RSLabImported.MoveNext
    amendmentnumber = amendmentnumber +1
wend
%>

<%
end if
%>

        	   
<%
if RSCPLSEResults.RecordCount > 0 then
RSCPLSEResults.MoveFirst
%>
	<p>
	<div class="titleBox">CPLSE Lab Results</div>
	<p>
<%
	amendmentnumber = 1
While not RSCPLSEResults.EOF
if (RSCPLSEResults("status") = "Ready") then
%>
<font color="green">Ready</font>
<%
end if     
%>

<a href="#" onclick="popScrollWindowMax('/CPLSELabOrder/viewresult.aspx?mid=<%=RSCPLSEResults("ORDER_ID")%>','800','600')"><font color="<%=font_color%>"><%=RSCPLSEResults("TEST_NAME")%></font>
	<%if (RSCPLSEResults("status") = "Ready") then%>
	- <%=RSCPLSEResults("last_updated")%>
	<%end if%></a>
<br />
<%
RSCPLSEResults.MoveNext
	amendmentnumber = amendmentnumber +1
wend
end if
%>


        
<%
if RSProPathResults.RecordCount > 0 then
RSProPathResults.MoveFirst
%>
	<p>
	<div class="titleBox">ProPath Lab Results</div>
	<p>
<%
	amendmentnumber = 1
While not RSProPathResults.EOF
if (RSProPathResults("status") = "Ready") then
%>
<font color="green">Ready</font>
<%
end if     
%>
   	<a href="../../../propathLabOrder/DisplayPDF.aspx?order_id=<%=RSProPathResults("ORDER_ID")%>"><%=RSProPathResults("TEST_NAME")%> -
	<%if (RSProPathResults("status") = "Ready") then%>
		<%=RSProPathResults("last_updated")%>
	<%end if%>
<br />
<%
RSProPathResults.MoveNext
	amendmentnumber = amendmentnumber +1
wend
end if
%>





<%
if RSPaper.RecordCount > 0 then
RSPaper.MoveFirst
%>
	<p>
	<div class="titleBox">Lab Orders - Paper</div>
	<p>
<%
While not RSPaper.EOF
%>
   
<font class="st"> <%=RSPaper("LAB_TEST_NAME")%> - <%=RSPaper("notes")%> - <%=formatdatetime(RSPaper("date_ordered"),2)%></font>
        - <a href="#" onclick="delete_paperlab('<%=RSPaper("order_id")%>')"><font class="st" color=red>delete</font></a><br>   
<%
RSPaper.MoveNext
wend
end if
%>


<%

if cl_ins1 <>"" then
%>
<p>
<div class="titleBox">Clinical Instructions</div>
<p>
<%
response.Write cl_ins&"<br>"
end if
%>

<%

set RS=nothing
set RSNoteData=nothing
set RSPastMedicalHx=nothing
set RSSocialHx=nothing
set RSSurgicalHx =nothing
set RSFamilyHx=nothing
set RSFileData=nothing
set RSNoteAmend=nothing
set RSPatExams=nothing
set RSUploadData=nothing
set RSNotes=nothing
set RSLabcorp=nothing
set RSLabquestOrders=nothing
set RSLabCorpOrders=nothing
set RSLabquest=nothing
set RSLabManual=nothing
set RSLabImported=nothing
set RSMillenniumOrders=nothing
set RSCPLSEOrders=nothing
set RSMillenniumResults=nothing
set RSProPathOrders=nothing
set RSCPLSEResults=nothing
set RSProPathResults=nothing
set RSNorthShoreOrders=nothing
set RSNorthShoreResults=nothing
set RSDiagTests=nothing
set RSDiagTST=nothing
set RSssForm=nothing
set RSAEForm=nothing
set RSExForm=nothing
set RSRefForm=nothing
set sum_adoDb=nothing
set sum_adoDb2=nothing
set sum_adoDbRef=nothing
set sum_adoDbRef2=nothing
set RShpi=nothing
set RSProblem=nothing
set RSAllergy=nothing
set RSImm=nothing
set RSMeds=nothing
set RSOpth=nothing
set RSSubSpec=nothing
set RSOpth=nothing
set RSVitals=nothing
set RSVitalVisual=nothing
set RSVitalIP=nothing
set RSVitalMR=nothing
set RSVitalP=nothing
set RSAss=nothing
set RSMeds=nothing
set res=nothing
set res1=nothing
set RSpathLab=nothing
set RSDyna=nothing

%>

</div>
</body>
</html>
<%
else
%>
<h3>General Error: Visit key missing.</h3>
<%
end if
CONN.close()

%>