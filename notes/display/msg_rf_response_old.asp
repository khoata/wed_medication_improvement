
<!--#include file="../../../library/connection.inc.asp"-->

<html>
<head>
<title>Refill Response</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>

<script language="javascript">
function chk_refrsp()
{

var radio_choice = false;
var rad_val;

for (counter = 0; counter < document.f1.Response_Type.length; counter++)
{
	if (document.f1.Response_Type[counter].checked)
	{
	rad_val = document.f1.Response_Type[counter].value;
	radio_choice = true; 
	}
}
if (!radio_choice)
{
alert("Please select your response to the Refill.")
return (false);
}

if(rad_val=='ApprovedWithChanges' && document.f1.Response_Note.value=='')
{	alert('Please Enter the Notes to Approve the Refill with Changes');
		document.f1.Response_Note.select();
		return false;
}	

if(rad_val=='Denied' && document.f1.Response_Reason_Code.selectedIndex==0)
{	alert('Please select the Denial Reason from the dropdown menu and enter the notes');
		document.f1.Response_Reason_Code.focus();
		return false;
}
}





function checklength(i)
{
var txt;
txt=document.f1.Response_Note.value;
n=txt.length;
if (n>i) 
{
alert('Only 70 characters are allowed for this field');
document.f1.Response_Note.value=text1;
return;
}
text1=document.f1.Response_Note.value;
}

</script>


</head>
<body>
<body>

<%
drug_id = Request.QueryString("drug_id")
visit_key = Request.QueryString("visit_key")
patient_id = Request.QueryString("patient_id")

set rs1 = server.CreateObject("ADODB.Recordset")
strSQL="select drug_name,PRESCRIBED_BY from notes_medications where drug_id='" & drug_id & "'"

rs1.open strSQL,con		
If not (rs1.EOF or rs1.BOF) then
	drug_name=rs1("drug_name")
	Physician_ID=rs1("PRESCRIBED_BY")
End if
rs1.close

'check if there is any change requeted from the pharmacy



'get the pharmacy Id
strSQL="select PharmacyID from SS_MESSAGE where messagetype='RefillRequest' and drugid='" & drug_id & "'"
rs1.open strSQL,con		
If not (rs1.EOF or rs1.BOF) then
	NCPDPID=rs1("PharmacyID")
End if
rs1.close

%>
<p align=center><b><u>Refill Response</u></b></p>
<br>

<form action="http://doctor2k3/surescriptInterface/default.aspx" method="post" name="f1" onsubmit="return chk_refrsp()">

<input type="hidden" name="selected_mid" value="<%=drug_id%>">
<input type="hidden" name="Visit_Key" value="<%=visit_key%>">
<input type="hidden" name="Patient_Id" value="<%=patient_id%>">
<input type="hidden" name="MsgType" value="RefillResponse">
<input type="hidden" name="NCPDPID" value="<%=NCPDPID%>">
<input type="hidden" name="Physician_ID" value="<%=Physician_ID%>">

<table border="0"  width="100%">
			<tr><td><b>Medication:&nbsp;<%=drug_name%><b></td></tr>
				
			<tr><td> <input type=radio name="Response_Type" value="Approved">Approve</td></tr>			
			<tr><td> <input type=radio name="Response_Type" value="ApprovedWithChanges">Approved With Changes</td></tr>
			<tr><td> <input type=radio name="Response_Type" value="Denied">Deny&nbsp;
			<select name="Response_Reason_Code">
			<option value="">Select Denial Reason</option>
				<%
				rs1.open "SELECT * FROM SS_DENIALREASON", con
				while not rs1.eof
				%>
					<option value="<%=rs1("DENIAL_CODE")%>"><%=rs1("DENIAL_desc")%></option>
				<%
				 rs1.movenext
				wend
				rs1.close
				%>
				</select>			
			</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td><b>Note:&nbsp;</b>
					<textarea name="Response_Note" rows="2" cols="35" onkeydown="javascript:checklength(69)"></textarea>
			</td></tr>

</table>
<br><br><br>
<center>
<input type=submit value="Submit" id=a1 name=a1>			
<input type=button value="Close Window" onclick="javascript:window.close();" id=button2 name=button2>

</form>
</center>			
</body>
</html>
