<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../../../library/clsEPCSAuditLog.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%
dim epcsAuditLogObj
set epcsAuditLogObj = new clsEPCSAuditLog

dim auditObj
set auditObj = new clsAudit
rx_type = trim(Request.Form("rx_type"))
set CMD1 = Server.CreateObject("ADODB.Command")
CMD1.ActiveConnection = CONN

set CMD = Server.CreateObject("ADODB.Command")
CMD.ActiveConnection = CONN

dim RSSeq
set RSSeq = Server.CreateObject("ADODB.Recordset")

msgtype=Request("msgtype")

If msgtype<>"" and msgtype="RefillResponse" then
	Response.Write "<script>parent.opener.location.reload(true);</script>"
	Response.Write "<script>self.close();</script>"
	Response.end
end if

If msgtype<>"" and msgtype<>"NewRX" and msgtype<>"CancelRx" then
	Response.Write "<script>self.close();</script>"
	Response.end
end if

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id_hidden")
PhysicianId = Session("user")
show = Request("show")
if (Request("other_meds") <> "") then
	Session("other_meds") = Request("other_meds")
end if

other_meds = Session("other_meds")

'response.write "aaaa="&other_meds

call auditObj.LogActivity("Accessing Medications","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)

set RS_ep = server.createobject("adodb.recordset")

RS_ep.Open "select * from ss_accounts order by acc_id",conn
ep_option=false

while not RS_ep.EOF and flex_option = false
	if RS_ep("Acc_ID") = session("pid") then
		ep_option=true
	end if
	RS_ep.MoveNext
wend
RS_ep.Close

if session("docid")="" then session("docid")=PhysicianId

RS_ep.Open "select spi, bitand(cast(nvl(ss_service_level,'0') as varchar2(20)), 1) as newrx from doctortable where docuser='" & session("docid") & "'",conn
if (RS_ep.EOF or isnull(RS_ep("spi")) or (not (RS_ep("newrx") = "1"))) then
    ep_option = false
end if
RS_ep.Close

set RS_ep=nothing

If ep_option=true then
	dis1=""
else
	dis1="disabled"
end if

if Request("purge") <> "" then

  dim CMD
  set CMD = Server.CreateObject("ADODB.Command")
  set RS_m = server.createobject("adodb.recordset")

  RS_m.Open "select drug_name, formulation from  notes_medications WHERE drug_id='" & drug_id & "'",conn
  if not (RS_m.eof or RS_m.bof) then
    drug_deleted = RS_m("drug_name")&"-"&RS_m("formulation")
  end if
  RS_m.close

  CMD.ActiveConnection = CONN

  sqlQuery = "DELETE FROM notes_medications WHERE drug_id='" & drug_id & "'"
  CMD.CommandText = sqlQuery
  CMD.Execute
  call auditObj.LogActivity("Deleted a Medication","Drug_id-"+Cstr(drug_deleted),"",Cstr(patient_id),Cstr(visit_key),SYSDATE)
end if

if Request("NoMed") <> "" then

  set CMD = Server.CreateObject("ADODB.Command")
  CMD.ActiveConnection = CONN

  sqlQuery = "SELECT notes_medications_drug_id.nextval as drug_id_nomed FROM DUAL"
  CMD.CommandText = sqlQuery
  set RS_nomed = CMD.Execute
  drug_id_nomed = RS_nomed("drug_id_nomed").value
  RS_nomed.Close
  set RS_nomed = nothing

  prescribed_by = session("user")
  start_date=date()
  sqlQuery = "INSERT INTO notes_medications " &_
              "(drug_id,visit_key,drug_name,formulation,prescribed_by,prescribed_date,patient_id,ndc_code,status,refill,ss_flag) " &_
              "VALUES " &_
              "('" & drug_id_nomed & "','" & visit_key & "','No Medication Prescribed',' ','" & prescribed_by & "',to_date('" & start_date & "','MM/DD/YYYY'),'" & patient_id & "','0000000000','Current',0,1)"

  'response.Write sqlQuery
  CMD.CommandText = sqlQuery
  CMD.Execute

end if


if Request("ListR") <> "" then

  set CMD = Server.CreateObject("ADODB.Command")
  CMD.ActiveConnection = CONN

  rev_by = session("user")  
  sqlQuery = "UPDATE VISIT SET MED_REVIEW_DATE =sysdate , MED_REVIEW_WHO = '" & rev_by & "' WHERE visit_key ='" & visit_key & "'"

  'response.Write sqlQuery
  CMD.CommandText = sqlQuery
  CMD.Execute

end if



if Request("addtocurrentrx") <> "" then
	sqlQuery = "SELECT status FROM notes_medications WHERE drug_id='" & drug_id & "'"
	CMD1.CommandText = sqlQuery

	RSSeq = CMD1.Execute
	sqlQuery = "UPDATE notes_medications SET status='Current', stop_date =  NULL WHERE drug_id='" & drug_id & "'"
	CMD.CommandText = sqlQuery
	CMD.Execute

	call auditObj.LogActivity("Added Medication to Current RX - "+Cstr(drug_id),RSSeq("status").value,"Current",Cstr(patient_id),Cstr(visit_key),SYSDATE)
end if



if Request("stop") <> "" then
  set CMD = Server.CreateObject("ADODB.Command")

  CMD.ActiveConnection = CONN

  sqlQuery = "SELECT status, ndc_code,drug_name,formulation FROM notes_medications WHERE drug_id='" & drug_id & "'"
  CMD1.CommandText = sqlQuery
  RSSeq = CMD1.Execute
  if (other_meds = "Current") then
	sqlQuery = "UPDATE notes_medications SET status='Stopped', stop_date = to_date('" & date() & "','MM/DD/YYYY') WHERE drug_id='" & drug_id & "'"
  else
	sqlQuery = "UPDATE notes_medications SET status='O Stopped', stop_date = to_date('" & date() & "','MM/DD/YYYY') WHERE drug_id='" & drug_id & "'"
  end if
  CMD.CommandText = sqlQuery
  CMD.Execute

 call auditObj.LogActivity("Modified a Medications STATUS with ID - "+Cstr(drug_id),RSSeq("status").value,"Stopped",Cstr(patient_id),Cstr(visit_key),SYSDATE)
  'Response.Redirect("/practice/notes/display/immunization_history.asp?visit_key=" & visit_key & "&patient_id=" & patient_id)
  'RSSeq.close
  
  '---START: Add to EPCS_Audit_Log
  set rs = server.CreateObject("ADODB.Recordset")
  strSQL = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and ndc_code = '" & RSSeq("ndc_code") & "'"
  rs.open strSQL,con
  if not (rs.eof or rs.bof) then

    detailAuditLog = "- Drug Name: " & RSSeq("drug_name") & "<br/>" &_
                     "- Formulation: " & RSSeq("formulation") & "<br/>"

    call epcsAuditLogObj.LogEPCSActivity("Controlled Substance Prescription","Stop CS Medication",detailAuditLog,"SUCCESS")

  end if
  rs.close
  'END

end if

if Request("start") <> "" then


  sqlQuery = "SELECT status, ndc_code, drug_name, formulation FROM notes_medications WHERE drug_id='" & drug_id & "'"
  CMD1.CommandText = sqlQuery

  RSSeq = CMD1.Execute
  if (other_meds = "Current") then
	sqlQuery = "UPDATE notes_medications SET status='Current', stop_date =  NULL WHERE drug_id='" & drug_id & "'"
  else
	sqlQuery = "UPDATE notes_medications SET status='O Current', stop_date =  NULL WHERE drug_id='" & drug_id & "'"
  end if
  CMD.CommandText = sqlQuery
  CMD.Execute
  call auditObj.LogActivity("Modified a Medications status with ID - "+Cstr(drug_id),RSSeq("status").value,"Current",Cstr(patient_id),Cstr(visit_key),SYSDATE)
  'Response.Redirect("/practice/notes/display/immunization_history.asp?visit_key=" & visit_key & "&patient_id=" & patient_id)
  'RSSeq.Close
  
  '---START: Add to EPCS_Audit_Log
  set rs = server.CreateObject("ADODB.Recordset")
  strSQL = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and ndc_code = '" & RSSeq("ndc_code") & "'"
  rs.open strSQL,con
  if not (rs.eof or rs.bof) then

    detailAuditLog = "- Drug Name: " & RSSeq("drug_name") & "<br/>" &_
                     "- Formulation: " & RSSeq("formulation") & "<br/>"

    call epcsAuditLogObj.LogEPCSActivity("Controlled Substance Prescription","Restart CS Medication",detailAuditLog,"SUCCESS")

  end if
  rs.close
  'END
end if

if Request("refill") <> "" then

	sqlQuery = "SELECT notes FROM notes_medications WHERE drug_id='" & drug_id & "'"
	CMD1.CommandText = sqlQuery

	RSSeq = CMD1.Execute

	set CMD = server.CreateObject("ADODB.Command")
	CMD.ActiveConnection = CONN

	refill_date=CalculateLocalTime(now)

    sqlQuery = "Update notes_medications set notes = notes || 'Refilled on "& refill_date &";' WHERE drug_id='"& drug_id &"'"
	CMD.CommandText = sqlQuery
	CMD.Execute
	call auditObj.LogActivity("Modified a Medications status with ID - "+Cstr(drug_id),RSSeq("notes").value,Cstr(notes)+ "Refilled on: "+Cstr(Cdate(sysdate)),Cstr(patient_id),Cstr(visit_key),SYSDATE)
	'RSSeq.Close
end if

'To get the list of medications which are prescribed from Other Sources
sqlQuery = "SELECT * FROM notes_medications " &_
			" WHERE status ='O Current' " &_
			" and patient_id = '" & patient_id & "'"

set RS_re = server.createobject("adodb.recordset")
RS_re.open sqlQuery,con
if not RS_re.eof then
	button_reconcile = 1
else
	'button_reconcile = 0
    button_reconcile = 1
end if
RS_re.Close

'To get the list of medications which are Reconciled
sqlQuery = "SELECT * FROM notes_medications " &_
			" WHERE status ='Reconciled'" &_
			" and patient_id = '" & patient_id & "'"

set RS_re = server.createobject("adodb.recordset")
RS_re.open sqlQuery,con
if not RS_re.eof then
	preReconcileList = 1
else
	preReconcileList = 0

end if
RS_re.Close

'get drug interaction severity info
RS_re.Open "select minor, moderate, major,critical,noncritical from drug_interactions_severity where physician_id = '"& session("user") &"'",CONN
if not RS_re.EOF then
	if RS_re("minor") <> "" then
		severity = severity & "1,"
	end if
	if RS_re("moderate") <> "" then
		severity = severity & "2,"
	end if
	if RS_re("major") <> "" then
		severity = severity & "3,"
	end if
	if RS_re("Critical") <> "" then
		A_Sev = A_Sev & "'Critical',"
	end if
	if RS_re("Noncritical") <> "" then
		A_Sev = A_Sev & "'Non-Critical',"
	end if
end if
RS_re.Close
if severity = "" then
	severity = "1,2,3"
end if

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<script language="JavaScript" src="../../../library/calendar.js"></script>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script language="javascript">
    function editDrug() {

        var value = document.getElementById('drug_id_hidden').value;
        if (value != "" && value != null) {
            getFrameByName(top, 'leftNoteFrameTop').gotoBottomBar('/practice/notes/edit/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=' + value);
        } else {
            alert("Please select a prescription to edit.")
        }
    }

    function newDrug() {

        var value = document.getElementById('drug_id_hidden').value;
        value = 123;
        if (value != "" && value != null) {
            getFrameByName(top, 'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=' + value);
        } else {
            alert("Please select a prescription to edit.")
        }
    }

    function newDrug_bh() {

        var value = document.getElementById('drug_id_hidden').value;
        value = 123;
        if (value != "" && value != null) {
            getFrameByName(top, 'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/medication_bh.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=' + value);
        } else {
            alert("Please select a prescription to edit.")
        }
    }

    function checkDrugID() {
        var value = document.getElementById('drug_id_hidden').value;

        if (value != "" && value != null) {
            return true;
        } else {
            alert("Please select a prescription.")
            return false;
        }
    }

    function addtoRx() {

        var value = document.getElementById('drug_id_hidden').value;

        if (value != "" && value != null) {
            getFrameByName(top, 'rightNoteFrame').gotoBottomBar('/practice/notes/edit/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&drug_id=' + value);
        } else {
            alert("Please select a prescription to add to Current RX.")
        }
    }

    function printmedhx() {
        var all;
        all = 0;

        if (confirm('Do you want to print All medications history (Including Medications Stopped)?')) {
            all = 1;
        }
        popScrollWindow('/practice/notes/display/med_hx_print.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&all=' + all, 700, 400);
    }

</script>

<script LANGUAGE="JavaScript" SRC="/library/OverLibNew/overlib.js"></script>
<script type="text/javascript"  language="javascript">
    function CheckDates() {
        var valuefrom = document.getElementById("dtfrom").value;
        var valueto = document.getElementById("dtto").value;


        if ((valuefrom == "") & (valueto == "")) {
            return true;
        }
        else {
            if (valuefrom == "" | valueto == "") {
                alert("Please enter both dates.")
                return false;
            }
        }

    }

    function runScript(e) {
        if (e.keyCode == 13) {
            var tb = document.getElementById("scriptBox");
            eval(tb.value);
            var tb1 = document.getElementById("scriptBox1");
            eval(tb1.value);
            return false;
        }
    }

</script>
</head>
<body>
<%

dtfrom=trim(Request.Form("dtfrom"))
dtto=trim(Request.Form("dtto"))
%>
<%
'response.write "aaaa1="&other_meds
%>

<% if (other_meds = "Current") then %>
<div class="titleBox">Current Medications</div>
<% else %>
<div class="titleBox">Medications from Other Sources</div>
<%end if %>
<p>

<form action="medication_history.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>" method="post" name="medhist" id="medhist">
<input type="hidden" name="drug_id_hidden" id="drug_id_hidden" value>

        <%   dim status6 
                    status6 = "" %>
             <% set RS20 = Server.CreateObject("ADODB.Recordset")
               RS20.Open " SELECT SURESCRIPTS_ACCESS,docuser  from  doctortable where SURESCRIPTS_ACCESS = 1 and docuser = '" & SQLFixUp(session("user")) & "' ", CONN %>
                     <%while not RS20.EOF%>
             <%if RS20("docuser") = session("user") then
                status6="1"
                else
                status6=""
                end if                  
                                                 
             %>                                                
             <%  RS20.MoveNext
                 wend
                 set RS20=nothing                        
             %> 

      <%if status6= "" then
                status6="0"                
                end if                           
             %> 
       




<p>

    
<% if status6 <> "0" then %>
          <input type="button" onclick="newDrug_bh();" value="New" id="buttonNew" name="buttonNew">&nbsp;        
<% end if %>


<% if status6 = "0" then %>
          <input type="button" onclick="newDrug();" value="New" id="buttonNew" name="buttonNew">&nbsp;
<% end if %>


<input type="submit" name="refill" onclick="return checkDrugID();return confirm('Are you sure you wish to refill this prescription?');" value="Refill" id="buttonRefill" name="buttonRefill">&nbsp;
<input type="button" onclick="editDrug();" value="Edit" id="buttonEdit" name="buttonEdit">&nbsp;
<input type="submit" name="stop" value="Stop Medication" onclick="return checkDrugID();return confirm('Are you sure you wish to stop this prescription?');">&nbsp;
<input type="submit" name="purge" value="Purge Medication" onclick="return checkDrugID();return confirm('Are you sure you want to purge this prescription?');">&nbsp;
<input type="submit" name="start" value="(Re)start Medication" onclick="return checkDrugID();return confirm('Are you sure you wish to (re)start this prescription?');">&nbsp;
<input type="button" onclick="document.location='/practice/notes/display/medication_printrx.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'" value="Print Rx..." id="buttonPrintRx" name="buttonPrintRx">&nbsp;

<% set RS9 = Server.CreateObject("ADODB.Recordset")%>
<%RS9.Open "SELECT ACC_ID  FROM FaxSending_Rights WHERE ACC_ID = '" & SQLFixUp(session("pid")) & "' ", CONN%>
                   <%while not RS9.EOF%>
                                                <%if RS9("ACC_ID") = session("pid") then
                                                              status1="selected"
                                                       else
                                                              status1=""
                                                       end if
                                                %>
                          <%
                                                RS9.MoveNext
                                                wend
                                                set rs9=nothing
                          %>

                   <% if status1 <> "" then %>
                   <input type="button" onclick="document.location='/practice/notes/display/medication_faxrx.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'" value="Fax Rx..." id="button1" name="buttonPrintRx">&nbsp;
<% end if %>




<input type="button" onclick="printmedhx();" value="Print Med Hx" id="button8" name="button8">&nbsp;
<% if session("role") = "physician" then %>
	<input type="button" onclick="document.location='/practice/notes/display/medication_order_rx.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'" value="Order Prescription" id="buttonOrder" name="buttonOrder" >
	<input type="button" onclick="document.location='/practice/notes/display/medication_order_rx_m.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'" value="Order Multiple Prescription" id="buttonOrder" name="buttonOrder" >
	<input type="button" onclick="document.location='/practice/notes/display/medication_cancel_rx.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'" value="Cancel Prescription" id="buttonOrder" name="buttonOrder" >
<% end if %>
<% if session("pid") = "KEN1000" or  session("pid") = "BIN1233" then %>

<% end if %>
<input type="button" onclick="document.location='/DrugFunctions/DrugToDrug.aspx?Drugs='+document.getElementById('multum_drug_id_list').value+'&amp;SearchBy=ID&amp;Visit_Key=<%=visit_key%>&amp;Severity=<%=severity%>&amp;Allergies='+document.getElementById('allergies').value+'&amp;Allergy_Sev='+document.getElementById('allergy_sev').value+'&amp;Allergy_Reac='+document.getElementById('allergy_reac').value" value="Run Drug Validations" id="buttonRun" name="buttonRun">&nbsp;

<input type="submit" name="NoMed" value="No Medication Prescribed">&nbsp;

<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').toggleTop();document.location='/practice/notes/display/note.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Cancel" id="buttonCancel" name="buttonCancel">&nbsp;
<%if (button_reconcile) then%>
	<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').toggleTop();document.location='/practice/notes/display/reconcile.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Reconcile" id="buttonReconcile" name="buttonReconcile">&nbsp;
<%end if%>
<%if (preReconcileList1) then%>
	<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').toggleTop();document.location='/practice/notes/display/prvReconcileList.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Previous Reconciled Lists" id="buttonPrvReconcile" name="buttonPrvReconcile">&nbsp;
<%end if%>
<input type="submit" name="ListR" value="Med List Reviewed">

<%
if dtfrom <>"" and dtto <>""  then
	    dtfrom_1	= "to_date('"& dtfrom &"','mm/dd/yyyy')"
        dtto_1	= "to_date('"& dtto &"','mm/dd/yyyy')"
        date_clause=" and  a.prescribed_date between " & dtfrom_1 &" and "& dtto_1
elseif  1=1 then
	    dtfrom=""     'dateadd("m",-24,Date())
        dtto=""
        dtfrom_1	= "to_date('"& dtfrom &"','mm/dd/yyyy')"
        dtto_1	= "to_date('"& dtto &"','mm/dd/yyyy')"
        'date_clause=" a.prescribed_date between " & dtfrom_1 &" and "& dtto_1
        date_clause =""
end if
%>

<p>

  <table border="0"  width="100%" bgcolor="#CCCCCC" width="100%">
  <tr>
  <td>
  &nbsp;&nbsp;From Date Started &nbsp;<input type=text size=9 maxlength=10 name=dtfrom id="scriptBox" value="<%=dtfrom%>" onkeypress="return runScript(event)">
  <a href="#" onClick="getCalendarFor(document.medhist.dtfrom); return false;"> <img src="/images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></a>
  &nbsp;&nbsp;To Date Started &nbsp;<input id="scriptBox1" type=text size=9 maxlength=10 name=dtto value="<%=dtto%>" onkeypress="return runScript(event)">
<a href="#" onClick="getCalendarFor(document.medhist.dtto); return false;"> <img src="/images/global/calendar.gif" border="0" WIDTH="26" HEIGHT="16"></a>
&nbsp;Medication Status&nbsp;

<%
if other_meds="Current" then
    if rx_type="Current" then
	    sel = " and a.status = 'Current' "
        sel1=" selected"
     elseif rx_type="Stopped" then
	     sel = " and a.status = 'Stopped' "
        sel2=" selected"
     elseif rx_type="All"  then
	    sel=" and a.status not in ('O Current', 'O Stopped') "
        sel3=" selected"
    else
        sel = " and a.status = 'Current' "
        sel1=" selected"
     end if
end if
%>


<%
if other_meds="Other" then
    if rx_type="Current" then
	    sel = " and a.status = 'O Current' "
        sel1=" selected"
     elseif rx_type="Stopped" then
	     sel = " and a.status = 'O Stopped' "
        sel2=" selected"
     elseif rx_type="All"  then
	    sel=" and a.status not in ('Current', 'Stopped') "
        sel3=" selected"
    else
        sel = " and a.status = 'O Current' "
        sel1=" selected"
     end if
end if
%>


<select name = "rx_type">
<option value = "Current" <%=sel1%>>Current</option>
<option  value="Stopped" <%=sel2%>>Stopped</option>
<option  value="All"<%=sel3%> >All</option>
</select>

&nbsp;&nbsp;<input type="image" src="/images/global/icon_search.gif" border="0" width="18" height="18" id="image1" name="image1" onclick="return CheckDates();">
</td>

  </tr>
  <table>

<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">

  <tr bgcolor="#3366CC">
    <td colspan="1"><font color="white"><b>Date Started</td>
    <td colspan="1"><font color="white"><b>Date Stopped</td>
    <td colspan="1"><font color="white"><b>Date Modified</td>
    <td><font color="white"><b>Prescription</td>
    <% if (other_meds = "Current") then %>
    <td><font color="white"><b>Note to Pharmacist</td>
	<%else%>
	<td><font color="white"><b>Note</td>
	<%end if%>
    <td><font color="white"><b>Medication Status</td>
    <td><font color="white"><b>Validation Errors</td>
    <td><font color="red"><b>Current Order Status</td>
  </tr>


<%

set res1 = server.CreateObject("ADODB.Recordset")
set rs1 = server.CreateObject("ADODB.Recordset")

if A_Sev <> "" then
	Sev_Condition = " AND Severity in (" & left(A_Sev, len(A_Sev)-1) & ")"
end if
strQuery = "select * from notes_allergy where patient_id = '"& patient_id &"' and date_resolved is null and Allergy_Type='class'" & Sev_Condition
'Response.Write strQuery
rs1.Open strQuery,con
if not (rs1.EOF or rs1.BOF) then
	while not rs1.EOF
		Allergies = Allergies & rs1("allergy_text") & ","
		Allergy_Sev = Allergy_Sev & rs1("severity") & "|"
		Allergy_Reac = Allergy_Reac & rs1("Notes") & "|"
		rs1.MoveNext
	wend
end if
rs1.Close

if show = "new" or session("show") = "new" then
	sqlQuery = "Select a.multum_drug_id,a.status,a.drug_name,a.multum_brand_code,a.notes,a.ndc_code,a.drug_id,a.prescribed_date,a.stop_date,a.modified_date,a.formulation,a.dose,a.route,a.dispense,a.refill,a.frequency, a.ss_flag, a.medical_type, b.route_description, a.B_PayerId, a.B_BINLocationNumber, a.B_GroupId, a.B_PayerName, a.B_CardHolderId " &_
				" from notes_medications a, multum_route b " &_
				" where a.route=b.route_abbr(+) " &_
				" and patient_id='" & patient_id & "' " &_
				" and iscurrent = 0 and visit_key='" & visit_key & "' " &_
				"  " & sel & " " &_
                "  " & date_clause & " " &_
				" order by status,drug_name"
	session("show") = "new"
end if
if show = "current" or session("show") = "current" then
	sqlQuery = "Select a.multum_drug_id,a.status,a.drug_name,a.multum_brand_code,a.notes,a.ndc_code,a.drug_id,a.prescribed_date,a.stop_date,a.modified_date,a.formulation,a.dose,a.route,a.dispense,a.refill,a.frequency, a.ss_flag, a.medical_type, b.route_description, a.B_PayerId, a.B_BINLocationNumber, a.B_GroupId, a.B_PayerName, a.B_CardHolderId " &_
				" from notes_medications a, multum_route b " &_
				" where a.route=b.route_abbr(+) " &_
				" and patient_id='" & patient_id & "' " &_
				"  " & sel & " " &_
                "  " & date_clause & " " &_
                " order by status,drug_name"
	session("show") = "current"


end if

''Response.Write sqlQuery

res1.open sqlQuery,con
if not res1.eof then

	cnt = 1
	while not res1.eof
		notes = ""
		if cnt mod 2 = 0 then
			bgcolor = "#FFFFFF"
		else
			bgcolor = "#EFEFEF"
		end if

		if not isnull(res1("multum_drug_id")) and res1("status") <> "Stopped" then
			multum_drug_id_list =  res1("multum_drug_id") & "|" & multum_drug_id_list
			multum_drug_name_list =  res1("drug_name") & "|" & multum_drug_name_list

			if not isnull(res1("multum_brand_code")) then
				multum_query_string = multum_query_string & "&drugname=" & res1("drug_name") & ";drugid=" & res1("multum_brand_code") & " "
			end if
		end if

		if res1("notes") <> "" then
			notes = replace(res1("notes"),";","<br>")
		end if

		'check for the contolled substance drug...
		sqlQuery = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and upper(drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%'"
		sqlQuery = "select a.drug_id, b.ndc_code  from notes_medications a,ndc_drugs b " &_
					" where a.ndc_code = b.ndc_code " &_
					" and a.ndc_code = '" & res1("ndc_code") & "'" &_
					" and csa_schedule in (2,3,4,5)" &_
					" and upper(a.drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%'"
		rs1.open sqlQuery,con
		if not (rs1.eof or rs1.bof) then
			Is_CS="Y"
		else
			Is_CS="N"
		end if
		rs1.close
		'If res1("drug_name")="ibuprofen-oxycodone" then Is_CS="Y"	'--only for ibuprofen-oxycodone

		'get surescript status of the medication -start
		strSQL=" select MessageID,MessageType,STATUS, STATUSDESC, REFRESPONSECODE from SS_MESSAGE where DrugID='" & res1("drug_id") & "' order by timesent desc"
		rs1.open strSQL,con

		If not (rs1.EOF or rs1.BOF) then
			ss_MsgId=rs1("MessageID")
			ss_status=rs1("STATUS")
			ss_status_desc=	rs1("STATUSDESC")
			ss_MsgType= rs1("MessageType")
			ss_refrep= trim(rs1("REFRESPONSECODE"))

			if ss_MsgType = "CancelRx" or ss_MsgType = "CancelRxResponse" then
				ss_status = rs1("CancelCode")
				ss_status_desc = rs1("CancelDesc")
			end if
		else
			ss_MsgId=""
			ss_status=""
			ss_status_desc=""
			ss_MsgType=""
			ss_refrep=""
		End if
		rs1.close
		'--------end

		'get drug validation info
		Drug_Validation = ""
		if res1("status") <> "Stopped" then
		strSQL = "select Description,TimeCreated from Notes_DrugValidations where visit_key='"&visit_key&"' and multum_drug_id = '"&res1("multum_drug_id")&"'"
		'Response.Write strSQL
		rs1.Open strSQL,con

		if not (rs1.EOF or rs1.BOF) then
			bgcolor = "#FF9999"
			while not rs1.EOF
				Drug_Validation = rs1("Description") & " - " & rs1("TimeCreated") & "<BR>"
				rs1.MoveNext
			wend
		end if
		rs1.Close
		end if

		'get Medicare coverage info (ndc_nonmatched table / ndc_code column has ndc codes of drugs which are not covered by Medicare
		ndc_nonmatched = "No information"
		if (res1("ndc_code") <> "" ) then
			ndc_codeVal = res1("ndc_code")

			strSQL = "select ndc_code from ndc_nonmatched where REPLACE(LTRIM(REPLACE(ndc_code, '0', ' ')), ' ', '0') = REPLACE(LTRIM(REPLACE( " & ndc_codeVal & ", '0', ' ')), ' ', '0')"
			rs1.Open strSQL,con
			ndc_nonmatched = "Covered"
			if not (rs1.EOF or rs1.BOF) then

				bgcolor = "#FF9999"
				while not rs1.EOF
					ndc_nonmatched = "Not covered"
					rs1.MoveNext
				wend
			end if
			rs1.Close
		end if

		'Check free text medication
        freetext_med = false
        if (res1("ss_flag") = "0") then
            freetext_med = true
        end if

		'Check wrong NDC medication
		wrong_ndc = false
		if not freetext_med and res1("medical_type") = "DRU" then
			strSQL = 	"select distinct d.ndc_code "  &_
						"from ndc_brand_name a, multum_product_strength b, ndc_main_multum_drug_code c, ndc_core_description d, multum_dose_form e "  &_
						"where d.ndc_code = '" & res1("ndc_code") & "'"  &_
						"and a.brand_code = d.brand_code " &_
						"and d.main_multum_drug_code = c.main_multum_drug_code " &_
						"and b.product_strength_code = c.product_strength_code " &_
						"and c.dose_form_code = e.dose_form_code " &_
						"and(d.obselete_date is null or d.obselete_date > sysdate) " &_
						"and (regexp_replace(lower(trim('" & res1("drug_name") & "')), '[#]', '') = regexp_replace(lower(trim(a.brand_description)), '[#]', '')) "  &_
						"and (regexp_replace(lower(trim('" & res1("formulation") & "')), '[#]', '') = regexp_replace(lower(concat(concat(trim(b.product_strength_description), ' '), trim(e.dose_from_description))), '[#]', ''))"
			rs1.Open strSQL,con
			if (rs1.EOF or rs1.BOF) then
				wrong_ndc = true
			end if
			rs1.Close
		end if

%>
		<tr bgcolor="<%=bgcolor%>">
			<td valign="top">
				<input type="radio" name="mid" value="<%=res1("drug_id")%>" onclick="document.getElementById('drug_id_hidden').value=this.value">
				<b><%=res1("prescribed_date")%></td>
			<td valign="top"><b><%=res1("stop_date")%></td>
            <td valign="top"><b><%=res1("modified_date")%></td>
			<td valign="top">
				<b><a href="#" onclick="popScrollWindow('message_history.asp?drug_id=<%=res1("drug_id")%>','500','400');"><%=res1("drug_name")%></a></b>
				<%=res1("formulation")%>
				<%if Is_CS="Y" then%>
					<img alt="Controlled Substance" src="/images/practice/red_star.gif" border="0">
				<%end if%>
				<br>
				<b>Dose:</b> <%=res1("dose")%><br>
				<b>Route:</b> <%=res1("route")%><br>
				<% if trim(res1("dispense").value) <> "" then %>
					<b>Dispense:</b> <%=res1("dispense")%><br>
				<% end if %>
				<b>Refill:</b>
				<%If cint(res1("refill"))=999 then
					Response.Write "PRN"
				else Response.Write res1("refill")
				end if%>
				<% if res1("frequency") <> "" then %>
					<br><b>Directions:</b> <%=res1("frequency")%><% if res1("route") <> "" then Response.write("-" & PCase(res1("route")))%>
				<% end if %>
			</td>
			<td class="st" valign="top"><%=notes%></td>
			<td valign="top">
				<b><%=res1("status")%></b><br> <%=ndc_nonmatched%><br /><br />
				<% if freetext_med then %><b>Free text medication</b>&nbsp;<img style="cursor:pointer" onclick="alert('Free text medication is NOT allowed to e-prescribe')" src="/images/practice/question.jpg" /><br /><br /><% end if %>
				<% if wrong_ndc then %><b>Medication information updated</b>&nbsp;<img style="cursor:pointer" onclick="alert('Medication information is updated from latest drug database version.\nIt is NOT allowed to e-prescribe.\nRecommend to STOP this medication and add a new one')" src="/images/practice/question.jpg" /><% end if %>
			</td>
			<td valign="top"><%=Drug_Validation%></td>
			<td valign="top">
				<%If ss_MsgType<>"" then%>
					<b><a onMouseOver="return overlib('<%=ss_status_desc%>',LEFT,STICKY,CAPTION,'Electronic Submission Response');" onmouseout="return nd();"><%=ss_MsgType%> - <%=ss_refrep%> - <%=ss_status%></a><br>
				<%end if%>
				<% If ss_MsgType="RefillRequest" and ss_status="OK" then %>
					____________
					<br><font size="1"><a href="#" onclick="popScrollWindow('msg_rf_response_new.asp?messageID=<%=ss_MsgId%>&amp;drug_id=<%=res1("drug_id")%>&amp;visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>','580','475');">Response</a></font>
				<%end if %>
			</td>
		</tr>
<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close

else
%>
	<tr>
		<td colspan="8">No medications found on record.</td>
	</tr>
<%
end if
%>
</table>
<input type="hidden" name="multum_drug_id_list" value="<%=multum_drug_id_list%>">
<input type="hidden" name="multum_drug_name_list" value="<%=multum_drug_name_list%>">
<input type="hidden" name="multum_query_string" value="<%=multum_query_string%>">
<input type="hidden" name="allergies" value="<%=Allergies%>">
<input type="hidden" name="allergy_sev" value="<%=allergy_sev%>">
<input type="hidden" name="allergy_reac" value="<%=allergy_reac%>">
<input type="hidden" name="show" value="<%=session("show")%>">

<img alt="Controlled Substance" src="/images/practice/red_star.gif" border="0"><font size="1"> Controlled substance</font>

<p>

    <% if status6 <> "0" then %>
          <input type="button" onclick="newDrug_bh();" value="New" id="buttonNew" name="buttonNew">&nbsp;        
<% end if %>


<% if status6 = "0" then %>
          <input type="button" onclick="newDrug();" value="New" id="buttonNew" name="buttonNew">&nbsp;
<% end if %>



<input type="submit" name="refill" onclick="return checkDrugID();return confirm('Are you sure you wish to refill this prescription?');" value="Refill" id="buttonRefill" name="buttonRefill">&nbsp;
<input type="button" onclick="editDrug();" value="Edit" id="buttonEdit" name="buttonEdit">&nbsp;
<input type="submit" name="stop" value="Stop Medication" onclick="return checkDrugID();return confirm('Are you sure you wish to stop this prescription?');">&nbsp;
<input type="submit" name="purge" value="Purge Medication" onclick="return checkDrugID();return confirm('Are you sure you want to purge this prescription?');">&nbsp;
<input type="submit" name="start" value="(Re)start Medication" onclick="return checkDrugID();return confirm('Are you sure you wish to (re)start this prescription?');">&nbsp;
<input type="button" onclick="document.location='/practice/notes/display/medication_printrx.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'" value="Print Rx..." id="buttonPrintRx" name="buttonPrintRx">&nbsp;

<% set RS9 = Server.CreateObject("ADODB.Recordset")%>
<%RS9.Open "SELECT ACC_ID  FROM FaxSending_Rights WHERE ACC_ID = '" & SQLFixUp(session("pid")) & "' ", CONN%>
                   <%while not RS9.EOF%>
                                                <%if RS9("ACC_ID") = session("pid") then
                                                              status1="selected"
                                                       else
                                                              status1=""
                                                       end if
                                                %>
                          <%
                                                RS9.MoveNext
                                                wend
                                                set rs9=nothing
                          %>

                   <% if status1 <> "" then %>
                 <input type="button" onclick="document.location='/practice/notes/display/medication_faxrx.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'" value="Fax Rx..." id="button2" name="buttonPrintRx">&nbsp;
<% end if %>


<input type="button" onclick="printmedhx();" value="Print Med Hx" id="button8" name="button8">&nbsp;
<% if session("role") = "physician" then %>
	<input type="button" onclick="document.location='/practice/notes/display/medication_order_rx.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'" value="Order Prescription" id="buttonOrder" name="buttonOrder" >
<% end if %>
<input type="button" onclick="document.location='/DrugFunctions/DrugToDrug.aspx?Drugs='+document.getElementById('multum_drug_id_list').value+'&amp;SearchBy=ID&amp;Visit_Key=<%=visit_key%>&amp;Severity=<%=severity%>&amp;Allergies='+document.getElementById('allergies').value+'&amp;Allergy_Sev='+document.getElementById('allergy_sev').value+'&amp;Allergy_Reac='+document.getElementById('allergy_reac').value" value="Run Drug Validations" id="buttonRun" name="buttonRun">&nbsp;
<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').toggleTop();document.location='/practice/notes/display/note.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Cancel" id="buttonCancel" name="buttonCancel">&nbsp;

<%if (button_reconcile) then%>
	<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').toggleTop();document.location='/practice/notes/display/reconcile.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Reconcile" id="buttonReconcile" name="buttonReconcile">&nbsp;
<%end if%>
<%if (preReconcileList1) then%>
	<input type="button" onclick="getFrameByName(top,'leftNoteFrameTop').toggleTop();document.location='/practice/notes/display/prvReconcileList.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Previous Reconciled Lists" id="buttonPrvReconcile" name="buttonPrvReconcile">&nbsp;
<%end if%>

<p>
</form>
</body>
</html>
