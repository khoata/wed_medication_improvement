<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id_hidden")

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">

<script language="javascript">


function chk_pharmacy()
{
	/*if (document.medprint.selected_mid.value=='')
	{
	    alert("Please select a Prescription to be send.")
	    //alert('aaaaa1');
	return (false);
	}

	if (document.medprint.is_sub.value=='Y')
	{
	    alert("Medication is a controlled substance, it can not be sent electronacally.")
	   // alert('aaaaa2');
	return false;
	}*/

    var cbs = document.getElementsByTagName('input');
    var med_selected = false;
    for (var i = 0; i < cbs.length; i++) {
        if (cbs[i].type == 'checkbox' && !cbs[i].disabled) {
            if (cbs[i].checked) {med_selected = true;break;}
        }
    }
    if (!med_selected) {
        alert("Please select a Prescription to be send.")
        return (false);
    }


	if(document.medprint.ph_name.value=='')
		{	alert('Please select the Pharmacy to send the Prescription Electronically');
		document.medprint.ph_name.select();
		//alert('aaaaa3');
			return false;
		}
}

function checkAll(bx) {
  var cbs = document.getElementsByTagName('input');
  for(var i=0; i < cbs.length; i++) {
    if (cbs[i].type == 'checkbox' && !cbs[i].disabled) {
      cbs[i].checked = bx.checked;
    }
  }
}

</script>

</head>

<body>
<div class="titleBox">Medications &gt; Order Multiple Prescriptions Electronically</div>
<%
dim prescriberInfo
set drRS = server.CreateObject("ADODB.Recordset")
sqlQuery = "select * from doctortable where docuser='" & session("docid") & "'"
drRS.open sqlQuery,con

if not (drRS.EOF or drRS.BOF) then
    prescriberInfo = drRS("first_name") & " " & drRS("last_name") & ","
    prescriberInfo = prescriberInfo & drRS("street") & ", " & drRS("city") & ", " & drRS("state") & ", " & drRS("zip") & ", Phone: " & drRS("phone")
end if

drRS.close
%>
<p><b>Prescriber:</b> <%=prescriberInfo%></p>
<p>
Select the prescription you would like to order electronically.
<p>

<form action="medication_order_rx_m_stage.asp"  method="post" name="medprint"  onsubmit="return chk_pharmacy()">

<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">

  <tr bgcolor="#3366CC">
    <td width="2%"><input type="checkbox" onclick="checkAll(this)"></td>
    <td width="8%"><font color="white"><b>Start Date</td>
    <td width="8%"><font color="white"><b>Written Date</td>
    <td width="40%"><font color="white"><b>Prescription</td>
    <td width="17%"><font color="white"><b>Note to Pharmacist</td>
    <td width="15%"><font color="white"><b>Current Order Status</td>
  </tr>

<%

set res1 = server.CreateObject("ADODB.Recordset")
set rs = server.CreateObject("ADODB.Recordset")

'Ignore wrong NDC medication
sqlQuery =	"select a.*, b.route_description,c.doctor_id as doctor_id, " &_
      "case when a.modified_date is not null then a.modified_date else a.prescribed_date end as last_action_date " &_
			"from notes_medications a, multum_route b, visit c, ndc_brand_name aa, multum_product_strength bb, ndc_main_multum_drug_code cc, ndc_core_description dd, multum_dose_form ee " &_
			"where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "' and a.medical_type = 'DRU' " &_
			"and a.ndc_code = dd.ndc_code " &_
			"and aa.brand_code = dd.brand_code " &_
			"and dd.main_multum_drug_code = cc.main_multum_drug_code " &_
			"and bb.product_strength_code = cc.product_strength_code " &_
			"and cc.dose_form_code = ee.dose_form_code " &_
			"and(dd.obselete_date is null or dd.obselete_date > sysdate) " &_
			"and regexp_replace(lower(trim(a.drug_name)), '[#]', '') = regexp_replace(lower(trim(aa.brand_description)), '[#]', '')" &_
			"and regexp_replace(lower(trim(a.formulation)), '[#]', '') = regexp_replace(lower(concat(concat(trim(bb.product_strength_description), ' '), trim(ee.dose_from_description))), '[#]', '') " &_
      "UNION " &_
      "select a.*, b.route_description,c.doctor_id as doctor_id, " &_
      "case when a.modified_date is not null then a.modified_date else a.prescribed_date end as last_action_date " &_
			"from notes_medications a, multum_route b, visit c " &_
			"where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "' and a.medical_type = 'SP' " &_
      "order by last_action_date desc"

'sqlQuery = "select a.*, b.route_description,c.doctor_id as doctor_id from notes_medications a, multum_route b, visit c where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "' order by drug_name"
'sqlQuery = "select a.*, b.route_description,a.PRESCRIBED_BY as doctor_id from notes_medications a, multum_route b where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.patient_id='" & patient_id & "'"

'Response.Write sqlQuery

res1.open sqlQuery,con
if not res1.eof then
	cnt = 1
	while not res1.eof
		Physician_ID=res1("doctor_id")
		if res1("visit_key") = visit_key then
			bgcolor = "#FFFFCC"
		else
			if cnt mod 2 = 0 then
				bgcolor = "#FFFFFF"
			else
				bgcolor = "#EFEFEF"
			end if
		end if

		'check for the contolled substance drug.....
		'sqlQuery = "select * from SS_Substances where schedule='III' and (upper(drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%' or upper(SYNM) like '"& ucase(left(res1("drug_name"),7)) & "%' )"
		'sqlQuery = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and upper(drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%'"
		sqlQuery = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and upper(brand_name) = '" & ucase(res1("drug_name")) &"'"
		sqlQuery = "select a.drug_id, b.ndc_code  from notes_medications a,ndc_drugs b " &_
					" where a.ndc_code = b.ndc_code " &_
					" and a.ndc_code = '" & res1("ndc_code") & "'" &_
					" and csa_schedule in (2,3,4,5)" &_
					" and upper(a.drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%'"

		rs.open sqlQuery,con
		if not (rs.eof or rs.bof) then
			Is_CS="Y"
		else
			Is_CS="N"
		end if
		rs.close

		'get surescript status of the medication -start
		strSQL=" select MessageType,STATUS, STATUSDESC, REFRESPONSECODE, CancelCode, CancelDesc from SS_MESSAGE where DrugID='" & res1("drug_id") & "' order by timesent desc"
		rs.open strSQL,con

		If not (rs.EOF or rs.BOF) then
			ss_status=rs("STATUS")
			ss_status_desc=	rs("STATUSDESC")
			ss_MsgType= rs("MessageType")
			ss_refrep= trim(rs("REFRESPONSECODE"))
			If ss_refrep="DeniedNewPrescriptionToFollow" then
				ss_followup="Y"
			else
				ss_followup="N"
			end if

			if ss_MsgType = "CancelRx" or ss_MsgType = "CancelRxResponse" then
				ss_status = rs("CancelCode")
				ss_status_desc = rs("CancelDesc")
			end if
		else
			ss_status=""
			ss_status_desc=""
			ss_MsgType=""
			ss_refrep=""
			ss_followup="N"
		End if
		rs.close
		'--------end

        substitution = ""
        if not isnull(res1("substitution")) then
            if res1("substitution") = "0" then
                substitution = "Allowed"
            elseif res1("substitution") = "1" then
                substitution = "Not allowed"
            else
                substitution = res1("substitution")
            end if
        end if

        notSent = false
        if Is_CS = "Y" then
            notSent = true
            alertMsg = "Medication is a controlled substance, it can not be sent in order multiple prescriptions."
        elseif len(res1("notes")) > 210 then
            notSent = true
            alertMsg = "Note to Pharmacist is longer than 210 characters, please shorten it before sending electronically."
        end if
%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top">
        <% if notSent then %>
        <span style="position:relative;">
          <input type="checkbox" disabled/>
          <div style="position:absolute;top:0;bottom:0;left:0;right:0;" onclick="alert('<%=alertMsg %>')"></div>
        </span>
        <% else  %>
        <input type="checkbox" name="medi_sel<%=cnt%>" value="<%=res1("drug_id")%>,<%=Physician_ID%>,<%=Is_CS%>,<%=ss_followup%>,<%=res1("drug_name")%>"  >
        <% end if %>
        </td>
        <td valign="top"><b><%=res1("prescribed_date")%></td>
        <td valign="top"><b><%=date()%></b></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b>
          <%=res1("formulation")%><%if Is_CS="Y" then%><img alt="controlled substance,can not be sent electronacally" src="/images/practice/red_star.gif" border="0"><%end if%> <br>
	      <% if trim(res1("dispense").value) <> "" then %>
	      <b>Dispense:</b> <%=res1("dispense")%><br>
	      <% end if %>
	      <b>Refill:</b> <%If cint(res1("refill"))=999 then Response.Write "PRN" else Response.Write res1("refill") end if%>
	      <% if res1("frequency") <> "" then %>
	      <br><b>Directions:</b> <%=res1("dose") & " " & res1("frequency")%><% if res1("route") <> "" then Response.write("-" & PCase(res1("route")))%>
          <br /><b>Substitution:</b> <%=substitution %>
	      <% end if%>
	    </td>
	    <td class="st" valign="top"><%=res1("notes")%></td>
	    <td valign="top">
	    <%If ss_MsgType<>"" then%>
			<b><a onMouseOver="return overlib('<%=ss_status_desc%>',LEFT,STICKY,CAPTION,'Electronic Submission Response');" onmouseout="return nd();"><%=ss_MsgType%> - <%=ss_refrep%> - <%=ss_status%></a><br>
			<%end if%>
	    </td>
	</tr>
<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close
%>


<%
strSQL = "select * from patienttable where userID  ='" & patient_id & "'"
rs.open strSQL,con

if not (rs.eof or rs.BOF) then
	LAST_NAME = ucase(rs("LAST_NAME"))
	FIRST_NAME = ucase(rs("FIRST_NAME"))
	BIRTH_DATE = rs("BIRTH_DATE")
	GENDER  =  ucase(rs("GENDER"))
	Ph_name = rs("PREFERRED_PHARMACY")
	NCPDPID =  rs("NCPDPID")
	NCPDPID2 =  rs("NCPDPID2")
End if
rs.close

If NCPDPID<>"" then
	strSQL = "select storename, addressline1, city, state, zip, phoneprimary from ss_pharmacy where NCPDPID ='" & NCPDPID & "'"
	rs.open strSQL,con
	Ph_fullname= rs("storename")&" "&rs("addressline1")&" "&rs("city")&" "&rs("state")&" "&rs("zip")&" PH: "&rs("phoneprimary")
	rs.Close
end if
If NCPDPID2<>"" then
	strSQL = "select storename, addressline1, city, state, zip, phoneprimary from ss_pharmacy where NCPDPID ='" & NCPDPID2 & "'"
	rs.open strSQL,con
	Ph_fullname2= rs("storename")&" "&rs("addressline1")&" "&rs("city")&" "&rs("state")&" "&rs("zip")&" PH: "&rs("phoneprimary")
	rs.Close
end if

strSQL="select a.MessageID,a.PATIENTLASTNAME, a.PATIENTFIRSTNAME, a.PATIENTGENDER, a.PATIENTDOB, a.MEDICATIONDESCRIPTION, a.MEDICATIONnote, c.ncpdpid, replace(c.storename, '''', '') storename, d.rxreferencenumber "&_
	   "	From SS_MESSAGE_CONTENT a " &_
	   "   join ss_message d on d.messageid = a.messageid "&_
	   "   left outer join ss_pharmacy c on c.ncpdpid = d.pharmacyid where a.MessageID in ( "&_
	   "	select  b.MessageID  from ss_message b where b.NEWRXNEEDED='YES') "&_
	   "   and upper(a.PATIENTLASTNAME) ='" & LAST_NAME & "' and  upper(a.PATIENTFIRSTNAME) ='" & FIRST_NAME & "'"&_
	   "  order by a.PATIENTLASTNAME, a.PATIENTFIRSTNAME "
'response.write strSQL
rs.open strSQL,con
if not (rs.eof or rs.BOF) then
%>
<tr bgcolor="#3366CC">
    <td colspan="6"><font color="white">The following Refills have been Denied with NewRXToFollow, please select the appropriate medication below if you are sending a NewRX for one of the DinedwithNewRXtoFollow Refills. </td>
</tr>

<%
 while not rs.eof
   If ucase(rs("PATIENTLASTNAME")) = LAST_NAME and ucase(rs("PATIENTFIRSTNAME"))=FIRST_NAME and ucase(rs("PATIENTGENDER"))= left(GENDER,1) then

%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top"><input type="radio" name="refres_messageid" id="refres_messageid" value="<%=rs("MessageID")%>" onclick="document.medprint.Followup_Flag.value='Y';document.medprint.ph_name.value='<%=rs("storename") %>';document.medprint.NCPDPID.value='<%=rs("ncpdpid") %>'; document.getElementById('searcher').style.display='none'; document.medprint.RXReferenceNumber.value='<%=rs("RXReferencenumber") %>'; return true;">
         <td valign="top" colspan="3">  <b><%=rs("MEDICATIONDESCRIPTION")%></b>
	     <td class="st" valign="top" colspan="2"><%=rs("MEDICATIONnote")%></td>
	</tr>
<%
	end if
 rs.movenext
 wend
	rs.Close

end if
%>


<%
else
%>
	<tr>
		<td colspan="8">No medications found on record.</td>
	</tr>
<%
end if
%>
</table>


<p>
<div class="titleBox_HL2">
<input type="hidden" name="NCPDPID" id="NCPDPID" value="<%=NCPDPID%>">
<input type="hidden" name="visit_key" value="<%=visit_key%>">
<input type="hidden" name="patient_id" value="<%=patient_id%>">
<input type="hidden" name="MsgType" value="NewRX">
<input type="hidden" name="RXReferenceNumber" value>
<input type="hidden" name="MedCnt" value="<%=cnt-1%>">



Pharmacy (Primary):
<input maxlength="100" name="ph_name" size="95" value="<%=ph_fullname%>" readonly>&nbsp;&nbsp;
<a href="#" id="searcher" onclick="popScrollWindowMax('pharmacy_search.asp?patient_id=<%=patient_id%>','800','500');"><img border="0" height="18" src="/images/global/icon_search.gif"></a>
<%
if ph_fullname2 <>"" then
	Response.Write"<br><br> Pharmacy (Secondary):"& ph_fullname2
end if
%>
</div>

<font color="red" size="2"><b>*</b></font><font size="1"> Controlled substance can not be sent in Order Multiple Prescription.</font>
<br><p>
<input type="submit" value="Order Prescription" id="s1" name="s2">
<!--<input type="button" value="Print Rx" name="b3" onclick="print_med();">-->&nbsp;
<input type="button" onclick="document.location='/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Cancel" id="button4" name="button4">
</form>
</body>
</html>
