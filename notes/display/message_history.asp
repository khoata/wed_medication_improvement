
<!--#include file="../../../library/connection.inc.asp"-->

<html>
<head>
<title>Medication Submission History</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
</head>
<body style="margin-left: 0px; margin-right: 0px;">

<%
drug_id = Request.QueryString("drug_id")

set rs1 = server.CreateObject("ADODB.Recordset")
strSQL="select drug_name from notes_medications where drug_id='" & drug_id & "'"

rs1.open strSQL,con		
If not (rs1.EOF or rs1.BOF) then
	drug_name=rs1("drug_name")
End if
rs1.close

%>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr>
		<td valign="top">
		<table border="0" cellpadding="3" cellspacing="3" width="100%">
			<tr><td><b>Medication:&nbsp;<%=drug_name%><b></td>	
			</tr>
			<tr><td>
<%   
rs1.Open "select TIMESENT, " _
                & " case when STATUS is null then '&nbsp;' else status end status, " _
                & " case when STATUSDESC is null then '&nbsp;' else STATUSDESC end STATUSDESC, " _
                & " storename,REFRESPONSECODE, messagetype, CancelCode, " _
                & " case when note is null then '&nbsp;' else note end note " _
                & " from ss_message a, ss_pharmacy b where pharmacyid=ncpdpid and drugid='" & drug_id & "' order by timesent desc", CONN
if not rs1.EOF then
%>				<P class="titlebox_HL">History</p>
				<table border="0" cellpadding="1" cellspacing="0" width="100%" bgcolor="#3366CC">
				<tr><td>
				<table border="0" cellpadding="5" cellspacing="0" width="100%" class="sched" bgcolor="white">
				<tr height="25" width="100%">
					<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Date</b></td>
					<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Pharmacy</b></td>
					<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Message Type</b></td>
                    <td class="w" height="1" bgcolor="#3366CC" align="left"><b>Note</b></td>
					<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Status</b></td>
					<td class="w" height="1" bgcolor="#3366CC" align="left"><b>Description</b></td>
				</tr>
<%				while not rs1.EOF 
				status=rs1("STATUS")
                if rs1("messagetype") = "CancelRxResponse" then
                    status=rs1("CancelCode")
                end if
				ss_reqrsp=trim(rs1("REFRESPONSECODE"))
%>
				<tr height="25" width="100%">
					<td><b><%=rs1("TIMESENT")%></b></td>
					<td><b><%=rs1("storename")%></b></td>
					<td><b><%=rs1("messagetype")%></b><%if ss_reqrsp<>"" then Response.Write " - "&ss_reqrsp%></td>
                    <td><%=rs1("note")%></td>
					<td><%=status%></td>
					<td><%=rs1("STATUSDESC")%></td>
				</tr>
<%				rs1.MoveNext 
				wend %>
				
<% else %>
<tr height="25" width="100%">
	<td colspan=3>No Submission History Found.</td>
</tr>
<%
end if 
rs1.Close 
%>
</table>
		</table>
		</td></tr>
		</table>
		</td></tr>
</table>
<center><input type=button value="Close Window" onclick="javascript:window.close();" id=button1 name=button1></center>			
</body>
</html>