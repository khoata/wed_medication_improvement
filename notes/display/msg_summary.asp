<!--#include file="../../../library/connection.inc.asp"-->
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<%=Application("Styles")%>
</HEAD>
<BODY>
<h3 style="display: inline;">Prescription Summary</h3>
<div class="titleBox">MessageID - <%=Request("messageID")%></div>
<div class="titleBox_HL" style="background-color: #FFF; font-weight: normal">

<% 
	messageID = Request("messageID")

    if (messageID <> "") then
        set CMD = Server.CreateObject("ADODB.Command")
		CMD.CommandText = "update ss_message set is_unread=null where messageid='" & messageID & "'"
		CMD.ActiveConnection = CONN
		CMD.Execute
        set CMD.ActiveConnection = nothing
    end if
	
	dim RS
	set RS = Server.CreateObject("ADODB.RecordSet")
	
	RS.Open "Select MESSAGEID, PHARMACYID, PHARMACYNAME, PHARMACYADDRESS, PHARMACYCITY, PHARMACYSTATE, PHARMACYZIP, PHARMACYPHONE, PHARMACYPHONETYPE, " &_
			"PRESCRIBERCLINICNAME, PRESCRIBERID, PRESCRIBERSPECIALTYQUAL, PRESCRIBERSPECIALTYCODE, PRESCRIBERAGENTLASTNAME, PRESCRIBERAGENTFIRSTNAME, " &_
			"PRESCRIBERADDRESS, PRESCRIBERCITY, PRESCRIBERSTATE, PRESCRIBERZIP, PRESCRIBEREMAIL, PRESCRIBERPHONETYPE, PATIENTID, PATIENTSSN, PATIENTLASTNAME, " &_
			"PATIENTFIRSTNAME, PATIENTPREFIX, PATIENTGENDER, PATIENTDOB, PATIENTADDRESS, PATIENTCITY, PATIENTSTATE, PATIENTZIP, PATIENTEMAIL, PATIENTPHONE, " &_
			"PATIENTPHONETYPE, MEDICATIONDESCRIPTION, MEDICATIONCODE, MEDICATIONCODETYPE, MEDICATIONQUANTITYQUAL, MEDICATIONQUANTITY, MEDICATIONDIRECTIONS," &_
			"MEDICATIONNOTE, MEDICATIONREFILLQUAL, MEDICATIONREFILLQUAN, MEDICATIONSUBSTITUTION, MEDICATIONWRITTENDATE, PRESCRIBERPHONE, DATECREATED " &_
			"FROM SS_MESSAGE_CONTENT " &_
			"WHERE MESSAGEID = '"& messageID &"'", CONN
	if not RS.EOF then
	
%>
<table cellspacing = 2 cellpadding = 1 width="100%" border=1>
<tr><td valign="top" width="33%">
	<div class="titleBox">Prescriber</div>
	<%=RS("PRESCRIBERAGENTFIRSTNAME")%>&nbsp;<%=RS("PRESCRIBERAGENTLASTNAME")%><br>
	<%=RS("PRESCRIBERADDRESS")%><br>
	<%=RS("PRESCRIBERCITY")& ", " & RS("PRESCRIBERSTATE") & "  " & RS("PRESCRIBERZIP")%><br>
	<b>Phone</b><%=RS("PRESCRIBERPHONE")%><br>
	<b><%=RS("PRESCRIBEREMAIL")%></b><br>
	</td>
	<td valign="top" width="33%">
	<div class="titleBox">Pharmacy</div>
	<%=RS("PHARMACYNAME")%>
	<%=RS("PHARMACYADDRESS")%><br>
	<%=RS("PHARMACYCITY") & ", " & RS("PHARMACYSTATE") & "  " & RS("PHARMACYZIP") %><br>
	<b>Phone:</b> <%= RS("PHARMACYPHONE")%><br>
	</td>
	<td valign="top">
	<div class="titleBox">Patient</div>
	<%=RS("PATIENTFIRSTNAME") & " " & RS("PATIENTLASTNAME") %><br>
	<%=RS("PATIENTADDRESS") %> <br>
	<%=RS("PATIENTCITY") & ", " & RS("PATIENTSTATE") & "  " & RS("PATIENTZIP") %><br>
	<b>Phone:</b> <%= RS("PATIENTPHONE") %><br>
	<b>DOB:</b> <%=RS("PATIENTDOB")%>
	</td>
</tr>	
<tr>
	<td colspan=3 width="33%">
	<div class="titleBox">Medication Prescribed</div>
	<b><%=RS("MEDICATIONDESCRIPTION")%></b><p>
	NDC: <%=RS("MEDICATIONCODE")%><br>
	Dispense: <%=RS("MEDICATIONDIRECTIONS")%><br>
	Notes: <%=RS("MEDICATIONNOTE")%><br>
	Substitution: <%=RS("MEDICATIONSUBSTITUTION")%><br>
	Refill: <% if not isnull(RS("MEDICATIONREFILLQUAN")) and RS("MEDICATIONREFILLQUAN") <> "" then response.Write RS("MEDICATIONREFILLQUAN") else response.Write RS("MEDICATIONREFILLQUAL")%><BR>	
	Written Date: <%=RS("MEDICATIONWRITTENDATE")%>
	</td>
</tr>
</table>
<%
	else
		Response.Write "This record does not exist any more."
	
	end if
%>
</div>
<center><input type=button name=close value="Close" onclick="window.close();"></center>
</BODY>
</HTML>
