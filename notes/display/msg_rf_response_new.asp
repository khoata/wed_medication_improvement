<!--#include file="../../../library/connection.inc.asp"-->
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<%=Application("Styles")%>

<%
function multiplyString( theString, howMany )

    theResponse = ""
    for i = 1 to howMany
        theResponse = theResponse & theString & ","
    next


    multiplyString = theResponse
end function

messageID = Request("messageID")

if (messageID <> "") then
    set CMD = Server.CreateObject("ADODB.Command")
	CMD.CommandText = "update ss_message set is_unread=null where messageid='" & messageID & "'"
	CMD.ActiveConnection = CONN
	CMD.Execute
    set CMD.ActiveConnection = nothing
end if

%>

<script>
function CheckEPCSPrescription() {
        if (CheckEPCSRequiredInformation() == false) {
            return false;
        }
        if (CheckPasswordAndOTPCode() == false) {
            return false;
        }
        return true;
    }

    function CheckEPCSRequiredInformation() {
        
        if (document.getElementById('epcs_signtwofactor').style.display == 'none') {
            return true;
        }

        var alertDetail = "";
        //Prescriber Information
        if (document.getElementById("epcs_prescriber_name").innerHTML == "") {
            alertDetail += "Prescriber's name is missing!\n";
        }
        if (document.getElementById("epcs_prescriber_address").innerHTML == "") {
            alertDetail += "Prescriber's address is missing!\n";
        }
        if (document.getElementById("epcs_prescriber_phone").innerHTML == "") {
            alertDetail += "Prescriber's phone is missing!\n";
        }
        if (document.getElementById("epcs_prescriber_DEA").innerHTML == "") {
            alertDetail += "Prescriber's DEA number is missing!\n";
        }

        //Pharmacy Information
        if (document.getElementById("epcs_pharmacy_name").innerHTML == "") {
            alertDetail += "Pharmacy's name is missing!\n";
        }
        if (document.getElementById("epcs_pharmacy_address").innerHTML == "") {
            alertDetail += "Pharmacy's address is missing!\n";
        }
        if (document.getElementById("epcs_pharmacy_phone").innerHTML == "") {
            alertDetail += "Pharmacy's phone is missing!\n";
        }

        //Patient Information
        if (document.getElementById("epcs_patient_firstname").innerHTML == "") {
            alertDetail += "Patient's first name is missing!\n";
        }
        if (document.getElementById("epcs_patient_lastname").innerHTML == "") {
            alertDetail += "Patient's last name is missing!\n";
        }
        if (document.getElementById("epcs_patient_gender").innerHTML == "") {
            alertDetail += "Patient's gender is missing!\n";
        }
        if (document.getElementById("epcs_patient_dob").innerHTML == "") {
            alertDetail += "Patient's DOB is missing!\n";
        }

        //Medication Information
        if (document.getElementById("epcs_medication_description").innerHTML == "") {
            alertDetail += "Drug description is missing!\n";
        }
        if (document.getElementById("epcs_medication_dispense").innerHTML == "") {
            alertDetail += "Drug quantity is missing!\n";
        }
//        if (document.getElementById("epcs_medication_refill").innerHTML == "") {
//            alertDetail += "Drug refill is missing!\n";
//        }
//        if (document.getElementById("epcs_medication_direction").innerHTML == "") {
//            alertDetail += "Drug direction is missing!\n";
//        }
        if (document.getElementById("epcs_medication_substitution").innerHTML == "") {
            alertDetail += "Drug substitution is missing!\n";
        }
        if (document.getElementById("epcs_medication_note").innerHTML == "") {
            alertDetail += "Drug note is missing!\n";
        }

        if (alertDetail != "") {
            alert(alertDetail);
            return false;
        }

        return true;
    }

function CheckPasswordAndOTPCode() {
    if (document.getElementById('epcs_signtwofactor').style.display == 'none') {
        return true;
    }

	if (document.f1.epcs_twofactor_password.value == "" || document.f1.epcs_twofactor_otpcode.value == "") {
		alert("Password and OTP code are required!")
		return false;
}

	return true;
}

function ChooseAction(ActionOption, ControlledSub)
{
	document.getElementById('Approved').style.display  = 'none';
	document.getElementById('Note').style.display = 'none';
	document.getElementById('DenialReason').style.display = 'none';
	document.getElementById('epcs_signtwofactor').style.display = 'none';
	document.getElementById('btnSubmit').value = 'Submit';
	
	if (ActionOption=='ApprovedWithChanges')
	{
		document.getElementById(ActionOption).style.display = '';
		document.getElementById('Note').style.display = '';
	}
	else if (ActionOption=='Approved') 
	{
	    //if( document.f1["req_refills"].value == '1' )
	    //{
    	//	document.getElementById('ApprovedWithChanges').style.display = '';
	    //}
	    document.getElementById('Approved').style.display = '';
		document.getElementById('Note').style.display = '';
		if (ControlledSub == 'C') {
	        document.getElementById('epcs_signtwofactor').style.display = '';
	        document.getElementById('btnSubmit').value = 'Sign + Submit';
	    }
	}
	else if ( ActionOption == 'DeniedNewPrescriptionToFollow')
	{
		document.getElementById('Note').style.display = '';
	}
	else if (ActionOption == 'Denied')
	{
		document.getElementById('DenialReason').style.display = '';
	}
	return true;
}

function checklength(i)
{
    var txt;
    txt=document.f1.Response_Note.value;
    n=txt.length;
    if (n > i) 
    {
    alert('Note allows 70 characters only.');
    document.f1.Response_Note.value=txt.substring(0, i);
    return;
    }
}


function chk_refrsp()
{
	document.f1.no_refill.value = document.f1.no_refill_select.options[document.f1.no_refill_select.selectedIndex].value;
    document.f1.Response_Reason.value = document.f1.Response_Reason_Code.options[document.f1.Response_Reason_Code.selectedIndex].innerText;
    if (document.f1.Response_Type.options[document.f1.Response_Type.selectedIndex].value == 'None')
    {
        alert("Please select your response to the Refill.")
        return (false);
    }
    /*
    if((document.f1.Response_Type.options[document.f1.Response_Type.selectedIndex].value =='ApprovedWithChanges' 
    || document.f1.Response_Type.options[document.f1.Response_Type.selectedIndex].value == 'DeniedNewRxToFollow')
    && document.f1.Response_Note.value=='')
    {	alert('Please Enter the Notes to Approve the Refill with Changes');
		    document.f1.Response_Note.select();
		    return false;
    }	
    */
    if(document.f1.Response_Type.options[document.f1.Response_Type.selectedIndex].value == 'Denied' 
    && document.f1.Response_Reason_Code.selectedIndex==0)
    {	alert('Please select the Denial Reason from the dropdown menu and enter the notes');
		    document.f1.Response_Reason_Code.focus();
		    return false;
    }
    /*
    if(document.f1.Response_Type.options[document.f1.Response_Type.selectedIndex].value =='ApprovedWithChanges')
    {
	    document.f1.action ="msg_rf_change.asp"
	    document.f1.submit();
	    return;
    }
*/
    if(document.f1.Response_Type.options[document.f1.Response_Type.selectedIndex].value =='Approved')
    {	
		pharmacy_refills = document.f1.pharmacy_refills.value;
		no_refill = document.f1.no_refill_select.options[document.f1.no_refill_select.selectedIndex].value;
		//no_refill=no_refill-1;
		if (document.f1.Response_Note.value.length > 70) {
		    alert("Note allows 70 characters only.")
		    return false;
		}

		if (pharmacy_refills == 'PRN') {
		    pharmacy_refills = '999'
		}

		//if (pharmacy_refills.length > 0 && pharmacy_refills != no_refill)
		if (pharmacy_refills != no_refill)
			{			

				document.f1.action ="msg_rf_change.asp"
			}
		else
			{	
				document.f1.action ="msg_rf_app.asp"
			}
			
		deaNumberChange = document.f1.DeaNumberChange.value
        if (deaNumberChange = "Y") {
            document.f1.action = "msg_rf_change.asp"
        }
    
	    //document.f1.action ="msg_rf_app.asp"	    
	    document.f1.submit();
	    return;
    }
    
    if(document.f1.Response_Type.options[document.f1.Response_Type.selectedIndex].value =='Denied')
    {
	    document.f1.action ="msg_rf_denied.asp"
	    document.f1.submit();
	    return;
    }

    if(document.f1.Response_Type.options[document.f1.Response_Type.selectedIndex].value =='DeniedNewPrescriptionToFollow')
    {
        if (document.f1.Response_Note.value.length == 0) {
            alert("Please enter denied reason.")
            return false;
        }

	    document.f1.action ="msg_rf_deniednew.asp"
	    document.f1.submit();
	    return;
    }

}

function update_refill_status(a)
{
	document.f1.action ="msg_rf_status.asp?MsgId="+a;
	document.f1.submit();
	return;
}

function noRefillChange(selectedVal) {
    if (selectedVal == "999") {
        document.getElementById('no_refill_one').style.display = 'none';
    } else {
        document.getElementById('no_refill_one').style.display = '';
    }
}

</script>
</HEAD>
<TITLE>Refill Response</TITLE>
<BODY>
<h3 style="display: inline;">Refill Response</h3>
<div class="titleBox">MessageID - <%=Request("messageID")%></div>
<div class="titleBox_HL" style="background-color: #FFF; font-weight: normal">

<% 
    schedule = "6"
	messageID = Request("messageID")
	
	dim RS1
	Set RS1 = server.CreateObject("ADODB.RecordSet")
	
	dim RS
	set RS = Server.CreateObject("ADODB.RecordSet")
	
	strSQL= "Select last_name,first_name, MESSAGEID, PHARMACYID, PHARMACYNAME, PHARMACYADDRESS, PHARMACYCITY, PHARMACYSTATE, PHARMACYZIP, PHARMACYPHONE, PHARMACYPHONETYPE, " &_
			"PRESCRIBERCLINICNAME, PRESCRIBERID, PRESCRIBERSPECIALTYQUAL, PRESCRIBERSPECIALTYCODE, PRESCRIBERAGENTLASTNAME, PRESCRIBERAGENTFIRSTNAME, " &_
			"PRESCRIBERADDRESS, PRESCRIBERCITY, PRESCRIBERSTATE, PRESCRIBERZIP, PRESCRIBEREMAIL, PRESCRIBERPHONETYPE, PATIENTID, PATIENTSSN, PATIENTLASTNAME, " &_
			"PATIENTFIRSTNAME, PATIENTPREFIX, PATIENTGENDER, PATIENTDOB, PATIENTADDRESS, PATIENTADDRESS2, PATIENTCITY, PATIENTSTATE, PATIENTZIP, PATIENTEMAIL, PATIENTPHONE, " &_
			"PATIENTPHONETYPE, MEDICATIONDESCRIPTION, MEDICATIONCODE, MEDICATIONCODETYPE, MEDICATIONQUANTITYQUAL, MEDICATIONQUANTITY, MEDICATIONDIRECTIONS," &_
			"MEDICATIONNOTE, MEDICATIONREFILLQUAL, MEDICATIONREFILLQUAN, MEDICATIONSUBSTITUTION, MEDICATIONWRITTENDATE, PRESCRIBERPHONE, DATECREATED, to_char(to_date(Patientdob, 'yyyymmdd'), 'mm/dd/yyyy') prettydate, " &_
			"MEDDISPENSEDESCRIPTION, MEDDISPENSECODE, MEDDISPENSECODETYPE, MEDDISPENSEQUANTITYQUAL, MEDDISPENSEQUANTITY, MEDDISPENSEDIRECTIONS, " &_
            "MEDDISPENSENOTE, MEDDISPENSEREFILLQUAL, MEDDISPENSEREFILLQUAN, MEDDISPENSESUBSTITUTION, MEDDISPENSEWRITTENDATE, " &_
            "c.DESCRIPTION MEDICATIONQUANTITYDESC, d.DESCRIPTION MEDDISPENSEQUANTITYDESC, PRESCRIBERDEA, DEASCHEDULE, DEASCHEDULEPRESCRIBE " &_
            "FROM SS_MESSAGE_CONTENT a left outer join doctortable b on a.prescriberID = b.spi " &_
			"left outer join ss_dispense c on a.MEDICATIONQUANTITYQUAL = c.code and c.inuse = 1 " &_
			"left outer join ss_dispense d on a.MEDDISPENSEQUANTITYQUAL = d.code and d.inuse = 1 " &_
			"WHERE MESSAGEID = '"& messageID &"'"
	
	RS.Open strSQL, CONN	
	'Response.Write strSQL
		
	if not RS.EOF then
	
%>
<table cellspacing = 2 cellpadding = 1 width="100%" border=1>
<tr><td valign="top" width="33%">
	<div class="titleBox">Prescriber</div>
	<%
    missingPrescriberName = false 

    if IsNull(RS("PRESCRIBERAGENTFIRSTNAME")) or IsNull(RS("PRESCRIBERAGENTLASTNAME")) then
        missingPrescriberName = true
    end if
    %>
    <span id="epcs_prescriber_name"><%=RS("PRESCRIBERAGENTLASTNAME") & ", " & RS("PRESCRIBERAGENTFIRSTNAME") %></span>

	<%
	strSQL = "select * from doctortable where docuser='" & session("user") & "' and EPCS = 'Y' and bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  2048) = 2048"
	RS1.Open strSQL, con
	if not RS1.EOF then
	epcs_prescriber = "Y"
    epcs_twofactor_userid = RS1("EPCS_USERID")
	%>
	<img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" />
	<%
	else
	epcs_prescriber = "N"
	end if
	RS1.Close
	%>

	<%
	'EPCS: 3. Prescriber in Texas must have Texas DPS number <StateLicenseNumber>DPS12345678</StateLicenseNumber>
    ' TODO: Check if prescriber's in Taxas
	DPSNumber = ""
    DEANumber = ""
    DEANumberChange = "N"

	'strSQL = "select * from doctortable where docuser='" & session("user") & "' and PROVIDER_LICNOSTATE = 'TX'"
    strSQL = "select * from doctortable where docuser='" & session("user") & "'"
	RS1.Open strSQL, con
	if not(RS1.EOF and RS1.BOF) then
		if IsNull(RS1("DPS_NO")) then
			DPSNumber = ""
		else
			DPSNumber = "DPS" & RS1("DPS_NO")        
		end if

        if isNull(RS("PRESCRIBERDEA")) then
            if IsNull(RS1("DEA_NO")) then
                DEANumber = ""
            else
                DEANumber = RS1("DEA_NO")
                DEANumberChange = "Y"
            end if
        else
            DEANumber = RS("PRESCRIBERDEA")
        end if
	end if
	RS1.Close

    set rsEPCS = server.CreateObject("ADODB.Recordset")
    strSQLEPCSAudit = "SELECT userid, role FROM acc WHERE upper(streetadd)=upper('"&session("pid")&"') and upper(userid) = upper('" & session("user") & "')"
    rsEPCS.open strSQLEPCSAudit,con
    if not (rsEPCS.eof or rsEPCS.bof) then
        epcsAuditAuthor = rsEPCS("userid")
        epcsAuditRole = rsEPCS("role")
    end if
    rsEPCS.close

	%>

	<br/>
    <span id="epcs_prescriber_address"><%=RS("PRESCRIBERADDRESS") %></span><br>
	<%=RS("PRESCRIBERCITY")& ", " & RS("PRESCRIBERSTATE") & "  " & RS("PRESCRIBERZIP")%><br>
	Phone: <span id="epcs_prescriber_phone"><%=RS("PRESCRIBERPHONE") %></span><br/>
    DEA Number: <span id="epcs_prescriber_DEA"><% if DEANumber <> "" then response.Write(DEANumber) end if %></span><br/>
	<b><%=RS("PRESCRIBEREMAIL")%></b><br>
	</td>
	<td valign="top" width="33%">
	<div class="titleBox">Pharmacy</div>
	<span id="epcs_pharmacy_name"><%=RS("PHARMACYNAME")%></span><br />
	
	<%
	strSQL = "SELECT * FROM SS_PHARMACY WHERE NCPDPID = '" & RS("PHARMACYID") & "' and bitand(cast(nvl(servicelevel,'0') as varchar2(20)) ,  2048) = 2048"
	RS1.Open strSQL, con
	if not RS1.EOF then
	epcs_pharmacy = "Y"
	%>
	<img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" />
	<%
	else
	epcs_pharmacy = "N"
	end if
	RS1.Close
	%>

	<br/>
    <span id="epcs_pharmacy_address"><%=RS("PHARMACYADDRESS")%></span><br />
	<%=RS("PHARMACYCITY") & ", " & RS("PHARMACYSTATE") & "  " & RS("PHARMACYZIP") %><br>
    Phone: <span id="epcs_pharmacy_phone"><%= RS("PHARMACYPHONE")%></span><br />
	</td>
	<td valign="top">
	<div class="titleBox">Patient</div>
	<span id="epcs_patient_firstname"><%=RS("PATIENTFIRSTNAME")%></span> <span id="epcs_patient_lastname"><%=RS("PATIENTLASTNAME")%></span><br />
	
    <%
    missingAddressInfo = false 

    if IsNull(RS("PATIENTADDRESS")) or IsNull(RS("PATIENTCITY")) or IsNull(RS("PATIENTSTATE")) or IsNull(RS("PATIENTZIP")) then
        missingAddressInfo = true
    end if
	
	missingDEASchedule = false

    if IsNull(RS("DEASCHEDULE")) then
        missingDEASchedule = true
    end if
    %>

    <span id="epcs_patient_address"><%=RS("PATIENTADDRESS") %></span> <br />
	<% if not IsNull(RS("PATIENTADDRESS2")) then %>
		<span id="epcs_patient_address2"><%=RS("PATIENTADDRESS2") %></span> <br />
	<% end if %>
	<span id="epcs_patient_city"><%=RS("PATIENTCITY") %></span>, 
    <span id="epcs_patient_state"><%=RS("PATIENTSTATE") %></span>, 
    <span id="epcs_patient_zip"><%=RS("PATIENTZIP") %></span><br />
	Phone: <%= RS("PATIENTPHONE") %><br/>
    Gender: <span id="epcs_patient_gender"><%=RS("PATIENTGENDER")%></span><br />
    DOB: <span id="epcs_patient_dob"><%=RS("PATIENTDOB")%></span>
	<div id="showPatientLink" style="display:none;">
	    <br /><a href="../../patients/quick_add_patient.asp?first_name=<%if Not(IsNull(rs("PATIENTFIRSTNAME"))) then%><%=Escape(rs("PATIENTFIRSTNAME")) %><%end if%>&last_name=<%if Not(IsNull(rs("PATIENTLASTNAME"))) then%><%=Escape(rs("PATIENTLASTNAME")) %><%end if%>&bdate=<%=rs("prettydate") %>&gender=<%if Not(IsNull(rs("PATIENTGENDER"))) then%><%=Escape(rs("PATIENTGENDER")) %><%end if%>&phone_number=<%if Not(IsNull(rs("PATIENTPHONE"))) then%><%=Escape(rs("PATIENTPHONE")) %><%end if%>">Quick Add Patient</a>
	</div>
	</td>
</tr>	
<tr>
<%
		date_written=RS("MEDICATIONWRITTENDATE")
        if not isnull(date_written) then
			date_written= Mid(date_written, 5, 2)& "/" & Right(date_written, 2) & "/" & Left(date_written, 4)
		end if

		meddispense_date_written=RS("MEDDISPENSEWRITTENDATE")
		if not isnull(meddispense_date_written) then
			meddispense_date_written= Mid(meddispense_date_written, 5, 2)& "/" & Right(meddispense_date_written, 2) & "/" & Left(meddispense_date_written, 4)
		end if

		substitution = ""
		if not isnull(RS("MEDICATIONSUBSTITUTION")) then 
			if RS("MEDICATIONSUBSTITUTION") = "0" then 
				substitution = "Allowed"  
			elseif RS("MEDICATIONSUBSTITUTION") = "1" then 
				substitution = "Not allowed" 
			else 
				substitution = RS("MEDICATIONSUBSTITUTION")
			end if 
		end if

		meddispense_substitution = ""
		if not isnull(RS("MEDDISPENSESUBSTITUTION")) then 
			if RS("MEDDISPENSESUBSTITUTION") = "0" then 
				meddispense_substitution = "Allowed"  
			elseif RS("MEDDISPENSESUBSTITUTION") = "1" then 
				meddispense_substitution = "Not allowed" 
			else 
				meddispense_substitution = RS("MEDDISPENSESUBSTITUTION")
			end if 
		end if

		quantity_desc = ""
		if not isnull(RS("MEDICATIONQUANTITYDESC")) then
			quantity_desc = RS("MEDICATIONQUANTITYDESC")
		elseif not isnull(RS("MEDICATIONQUANTITYQUAL")) THEN
			quantity_desc = RS("MEDICATIONQUANTITYQUAL")
		end if

		meddispense_quantity_desc = ""
		if not isnull(RS("MEDDISPENSEQUANTITYDESC")) then
			meddispense_quantity_desc = RS("MEDDISPENSEQUANTITYDESC")
		elseif not isnull(RS("MEDDISPENSEQUANTITYQUAL")) THEN
			meddispense_quantity_desc = RS("MEDDISPENSEQUANTITYQUAL")
		end if
		
		' check dispensed medication for controlled substance.
        CS2 = "N"
		CS5 = "N"
		if not isnull(RS("MEDDISPENSECODE")) then
			RS1.Open "select CSA_Schedule from NDC_Drugs where ndc_code = '"&RS("MEDDISPENSECODE")&"' ",CONN
			if not RS1.EOF then
				Schedule = RS1("CSA_Schedule")
				if Schedule > 1 then
					ControlledSub = "C"
                    if Schedule = 2 then
                        CS2 = "Y"
                    end if
					if Schedule = 5 then
                        CS5 = "Y"
                    end if
				else
					ControlledSub = "N"
				end if
			else
				ControlledSub = ""
			end if
			RS1.Close 
		else
			ControlledSub = ""
		end if
		
		' check prescribed medication for controlled substance.
		PrescribeDEAScheduleCode = RS("DEASCHEDULEPRESCRIBE")
		Select Case PrescribeDEAScheduleCode
			Case "C48675"
				PrescribeScheduleNumber = 2
			Case "C48676"
				PrescribeScheduleNumber = 3
			Case "C48677"
				PrescribeScheduleNumber = 4
			Case "C48679"
				PrescribeScheduleNumber = 5
			Case Else
				PrescribeScheduleNumber = -1
		End Select
		
		if PrescribeScheduleNumber > 1 then
			PrescribeControlledSub = "C"
		else
			PrescribeControlledSub = "N"
		end if
 %>
	<td colspan=3 width="33%">
	<table width="100%"><tr><td width="50%" style="vertical-align:top;">
	<div class="titleBox">Medication Prescribed</div>
    <b><span id="epcs_medication_description"><%=RS("MEDICATIONDESCRIPTION")%></span></b>
	<% if not isnull(PrescribeDEAScheduleCode) then %>
        <img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /><br/>
        Schedule: 
        <% Select Case PrescribeDEAScheduleCode 
                Case "C48675"
                    response.Write("II")    
                Case "C48676"
                    response.Write("III")
                Case "C48677"
                    response.Write("IV")
                Case "C48679"
                    response.Write("V")
           End Select
        %>
    <%end if%>
    <br/>
	<p>
	NDC: <%=RS("MEDICATIONCODE")%><br>
	Dispense: <%=RS("MEDICATIONDIRECTIONS")%><br>
	Quantity: <span id="epcs_medication_dispense"><%=RS("MEDICATIONQUANTITY") %></span>&nbsp;<%=quantity_desc %>
	<br>

	Notes: <%=RS("MEDICATIONNOTE")%><br>
	Refill: <% if not isnull(RS("MEDICATIONREFILLQUAN")) and RS("MEDICATIONREFILLQUAN") <> "" then response.Write RS("MEDICATIONREFILLQUAN") else response.Write RS("MEDICATIONREFILLQUAL") end if %><BR>
    Substitution: <span id="epcs_medication_substitution"><%=substitution  %></span><br />
	Written Date: <%=date_written%>
	</td><td width="50%" style="vertical-align:top;">
	<div class="titleBox">Medication Dispensed</div>
	<b><%=RS("MEDDISPENSEDESCRIPTION")%></b> 
    <% if ControlledSub = "C" then %>
        <img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /><br/>
        Schedule: 
        <% Select Case Schedule 
                Case 2
                    response.Write("II")    
                Case 3
                    response.Write("III")
                Case 4
                    response.Write("IV")
                Case 5
                    response.Write("V")
           End Select
        %>
    <%elseif ControlledSub = "" then %>
         <b><font color="red">(No information about this substance in the system, allow to deny only)</font></b>
    <%end if%>
    <br/> 
	<p>
	NDC: <%=RS("MEDDISPENSECODE")%><br>
	Dispense: <%=RS("MEDDISPENSEDIRECTIONS")%><br>
	Quantity: <%=RS("MEDDISPENSEQUANTITY")%>&nbsp;<%=meddispense_quantity_desc %>
	<br>

	Notes: <%=RS("MEDDISPENSENOTE")%><br>
	Refill: <% if not isnull(RS("MEDDISPENSEREFILLQUAN")) and RS("MEDDISPENSEREFILLQUAN") <> "" then response.Write RS("MEDDISPENSEREFILLQUAN") else response.Write RS("MEDDISPENSEREFILLQUAL") end if %><BR>
    Substitution: <%=meddispense_substitution  %><br />
	Written Date: <%=meddispense_date_written%>
    </td></tr></table>
	</td>
</tr>
</table>
<table cellspacing = 2 cellpadding = 1 width="100%" border=1>
<form action="https://www.webedoctor.com/surescriptInterface/default_multiple.aspx" method="post" name="f1" onsubmit="return chk_refrsp()">
<tr><td valign="top" >
	<div class="titleBox">Refill Response Action</div>
<%
		NCPDPID = RS("PHARMACYID")
		subs_id=RS("MEDICATIONSUBSTITUTION")
		med_note=RS("MEDICATIONNOTE")
		no_refill=RS("MEDICATIONREFILLQUAN")
		note=RS("MEDICATIONNOTE")
		
	else
		Response.Write "This record does not exist any more."
	
	end if
%>


<%
drug_id = Request.QueryString("drug_id")
visit_key = Request.QueryString("visit_key")
patient_id = Request.QueryString("patient_id")

If (drug_id)="" or isnull(drug_id) then
   'check the patient name and display error message if any - start
'   strSQL="select refreq.drugid from ss_message newrx join ss_message refreq on refres.prescriberordernumber = newrx.prescriberordernumber where  messagetype='NewRX' and prescriberordernumber  in (select prescriberordernumber from ss_message where messageid =  '"& messageID &"')"
	strSQL = ""
	strSQL = strSQL & "    select refreq.drugid  "
	strSQL = strSQL & "    from  "
	strSQL = strSQL & "     ss_message newrx  "
	strSQL = strSQL & "     join ss_message_content newrx_content on newrx_content.messageid = newrx.messageid "
	strSQL = strSQL & "     join ss_message refreq on refreq.prescriberordernumber = newrx.prescriberordernumber  "
	strSQL = strSQL & "     join ss_message_content refreq_content on refreq_content.messageid = refreq.messageid "
	strSQL = strSQL & "     where  newrx.messagetype='NewRX' and refreq.messageid = '" & messageID & "' "
'	strSQL = strSQL & "       and upper(trim(nvl(newrx_content.PATIENTLASTNAME,''))) = upper(trim(nvl(refreq_content.PATIENTLASTNAME,'')))  "
'	strSQL = strSQL & "       and upper(trim(nvl(newrx_content.PATIENTFIRSTNAME,''))) = upper(trim(nvl(refreq_content.PATIENTFIRSTNAME,''))) "
'	strSQL = strSQL & "       and upper(trim(nvl(newrx_content.PATIENTGender,''))) = upper(trim(nvl(refreq_content.PATIENTGender,''))) "
'	strSQL = strSQL & "       and upper(trim(nvl(newrx_content.PATIENTDOB,''))) = upper(trim(nvl(refreq_content.PATIENTDOB,''))) "
'	strSQL = strSQL & "       and upper(trim(nvl(newrx_content.PATIENTID,''))) = upper(trim(nvl(refreq_content.PATIENTID,''))) "

   'Response.Write strSQL
   RS1. open strSQL ,conn
    if not (RS1.EOF or RS1.bof) then
		drugid1= rs1("drugid")
	else
		drugid1=""
	end if
	RS1.Close
    
else    
    drugid1 = drug_id
end if

    
    if drugid1 <> "" then
	    RS1.open "select patient_id from notes_medications where drug_id="&drugid1, conn
	    if not (RS1.EOF or RS1.bof) then
		    patient_id1= rs1("patient_id")
	    else
		    patient_id1=""
	    end if
	    RS1.Close
	else
	    patient_id1=""
	end if

    
	BIRTH_DATE1 = rs("PATIENTDOB")
	LAST_NAME1= rs("PATIENTLASTNAME")
	FIRST_NAME1= rs("PATIENTFIRSTNAME")

    if patient_id1 <> "" then	
		pat_name_match="YES"
	else
    	pat_name_match="NO"
    end if
%>

<% if pat_name_match="NO" then %>
  <p align=left><b><font color=red>Note: WebEDoctor was not able to link this request to a patient visit.</font></b><br/>
  
<script type="text/javascript">
    document.getElementById('showPatientLink').style.display = 'inline';
</script>  
  
<%end if %>

<%if ControlledSub = "C" and epcs_prescriber <> "Y" then %><span style="color: Red"><b>* Prescriber is not set up EPCS Service Level</b></span><br/><%end if %>
<%if ControlledSub = "C" and epcs_pharmacy <> "Y" then %><span style="color: Red"><b>* Pharmacy is not set up EPCS Service Level</b></span><br/><%end if %>
<%if ControlledSub = "C" and missingAddressInfo = true then %><span style="color: Red"><b>* Missing patient's address information</b></span><br/><%end if %>
<%if ControlledSub = "C" and missingPrescriberName = true then %><span style="color: Red"><b>* Missing prescriber's first name or last name</b></span><br/><%end if %>
<%if ControlledSub = "C" and missingDEASchedule = true then %><span style="color: Red"><b>* The refill request message from Surescripts for this controlled substance is missing DEA Schedule</b></span><br/><%end if %>
<%if ControlledSub = "C" and CS2 = "Y" then %><span style="color: Red"><b>* Schedule II controlled substances cannot be refilled</b></span><br/><%end if %>
<input type="hidden" name="is_sub" value="<%if ControlledSub = "C" then %>Y<% else %>N<%end if %>" />

<% if PrescribeControlledSub = "C" then %>
	<input type="hidden" name="ScheduleCode" value="<%=PrescribeDEAScheduleCode %>" />
<%end if%>

  <select name="Response_Type" width="20" onchange="ChooseAction(this.value);">
		
		<option value="None">Please choose an action below.</option>
		
		<% if ControlledSub = "N" then %>
		<option value="Approved">Approved</option>
		<% end if %>
		
		<option value="Denied">Denied</option>
		
		<% if ControlledSub = "N" then %>
		<option value="DeniedNewPrescriptionToFollow">Denied W/ New RX To Follow</option>
		<% end if %>
  </select>
<%

' get physician ID
'strSQL="select PRESCRIBED_BY,formulation from notes_medications where drug_id='" & drug_id & "'"

'rs1.open strSQL,con		
'If not (rs1.EOF or rs1.BOF) then
'	Physician_ID=rs1("PRESCRIBED_BY")
'End if
'rs1.close

duplicate_key = request("duplicate_key")

multiples = 1

messageids = messageid & ","


if not isnull( duplicate_key ) and duplicate_key <> "" then

    multiples = ubound(split(duplicate_key,",")) + 2
    messageids = messageids  & duplicate_key & ","

end if



%>
</td></tr>
<input type="hidden" name="selected_mid" value="<%=multiplyString(drug_id, multiples)%>">
<input type="hidden" name="Visit_Key" value="<%=multiplyString(visit_key, multiples)%>">
<input type="hidden" name="Patient_Id" value="<%=multiplyString(patient_id, multiples)%>">
<input type="hidden" name="MsgType" value="<%=multiplyString("RefillResponse", multiples)%>">
<input type="hidden" name="NCPDPID" value="<%=multiplyString(NCPDPID, multiples)%>">
<input type="hidden" name="Physician_ID" value="<%=multiplyString(Physician_ID, multiples) %>">
<input type="hidden" name="Physician_PON" value="<%=multiplyString(rs("PRESCRIBERID"), multiples) %>">
<input type="hidden" name="date_written" value="<%=multiplyString(date_written, multiples)%>">
<input type="hidden" name="request_messageid" value="<%=messageids%>">
<input type="hidden" name="Refills" value="<% if not isnull(RS("MEDICATIONREFILLQUAN")) and RS("MEDICATIONREFILLQUAN") <> "" then response.Write multiplyString(RS("MEDICATIONREFILLQUAN") + 1, multiples) else response.Write multiplyString(RS("MEDICATIONREFILLQUAL"), multiples)%>">
<input type="hidden" name="no_refill">
<input type="hidden" name="Response_Reason">

<input type="hidden" name="pharmacy_refills" value="<% if not isnull(RS("MEDICATIONREFILLQUAN")) and RS("MEDICATIONREFILLQUAN") <> "" then response.Write RS("MEDICATIONREFILLQUAN") else response.Write RS("MEDICATIONREFILLQUAL")%>">

<input type="hidden" name="DPSNumber" value="<%=DPSNumber %>" />
<input type="hidden" name="DEANumber" value="<%=DEANumber %>" />
<input type="hidden" name="DEANumberChange" value="<%=DEANumberChange %>" />

<input type="hidden" name="epcsAuditAuthor" value="<%=epcsAuditAuthor %>" />
<input type="hidden" name="epcsAuditRole" value="<%=epcsAuditRole %>" />

</table>
<p>
<div id="Approved" style="display:none;">
<%if ControlledSub = "C" and CS2 = "Y" then %>
    <span style="color: Red">* No more than a single dispense is allowed.</span><br/><br/>
<%elseif ControlledSub = "C" then %>
    <span style="color: Red">* PRN is not allowed for controlled substance.</span><br/><br/>
<%end if %>
&nbsp;&nbsp;&nbsp;&nbsp;Total Number of Refills Allowed: <span id="no_refill_one">1 + </span>
	  <select name="no_refill_select" SIZE="1" onchange="noRefillChange(this.value)" <%if ControlledSub = "C" and CS2 = "Y" then%>disabled<%end if%>>	
		<%
		  for i = 1 to 99
		%>
            <%if i < 6 then %>
		        <option value="<%= i %>" <%if ControlledSub = "C" then Response.write"" else if cstr(i) = no_refill then Response.write " selected " end if end if %>><%=(i-1)%></option>
            <%else %>
                <%if ControlledSub = "C" and CS5 <> "Y" then %>
		            <option disabled value="<%= i %>" <%if ControlledSub = "C" then Response.write"" else if cstr(i) = no_refill then Response.write " selected " end if end if %>><%=(i-1)%></option>
                <%else %>
                    <option value="<%= i %>" <%if ControlledSub = "C" then Response.write"" else if cstr(i) = no_refill then Response.write " selected " end if end if %>><%=(i-1)%></option>
                <%end if %>
            <%end if %>
		<%
		  next
		%>
		
		<%if ControlledSub <> "C" then %>
		    <option value="999" <%if ControlledSub = "C" then Response.write " disabled " end if %> <% if "999" = no_refill then Response.write " selected " end if %>>PRN</option>
        <%end if %>
	  </select> 
	  <br>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<div id = "Note" style="display:none;">
<b>Note:&nbsp;</b><span id="epcs_medication_note"><textarea name="Response_Note" rows="2" cols="35" onblur="return checklength(70);"></textarea></span><br />
Date Issued: <%=date()%><br />
</div>
<!--<div id = "DenialReason" style="display:<% if schedule="2" then response.write "block" else response.write "none" %>">-->
<div id = "DenialReason" style="display:none">
<select name="Response_Reason_Code">
			<option value="">Select Denial Reason</option>
				<%
				rs1.open "SELECT * FROM SS_DENIALREASON", con
				while not rs1.eof
				%>
					<option value="<%=rs1("DENIAL_CODE")%>"><%=rs1("DENIAL_desc")%></option>
				<%
				 rs1.movenext
				wend
				rs1.close
				%>
				</select>			
</div>
</p>
<center>

<table id="epcs_signtwofactor" cellspacing = "2" cellpadding = "1" width="100%" border="1" style="display:none;">
<tr>
	<td valign="top">
		<div class="titleBox">Sign with Two-Factor authentication</div>
		<input type="text" name="epcs_twofactor_userid" value="<%=epcs_twofactor_userid %>" style="display: none;"/>
		<table cellspacing = "2" cellpadding = "1" width="100%" border="0">
			<tr>
				<td>
					Password:
				</td>
				<td>
					<input type="password" name="epcs_twofactor_password" id="epcs_twofactor_password" />
					(Your password via IdenTrust)
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					OTP Code:
				</td>
				<td>
					<input type="text" name="epcs_twofactor_otpcode" id="epcs_twofactor_otpcode" />
					(Get this code from your OTP token device)
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="4">
					<p>By completing the two-factor authentication protocol at this time,
					you are legally signing the prescription(s) and authorizing the transmission of the above information
					to the pharmacy for dispensing. The two-factor authentication protocol may only be completed by the practitioner
					whose name and DEA registration number appear above.
					</p>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>

<input type=submit value="Submit" id="btnSubmit" name=a1 onclick="return CheckEPCSPrescription();">			
<input type=button value="Cancel" onclick="javascript:window.close();" id=button2 name=button2>
</center>
<img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /> Controlled sustance.
<br/><br/>
<font size="2">* CS III & CS IV: possible refill number is limited to 5</font>
</form>
</BODY>
</HTML>
