<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%


select case Session("role")
  case "frontdesk"
    infoTable = "fdreg"
    userColumn = "logid"
    first_name_ = "first_name"
    middle_name_ = "middle"
    last_name_ = "last_name"
    tel_num = "tel"
    address = "address"
    
    
  case "admin"
    infoTable = "streetreg"
    userColumn = "userid"
    first_name_ = "first_name"
    middle_name_ = "middle"
    last_name_ = "last_name"
    tel_num = "tel"
    address = "addr1"
    address2 = "addr2"
    
  case "physician"
    infoTable = "doctortable"
    userColumn = "docuser"
    first_name_ = "first_name"
    middle_name_ = "middle"
    last_name_ = "last_name"
    tel_num = "phone"
    address = "street"
     
  case "biller"
    infoTable = "billerreg"
    userColumn = "logid"
    first_name_ = "first_name"
    middle_name_ = "middle"
    last_name_ = "last_name"
    tel_num = "tel"
    address = "address"
     
    
      
  case "nurse"
    infoTable = "nursereg"
    userColumn = "logid"
    first_name_ = "first_name"
    middle_name_ = "middle"
    last_name_ = "last_name"
    tel_num = "tel"
    address = "address"
    
end select
    %> 

    <% 
dim RSG
dim QueryRS

set RSG = Server.CreateObject("ADODB.Recordset")

QueryRS = "SELECT * FROM " & infoTable & " WHERE " & userColumn & " = '" & Session("user") & "'"
RSG.Open QueryRS, CONN
role_names = RSG("first_name") & " " & RSG("last_name")
RSG.MoveNext
 
if role_names = " " Then
   role_names = Session("user_update")
  
 End if
 
 
 %>






<%
Response.Expires = -1
%>

<%
dim auditObj
set auditObj = new clsAudit

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_list = Request("list")
call auditObj.LogActivity("Accessing Medications Print page","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)

if request("sig") = "true" then
	show_ds = 1
end if
if request("necessary") = "true" then
	show_mn = 1
end if

if session("docids") <> "" then
  docids  = replace(session("docids"),",","','")
  docids =replace(docids," ","")
  phy_id=" docuser in ( '"&docids&"')"
end if

%>
<html>
<head>
<title>Medications &gt; Print Prescription</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
     <script language="JavaScript" src="../../../library/HtmlPost.js"></script> 	
     <link rel="stylesheet" href="../../../library/SendFax.css" type="text/css">
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">

<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
</head>

<body onload="cacheOff()">
<div id="MainDIV" style="display:none;" >
<table >

<% set RS9 = Server.CreateObject("ADODB.Recordset")%>
<%RS9.Open "SELECT ACC_ID  FROM FaxSending_Rights WHERE COVERLETTER = 1 AND ACC_ID = '" & SQLFixUp(session("pid")) & "' ", CONN%>                  
                   <%while not RS9.EOF%>
                                                <%if RS9("ACC_ID") = session("pid") then
                                                              status1="selected"
                                                       else
                                                              status1=""
                                                       end if                                   
                                                %>                                                
                          <%    
                                                RS9.MoveNext
                                                wend
                                                set rs9=nothing                                                   
                          %>  
                                     
                   <% if status1 <> "" then %>
                    <!--#include file="../../../library/fax_coverLetter.inc"-->
                  <%   else %>
                      <!--#include file="../../../library/fax.inc"-->

<% end if %>

    <input type="hidden" name="facilityid" id="facilityid" value="<%=session("pid")%>"> 
    <input type="hidden" name="role" id="role" value="<%=role_names%>"> 
    <input type="hidden" name="reportsubject"   id="reportsubject" value="Prescription"> 
</table>
</div>

<div id="report_data">

<div class="titleBox" style="background-color: #FFF; width: 550px; font-weight: normal; align: center; vertical-align: middle;">

<table width="100%">
<%

set res = server.CreateObject("adodb.recordset")
set res1 = server.CreateObject("adodb.recordset")

strsql="select doctor_id from visit where visit_key='"&visit_key&"'"
res.open strsql,CONN
doc_id= res("doctor_id")
res.Close

strsql="select b.orgname,a.first_name,a.last_name,a.middle_name,a.credential,a.street,a.city,a.state,a.zip,a.wphone,a.ext,a.fax,a.email,a.provider_licno,a.provider_licnostate,a.dea_no,a.dps_no,a.extra,a.speciality from  doctortable a,streetreg b where a.FACILITY  = b.streetadd and a.docuser='"&doc_id&"'"
res.open strsql,CONN

name=res("first_name")&" "&res("middle_name")&" "&res("last_name")&", "&res("credential")
credential = res("credential")

dim res2
set res2 = Server.CreateObject("ADODB.Recordset")

res2.open "select * from pharmreg where id='"&SQLFixUp(request("pharmacy"))&"' and pid='"&session("pid")&"' order by org_name asc", con

%>
<tr>
<td valign="top"><img src="/images/practice/rx.gif" border="0" align="left" WIDTH="46" HEIGHT="48"></td>
<td align="left" width="50%" valign="top">

<%
if not res2.EOF then
%>
<b><%=res2("org_name")%></b><br>
<% if res2("first_name") <> "" then %>
ATTN: <%=res2("first_name")%><br>
<% end if %>
<%=res2("address")%><br>
<%=res2("city")&", "&res2("state")&"  "&res2("zip")%><br>
<% if res2("tel") <> "" then %>
Tel: <%=res2("tel")%><br>
<% end if %>
<% if res2("fax") <> "" then %>
Fax: <%=res2("fax")%><br>
<% end if %>
<%
end if
%>
</td>
<td align="right" width="50%" valign="top">
<%=res("orgname")%> <br>
<% if session("pid") <> "JON1176" then %> <b><%=name%></b><br><% end if %>
<% if res("extra")<>"" then Response.Write "Collaborating Phy: "&res("extra")&"<br>" %>
<%=res("street")%><br>
<%=res("city")&", "&res("state")&"  "&res("zip")%><br>
Tel: <%=res("wphone")%><br>
<% if res("fax") <> "" then %>
Fax: <%=res("fax")%><br>
<% end if %>
DEA#&nbsp;<%=res("dea_no")%><br>
<% if res("dps_no")<>"" then Response.Write "DPS# "&res("dps_no")&"<br>" %>
License#&nbsp;<%=res("provider_licno")%>
</td>
</tr>
</table>

<%
res.close
res1.open "select first_name,middle_name,last_name,birth_date,HOME_PHONE,street,city,state,zip,primary_insurance,patient_employer,ssn from patienttable where userid='"&patient_id&"'",CONN
%>
<p>
<div class="titleBox_HL" style="background-color: #FFF; font-weight: normal">
<b>Patient</b>
<p>
<%=res1("first_name")&" "&res1("last_name")%><br>
<%=res1("street")%><br>
<%=res1("city")&", "&res1("state")&" "&res1("zip")%><br>
<b>Phone:</b> <%=res1("HOME_PHONE")%><br>
<b>DOB:</b> <%=res1("birth_date")%>
</div>

<%
res1.close
%>
<p>
<%
res1.open "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and drug_id in (" & drug_list & ")",CONN
if not res1.eof then
  while not res1.eof
%>
<div class="titleBox_HL2" style="background-color: #FFF; font-weight: normal">
<img src="/images/practice/rx.gif" border="0" HEIGHT="12" align="left"> <b><%=res1("drug_name")%></b><p>
Formulation: <%=res1("formulation")%><br>
<% if trim(res1("dispense").value) <> "" then %>
Dispense: <%=res1("dispense")%><br>
<% end if %>
Dose: <%=res1("dose")%><br>
Frequency: <%=res1("frequency")%><br>
Route: <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %><br>
Refill: <%=res1("refill")%><p>
<%=res1("notes")%>
</div>
<p>
<%
    res1.MoveNext
  wend
end if
%>

<p>
<table height="100">
<tr>
<td valign="left">
<span style="font-size: 24px">X</span>
<%
if show_mn = 1 then
	session("authentication") = "1"
%>
<img src="/practice/customization/signature/display_signature.asp">
<%
else
%>
_____________
<% end if %>
,<%=credential%> 
<span style="font-size: 24px">X</span>
<%
if show_ds = 1 then
	session("authentication") = "1"
%>
<img src="/practice/customization/signature/display_signature.asp">
<%
else
%>
_____________
<% end if %>
,<%=credential%>
</td></tr>
<tr><td valign="middle">
Substitution  Permitted&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dispense as Written
<br><%=date()%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=date()%>
</td></tr>
</table>

</div>
</div>
</body>
</html>
