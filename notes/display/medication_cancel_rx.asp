<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%
'dim auditObj
'set auditObj = new clsAudit

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id_hidden")

'call auditObj.LogActivity("Accessing Medications Order","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script src="../../../library/jquery-1.10.2.min.js"></script>
<style type="text/css">
</style>
<script language="javascript" type="text/javascript">

function setmedval(obj, x) {
    type = x.split(',')
    document.medprint.selected_mid.value=type[0];
    document.medprint.Physician_ID.value = type[1];
    document.medprint.NCPDPID.value = type[2];
    document.medprint.NewRx_MessageId.value = type[3];
}

</script>

</head>
<body>
<div class="titleBox">Medications &gt; Cancel Prescription Electronically</div>
<%
dim prescriberInfo
set drRS = server.CreateObject("ADODB.Recordset")
sqlQuery = "select * from doctortable where docuser='" & session("docid") & "'"
drRS.open sqlQuery,con

if not (drRS.EOF or drRS.BOF) then
    prescriberInfo = drRS("first_name") & " " & drRS("last_name") & ","
    prescriberInfo = prescriberInfo & drRS("street") & ", " & drRS("city") & ", " & drRS("state") & ", " & drRS("zip") & ", Phone: " & drRS("phone")
end if

drRS.close
%>
<p><b>Prescriber:</b> <%=prescriberInfo%></p>
<p>
Columns highlighted in <span style="background-color: #FFFFCC">yellow</span> indicate medication prescribed during this visit.
<p>

<form action="http://192.168.73.4/surescriptInterface/default.aspx" method="post" name="medprint" id="medprint">

<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">

  <tr bgcolor="#3366CC">
    <td width="2%"></td>
    <td width="6%"><font color="white"><b>Start Date</td>
    <td width="6%"><font color="white"><b>Written Date</td>
    <td width="18%"><font color="white"><b>Prescription</td>
    <td width="18%"><font color="white"><b>Detail for EPCS</td>
    <td width="18%"><font color="white"><b>Note to Pharmacist</td>
	<td width="17%"><font color="white"><b>Current Pharmacy</td>
    <td width="15%"><font color="white"><b>Current Order Status</td>
  </tr>


<%

set res1 = server.CreateObject("ADODB.Recordset")
set rs = server.CreateObject("ADODB.Recordset")

'Ignore wrong NDC medication
sqlQuery =	"select a.*, b.route_description,c.doctor_id as doctor_id, " &_
      "case when a.modified_date is not null then a.modified_date else a.prescribed_date end as last_action_date " &_
			"from notes_medications a, multum_route b, visit c, ndc_brand_name aa, multum_product_strength bb, ndc_main_multum_drug_code cc, ndc_core_description dd, multum_dose_form ee, ss_message sm " &_
			"where a.route=b.route_abbr(+) and a.status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "' and a.medical_type = 'DRU' " &_
			"and a.ndc_code = dd.ndc_code " &_
			"and aa.brand_code = dd.brand_code " &_
			"and dd.main_multum_drug_code = cc.main_multum_drug_code " &_
			"and bb.product_strength_code = cc.product_strength_code " &_
			"and cc.dose_form_code = ee.dose_form_code " &_
			"and(dd.obselete_date is null or dd.obselete_date > sysdate) " &_
			"and regexp_replace(lower(trim(a.drug_name)), '[#]', '') = regexp_replace(lower(trim(aa.brand_description)), '[#]', '')" &_
			"and regexp_replace(lower(trim(a.formulation)), '[#]', '') = regexp_replace(lower(concat(concat(trim(bb.product_strength_description), ' '), trim(ee.dose_from_description))), '[#]', '') " &_
			"and a.drug_id = sm.drugid " &_
			"and sm.messagetype = 'NewRX' and sm.Status != 'Error' " &_
      "UNION " &_
      "select a.*, b.route_description,c.doctor_id as doctor_id, " &_
      "case when a.modified_date is not null then a.modified_date else a.prescribed_date end as last_action_date " &_
			"from notes_medications a, multum_route b, visit c, ss_message sm " &_
			"where a.route=b.route_abbr(+) and a.status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "' and a.medical_type = 'SP' " &_
			"and a.drug_id = sm.drugid " &_
			"and sm.messagetype = 'NewRX' and sm.Status != 'Error' " &_
      "order by last_action_date desc"

'sqlQuery = "select a.*, b.route_description,c.doctor_id as doctor_id from notes_medications a, multum_route b, visit c where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "'"
'sqlQuery = "select a.*, b.route_description,a.PRESCRIBED_BY as doctor_id from notes_medications a, multum_route b where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.patient_id='" & patient_id & "'"

'Response.Write sqlQuery

res1.open sqlQuery,con
if not res1.eof then
	cnt = 0
	while not res1.eof
		'Get sent newrx message id and pharmacy id/servicelevel
		strSQL=" select MessageId, PharmacyId from SS_MESSAGE where MessageType = 'NewRX' and Status != 'Error' and DrugID='" & res1("drug_id") & "' order by timesent desc"
		rs.open strSQL,con

		If not (rs.EOF or rs.BOF) then
			NewRx_MessageId = rs("MessageId")
			NCPDPID = rs("PharmacyId")
		else
			newrx_messageid = ""
			NCPDPID = ""
		End if
		rs.close

		If NCPDPID <> "" then
			strSQL = "select servicelevel, storename, addressline1, city, state, zip, phoneprimary from ss_pharmacy where NCPDPID ='" & NCPDPID & "'"
			rs.open strSQL,con
			Current_Ph_Fullname= rs("storename")&" "&rs("addressline1")&" "&rs("city")&" "&rs("state")&" "&rs("zip")&" PH: "&rs("phoneprimary")
            Current_Ph_ServiceLevel = rs("servicelevel")
			rs.Close
		end if

        'Only display pharmacies with CancelRx Service Level
        if (NCPDPID <> "" and (Current_Ph_ServiceLevel and 16) = 16) then

		    Physician_ID=res1("doctor_id")
		    if res1("visit_key") = visit_key then
			    bgcolor = "#FFFFCC"
		    else
			    if cnt mod 2 = 0 then
                    bgcolor = "#EFEFEF"
			    else
				    bgcolor = "#FFFFFF"
			    end if
		    end if

		    'check for the contolled substance drug.....
		    'sqlQuery = "select * from SS_Substances where schedule='III' and (upper(drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%' or upper(SYNM) like '"& ucase(left(res1("drug_name"),7)) & "%' )"
		    'sqlQuery = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and upper(drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%'"
		    sqlQuery = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and upper(brand_name) = '" & ucase(res1("drug_name")) &"'"
		    sqlQuery = "select a.drug_id, b.ndc_code  from notes_medications a,ndc_drugs b " &_
					    " where a.ndc_code = b.ndc_code " &_
					    " and a.ndc_code = '" & res1("ndc_code") & "'" &_
					    " and csa_schedule in (2,3,4,5)" &_
					    " and upper(a.drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%'"

		    rs.open sqlQuery,con
		    if not (rs.eof or rs.bof) then
			    Is_CS="Y"
		    else
			    Is_CS="N"
		    end if
		    rs.close

		    'get surescript status of the medication -start
		    strSQL=" select MessageType,STATUS, STATUSDESC, REFRESPONSECODE, CancelCode, CancelDesc from SS_MESSAGE where DrugID='" & res1("drug_id") & "' order by timesent desc"
		    rs.open strSQL,con

		    If not (rs.EOF or rs.BOF) then
			    ss_status=rs("STATUS")
			    ss_status_desc=	rs("STATUSDESC")
			    ss_MsgType= rs("MessageType")
			    ss_refrep= trim(rs("REFRESPONSECODE"))

			    if ss_MsgType = "CancelRx" or ss_MsgType = "CancelRxResponse" then
				    ss_status = rs("CancelCode")
				    ss_status_desc = rs("CancelDesc")
			    end if
		    else
			    ss_status=""
			    ss_status_desc=""
			    ss_MsgType=""
			    ss_refrep=""
		    End if
		    rs.close
		    '--------end

            substitution = ""
            if not isnull(res1("substitution")) then
                if res1("substitution") = "0" then
                    substitution = "Allowed"
                elseif res1("substitution") = "1" then
                    substitution = "Not allowed"
                else
                    substitution = res1("substitution")
                end if
            end if

%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top">
			<input type="radio" name="medi_sel" value="<%=res1("drug_id")%>,<%=Physician_ID%>,<%=NCPDPID%>,<%=NewRx_MessageId%>" onclick="setmedval(this, this.value)" />
        </td>
        <td valign="top"><b><%=res1("prescribed_date")%></td>
        <td valign="top"><b><%=date()%></b></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b>
          <%=res1("formulation")%><%if Is_CS="Y" then%><img alt="controlled substance" src="/images/practice/red_star.gif" border="0" /><%end if%> <br>

	      <b>Dispense:</b> <%=res1("dispense")%><br>

	      <b>Refill:</b> <%If cint(res1("refill"))=999 then Response.Write "PRN" else Response.Write res1("refill") end if%>

	      <br><b>Directions:</b> <%=res1("dose") & " " & res1("frequency")%><% if res1("route") <> "" then Response.write("-" & PCase(res1("route")))%>
	      <br /><b>Substitution:</b> <%=substitution %>
	    </td>
		<td>
            <%
                sqlEPCS = "SELECT v.VISIT_KEY, VISIT_DATE, VISIT_TIME " &_
                         "FROM VISIT v " &_
                         "INNER JOIN (SELECT VISIT_KEY FROM (SELECT * FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC) WHERE rownum = 1) p " &_
                         "ON v.VISIT_KEY = p.VISIT_KEY "

                set rsEPCS = server.CreateObject("ADODB.Recordset")
                rsEPCS.open sqlEPCS,con

                if not (rsEPCS.EOF or rsEPCS.BOF) then

                    set rsEPCS2 = server.CreateObject("ADODB.Recordset")

                    sqlEPCS2 = "SELECT Status, Created_Date, ss_message_id FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC"

                    rsEPCS2.open sqlEPCS2,con
                    while not rsEPCS2.EOF
                        if UCase(rsEPCS2("Status")) <> "NEW" then
                            response.Write("- <b>" & rsEPCS2("Status") & "</b> ")
                            if UCase(rsEPCS2("Status")) <> "PRINTED" then
                                response.Write("signed on " & rsEPCS2("Created_Date"))
                                checkChangeResult = FUNC_CHECKCHANGES(rsEPCS2("ss_message_id"))
                                if checkChangeResult <> "VALID" then
                                    response.Write(" <span style=""color: red; text-decoration: underline; cursor: pointer;"" onclick=""showIllegalChanges('" & checkChangeResult & "');"">Changed!</span>")
                                end if
                                response.Write("<br/>")
                            else
                                response.Write("on " & rsEPCS2("Created_Date") & "<br/>")
                            end if                            
                        end if
                        rsEPCS2.movenext
                    wend
                    rsEPCS2.close

                    response.Write("- <b>Encounter:</b> " & rsEPCS("VISIT_DATE") & " " & rsEPCS("VISIT_TIME"))
                    if rsEPCS("VISIT_KEY") = visit_key then
                        response.Write(" (Current)")
                    end if

                    sqlEPCS2 = "SELECT VISIT_ID FROM VISIT WHERE VISIT_KEY = '" & visit_key & "'"

                    rsEPCS2.open sqlEPCS2,con
                    if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                        visit_id_current = CLng(rsEPCS2("VISIT_ID"))
                    end if
                    rsEPCS2.close

                    sqlEPCS2 = "SELECT VISIT_ID FROM VISIT WHERE VISIT_KEY = '" & rsEPCS("VISIT_KEY") & "'"

                    rsEPCS2.open sqlEPCS2,con
                    if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                        visit_id_newest = CLng(rsEPCS2("VISIT_ID"))
                    end if
                    rsEPCS2.close

                    if visit_id_current < visit_id_newest then
                        %>
                        <span class="epcsStatus" style="display: none;">NO</span>
                        <%
                    elseif visit_id_current > visit_id_newest then
                        %>
                        <span class="epcsStatus" style="display: none;">READY</span>
                        <%
                    else
                        sqlEPCS2 = "SELECT STATUS FROM (SELECT * FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC) WHERE rownum = 1"

                        rsEPCS2.open sqlEPCS2,con
                        if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                            if UCase(rsEPCS2("STATUS")) = "PRINTED" then
                                %>
                                <span class="epcsStatus" style="display: none;">PRINTED</span>
                                <% 
                            else
                                %>
                                <span class="epcsStatus" style="display: none;">READY</span>
                                <% 
                            end if
                        end if
                        rsEPCS2.close
                    end if

                end if
                rsEPCS.close
            %> 
        </td>
	    <td class="st" valign="top"><%=res1("notes")%></td>
		<td valign="top"><%=Current_Ph_Fullname%></td>
	    <td valign="top">
	    <%If ss_MsgType<>"" then%>
			<b><a onMouseOver="return overlib('<%=ss_status_desc%>',LEFT,STICKY,CAPTION,'Electronic Submission Response');" onmouseout="return nd();"><%=ss_MsgType%> - <%=ss_refrep%> - <%=ss_status%></a><br>
			<%end if%>
	    </td>
	</tr>
<%
		    cnt = cnt + 1
        end if
		res1.movenext
	wend
	res1.Close

    if (cnt = 0) then
        response.write("<tr><td colspan='8'>All medications sent to pharmacies that don't have CancelRx permission.</td></tr>")
    end if
else
%>
	<tr>
		<td colspan="8">No medications found on record.</td>
	</tr>
<%
end if
%>
</table>


<p></p>
<div>
<input type="hidden" name="NCPDPID" id="NCPDPID" />
<input type="hidden" name="visit_key" value="<%=visit_key%>" />
<input type="hidden" name="patient_id" value="<%=patient_id%>" />
<input type="hidden" name="MsgType" value="CancelRx" />
<input type="hidden" name="Physician_ID" />
<input type="hidden" name="Current_Physician_ID" value="<%=session("user") %>" />
<input type="hidden" name="selected_mid" />
<input type="hidden" name="NewRx_MessageId" />
</div>

<!--<font color="red" size="2"><b>*</b></font><font size="1"> Controlled substance,can not be sent electronacally</font>-->
<img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /> Controlled substance
<br/>
<input type="submit" value="Cancel Prescription" id="s1" name="s2"/>
<input type="button" onclick="document.location='/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Cancel" id="button4" name="button4" />
</form>

<br />
<p align="center">
<img src="/images/global/stop.gif" border="0">
<font size="3" color="red"><b>Please do NOT order hand written or free text Medications written prior to 2015 electronically.</b></font>
</p>

</body>
</html>

<%
FUNCTION FUNC_CHECKCHANGES(ssMessageId)
    Set oXMLHTTP = CreateObject("MSXML2.XMLHTTP")
    Set oXMLDoc = CreateObject("MSXML2.DOMDocument")
    Server.ScriptTimeout=10000
    dim url: url = "http://192.168.72.8/surescriptInterface/EPCS_Signature_Page.aspx?runType=verify&ssMessageId="&ssMessageId
    call oXMLHTTP.open("GET",url,false)
    call oXMLHTTP.setRequestHeader("Content-Type","application/x-www-form-urlencoded")

    call oXMLHTTP.send()

    FUNC_CHECKCHANGES = oXMLHTTP.responseText
END FUNCTION

%>
