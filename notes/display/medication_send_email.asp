<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_list = Request("list")
mail_body="To:" & vbCrLf & vbCrLf

set res = server.CreateObject("adodb.recordset")
set res1 = server.CreateObject("adodb.recordset")
set res2 = Server.CreateObject("ADODB.Recordset")

strsql="select doctor_id from visit where visit_key='"&visit_key&"'"
res.open strsql,CONN
doc_id= res("doctor_id")
res.Close

strsql="select email from doctortable where docuser = '"& Session("user") &"'"
res.open strsql,CONN
mail_from = res("email")
res.Close

strsql="select b.orgname,a.first_name,a.last_name,a.middle_name,a.credential,a.street,a.city,a.state,a.zip,a.wphone,a.ext,a.fax,a.email,a.provider_licno,a.provider_licnostate,a.dea_no,a.speciality from  doctortable a,streetreg b where a.FACILITY  = b.streetadd and a.docuser='"&doc_id&"'"
res.open strsql,CONN

name=res("first_name")&" "&res("middle_name")&" "&res("last_name")&", "&res("credential")
credential = res("credential")

res2.open "select * from pharmreg where id='"&SQLFixUp(request("pharmacy"))&"' and pid='"&session("pid")&"' order by org_name asc", con

if not res2.EOF then
	mail_to = trim(res2("email"))
	mail_body= mail_body & res2("org_name") & vbCrLf
	if res2("first_name")<>"" then
		mail_body= mail_body &"ATTN:" & res2("first_name") & vbCrLf
	end if
	mail_body= mail_body & res2("address") & vbCrLf & res2("city") &" "&res2("state")&" "& res2("zip") & vbCrLf
	mail_body= mail_body & "Phone: "&res2("tel") & vbCrLf
	mail_body= mail_body & "Fax: "&res2("fax") & vbCrLf & vbCrLf
end if

mail_body= mail_body & "From: "& vbCrLf & vbCrLf
mail_body= mail_body & res("orgname")& vbCrLf
mail_body= mail_body & res("street") & vbCrLf
mail_body= mail_body & res("city")&", "&res("state")&"  "&res("zip") & vbCrLf
mail_body= mail_body & "Phone: "&res("wphone") & vbCrLf
mail_body= mail_body & "Fax: "&res("fax") & vbCrLf
mail_body= mail_body & "DEA# "&res("dea_no") & vbCrLf
mail_body= mail_body & "License# "&res("provider_licno") & vbCrLf & vbCrLf

res.close
res1.open "select first_name,middle_name,last_name,birth_date,street,city,state,zip,primary_insurance,patient_employer,ssn from patienttable where userid='"&patient_id&"'",CONN

mail_body= mail_body & "Re: Patient Name: "&res1("first_name")&" "&res1("last_name") & vbCrLf
mail_body= mail_body & res1("street") & vbCrLf
mail_body= mail_body & res1("city")&", "&res1("state")&" "&res1("zip") & vbCrLf
mail_body= mail_body & "DOB: "&res1("birth_date") & vbCrLf & vbCrLf

res1.close

res1.open "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and drug_id in (" & drug_list & ")",CONN
if not res1.eof then
  while not res1.eof
		mail_body= mail_body & res1("drug_name") & vbCrLf
		mail_body= mail_body & "Formulation: "&res1("formulation") & vbCrLf
		if trim(res1("dispense").value) <> "" then
			mail_body= mail_body & "Dispense: "&res1("dispense") & vbCrLf
		end if
		mail_body= mail_body & "Dose: "&res1("dose") & vbCrLf
		mail_body= mail_body & "Frequency: "&res1("frequency") & vbCrLf
		if res1("route_description") <> "" then
			mail_body= mail_body & "Route: " &PCase(res1("route_description")) & vbCrLf
		else
			mail_body= mail_body & "Route: " &PCase(res1("route"))& vbCrLf
		end if
		mail_body= mail_body & "Refill: " &res1("refill")& vbCrLf
		mail_body= mail_body & "Notes: " &res1("notes")& vbCrLf & vbCrLf

  res1.MoveNext
  wend
end if

mail_body = mail_body &vbCrLf
if Request.QueryString("necessary")="true" then
	mail_body= mail_body & "Substitution permitted? =   Yes " & vbCrLf
else
	mail_body= mail_body & "Substitution permitted? =   No " & vbCrLf
end if

'Response.Write mail_body
Set CDOMail = CreateObject("CDO.Message")

  CDOMail.From = mail_from
  CDOMail.To = mail_to
  CDOMail.Subject = "Prescription"
  CDOMail.TextBody = mail_body

  CDOMail.Send

 set CDOMail=nothing


 Response.Write "<p align=center> Email has been sent."&"<br><br><br>"

 Response.Write "<input type=button name=b1 value=Close onclick='self.close()'>"

%>


