
<!--#include file="../../../library/connection.inc.asp"-->


<%

function makeResponseType(rType)
    theResponse = "" & rType
    
    for i = 1 to ubound(split(Request.form("patient_id"), ","))
        theResponse = theResponse & "," & "Denied"
    next
    theResponse = theResponse & "," 
    makeResponseType = theResponse
end function

function padMid(rType)
    theResponse = "" & rType
    
    for i = 1 to ubound(split(Request.form("patient_id"), ","))
        theResponse = theResponse & "," & rType
    next
    theResponse = theResponse & "," 
    padMid = theResponse
end function


function makeResponseNote(note)
    theResponse = "" & replace(note, ","," ")
    
    for i = 1 to ubound(split(Request.form("patient_id"), ","))
        theResponse = theResponse & "," & "Already Responded To"
    next
    theResponse = theResponse & "," 
    makeResponseNote = theResponse
end function

function makeResponseReasonCode()
    theResponse = "" & "AP"
    
    for i = 1 to ubound(split(Request.form("patient_id"), ","))
        theResponse = theResponse & ",AP"
    next
    theResponse = theResponse & "," 
    makeResponseReasonCode = theResponse
end function

function makeMessageType(rType)
    theResponse = "" & rType
    
    for i = 1 to ubound(split(Request.form("patient_id"), ","))
        theResponse = theResponse & "," & rType
    next
    theResponse = theResponse & "," 
    makeMessageType = theResponse
end function

function padRefills(rfills)
    theResponse = "" & rfills
    
    for i = 1 to ubound(split(Request.form("patient_id"), ","))
        theResponse = theResponse & ",0"
    next
    theResponse = theResponse & "," 
    padRefills = theResponse
end function

drug_id = split(trim(Request.form("selected_mid")), ",")(0)
visit_key = Request.form("visit_key")
patient_id  = Request.form("patient_id")
NCPDPID = Request.form("NCPDPID")
Physician_ID = Request.form("Physician_ID")
request_messageid = Request.form("request_messageid")
Physician_PON = Request.Form("Physician_PON")

is_sub = Request.form("is_sub")
ScheduleCode = Request.form("ScheduleCode")
epcs_twofactor_userid = Request.form("epcs_twofactor_userid")
epcs_twofactor_password = Request.form("epcs_twofactor_password")
epcs_twofactor_otpcode = Request.form("epcs_twofactor_otpcode")
epcsAuditAuthor = Request.Form("epcsAuditAuthor")
epcsAuditRole = Request.Form("epcsAuditRole")

DPSNumber = Request.Form("DPSNumber")
DEANumber = Request.Form("DEANumber")

if trim(Request.form("no_refill")) = "" then
	no_refill=0
else
	no_refill=cint(trim(Request.form("no_refill")))
end if

date_written= split(trim(Request.form("date_written")), ",")(0)


Response_Note=Request.form("Response_Note")
Response_Note=replace(Response_Note,"'","''")

set rs = server.CreateObject("ADODB.Recordset")
strSQL="update notes_medications set"&_
		" Notes= '" & Response_Note & "', refill_remaining = '" & (no_refill -1) & "'" & _
		",prescribed_date= TO_DATE('" & cdate(date_written) & "','MM/DD/YYYY')" &_
		" where drug_id='" & drug_id & "'"

'Response.Write strSQL
		
rs.Open strSQL, conn

%>



<form method="post" name="f1">

<input type="hidden" name="selected_mid" value="<%=padMid(drug_id)%>">
<input type="hidden" name="Visit_Key" value="<%=visit_key%>">
<input type="hidden" name="Patient_Id" value="<%=patient_id%>">
<input type="hidden" name="MsgType" value="<%=makeMessageType("RefillResponse") %>">
<input type="hidden" name="Response_Type" value="<%= makeResponseType("ApprovedWithChanges")%>">
<input type="hidden" name="NCPDPID" value="<%=NCPDPID%>">
<input type="hidden" name="Physician_ID" value="<%=Physician_ID%>">
<input type="hidden" name="Response_Reason_Code" value="<%=makeResponseReasonCode() %>">
<input type="hidden" name="request_messageid" value="<%=request_messageid%>">
<input type="hidden" name="Physician_PON" value="<%= Physician_PON %>" />
<input type="hidden" name="Response_Note" value="<%=makeResponseNote(Response_Note)%>"/>
<input type="hidden" name="Refills" value="<%=padRefills(no_refill)%>"/>

<!--EPCS-->
<input type="hidden" name="is_sub" value="<%=is_sub %>"/>
<input type="hidden" name="ScheduleCode" value="<%=ScheduleCode %>" />
<input type="hidden" name="epcs_twofactor_userid" value="<%=epcs_twofactor_userid %>"/>
<input type="hidden" name="epcs_twofactor_password" value="<%=epcs_twofactor_password %>"/>
<input type="hidden" name="epcs_twofactor_otpcode" value="<%=epcs_twofactor_otpcode %>"/>

<input type="hidden" name="DPSNumber" value="<%=DPSNumber %>" />
<input type="hidden" name="DEANumber" value="<%=DEANumber %>" />
<input type="hidden" name="epcsAuditAuthor" value="<%=epcsAuditAuthor %>" />
<input type="hidden" name="epcsAuditRole" value="<%=epcsAuditRole %>" />


		
</form>

<script>
	document.f1.action ="https://www.webedoctor.com/surescriptInterface/default_multiple.aspx"
	document.f1.submit();
</script>



			
