<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
All_med = Request("all")

%>

<html>

<head>
<title>Print Medication History...</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
</head>

<body onload="window.print();">
<%

set res1 = server.CreateObject("ADODB.Recordset")

res1.open "select * from PATIENTTABLE where USERID='"&PATIENT_ID&"'",CONN	
PAT_NAME	=	ReS1("FIRST_NAME")&" "&ReS1("LAST_NAME")
DOB			=	res1("BIRTH_DATE")
res1.close
%>
<div class="titleBox">Medications</div>
<p>

<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">
  <tr>
    <td colspan=4><b>Patient Name:</b> <%=PAT_NAME%> </td>    
  </tr>
	<tr>
    <td colspan=4><b>Date of Birth:</b><%=DOB%>  </td>    
  </tr>

  <tr bgcolor="#3366CC">
    <td><font color="white"><b>Date</td>
    <td><font color="white"><b>Prescription</td>
    <td><font color="white"><b>Notes</td>
    <td><font color="white"><b>Status</td>
  </tr>

<%
if All_med = "1" then
	sqlQuery = "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and patient_id='" & patient_id & "'"
else
	sqlQuery = "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and  status not in ( 'O Stopped', 'Stopped') and patient_id='" & patient_id & "'"
end if

res1.open sqlQuery,con
if not res1.eof then

	cnt = 1
	while not res1.eof		
		notes = ""		
		if cnt mod 2 = 0 then
			bgcolor = "#FFFFFF"
		else
			bgcolor = "#EFEFEF"
		end if
				
		if res1("notes") <> "" then
			notes = replace(res1("notes"),";","<br>")
		end if
			
%>
	<tr bgcolor="<%=bgcolor%>">
        <td valign="top"><b><%=res1("prescribed_date")%></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b> - 
          <%=res1("formulation")%><br>
	      <b>Dose:</b> <%=res1("dose")%><br>
	      <b>Route:</b> <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %><br>
	      <% if trim(res1("dispense").value) <> "" then %>
	      <b>Dispense:</b> <%=res1("dispense")%><br>
	      <% end if %>
	      <b>Refill:</b> <%=res1("refill")%>
	      <% if res1("frequency") <> "" then %>
	      <br><b>Frequency:</b> <%=res1("frequency")%>
	      <% end if %>
	    </td>
	    <td class="st" valign="top"><%=notes%></td>
	    <td valign="top"><b><%=res1("status")%></td>
	</tr>
	<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close 

else
%>
	<tr>
		<td colspan="8">No medications found on record.</td>
	</tr>
<%
end if
%>
</table>
</BODY>
</HTML>
