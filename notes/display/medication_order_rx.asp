<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%
'dim auditObj
'set auditObj = new clsAudit

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_id = Request("drug_id_hidden")

'call auditObj.LogActivity("Accessing Medications Order","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script src="../../../library/jquery-1.10.2.min.js"></script>
<style type="text/css">
	.unactive
	{
		z-index: -10 !important;
		display: none;
		}
</style>
<script type="text/javascript">
	function ShowEPCSSummaryNote() {
		$("#epcs_summary_note").removeClass("unactive");
	}
	function HideEPCSSummaryNote() {
		$("#epcs_summary_note").addClass("unactive");
    }
    function showIllegalChanges(changeDetail) {
        $("#epcs_illegalChanges_Detail").html(changeDetail);
        var medprintHeight = $(document).height();
        $("#epcs_illegalChanges").height(medprintHeight);
        $("#epcs_illegalChanges").removeClass("unactive");
    }
    function closeIllegalChanges() {
        $("#epcs_illegalChanges").addClass("unactive");
    }
</script>
<script language="javascript" type="text/javascript">

function setmedval(obj, x) {
    type = x.split(',')
    document.medprint.selected_mid.value=type[0];
    document.medprint.Physician_ID.value=type[1];
    document.medprint.is_sub.value = type[2];
    if (document.medprint.Followup_Flag.value.length == 0) {
        document.medprint.Followup_Flag.value = type[3];
    }
    document.medprint.notes_len.value = type[4];
	
	if (document.medprint.is_sub.value == 'Y') {
        var status = $(obj).parent().parent().find('.epcsStatus').html();

        if (status == "READY") {
            document.medprint.epcsStatus.value = "READY";
        }
        else if (status == "PRINTED") {
            document.medprint.epcsStatus.value = "PRINTED";
        }
        else {
            document.medprint.epcsStatus.value = "NO";
        }
    }
	
	if ($(obj).parent().find('.mBPayerId')[0].value) {
        document.medprint.B_PayerId.value = $(obj).parent().find('.mBPayerId')[0].value;
    }
	if ($(obj).parent().find('.mBPayerName')[0].value) {
        document.medprint.B_PayerName.value = $(obj).parent().find('.mBPayerName')[0].value;
    }
    if ($(obj).parent().find('.mBBINLocationNumber')[0].value) {
        document.medprint.B_BINLocationNumber.value = $(obj).parent().find('.mBBINLocationNumber')[0].value;
    }
	if ($(obj).parent().find('.mBProcessorControlNumber')[0].value) {
        document.medprint.B_ProcessorControlNumber.value = $(obj).parent().find('.mBProcessorControlNumber')[0].value;
    }
    if ($(obj).parent().find('.mBGroupId')[0].value) {
        document.medprint.B_GroupId.value = $(obj).parent().find('.mBGroupId')[0].value;
    }
    if ($(obj).parent().find('.mBGroupName')[0].value) {
        document.medprint.B_GroupName.value = $(obj).parent().find('.mBGroupName')[0].value;
    }
    if ($(obj).parent().find('.mBCardHolderId')[0].value) {
        document.medprint.B_CardHolderId.value = $(obj).parent().find('.mBCardHolderId')[0].value;
    }
	if ($(obj).parent().find('.mBCardHolderFirstName')[0].value) {
        document.medprint.B_CardHolderFirstName.value = $(obj).parent().find('.mBCardHolderFirstName')[0].value;
    }
	if ($(obj).parent().find('.mBCardHolderLastName')[0].value) {
        document.medprint.B_CardHolderLastName.value = $(obj).parent().find('.mBCardHolderLastName')[0].value;
    }
	if ($(obj).parent().find('.mBMutuallyDefined')[0].value) {
        document.medprint.B_MutuallyDefined.value = $(obj).parent().find('.mBMutuallyDefined')[0].value;
    }
}


function chk_pharmacy()
{
    //document.medprint.s1.disabled = true;

	if (document.medprint.selected_mid.value=='')
	{
	alert("Please select a Prescription to be sent.")
	return (false);
	}

	if (document.medprint.is_sub.value=='Y')
	{
		//alert("Medication is a controlled substance, it can not be sent electronically.")

		
		//EPCS: 1. Prescriber must have EPCS
		if (document.medprint.doctorHasEPCS.value != "Y") {
		    alert("Prescriber isn't set up for Electronic Prescriptions for Controlled Substances. You can only print this prescription.");
		    return false;
		}
		
		//EPCS: 1.2 Check medication's owner.
		var owner_physicianid = $("input[name='Physician_ID']").val(); //This is a physician who has an encounter with patient
		var current_physicianid = $("input[name='Current_Physician_ID']").val(); //This is a physician who is in this session.
		if (current_physicianid != owner_physicianid) {
		    alert("You can not order a controlled substance for a patient who does not have a visit with you electronically.");
		    return false;
		}

		//EPCS: 2. Pharmacy must have EPCS
		if (document.medprint.pharmacyHasEPCS.value != "Y") {
		    alert("Pharmacy isn't set up for Electronic Prescriptions for Controlled Substances.");
		    return false;
		}

		//EPCS: 3. Check Printing Status
		if (document.medprint.epcsStatus.value == "PRINTED") {
		    alert("This controlled substance was printed for dispense in this encounter. You can not order it electronically in the same encounter.");
		    return false;
		}
		else if (document.medprint.epcsStatus.value == "NO") {
		    alert("Can not order controlled substance which is added in newer encounter!");
		    return false;            
        }


		//EPCS: 4. Open Summary Note
		ph_id = document.medprint.NCPDPID.value;
		med_id = document.medprint.selected_mid.value;
		ndc_code = document.medprint.NCPDPID.value;
		popScrollWindow('../display/epcs_sign_two_factor.asp?' +
			'drug_id=' + med_id + 
			'&visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&pharmacy_id=' + ph_id + 
			'&NCPDPID=' + $("input[name='NCPDPID']").val() +
			'&MsgType=' + $("input[name='MsgType']").val() +
			'&RXReferenceNumber=' + $("input[name='RXReferenceNumber']").val() +
			'&Physician_ID=' + $("input[name='Physician_ID']").val() +
			'&selected_mid=' + $("input[name='selected_mid']").val() +
			'&is_sub=' + $("input[name='is_sub']").val() +
			'&Followup_Flag=' + $("input[name='Followup_Flag']").val() +
			'&ph_name=' + $("input[name='ph_name']").val() +
			'&medi_sel=' + $("input[name='medi_sel']").val() +
			'&refres_messageid=' + $("input[name='refres_messageid']").val() +
            '&B_PayerId=' + $("input[name='B_PayerId']").val() +
			'&B_PayerName=' + $("input[name='B_PayerName']").val() +
            '&B_BINLocationNumber=' + $("input[name='B_BINLocationNumber']").val() +
			'&B_ProcessorControlNumber=' + $("input[name='B_ProcessorControlNumber']").val() +
            '&B_GroupId=' + $("input[name='B_GroupId']").val() +
			'&B_GroupName=' + $("input[name='B_GroupName']").val() +
            '&B_CardHolderId=' + $("input[name='B_CardHolderId']").val() +
			'&B_CardHolderFirstName=' + $("input[name='B_CardHolderFirstName']").val() +
			'&B_CardHolderLastName=' + $("input[name='B_CardHolderLastName']").val() +
			'&B_MutuallyDefined=' + $("input[name='B_MutuallyDefined']").val(), '620', '600');
		//ShowEPCSSummaryNote();
		return false;
	}

	if(document.medprint.ph_name.value=='')
	{	alert('Please select the Pharmacy to send the Prescription Electronically');
		document.medprint.ph_name.select();
		return false;
    }

    if (!isNaN(document.medprint.notes_len.value) && document.medprint.notes_len.value > 210) {
        alert('Note to Pharmacist is longer than 210 characters, please shorten it before sending electronically.');
        return false;
    }

    //document.medprint.s1.disabled = true;
}

function print_med()
{
	med_id_print=document.medprint.selected_mid.value;
	ph_id=document.medprint.NCPDPID.value;

    var oldMessageId = '';



    if( document.medprint.Followup_Flag.value == 'Y' )
    {
	    oldMessageId = document.medprint.RXReferenceNumber.value;
    }




	if (med_id_print=='')
		{
		alert("Please select a Prescription to Print.")
		return (false);
		}
	else {
	    popScrollWindowMax('medication_printrx.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&pharmacy=' + ph_id + '&sig=true&necessary=false&list=' + med_id_print + '&oldMessageId=' + oldMessageId, '600', '400');
		}
}


</script>

</head>
<body>
<div class="titleBox">Medications &gt; Order Prescription Electronically</div>
<%
dim prescriberInfo
set drRS = server.CreateObject("ADODB.Recordset")
sqlQuery = "select * from doctortable where docuser='" & session("docid") & "'"
drRS.open sqlQuery,con

if not (drRS.EOF or drRS.BOF) then
    prescriberInfo = drRS("first_name") & " " & drRS("last_name") & ","
    prescriberInfo = prescriberInfo & drRS("street") & ", " & drRS("city") & ", " & drRS("state") & ", " & drRS("zip") & ", Phone: " & drRS("phone")
end if

drRS.close
%>
<p><b>Prescriber:</b> <%=prescriberInfo%></p>
<p>
Columns highlighted in <span style="background-color: #FFFFCC">yellow</span> indicate medication prescribed during this visit.
<p>

<form action="https://www.webedoctor.com/surescriptInterface/default.aspx" method="post" name="medprint" id="medprint" onsubmit="return chk_pharmacy()">

<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">

  <tr bgcolor="#3366CC">
    <td width="2%"></td>
    <td width="6%"><font color="white"><b>Start Date</td>
    <td width="6%"><font color="white"><b>Written Date</td>
    <td width="30%"><font color="white"><b>Prescription</td>
    <td width="24%"><font color="white"><b>Detail for EPCS</td>
    <td width="17%"><font color="white"><b>Note to Pharmacist</td>
    <td width="15%"><font color="white"><b>Current Order Status</td>

  </tr>


<%

set res1 = server.CreateObject("ADODB.Recordset")
set rs = server.CreateObject("ADODB.Recordset")

'Ignore wrong NDC medication
sqlQuery =	"select a.*, b.route_description,c.doctor_id as doctor_id, " &_
      "case when a.modified_date is not null then a.modified_date else a.prescribed_date end as last_action_date " &_
			"from notes_medications a, multum_route b, visit c, ndc_brand_name aa, multum_product_strength bb, ndc_main_multum_drug_code cc, ndc_core_description dd, multum_dose_form ee " &_
			"where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "' and a.medical_type = 'DRU' " &_
			"and a.ndc_code = dd.ndc_code " &_
			"and aa.brand_code = dd.brand_code " &_
			"and dd.main_multum_drug_code = cc.main_multum_drug_code " &_
			"and bb.product_strength_code = cc.product_strength_code " &_
			"and cc.dose_form_code = ee.dose_form_code " &_
			"and(dd.obselete_date is null or dd.obselete_date > sysdate) " &_
			"and regexp_replace(lower(trim(a.drug_name)), '[#]', '') = regexp_replace(lower(trim(aa.brand_description)), '[#]', '')" &_
			"and regexp_replace(lower(trim(a.formulation)), '[#]', '') = regexp_replace(lower(concat(concat(trim(bb.product_strength_description), ' '), trim(ee.dose_from_description))), '[#]', '') " &_
      "UNION " &_
      "select a.*, b.route_description,c.doctor_id as doctor_id, " &_
      "case when a.modified_date is not null then a.modified_date else a.prescribed_date end as last_action_date " &_
			"from notes_medications a, multum_route b, visit c " &_
			"where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "' and a.medical_type = 'SP' " &_
      "order by last_action_date desc"

'sqlQuery = "select a.*, b.route_description,c.doctor_id as doctor_id from notes_medications a, multum_route b, visit c where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.visit_key = c.visit_key and a.patient_id='" & patient_id & "'"
'sqlQuery = "select a.*, b.route_description,a.PRESCRIBED_BY as doctor_id from notes_medications a, multum_route b where a.route=b.route_abbr(+) and status='Current' and a.ss_flag <> 0 and a.patient_id='" & patient_id & "'"

'Response.Write sqlQuery

res1.open sqlQuery,con
if not res1.eof then
	cnt = 1
	while not res1.eof
		Physician_ID=res1("doctor_id")
		if res1("visit_key") = visit_key then
			bgcolor = "#FFFFCC"
		else
			if cnt mod 2 = 0 then
				bgcolor = "#FFFFFF"
			else
				bgcolor = "#EFEFEF"
			end if
		end if

		'check for the contolled substance drug.....
		'sqlQuery = "select * from SS_Substances where schedule='III' and (upper(drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%' or upper(SYNM) like '"& ucase(left(res1("drug_name"),7)) & "%' )"
		'sqlQuery = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and upper(drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%'"
		sqlQuery = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and upper(brand_name) = '" & ucase(res1("drug_name")) &"'"
		sqlQuery = "select a.drug_id, b.ndc_code  from notes_medications a,ndc_drugs b " &_
					" where a.ndc_code = b.ndc_code " &_
					" and a.ndc_code = '" & res1("ndc_code") & "'" &_
					" and csa_schedule in (2,3,4,5)" &_
					" and upper(a.drug_name) like '" & ucase(left(res1("drug_name"),7)) & "%'"

		rs.open sqlQuery,con
		if not (rs.eof or rs.bof) then
			Is_CS="Y"
		else
			Is_CS="N"
		end if
		rs.close

		'get surescript status of the medication -start
		strSQL=" select MessageType,STATUS, STATUSDESC, REFRESPONSECODE, CancelCode, CancelDesc from SS_MESSAGE where DrugID='" & res1("drug_id") & "' order by timesent desc"
		rs.open strSQL,con

		If not (rs.EOF or rs.BOF) then
			ss_status=rs("STATUS")
			ss_status_desc=	rs("STATUSDESC")
			ss_MsgType= rs("MessageType")
			ss_refrep= trim(rs("REFRESPONSECODE"))
			If ss_refrep="DeniedNewPrescriptionToFollow" then
				ss_followup="Y"
			else
				ss_followup="N"
			end if

			if ss_MsgType = "CancelRx" or ss_MsgType = "CancelRxResponse" then
				ss_status = rs("CancelCode")
				ss_status_desc = rs("CancelDesc")
			end if
		else
			ss_status=""
			ss_status_desc=""
			ss_MsgType=""
			ss_refrep=""
			ss_followup="N"
		End if
		rs.close
		'--------end

        substitution = ""
        if not isnull(res1("substitution")) then
            if res1("substitution") = "0" then
                substitution = "Allowed"
            elseif res1("substitution") = "1" then
                substitution = "Not allowed"
            else
                substitution = res1("substitution")
            end if
        end if

%>
	<tr bgcolor="<%=bgcolor%>">
		<td valign="top">
			<input type="radio" name="medi_sel" value="<%=res1("drug_id")%>,<%=Physician_ID%>,<%=Is_CS%>,<%=ss_followup%>,<%=len(res1("notes")) %>" onclick="setmedval(this, this.value)" />
			<input type="hidden" name="mBPayerId" class="mBPayerId" value="<%=res1("B_PayerId") %>" />
			<input type="hidden" name="mBPayerName" class="mBPayerName" value="<%=res1("B_PayerName") %>" />
            <input type="hidden" name="mBBINLocationNumber" class="mBBINLocationNumber" value="<%=res1("B_BINLocationNumber") %>" />
			<input type="hidden" name="mBProcessorControlNumber" class="mBProcessorControlNumber" value="<%=res1("B_ProcessorControlNumber") %>" />
            <input type="hidden" name="mBGroupId" class="mBGroupId" value="<%=res1("B_GroupId") %>" />
			<input type="hidden" name="mBGroupName" class="mBGroupName" value="<%=res1("B_GroupName") %>" />
            <input type="hidden" name="mBCardHolderId" class="mBCardHolderId" value="<%=res1("B_CardHolderId") %>" />
			<input type="hidden" name="mBCardHolderFirstName" class="mBCardHolderFirstName" value="<%=res1("B_CardHolderFirstName") %>" />
			<input type="hidden" name="mBCardHolderLastName" class="mBCardHolderLastName" value="<%=res1("B_CardHolderLastName") %>" />
			<input type="hidden" name="mBMutuallyDefined" class="mBMutuallyDefined" value="<%=res1("B_MutuallyDefined") %>" />
        </td>
        <td valign="top"><b><%=res1("prescribed_date")%></td>
        <td valign="top"><b><%=date()%></b></td>
        <td valign="top">
          <b><%=res1("drug_name")%></b>
          <%=res1("formulation")%><%if Is_CS="Y" then%><img alt="controlled substance" src="/images/practice/red_star.gif" border="0" /><%end if%> <br>

	      <b>Dispense:</b> <%=res1("dispense")%><br>

	      <b>Refill:</b> <%If cint(res1("refill"))=999 then Response.Write "PRN" else Response.Write res1("refill") end if%>

	      <br><b>Directions:</b> <%=res1("dose") & " " & res1("frequency")%><% if res1("route") <> "" then Response.write("-" & PCase(res1("route")))%>
	      <br /><b>Substitution:</b> <%=substitution %>
	    </td>
		<td>
            <%
                sqlEPCS = "SELECT v.VISIT_KEY, VISIT_DATE, VISIT_TIME " &_
                         "FROM VISIT v " &_
                         "INNER JOIN (SELECT VISIT_KEY FROM (SELECT * FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC) WHERE rownum = 1) p " &_
                         "ON v.VISIT_KEY = p.VISIT_KEY "

                set rsEPCS = server.CreateObject("ADODB.Recordset")
                rsEPCS.open sqlEPCS,con

                if not (rsEPCS.EOF or rsEPCS.BOF) then

                    set rsEPCS2 = server.CreateObject("ADODB.Recordset")

                    sqlEPCS2 = "SELECT Status, Created_Date, ss_message_id FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC"

                    rsEPCS2.open sqlEPCS2,con
                    while not rsEPCS2.EOF
                        if UCase(rsEPCS2("Status")) <> "NEW" then
                            response.Write("- <b>" & rsEPCS2("Status") & "</b> ")
                            if UCase(rsEPCS2("Status")) <> "PRINTED" then
                                response.Write("signed on " & rsEPCS2("Created_Date"))
                                checkChangeResult = FUNC_CHECKCHANGES(rsEPCS2("ss_message_id"))
                                if checkChangeResult <> "VALID" then
                                    response.Write(" <span style=""color: red; text-decoration: underline; cursor: pointer;"" onclick=""showIllegalChanges('" & checkChangeResult & "');"">Changed!</span>")
                                end if
                                response.Write("<br/>")
                            else
                                response.Write("on " & rsEPCS2("Created_Date") & "<br/>")
                            end if                            
                        end if
                        rsEPCS2.movenext
                    wend
                    rsEPCS2.close

                    response.Write("- <b>Encounter:</b> " & rsEPCS("VISIT_DATE") & " " & rsEPCS("VISIT_TIME"))
                    if rsEPCS("VISIT_KEY") = visit_key then
                        response.Write(" (Current)")
                    end if

                    sqlEPCS2 = "SELECT VISIT_ID FROM VISIT WHERE VISIT_KEY = '" & visit_key & "'"

                    rsEPCS2.open sqlEPCS2,con
                    if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                        visit_id_current = CLng(rsEPCS2("VISIT_ID"))
                    end if
                    rsEPCS2.close

                    sqlEPCS2 = "SELECT VISIT_ID FROM VISIT WHERE VISIT_KEY = '" & rsEPCS("VISIT_KEY") & "'"

                    rsEPCS2.open sqlEPCS2,con
                    if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                        visit_id_newest = CLng(rsEPCS2("VISIT_ID"))
                    end if
                    rsEPCS2.close

                    if visit_id_current < visit_id_newest then
                        %>
                        <span class="epcsStatus" style="display: none;">NO</span>
                        <%
                    elseif visit_id_current > visit_id_newest then
                        %>
                        <span class="epcsStatus" style="display: none;">READY</span>
                        <%
                    else
                        sqlEPCS2 = "SELECT STATUS FROM (SELECT * FROM EPCS_PRESCRIPTIONS WHERE DRUG_ID = '" & res1("drug_id") & "' ORDER BY CREATED_DATE DESC) WHERE rownum = 1"

                        rsEPCS2.open sqlEPCS2,con
                        if not (rsEPCS2.EOF or rsEPCS2.BOF) then
                            if UCase(rsEPCS2("STATUS")) = "PRINTED" then
                                %>
                                <span class="epcsStatus" style="display: none;">PRINTED</span>
                                <% 
                            else
                                %>
                                <span class="epcsStatus" style="display: none;">READY</span>
                                <% 
                            end if
                        end if
                        rsEPCS2.close
                    end if

                end if
                rsEPCS.close
            %> 
        </td>
	    <td class="st" valign="top"><%=res1("notes")%></td>
	    <td valign="top">
	    <%If ss_MsgType<>"" then%>
			<b><a onMouseOver="return overlib('<%=ss_status_desc%>',LEFT,STICKY,CAPTION,'Electronic Submission Response');" onmouseout="return nd();"><%=ss_MsgType%> - <%=ss_refrep%> - <%=ss_status%></a><br>
			<%end if%>
	    </td>
	</tr>
<%
		cnt = cnt + 1
		res1.movenext
	wend
	res1.Close
%>


<%
strSQL = "select * from patienttable where userID  ='" & patient_id & "'"
rs.open strSQL,con

if not (rs.eof or rs.BOF) then
	LAST_NAME = ucase(rs("LAST_NAME"))
	FIRST_NAME = ucase(rs("FIRST_NAME"))
	BIRTH_DATE = rs("BIRTH_DATE")
	GENDER  =  ucase(rs("GENDER"))
	Ph_name = rs("PREFERRED_PHARMACY")
	NCPDPID =  rs("NCPDPID")
	NCPDPID2 =  rs("NCPDPID2")
End if
rs.close

If NCPDPID<>"" then
	strSQL = "select storename, addressline1, city, state, zip, phoneprimary from ss_pharmacy where NCPDPID ='" & NCPDPID & "'"
	rs.open strSQL,con
	Ph_fullname= rs("storename")&" "&rs("addressline1")&" "&rs("city")&" "&rs("state")&" "&rs("zip")&" PH: "&rs("phoneprimary")
	rs.Close
end if
If NCPDPID2<>"" then
	strSQL = "select storename, addressline1, city, state, zip, phoneprimary from ss_pharmacy where NCPDPID ='" & NCPDPID2 & "'"
	rs.open strSQL,con
	Ph_fullname2= rs("storename")&" "&rs("addressline1")&" "&rs("city")&" "&rs("state")&" "&rs("zip")&" PH: "&rs("phoneprimary")
	rs.Close
end if

strSQL="select a.MessageID,a.PATIENTLASTNAME, a.PATIENTFIRSTNAME, a.PATIENTGENDER, a.PATIENTDOB, a.MEDICATIONDESCRIPTION, a.MEDICATIONnote, c.ncpdpid, replace(c.storename, '''', '') storename, c.addressline1, c.city, c.state, c.zip, c.phoneprimary, d.rxreferencenumber "&_
	   "	From SS_MESSAGE_CONTENT a " &_
	   "   join ss_message d on d.messageid = a.messageid "&_
	   "   left outer join ss_pharmacy c on c.ncpdpid = d.pharmacyid where a.MessageID in ( "&_
	   "	select  b.MessageID  from ss_message b where b.NEWRXNEEDED='YES') "&_
	   "   and upper(a.PATIENTLASTNAME) ='" & LAST_NAME & "' and  upper(a.PATIENTFIRSTNAME) ='" & FIRST_NAME & "'"&_
	   "  order by a.PATIENTLASTNAME, a.PATIENTFIRSTNAME "
'response.write strSQL
rs.open strSQL,con
if not (rs.eof or rs.BOF) then
%>
<tr bgcolor="#3366CC">
    <td colspan="7"><font color="white">The following Refills have been Denied with NewRXToFollow, please select the appropriate medication below if you are sending a NewRX for one of the DinedwithNewRXtoFollow Refills. </td>
</tr>

<%
 while not rs.eof
   If ucase(rs("PATIENTLASTNAME")) = LAST_NAME and ucase(rs("PATIENTFIRSTNAME"))=FIRST_NAME and ucase(rs("PATIENTGENDER"))= left(GENDER,1) then

%>
	<tr bgcolor="<%=bgcolor%>">
		 <td valign="top"><input type="radio" name="refres_messageid" id="refres_messageid" value="<%=rs("MessageID")%>" onclick="document.medprint.Followup_Flag.value='Y';document.medprint.ph_name.value='<%=rs("storename")&" "&rs("addressline1")&" "&rs("city")&" "&rs("state")&" "&rs("zip")&" PH: "&rs("phoneprimary") %>';document.medprint.NCPDPID.value='<%=rs("ncpdpid") %>'; document.getElementById('searcher').style.display='none'; document.medprint.RXReferenceNumber.value='<%=rs("RXReferencenumber") %>'; return true;">
         <td valign="top" colspan="3">  <b><%=rs("MEDICATIONDESCRIPTION")%></b>
	     <td class="st" valign="top" colspan="3"><%=rs("MEDICATIONnote")%></td>
	</tr>
<%
	end if
 rs.movenext
 wend
	rs.Close

end if
rs.Close
%>


<%
else
%>
	<tr>
		<td colspan="8">No medications found on record.</td>
	</tr>
<%
end if
%>
</table>


<p></p>
<div class="titleBox_HL2">
<input type="hidden" name="NCPDPID" id="NCPDPID" value="<%=NCPDPID%>" />
<input type="hidden" name="visit_key" value="<%=visit_key%>" />
<input type="hidden" name="patient_id" value="<%=patient_id%>" />
<input type="hidden" name="MsgType" value="NewRX" />
<input type="hidden" name="RXReferenceNumber" />
<input type="hidden" name="Physician_ID" />
<input type="hidden" name="Current_Physician_ID" value="<%=session("user") %>" />
<input type="hidden" name="selected_mid" />
<input type="hidden" name="is_sub" />
<input type="hidden" name="epcsStatus" />
<input type="hidden" name="Followup_Flag" />
<input type="hidden" name="notes_len" />

<input type="hidden" name="B_PayerId" value="" />
<input type="hidden" name="B_PayerName" value="" />
<input type="hidden" name="B_BINLocationNumber" value="" />
<input type="hidden" name="B_ProcessorControlNumber" value="" />
<input type="hidden" name="B_GroupId" value="" />
<input type="hidden" name="B_GroupName" value="" />
<input type="hidden" name="B_CardHolderId" value="" />
<input type="hidden" name="B_CardHolderFirstName" value="" />
<input type="hidden" name="B_CardHolderLastName" value="" />
<input type="hidden" name="B_MutuallyDefined" value="" />

<%
strSQL = "select * from doctortable where docuser='" & session("docid") & "' and EPCS = 'Y' and bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  2048) = 2048"
rs.Open strSQL, con
%>
<input type="hidden" name="doctorHasEPCS" value="<%if not rs.EOF then %>Y<% else %>N<%end if %>" />       
<%
rs.Close
%>

<%
strSQL = "SELECT * FROM SS_PHARMACY WHERE NCPDPID = '" & NCPDPID & "' and bitand(cast(nvl(servicelevel,'0') as varchar2(20)) ,  2048) = 2048"
rs.Open strSQL, con
%>
<input type="hidden" name="pharmacyHasEPCS" value="<%if not rs.EOF then %>Y<% else %>N<%end if %>" />
<%
rs.Close
%>

Pharmacy (Primary):
<input maxlength="100" name="ph_name" size="95" value="<%=ph_fullname%>" readonly="readonly" />&nbsp;&nbsp;
<a href="#" id="searcher" onclick="popScrollWindowMax('pharmacy_search.asp?patient_id=<%=patient_id%>','920','500');"><img border="0" height="18" src="/images/global/icon_search.gif"></a>
<%
if ph_fullname2 <>"" then
	Response.Write"<br><br> Pharmacy (Secondary):"& ph_fullname2
end if
%>
</div>

<!--<font color="red" size="2"><b>*</b></font><font size="1"> Controlled substance,can not be sent electronacally</font>-->
<img alt="Controlled substance" src="/images/practice/red_star.gif" border="0" /> Controlled substance
<br/>
<input type="submit" value="Order Prescription" id="s1" name="s2"/>
<input type="button" value="Print Rx" name="b3" onclick="print_med();"/>&nbsp;
<input type="button" onclick="document.location='/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>';" value="Cancel" id="button4" name="button4" />
</form>

<!--EPCS: Summary Note-->
<div id="epcs_summary_note" class="unactive" style="height: 100%; width: 100%; z-index: 100; position: absolute; top: 0;">
	<div style="height: 100%; width: 100%; background-color: Black; opacity: 0.4; position: absolute;"></div>
	<div style="width: 600px; height: 620px; background-color: White; margin: auto; border: 1px solid #ddd; position: relative; top: 70;">
	</div>    
</div>

<div id="epcs_illegalChanges" class="unactive" style="height: 100%; width: calc(100% - 15px); z-index: 100; position: absolute; top: 0;">
    <div style="width: 450px; height: 500px; background-color: White; margin: auto; border: 3px solid #ddd; position: relative; top: 70px;">
        <div style="width: 435px; height: 30px; position: absolute; top: 0; background-color: #3366CC; padding-top: 15px; padding-left: 15px;">
            <span style="font-weight: bold; color: #FFF;">ILLEGALLY CHANGES</span>
        </div>
        <div id="epcs_illegalChanges_Detail" style="width: 420px; height: 395px; position: absolute; top: 50px; padding-left: 15px; padding-right: 15px; overflow-y: auto; line-height: 2; font-size: 14px;">
        </div>
        <div style="width: 100%; height: 50px; position: absolute; bottom: 0;">
            <input type="button" value="Close" style="font-size: 15px; margin-left: 198px; margin-top: 10px" onclick="closeIllegalChanges();" />
        </div>
    </div>
</div>

<br />
<p align="center">
<img src="/images/global/stop.gif" border="0">
<font size="3" color="red"><b>Please do NOT order hand written or free text Medications written prior to 2015 electronically.</b></font>
</p>

</body>
</html>

<%
FUNCTION FUNC_CHECKCHANGES(ssMessageId)
    Set oXMLHTTP = CreateObject("MSXML2.XMLHTTP")
    Set oXMLDoc = CreateObject("MSXML2.DOMDocument")
    Server.ScriptTimeout=10000
    dim url: url = "http://192.168.72.8/surescriptInterface/EPCS_Signature_Page.aspx?runType=verify&ssMessageId="&ssMessageId
    call oXMLHTTP.open("GET",url,false)
    call oXMLHTTP.setRequestHeader("Content-Type","application/x-www-form-urlencoded")

    call oXMLHTTP.send()

    FUNC_CHECKCHANGES = oXMLHTTP.responseText
END FUNCTION

%>
