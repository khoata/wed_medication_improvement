<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/clsAudit.asp"-->
<%
dim auditObj
set auditObj = new clsAudit

visit_key = Request("visit_key")
patient_id = Request("patient_id")
PhysicianId = Session("user")

call auditObj.LogActivity("Accessing Medications Search page","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)

dim res
dim res1



set res = server.CreateObject("adodb.recordset")
set res1 = server.CreateObject("adodb.recordset")

if Request("drug_name") <>"" then
	dname=request("drug_name")
	strength=request("strength")
else
	dname=""
	strength=""
end if

%>

<%
    'get drug interaction severity info
 set RS1 = Server.CreateObject("ADODB.Recordset")
rs1.Open "select minor, moderate, major,critical,noncritical from drug_interactions_severity where physician_id = '"& session("user") &"'",CONN
if not rs1.EOF then
	if rs1("minor") <> "" then
		severity = severity & "1,"
	end if
	if rs1("moderate") <> "" then
		severity = severity & "2,"
	end if
	if rs1("major") <> "" then
		severity = severity & "3,"
	end if
	if rs1("Critical") <> "" then
		A_Sev = A_Sev & "'Critical',"
	end if
	if rs1("Noncritical") <> "" then
		A_Sev = A_Sev & "'Non-Critical',"
	end if
end if
rs1.Close
if severity = "" then
	severity = "1,2,3"
end if

if A_Sev <> "" then
	Sev_Condition = " AND Severity in (" & left(A_Sev, len(A_Sev)-1) & ")"
end if
strQuery = "select * from notes_allergy where patient_id = '"& patient_id &"' and date_resolved is null and Allergy_Type='class'" & Sev_Condition
rs1.Open strQuery,con
if not (rs1.EOF or rs1.BOF) then
	while not rs1.EOF
		Allergies = Allergies & rs1("allergy_text") & ","
		Allergy_Sev = Allergy_Sev & rs1("severity") & "|"
		Allergy_Reac = Allergy_Reac & rs1("Notes") & "|"
		rs1.MoveNext
	wend
end if
rs1.Close

if session("show") = "new" then
  showval = 0
else
  showval = 1
end if

if session("show")="All" then session("show")="current"

 if show = "new" or session("show") = "new" then
	sqlQuery = "Select a.multum_drug_id,a.status,a.drug_name,a.multum_brand_code,a.notes,a.ndc_code,a.drug_id,a.prescribed_date,a.stop_date,a.formulation,a.dose,a.route,a.dispense,a.refill,a.frequency, b.route_description " &_
				" from notes_medications a, multum_route b " &_
				" where a.route=b.route_abbr(+) " &_
				" and patient_id='" & patient_id & "' " &_
				" and iscurrent = 0 and visit_key='" & visit_key & "' " &_
				" order by status,drug_name"
	session("show") = "new"
end if
if show = "current" or session("show") = "current" then
	sqlQuery = "Select a.multum_drug_id,a.status,a.drug_name,a.multum_brand_code,a.notes,a.ndc_code,a.drug_id,a.prescribed_date,a.stop_date,a.formulation,a.dose,a.route,a.dispense,a.refill,a.frequency, b.route_description " &_
				" from notes_medications a, multum_route b " &_
				" where a.route=b.route_abbr(+) " &_
				" and patient_id='" & patient_id & "' " &_				
				" order by status,drug_name"
	session("show") = "current"
end if

'Response.Write sqlQuery
dim res3
    set res3 = server.CreateObject("adodb.recordset")
    res3.open sqlQuery,con
    if not res3.eof then
    	'cnt = 1
        while not res3.EOF
		if not isnull(res3("multum_drug_id")) and res3("status") <> "Stopped" then
			multum_drug_id_list =  res3("multum_drug_id")  & "|" & multum_drug_id_list			
		end if
        res3.MoveNext
        wend
    end if 
    res3.Close
    

'Response.Write multum_drug_id_list

     %>

<html>
<head>
<title>[W] - Medication Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
</head>
<body onLoad="document.frm.drug_name.focus();">
<form name=frm action="medication_search_bh.asp" method=get>
<div class="titleBox">
  Quick Drug Search > <span class="st">Drug Name</span> <input type="text" name="drug_name" size="20" value="<%=Request("drug_name")%>"> <input type=submit name="submit" value="Search">
  <div style="color: red; font-size: 1em; font-weight: normal; margin-top: 5px">
    Medical supply can NOT be searched when Prescription Benefits and History Services is turned ON. Please contact Administrator for more information
  </div>
</div>
<p>
<input type=hidden name=visit_key value="<%=visit_key%>">
<input type=hidden name=patient_id value="<%=patient_id%>">
<input type=hidden name=type value=1>
<input type=hidden name=operation value=<%=Request("operation")%>>
<input type=hidden name=by value=<%=session("by")%>>
<input type=hidden name=type value="yes">
</form>

<%


name = trim(lcase(Request("drug_name")))
strength = Request("strength")

if name <> "" then
%>

<!--<div style="display: block; position; normal; height: 100%; width: 100%; overflow: scroll;">-->
<table width="100%" cellspacing="1" cellpadding="3" border="0" bgcolor="#CCCCCC">
<%
     
	 
              dim status6 
                    status6 = "" %>
             <% set RS20 = Server.CreateObject("ADODB.Recordset")
               RS20.Open " SELECT SURESCRIPTS_ACCESS,docuser  from  doctortable where SURESCRIPTS_ACCESS = 1 and docuser = '" & SQLFixUp(session("user")) & "' ", CONN %>
                     <%while not RS20.EOF%>
             <%if RS20("docuser") = session("user") then
                status6="1"
                else
                status6=""
                end if                  
                                                 
             %>                                                
             <%  RS20.MoveNext
                 wend
                 set RS20=nothing                        
             %> 

      <%if status6= "" then
                status6="0"                
                end if                           
             %> 
       

<%
   ' Response.Write status6 

	iPageSize = 25 ' You could easily allow users to change this

	If Request.QueryString("page") = "" Then
		iPageCurrent = 1
	Else
		iPageCurrent = CInt(Request.QueryString("page"))
	End If

	res.CursorLocation = 3
	res.PageSize = iPageSize
	res.CacheSize = iPageSize

	strSQL = "select distinct " &_
					"a.brand_description as brand_name," &_
					"g.route_abbr," &_
					"g.route_description," &_
					"b.product_strength_description as strength," &_
					"c.drug_id as multum_drug_id," &_
					"a.brand_code as brand_code, d.main_multum_drug_code," &_
                    "d.gbo as brand_generic_code," &_
					"e.dose_from_description as dose_form," &_
					"f.drug_name, " &_
          "'DRU' as medical_type, " &_
          "'' as supply_ndc_code, " &_
          "'' as supply_dose_form, " &_
          "'' as supply_support_information " &_
				"from ndc_brand_name a, multum_product_strength b, ndc_main_multum_drug_code c, ndc_core_description d, multum_dose_form e, multum_drug_id f, multum_route g " &_
				"where " &_
					"a.brand_code = d.brand_code " &_
					"and d.main_multum_drug_code = c.main_multum_drug_code " &_
					"and b.product_strength_code = c.product_strength_code " &_
					"and c.principal_route_code = g.route_code " &_
					"and c.drug_id = f.drug_id " &_
					"and c.dose_form_code = e.dose_form_code " &_
					"and(D.OBSELETE_DATE is null or D.OBSELETE_DATE > SYSDATE)" &_
					"and lower(a.brand_description) like lower('"&name&"%') " &_
					"and b.product_strength_description like '"&strength&"%' " &_
        "UNION " &_
        "select distinct " &_
          "ms.ms_brand_description, " &_
          "'', '', '', " &_
          "'' as multum_drug_id, " &_
          "NULL, NULL as main_multum_drug_code, " &_
          "'N' as brand_generic_code, " &_
          "'', '', " &_
          "'SP' as medical_type, " &_
          "ms.ms_pkg_product_id as supply_ndc_code, " &_
          "'supply_' || ms.ms_inner_package_description as supply_dose_form, " &_
          "ms.ms_inner_package_size || ' ' || ms.ms_inner_package_description || ' x ' || ms.ms_outer_package_size as supply_support_information " &_
        "from med_sup_ndc_pkg_product ms " &_
        "where 1=0 and " &_
          "lower(ms.ms_brand_description) like lower('"&name&"%') " &_
        "order by brand_name asc"
'Response.Write strSQL
	res.Open strSQL, CONN

	iPageCount = res.PageCount

'Hari Krishnan
' create string

 valueSearched = " Medication Name: " & name   
  
set cmd = server.CreateObject("ADODB.command")
cmd.ActiveConnection = CONN
 	strSQL = "INSERT INTO audit_log(USERNAME,  SECTION_NAME, SUB_SECTION, VISIT_KEY, ACTION, FIELD,  OLD_VALUE, NEW_VALUE , TIME_STAMP , IP_ADDR, USER_ROLE, PATIENT_ID, PATIENT_LAST_NAME, PATIENT_FIRST_NAME, VISIT_DATE, VISIT_TIME) " &_
	" SELECT B.DOCTOR_ID,'','',B.visit_key, 'Physician Searched for medication', '','','" & valueSearched & "', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'),'','Physician', B.PATIENT_ID, A.last_name, A.first_name,  VISIT_DATE,VISIT_TIME  FROM PATIENTTABLE A, VISIT B WHERE A.USERID = B.patient_id AND B.visit_key = '" &  visit_key & "'"
 
		cmd.CommandText = strSQL
		cmd.Execute  


	'Response.Write strSQL

	If iPageCurrent > iPageCount Then iPageCurrent = iPageCount
	If iPageCurrent < 1 Then iPageCurrent = 1

	If iPageCount = 0 Then
		response.Write "<tr bgcolor=#FFFFFF><td colspan=10><b>No medications found with search criteria.</td></tr>"
	Else
		res.AbsolutePage = iPageCurrent
%>
	<tr bgcolor="#DEE8FD">
		<td><b>Drug Name</b></td>
		<td><b>Formulation</b></td>
		<td><b>Route</b></td>
	</tr>
<%
	iRecordsShown = 0

	do while iRecordsShown < iPageSize And not res.eof

		dose_form = res("dose_form")

		if iRecordsShown mod 2 = 0 then
			bgcolor = "#FFFFFF"
		else
			bgcolor = "#EFEFEF"
		end if

    if res("medical_type") = "DRU" then
        'get the NDC code for the drug selected
        strSQL="select ndc_code from ndc_core_description where (OBSELETE_DATE is null or OBSELETE_DATE > SYSDATE) and brand_code="& res("brand_code") &" and main_multum_drug_code="& res("main_multum_drug_code")

        res1.open  strSQL,conn
        If not (res1.eof or res1.bof) then
          ndc_code=trim(res1("NDC_Code"))
        else
          ndc_code=""
        end if
        res1.close

        'check for the contolled substance drug...
        strSQL = "select ndc_code, csa_schedule from ndc_drugs " &_
              " where ndc_code = '" & ndc_code & "'" &_
              " and csa_schedule in (2,3,4,5)" &_
              " and upper(drug_name) like '" & ucase(left(res("drug_name"),7)) & "%'" 
        res1.open strSQL,con
        if not (res1.eof or res1.bof) then
          Is_CS="Y"
                if res1("csa_schedule") = 2 then
                    CS2="Y"
                else
                    CS2="N"
                end if
          
          if res1("csa_schedule") = 5 then
                    CS5="Y"
                else
                    CS5="N"
                end if
        else
          Is_CS="N"
        end if
        res1.close
    else
        Is_CS = "N"
        CS2 = "N"
    end if

    found_dose_unit = ""
    found_dispense_unit = ""
    if res("medical_type") = "DRU" then
        Call FindOtherUnitsByFormuationUnit(res("dose_form"), found_dose_unit, found_dispense_unit)
    elseif res("medical_type") = "SP" then
        Call FindOtherUnitsByFormuationUnit(res("supply_dose_form"), found_dose_unit, found_dispense_unit)
    end if
    
    product_description = ""
    if res("medical_type") = "DRU" then
        product_description = res("drug_name")
    else
        product_description = res("supply_support_information")
    end if
  %>
	<tr bgcolor="<%=bgcolor%>">
		<td><b><a href="#" onclick="validateDrug('<%=HTMLEncode(res("brand_name"))%>','<%=HTMLEncode(res("strength")&" "&res("dose_form"))%>');
									setValue('<%=res("brand_code")%>','<%=res("brand_generic_code")%>','<%=res("main_multum_drug_code")%>','<%=HTMLEncode(res("brand_name"))%>','<%=HTMLEncode(res("strength")&" "&res("dose_form"))%>','<%=res("route_abbr")%>','<%=res("dose_form")%>','<%=res("multum_drug_id")%>','<%=res("brand_code")%>', '<%=Is_CS%>', '<%=CS2%>', '<%=CS5%>',<%=status6%>, '<%=found_dose_unit%>', '<%=found_dispense_unit%>', '<%=res("medical_type")%>', '<%=res("supply_ndc_code")%>')">
      <%=res("brand_name")%></a></b> <span class="st"><i>(<%=product_description%>)</i></span>
			<%if Is_CS="Y" then%>
				<img alt="Controlled Substance" src="/images/practice/red_star.gif" border="0">
			<%end if%>
		</td>
		<td class="st"><%=res("strength")&" "&res("dose_form")%></td>
		<td class="st"><%=res("route_description")%></td>
         <input type="hidden" name="Drug" value="<%=res("brand_name")%>">
	</tr>
<%
	iRecordsShown = iRecordsShown + 1
	res.movenext
loop

%>
</table>
<!--</div>-->

<table width="100%" cellspacing="1" cellpadding="3" border="0" bgcolor="#CCCCCC">
	<tr>
		<td bgcolor="#FFFFFF">

<%
' Close DB objects and free variables
res.Close
Set res = Nothing
'con.Close
'Set con = Nothing
%>
	Page <B><%= iPageCurrent %></B> of <B><%= iPageCount %>
	<%
	' Spacing
	response.Write " - " & vbCrLf

' Show "previous" and "next" page links which pass the page to view
' and any parameters needed to rebuild the query.  You could just as
' easily use a form but you'll need to change the lines that read
' the info back in at the top of the script.
If iPageCurrent > 1 Then
	%>
	<a href="medication_search_bh.asp?drug_name=<%=name%>&strength=<%=strength%>&page=<%= iPageCurrent - 1 %>&order=<%= Server.URLEncode(strOrderBy) %>">&lt;&lt;</a>&nbsp;
	<%
End If

' You can also show page numbers:
if iPageCount-iPageCurrent < 10  then
	For I = 1 to iPageCount
		If I = iPageCurrent Then
		%>
		<%= I %>
		<%
		Else
		%>
		<a href="medication_search_bh.asp?drug_name=<%=name%>&strength=<%=strength%>&page=<%= I %>&order=<%= Server.URLEncode(strOrderBy) %>"><%= I %></a>
		<%
		End If
	Next 'I
else
	For I = iPageCurrent To iPageCurrent+10
		If I = iPageCurrent Then
		%>
		<%= I %>
		<%
		Else
		%>
		<a href="medication_search_bh.asp?drug_name=<%=name%>&strength=<%=strength%>&page=<%= I %>&order=<%= Server.URLEncode(strOrderBy) %>"><%= I %></a>
		<%
		End If
	Next 'I
end if


If iPageCurrent < iPageCount Then
	%>
	&nbsp;<a href="medication_search_bh.asp?drug_name=<%=name%>&strength=<%=strength%>&page=<%= iPageCurrent + 1 %>&order=<%= Server.URLEncode(strOrderBy) %>">&gt;&gt;</a>
	<%
End If

' END RUNTIME CODE
end if
%>


		</td>
	</tr>

       <input type="hidden"  name="multum_drug_id_list" id="multum_drug_id_list" value="<%=multum_drug_id_list%>">
    <input type="hidden" name="Allergies" id="Allergies" value="<%=allergies%>">
</table>
<img alt="Controlled Substance" src="/images/practice/red_star.gif" border="0" /> Controlled Substance.
<%
end if
%>


<script language="javascript">
    function validateDrug(Drug_Name, Formulation) {
        var full_name = Drug_Name + ' ' + Formulation;
        if (full_name.length > 105) {
            alert('Drug name and formulation is longer than 105 characters, could not be prescribed electronically');
            getFrameByName(top, "leftNoteFrameBottom").document.frm2.ss_flag.value = 0;
        }
        else {
            getFrameByName(top, "leftNoteFrameBottom").document.frm2.ss_flag.value = 1;
        }
    }

    function setValue(brand_code, brand_generic_code, main_multum_drug_code, Drug_Name, Formulation, Route, Form, Multum_Drug_ID, Multum_Brand_Code, Is_CS, CS2, CS5,surescriptsaccess, found_dose_unit, found_dispense_unit, medical_type, supply_ndc_code)
{
    getFrameByName(top, "leftNoteFrameBottom").document.frm2.brand_code.value = brand_code;
    getFrameByName(top, "leftNoteFrameBottom").document.frm2.brand_generic_code.value = brand_generic_code;
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.main_multum_drug_code.value = main_multum_drug_code;
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.nm_med.value = removeSpecialCharacter(Drug_Name);
	if (Is_CS == "Y") {
		getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsIcon").style.display = "inline";
		getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsDescription").style.display = "inline";
		//getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefill").setAttribute("disabled", true);
		if (CS5 != "Y") {
			var listRefill, indexRefill;
			listRefill = getFrameByName(top, "leftNoteFrameBottom").document.getElementsByClassName("epcsRefillDisable");
			for (indexRefill = 0; indexRefill < listRefill.length; ++indexRefill) {
				listRefill[indexRefill].setAttribute("disabled", true);
			}
		}
		else {
			var listRefill, indexRefill;
			listRefill = getFrameByName(top, "leftNoteFrameBottom").document.getElementsByClassName("epcsRefillDisable");
			for (indexRefill = 0; indexRefill < listRefill.length; ++indexRefill) {
				listRefill[indexRefill].removeAttribute("disabled");
			}
		}
		
		if (getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefillPRN")) {
			getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefillPRN").setAttribute("disabled", true);
		}
		getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsPRNDescription").innerHTML = "* PRN is not allowed for controlled substance";
	}
	else {
		getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsIcon").style.display = "none";
		getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsDescription").style.display = "none";
		//getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefill").removeAttribute("disabled");
		var listRefill, indexRefill;
		listRefill = getFrameByName(top, "leftNoteFrameBottom").document.getElementsByClassName("epcsRefillDisable");
		for (indexRefill = 0; indexRefill < listRefill.length; ++indexRefill) {
			listRefill[indexRefill].removeAttribute("disabled");
		}
		
		if (getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefillPRN")) {
			getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefillPRN").removeAttribute("disabled");
		}
		getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsPRNDescription").innerHTML = "* PRN:As Needed";
	}
	if (CS2 == "Y") {
		if ( getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefill")) {
			getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefill").setAttribute("disabled", true);
		}
		getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsPRNDescription").innerHTML = "* No more than a single dispense is allowed for CS II.";
	}
	else {
		if ( getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefill")) {
			getFrameByName(top, "leftNoteFrameBottom").document.getElementById("epcsRefill").removeAttribute("disabled");
		}
	}
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.formulation.value = removeSpecialCharacter(Formulation);
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.multum_drug_id.value = Multum_Drug_ID;
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.multum_brand_code.value = Multum_Brand_Code;
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.Drugs.value = getFrameByName(top,"leftNoteFrameBottom").document.frm2.Drugs.value  + Multum_Drug_ID + '|';

	getFrameByName(top, "leftNoteFrameBottom").document.frm2.submit_button.setAttribute('disabled', 'disabled');
	for (var i = 0; i < getFrameByName(top,"leftNoteFrameBottom").document.frm2.route_select.length; i++) {
	  if (getFrameByName(top,"leftNoteFrameBottom").document.frm2.route_select[i].value == Route) {
	    getFrameByName(top,"leftNoteFrameBottom").document.frm2.route_select[i].selected = true;
	  }
	}

	getFrameByName(top,"leftNoteFrameBottom").focus();
	getFrameByName(top,"leftNoteFrameBottom").document.frm2.dosage.focus();

	for (var i = 0; i < getFrameByName(top,"leftNoteFrameBottom").document.frm2.dosage_units_select.length; i++) {
	  var a = (getFrameByName(top,"leftNoteFrameBottom").document.frm2.dosage_units_select[i].text).toUpperCase()
	  if (a == found_dose_unit.toUpperCase()) {
	    getFrameByName(top,"leftNoteFrameBottom").document.frm2.dosage_units_select[i].selected = true;
      break;
	  }
	}
	for (var i = 0; i < getFrameByName(top,"leftNoteFrameBottom").document.frm2.dispense_units.length; i++) {
	  var a = (getFrameByName(top,"leftNoteFrameBottom").document.frm2.dispense_units[i].text).toUpperCase()
	  if (a == found_dispense_unit.toUpperCase()) {
	    getFrameByName(top,"leftNoteFrameBottom").document.frm2.dispense_units[i].selected = true;
      break;
	  }
	}
  
  getFrameByName(top, "leftNoteFrameBottom").document.frm2.medical_type.value = medical_type;
  getFrameByName(top, "leftNoteFrameBottom").document.frm2.supply_ndc_code.value = supply_ndc_code;
  
	getFrameByName(top,"leftNoteFrameBottom").setDosage(getFrameByName(top,"leftNoteFrameBottom").document.getElementById('dosage_units_select'));
	//document.location = '/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>';

	document.body.style.cursor = "wait";

	var DrugIds = '';
	var element = document.getElementById("multum_drug_id_list");
        //alert(document.getElementById("multum_drug_id_list").value);
	if (element != null) {

	    DrugIds = document.getElementById("multum_drug_id_list").value + Multum_Drug_ID;
	    //alert(surescriptsaccess);
	    //alert(DrugIds);
	    //alert(surescriptsaccess);
	    if (surescriptsaccess == '1') {
	        document.location = '/DrugFunctions_bh/DrugToDrug.aspx?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&brand_code=' + brand_code + '&PhysicianId=<%=PhysicianId%>&Drug_Name=' + Drug_Name + '&multum_drug_id=' + Multum_Drug_ID + '&main_multum_drug_code=' + main_multum_drug_code + '&Allergies=<%=allergies%>&Drugs=' + DrugIds + '|&SearchBy=ID&Severity=<%=severity%>&allergy_sev=<%=allergy_sev%>&allergy_reac=<%=allergy_reac%>';
	    }
	    else {
	        alert('You do not have access to SureScripts services.');
	        document.body.style.cursor = "default";

	    }
	}

}

function removeSpecialCharacter(inputStr) {
    var result = inputStr;
    var specialChars = ["'", "#"];
    for (var i = 0; i < specialChars.length; i++) {
        result = result.replace(specialChars[i], "");
    }
    return result;
}

</script>


</body>
</html>