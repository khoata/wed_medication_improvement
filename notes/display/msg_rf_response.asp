
<!--#include file="../../../library/connection.inc.asp"-->

<html>
<head>
<title>Refill Response</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>

<script language="javascript">
function chk_refrsp()
{

var radio_choice = false;
var rad_val;

for (counter = 0; counter < document.f1.Response_Type.length; counter++)
{
	if (document.f1.Response_Type[counter].checked)
	{
	rad_val = document.f1.Response_Type[counter].value;
	radio_choice = true; 
	}
}
if (!radio_choice)
{
alert("Please select your response to the Refill.")
return (false);
}

if(rad_val=='ApprovedWithChanges' && document.f1.Response_Note.value=='')
{	alert('Please Enter the Notes to Approve the Refill with Changes');
		document.f1.Response_Note.select();
		return false;
}	

if(rad_val=='Denied' && document.f1.Response_Reason_Code.selectedIndex==0)
{	alert('Please select the Denial Reason from the dropdown menu and enter the notes');
		document.f1.Response_Reason_Code.focus();
		return false;
}

if(rad_val=='ApprovedWithChanges')
{
	document.f1.action ="msg_rf_change.asp"
	document.f1.submit();
	return;
}

if(rad_val=='Approved')
{
	document.f1.action ="msg_rf_app.asp"
	document.f1.submit();
	return;
}

}

function checklength(i)
{
var txt;
txt=document.f1.Response_Note.value;
n=txt.length;
if (n>i) 
{
alert('Only 210 characters are allowed for this field');
document.f1.Response_Note.value=text1;
return;
}
text1=document.f1.Response_Note.value;
}

</script>

</head>
<body>
<body>

<%
drug_id = Request.QueryString("drug_id")
visit_key = Request.QueryString("visit_key")
patient_id = Request.QueryString("patient_id")

If(drug_id)="" or isnull(drug_id) then
  Response.Write "<p align=left><b><font color=red>This Request can not be processed through our system because either Medication Mis match or Patient Mis match, please refill by calling pharmacy directly or by Fax </font></b>"
  Response.end	
end if

set rs1 = server.CreateObject("ADODB.Recordset")
strSQL="select drug_name,PRESCRIBED_BY,formulation from notes_medications where drug_id='" & drug_id & "'"

rs1.open strSQL,con		
If not (rs1.EOF or rs1.BOF) then
	drug_name=rs1("drug_name")&" "&rs1("formulation")
	Physician_ID=rs1("PRESCRIBED_BY")
End if
rs1.close

'check if there is any change requeted from the pharmacy
strSQL="select  a.DRUGID  ,MEDICATIONSUBSTITUTION, MEDICATIONWRITTENDATE,MEDICATIONNOTE,MEDICATIONREFILLQUAN, datecreated "&_
		"from ss_message a, SS_MESSAGE_CONTENT b "&_
		"where a.MESSAGEID=b.MESSAGEID "&_
		"and a.MESSAGETYPE='RefillRequest' and status='OK' "&_
		" and DRUGID='" & drug_id & "' order by datecreated desc "

rs1.open strSQL,con		
If not (rs1.EOF or rs1.BOF) then
	subs_id=rs1("MEDICATIONSUBSTITUTION")
	date_written=rs1("MEDICATIONWRITTENDATE")
	date_written= Mid(date_written, 5, 2)& "/" & Right(date_written, 2) & "/" & Left(date_written, 4)
	med_note=rs1("MEDICATIONNOTE")
	no_refill=rs1("MEDICATIONREFILLQUAN")
	note=rs1("MEDICATIONNOTE")
End if
rs1.close

RS1.open "select * from SS_SUBSTITUTIONS where sub_id="&subs_id , con
If not (rs1.EOF or rs1.BOF) then
	subs_desc=rs1("description")
End if
rs1.close


'get the pharmacy Id
strSQL="select PharmacyID from SS_MESSAGE where messagetype='RefillRequest' and drugid='" & drug_id & "'"
rs1.open strSQL,con		
If not (rs1.EOF or rs1.BOF) then
	NCPDPID=rs1("PharmacyID")
End if
rs1.close

%>
<p align=center><b><u>Refill Response</u></b></p>

<form action="https://www.webedoctor.com/surescriptInterface/default.aspx" method="post" name="f1" onsubmit="return chk_refrsp()">

<input type="hidden" name="selected_mid" value="<%=drug_id%>">
<input type="hidden" name="Visit_Key" value="<%=visit_key%>">
<input type="hidden" name="Patient_Id" value="<%=patient_id%>">
<input type="hidden" name="MsgType" value="RefillResponse">
<input type="hidden" name="NCPDPID" value="<%=NCPDPID%>">
<input type="hidden" name="Physician_ID" value="<%=Physician_ID%>">

<table border="0"  width="100%">
			<tr><td><b>Medication:&nbsp;<%=drug_name%></b></td></tr>
			<tr><td><b>Date Written:</b> <%=date_written%> <br>
				    <b>Refills Remaining:</b><%=no_refill%><br>
				    <b>Substitution:</b> <%=subs_desc%><br>
				    <b>Note:</b> <%=note%><br>------------------------------------------------------------
				</td></tr>			
	
			<tr><td><input type=radio name="Response_Type" value="Approved">Approve</td></tr>			
			<tr><td><input type=radio name="Response_Type" value="ApprovedWithChanges">Approved With Changes</td></tr>
			<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refill:<input name="no_refill" value="<%=no_refill%>" size="1">
					&nbsp;&nbsp;Written Date: <input name="date_written" value="<%=date_written%>" size="8">
					<br>&nbsp;&nbsp;&nbsp;&nbsp;Substitution: 
					<select name="SUB_ID">
						<%RS1.open "select * from SS_SUBSTITUTIONS where sub_id < 2", con
						while not rs1.eof
						if cint(RS1("SUB_ID"))= cint(subs_id) then 
							sel=" selected"
						else
							sel=""
						end if
						%>
							<option value="<%=RS1("SUB_ID")%>" <%=sel%>><%=RS1("DESCRIPTION")%></option>
						<%
						 RS1.movenext
						wend
						RS1.close
						%>
					</select>					
				</td>
			</tr>			
			<tr><td> &nbsp;</td></tr>			
			<tr><td> <input type=radio name="Response_Type" value="Denied">Deny&nbsp;
			<select name="Response_Reason_Code">
			<option value="">Select Denial Reason</option>
				<%
				rs1.open "SELECT * FROM SS_DENIALREASON", con
				while not rs1.eof
				%>
					<option value="<%=rs1("DENIAL_CODE")%>"><%=rs1("DENIAL_desc")%></option>
				<%
				 rs1.movenext
				wend
				rs1.close
				%>
				</select>			
			</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td><b>Note:&nbsp;</b>
					<textarea name="Response_Note" rows="2" cols="35" onkeydown="javascript:checklength(200)"></textarea>
			</td></tr>

</table>
<br><br><br>
<center>
<input type=submit value="Submit" id=a1 name=a1>			
<input type=button value="Close Window" onclick="javascript:window.close();" id=button2 name=button2>

</form>
</center>			
</body>
</html>
