<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<%

visit_key = Request("visit_key")
patient_id = Request("patient_id")
drug_list = Request("list")

if request("sig") = "true" then
	show_ds = 1
end if
if request("necessary") = "true" then
	show_mn = 1
end if

%>
<html>
<head>
<title>Medications &gt; Print Prescription</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
</head>

<body onload="window.focus();window.print();">

<%

set res = server.CreateObject("adodb.recordset")
set res1 = server.CreateObject("adodb.recordset")

strsql="select doctor_id from visit where visit_key='"&visit_key&"'"
res.open strsql,CONN
doc_id= res("doctor_id")
res.Close

strsql="select b.orgname,a.first_name,a.last_name,a.middle_name,a.credential,a.street,a.city,a.state,a.zip,a.wphone,a.ext,a.fax,a.email,a.provider_licno,a.provider_licnostate,a.dea_no,a.dps_no,a.speciality from  doctortable a,streetreg b where a.FACILITY  = b.streetadd and a.docuser='"&doc_id&"'"
res.open strsql,CONN

name=res("first_name")&" "&res("middle_name")&" "&res("last_name")&", "&res("credential")
credential = res("credential")

dim res2
set res2 = Server.CreateObject("ADODB.Recordset")

res2.open "select * from pharmreg where id='"&SQLFixUp(request("pharmacy"))&"' and pid='"&session("pid")&"' order by org_name asc", con

%>
<table width="67%" >
<tr>
<td valign="top"><%=date()%></td>

<td align="left" width="31%" valign="top">
<font size=1>
<%
if not res2.EOF then
%>
<b><%=res2("org_name")%></b><br>
<% if res2("first_name") <> "" then %>
ATTN: <%=res2("first_name")%><br>
<% end if %>
<%=res2("address")%><br>
<%=res2("city")&", "&res2("state")&"  "&res2("zip")%><br>
<% if res2("tel") <> "" then %>
Tel: <%=res2("tel")%><br>
<% end if %>
<% if res2("fax") <> "" then %>
Fax: <%=res2("fax")%><br></font>
<% end if %>
<%
end if
%>
</td>
<td align="left" width="50%" valign="top">
<font size=1>
<b><%=name%></b><br>
<%=res("street")%><br>
<%=res("city")&", "&res("state")&"  "&res("zip")%><br>
Tel: <%=res("wphone")%><br>
<% if res("fax") <> "" then %>
Fax: <%=res("fax")%><br>
<% end if %>
DEA#&nbsp;<%=res("dea_no")%><br>
<% if res("dps_no")<>"" then Response.Write "DPS# "&res("dps_no")&"<br>" %>
License#&nbsp;<%=res("provider_licno")%></font>
</td>
</tr>
</table>

<%
res.close
res1.open "select first_name,middle_name,last_name,birth_date,HOME_PHONE,street,city,state,gender,zip,primary_insurance,patient_employer,ssn from patienttable where userid='"&patient_id&"'",CONN
%>
<p>
<b>Patient</b>
<p><font size=1>
<%=res1("first_name")&" "&res1("last_name")%><br>
<%=res1("street")%><br>
<%=res1("city")&", "&res1("state")&" "&res1("zip")%><br>
<b>Phone:</b> <%=res1("HOME_PHONE")%><br>
<b>DOB:</b> <%=res1("birth_date")%>&nbsp;&nbsp;<b>Sex:</b><%=res1("gender")%>

</font>

<%
res1.close
%>
<p>
<%
res1.open "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and drug_id in (" & drug_list & ")",CONN
if not res1.eof then
  while not res1.eof
%>
<img src="/images/practice/rx.gif" border="0" HEIGHT="12" align="left"> <b><%=res1("drug_name")%></b><p>
<font size=1>
Formulation: <%=res1("formulation")%><br>
<% if trim(res1("dispense").value) <> "" then %>
Dispense: <%=res1("dispense")%><br>
<% end if %>
Dose: <%=res1("dose")%>;&nbsp;Frequency: <%=res1("frequency")%><br>
Route: <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %>;&nbsp;
Refill: <%=res1("refill")%><p>
<%=res1("notes")%>
<p>
<%
    res1.MoveNext
  wend
end if
%>

<b>Prescriber Signature ___________________________________</b>
<br><p style="font-size:0.64em;">
THIS PRESCRIPTION WILL BE FILLED GENERCALLY UNLESS PRESCRIBER WRITES 'daw' IN THE BOX BELOW
</p>

<img src="/images/practice/ny1.jpg" border="0" align="left">
</font>
</body>
</html>
