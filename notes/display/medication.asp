<%@ Language=VBScript %>
<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../../../library/clsEPCSAuditLog.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<!--#include file="../include/functions.inc.asp"-->
<!--#include file="../include/clsAudit.asp"-->

<%
dim epcsAuditLogObj
set epcsAuditLogObj = new clsEPCSAuditLog

visit_key	= Request("visit_key")
patient_id  = Request("patient_id")
other_meds = Session("other_meds")

'dim auditObj
'set auditObj = new clsAudit

'call auditObj.LogActivity("Accessing Medications","","",Cstr(patient_id),Cstr(visit_key),SYSDATE)

dim patientNotesObj
set rs1 = server.CreateObject("adodb.recordset")
set patientNotesObj = new clsPatientNotes
patientNotesObj.SetVisitKey(visit_key)

'if patientNotesObj.Closed = true then
'  Response.Write "<h1>Note has already been closed.</h1>"
'  Response.End
'end if
'get drug interaction severity info
rs1.Open "select minor, moderate, major,critical,noncritical from drug_interactions_severity where physician_id = '"& session("user") &"'",CONN
if not rs1.EOF then
	if rs1("minor") <> "" then
		severity = severity & "1,"
	end if
	if rs1("moderate") <> "" then
		severity = severity & "2,"
	end if
	if rs1("major") <> "" then
		severity = severity & "3,"
	end if
	if rs1("Critical") <> "" then
		A_Sev = A_Sev & "'Critical',"
	end if
	if rs1("Noncritical") <> "" then
		A_Sev = A_Sev & "'Non-Critical',"
	end if
end if
rs1.Close
if severity = "" then
	severity = "1,2,3"
end if

if A_Sev <> "" then
	Sev_Condition = " AND Severity in (" & left(A_Sev, len(A_Sev)-1) & ")"
end if
strQuery = "select * from notes_allergy where patient_id = '"& patient_id &"' and date_resolved is null and Allergy_Type='class'" & Sev_Condition
rs1.Open strQuery,con
if not (rs1.EOF or rs1.BOF) then
	while not rs1.EOF
		Allergies = Allergies & rs1("allergy_text") & ","
		Allergy_Sev = Allergy_Sev & rs1("severity") & "|"
		Allergy_Reac = Allergy_Reac & rs1("Notes") & "|"
		rs1.MoveNext
	wend
end if
rs1.Close

if session("show") = "new" then
  showval = 0
else
  showval = 1
end if

if Request("submit_button") <> "" then

  dim CMD
  err_msg = ""

  ss_flag = trim(Request("ss_flag"))
  drug_name =  trim(Request("nm_med"))
  formulation = unescape(trim(Request("formulation")))
  'formulation = trim(Request("formulation"))
  'formulation = AscEncode(formulation)
  'formulation = ChrEncode(formulation)
  dispense = trim(Request("dispense"))
  dispense_units = trim(Request("dispense_units"))
  dosage = trim(Request("dosage")) & " " & trim(Request("dosage_units"))
  frequency = trim(Request("freq"))
  route = trim(Request("route"))
  refill = trim(Request("refill"))
  if (refill = "") then
	refill = 0
  end if
  notes = trim(Request("notes"))

  Dim numRegEx
  Set numRegEx = New RegExp
  numRegEx.Pattern = "^(\.|\d)+$"
  dispenseRes = numRegEx.Test(dispense)

  if drug_name = "" then err_msg = err_msg & "Please select a medication to prescribe.<br/>"
  if formulation = "" then err_msg = err_msg & "Please describe the medication's formulation.<br/>"
  if dispense  = "" then 
    err_msg = err_msg & "Please enter the dispense quantity.<br/>"
  elseif not IsNumeric(dispense) or not dispenseRes then 
    err_msg = err_msg & "Invalid dispense quantity.<br/>"
  elseif InStr(1, dispense, ".") <> 0 and Len(dispense) > 11 then
    err_msg = err_msg & "Dispense quantity can not be longer than 10 digits.<br/>"
  elseif InStr(1, dispense, ".") = 0 and Len(dispense) > 10 then
    err_msg = err_msg & "Dispense quantity can not be longer than 10 digits.<br/>"
  else 
    dispenseDec = CDBl(dispense)
    if dispenseDec <= 0 then err_msg = err_msg & "Dispense quantity must not be zero.<br/>"
  end if
  if dispense_units = "" then err_msg = err_msg & "Please select an appropriate dispense unit.<br/>"
  if Request("dosage") = "" or Request("dosage_units") = ""  then err_msg = err_msg & "Please enter an appropriate dosage.<br/>"
  if Len(Request("dosage")) > 25 then err_msg = err_msg & "Dosage value should not longer than 25 characters.<br/>"
  'if frequency = "" then err_msg = err_msg & "Please select the frequency of dosage.<br/>"
  'if route = "" then err_msg = err_msg & "Please select the route of administering dosage.<br/>"
  if notes <> "" and Len(notes) > 210 then err_msg = err_msg & "Notes can not be longer than 210 characters.<br/>"

if err_msg = "" then
  dispense = CDBl(dispense) & " " & Request("dispense_units")	  
  if session("role") <> "physician" then
    strQuery = "SELECT a.userid as userid " &_
				" FROM user_pwd a, acc b, streetreg c " &_
				" WHERE lower(a.userid)=lower(b.userid) " &_
				" AND c.streetadd=b.streetadd " &_
				" and a.acc_status='V' " &_
				" and b.role ='physician' " &_
				" and b.streetadd ='"& session("pid") &"'"
	rs1.open strQuery,con
	if not (rs1.EOF or rs1.BOF) then
		prescribed_by = rs1("userid")
	end if
	rs1.close
  else
	prescribed_by = session("user")
  end if

  multum_drug_id = Request("multum_drug_id")
  multum_brand_code = Request("multum_brand_code")
  brand_generic_code = Request("brand_generic_code")
  stn=trim(Request("stn"))
  start_date = trim(Request("start_date"))
  if start_date = "" then
	start_date = date()
  end if
  sub_id = Request("sub_id")
  brand_code = Request("brand_code")
  main_multum_drug_code = Request("main_multum_drug_code")

  set CMD = Server.CreateObject("ADODB.Command")
  CMD.ActiveConnection = CONN

  CONN.BeginTrans

  sqlQuery = "SELECT notes_medications_drug_id.nextval as drug_id FROM DUAL"
  CMD.CommandText = sqlQuery
  set RSSeq = CMD.Execute

  drug_id = RSSeq("drug_id").value
  RSSeq.Close
  set RSSeq = nothing

  'get the NDC code for the drug selected
  strSQL="select ndc_code from ndc_core_description where (OBSELETE_DATE is null or OBSELETE_DATE > SYSDATE) and repackaged = 0 and brand_code="& brand_code &" and main_multum_drug_code="&main_multum_drug_code

  RS1.open  strSQL,conn
 If not (RS1.eof or rs1.bof) then
	ndc_code=trim(rs1("NDC_Code"))
 else
	ndc_code=""
 end if
 rs1.close

  synonym_query = "Select NPP.PKG_PRODUCT_ID, CGP.GENERIC_PRODUCT_NAME, CS.DISPLAY_NAME, CGPS.SYNONYM_TYPE_ID, CGPSR.RXCUI " &_
                        "from NDC_PKG_PRODUCT NPP " &_
                        "left join CORE_GENPRODUCT CGP on NPP.GENPRODUCT_ID = CGP.GENPRODUCT_ID " &_
                        "left join CORE_GENPRODUCT_SYNONYM CGPS on NPP.DRUG_SYN_ID = CGPS.DRUG_SYN_ID " &_
                        "left join CORE_SYNONYM CS on NPP.DRUG_SYN_ID = CS.DRUG_SYN_ID " &_
                        "left join XREF_GENPRODUCT_SYN_RXNORM CGPSR on NPP.DRUG_SYN_ID = CGPSR.DRUG_SYN_ID " &_
                        "where NPP.PKG_PRODUCT_ID = '" & ndc_code & "'"
  synonym_type_id = ""

'response.Write synonym_query
'response.End
    
  rs1.open synonym_query,conn
  If not (rs1.eof or rs1.bof) then
    synonym_type_id=trim(rs1("synonym_type_id"))    
  else
    synonym_type_id=""
  end if
  rs1.close

  rxnorm_query = ""
  rxnorm_code = ""
  if (synonym_type_id = "59") then
     rxnorm_query = "select XGPR.RXNORM_TTY, XGPR.RXCUI as RxNorm, CGP.GENERIC_PRODUCT_NAME, CGP.GENPRODUCT_ID, NPP.PKG_PRODUCT_ID, CS.DISPLAY_NAME " &_
                        "from NDC_PKG_PRODUCT NPP " &_
                        "left join CORE_GENPRODUCT CGP on NPP.GENPRODUCT_ID = CGP.GENPRODUCT_ID " &_
                        "left join XREF_GENPRODUCT_RXNORM XGPR on NPP.GENPRODUCT_ID = XGPR.GENPRODUCT_ID " &_
                        "left join CORE_SYNONYM CS on NPP.DRUG_SYN_ID = CS.DRUG_SYN_ID " &_
                        "where NPP.PKG_PRODUCT_ID = '" & ndc_code & "' " &_
                        "and XGPR.RXNORM_TTY = 'CD'"
  elseif (synonym_type_id = "60") then
    rxnorm_query = "select XGPSR.RXNORM_TTY, XGPSR.RXCUI as RxNorm, CS.DISPLAY_NAME, CS.DRUG_SYN_ID, NPP.PKG_PRODUCT_ID, CS.DISPLAY_NAME " &_
                        "from NDC_PKG_PRODUCT NPP " &_
                        "left join XREF_GENPRODUCT_SYN_RXNORM XGPSR on NPP.DRUG_SYN_ID = XGPSR.DRUG_SYN_ID " &_
                        "left join CORE_SYNONYM CS on NPP.DRUG_SYN_ID = CS.DRUG_SYN_ID " &_
                        "where NPP.PKG_PRODUCT_ID = '" & ndc_code & "'"
  end if

  if (rxnorm_query <> "") then
      rs1.open rxnorm_query,conn
      If not (rs1.eof or rs1.bof) then
        rxnorm_code=trim(rs1("RxNorm"))    
      else
        rxnorm_code=""
      end if
      rs1.close
  end if

  if (other_meds = "Current") then
	med_status = "Current"
  else
	med_status = "O Current"
  end if

  sqlQuery = "INSERT INTO notes_medications " &_
  "(drug_id,visit_key,drug_name,formulation,dispense,dose,frequency,route,refill,REFILL_REMAINING,prescribed_by,prescribed_date,patient_id,notes,iscurrent,multum_drug_id,multum_brand_code,SUBSTITUTION,ndc_code,ss_flag,status,brand_generic_code,rxnorm_code,synonym_type_id) " &_
  "VALUES " &_
  "('" & drug_id & "','" & visit_key & "','" & sqlFixUp(drug_name) & "','" & trim(sqlFixUp(formulation)) & "','" & sqlFixUp(dispense) & "','" & sqlFixUp(dosage) & "','" & sqlFixUp(frequency) & "','" & sqlFixUp(route) & "','" & sqlFixUp(refill) & "','" & sqlFixUp(refill) & "','" & prescribed_by & "',to_date('" & start_date & "','MM/DD/YYYY'),'" & patient_id & "','" & sqlFixUp(notes) &"','"& showval &"','"& multum_drug_id &"','"& multum_brand_code &"','"& sub_id &"','"& ndc_code &"','"& ss_flag &"','"& med_status &"','"& brand_generic_code &"','"& rxnorm_code &"','"& synonym_type_id &"')"
  CMD.CommandText = sqlQuery
  CMD.Execute
 ' call auditObj.LogActivity("Created a new MEDICATION with ID - "+Cstr(drug_id),"",Cstr(drug_name)+","+Cstr(formulation)+","+Cstr(dispense)+","+Cstr(dosage)+","+Cstr(frequency)+","+Cstr(route)+","+Cstr(refill)+","+Cstr(prescribed_by)+","+Cstr(notes)+","+Cstr(showval)+","+Cstr(multum_brand_code)+","+Cstr(sub_id)+","+Cstr(ndc_code),Cstr(patient_id),Cstr(visit_key),SYSDATE)

  CONN.CommitTrans

'---for audit log........
set cmd = server.CreateObject("ADODB.command")
cmd.ActiveConnection = CONN

NewLogdata =  " drug: " & drug_name &  ", formulation: " &   formulation & " ,dispense: " & dispense  & " , dispenseunits: " & dispense_units & " , dosage: " & dosage & " , frequency: " &   frequency & " , route: " & route & ", refill: " & refill

 	strSQL = "INSERT INTO audit_log(USERNAME,  SECTION_NAME, SUB_SECTION, VISIT_KEY, ACTION, FIELD,  OLD_VALUE, NEW_VALUE , TIME_STAMP , IP_ADDR, USER_ROLE, PATIENT_ID, PATIENT_LAST_NAME, PATIENT_FIRST_NAME, VISIT_DATE, VISIT_TIME) " &_
	" SELECT B.DOCTOR_ID,'','',B.visit_key, 'Physician add new medication'  , '','','" & NewLogdata & "', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'),'','Physician', B.PATIENT_ID, A.last_name, A.first_name,  VISIT_DATE,VISIT_TIME  FROM PATIENTTABLE A, VISIT B WHERE A.USERID = B.patient_id AND B.visit_key = '" &  visit_key & "'"
'Response.Write strSQL
		cmd.CommandText = strSQL
		cmd.Execute 
'----------end 

'---START: Add to EPCS_Prescriptions
set rs = server.CreateObject("ADODB.Recordset")
strSQL = "select * from ndc_drugs where csa_schedule in (2,3,4,5) and ndc_code = '" & ndc_code & "'"
rs.open strSQL,con
if not (rs.eof or rs.bof) then
    set cmd = server.CreateObject("ADODB.command")
    cmd.ActiveConnection = CONN
	strSQL = "INSERT INTO EPCS_PRESCRIPTIONS(Drug_Id, Visit_Key, Patient_Id, Doctor_Id, Status, Created_Date) values ('" & drug_id & "','" & visit_key & "','" & patient_id & "','" & session("docid") & "', 'New', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'))"
    cmd.CommandText = strSQL
	cmd.Execute

    '---START: Add to EPCS_AUDIT_LOG
    subText = ""
    if sub_id = 0 then
        subText  = "allowed"
    else
        subText = "not allowed"
    end if

    detailAuditLog = "- Drug Name: " & drug_name & "<br/>" &_
                     "- Formulation: " & formulation & "<br/>" &_
                     "- Dispense: " & dispense & "<br/>" &_
                     "- Dosage: " & dosage & "<br/>" &_
                     "- Frequency: " & frequency & "<br/>" &_
                     "- Duration: " & Request("d_days") & "<br/>" &_
                     "- Route: " & route & "<br/>" &_                 
                     "- Refill: " & refill & "<br/>" &_
                     "- Note: " & notes & "<br/>" &_
                     "- Substitution: " & subText & "<br/>" &_
                     "- Recorded Date: " & start_date & "<br/>"

    call epcsAuditLogObj.LogEPCSActivity("Controlled Substance Prescription","Add New CS Medication",detailAuditLog,"SUCCESS")
    '---END

end if
rs.close

'---End 

end if  
msg = "Medication Added."
if err_msg <> "" then 
  msg = ""
  err_msg = "There is error in input data. Please try again."
end if
  set cmd = server.CreateObject("ADODB.command")
cmd.ActiveConnection = CONN
 	strSQL = "INSERT INTO audit_log(USERNAME,  SECTION_NAME, SUB_SECTION, VISIT_KEY, ACTION, FIELD,  OLD_VALUE, NEW_VALUE , TIME_STAMP , IP_ADDR, USER_ROLE, PATIENT_ID, PATIENT_LAST_NAME, PATIENT_FIRST_NAME, VISIT_DATE, VISIT_TIME) " &_
	" SELECT B.DOCTOR_ID,'"&  sqlFixUp(drug_name) &"','" & trim(sqlFixUp(formulation)) & "',B.visit_key, 'Physician add new medication'  , '','','', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'),'','Physician', B.PATIENT_ID, A.last_name, A.first_name,  VISIT_DATE,VISIT_TIME  FROM PATIENTTABLE A, VISIT B WHERE A.USERID = B.patient_id AND B.visit_key = '" &  visit_key & "'"
'Response.Write strSQL
		cmd.CommandText = strSQL
		cmd.Execute 

  %>
  <script language="javascript">
		 //parent.rightNoteFrame.location.reload(true);
		//document.location='/practice/notes/add/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&msg=Medication Added.';
	parent.leftNoteFrameBottom.location.replace("/practice/notes/add/medication.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>&msg=<%=msg%>&errmsg=<%=err_msg%>")
	parent.rightNoteFrame.location.replace('/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>');
  </script>
  <%

  Response.End

end if


set RSVitals = Server.CreateObject("ADODB.Recordset")
RSVitals.CursorLocation = 3
sqlQuery = "SELECT DISTINCT take FROM notes_vitals WHERE visit_key='" & visit_key & "'"
RSVitals.Open sqlQuery, CONN, 3, 4
VCount = RSVitals.RecordCount
RSVitals.Close

if VCount > 0 then
  sqlQuery = "SELECT * FROM notes_vitals WHERE visit_key='" & visit_key & "' AND vital_type='W' ORDER BY take DESC"
  RSVitals.Open sqlQuery, CONN, 3, 4
  set RSVitals.ActiveConnection = nothing

  dim VCounter

  if RSVitals.RecordCount > 0 then
    weight = RSVitals("vital_data1") & "kgs (" & Round(RSVitals("vital_data1").Value / .4536,2) & "lbs)"
  end if

end if

if weight = "" then
  weight = "Not recorded yet."
end if

Set DoctorDataSet = GetDoctorData(visit_key)
DOCTOR_NAME = DoctorDataSet("Last_Name")&", "&DoctorDataSet("First_Name")
DoctorDataSet.Close
Set DoctorDataSet = nothing

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%=Application("Styles")%>
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<script language="javascript">
function validateForm(that) {
	setDosage();
	//setFreq(document.frm2.freq_select);
	//setRoute(document.frm2.route_select);

	//alert(document.frm2.dosage_units_select.selectedIndex);


  if (that.nm_med.value == "") {
    alert("Please select a medication to prescribe.");
    that.nm_med.focus();
    return false;
  }

  if (that.formulation.value == "") {
    alert("Please describe the medication's formulation.");
    that.formulation.focus();
    return false;
  }



  var re = /^\d+$/;


  /* if (!that.dispense.value.match(re)) {
    alert("Please enter an appropriate amount of drug to dispense.");
    that.dispense.focus();
    return false;
  } */

  if (that.dispense.value == "") {
    alert("Please enter the dispense quantity");
    that.dispense.focus();
    return false;
  }


  if (that.dispense_units.selectedIndex <= 0) {
    alert("Please select an appropriate dispense unit.");
    that.dispense_units.focus();
    return false;
  }

  if (alltrim(that.dosage.value) == '') {
      alert("Please enter an appropriate dosage.");
      that.dosage.focus();
      return false;
  }

  if (that.dosage_units_select.selectedIndex == 0) {
    alert("Please enter an appropriate dosage unit.");
    that.dosage_units_select.focus();
    return false;
  }
  if (that.dosage_units_select.selectedIndex == 21 && that.dosage_units.value == '' ) {
      alert("Please enter Other Dosage Unit.");
      that.dosage_units_select.selectedIndex = 21;
      that.dosage_units.focus();
      return false;
  }

  if (!that.dosage_units.value != "" && that.showval.value == 0) {
      alert("Please select an appropriate dosage unit: " + that.dosage_units.value);
      return false;
  }

  var units = /^[\w\(\)\d]+$/;

  if (that.freq.value == "") {
      setFreq(document.frm2.freq_select);
  }
  if (that.route.value == "") {
      setRoute(document.frm2.route_select);
  }

  if (!that.freq.value != "") {
      alert("Please select the frequency of dosage: " + that.freq.value);
      return false;
  }

  if (!that.route.value != "") {
      alert("Please select the route of administering dosage: " + that.route.value);
      return false;
  }

  if (!isNumber()) {
      return false;
  }

  if (!validateSpecialChars(that)) {
      return false;
  }

  if (that.formulation.value != "") {
      that.formulation.value = escape(that.formulation.value);
      that.formulation.value = that.formulation.value;
  } 
	/*
  var startDate = /^\d{2}\/\d{2}\/\d{4}$/;
  if (that.start_date.value == "") {
	alert("Please set the Medication Start Date.");
	that.start_date.focus();
	return false;
	}
  else
  {
	if (!that.start_date.value.match(startDate))
	{
		alert("Please enter the start date in correct format(MM/DD/YYYY)");
		return false;
	}
  }
  return true; */

  return true;
}

function setDosage() {

  for (var i = 0; i < document.frm2.dosage_units_select.length; i++) {
    if (document.frm2.dosage_units_select[i].selected == true && document.frm2.dosage_units_select[i].text == 'Other') {
      document.getElementById('dosage_tr').style.display = '';
      //document.getElementById('dosage_units').value = '';
      document.getElementById('dosage_units').focus();

      return true;
    }
    if (document.frm2.dosage_units_select[i].selected == true && document.frm2.dosage_units_select[i].text != 'Other') {
      document.getElementById('dosage_tr').style.display = 'none';
      document.getElementById('dosage_units').value = document.frm2.dosage_units_select[i].text;
      return true;
    }
  }
}

function setFreq123() {

  for (var i = 0; i < document.frm2.freq_select.length; i++) {
    if (document.frm2.freq_select[i].selected == true && document.frm2.freq_select[i].text == 'Other') {
      document.getElementById('freq_tr').style.display = '';
      document.getElementById('freq').value = '';
      document.getElementById('freq').focus();

      return true;
    }
    if (document.frm2.freq_select[i].selected == true && document.frm2.freq_select[i].text != 'Other') {
      document.getElementById('freq_tr').style.display = 'none';
      document.getElementById('freq').value = document.frm2.freq_select[i].text;
      return true;
    }
  }

}

function setFreq(that) {

  for (var i = 0; i < that.length; i++) {
    if (that[i].selected == true && that[i].text == 'Other') {
      document.getElementById('freq_tr').style.display = '';
      document.getElementById('freq').value = '';
      document.getElementById('freq').focus();

      return true;
    }
    if (that[i].selected == true && that[i].text != 'Other') {
      document.getElementById('freq_tr').style.display = 'none';
      document.getElementById('freq').value = that[i].text;
      return true;
    }
  }

}


function setRoute123() {

  for (var i = 0; i < document.frm2.route_select.length; i++) {
    if (document.frm2.route_select[i].selected == true && document.frm2.route_select[i].text == 'Other') {
      document.getElementById('route_tr').style.display = '';
      document.getElementById('route').value = '';
      document.getElementById('route').focus();

      return true;
    }
    if (document.frm2.route_select[i].selected == true && document.frm2.route_select[i].text != 'Other') {
      document.getElementById('route_tr').style.display = 'none';
      document.getElementById('route').value = document.frm2.route_select[i].text;
      return true;
    }
  }

}



function setRoute(that) {

  for (var i = 0; i < that.length; i++) {
    if (that[i].selected == true && that[i].text == 'Other') {
      document.getElementById('route_tr').style.display = '';
      document.getElementById('route').value = '';
      document.getElementById('route').focus();

      return true;
    }
    if (that[i].selected == true && that[i].text != 'Other') {
      document.getElementById('route_tr').style.display = 'none';
      document.getElementById('route').value = that[i].text;
      return true;
    }
  }

}

function setDate(frmObj, d) {

  var month, day, year;
  re = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;

  if (frmObj.value.match(re)) {

    var now = new Date(frmObj.value);

    if (d == 0) {
      now.setTime(now.getTime() - 24 * 60 * 60 * 1000);
      month = now.getMonth() + 1;
      day = now.getDate();
      year = now.getYear();

      if (year < 1000)
        year = year + 1900;

      frmObj.value = month + "/" + day + "/" + year;
    } else {
      now.setTime(now.getTime() + 24 * 60 * 60 * 1000);
      month = now.getMonth() + 1;
      day = now.getDate();
      year = now.getYear();
      if (year < 1000)
        year = year + 1900;
      frmObj.value = month + "/" + day + "/" + year;
    }


  } else {
    var today = new Date();
    month = today.getMonth() + 1;
    day = today.getDate();
    year = today.getYear();
    if (year < 1000)
      year = year + 1900;
    frmObj.value = month + "/" + day + "/" + year;

  }
}

function checklength(i)
{
var txt;
	txt=document.frm2.notes.value;
	n=txt.length;
if (n>i)
{
	alert('Only 210 characters are allowed for this field');
	document.frm2.notes.focus();
	document.frm2.notes.value=txt;
return;
}
	text1=document.frm2.notes.value;
}


function fx_number3(){
			var str = document.frm2.dispense.value;
			var valid;
			var val='exp1';

			switch (val){
			case "exp1":
				valid = test1(str);
				break;
			case "exp2":
				valid = test2(str);
				break;
			case "exp3":
				valid = test3(str);
				break;
            case "exp4":
                valid = test4(str);
                break;
			default:
				valid = false;
			}
			function test1(str) {
				str = alltrim(str);
				return /^[-+]?[0-9]+(\.[0-9]+)?$/.test(str);
			}
			function test2(str) {
				str = alltrim(str);
				return /^[-+]?\d+(\.\d+)?$/.test(str);
			}
			function test3(str) {
				str = alltrim(str);
				return /^[-+]?\d{3,5}(\.\d{1,3})?$/.test(str);
			}
			//document.frm2.dispense.value = (valid)?"Valid":"Invalid";

			if (document.frm2.dispense.value !='' &&  valid==false)
			{
				alert("Invalid Number");
				document.frm2.dispense.focus();
			}
}

function isNumber() {
    var val = alltrim(document.frm2.dispense.value);
    if (val != "") {
        var num = Number(val);
        if (isNaN(num) || val.match(/^(\.|\d)+$/g) == null) {
            alert("Invalid Number");
            document.frm2.dispense.focus();
        } else if (val.indexOf(".") != -1 && val.length > 11) {
            alert("Dispense must not be longer than 10 digits.");
            document.frm2.dispense.focus();
        } else if (val.indexOf(".") == -1 && val.length > 10) {
            alert("Dispense must not be longer than 10 digits.");
            document.frm2.dispense.focus();
        } else if (num <= 0) {
            alert("Dispense must not be zero.");
            document.frm2.dispense.focus();
        } else {
            document.frm2.dispense.value = num.toFixed(decimalPlaces(val));
            return true;
        }
    }
    return false;
}

function decimalPlaces(numberStr) {
    // toFixed produces a fixed representation accurate to 20 decimal places
    // without an exponent.
    // The ^-?\d*\. strips off any sign, integer portion, and decimal point
    // leaving only the decimal fraction.
    // The 0+$ strips off any trailing zeroes.
    return (numberStr).replace(/^-?\d*\.?|0+$/g, '').length
}

function alltrim(str) {
			return str.replace(/^\s+|\s+$/g, '');
		}

function dose_dispense()
{
a=document.frm2.dosage.value;
document.frm2.dispense.value=a;
}

function validateSpecialChars(that) {
    if (that.nm_med.value != "" && hasSpecialChars(that.nm_med.value)) {
        alert("Illegal characters (#, ') in Medication Name.");
        that.nm_med.focus();
        return false;
    }

    if (that.formulation.value != "" && hasSpecialChars(that.formulation.value)) {
        alert("Illegal characters (#, ') in Formulation.");
        that.formulation.focus();
        return false;
    }

    if (that.dosage_units_select.selectedIndex != 21 && alltrim(that.dosage.value) != '' && hasSpecialChars(that.dosage.value)) {
        alert("Illegal characters (#, ') in Dosage.");
        that.dosage.focus();
        return false;
    }
    if (that.dosage_units_select.selectedIndex == 21 && that.dosage_units.value != '' && hasSpecialChars(that.dosage_units.value)) {
        alert("Illegal characters (#, ') in Other Dosage.");
        that.dosage_units.focus();
        return false;
    }
    if (that.freq.value != "" && hasSpecialChars(that.freq.value)) {
        alert("Illegal characters (#, ') in Frequency");
        return false;
    }

    if (that.route.value != "" && hasSpecialChars(that.route.value)) {
        alert("Illegal characters (#, ') in Route");
        return false;
    }
    if (that.notes.value != "" && hasSpecialChars(that.notes.value)) {
        alert("Illegal characters (#, ') in Notes.");
        that.formulation.focus();
        return false;
    }
    return true;
}

function hasSpecialChars(inputStr) {
    var specialChars = ["'", "#"];
    for (var i = 0; i < specialChars.length; i++) {
        if (inputStr.indexOf(specialChars[i]) != -1) {
            return true;
        }
    }
    return false;
}

function setdis() {
    for (var i = 0; i < document.frm2.d_days.length; i++) {
        if (document.frm2.d_days[i].selected == true) {
            c = document.frm2.d_days[i].text
        }
    }

    a = document.frm2.dosage.value;
    f = document.getElementById('freq').value;
    if (f == '') { b = 1; }
    if (f == 'Once Daily') { b = 1; }
    if (f == 'Twice Daily') { b = 2; }
    if (f == 'Three Times Daily') { b = 3; }
    if (f == 'Four Times Daily') { b = 4; }
    if (f == 'Five Times Daily') { b = 5; }
    if (f == 'Six Times Daily') { b = 6; }

    if (c == '60 Days') { c = 60; }
    if (c == '90 Days') { c = 90; }
    if (c == '120 Days') { c = 120; }
    if (c == '150 Days') { c = 150; }
    if (c == '180 Days') { c = 180; }

    document.frm2.dispense.value = c * (a * b);

    d = document.frm2.dosage_units_select.selectedIndex;
    if (d == 5) { document.frm2.dispense_units.selectedIndex = 74; }
    //if (d == 6) { document.frm2.dispense_units.selectedIndex = 4; }
   // if (d == 1) { document.frm2.dispense_units.selectedIndex = 17; }

}


</script>
</head>
<body>
<div class="titleBox">Add New Medication</div>
<p>
<% if Request("msg") <> "" then %>
<span style="color: green"><b><%=Request("msg")%></b></span>
<p>
<% end if %>
Fields in <b>BOLD</b> are required.<br>To add a medication NOT FOUND in our database, click on "Drug Name" field.
<p>
<form name="frm2" id="frm2" action="/DrugFunctions/DrugToDrug.aspx" target="rightNoteFrame" method="post" onsubmit="return validateForm(this);">
<!--<form name="frm2" id="frm2" action="medication.asp" target="rightNoteFrame" method="post" onsubmit="return validateForm(this);">-->
<table>
<% if Request("errmsg")<> "" then %>
  <tr>
    <td colspan="2" style="color: red"><%=Request("errmsg")%></td>
  </tr>
<% end if %>
  <tr>
    <input type="hidden" name="ss_flag"/>
    <input type="hidden" name="brand_generic_code"/>
	<input type="hidden" name="multum_drug_id">
	<input type="hidden" name="multum_brand_code">
	<input type="hidden" name="showval" value=<%=showval%>>
	<input type="hidden" name="stn">
	<input type="hidden" name="brand_code">
	<input type="hidden" name="main_multum_drug_code">
	<input type="hidden" name="Drugs" value="<%=Request("currentdrugs")%>">
	<input type="hidden" name="SearchBy" value="ID">
	<input type="hidden" name="visit_key" value="<%=visit_key%>">
	<input type="hidden" name="patient_id" value="<%=patient_id%>">
	<input type="hidden" name="Allergies" value="<%=allergies%>">
	<input type="hidden" name="Severity" value="<%=severity%>">
	<input type="hidden" name="allergy_sev" value="<%=allergy_sev%>">
	<input type="hidden" name="allergy_reac" value="<%=allergy_reac%>">

    <td align="right"><b>Patient Weight</b></td>
    <td><%=weight%></td>
  </tr>
  <tr>
    <td align="right">
		<b>
			<span style="float: left;">
				<img id="epcsIcon" alt="Controlled Substance" src="/images/practice/red_star.gif" border="0" style="display: none;"/>
			</span>
			<a href="#" onclick="getFrameByName(top,'leftNoteFrameTop').gotoBottomBar('/practice/notes/add/medicationNOSS.asp?visit_key=<%=visit_key%>&amp;patient_id=<%=patient_id%>'); return false;">Drug Name</a>
		</b>
	</td>
    <td>
		<input maxlength="100" name="nm_med" size="20" value="<%=drug_name%>" readonly>
		<a href="#" onclick="parent.rightNoteFrame.location='/practice/notes/display/medication_search.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>'">
			<img border="0" height="18" src="/images/global/icon_search.gif">
		</a>
	</td>
  </tr>
  <tr>
    <td align="right"><b>Formulation</b></td>
    <td><input name="formulation" value="<%=forumlation%>" size="25" readonly></td>
  </tr>
  <tr>
    <td align="right"><b>Dose</b></td>
    <td>
	  <input name="dosage" value="<%=dosage%>" size="5" maxlength="9">
      <select id="dosage_units_select" name="dosage_units_select" onchange="setDosage();">
      <option>
      <option>Milligrams
      <option>Micrograms
      <option>Milliliters
      <option>Gram(s)
      <option selected>Tablet(s)
      <option>Capsule(s)
      <option>Tablespoon(s)
      <option>Teaspoon(s)
      <option>Drop(s)
      <option>Puff(s)
      <option>Inhalation(s)
      <option>Suppository(s)
      <option>Application(s)
      <option>Troche(s)
      <option>Lozenge(s)
      <option>Pastille(s)
      <option>Patch(es)
      <option>Ampoule(s)
      <option>Vaginal Ring
      <option>Unit(s)
      <option>Other
      </select>
    </td>
  </tr>
<tr id="dosage_tr" style="display: none;">
<td class="st" align="right"><i>Define Other Dosage</td>
<td><input type="text" id="dosage_units" name="dosage_units" maxlength=15></td>
</tr>
  <tr>
    <td align="right"><b>Frequency</b></td>
    <td>
      <select name="freq_select" id="freq_select" onchange="setFreq(this);">
        <option>
        <option selected>Once Daily
        <option>Twice Daily
        <option>Three Times Daily
        <option>Four Times Daily
        <option>Five Times Daily
        <option>Six Times Daily
        <option>Every Other Day
        <option>7:00am and 4:00pm
        <option>At Bedtime
        <option>Once Weekly
        <option>Once Monthly
        <option>Twice Weekly
        <option>Three Times Weekly
        <option>As Needed
        <option>Every Morning
        <option>Every Night
        <option>Every Morning and Night
        <option>
        <option>Every 0.25 Hours
        <option>Every 0.50 Hours
        <option>Every 1 Hours
        <%
          for i = 2 to 24
        %>
        <option>Every <%=i%> Hours
        <%
          next
        %>
        <option>
        <option>Other
      </select>
    </td>
  </tr>

 <tr>
    <td align="right"><b>Duration</b></td>
    <td>Days:
      <select name="d_days" onchange="setdis();">
        <option>
        <%for i = 1 to 30%>
        <option><%=i%>
        <% next %>
        <option>60 Days
        <option>90 Days
        <option>120 Days
        <option>150 Days
        <option>180 Days
      </select>
    </td>
 </tr>

  <tr id="freq_tr" style="display: none;">
    <td class="st" align="right"><i>other freq.</td>
    <td><input type="text" id="freq" name="freq" maxlength=70></td>
  </tr>



  <tr>
    <td align="right"><b>Route</b></td>
    <td>
      <select name="route_select" id="route_select" onchange="setRoute(this);">
        <option>
      <%
        dim RSRoute
        set RSRoute = Server.CreateObject("ADODB.Recordset")
        RSRoute.CursorLocation = 3
        sqlQuery = "SELECT * FROM multum_route ORDER BY route_description"
        RSRoute.Open sqlQuery, CONN, 3, 4
        set RSRoute.ActiveConnection = nothing

        while not RSRoute.EOF
      %>
        <option value="<%=RSRoute("route_abbr")%>" <% if ucase(RSRoute("route_description")) = "ORAL" then %> selected <% end if %> ><%=PCase(RSRoute("route_description"))%>
      <%
          RSRoute.MoveNext
        wend
        RSRoute.close

      %>
        <option>Other
      </select>
    </td>
  </tr>
  <tr id="route_tr" style="display: none;">
    <td class="st" align="right"><i>other route</td>
    <td><input type="text" id="route" name="route" maxlength="25"></td>
  </tr>
   <tr>
    <td align="right"><b>Dispense</b></td>
    <td>
      <input name="dispense" value="<%=dispense%>" size="6" maxlength=11 onblur="isNumber();">
      <select name="dispense_units">
      <option>
        <%
					set RSDSP = Server.CreateObject("ADODB.Recordset")
					RSDSP.open "select * from SS_DISPENSE where inuse = 1 order by description", con
					while not RSDSP.EOF
		%>
					<option value="<%=RSDSP("description")%>"><%=RSDSP("description")%></option>
		<%
					RSDSP.movenext
					wend
					RSDSP.close
		%>
      </select>
    </td>
  </tr>
	<% if (other_meds = "Current") then %>
		<tr>
			<td align="right"><b>Refill Allowed</b></td>
			<td>
				<select id="epcsRefill" name="refill" SIZE="1" onchange="document.frm2.submit_button.focus();">
	<%
				for i = 0 to 99
    %>
					<%if i > 5 then %>
                        <option class="epcsRefillDisable"><%=i%></option>
                    <%else%>
					    <option><%=i%></option>
                    <%end if %>
    <%
				next
    %>
					<!--<option>60
					<option>90
					<option>120	-->
                    <option id="epcsRefillPRN" value=999>PRN</option>
				</select> times
			</td>
		</tr>
	<% end if%>
		<tr>
	<% if (other_meds = "Current") then %>
			<td align="right">Note to Pharmacist</td>
	<% else %>
			<td align="right">Note to Doctor</td>
	<%end if%>

			<td>
				<textarea name="notes" cols="15" rows="5" onblur="checklength(210)" ></textarea>
			</td>
		</tr>

		<tr>
			<td align="right"><b>Substitution Permitted?</b></td>
			<td>
				<select name="SUB_ID" style='width: 200px;'>
		<%
					set RS1 = Server.CreateObject("ADODB.Recordset")
					RS1.open "select * from SS_SUBSTITUTIONS where sub_id in (0,1)", con
					while not rs1.eof
		%>
					<option value="<%=RS1("SUB_ID")%>"><%=RS1("DESCRIPTION")%></option>
		<%
					RS1.movenext
					wend
					RS1.close
		%>
				</select>
			</td>
		</tr>

	<tr>
	<% if (other_meds = "Current") then %>
		<td align="right"><% if session("show") = "current" then %> Recorded Date <% else %> Prescribed Date <% end if %></td>
	<% else %>
		<td align="right"> Prescribed Date </td>
	<% end if %>
		<td>
			<table border="0">
			<tr>
				<% if (other_meds = "Current") then %>
				<td><input type=text name=start_date id="dt" size=10 value="<%=date()%>" readonly></td>
				<td>&nbsp;<!--<a href="#"><img onClick="setDate(document.getElementById('dt'),1);" alt="Up" src="/images/practice/date_up_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a><br><img src="/images/spacer.gif" WIDTH="1" HEIGHT="1"><br><a href="#"><img onClick="setDate(document.getElementById('dt'),0);" alt="Down" src="/images/practice/date_down_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a>--></td>
				<% else %>
				<td><input type=text name=start_date id="dt" size=10 value="<%=date()%>"></td>
				<td>&nbsp;<a href="#"><img onClick="setDate(document.getElementById('dt'),1);" alt="Up" src="/images/practice/date_up_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a><br><img src="/images/spacer.gif" WIDTH="1" HEIGHT="1"><br><a href="#"><img onClick="setDate(document.getElementById('dt'),0);" alt="Down" src="/images/practice/date_down_arrow.gif" border="0" WIDTH="18" HEIGHT="8"></a></td>
				<% end if%>
			</tr>
			</table>
			(MM/DD/YYYY)
		</td>
	</tr>
 <!-- <tr>
    <td align="right"><b>Provider</td>
    <td><%=DOCTOR_NAME%></td>
  </tr>-->
</table>
<p>
<input type="submit" value="Submit" id="submit_button" name="submit_button">
<input type="button" value="Cancel" onclick="parent.leftNoteFrameTop.toggleTop(); parent.rightNoteFrame.location.replace('/practice/notes/display/medication_history.asp?visit_key=<%=visit_key%>&patient_id=<%=patient_id%>')" id=button1 name=button1>
</form>

<font size="1" id="epcsPRNDescription">* PRN:As Needed</font>
<br/>
<span id="epcsDescription" style="display: none;">
<img alt="Controlled Substance" src="/images/practice/red_star.gif" border="0" /> Controlled Substance.
</span>

</body>
</html>

<%
Function AscEncode(str)
    Dim i
    Dim sAscii

    sAscii = ""
    For i = 1 To Len(str)
        sAscii = sAscii + CStr(Hex(Asc(Mid(str, i, 1))))
    Next

    AscEncode = sAscii
End Function


Function ChrEncode(str)
    Dim i
    Dim sStr

    sStr = ""
    For i = 1 To Len(str) Step 2
        sStr = sStr + Chr(CLng("&H" & Mid(str, i, 2)))
    Next

    ChrEncode = sStr
End Function


%>