<!--#include file="../../../library/connection.inc.asp"-->
<!--#include file="../../../library/functions.inc.asp"-->
<!--#include file="../include/functions.inc.asp"-->
<!--#include file="../include/clsPatientNotes.asp"-->
<%
Response.Buffer = false

function substitute_text(text)

  text = ReplaceStr(text, "<NAME>", PATIENT_NAME, 0)

  if lcase(PATIENT_GENDER) = "male" then
    text = ReplaceStr(text, "<S/HE>", "He", 0)
    text = ReplaceStr(text, "$he/she", "He", 0)
  else
    text = ReplaceStr(text, "<S/HE>", "She", 0)
    text = ReplaceStr(text, "$he/she", "She", 0)
  end if

  substitute_text = text

end function

function fix_text(byval text)
  dim new_text
  new_text = text
  continue = true
  do
    if left(new_text,2) = vbCrLf then
      new_text = right(new_text, len(new_text) - 2)
    else
      continue = false
    end if
  loop while continue = true
  new_text =Replace(new_text,"<br />", "")
  new_text = ReplaceStr(new_text, vbCrLf, "<br>" & vbCrLf, 0)
  'new_text = ReplaceStr(new_text, vbCrLf, vbCrLf, 0)

  fix_text = new_text

end function


gotoanchor = Request.QueryString("goto")


%>
<html>
<head>
<title>Print Visit Note</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
<%=Application("Styles")%>
</head>
<style type="text/css">

thead { display: table-header-group; } 
tfoot { display: table-footer-group; }

</style>

<body onload="window.focus(); window.print();">

<%
 
 visit_key = Request("visit_key")
 
'dim cmd
set cmd = server.CreateObject("ADODB.command")
cmd.ActiveConnection = CONN
 	strSQL = "INSERT INTO audit_log(USERNAME,  SECTION_NAME, SUB_SECTION, VISIT_KEY, ACTION, FIELD,  OLD_VALUE, NEW_VALUE , TIME_STAMP , IP_ADDR, USER_ROLE, PATIENT_ID, PATIENT_LAST_NAME, PATIENT_FIRST_NAME, VISIT_DATE, VISIT_TIME) " &_
	" SELECT B.DOCTOR_ID, '','',B.visit_key,'Physician printing patient notes', '','','', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'),'','Physician', B.PATIENT_ID, A.last_name, A.first_name,  VISIT_DATE,VISIT_TIME  FROM PATIENTTABLE A, VISIT B WHERE A.USERID = B.patient_id AND B.visit_key = '" &  visit_key & "'"
'Response.Write strSQL
		cmd.CommandText = strSQL
		cmd.Execute 
'cmd.close
  
 
%>


<%
 
 visit_key = Request("visit_key")
 
'dim cmd
set cmd = server.CreateObject("ADODB.command")
cmd.ActiveConnection = CONN
 	strSQL = "INSERT INTO audit_log(USERNAME,  SECTION_NAME, SUB_SECTION, VISIT_KEY, ACTION, FIELD,  OLD_VALUE, NEW_VALUE , TIME_STAMP , IP_ADDR, USER_ROLE, PATIENT_ID, PATIENT_LAST_NAME, PATIENT_FIRST_NAME, VISIT_DATE, VISIT_TIME) " &_
	" SELECT B.DOCTOR_ID, '','',B.visit_key,'Physician print  patient notes', '','','', to_date(to_char(sysdate,'MM/DD/YYYY hh12:mi:ss AM'),'MM/DD/YYYY hh12:mi:ss AM'),'','Physician', B.PATIENT_ID, A.last_name, A.first_name,  VISIT_DATE,VISIT_TIME  FROM PATIENTTABLE A, VISIT B WHERE A.USERID = B.patient_id AND B.visit_key = '" &  visit_key & "'"
'Response.Write strSQL
		cmd.CommandText = strSQL
		cmd.Execute 
'cmd.close
 
%>

<%
' Get patient and visit information for visit_key

visit_key = Request("visit_key")
ref_print= Request("ref_print")

set RSPatient = Server.CreateObject("ADODB.Recordset")
RSPatient.CursorLocation = 3

sqlQuery = "SELECT A.userid,A.ChartID, A.first_name, A.last_name, A.middle_name, A.gender,A.REFER_PHYSICIAN_NAME, A.HOME_PHONE  FROM PATIENTTABLE A, VISIT B WHERE A.userid = B.patient_id AND B.visit_key = '" & visit_key & "'"
RSPatient.Open sqlQuery, CONN, 3, 4

set RSPatient.ActiveConnection = nothing

dim PATIENT_NAME
dim PATIENT_GENDER

PATIENT_NAME = RSPatient("first_name") & " " & RSPatient("middle_name") & " " & RSPatient("last_name")
PATIENT_GENDER = RSPatient("gender")
patient_id = RSPatient("userid")
Ref_Phy_Name= RSPatient("REFER_PHYSICIAN_NAME")
Telephone = RSPatient("HOME_PHONE")
chart_id=RSPatient("chartid")

RSPatient.Close
if Ref_Phy_Name <>"" then
	strSQL="Select * from Referring_Physicians where Record_Owner = '"& Session("pid") &"' and lastname||','||firstname='"& Ref_Phy_Name &"'"
	RSPatient.Open strsql,CONN,3,4
	if not RSPatient.EOF then
		Ref_Name = "Dr. "&Ref_Phy_Name&", "
		ref_st= RSPatient("address")
		ref_city =RSPatient("city")&", "&RSPatient("state")&"  "&RSPatient("zip")
		ref_phone="Phone: "&RSPatient("phone")
		ref_fax="Fax: "&RSPatient("Fax")
		ref_npi ="NPI: "&RSPatient("npi")
	end if
	RSPatient.Close
end if


if visit_key <> "" then

set RS = Server.CreateObject("ADODB.Recordset")
set RSNoteData = Server.CreateObject("ADODB.Recordset")

sqlQuery = "SELECT A.*, B.* FROM visit A, patienttable B WHERE A.visit_key = '" & visit_key & "' AND A.patient_id = B.userid"
RS.CursorLocation = 3
RS.Open sqlQuery,CONN,3,4
set RS.ActiveConnection = nothing

sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+) AND A.VISIT_KEY='" & visit_key & "' ORDER BY CUSTOM_NOTE ASC"

RSNoteData.CursorLocation = 3
RSNoteData.Open sqlQuery, CONN,3,4
RSNoteData.ActiveConnection = nothing

dim RSPastMedicalHx
set RSPastMedicalHx = server.CreateObject("ADODB.RecordSet")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type, d.visit_date FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B, VISIT C, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')D WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+)  AND C.VISIT_KEY = A.VISIT_KEY and C.visit_date <= D.visit_date and A.template_type = '7' and C.patient_id = '"&patient_id&"' ORDER BY A.CUSTOM_NOTE ASC"
RSPastMedicalHx.CursorLocation = 3
RSPastMedicalHx.Open sqlQuery, CONN

'Response.Write sqlQuery

dim RSFamilyHx
set RSFamilyHx = server.CreateObject("ADODB.RecordSet")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type, d.visit_date FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B, VISIT C, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')D WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+)  AND C.VISIT_KEY = A.VISIT_KEY and C.visit_date <= D.visit_date and A.template_type = '13' and C.patient_id = '"&patient_id&"' ORDER BY A.CUSTOM_NOTE ASC"
RSFamilyHx.CursorLocation = 3
RSFamilyHx.Open sqlQuery, CONN

dim RSSocialHx
set RSSocialHx = server.CreateObject("ADODB.RecordSet")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type, d.visit_date FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B, VISIT C, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')D WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+)  AND C.VISIT_KEY = A.VISIT_KEY and C.visit_date <= D.visit_date and A.template_type = '14' and C.patient_id = '"&patient_id&"' ORDER BY A.CUSTOM_NOTE ASC"
RSSocialHx.CursorLocation = 3
RSSocialHx.Open sqlQuery, CONN

dim RSSurgicalHx
set RSSurgicalHx = server.CreateObject("ADODB.RecordSet")
sqlQuery = "SELECT A.template_id,A.soap_id,A.custom_note, A.sentence_text, B.description_text, A.template_type, d.visit_date FROM NOTES_SOAP_TEXT A, NOTES_TEMPLATE_INDEX B, VISIT C, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')D WHERE A.TEMPLATE_ID = B.TEMPLATE_ID(+)  AND C.VISIT_KEY = A.VISIT_KEY and C.visit_date <= D.visit_date and A.template_type = '12' and C.patient_id = '"&patient_id&"' ORDER BY A.CUSTOM_NOTE ASC"
RSSurgicalHx.CursorLocation = 3
RSSurgicalHx.Open sqlQuery, CONN

set RSProblemList = server.CreateObject("ADODB.RecordSet")
RSProblemList.CursorLocation = 3
sqlQuery = "SELECT DISTINCT a.PROBLEM_ID,a.VISIT_KEY,a.DIAG_CODE,d.icdname as DIAG_DESC,a.STATUS,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_ONSET,'mm/dd/yyyy') as date_onset,to_char(a.DATE_RESOLVED,'mm/dd/yyyy') as date_resolved,a.NOTES FROM notes_PROBLEMLIST a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c, icdtable d WHERE a.visit_key = b.visit_key and b.visit_date <= c.visit_date and a.diag_code = d.icdcode and a.patient_id='" & patient_id & "'"
RSProblemList.Open sqlQuery, CONN,3,4

set RSAllergy = Server.CreateObject("ADODB.Recordset")
RSAllergy.CursorLocation = 3
sqlQuery = "SELECT DISTINCT a.ALLERGY_ID,a.VISIT_KEY,a.RECORDED_BY,a.ALLERGY_TEXT,a.STATUS,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_ONSET,'mm/dd/yyyy') as date_onset,to_char(a.DATE_RESOLVED,'mm/dd/yyyy') as date_resolved,a.APPROVED_BY,a.NOTES FROM notes_allergy a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c WHERE a.visit_key = b.visit_key and b.visit_date <= c.visit_date and a.patient_id='" & patient_id & "'"
RSAllergy.Open sqlQuery, CONN, 3, 4

Dim RSDiagTests
set RSDiagTests = server.CreateObject("ADODB.RecordSet")
strSQL = "select STUDY_DATE from RTE_ECHO where patient_id = '"& patient_id &"' and to_date(STUDY_DATE,'mm/dd/yyyy') <= (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"')"
RSDiagTests.CursorLocation = 3
RSDiagTests.Open strSQL, CONN

Dim RSNoteAmend
Set RSNoteAmend = server.CreateObject("ADODB.RecordSet")
sqlQuery = "Select * from NOTES_AMEND where visit_key = '"&visit_key&"'"
RSNoteAmend.CursorLocation = 3
RSNoteAmend.Open sqlQuery, CONN

Set PatientDataSet = GetPatientData(Request("Visit_Key"))
Set DoctorDataSet = GetDoctorData(Request("Visit_Key"))
DOCTOR_NAME = DoctorDataSet("First_Name") & " " & DoctorDataSet("Last_Name") & "," & DoctorDataSet("Credential")
DOCTOR_ADDRESS1 = DoctorDataSet("Street")
DOCTOR_ADDRESS2 = DoctorDataSet("City")&","&DoctorDataSet("State")&"  "&DoctorDataSet("Zip")
DOCTOR_phone = DoctorDataSet("Phone")
DoctorDataSet.Close
Set DoctorDataSet = nothing

%>

<%
DIM RS1
Set RS1 = Server.CreateObject("ADODB.RecordSet")

strSQL="select * from PATIENT_INSURANCE where patient_id = '"& SQLFixUp(patient_id) &"' order by INS_ORDER"
Rs1.Open strsql,CONN

if not( rs1.eof or rs1.bof) then
 i=0
 redim Grp(10)
 redim id(10)
 redim nt(10)
	while not rs1.eof
	  Grp(i)= rs1("policy_group")
	  id(i)= rs1("insurance_id")
	  nt(i)= rs1("notes")
	  i=i+1
	  rs1.movenext
	 wend
else
	i=0
	redim Grp(10)
	redim id(10)
	redim nt(10)
	for i=0 to 9
		Grp(i)=""
		id(i)=""
		nt(i)=""
		i=i+1
	next
end if
rs1.close

RS1.open "select visit_id,visit_date,visit_key,visit_time from visit where visit_key = '"&visit_key&"'",CONN
age = getAge(RS1("visit_date").Value, PatientDataSet("birth_date"))

%>

<!--To print the header on each page. Do not change this -->
<table border=0 align="center" width="100%">
<thead>
	<tr>
		<th width=100% align=left >
			<font size=2> 
			<%=PCase(PatientDataSet("last_name"))%>, <%=PCase(PatientDataSet("first_name"))%>
			&nbsp;(ChartID# <%=chart_id%>);
			&nbsp;DOB:<%=PatientDataSet("birth_date")%>;
			&nbsp;Age:<%=age%>;
			&nbsp;Visit Date:<%=RS1("visit_date")%>;
			&nbsp;Phone:<%=Telephone%>
			</font>
			<hr>
		</th>
	</tr>
</thead> 
<tbody> 
<tr> 
<td width="100%">
<!--Do not change the above code -->


<table width="100%">
<%
set res = server.CreateObject("adodb.recordset")
set res1 = server.CreateObject("adodb.recordset")
res.open "select * from streetreg where streetadd='"&ucase(session("pid"))&"'",CONN
%>
<tr>
	<td align="left" width="100%" valign="top">
		<h2 style="display: inline;"><%=res("orgname")%></h2><br>
		<%=Doctor_Address1%><br>
		<% if res("addr2") <> "" then Response.Write res("addr2") & "<br>" end if %>
		<%=Doctor_Address2%><br>
		Phone:&nbsp;<%=DOCTOR_phone%><br>
		Fax:&nbsp;<%=res("Fax")%>
	</td>
</tr>
</table>
<p>
<div class="titleBox"><u>Visit Note</u></div>
<p>

<%
'pat_age = datediff("YYYY",PatientDataSet("birth_date"),date)
pat_age = left(age,2)
%>
<h2 style="display: inline;"><%=PCase(PatientDataSet("last_name"))%>, <%=PCase(PatientDataSet("first_name"))%></h2> -
<b>DOB:</b> <%=PatientDataSet("birth_date")%> <b>Age:</b> <%=age%> - 
<b>Visit Date:</b>
<%
Response.Write RS1("visit_date") & " " & RS1("visit_time")
%>
<!--<B>- BWC Claim:</b> <%'=PatientDataSet("BWC_Claim")%> -->
<br><b>Tel:</b> <%=Telephone%>

<% if trim(rs("PRIMARY_INSURANCE")) <>"" then %>
    <br><b>Insurance Info: </b>    
		<%=rs("PRIMARY_INSURANCE")%>
		<B>Group#</B> <%=Grp(0)%>
		<B>Insurance ID#</B> <%=id(0)%>		
  <% end if %>

<br><b>Provider:</b>&nbsp;<%=DOCTOR_NAME%>
<% If Ref_Phy_Name<>"" then %>
<br><b>Referred By:</b>&nbsp;<%=Ref_Name%>&nbsp;<%=ref_st%>&nbsp;<%=ref_city%>&nbsp;<%=ref_phone%>&nbsp;<%=ref_fax%>&nbsp;<%=ref_npi%>
<%end if %>
<p>

<%

'apptrequest, confappt

sqlQuery = "SELECT reason_visit, doctor_id,pain_m FROM visit WHERE visit_key='" & visit_key & "'"

set RSCC = CONN.Execute(sqlQuery)

if not RSCC.EOF then
  doctor_id =  RSCC("doctor_id").value
  pain_m =  RSCC("pain_m").value
  reason_visit = RSCC("reason_visit").value
  if reason_visit = "" then reason_visit = "No chief complaint noted."
else
  reason_visit = "No chief complaint noted."
end if

set RSCC = nothing
%>

<%If ref_print="1" then %>
<br><br>
Thank you for your kind referral requesting consultation for this <%=pat_age%>&nbsp;year old&nbsp;<%=lcase(patient_gender)%>&nbsp;with a <%=reason_visit%>. Below you will find the patientís office visit note.
<%end if%>

<table width="100%" cellpadding="3" cellspacing="0" border="0">
<tr>
<td width="100%" valign="top">

<p>
<b><u>CC:</u></b>
<p>
<b><%=reason_visit%></b>

<%'--smoking status
sqlQuery = "SELECT smoke, visit_date FROM visit WHERE smoke is not null and patient_id ='" & patient_id & "'"
set RSCC = CONN.Execute(sqlQuery)
If not(RSCC.eof or RSCC.bof) then
%>
<%
	if not RSCC.EOF then
        Response.Write "<br><b><u>Smoking Status:</u></b><br>"
		while not RSCC.EOF
			smoke= RSCC("smoke")
			smoke_date= RSCC("visit_date")
		    If smoke="1" then 
				smoke="Current every day smoker"
		    elseif smoke="2" then 
				smoke="Current some day smoker"
			elseif smoke="3" then 
				smoke="Former smoker"
			elseif smoke="4" then 
				smoke="Never smoker"
			elseif smoke="5" then 
				smoke="Smoker, current status unknown"
			elseif smoke="9" then 
				smoke="Unknown if ever smoked"
			else
				smoke=smoke
			end if				
		  Response.Write smoke&"(as of "&smoke_date&")"&"<br>"		  
		  RSCC.movenext
		wend
	end if
	set RSCC = nothing
end if
%>


<%
RSNoteData.Filter = "template_type = '2'"

if RSNoteData.RecordCount > 0 then
%>
<p>
<b><u>HPI:</u></b>
<%
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<P>" & fix_text(RSNoteData("sentence_text"))
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text")) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
  end if
  RSNoteData.MoveNext
wend
end if

if Request("hx") <> "0" then

if RSPastMedicalHx.RecordCount > 0 then
%>
<p>
<b><u>Past Medical Hx:</u></b>
<%

RSPastMedicalHx.MoveFirst
while not RSPastMedicalHx.EOF
  if Response.IsClientConnected = false then Response.End
  if RSPastMedicalHx("custom_note") = "1" then ' free text
    Response.Write "" & fix_text(RSPastMedicalHx("sentence_text"))
    Response.Write "<p>"
  else ' not free text
    if RSPastMedicalHx("description_text") <> "" and RSPastMedicalHx("description_text") <> vbCrLf then
		Response.Write "<P><B>" & substitute_text(RSPastMedicalHx("description_text")) & "</B>"
    else
		 Response.Write "" 'Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSPastMedicalHx("template_id") & "'>" & fix_text(RSPastMedicalHx("sentence_text")) & "</div>"
    Response.Write "<p>"  end if
  RSPastMedicalHx.MoveNext
wend
end if

if RSFamilyHx.RecordCount > 0 then
%>
<p>
<b><u>Family Hx:</u></b>
<%	RSFamilyHx.MoveFirst
	while not RSFamilyHx.EOF
	  if Response.IsClientConnected = false then Response.End
	  if RSFamilyHx("custom_note") = "1" then ' free text
	    Response.Write "" & fix_text(RSFamilyHx("sentence_text"))
	    Response.Write "<p>"
	  else ' not free text
	    if RSFamilyHx("description_text") <> "" and RSFamilyHx("description_text") <> vbCrLf then
	      Response.Write "<P><B>" & substitute_text(RSFamilyHx("description_text"), patient_name, patient_gender) & "</B>"
	    else
	      Response.Write "" ' Clear
	    end if
	    Response.Write "<div style='display: inline;' id='id_" & RSFamilyHx("template_id") & "'>" & fix_text(RSFamilyHx("sentence_text")) & "</div>"
	     Response.Write "<p>"  end if
	  RSFamilyHx.MoveNext
	wend
end if
end if ' print hx
%>

<% problem_list_print = Request("pl")
	if session("pid")= "SHA1050" then 
		problem_list_print =0
    end if

	if problem_list_print <> "0" then
	if RSProblemList.RecordCount > 0 then %>
<p>
<b><u>Problem List:</u></b><br>
<% while not RSProblemList.EOF
	if Response.IsClientConnected = false then Response.End
	Response.Write "<b>" & RSProblemList("diag_code")& "- <span class='st'>" & RSProblemList("diag_desc") & "</span></b>"
	if RSProblemList("date_onset") <> "" then
	  Response.Write " - onset: " & RSProblemList("date_onset")
	end if
	if RSProblemList("date_resolved") <> "" then
	  Response.Write " - resolved: " & RSProblemList("date_resolved")
	end if
	if RSProblemList("notes") <> "" then
	  Response.Write " - note: " & RSProblemList("notes")
	end if
	Response.Write "<br>"
	RSProblemList.MoveNext
	Wend
 end if
end if
	RSProblemList.Close
%>

<% if not RSAllergy.EOF then %>
<p>
<b><u>Allergies:</u></b><br>
<%

while not RSAllergy.EOF
  if Response.IsClientConnected = false then Response.End
  Response.Write "<b><span style='color: #FF0000'>" & RSAllergy("allergy_text") & "</span></b>"
  if RSAllergy("date_onset") <> "" then
    Response.Write " - onset: " & RSAllergy("date_onset")
  end if
  if RSAllergy("date_resolved") <> "" then
    Response.Write " - resolved: " & RSAllergy("date_resolved")
  end if
  if RSAllergy("notes") <> "" then
    Response.Write " - note: " & RSAllergy("notes")
  end if
  Response.Write "<br>"
  RSAllergy.MoveNext
wend

end if

RSAllergy.Close

set RSMeds = Server.CreateObject("ADODB.Recordset")
RSMeds.CursorLocation = 3
'sqlQuery = "select DISTINCT a.* from notes_medications a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c where a.visit_key = b.visit_key and b.visit_date <= c.visit_date and a.patient_id='" & patient_id & "' and a.status='Current'"
'sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.prescribed_date <= c.visit_date AND a.patient_id='" & patient_id & "' AND a.status='Current' UNION SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"&patient_id&"' AND visit_key = '"&visit_key&"') c WHERE   a.prescribed_date <= c.visit_date AND a.stop_date > c.visit_date AND a.patient_id='"&patient_id&"' AND a.status='Stopped'"
sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status in ('Current','O Reconciled')  AND a.prescribed_date <= c.visit_date" &_
		   " UNION " &_
		   "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status='Stopped' AND a.prescribed_date <= c.visit_date and a.stop_date > c.visit_date "

RSMeds.Open sqlQuery, CONN, 3, 4
%>

<p>
<b><u>Current Medications:</u></b>
<br>

<%
if RSMeds.RecordCount > 0 then

while not RSMeds.EOF
  if Response.IsClientConnected = false then Response.End
%>
<b><%=RSMeds("drug_name")%></b> - <%=RSMeds("formulation")%> (<%=RSMeds("dose")&"/"&RSMeds("frequency")%>)<br>
<%
  RSMeds.MoveNext
wend
else
	Response.Write "No Medications<br>"
end if
RSMeds.close
%>

<% 'if request("o_meds") = "1"  then %>
<%
'sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "' AND a.status='O Current' AND a.prescribed_date <= c.visit_date" 
sqlQuery = "SELECT DISTINCT a.* FROM notes_medications a, (SELECT visit_date FROM visit WHERE patient_id = '"& patient_id &"' AND visit_key = '"& visit_key &"') c WHERE a.patient_id='" & patient_id & "'  AND a.status in ('O Current','O Reconciled')" 
RSMeds.Open sqlQuery, CONN
%>
	<p>
	<b><u>Meds from other sources:</u></b>
	<br>

	<%
	while not RSMeds.EOF
	%>
	<b><%=RSMeds("drug_name")%></b> - <%=RSMeds("formulation")%> (<%=RSMeds("dose")&"/"&RSMeds("frequency")%>)<br>
	<%
	  RSMeds.MoveNext
	wend
'end if

'---opth prescriptions --start
set RSOpth = Server.CreateObject("ADODB.Recordset")
sqlQuery = "SELECT * FROM OPTH_PRESCRIPTIONS where visit_key = '"& visit_key &"'"
RSOpth.Open sqlQuery, CONN, 3, 4
if not (RSOpth.eof or RSOpth.bof) then

	%>
	<p>
	<u><b>Opth Prescriptions:</b></u>
	<br>
	<%while not RSOpth.EOF	%>
		<%If RSOpth("pres_type")="CL" then%>
				Contact Lens;<%=RSOpth("PRES_RECORD_DATE")%>;<%=RSOpth("PRES_TS")%><br>				
		<%elseIf RSOpth("pres_type")="EG" then%>
				Eyeglass;<%=RSOpth("PRES_RECORD_DATE")%>;<%=RSOpth("PRES_TS")%><br>
		<%end if%>	
		<% od_1=RSOpth("OD_1")
		   od_2=RSOpth("OD_2")	
		   od_3=RSOpth("OD_3")
		   od_4=RSOpth("OD_4")
		   od_5=RSOpth("OD_5")
		   od_6=RSOpth("OD_6")
		   od_7=RSOpth("OD_7")
		   
		   os_1=RSOpth("OS_1")
		   os_2=RSOpth("OS_2")	
		   os_3=RSOpth("OS_3")
		   os_4=RSOpth("OS_4")
		   os_5=RSOpth("OS_5")
		   oS_6=RSOpth("OS_6")
		   os_7=RSOpth("OS_7")
		
		If RSOpth("pres_type") = "CL" then 
			r1="PWR(Sphere)"
			r2="BC"
			r3="DIA"
			r4="CYL"
			r5="AXIS"
			r6="ADD"
			r7="COLOR"				
		elseif RSOpth("pres_type")="EG" then
			r1="SPH"
			r2="CYL"
			r3="AXIS"
			r4="ADD"
			r5="PRISM"
			r6="PD"
			r7="NEAR PD"
		end if
     %>		
<table>
<tr>
    <td>&nbsp;</td>
    <td><b>OD(Right)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td><b>OS(Left)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
   
  <tr>
    <td><%=r1%></td>
    <td><%=od_1%></td>
	<td><%=os_1%></td>
  </tr>
  <tr>
    <td><%=r2%></td>
    <td><%=od_2%></td>
	<td><%=os_2%></td>
  </tr>
  <tr>
    <td><%=r3%></td>
    <td><%=od_3%></td>
	<td><%=os_3%></td>
  </tr>
  <tr>
    <td><%=r4%></td>
    <td><%=od_4%></td>
	<td><%=os_4%></td>
  </tr>
  <tr>
    <td><%=r5%></td>
    <td><%=od_5%></td>
	<td><%=os_5%></td>
  </tr>
  <tr>
    <td><%=r6%></td>
    <td><%=od_6%></td>
	<td><%=os_6%></td>
  </tr>
  <tr>
    <td><%=r7%></td>
    <td><%=od_7%></td>
	<td><%=os_7%></td>
  </tr>	
</table>	
<%	
	  RSOpth.MoveNext
	wend
end if
RSOpth.close

'---opth prescriptions --End

if Request("hx") <> "0" then
%>
<p>

<b><u>Social Hx:</u></b>
<%
if RSSocialHx.RecordCount > 0 then
RSSocialHx.MoveFirst
while not RSSocialHx.EOF
  if Response.IsClientConnected = false then Response.End
  if RSSocialHx("custom_note") = "1" then ' free text
    Response.Write "" & fix_text(RSSocialHx("sentence_text"))
    Response.Write "<p>"
  else ' not free text
    if RSSocialHx("description_text") <> "" and RSSocialHx("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSSocialHx("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSSocialHx("template_id") & "'>" & fix_text(RSSocialHx("sentence_text")) & "</div>"
    Response.Write "<p>"  end if
  RSSocialHx.MoveNext
wend
end if
%>
<p>

<b><u>Surgical Hx:</u></b>
<%

if RSSurgicalHx.RecordCount > 0 then
RSSurgicalHx.MoveFirst
while not RSSurgicalHx.EOF
  if Response.IsClientConnected = false then Response.End
  if RSSurgicalHx("custom_note") = "1" then ' free text
    Response.Write "" & fix_text(RSSurgicalHx("sentence_text"))
    Response.Write "<p>"
  else ' not free text
    if RSSurgicalHx("description_text") <> "" and RSSurgicalHx("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSSurgicalHx("description_text"), patient_name, patient_gender) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSSurgicalHx("template_id") & "'>" & fix_text(RSSurgicalHx("sentence_text")) & "</div>"
    Response.Write "<p>"  end if
  RSSurgicalHx.MoveNext
wend
end if
end if
%>

<%
RSNoteData.Filter = "template_type = '10'"
if RSNoteData.RecordCount > 0 then
%>
<p>
<b><u>Habits:</u></b>
<%

RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<P>" & fix_text(RSNoteData("sentence_text"))
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text")) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
  end if
  RSNoteData.MoveNext
wend
end if

RSNoteData.Filter = "template_type = '3'"

if RSNoteData.RecordCount > 0 then
%>
<p>
<b><u>ROS:</u></b>
<%
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<P>" & fix_text(RSNoteData("sentence_text"))
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & RSNoteData("description_text") & "</B><BR>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
  end if
  RSNoteData.MoveNext
wend

end if

set RSSubSpec = Server.CreateObject("ADODB.Recordset")
RSSubSpec.CursorLocation = 3
'sqlQuery = "SELECT * FROM notes_allergy WHERE visit_key='" & visit_key & "'"
sqlQuery = "SELECT DISTINCT a.ID,a.VISIT_KEY,a.RECORDED_BY,a.SPECIALTY_TYPE,a.SPECIALIST_NAME,a.PATIENT_ID,to_char(a.DATESTAMP,'mm/dd/yyyy') as datestamp,to_char(a.DATE_STARTED,'mm/dd/yyyy') as date_started,to_char(a.DATE_ENDED,'mm/dd/yyyy') as date_ended,a.APPROVED_BY,a.NOTES FROM notes_subspecialists a, visit b, (select visit_date from visit where patient_id = '"& patient_id &"' and visit_key = '"& visit_key &"') c WHERE a.visit_key = b.visit_key and b.visit_date <= c.visit_date and a.patient_id='" & patient_id & "'"
'Response.Write sqlQuery
RSSubSpec.Open sqlQuery, CONN, 3, 4

if not RSSubSpec.EOF then

%>
<p>
<b><u>Sub Specialists:</u></b>
<br>
<%

while not RSSubSpec.EOF
  if Response.IsClientConnected = false then Response.End
  Response.Write "<b><span>" & RSSubSpec("Specialty_Type") & " : " & RSSubSpec("Specialist_Name") & "</span></b>"
  if RSSubSpec("date_started") <> "" then
    Response.Write " - started: " & RSSubSpec("date_started")
  end if
  if RSSubSpec("date_ended") <> "" then
    Response.Write " - ended: " & RSSubSpec("date_ended")
  end if
  if RSSubSpec("notes") <> "" then
    Response.Write " - note: " & RSSubSpec("notes")
  end if
  Response.Write "<br>"
  RSSubSpec.MoveNext
wend

end if

RSSubSpec.Close

%>
<% if pain_m <>"" then %>
<b><u><br>Pain Management:</u></b>
<%
response.Write "<br>"&pain_m&"<br>"
end if
%>

<p>
<div style="page-break-before: auto">


<% '--vital signs -start

set RSVitals = Server.CreateObject("ADODB.Recordset")
RSVitals.CursorLocation = 3
sqlQuery = "SELECT DISTINCT take FROM notes_vitals WHERE visit_key='" & visit_key & "'"
RSVitals.Open sqlQuery, CONN, 3, 4
VCount = RSVitals.RecordCount
RSVitals.Close

if VCount > 0 then

%>
<p>
<b><u>Vital Signs:</u></b>
<br>
<%
  sqlQuery = "SELECT * FROM notes_vitals WHERE visit_key='" & visit_key & "' ORDER BY take ASC, vital_type ASC"
  RSVitals.Open sqlQuery, CONN, 3, 4
  set RSVitals.ActiveConnection = nothing

  dim VCounter, invalidVital
  invalidVital = false

  for VCounter = 1 to VCount
    if Response.IsClientConnected = false then Response.End
    'reset Ophal flag
    Ophal_Started = 0
    IP_Stared = 0
    MR_Started = 0
    P_Started = 0

    RSVitals.Filter = "take = '" & VCounter & "'"
    RSVitals.Sort = " Vital_ID ASC"
    if RSVitals.RecordCount > 0 then

      if RSVitals("status") = "I" then
        Response.Write "<s>"
        invalidVital = true
      else
		Response.Write "</s>"
      end if
      Response.Write ("<br><b>Take " & VCounter & "</b>: ")
      Response.Write "" & RSVitals("datestamp") & " - <BR>"

      RSVitals.MoveFirst
      while not RSVitals.EOF
        select case RSVitals("vital_type").Value
          case "T":
            Response.Write "<b>T:</b>" & RSVitals("vital_data1")&";"
          case "HR":
            Response.Write "<b>HR:</b>" & RSVitals("vital_data1")&";"
          case "BP":
            Response.Write "<b>BP:</b>" & RSVitals("vital_data1") & "/" & RSVitals("vital_data2")&";"
            if RSVitals("vital_data3").value <> "" then Response.Write " " & RSVitals("vital_data3").value
            if RSVitals("vital_data3").value <> "" and RSVitals("vital_data4").value <> "" then Response.Write ","
            if RSVitals("vital_data4").value <> "" then Response.Write " " & RSVitals("vital_data4").value&";"
          case "RR":
            Response.Write "<b>RR:</b>" & RSVitals("vital_data1")&";"
          case "W":
            Response.Write "<b>Weight:</b>" & RSVitals("vital_data1") & "kgs (" & Round(RSVitals("vital_data1").Value / .4536,2) & "lbs)"&";"
          case "H":
            Response.Write "<b>Height:</b>" & RSVitals("vital_data1") & "cm (" & Round(RSVitals("vital_data1").Value * .3937,2) & "in)"&";"
          case "HC":
            Response.Write "<b>Head:</b>" & RSVitals("vital_data1") & "cm (" & Round(RSVitals("vital_data1").Value * .3937,2) & "in)"&";"
          case "BFP":
            Response.Write "<b>Body Fat%:</b>" & RSVitals("vital_data1")&";"
          case "BMI":
            Response.Write "<b>Body Mass Index:</b>" & RSVitals("vital_data1")&";"
          case "O2 SAT":
            Response.Write "<b>O2 SAT:</b>" & RSVitals("vital_data1")&";"
          case "Shoe Size":
            Response.Write "Shoe Size:" & RSVitals("vital_data1")&";"
          'case "VaSC_RDist","VaSC RPH","VaSC RNear","VaCC RDist","VaCC RPH","VaCC RNear","VaSC LDist","VaSC LPH","VaSC LNear","VaCC LDist","VaCC LPH","VaCC LNear":
		'	Response.Write RSVitals("Vital_Type")& ": 20/" & RSVitals("vital_data1")
		  case "VaSC RDist","VaSC RPH","VaSC RNear","VaCC RDist","VaCC RPH","VaCC RNear","VaSC LDist","VaSC LPH","VaSC LNear","VaCC LDist","VaCC LPH","VaCC LNear":
			'Response.Write RSVitals("Vital_Type")& ": 20/" & RSVitals("vital_data1")
			if Ophal_Started <> 1 then
				set RSVitalVisual = server.CreateObject("ADODB.RecordSet")
				Set RSVitalVisual = RSVitals.Clone
				Response.Write "<table width='70%' border=0 cellspacing=0 cellpadding=0>"
				Response.Write "<tr><td colspan=3><u><b>Visual Acuity</b></u></td></tr>"

				Ophal_Type_Str = "VaSC RDist;VaSC LDist*VaSC RPH;VaSC LPH*VaSC RNear;VaSC LNear*VaCC RDist;VaCC LDist*VaCC RPH;VaCC LPH*VaCC RNear;VaCC LNear"
				'.write "<tr><td colspan=2>VaSC</td></tr>"
				Ophal_Type = split(Ophal_Type_Str,"*")
				For i = 0 to UBound(Ophal_Type)
					Response.Write "<tr><td>" & left(Ophal_Type(i),4) & ":</td>"
					Ophal_SubType = split(Ophal_Type(i),";")
					For k = 0 to UBound(Ophal_SubType)
						'RSVitalVisual.MoveFirst
						RSVitalVisual.Filter = " Vital_Type = '"& Ophal_SubType(k) &"' and take = '" & VCounter & "'"
						if not RSVitalVisual.EOF  Then
							Response.write "<td>"&right(RSVitalVisual("Vital_Type"),len(RSVitalVisual("Vital_Type"))-4)& " = 20/" & RSVitalVisual("vital_data1") & "</td>"
						end if
					Next
					Response.Write "</tr>"
				Next

				Response.Write "</table>"

				' Short circuit if Ophal info has already been proccessed.
				Ophal_Started = 1
				RSVitalVisual.Close
				Set RSVitalVisual = Nothing
			end if
		  case "Method","R Ta","R Tc","L Ta","L Tc":
			if IP_Started <> 1 then
				set RSVitalIP = server.CreateObject("ADODB.RecordSet")
				Set RSVitalIP = RSVitals.Clone

				Response.Write "<table width='70%' border=0 cellspacing=0 cellpadding=0>"
				if IP_Flag <> 1 then
					Response.Write "<tr><td colspan=3><u><b>Instaraocular Pressure</b></u></td></tr>"
				end if
								
				'RSVitalIP.MoveFirst
				RSVitalIP.Filter = " Vital_Type = 'Method' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "<td>Method="&RSVitalIP("vital_data1")&"<td></tr>"
				end if				
				
				RSVitalIP.Filter = " Vital_Type = 'R Ta' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "<tr><td>"&right(RSVitalIP("Vital_Type"),2)&":R="&RSVitalIP("vital_data1")&";&nbsp;"
				end if
				
				'RSVitalIP.MoveFirst
				RSVitalIP.Filter = " Vital_Type = 'L Ta' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "L="&RSVitalIP("vital_data1")&"<td></tr>"
				end if

				RSVitalIP.MoveFirst
				RSVitalIP.Filter = " Vital_Type = 'R Tc' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "<tr><td>"&right(RSVitalIP("Vital_Type"),2)&":</td><td>R="&RSVitalIP("vital_data1")&"<td>"
				end if
				'RSVitalIP.MoveFirst
				RSVitalIP.Filter = " Vital_Type = 'L Tc' and take = '" & VCounter & "'"
				if not RSVitalIP.EOF Then
					Response.Write "<td>L="&RSVitalIP("vital_data1")&"<td></tr>"
				end if
				Response.Write "</table>"
			end if
			IP_Started = 1
			IP_Flag = 1
          case "R MR OD","R MR OS","R MR OD 20","R MR OS 20","L MR OD","L MR OS","L MR OD 20","L MR OS 20":
			'Response.Write RSVitals("Vital_Type") & ": 20/" & RSVitals("vital_data1")
			if MR_Started <> 1 Then
				set RSVitalMR = server.CreateObject("ADODB.RecordSet")
				Set RSVitalMR = RSVitals.Clone

				Response.Write "<table width='70%' border=0 cellspacing=0 cellpadding=0>"
				if IP_Flag <> 1 then
					Response.Write "<tr><td colspan=3><u><b>Instaraocular Pressure</b></u></td></tr>"
				end if
				'RSVitalMR.MoveFirst
				RSVitalMR.Filter = " Vital_Type = 'R MR OD' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "<tr><td>"&left(RSVitalMR("Vital_Type"),4)&" ("&right(RSVitalMR("vital_type"),2)&"):</td><td>"&RSVitalMR("vital_data1")
				end if
				RSVitalMR.Filter = " Vital_Type = 'R MR OD 20' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "  "&right(RSVitalMR("Vital_Type"),2)&"/"&RSVitalMR("vital_data1")&"<td>"
				end if
				'RSVitalMR.MoveFirst
				RSVitalMR.Filter = " Vital_Type = 'R MR OS' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "<td>"&left(RSVitalMR("Vital_Type"),4)&" ("&right(RSVitalMR("vital_type"),2)&"):</td><td>"&RSVitalMR("vital_data1")
				end if
				RSVitalMR.Filter = " Vital_Type = 'R MR OS 20' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "  "&right(RSVitalMR("Vital_Type"),2)&"/"&RSVitalMR("vital_data1")&"<td></tr>"
				end if

				RSVitalMR.Filter = " Vital_Type = 'L MR OD' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "<tr><td>"&left(RSVitalMR("Vital_Type"),4)&" ("&right(RSVitalMR("vital_type"),2)&"):</td><td>"&RSVitalMR("vital_data1")
				end if
				RSVitalMR.Filter = " Vital_Type = 'L MR OD 20' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "  "&right(RSVitalMR("Vital_Type"),2)&"/"&RSVitalMR("vital_data1")&"<td>"
				end if
				'RSVitalMR.MoveFirst
				RSVitalMR.Filter = " Vital_Type = 'L MR OS' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "<td>"&left(RSVitalMR("Vital_Type"),4)&" ("&right(RSVitalMR("vital_type"),2)&"):</td><td>"&RSVitalMR("vital_data1")
				end if
				RSVitalMR.Filter = " Vital_Type = 'L MR OS 20' and take = '" & VCounter & "'"
				if not RSVitalMR.EOF Then
					Response.Write "  "&right(RSVitalMR("Vital_Type"),2)&"/"&RSVitalMR("vital_data1")&"<td></tr>"
				end if
				Response.Write "</table>"
			End if
			MR_Started = 1
			IP_Flag = 1
		  case "R P OD","R P OS","L P OD","L P OS":
			if P_Started <> 1 Then
				set RSVitalP = server.CreateObject("ADODB.RecordSet")
				Set RSVitalP = RSVitals.Clone

				Response.Write "<table width='70%' border=0 cellspacing=0 cellpadding=0>"
				if IP_Flag <> 1 then
					Response.Write "<tr><td colspan=3><u><b>Instaraocular Pressure</b></u></td></tr>"
				end if
				'RSVitalP.MoveFirst
				RSVitalP.Filter = " Vital_Type = 'R P OD' and take = '" & VCounter & "'"
				if not RSVitalP.EOF Then
					Response.Write "<tr><td>"&left(RSVitalP("Vital_Type"),4)&" ("&right(RSVitalP("vital_type"),2)&"):</td><td>"&RSVitalP("vital_data1")&"</td>"
				end if
				'RSVitalP.MoveFirst
				RSVitalP.Filter = " Vital_Type = 'R P OS' and take = '" & VCounter & "'"
				if not RSVitalP.EOF Then
					Response.Write "<td>"&left(RSVitalP("Vital_Type"),4)&" ("&right(RSVitalP("vital_type"),2)&"):</td><td>"&RSVitalP("vital_data1")& "</td></tr>"
				end if

				RSVitalP.Filter = " Vital_Type = 'L P OD' and take = '" & VCounter & "'"
				if not RSVitalP.EOF Then
					Response.Write "<tr><td>"&left(RSVitalP("Vital_Type"),4)&" ("&right(RSVitalP("vital_type"),2)&"):</td><td>"&RSVitalP("vital_data1")&"</td>"
				end if
				'RSVitalP.MoveFirst
				RSVitalP.Filter = " Vital_Type = 'L P OS' and take = '" & VCounter & "'"
				if not RSVitalP.EOF Then
					Response.Write "<td>"&left(RSVitalP("Vital_Type"),4)&" ("&right(RSVitalP("vital_type"),2)&"):</td><td>"&RSVitalP("vital_data1")&"</td></tr>"
				end if
				Response.Write "</table>"
			End if
			P_Started = 1
			IP_Flag = 1
          case else
			Response.Write RSVitals("Vital_Type")& ":" & RSVitals("vital_data1")&";&nbsp;"

        end select
        'Response.Write ";&nbsp;"        
        RSVitals.MoveNext       
      wend  
      Response.Write "</s>"          
      end if    
    IP_Flag = 0
  next 
end if

'--end of vital signs
%>

<%'-----------------Ophthalmology - start-------------------- %>
<%
'---Visual Acuity -start
set RSOpth = Server.CreateObject("ADODB.Recordset")
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='Visual Acuity' and visit_key = '"& visit_key &"' order by DPLY_ORD "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Visual Acuity:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")		
		Response.Write "<br>"		
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Visual Acuity - end
%>

<%'---external exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='EE' and visit_key = '"& visit_key &"' order by DPLY_ORD"
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>External Examination:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")
		If RSOpth("OD_TEXT")<>"" then Response.Write ";OD-"&RSOpth("OD_TEXT")
		If RSOpth("OS_TEXT")<>"" then Response.Write ";OS-"&RSOpth("OS_TEXT")
		Response.Write "<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--external exam -end
%>


<%
'---Slit-Lamp exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='SL' and visit_key = '"& visit_key &"' order by DPLY_ORD"
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Slit-Lamp Examination:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")
		If RSOpth("OD_TEXT")<>"" then Response.Write ";OD-"&RSOpth("OD_TEXT")
		If RSOpth("OS_TEXT")<>"" then Response.Write ";OS-"&RSOpth("OS_TEXT")
		Response.Write "<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Slit-Lamp exam -end
%>

<%'---Dilated Fundus -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='Dilated Fundus' and visit_key = '"& visit_key &"' order by DPLY_ORD"
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Dilated Fundus Exam:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "
		If RSOpth("OD_TEXT")<>"" then Response.Write ";OD-"&RSOpth("OD_TEXT")
		If RSOpth("OS_TEXT")<>"" then Response.Write ";OS-"&RSOpth("OS_TEXT")
		If RSOpth("EXAM_notes")<>"" then Response.Write ";"&RSOpth("EXAM_notes")
		Response.Write "<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Dilated Fundus -end
%>

<%
'---Intraocular Pressure -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='Intraocular Pressure' and visit_key = '"& visit_key &"' order by DPLY_ORD "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Intraocular Pressure:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")		
		Response.Write "<br>"		
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Intraocular Pressure - end
%>

<%'---HRT exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='HRT' and visit_key = '"& visit_key &"' "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>HRT Test:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")&" OD-"&RSOpth("OD_TEXT")&";OS- "&RSOpth("OS_TEXT")&" "&RSOpth("exam_notes")&"<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--HRT exam -end
%>

<%'---Visual Fields -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='Visual Fields' and visit_key = '"& visit_key &"' order by DPLY_ORD "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Visual Fields Test:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")
		If RSOpth("OD_TEXT") <>"" then Response.Write " OD-"&RSOpth("OD_TEXT")&";"
		If RSOpth("OS_TEXT") <>"" then Response.Write " OS- "&RSOpth("OS_TEXT")&";"
		If RSOpth("exam_notes") <>"" then Response.Write RSOpth("exam_notes")
		Response.Write "<br>"		
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Visual Fields -end
%>

<%'---Gonioscopic exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='GE' and visit_key = '"& visit_key &"' "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>Gonioscopic Examination:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_DEFAULT")&" OD-"&RSOpth("OD_TEXT")&";OS- "&RSOpth("OS_TEXT")&" "&RSOpth("exam_notes")&"<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--Gonioscopic exam -end
%>

<%'---vsp exam -start
sqlQuery = "SELECT * FROM OPTH_EXAMS where exam_name='VSP' and visit_key = '"& visit_key &"' "
RSOpth.Open sqlQuery, CONN, 3, 4
If not (RSOpth.eof or RSOpth.bof) then
%>
	<u><b>VSP Examination:</b></u><br>
<%
	while not RSOpth.EOF
		Response.Write RSOpth("EXAM_TYPE")&": "&RSOpth("EXAM_NOTES")&"<br>"
	RSOpth.MoveNext
	wend
end if
RSOpth.close
'--vsp exam -end
%>

<%'-----------------Ophthalmology - End ------------------------------ %>


<%
RSNoteData.Filter = "template_type = '1'"

if RSNoteData.RecordCount > 0 then
%>
<p>
<b><u>Exams:</u></b>
<%
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<P>" & fix_text(RSNoteData("sentence_text"))
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text")) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
  end if
  RSNoteData.MoveNext
wend
end if

%>
</div>

<p>
<div style="page-break-before: auto">

<%
RSNoteData.Filter = "template_type = '18'"

if RSNoteData.RecordCount > 0 then
%>
<p>
<b><u>Assessment:</u></b>
<%
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<P>" & fix_text(RSNoteData("sentence_text"))
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text")) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
  end if
  RSNoteData.MoveNext
wend
end if

RSNoteData.Filter = "template_type = '5'"

if RSNoteData.RecordCount > 0 then
%>
<p>
<b><u>Lab/Test Reports:</u></b>
<%
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<P>" & fix_text(RSNoteData("sentence_text"))
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text")) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
  end if
  RSNoteData.MoveNext
wend
end if

RSNoteData.Filter = "template_type = '6'"

if RSNoteData.RecordCount > 0 then
%>
<p>
<b><u>Procedure Reports:</u></b>
<%
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<P>" & fix_text(RSNoteData("sentence_text"))
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text")) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
  end if
  RSNoteData.MoveNext
wend
end if
%>


</div>
<p>
<div style="page-break-before: auto">
<%

set RSMeds = Server.CreateObject("ADODB.Recordset")
RSMeds.CursorLocation = 3
sqlQuery = "select * from notes_medications where patient_id='" & patient_id & "' and visit_key='" & visit_key & "' and iscurrent=0"
RSMeds.Open sqlQuery, CONN, 3, 4

if RSMeds.RecordCount > 0 then
%>
<p>
<b><u>New Medications:</u></b>
<p>
<%

while not RSMeds.EOF
%>
<b><%=RSMeds("drug_name")%></b> - <%=RSMeds("formulation")%><br>
<%
  RSMeds.MoveNext
wend

end if

RSNoteData.Filter = "template_type = '4'"

if RSNoteData.RecordCount > 0 then
%>
<p>
<b><u>Plan of Care:</u></b>
<%
RSNoteData.MoveFirst
while not RSNoteData.EOF
  if RSNoteData("custom_note") = "1" then ' free text
    Response.Write "<P>" & fix_text(RSNoteData("sentence_text"))
  else ' not free text
    if RSNoteData("description_text") <> "" and RSNoteData("description_text") <> vbCrLf then
      Response.Write "<P><B>" & substitute_text(RSNoteData("description_text")) & "</B>"
    else
      Response.Write "" ' Clear
    end if
    Response.Write "<div style='display: inline;' id='id_" & RSNoteData("template_id") & "'>" & fix_text(RSNoteData("sentence_text")) & "</div>"
  end if
  RSNoteData.MoveNext
wend
end if
%>
</div>

<%
set RSImm = Server.CreateObject("ADODB.Recordset")
RSImm.CursorLocation = 3
sqlQuery = "SELECT * FROM notes_immunization WHERE visit_key='" & visit_key & "' order by Imm_type"
RSImm.Open sqlQuery, CONN, 3, 4
VCount = RSImm.RecordCount

if VCount > 0 then

%>
<p>
<b><u>Immunizations:</u></b>
<p>
<span class="smt">
<%

  while not RSImm.EOF

    Response.Write "<b>" & RSImm("date_administered") & "</b> - " & RSImm("imm_type") & "<br>"
    RSImm.MoveNext
  wend
%>
</span>
<p>
<%
end if
RSImm.Close

%>
<p>

<%if request("dp") <> "0" then %>

<div class="titleBox"><u>CODING</u></div>
<%
	Dim res
	Set res = server.CreateObject("ADODB.RecordSet")

	'res.open "select a.assess as asses,a.icdcode as code from the (select ass from assessplan where visit_key='"&visit_key&"')a",CONN
	res.open "select b.icdname as asses,b.icdcode as code from assessplan_new a,icdtable b where a.icdcode = b.icdcode and a.visit_key='"&visit_key&"'",CONN
	if not res.eof then
%>
<p>
<b><u>Diagnosis Codes:</u></b>
<p>
<% while not res.eof %>

<b><%=res("code")%></b> - <span class="st"><%=res("asses")%></span><br>

<%
		res.movenext
		wend
end if
res.Close

res.Open "select * from assessprocordered where visitkey='"&visit_key&"'",con
if not res.EOF then
%>
<p>
<b><u>Procedure Codes:</u></b>
<p>
<%
			Dim res1
			set res1 = server.CreateObject("ADODB.RecordSet")
			while not res.EOF
			if ucase(res("code_type")) <> ucase("hcpcs") then
				res1.Open "select procname from cpt_list where proccode='"&res("cpt_code")&"'",con
			else
				res1.Open "select hcpsname as procname from hcpstable where hcpscode='"&res("cpt_code")&"'",con
			end if

			'if left(ucase(res1("procname")),6) <> "OFFICE" then
%>

<b><%=res("cpt_code")%></b> - <span class="st"><%=ucase(res1("procname"))%></span><br>
		<%
			'end if
			res.MoveNext
			res1.Close
			wend
end if
res.Close
%>
<%end if%>

</td>
</tr>

<%

if request("meds") <> "0" then

	set res1 = server.CreateObject("ADODB.Recordset")

	'sqlQuery = "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and patient_id='" & patient_id & "' order by status,drug_name"
	sqlQuery = "select a.*, b.route_description from notes_medications a, multum_route b where a.route=b.route_abbr(+) and patient_id='" & patient_id & "' and a.prescribed_date <= (select visit_date from visit where visit_key = '"&visit_key&"') order by status,drug_name"

	res1.open sqlQuery,con
	if not res1.eof then

	%>
	<p>
	<div class="titleBox">MEDICAL HISTORY</div>
	<table border="0" cellspacing="1" cellpadding="3" width="100%" bgcolor="#CCCCCC" width="100%">
	  <tr bgcolor="#3366CC">
	    <td><font color="white"><b>Date</td>
	    <td><font color="white"><b>Prescription</td>
	    <td><font color="white"><b>Notes</td>
	    <td><font color="white"><b>Status</td>
	  </tr>
	<%	cnt = 1
		while not res1.eof

			if res1("visit_key") = visit_key then
				bgcolor = "#FFFFCC"
			else
				if cnt mod 2 = 0 then
					bgcolor = "#FFFFFF"
				else
					bgcolor = "#EFEFEF"
				end if
			end if
	%>
		<tr bgcolor="<%=bgcolor%>">
	        <td valign="top"><b><%=res1("prescribed_date")%></td>
	        <td valign="top">
	          <b><%=res1("drug_name")%></b> -
	          <%=res1("formulation")%><br>
		      <b>Dose:</b> <%=res1("dose")%><br>
		      <b>Route:</b> <% if res1("route_description") <> "" then Response.write PCase(res1("route_description")) else Response.Write PCase(res1("route")) end if %><br>
		      <% if trim(res1("dispense").value) <> "" then %>
		      <b>Dispense:</b> <%=res1("dispense")%><br>
		      <% end if %>
		      <b>Refill:</b> <%=res1("refill")%>
		      <% if res1("frequency") <> "" then %>
		      <br><b>Frequency:</b> <%=res1("frequency")%>
		      <% end if %>
		    </td>
		    <td class="st" valign="top"><% if res1("notes") <> "" then Response.Write SQLFixUp(res1("notes")) else Response.Write "None" end if %></td>
		    <td valign="top"><b><%=res1("status")%></td>
		</tr>
	<%
			cnt = cnt + 1
			res1.movenext
		wend
		res1.Close
	%>

	</table>

<%
end if
end if
%>

<%
if RSNoteAmend.RecordCount > 0 then
RSNoteAmend.MoveFirst
%><tr><td>
	<b><u>Amendment:</u></b>
   <td></tr>
<%
While not RSNoteAmend.EOF
Response.write "<tr><td>"
Response.Write RSNoteAmend("notes_amend")
Response.Write RSNoteAmend("doc_id")&" "&CalculateLocalTime(RSNoteAmend("date_amend"))&"("&session("TimeZone")&")"&"<br>"
Response.Write "<td></tr>"
RSNoteAmend.MoveNext
wend
end if
%>


<%If ref_print="1" then
	if patient_gender="Male" then
		his_her= " his "
	elseif patient_gender="Female"	then
		his_her= " her "
	else
		his_her= " his\her "
	end if
%>


<tr><td align="left">
<br><br>I appreciate <%=his_her%> referral for consultation. Please call with any questions.<br><br>
</td></tr>
<%end if%>


<tr><td align="left">

<% if request("sig") <> "0" then %>
	<% session("authentication")=1 
		if session("role") ="nurse" then doctor_id = doctor_id
	%>
	<img src="/practice/customization/signature/display_signature.asp?doctor_id=<%=doctor_id%>">
</td></tr>
<tr><td align="left">
	 <%=DOCTOR_NAME%>
<% end if %>

</td></tr>
</table>


<!--To print the header on each page. Do not change this -->
</td>
</tr> 
</tbody> 
</table>
<!--Do not change this -->


</body>
</html>
<%
else
%>
<h3>General Error: Visit key missing.</h3>
<%
end if
%>