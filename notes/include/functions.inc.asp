<%
Function GetVisitInfo(Visit_Key,Data_Type)
	Dim RS
	Set RS = Server.CreateObject("ADODB.RecordSet")	
	
	RS.CursorLocation = 3
	RS.Open "select * from Notes_Vitals where VISIT_KEY = '"&Visit_Key&"' order by DATESTAMP",CONN
	Select Case Data_Type
	Case 1:
		RS.Filter = " vital_type = 'W' "
	Case 2:
		RS.Filter = " vital_type = 'W' "
	Case 3:
		RS.Filter = " vital_type = 'H' "
	Case 4:
		RS.Filter = " vital_type = 'H' "
	Case 5:
		RS.Filter = " vital_type = 'HC' "
	Case 6:
		RS.Filter = " vital_type = 'W' or vital_type = 'H' "
	End Select
	
	Set GetVisitInfo = RS
	
	'RS.Close 
End Function

Function GetPatientBDate(Visit_Key)
	Dim RS
	Set RS = Server.CreateObject("ADODB.RecordSet")
	
	RS.CursorLocation = 3
	RS.Open "select a.BIRTH_DATE from PATIENTTABLE a, visit b where a.userid = b.patient_id and b.VISIT_KEY = '"&Visit_Key&"'",CONN
	if not RS.eof then
		Birth_Date = RS("Birth_Date")
	End if
	
	GetPatientBDate = Birth_Date
End Function

Function GetPatientSex(Visit_Key)
	Dim RS
	Set RS = Server.CreateObject("ADODB.RecordSet")
	
	RS.CursorLocation = 3
	RS.Open "select a.GENDER from PATIENTTABLE a, visit b where a.userid = b.patient_id and b.VISIT_KEY = '"&Visit_Key&"'",CONN
	if not RS.eof then
		if ucase(RS("GENDER")) = "FEMALE" then
			Sex = "2"
		elseif ucase(RS("GENDER")) = "MALE" then
			Sex = "1"
		end if
	End if
	
	GetPatientSex = Sex
End Function 

Function GetPatientData(Visit_Key)
	Dim RS
	Set RS = Server.CreateObject("ADODB.RecordSet")
	
	RS.CursorLocation = 3
	RS.Open "select a.*,b.visit_date,b.visit_time from PATIENTTABLE a, visit b where a.userid = b.patient_id and b.VISIT_KEY = '"&Visit_Key&"'",CONN
	
	set GetPatientData = RS
End Function

Function GetDoctorData(Visit_Key)
	Dim RS
	Set RS = Server.CreateObject("ADODB.RecordSet")
	
	RS.CursorLocation = 3
	RS.Open "select a.* from DOCTORTABLE a, visit b where a.DOCUSER = b.doctor_id and b.VISIT_KEY = '"&Visit_Key&"'",CONN
	
	set GetDoctorData = RS
End Function

Function CalculatePencentile(Measurement,Data_Type,Sex,Age)
	Dim RS
	Set RS = Server.CreateObject("ADODB.RecordSet")
	Dim MyPercentile
	
	MyPercentile = 0
	Age = cdbl(Age)+.5
	RS.CursorLocation = 3
	'Response.Write "select * from pediatric_gc_data where data_type = '"&Data_Type&"' and Sex = '"&Sex&"' and months='"&Age&"'"
	RS.Open "select * from pediatric_gc_data where data_type = '"&Data_Type&"' and Sex = '"&Sex&"' and months='"&Age&"'",CONN
	If not RS.eof then
		'Response.Write RS("P97")&"<BR>"
		E = cdbl((1/cdbl(RS("L"))))
		Reference = cdbl(RS("M"))*cdbl((1+cdbl(RS("L"))*cdbl(RS("S"))*cdbl(1.881))) ^ E
		'Response.Write reference&"<BR>"
		MyPercentile = cdbl(measurement) * 97 / cdbl(reference)
	End if
	'Response.write MyPercentile
	CalculatePencentile = cdbl(MyPercentile)
	RS.Close
End Function

Function CalculateBMI(Weight,Height)
	Dim BMI
	
	BMI = (cdbl(Weight)/cdbl(Height)/cdbl(Height))*10000
	CalculateBMI = BMI
	
End Function

Function KgToLbs(Value)
	Value = cint(Value) * .4536
	KgToLbs = Value
End Function

Function GetAllVisitKeys(Visit_Key)
	Dim RS
	Set RS = Server.CreateObject("ADODB.RecordSet")
	
	RS.CursorLocation = 3
	RS.Open "Select Visit_Key from Visit where Patient_ID = " &_
	" (Select Patient_ID from Visit where Visit_Key = '"&Visit_Key&"') ",CONN
	
	set GetAllVisitKeys = RS
End Function
%>
