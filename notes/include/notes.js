<!--

var eArray;
var verbosity = 1;

function entityInArray(item, array) {
  if (array.length && item) {
    for (var i = 0; i < array.length; i++) {
      if (array[i][1] == item[1]) {
        return true;
      } 
    }
  }
  return false;
}

function buildEntityArray(frm) {
  var entityArray = new Array();
  for (var i = 0; i < frm.elements.length; i++) {
    if (frm.elements[i].getAttribute('alt')) {
      var entityType = frm.elements[i].getAttribute('alt');
      var entityObj = frm.elements[i];
      var params = entityType.split("|");
      var re = /^\d$/;
      if (params[0].match(re)) {
        // form object is an entity, add to entity array
        if (!entityInArray(params, entityArray)) {
          entityArray.push(params);
        }
      } 
    }
  }
  // if (verbosity > 2) alert("Found " + entityArray.length + " entities.");
  return entityArray;
}

function getEntityAttributes(formObject) {
  if (formObject.getAttribute('alt')) {
    var entityType = formObject.getAttribute('alt');
    var entityAttributes = entityType.split("|");
    var re = /^\d$/;
    if (entityAttributes[0].match(re)) {
      return entityAttributes;
    } else {
      printError(that);
      return null;
    }
  } else {
    printError(that);
    return null;
  }
}

function printError(that) {
  if (that.getAttribute('name')) {
    alert("Object '" + that.getAttribute('name') + "' missing attributes.");
  } else {
    alert("Unknown object missing attributes.");
  }
}

function isExclusive(entityObject) {
  if (entityObject && entityObject[0] == 3) {
    var entityExclusive = Number(entityObject[4]).valueOf();
    if (entityExclusive == 1) {
      return true;
    } else {
      return false;
    }
  } else {
      return false;
  }
}

function isTextBox(entityObject) {
  if (entityObject && (entityObject[0] == 1 || entityObject[0] == 4)) {
    return true;
  } else {
    return false;
  }
}

function isRadioTextBox(entityObject) {
  if (entityObject && entityObject[0] == 4) {
    return true;
  } else {
    return false;
  }
}

function haveChild(entityObject) {
  if (entityObject && entityObject[5] == 1) {
    return true;
  } else {
    return false;
  }
}


function isRadioTypeThree(entityObject) {
  if (entityObject && entityObject[0] == 2) {
    var radioPos = Number(entityObject[4]).valueOf();
    if (radioPos == 3) 
      return true;
    else
      return false;
  } else {
    return false;
  }
}

function unSelectTextBox(formObject, entityObject) {

  if (verbosity > 3) alert("Unselecting TB " + entityObject[1]);

  var tbId = "tb" + entityObject[1];
  var cbId = "cb" + entityObject[1];
  
  document.getElementById(tbId).value = "";
  document.getElementById(cbId).checked = false;
  
}

function unSelectRadio(formObject, entityObject) {

  if (entityObject[4] == '1') {
    if (verbosity > 3) alert("Unselecting R1 " + entityObject[1]);
    var cbId = "cb" + entityObject[1];
    eval("document.getElementById('" + cbId + "').checked = false");
  }
  
  if (entityObject[4] == '2') {
    if (verbosity > 3) alert("Unselecting R2 " + entityObject[1]);
    var cbId = "cb" + entityObject[1];
    eval("document.getElementById('" + cbId + "').checked = false");
  }

  if (entityObject[4] == '3') {
    if (verbosity > 3) alert("Unselecting R3 " + entityObject[1]);
    var cbIdPositive = "cb" + entityObject[1] + "p";
    var cbIdNegative = "cb" + entityObject[1] + "n";
    eval("document.getElementById('" + cbIdPositive + "').checked = false");
    eval("document.getElementById('" + cbIdNegative + "').checked = false");
  }
  
}

function unSelectCheckBox(formObject, entityObject) {

  if (verbosity > 3) alert("Unselecting CB " + entityObject[1]);

  var cbId = "cb" + entityObject[1];
  document.getElementById(cbId).checked = false;
}

function unSelectRadioTextBox(formObject, entityObject) {

  if (verbosity > 3) alert("Unselecting RTB " + entityObject[1]);

  var tbId = "tb" + entityObject[1];
  var cbId = "cb" + entityObject[1];
  
  document.getElementById(tbId).value = "";
  document.getElementById(cbId).checked = false;

}



function selectTextBox(formObject, entityObject) {

  if (verbosity > 3) alert("Selecting TB " + entityObject[1]);

  var tbId = "tb" + entityObject[1];
  var cbId = "cb" + entityObject[1];
  
  document.getElementById(cbId).checked = true;
  document.getElementById(tbId).focus();

}

function selectRadio(formObject, entityObject) {

  if (entityObject[4] == '1') {
    if (verbosity > 3) alert("Selecting R1 " + entityObject[1]);
    var cbId = "cb" + entityObject[1];
    document.getElementById(cbId).checked = true;
  }
  
  if (entityObject[4] == '2') {
    if (verbosity > 3) alert("Selecting R2 " + entityObject[1]);
    var cbId = "cb" + entityObject[1];
    document.getElementById(cbId).checked = true;
  }

  if (entityObject[4] == '3') {
    if (verbosity > 3) alert("Selecting R3 " + entityObject[1]);
    var cbIdPositive = "cb" + entityObject[1] + "p";
    var cbIdNegative = "cb" + entityObject[1] + "n";
    
    if (formObject.getAttribute('name') == cbIdPositive)
      document.getElementById(cbIdNegative).checked = false;
    else if (formObject.getAttribute('name') == cbIdNegative)
      document.getElementById(cbIdPositive).checked = false;
   
  }
  
}

function selectCheckBox(formObject, entityObject) {

  if (verbosity > 3) alert("Selecting CB " + entityObject[1]);

  var cbId = "cb" + entityObject[1];
  document.getElementById(cbId).checked = true;
}

function selectRadioTextBox(formObject, entityObject) {

  if (verbosity > 3) alert("Selecting RTB " + entityObject[1]);

  var tbId = "tb" + entityObject[1];
  var cbId = "cb" + entityObject[1];


  document.getElementById(tbId).disabled = false;
  document.getElementById(tbId).focus();
  
}

function unSelectEntity(formObject, entityObject) {

  switch (entityObject[0]) {
  
    case '1': unSelectTextBox(formObject, entityObject); break;
    case '2': unSelectRadio(formObject, entityObject); break;
    case '3': unSelectCheckBox(formObject, entityObject); break;
    case '4': unSelectRadioTextBox(formObject, entityObject); break;
  
  }

}

function selectEntity(formObject, entityObject) {

  switch (entityObject[0]) {
  
    case '1': selectTextBox(formObject, entityObject); break;
    case '2': selectRadio(formObject, entityObject); break;
    case '3': selectCheckBox(formObject, entityObject); break;
    case '4': selectRadioTextBox(formObject, entityObject); break;
  
  }

}


function unSelectAll(formObject, entityObject, entityArray) {
  if (verbosity > 3) alert("Unselecting everything in group " + entityObject[2]);

  for (var i = 0; i < entityArray.length; i++) {
    // if entity is in object's group, deselect it
    if (entityArray[i][2] == entityObject[2]) {
      unSelectEntity(formObject, entityArray[i]);
    }
  }
}

function unSelectExclusive(formObject, entityObject, entityArray) {
  if (verbosity > 3) alert("Unselecting exclusives in group " + entityObject[2]);

  for (var i = 0; i < entityArray.length; i++) {
    // if entity is in object's group, deselect it
    if (isExclusive(entityArray[i])) {
      unSelectEntity(formObject, entityArray[i]);
    }
  }
}

function focusTextBox(that) {
  if (getEntityAttributes(that)) {
    var entityObject = getEntityAttributes(that);
    var cbId = "cb" + entityObject[1];
    var tbId = "tb" + entityObject[1];

    document.getElementById(cbId).checked = true;

  }
}

function blurTextBox(that) {
  if (getEntityAttributes(that)) {
    var entityObject = getEntityAttributes(that);
    var cbId = "cb" + entityObject[1];
    var tbId = "tb" + entityObject[1];

    if (document.getElementById(tbId).value == "") {
      document.getElementById(cbId).checked = false;
      if (isRadioTextBox(entityObject)) {
        document.getElementById(tbId).disabled = true;
      }
    }

  }
}

function goToChild(entityObject) {

  var visit_key = document.getElementById('template').visit_key.value;
  var template_type = document.getElementById('template').template_type.value;
  var template_id = entityObject[3];
  var parent_id = entityObject[1];

  var child_url = '/practice/notes/add/template.asp' + 
                  '?visit_key=' + visit_key + 
                  '&template_type=' + template_type +
                  '&template_id=' + template_id +
                  '&parent_id=' + parent_id;

  document.location.replace(child_url);
  return true;
}

function updateNotes(formObject, entityObject) {

  var entity_type = entityObject[0];
  var entity_id = entityObject[1];
  var group_id = entityObject[2];
  var template_id = entityObject[3];
  var entity_type_modifier = entityObject[4];
  var delete = 0;
  var value = "";
  
  if (group_id == null) group_id = "";
    
  if (entity_type_modifier == null) entity_type_modifier = "";

  if (entity_type == "3") {
    status_field = "cb" + entity_id;
    if (eval("document.getElementById('template')." + status_field + ".checked") == true) {
      value = "1";
    } else {
      delete = 1;
    }
  } else {
    if (value == "") delete = 1;
  }
  
  var update_url = '/practice/notes/edit/update.asp' + 
                     '?visit_key=' + visit_key + 
                     '&template_type=' + template_type +
                     '&template_id=' + template_id +
                     '&entity_type=' + entity_type +
                     '&entity_type_modifier=' + entity_type_modifier +
                     '&entity_id=' + entity_id +
                     '&group_id=' + group_id +
                     '&value=' + value 
                     '&delete=' + delete;

  //alert(update_url);
  parent.frames.updateNoteFrame.location.replace(update_url);
  //parent.frames.updateNoteFrame.reload(true);
  return true;
}

function updateForm(that) {

  if (getEntityAttributes(that)) {
  
    var entityObject = getEntityAttributes(that);
    
    // exclusive, unSelect all
    if (isExclusive(entityObject)) {
      unSelectAll(that, entityObject, eArray);
      selectEntity(that, entityObject);
    } else {
      unSelectExclusive(that, entityObject, eArray);
      if (isTextBox(entityObject) || isRadioTypeThree(entityObject)) {
        selectEntity(that, entityObject);
      }
    }

  } else {

    printError(that);

  }
  
  if (haveChild(entityObject)) {
    goToChild(entityObject);
  }
  
  updateNotes(that, entityObject);

}

//-->