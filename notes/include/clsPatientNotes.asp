<%

class clsPatientNotes

  private PatientName
  private PatientGender
  private PatientVisitKey
  private PatientID
  private Visit_Date
  private VisitClosed
  private Birth_Date
  private ReferPhysicianName
  private ReferPhysicianPhone
  private ReferPhysicianUPIN
  private PostOp_Global
  private Nurse_Complete
  private cmd
  private ConnObj
  private Surg_Complete
  private Anes_Complete 

  public property get Name
    Name = PatientName
  end property

  public property get VisitKey
    VisitKey = PatientVisitKey
  end property

  public property get VisitDate
	VisitDate = Visit_Date
  end property

  public property get Age

	dim tyear, tmonth,ageStr

	if isDate(birth_date) then

		tyear = Year(date()) - Year(birth_date)

		if Month(date()) < Month(birth_date) then
			tyear = tyear - 1
		end if

		tmonth = Month(date()) - Month(birth_date)

		if tmonth < 0 then
			tmonth = tmonth + 12
		end if

		if tyear > 0 then
			ageStr = tyear & "Y"
		end if

		if tyear > 0 and tmonth > 0 then
			ageStr = ageStr & "/" & tmonth & "M"
		elseif tmonth > 0 then
			ageStr = tmonth & " month(s)"
		end if


	else

		ageStr = "Unknown"

	end if

	Age = ageStr

  end property

  public property get BirthDate
    if not isDate(Birth_Date) then
      BirthDate = "Unknown"
    else
	  BirthDate = Birth_Date
	end if
  end property


  public property get ID
    ID = PatientID
  end property

  public property get Closed
    if VisitClosed = "1" then
      Closed = true
    else
      Closed = false
    end if
  end property

  public property get PostOp
	if PostOp_Global = "1" then
		PostOp = true
	else
		PostOp = false
	end if
  end property

  public property get NurseComplete
	if Nurse_Complete = "1" then
		NurseComplete = true
	else
		NurseComplete = false
	end if
  end property

 public property get SurgComplete
  	if Surg_Complete = "1" then
  		SurgComplete = true
  	else
  		SurgComplete = false
  	end if
 end property
  
 public property get AnesComplete
  	if Anes_Complete = "1" then
  		AnesComplete = true
  	else
  		AnesComplete = false
  	end if
 end property

  public property get Gender
    if ucase(left(PatientGender,1)) = "M" then
      Gender = "M"
    elseif ucase(left(PatientGender,1)) = "F" then
      Gender = "F"
    else
      Gender = "U"
    end if
  end property
   

  public property get ReferName
	ReferName = ReferPhysicianName
  end property

  public property get ReferPhone
	ReferPhone = ReferPhysicianPhone
  end property

  public property get ReferUPIN
	ReferUPIN = ReferPhysicianUPIN
  end property

  private sub Class_Initialize()

    if isObject(CONN) then
      ConnObj = CONN
    else
      CreateConnection
    end if

  end sub

  private sub CreateConnection()

    dim ConnStr
    ConnStr = "DRIVER={Microsoft ODBC for Oracle};UID=MEDICAL;PWD=cyb3rd0c;SERVER=mdm3;"
    'connStr = "Provider=OraOLEDB.Oracle;" & _
    '       "Data Source=mdm3;" & _
    '       "User Id=medical;" & _
    '       "Password=cyb3rd0c"
    set connObj = Server.CreateObject("ADODB.Connection")
    ConnObj.Open ConnStr
    connobj.CursorLocation = 3

  end sub

  private sub DestroyConnection()
	set ConnObj = nothing
  end sub

  public sub SetVisitKey(visit_key)
    PatientVisitKey = visit_key
    InitializeData
  end sub

  public sub InitializeData()

    if VisitKey <> "" then

      dim RS

      CreateConnection

      set RS = Server.CreateObject("ADODB.Recordset")

      RS.CursorLocation = 3
      sqlQuery = "SELECT A.userid, A.birth_date, A.first_name, A.last_name, A.middle_name, A.gender, A.refer_physician_name, B.closed, B.visit_date, b.global, b.nurse_complete,b.SURG_COMPLETE, b.ANES_COMPLETE FROM patienttable A, visit B WHERE A.userid = B.patient_id AND B.visit_key = '" & PatientVisitKey & "'"
      RS.Open sqlQuery, connObj

      if RS.RecordCount > 0 then
        PatientName = RS("first_name").Value & " " & RS("middle_name").Value & " " & RS("last_name").Value
        PatientGender = RS("gender").Value
        PatientID = RS("userid").Value
        VisitClosed = RS("closed").Value
        Visit_Date = RS("visit_date").Value
        Birth_Date = RS("birth_date").Value
        ReferPhysicianName = RS("Refer_Physician_Name").Value
        PostOp_Global = RS("global").Value
        Nurse_Complete = RS("nurse_complete").Value   
        Surg_Complete = RS("surg_complete").Value
        Anes_Complete = RS("anes_complete").Value      
      else
		'Response.Write sqlQuery & "<BR>"
        Response.Write "PatientNotes: Could not find visit key."
        Response.End
      end if

      RS.Close

      RS.Open "Select UPIN, Phone from Referring_Physicians where Record_Owner = '"& Session("pid") &"' and lastname||','||firstname='"& ReferPhysicianName &"'",connObj
      if not RS.EOF then
		ReferPhysicianPhone = RS("Phone").Value
		ReferPhysicianUPIN = RS("UPIN").Value
      end if
      RS.Close

      set RS = nothing

    end if

  end sub

  public sub SetOpen

    if VisitKey <> "" then

      sqlQuery = "UPDATE visit SET closed = 0 WHERE visit_key ='" & visit_key & "'"
      connObj.Execute(sqlQuery)

      ' add logging hook here

    end if

  end sub

  public sub SetClosed

    if VisitKey <> "" then

      sqlQuery = "UPDATE visit SET closed = 1,  WHO_CLOSED='"&session("user")&"' WHERE visit_key ='" & visit_key & "'"
      connObj.Execute(sqlQuery)

    end if

  end sub

  public sub MarkPostOp
	if VisitKey <> "" then
		sqlQuery = "UPDATE visit SET global=1 WHERE visit_key ='" & visit_key & "'"
		connObj.Execute(sqlQuery)
		'set cmd = Server.CreateObject("ADODB.Command")
		'cmd.ActiveConnection = connObj
		'connobj.BeginTrans
		'cmd.CommandText = sqlQuery
		'cmd.Execute
		'connobj.CommitTrans
	end if
  end sub

  public sub UnmarkPostOp
	if VisitKey <> "" then
		sqlQuery = "UPDATE visit SET global=0 WHERE visit_key ='" & visit_key & "'"
		connObj.Execute(sqlQuery)
		'set cmd = Server.CreateObject("ADODB.Command")
		'cmd.ActiveConnection = connObj
		'connobj.BeginTrans
		'cmd.CommandText = sqlQuery
		'cmd.Execute
		'connobj.CommitTrans
	end if
  end sub

  public sub MarkNurseComplete
	if VisitKey <> "" then
		sqlQuery = "Update visit set Nurse_Complete=1 WHERE visit_key = '"&visit_key&"'"
		connObj.Execute(sqlQuery)
	end if
  end sub

  public sub UnmarkNurseComplete
		sqlQuery = "Update visit set Nurse_Complete=0 WHERE visit_key = '"&visit_key&"'"
		connObj.Execute(SqlQuery)
  end sub

   public sub MarkSurgeonComplete
	if VisitKey <> "" then
		sqlQuery = "Update visit set SURG_COMPLETE=1 WHERE visit_key = '"&visit_key&"'"
		connObj.Execute(sqlQuery)
	end if
  end sub
  
  public sub UnmarkSurgeonComplete
		sqlQuery = "Update visit set SURG_COMPLETE=0 WHERE visit_key = '"&visit_key&"'"
		connObj.Execute(SqlQuery)
  end sub
 
  public sub MarkANESComplete
	if VisitKey <> "" then
		sqlQuery = "Update visit set ANES_COMPLETE=1 WHERE visit_key = '"&visit_key&"'"
		connObj.Execute(sqlQuery)
	end if
  end sub
  
  public sub UnmarkANESComplete
		sqlQuery = "Update visit set ANES_COMPLETE=0 WHERE visit_key = '"&visit_key&"'"
		connObj.Execute(SqlQuery)
  end sub

  public function SectionToTemplateType(section)

    dim template_type

    select case section
      case "Exam"
        template_type = 1
      case "HPI"
        template_type = 2
      case "ROS"
        template_type = 3
      case "Plan"
        template_type = 4
      case "Reports"
        template_type = 5
      case "Procedure"
        template_type = 6
      case "PMH"
        template_type = 7
      case "Orders"
        template_type = 8
      case "Pain"
        template_type = 9
      case "Habits"
        template_type = 10
      case "Surgical"
        template_type = 12
      case "FH"
        template_type = 13
      case "PSH"
        template_type = 14
      case "CD"
        template_type = 15
      case "Mental"
        template_type = 16
      case "Diagnoses"
        template_type = 17
      case "Assessment"
		template_type = 18
	  case "Other"
		template_type = 19
	  case "WC"
		template_type = 20

    end select

    SectionToTemplateType = template_type

  end function

public function TemplateTypeToSection(template_type)
  
    dim section 
    
    select case template_type
      case "1" 
        section = "Exam"
      case "2" 
        section = "HPI"
      case "3" 
        section = "ROS"
      case "4" 
        section = "Plan"
      case "5" 
        section = "Reports"
      case "6" 
        section = "Procedure"
      case "7" 
        section ="PMH"
      case "8"  
        section = "Orders"
      case "9"
        section = "Pain"
      case "10"
        section = "Habits"
      case "12"
        section = "Surgical"
      case  "13"
        section =  "FH"
      case "14" 
        section = "PSH"
      case "15"
        section =  "CD"
      case "16" 
        section = "Mental"
      case "17"
        section =  "Diagnoses"
      case "18"
		section = "Assessment" 
	  case "19"
		section =  "Other"
	  case "20"
		section = "WC"

    end select
  
    TemplateTypeToSection = section
  
  end function

  public function getAgeFrom(from_date)

	dim tyear, tmonth,ageStr

	if isDate(from_date) and isDate(birth_date) then

		tyear = Year(from_date) - Year(birth_date)

		if Month(from_date) < Month(birth_date) then
			tyear = tyear - 1
		end if

		tmonth = Month(from_date) - Month(birth_date)

		if tmonth < 0 then
			tmonth = tmonth + 12
		end if

		if tyear > 0 then
			ageStr = tyear & "Y"
		end if

		if tyear > 0 and tmonth > 0 then
			ageStr = ageStr & "/" & tmonth & "M"
		elseif tmonth > 0 then
			ageStr = tmonth & " month(s)"
		end if


	else

		ageStr = "Unknown"

	end if

	Age = ageStr

  end function


  private sub Class_Terminate()
    DestroyConnection
  end sub

end class

%>