<%

Function InArray(arrayName, strValue)
	Dim i
	For i = 0 to UBound(arrayName)
		If arrayName(i) = cstr(strValue) Then
			InArray = TRUE
			Exit Function
		End If
	Next
	InArray = FALSE 
End Function

function getRoot(URL)
	dim URLP, URLT
	
	URLP = split(URL,"/")
	
	if isarray(URLP) then
		for i = 0 to ubound(URLP)
			if len(URLP(i)) > 0 and not InStr(URLP(i),"?") and not InStr(URLP(i),"&") then
				URLT = URLT & "/" & URLP(i)
			end if
		next
	else
		URLT = ""
	end if
	
	getRoot = URLT
	
end function

function getScript(URL)
	dim URLP, URLT
	
	URLP = split(URL,"/")
	
	if isarray(URLP) then
		for i = 0 to ubound(URLP)
			if len(URLP(i)) > 0 and InStr(URLP(i),"?") or Instr(URLP(i),"&") then
				URLT = URLT & "/" & URLP(i)
			end if
		next
	else
		URLT = ""
	end if
	
	getScript = URLT
	
end function

function ceiling(byVal x)
  if isNumeric(x) then
    if x > int(x) then x = int(x) + 1
  end if
  ceiling = x
end function

function getPatientName(ByVal userid, forder, fcase)

  dim RS, patname
  
  set RS = server.CreateObject("ADODB.Recordset")

  sqlQuery = "select first_name,last_name,middle_name from patienttable where userid='"&userid&"'"
  'Response.Write sqlQuery
  RS.open sqlQuery,CONN
  
  if not RS.eof then
    
    if forder = 1 then ' first middle last
	    patname = RS("first_name")
	    if (RS("middle_name") <> "") then patname = patname & " " & left(trim(RS("middle_name")),1)&"."
	    patname = patname & " "&RS("last_name")
	else ' last, first middle
        patname = RS("last_name")&", "&RS("first_name")
	    if (RS("middle_name") <> "") then patname = patname & " " & left(trim(RS("middle_name")),1)&"."
	end if
	
	
	
	select case fcase
	  case 1 ' upper case
	    patname = ucase(patname)
	  case else ' proper case
	    patname = pcase(patname)
	end select
	
  end if
  
  set RS = nothing
  
  getPatientName = patname
  
end function

function getPatientPhone(ByVal userid)

  dim RS, phone
  
  set RS = server.CreateObject("ADODB.Recordset")

  sqlQuery = "select home_phone from patienttable where userid='"&userid&"'"
  
  RS.open sqlQuery,CONN
  
  if not RS.eof then
    phone = formatPhone(RS("home_phone").value)
  else
    phone = ""
  end if
  
  set RS = nothing
  
  getPatientPhone = phone
  
end function

function getDoctorName(ByVal userid, forder, fcase)

  dim RS, patname
  
  set RS = server.CreateObject("ADODB.Recordset")

  sqlQuery = "select first_name,last_name,middle_name from doctortable where docuser='"&userid&"'"
  'Response.Write sqlQuery
  RS.open sqlQuery,CONN
  
  if not RS.eof then
    
    if forder = 1 then ' first middle last
	    patname = RS("first_name")
	    if (RS("middle_name") <> "") then patname = patname & " " & left(trim(RS("middle_name")),1)&"."
	    patname = patname & " "&RS("last_name")
	else ' last, first middle
        patname = RS("last_name")&", "&RS("first_name")
	    if (RS("middle_name") <> "") then patname = patname & " " & left(trim(RS("middle_name")),1)&"."
	end if
	
	
	
	select case fcase
	  case 1 ' upper case
	    patname = ucase(patname)
	  case else ' proper case
	    patname = pcase(patname)
	end select
	
  end if
  
  set RS = nothing
  
  getDoctorName = patname
  
end function

function formatLongDate(theDate)

  formatLongDate = monthname(month(theDate)) & " " & day(theDate) & ", " & year(theDate)

end function

function formatTimeSlot(slot)

  dim tmpSlot, slots, slot_1, slot_2, result
  
  if len(slot) >= 0 then
  
  tmpSlot = replace(slot,":00 ","")
  slots = split(tmpSlot, "-")
  
  
  slot_1 = split(slots(0),":")
  slot_2 = split(slots(1),":")
  
  if cint(slot_1(0)) < 10 then slot_1(0) = "0" & slot_1(0)
  if cint(slot_2(0)) < 10 then slot_2(0) = "0" & slot_2(0)
  
  
  
  result = "<span class='timeslot'>" & join(slot_1,":") & "-" & join(slot_2,":") & "</span>"

  end if

  
  formatTimeSlot = result

end function

function getAge(ByVal from_date,ByVal birth_date)

	dim tyear, tmonth,ageStr
	
	if isDate(from_date) and isDate(birth_date) then

		'tyear = Year(from_date) - Year(birth_date)	
		'if Month(from_date) < Month(birth_date) then
			'tyear = tyear - 1
		'end if	
		'tmonth = Month(from_date) - Month(birth_date)	
		'if tmonth < 0 then
			'tmonth = tmonth + 12
		'end if		
		'if tyear > 0 then
			'ageStr = tyear & "Y"
		'end if	
		'if tyear > 0 and tmonth > 0 then
			'ageStr = ageStr & "/" & tmonth & "M"
		'elseif tmonth > 0 then
			'ageStr = tmonth & " month"
		'end if
		
		x=DateDiff("y",birth_date,from_date)
		x_years=int(x/365.5)
		x_months=int((x-(x_years*365.5))/30.4583)
		ageStr = x_years &"Y/"& x_months &"M"		
		
		if DateDiff("y",birth_date,from_date) < 366 then
			if DateDiff("y",birth_date,from_date) < 31 then 	
				ageStr	=	DateDiff("y",birth_date,from_date)&"Days"
			else	
				'b_months	=DateDiff("m",birth_date,from_date)			
				'b_days= abs(day(birth_date)	- day(from_date))			
				'ageStr = b_months &"M/"& b_days &"D"
				
				x=DateDiff("y",birth_date,from_date)	
				b_months = int(x/30.4583)
				b_days = int(x-(b_months*30.4583))							
				ageStr = b_months &"M/"& b_days &"D"
			end if						
		end if
    else
	
		ageStr = "Unknown"
	
	end if
	
	getAge = ageStr

end function

function formatPlainTimeSlot(slot)

  dim tmpSlot, slots, slot_1, slot_2, result
  
  tmpSlot = replace(slot,":00 ","")
  slots = split(tmpSlot, "-")
  
  
  slot_1 = split(slots(0),":")
  slot_2 = split(slots(1),":")
  
  if cint(slot_1(0)) < 10 then slot_1(0) = "0" & slot_1(0)
  if cint(slot_2(0)) < 10 then slot_2(0) = "0" & slot_2(0)
  
  
  
  result = join(slot_1,":") & "-" & join(slot_2,":")
  
  formatPlainTimeSlot = result

end function

function formatSSN(SSN)

  dim finalSSN, tmpSSN

  if len(SSN) > 0 then

  
  tmpSSN = ReplaceStr(trim(SSN),"-", "",0)
  tmpSSN = ReplaceStr(tmpSSN," ", "",0)
  
  end if
  
  if len(tmpSSN) = 9 then
    finalSSN = left(tmpSSN,3) & "-" & mid(tmpSSN,4,2) & "-" & right(tmpSSN,4)
  else
    finalSSN = SSN
  end if
     
  formatSSN = finalSSN

end function

function formatPhone(Phone)

  dim finalPhone, tmpPhone
  
  if len(Phone) > 0 then
  
  tmpPhone = ReplaceStr(trim(Phone),"-","",0)
  tmpPhone = ReplaceStr(tmpPhone," ","",0)
  tmpPhone = ReplaceStr(tmpPhone,"\(","",0)
  tmpPhone = ReplaceStr(tmpPhone,"\)","",0)
  
  end if
  
  if len(tmpPhone) = 10 then
    finalPhone = "(" & left(tmpPhone,3) & ") " & mid(tmpPhone,4,3) & "-" & right(tmpPhone,4)
  else
    finalPhone = Phone
  end if
     
  formatPhone = finalPhone

end function

Function MediumDate( ByVal stringToCheck, ByVal defaultOut )

    Dim dayPart
    Dim monthPart
    Dim yearPart
	
    If IsDate( stringToCheck ) Then
		
        dayPart   = Day( stringToCheck )
        monthPart = Monthname( Month( stringToCheck ), True )
        yearPart  = Year( stringToCheck )
		
        MediumDate = dayPart & "-" & monthPart & "-" & yearPart
		
    Else
		
        MediumDate = defaultOut
		
    End If
    
End Function

Sub DebugPrint(message)
  if debug > 0 then
    Response.Write message & vbcrlf
  end if
End Sub

Sub CheckPermissions(login_type, user_role)
  if session("Start") = "" then
    Response.Redirect "/login/default.asp?expire=1"
  end if
  if session("type") <> login_type then
    Response.Redirect "/" & session("type")
  end if
  if user_role <> "" then
    if session("role") <> user_role then     
      Response.Redirect "/login/default.asp?expire=1"
    end if
  end if
End Sub

'A library of Miscellaneous functions
Function RemoveWhiteSpace(strText)
 Dim RegEx
 Set RegEx = New RegExp
 RegEx.Pattern = "\s+"
 RegEx.Multiline = True
 RegEx.Global = True
 strText = RegEx.Replace(strText, "")
 RemoveWhiteSpace = strText
End Function

Function nl2br(strText)
 Dim RegEx
 Set RegEx = New RegExp
 
 RegEx.Pattern = "(\n\r){4,}"
 RegEx.Multiline = True
 RegEx.Global = True
 strText = RegEx.Replace(strText, vbCr)
 
 RegEx.Pattern = "(\n){1}"
 RegEx.Multiline = True
 RegEx.Global = True
 strText = RegEx.Replace(strText, "<br>" & vbCr)
 nl2br = strText
End Function

Function FixName(NameStr)
	'NameStr = RemoveWhiteSpace(NameStr)
	NameStr = replace(NameStr, "'", "")
	FixName = NameStr
End Function

Function ReplaceStr (TextIn, ByVal SearchStr, _
                     ByVal Replacement, _
                     ByVal CompMode)
Dim WorkText, Pointer
  If IsNull(TextIn) Then
    ReplaceStr = Null
  Else
    WorkText = TextIn
    Pointer = InStr(1, WorkText, SearchStr, CompMode)
    Do While Pointer > 0
      WorkText = Left(WorkText, Pointer - 1) & Replacement & _
                 Mid(WorkText, Pointer + Len(SearchStr))
      Pointer = InStr(Pointer + Len(Replacement), WorkText, _
                      SearchStr, CompMode)
    Loop
    ReplaceStr = WorkText
  End If
End Function

Function SQLFixup(TextIn)
  TextIn = ReplaceStr(TextIn, "'", "", 0)
  TextIn = ReplaceStr(TextIn, "#", "", 0)
  SQLFixUp = TextIn
End Function

Function JSFixup(TextIn)
  dim Temp
  Temp = ReplaceStr(TextIn, """", "&quot;", 0)
  Temp = ReplaceStr(Temp, vbcr, "", 0)
  Temp = ReplaceStr(Temp, vblf, "", 0)
  JSFixup = ReplaceStr(Temp, "'", "\'", 0)
End Function

Function CreateFileDirectory(FolderPath)
	
End Function

Function testEMail(sEmail)
  testEMail = false
  Dim regEx, retVal
  Set regEx = New RegExp

  ' Create regular expression:
  regEx.Pattern ="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,3}$" 

  ' Set pattern:
  regEx.IgnoreCase = true

  ' Set case sensitivity.
  retVal = regEx.Test(sEmail)

  ' Execute the search test.
  If not retVal Then
    exit function
  End If

  testEMail = true
End Function

Function inStrGrp(src,reg)
  Dim regEx
  Set regEx = new RegExp
  
  regEx.Pattern = reg
  
  regEx.IgnoreCase = true
  
  inStrGrp = regEx.test(src)
End Function

function SplitStr(TextIn,ArgV)

	Dim Arr(4)
	'Response.Write left(TextIn,Instr(TextIn,"***")-1)

	if not isnull(TextIn) then
		for i = 0 to ArgV
			'Response.Write instr(TextIn,"***")
			if TextIn = "" then
				exit for
			elseif instr(TextIn,"***") = 0 then
				Arr(i) = TextIn
				TextIn = ""
			else
				Arr(i) = left(TextIn,Instr(TextIn,"***")-1)
				TextIn = right(TextIn,len(TextIn)-len(Arr(i))-3)
			end if
			
			'Response.Write "OriginalText"&TextIn&"<BR>"
			'Response.Write "ArrayItem:  "&Arr(i)&"<br>"
		next
	end if
	
	'return Arr
	SplitStr = Arr
	
end function


    '**************************************
    ' Name: Credit Card Mod 10 Validation
    ' Description:Based on ANSI X4.13, the L
    '     UHN formula (also known as the modulus 1
    '     0 -- or mod 10 -- algorithm ) is used to
    '     generate and/or validate and verify the 
    '     accuracy of credit-card numbers.
    ' By: Lewis Moten
    '
    ' Inputs:asCardType - Type of credit car
    '     d. (American Express, Discover, Visa, Ma
    '     sterCard)
    ' anCardNumber - The number appearing On the card. Dashes and spaces are ok. Numbers are stripped from the data provided.
    '
    ' Returns:Returns a boolean (true/false)
    '     determining if the number appears to be 
    '     valid or not.
    '
    ' Assumes:The user needs to be able to l
    '     ook through the code and determine wich 
    '     strings represent the cards. You can eit
    '     her type out the entire card name (i.e. 
    '     "American Express") or type in just a le
    '     tter representing the card name (i.e. "a
    '     ")
    '
    ' Side Effects:Just because the function
    '     returns that the card is valid, there ar
    '     e several other things that are not bein
    '     g validated.
    'Date - make sure the card has Not expired
    'Active Account - This script does Not communicate With any banks To determine if the account number is active
    'Authorization - again, this script does Not communicate With any banks To determine if the card has authorization to purchase a product.
    '
    
    	function IsCreditCard(ByRef asCardType, ByRef anCardNumber)
    		' Performs a Mod 10 check To make sure the credit card number
    		' appears valid
    		' Developers may use the following numbers as dummy data:
    		' Visa:					430-00000-00000
    		' American Express:		372-00000-00000
    		' Mastercard:			521-00000-00000
    		' Discover:				620-00000-00000
    			
    		Dim lsNumber		' Credit card number stripped of all spaces, dashes, etc.
    		Dim lsChar			' an individual character
    		Dim lnTotal			' Sum of all calculations
    		Dim lnDigit			' A digit found within a credit card number
    		Dim lnPosition		' identifies a character position In a String
    		Dim lnSum			' Sum of calculations For a specific Set
    			
    		' Default result is False
    		IsCreditCard = False
    			
    		' ====
    		' Strip all characters that are Not numbers.
    		' ====
    			
    		' Loop through Each character inthe card number submited
    		For lnPosition = 1 To Len(anCardNumber)
    			' Grab the current character
    			lsChar = Mid(anCardNumber, lnPosition, 1)
    			' if the character is a number, append it To our new number
    			if IsNumeric(lsChar) Then lsNumber = lsNumber & lsChar
    			
    		Next ' lnPosition
    			
    		' ====
    		' The credit card number must be between 13 and 16 digits.
    		' ====
    		' if the length of the number is less Then 13 digits, then Exit the routine
    		if Len(lsNumber) < 13 Then Exit function
    			
    		' if the length of the number is more Then 16 digits, then Exit the routine
    		if Len(lsNumber) > 16 Then Exit function
    			
    			
    		' ====
    		' The credit card number must start with: 
    		'	4 For Visa Cards 
    		'	37 For American Express Cards 
    		'	5 For MasterCards 
    		'	6 For Discover Cards 
    		' ====
    			
    		' Choose action based on Type of card
    		Select Case LCase(asCardType)
    			' VISA
    			Case "visa", "v"
    				' if first digit Not 4, Exit function
    				if Not Left(lsNumber, 1) = "4" Then Exit function
    			' American Express
    			Case "american express", "americanexpress", "american", "ax", "a"
    				' if first 2 digits Not 37, Exit function
    				if Not Left(lsNumber, 2) = "37" Then Exit function
    			' Mastercard
    			Case "mastercard", "master card", "master", "m"
    				' if first digit Not 5, Exit function
    				if Not Left(lsNumber, 1) = "5" Then Exit function
    			' Discover
    			Case "discover", "discovercard", "discover card", "d"
    				' if first digit Not 6, Exit function
    				if Not Left(lsNumber, 1) = "6" Then Exit function
    				
    			Case Else
    		End Select ' LCase(asCardType)
    			
    		' ====
    		' if the credit card number is less Then 16 digits add zeros
    		' To the beginning to make it 16 digits.
    		' ====
    		' Continue Loop While the length of the number is less Then 16 digits
    		While Not Len(lsNumber) = 16
    				
    			' Insert 0 To the beginning of the number
    			lsNumber = "0" & lsNumber
    			
    		Wend ' Not Len(lsNumber) = 16
    			
    		' ====
    		' Multiply Each digit of the credit card number by the corresponding digit of
    		' the mask, and sum the results together.
    		' ====
    			
    		' Loop through Each digit
    		For lnPosition = 1 To 16
    				
    			' Parse a digit from a specified position In the number
    			lnDigit = Mid(lsNumber, lnPosition, 1)
    				
    			' Determine if we multiply by:
    			'	1 (Even)
    			'	2 (Odd)
    			' based On the position that we are reading the digit from
    			lnMultiplier = 1 + (lnPosition Mod 2)
    				
    			' Calculate the sum by multiplying the digit and the Multiplier
    			lnSum = lnDigit * lnMultiplier
    				
    			' (Single digits roll over To remain single. We manually have to Do this.)
    			' if the Sum is 10 or more, subtract 9
    			if lnSum > 9 Then lnSum = lnSum - 9
    				
    			' Add the sum To the total of all sums
    			lnTotal = lnTotal + lnSum
    			
    		Next ' lnPosition
    			
    		' ====
    		' Once all the results are summed divide
    		' by 10, if there is no remainder Then the credit card number is valid.
    		' ====
    		IsCreditCard = ((lnTotal Mod 10) = 0)
    			
    	End function ' IsCreditCard


function getRealName(order, username)

	
	dim table, idcol
	dim realname, RSName
	dim user
	
	if username <> "" then
	  user = username
	else
	  user = session("user")
	end if
	
	set RSName = Server.CreateObject("ADODB.Recordset")
	
	select case session("role")
		case "Admin"
			table = "streetreg"
			idcol = "userid"
		case "Biller"
			table = "billerreg"
			idcol = "logid"
		case "Frontdesk"
			table = "fdreg"
			idcol = "logid"
		case "Physician"
			table = "doctortable"
			idcol = "docuser"
		case "admin"
			table = "streetreg"
			idcol = "userid"
		case "administrator"
			table = "streetreg"
			idcol = "userid"
		case "biller"
			table = "billerreg"
			idcol = "logid"
		case "frontdesk"
			table = "fdreg"
			idcol = "logid"
		case "nurse"
			table = "nursereg"
			idcol = "logid"
		case "physician"
			table = "doctortable"
			idcol = "docuser"
		case "rn"
			table = "nursereg"
			idcol = "logid"
	end select
  
	sqlQuery = "SELECT * FROM " & table & " WHERE " & idcol & " = '" & user & "'"
	
	RSName.Open sqlQuery, CONN
	
	if order = 1 then
	  realname = PCase(RSName("first_name")) & " " & PCase(RSName("last_name"))
	else
	  realname = PCase(RSName("last_name"))  & ", " &  PCase(RSName("first_name"))
	end if
	
	RSName.Close
	set RSName = nothing
	
	getRealname = realname

end function

Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function

Function CompareDates(dt1, dt2)
	If Year(dt1) > Year(dt2) then
		CompareDates = 1
	elseif Year(dt1) = Year(dt2) then
		If Month(dt1) > Month(dt2) then
			CompareDates = 1
		elseif Month(dt1) = Month(dt2) then
			if Day(dt1) > Day(dt2) then
				CompareDates = 1
			else
				CompareDates = 0
			end if
		end if
	end if
	'CompareDates = 0
End Function

Function UseNewClaimForm(ClaimCreationDate)
	if CDATE(ClaimCreationDate) > CDATE(Application("New_Claim_Form_Date")) then
		UseNewClaimForm = 1
	else
		UseNewClaimForm = 0
	end if
End Function

Function CalculateLocalTime(ServerTime)
	if NOT isnull(session("TimeZone")) then
		if session("TimeZone")="CST" then 
			CalculateLocalTime = DateAdd("h",2,cdate(ServerTime))
		end if
		if session("TimeZone")="EST" then 
			CalculateLocalTime = DateAdd("h",3,cdate(ServerTime))
		end if
		if session("TimeZone")="MST" then 
			CalculateLocalTime = DateAdd("h",1,cdate(ServerTime))
		end if
		if session("TimeZone")="PST" then 
			CalculateLocalTime = cdate(ServerTime)
		end if		
	else
		CalculateLocalTime = cdate(ServerTime)
	end if
End Function

Function getAge2(ByVal birth_date)

  set RS = server.CreateObject("ADODB.Recordset")
  sqlQuery="select trunc(months_between(sysdate,dob)/12) year, trunc(mod(months_between(sysdate,dob),12)) month, trunc(sysdate-add_months(dob,trunc(months_between(sysdate,dob)/12)*12+trunc(mod(months_between(sysdate,dob),12)))) day from (Select to_date('"&birth_date&"','mm/dd/yyyy') dob from dual)"
 ' response.Write sqlQuery

  RS.open sqlQuery,CONN  
  if not RS.eof then
        b_year=rs("year")
        b_month=rs("month")
        b_days=rs("day")
  end if
  rs.close
  set rs=nothing

    getAge2=  b_year &"Y/"& b_month &"M"

End Function
    
Function HTMLEncode(str)
    if not IsNull(str) then
        HTMLEncode = Server.HTMLEncode(str)
    else
        HTMLEncode = str
    end if
End Function

Function FindOtherUnitsByFormuationUnit (ByVal formulation_unit, ByRef dose_unit, ByRef dispense_unit)
  formulation_unit = trim(formulation_unit)
  
  if formulation_unit <> "" then
    set RS = server.CreateObject("ADODB.Recordset")
    sqlQuery="select * from NOTES_MEDICAL_UNIT_MAP where instr(lower('" & formulation_unit & "'), lower(FORMULATION_UNIT)) > 0"

    RS.open sqlQuery,CONN
    
    first = true
    do while not (RS.eof or RS.bof)
      if first = true then
        dose_unit=RS("DOSE_UNIT")
        dispense_unit=RS("DISPENSE_UNIT")
        first = false
      elseif (LCase(formulation_unit) = LCase(RS("FORMULATION_UNIT"))) then
        dose_unit=RS("DOSE_UNIT")
        dispense_unit=RS("DISPENSE_UNIT")
      end if
    	RS.movenext
    loop
    
    RS.close
    set RS=nothing
  end if

End Function

%>