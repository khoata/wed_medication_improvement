namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;

    public class PrescriberInfo
    {
        [XmlElement("ClinicName")]
        public string mClinicName;
        [XmlElement("DirectoryInformation")]
        public DirectoryInformation mDirectoryInfo;
        [XmlElement("Address")]
        public Address mPrescriberAddress;
        [XmlElement("PrescriberAgent")]
        public Name mPrescriberAgent;
        [XmlElement("Email")]
        public string mPrescriberEmail;
        [XmlElement("Identification")]
        public ID mPrescriberID;
        [XmlElement("Name")]
        public Name mPrescriberName;
        [XmlElement("PhoneNumbers")]
        public PhysicianPhoneNumbers mPrescriberPhone;
        [XmlElement("Specialty")]
        public Specialty mSpecialty;

        public PrescriberInfo()
        {
            this.mDirectoryInfo = new DirectoryInformation();
            this.mPrescriberID = new ID();
            this.mPrescriberName = new Name();
            this.mSpecialty = new Specialty();
            this.mPrescriberAgent = new Name();
            this.mPrescriberAddress = new Address();
            this.mPrescriberPhone = new PhysicianPhoneNumbers();
            this.mPrescriberPhone.mPhone = new Phone[1];
        }

        public PrescriberInfo(string PrescriberID)
        {
            this.Load(PrescriberID);
        }

        public PrescriberInfo(string PrescriberID, int ServiceLevel)
        {
            this.mDirectoryInfo = new DirectoryInformation();
            this.mDirectoryInfo.PortalID = ConfigurationManager.AppSettings["SSPortalID"].ToString();
            this.mDirectoryInfo.AccountID = ConfigurationManager.AppSettings["SSAccountID"].ToString();
            this.mDirectoryInfo.BackupPortalID = ConfigurationManager.AppSettings["SSBackupPortalID"].ToString();
            this.mDirectoryInfo.ServiceLevel = ServiceLevel.ToString();
            this.mDirectoryInfo.ActiveStartTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.0Z");
            this.mDirectoryInfo.ActiveEndTime = DateTime.Now.AddYears(5).ToString("yyyy-MM-ddTHH:mm:ss.0Z");
            this.Load(PrescriberID);
        }

        public PrescriberInfo(string dummyOverload, string MessageId)
        {
            this.LoadFromRefillRequest(MessageId);
        }

        public void Load(string PrescriberID)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPrescriberInfo";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPrescriberID", OracleType.VarChar)).Value = PrescriberID;
            command.Parameters.Add(new OracleParameter("CurPrescriberInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPrescriberID = new ID();
                this.mPrescriberID.SPI = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                this.mPrescriberID.NPI = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                this.mPrescriberID.DEANumber = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mClinicName = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPrescriberName = new Name();
                this.mPrescriberName.LastName = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mPrescriberName.FirstName = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.mSpecialty = new Specialty();
                this.mSpecialty.Qualifier = "AM";
                this.mSpecialty.SpecialtyCode = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));
                if (!reader.GetOracleString(6).IsNull)
                {
                    this.mPrescriberAddress = new Address();
                    this.mPrescriberAddress.Address1 = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                    this.mPrescriberAddress.City = reader.GetOracleString(8).IsNull ? null : ((string) reader.GetOracleString(8));
                    this.mPrescriberAddress.State = reader.GetOracleString(9).IsNull ? null : ((string) reader.GetOracleString(9));
                    this.mPrescriberAddress.Zip = reader.GetOracleString(10).IsNull ? null : ((string) reader.GetOracleString(10));
                }
                this.mPrescriberEmail = reader.GetOracleString(11).IsNull ? null : ((string) reader.GetOracleString(11));
                if (!reader.GetOracleString(12).IsNull)
                {
                    this.mPrescriberPhone = new PhysicianPhoneNumbers();
                    this.mPrescriberPhone.mPhone = new Phone[2];
                    this.mPrescriberPhone.mPhone[0] = new Phone();
                    this.mPrescriberPhone.mPhone[0].PhoneNumber = reader.GetOracleString(12).IsNull ? null : ((string) reader.GetOracleString(12)).Replace("-", "");
                    this.mPrescriberPhone.mPhone[0].Qualifier = reader.GetOracleString(13).IsNull ? null : ((string) reader.GetOracleString(13));
                    if (!reader.GetOracleString(14).IsNull)
                    {
                        this.mPrescriberPhone.mPhone[1] = new Phone();
                        this.mPrescriberPhone.mPhone[1].PhoneNumber = reader.GetOracleString(14).IsNull ? null : ((string) reader.GetOracleString(14)).Replace("-", "");
                        this.mPrescriberPhone.mPhone[1].Qualifier = reader.GetOracleString(15).IsNull ? null : ((string) reader.GetOracleString(15));
                    }
                }
            }
            reader.Close();
            connection.Close();
        }

        public void LoadFromRefillRequest(string MessageId)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPrescriberFromRefReq";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageId", OracleType.VarChar)).Value = MessageId;
            command.Parameters.Add(new OracleParameter("CurPrescriberInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPrescriberID = new ID();
                this.mPrescriberID.SPI = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                this.mPrescriberID.NPI = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                this.mPrescriberID.DEANumber = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mClinicName = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPrescriberName = new Name();
                this.mPrescriberName.LastName = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mPrescriberName.FirstName = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.mPrescriberAgent = new Name();
                this.mPrescriberAgent.LastName = this.mPrescriberName.LastName;
                this.mPrescriberAgent.FirstName = this.mPrescriberName.FirstName;
                this.mSpecialty = new Specialty();
                this.mSpecialty.Qualifier = "AM";
                this.mSpecialty.SpecialtyCode = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));
                if (!reader.GetOracleString(6).IsNull)
                {
                    this.mPrescriberAddress = new Address();
                    this.mPrescriberAddress.Address1 = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                    this.mPrescriberAddress.City = reader.GetOracleString(8).IsNull ? null : ((string) reader.GetOracleString(8));
                    this.mPrescriberAddress.State = reader.GetOracleString(9).IsNull ? null : ((string) reader.GetOracleString(9));
                    this.mPrescriberAddress.Zip = reader.GetOracleString(10).IsNull ? null : ((string) reader.GetOracleString(10));
                }
                this.mPrescriberEmail = reader.GetOracleString(11).IsNull ? null : ((string) reader.GetOracleString(11));
                if (!reader.GetOracleString(12).IsNull)
                {
                    this.mPrescriberPhone = new PhysicianPhoneNumbers();
                    this.mPrescriberPhone.mPhone = new Phone[] { new Phone() };
                    this.mPrescriberPhone.mPhone[0].PhoneNumber = reader.GetOracleString(12).IsNull ? null : ((string) reader.GetOracleString(12)).Replace("-", "");
                    this.mPrescriberPhone.mPhone[0].Qualifier = reader.GetOracleString(13).IsNull ? null : ((string) reader.GetOracleString(13));
                }
            }
            reader.Close();
            connection.Close();
        }
    }
}

