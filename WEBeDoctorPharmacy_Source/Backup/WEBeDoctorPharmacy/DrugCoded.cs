namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class DrugCoded
    {
        [XmlElement("DosageForm")]
        public string DosageForm;
        [XmlElement("DrugDBCode")]
        public string DrugDBCode;
        [XmlElement("DrugDBCodeQualifier")]
        public string DrugDBCodeQualifier;
        [XmlElement("ProductCode")]
        public string ProductCode;
        [XmlElement("ProductCodeQualifier")]
        public string ProductCodeQualifier;
        [XmlElement("Strength")]
        public string Strength;
        [XmlElement("StrengthUnits")]
        public string StrengthUnits;
    }
}

