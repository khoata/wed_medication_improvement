namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class PhoneNumbers
    {
        [XmlElement("Phone")]
        public Phone mPhone;
    }
}

