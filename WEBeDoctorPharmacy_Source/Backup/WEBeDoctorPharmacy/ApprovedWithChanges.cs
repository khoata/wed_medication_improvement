namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class ApprovedWithChanges
    {
        [XmlElement("Note")]
        public string note;
    }
}

