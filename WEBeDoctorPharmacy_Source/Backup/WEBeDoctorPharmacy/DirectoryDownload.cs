namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Xml.Serialization;

    public class DirectoryDownload
    {
        [XmlElement("DirectoryDate")]
        public string mDirectoryDate;
        [XmlElement("AccountID")]
        public string mSSAccountID = ConfigurationManager.AppSettings["SSAccountID"].ToString();
        [XmlElement("VersionID")]
        public string mSSVersionID = ConfigurationManager.AppSettings["SSVersionID"].ToString();
        [XmlElement("Taxonomy")]
        public Taxonomy mTaxonomy = new Taxonomy();

        public DirectoryDownload()
        {
            this.mTaxonomy.TaxonomyCode = ConfigurationManager.AppSettings["SSTaxonomyCode"].ToString();
            this.mDirectoryDate = DateTime.Now.AddDays(-30.0).ToString("yyyyMMdd");
        }

        public class Taxonomy
        {
            [XmlElement("TaxonomyCode")]
            public string TaxonomyCode;
        }
    }
}

