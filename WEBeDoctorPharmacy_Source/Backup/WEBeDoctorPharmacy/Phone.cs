namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Phone
    {
        [XmlElement("Number")]
        public string PhoneNumber;
        [XmlElement("Qualifier")]
        public string Qualifier;
    }
}

