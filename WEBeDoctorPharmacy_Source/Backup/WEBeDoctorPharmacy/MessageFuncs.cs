namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;

    public static class MessageFuncs
    {
        public static string[] LoadNewRxParams(string Drug_ID, string PrescriberOrderNumber)
        {
            string[] NewRXParams = new string[8];
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.LoadCorrelatedNewRxParams";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamDrugID", OracleType.VarChar)).Value = Drug_ID;
            command.Parameters.Add(new OracleParameter("ParamPRESCRIBERORDERNUMBER", OracleType.VarChar)).Value = PrescriberOrderNumber;
            command.Parameters.Add(new OracleParameter("CurNewRxParams", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                NewRXParams[0] = reader.GetOracleString(0).IsNull ? null : reader.GetOracleString(0).ToString();
                NewRXParams[1] = reader.GetOracleString(1).IsNull ? null : reader.GetOracleString(1).ToString();
                NewRXParams[2] = reader.GetOracleString(2).IsNull ? null : reader.GetOracleString(2).ToString();
                NewRXParams[3] = reader.GetOracleString(3).IsNull ? null : reader.GetOracleString(3).ToString();
                NewRXParams[4] = reader.GetOracleString(4).IsNull ? null : reader.GetOracleString(4).ToString();
            }
            reader.Close();
            connection.Close();
            return NewRXParams;
        }
    }
}

