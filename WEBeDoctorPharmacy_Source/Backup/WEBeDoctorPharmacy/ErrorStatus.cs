namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class ErrorStatus : GenericStatus
    {
        [XmlElement("Description")]
        public string Description;
        [XmlElement("DescriptionCode")]
        public string DescriptionCode;

        public ErrorStatus()
        {
        }

        public ErrorStatus(string code, string descCode, string desc) : base(code)
        {
            this.DescriptionCode = descCode;
            this.Description = desc;
        }
    }
}

