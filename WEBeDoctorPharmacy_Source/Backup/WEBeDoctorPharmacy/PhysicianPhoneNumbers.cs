namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class PhysicianPhoneNumbers
    {
        [XmlElement("Phone")]
        public Phone[] mPhone;
    }
}

