namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Denied
    {
        [XmlElement("DenialReason")]
        public string DenialReason;
        [XmlElement("DenialReasonCode")]
        public string DenialReasonCode;
    }
}

