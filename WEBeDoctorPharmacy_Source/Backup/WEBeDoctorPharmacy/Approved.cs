namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Approved
    {
        [XmlElement("Note")]
        public string Note;
    }
}

