namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class CoreMessage
    {
        [XmlElement("MedicationPrescribed")]
        public MedicationInfo mMedication;
        [XmlElement("Patient")]
        public PatientInfo mPatient;
        [XmlElement("Pharmacy")]
        public PharmacyInfo mPharmacy;
        [XmlElement("Prescriber")]
        public PrescriberInfo mPrescriber;
        private Guid OrderNumber;
        [XmlElement("PrescriberOrderNumber")]
        public string PrescriberOrderNumber;
        [XmlElement("Response")]
        public ResponseType ResponseMsg;
        [XmlElement("RxReferenceNumber")]
        public string RXReferenceNumber;

        public CoreMessage()
        {
        }

        public CoreMessage(string messageType, string[] inputParams)
        {
            if (messageType != "RefillResponse")
            {
                if (inputParams[6] != null)
                {
                    this.RXReferenceNumber = "999999";
                }
                this.OrderNumber = Guid.NewGuid();
                this.PrescriberOrderNumber = this.OrderNumber.ToString().Replace("-", "");
                if (inputParams[6] != null)
                {
                    this.ResponseMsg = new ResponseType(inputParams);
                }
                this.mPharmacy = new PharmacyInfo(inputParams[0]);
                this.mPrescriber = new PrescriberInfo(inputParams[1]);
                if ((inputParams.Length > 9) && ((inputParams[8] ?? "").Length > 0))
                {
                    this.mPatient = new PatientInfo(null, inputParams[8]);
                }
                if ((((this.mPatient == null) || !this.mPatient.wasLoadedFromDb()) && (inputParams[2] != null)) && (inputParams[2].Length > 0))
                {
                    this.mPatient = new PatientInfo(inputParams[2]);
                }
                this.mMedication = new MedicationInfo(inputParams[3], inputParams[4]);
            }
            else
            {
                if (inputParams[6] != null)
                {
                    this.RXReferenceNumber = "999999";
                }
                this.OrderNumber = Guid.NewGuid();
                this.PrescriberOrderNumber = this.OrderNumber.ToString().Replace("-", "");
                if (inputParams[6] != null)
                {
                    this.ResponseMsg = new ResponseType(inputParams);
                }
                this.mPharmacy = new PharmacyInfo(inputParams[0]);
                if ((inputParams[8] ?? "").Length > 0)
                {
                    this.mPrescriber = new PrescriberInfo(null, inputParams[8]);
                    this.mPatient = new PatientInfo(null, inputParams[8]);
                    this.mMedication = new MedicationInfo(inputParams[8], inputParams[10], "");
                    if (inputParams[9] != null)
                    {
                        this.mMedication.Refills = new QualifierQuantityPair();
                        this.mMedication.Refills.Quantity = inputParams[9];
                        if (this.mMedication.Refills.Quantity == "999")
                        {
                            this.mMedication.Refills.Qualifier = "PRN";
                            this.mMedication.Refills.Quantity = null;
                        }
                        else
                        {
                            this.mMedication.Refills.Qualifier = "R";
                            if (this.mMedication.Refills.Quantity == "0")
                            {
                                this.mMedication.Refills.Quantity = null;
                            }
                        }
                    }
                }
            }
        }
    }
}

