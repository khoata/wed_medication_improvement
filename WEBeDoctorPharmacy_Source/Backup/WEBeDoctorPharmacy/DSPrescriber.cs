namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class DSPrescriber
    {
        [XmlElement("Prescriber")]
        public PrescriberInfo mPrescriber;
        [XmlElement("Response")]
        public ResponseType mResponse;

        public DSPrescriber()
        {
        }

        public DSPrescriber(string PrescriberID)
        {
            this.mResponse = new ResponseType();
            this.mPrescriber = new PrescriberInfo(PrescriberID);
        }

        public DSPrescriber(string PrescriberID, int ServiceLevel)
        {
            this.mPrescriber = new PrescriberInfo(PrescriberID, ServiceLevel);
        }
    }
}

