namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;

    public class MedicationInfo
    {
        [XmlElement("DaysSupply")]
        public string DaysSupply;
        [XmlElement("Primary")]
        public Diagnosis Diagnosis1;
        [XmlElement("Secondary")]
        public Diagnosis Diagnosis2;
        [XmlElement("Directions")]
        public string Directions;
        [XmlElement("DrugCoded")]
        public DrugCoded DrugCode;
        [XmlElement("DrugDescription")]
        public string DrugDescription;
        [XmlElement("LastFillDate")]
        public string LastFillDate;
        [XmlElement("Note")]
        public string Note;
        [XmlElement("Quantity")]
        public QualifierValuePair Quantity;
        [XmlElement("Refills")]
        public QualifierQuantityPair Refills;
        [XmlElement("Substitutions")]
        public string Substitutions;
        [XmlElement("WrittenDate")]
        public string WrittenDate;

        public MedicationInfo()
        {
        }

        public MedicationInfo(string DrugID, string visitKey)
        {
            this.Load(DrugID, visitKey);
        }

        public MedicationInfo(string MessageId, string subid, string dummyOverload)
        {
            this.LoadFromRefillRequest(MessageId, subid);
        }

        public void Load(string drugID, string visitKey)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetMedicationPrescribed";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPrescriptionID", OracleType.VarChar)).Value = drugID;
            command.Parameters.Add(new OracleParameter("ParamVisitKey", OracleType.VarChar)).Value = visitKey;
            command.Parameters.Add(new OracleParameter("CurPrescription", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.DrugDescription = reader.GetOracleString(0).IsNull ? null : ((reader.GetOracleString(0).ToString().Replace("-", "/").Length > 0x69) ? reader.GetOracleString(0).ToString().Replace("-", "/").Substring(0, 0x69) : reader.GetOracleString(0).ToString().Replace("-", "/"));
                if (!reader.GetOracleString(1).IsNull)
                {
                    this.DrugCode = new DrugCoded();
                    this.DrugCode.ProductCode = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                    this.DrugCode.ProductCodeQualifier = (string) reader.GetOracleString(2);
                }
                this.Quantity = new QualifierValuePair();
                this.Quantity.Value = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.Quantity.Qualifier = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.Directions = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.Note = reader.GetOracleString(6).IsNull ? "NA" : ((string) reader.GetOracleString(6));
                this.Note = this.Note.Replace('\r', ' ').Replace('\n', ' ');
                this.Refills = new QualifierQuantityPair();
                this.Refills.Quantity = null;
                this.Refills.Qualifier = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                if (this.Refills.Qualifier != "PRN")
                {
                    this.Refills.Quantity = reader.GetOracleValue(8).ToString();
                }
                this.Substitutions = reader.GetOracleString(9).IsNull ? "0" : ((string) reader.GetOracleString(9));
                this.WrittenDate = DateTime.Parse(reader.GetOracleDateTime(10).ToString()).ToString("yyyyMMdd");
            }
            reader.Close();
            connection.Close();
        }

        public void LoadFromRefillRequest(string MessageId, string subid)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetMedFromRefReq";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = MessageId;
            command.Parameters.Add(new OracleParameter("ParamSubstitutionID", OracleType.VarChar)).Value = subid ?? "";
            command.Parameters.Add(new OracleParameter("CurPrescription", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.DrugDescription = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                if (!reader.GetOracleString(1).IsNull)
                {
                    this.DrugCode = new DrugCoded();
                    this.DrugCode.ProductCode = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                    this.DrugCode.ProductCodeQualifier = (string) reader.GetOracleString(2);
                }
                this.Quantity = new QualifierValuePair();
                this.Quantity.Value = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.Quantity.Qualifier = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.Directions = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.Note = reader.GetOracleString(6).IsNull ? "NA" : ((string) reader.GetOracleString(6));
                this.Note = this.Note.Replace('\r', ' ').Replace('\n', ' ');
                this.Refills = new QualifierQuantityPair();
                this.Refills.Quantity = null;
                this.Refills.Qualifier = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                if (this.Refills.Qualifier != "PRN")
                {
                    this.Refills.Quantity = reader.GetOracleValue(8).ToString();
                }
                if (((this.Refills.Quantity ?? "0") == "0") && (this.Refills.Qualifier == "R"))
                {
                    this.Refills.Quantity = null;
                }
                this.Substitutions = reader.GetOracleString(9).IsNull ? "0" : ((string) reader.GetOracleString(9));
                this.WrittenDate = reader.GetOracleString(10).IsNull ? "0" : ((string) reader.GetOracleString(10));
            }
            reader.Close();
            connection.Close();
        }
    }
}

