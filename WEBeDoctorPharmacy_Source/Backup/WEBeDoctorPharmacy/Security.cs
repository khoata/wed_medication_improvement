namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Security
    {
        [XmlElement("UsernameToken")]
        public UserNameToken mUsernameToken;

        public class UserNameToken
        {
            [XmlElement("Created")]
            public string mCreated;
            [XmlElement("Nonce")]
            public string mNonce;
            [XmlElement("Password")]
            public Password mPassword;
            [XmlElement("Username")]
            public string mUsername;

            public class Password
            {
                [XmlText]
                public string mmPassword;
                [XmlAttribute("Type")]
                public string mmPasswordType = "PasswordDigest";
            }
        }
    }
}

