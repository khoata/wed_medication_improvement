namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Diagnosis
    {
        [XmlElement("ClinicalInformationQualifier")]
        public string ClinicalInformationQualifier;
        [XmlElement("Primary")]
        public QualifierValuePair Primary;
        [XmlElement("Secondary")]
        public QualifierValuePair Secondary;
    }
}

