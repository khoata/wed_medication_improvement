namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class ID
    {
        [XmlElement("DEANumber")]
        public string DEANumber;
        [XmlElement("DentistLicenseNumber")]
        public string DentistLicenseNumber;
        [XmlElement("FileID")]
        public string FileID;
        [XmlElement("MedicaidNumber")]
        public string MedicaidNumber;
        [XmlElement("MedicareNumber")]
        public string MedicareNumber;
        [XmlElement("MutuallyDefined")]
        public string MutuallyDefined;
        [XmlElement("NCPDPID")]
        public string NCPDPID;
        [XmlElement("NPI")]
        public string NPI;
        [XmlElement("PayerID")]
        public string PayerID;
        [XmlElement("PPONumber")]
        public string PPONumber;
        [XmlElement("PriorAuthorization")]
        public string PriorAuthorization;
        [XmlElement("SocialSecurity")]
        public string SocialSecurityNumber;
        [XmlElement("SPI")]
        public string SPI;
        [XmlElement("StateLicenseNumber")]
        public string StateLicenseNumber;
        [XmlElement("UPIN")]
        public string UPIN;
    }
}

