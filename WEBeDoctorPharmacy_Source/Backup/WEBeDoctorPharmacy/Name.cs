namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Name
    {
        [XmlElement("FirstName")]
        public string FirstName;
        [XmlElement("LastName")]
        public string LastName;
        [XmlElement("MiddleName")]
        public string MiddleName;
        [XmlElement("Prefix")]
        public string Prefix;
        [XmlElement("Suffix")]
        public string Suffix;
    }
}

