namespace WEBeDoctorPharmacy
{
    using System;

    public static class StatusCode
    {
        public const string CommunicationError = "600";
        public const string ReceiverSystemError = "602";
        public const string ReceiverUnableToProcess = "601";
        public const string TransactionRejected = "900";
        public const string TransactionSuccessfulPending = "000";
        public const string TransactionSuccessfulVerified = "010";
    }
}

