namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class DeniedNewPrescriptionToFollow
    {
        [XmlElement("Note")]
        public string note;
    }
}

