namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class ResponseType
    {
        [XmlElement("Approved")]
        public Approved ApprovedResp;
        [XmlElement("ApprovedWithChanges")]
        public ApprovedWithChanges ApprovedWithChangesResp;
        [XmlElement("DeniedNewPrescriptionToFollow")]
        public DeniedNewPrescriptionToFollow DeniedNewPrescripResp;
        [XmlElement("Denied")]
        public Denied DeniedResp;

        public ResponseType()
        {
        }

        public ResponseType(string[] inputParams)
        {
            string CS$4$0000 = inputParams[5];
            if (CS$4$0000 != null)
            {
                if (!(CS$4$0000 == "Approved"))
                {
                    if (CS$4$0000 == "Denied")
                    {
                        this.DeniedResp = new Denied();
                        this.DeniedResp.DenialReasonCode = inputParams[6];
                        this.DeniedResp.DenialReason = inputParams[7];
                    }
                    else if (CS$4$0000 == "ApprovedWithChanges")
                    {
                        this.ApprovedWithChangesResp = new ApprovedWithChanges();
                        this.ApprovedWithChangesResp.note = inputParams[7];
                    }
                    else if (CS$4$0000 == "DeniedNewPrescriptionToFollow")
                    {
                        this.DeniedNewPrescripResp = new DeniedNewPrescriptionToFollow();
                        this.DeniedNewPrescripResp.note = inputParams[7];
                    }
                }
                else
                {
                    this.ApprovedResp = new Approved();
                    this.ApprovedResp.Note = inputParams[7];
                }
            }
        }
    }
}

