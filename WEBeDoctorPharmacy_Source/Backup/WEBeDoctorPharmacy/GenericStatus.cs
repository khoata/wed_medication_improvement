namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class GenericStatus
    {
        [XmlElement("Code")]
        public string Code;

        public GenericStatus()
        {
        }

        public GenericStatus(string InputCode)
        {
            this.Code = InputCode;
        }
    }
}

