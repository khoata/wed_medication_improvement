namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class DirectoryDownloadResponse
    {
        [XmlElement("URL")]
        public string URL;
    }
}

