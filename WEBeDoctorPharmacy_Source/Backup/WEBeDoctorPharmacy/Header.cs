namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml.Serialization;

    public class Header
    {
        [XmlElement("From")]
        public string From;
        private Guid MessageGuid;
        [XmlElement("MessageID")]
        public string MessageID;
        private WEBeDoctorPharmacy.Security.UserNameToken.Password mPassword;
        [XmlElement("RelatesToMessageID")]
        public string RelatesToMessageID;
        [XmlElement("Security")]
        public WEBeDoctorPharmacy.Security Security;
        [XmlElement("SentTime")]
        public string SentTime;
        [XmlElement("To")]
        public string To;
        private WEBeDoctorPharmacy.Security.UserNameToken UserNameToken;

        public Header()
        {
        }

        public Header(string MsgType)
        {
            this.To = "mailto:SSSDIR.dp@surescripts.com";
            this.From = "mailto:WEBDOC001.dp@surescripts.com";
            this.MessageGuid = Guid.NewGuid();
            this.MessageID = this.MessageGuid.ToString().Replace("-", "");
            this.SentTime = DateTime.Now.AddHours(7.0).ToString("yyyy-MM-ddTHH:mm:ss.0Z");
            if ((((MsgType == "DirDwnld") || (MsgType == "AddPrescriber")) || (MsgType == "AddPrescriberResp")) || (MsgType == "UpdatePrescriber"))
            {
                this.From = "mailto:WEBDOC.dp@surescripts.com";
                this.Security = new WEBeDoctorPharmacy.Security();
                this.UserNameToken = new WEBeDoctorPharmacy.Security.UserNameToken();
                this.UserNameToken.mUsername = ConfigurationManager.AppSettings["SSLogin"].ToString();
                this.mPassword = new WEBeDoctorPharmacy.Security.UserNameToken.Password();
                SHA1CryptoServiceProvider cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
                this.mPassword.mmPassword = Convert.ToBase64String(cryptoTransformSHA1.ComputeHash(Encoding.Unicode.GetBytes(ConfigurationManager.AppSettings["SSPassword"].ToString().ToUpper().ToString())));
                this.UserNameToken.mPassword = this.mPassword;
                this.UserNameToken.mNonce = "1";
                this.UserNameToken.mCreated = this.SentTime;
                this.Security.mUsernameToken = this.UserNameToken;
            }
        }
    }
}

