namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;

    public class PharmacyInfo
    {
        [XmlElement("Address")]
        public Address mPharmacyAddress;
        [XmlElement("Email")]
        public string mPharmacyEmail;
        [XmlElement("Identification")]
        public ID mPharmacyID;
        [XmlElement("PhoneNumbers")]
        public PhoneNumbers mPharmacyPhone;
        private Phone mPhone;
        [XmlElement("StoreName")]
        public string mStoreName;

        public PharmacyInfo()
        {
        }

        public PharmacyInfo(string vPharmacyID)
        {
            this.Load(vPharmacyID);
        }

        public void Load(string PharmacyID)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPharmacyInfo";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPharmacyID", OracleType.VarChar)).Value = PharmacyID;
            command.Parameters.Add(new OracleParameter("CurPharmacyInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPharmacyID = new ID();
                this.mPharmacyID.NCPDPID = (string) reader.GetOracleString(0);
                this.mStoreName = (string) reader.GetOracleString(1);
                this.mPharmacyAddress = new Address();
                this.mPharmacyAddress.Address1 = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mPharmacyAddress.City = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPharmacyAddress.State = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mPharmacyAddress.Zip = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.mPharmacyEmail = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));
                if (!reader.GetOracleString(7).IsNull)
                {
                    this.mPharmacyPhone = new PhoneNumbers();
                    this.mPhone = new Phone();
                    this.mPhone.PhoneNumber = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                    this.mPhone.Qualifier = reader.GetOracleString(8).IsNull ? null : ((string) reader.GetOracleString(8));
                    this.mPharmacyPhone.mPhone = this.mPhone;
                }
            }
            reader.Close();
            connection.Close();
        }
    }
}

