namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;

    [XmlRoot("Message", Namespace="http://www.surescripts.com/messaging", IsNullable=false)]
    public class Message
    {
        private string DrugID;
        private string MessageType;
        [XmlElement("Body")]
        public Body msgBody;
        [XmlElement("Header")]
        public Header msgHeader;
        [XmlAttribute("version")]
        public string mVersion;

        public Message()
        {
        }

        public Message(string MsgType, string[] InputParams)
        {
            this.mVersion = "1.5";
            this.MessageType = MsgType;
            this.DrugID = InputParams[3] ?? "";
            this.msgHeader = new Header(MsgType);
            this.msgBody = new Body(MsgType, InputParams);
        }

        public void ConstructHeaderFields()
        {
            if (this.msgBody.NewRq != null)
            {
                this.msgHeader.To = "mailto:" + this.msgBody.NewRq.mPharmacy.mPharmacyID.NCPDPID + ".ncpdp@surescripts.com";
                this.msgHeader.From = "mailto:" + this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI + ".spi@surescripts.com";
            }
            else if (this.msgBody.RefResp != null)
            {
                this.msgHeader.To = "mailto:" + this.msgBody.RefResp.mPharmacy.mPharmacyID.NCPDPID + ".ncpdp@surescripts.com";
                this.msgHeader.From = "mailto:" + this.msgBody.RefResp.mPrescriber.mPrescriberID.SPI + ".spi@surescripts.com";
            }
        }

        public void LoadCorrelatedMessageInfo(string msgType, string[] InputParams)
        {
            if ((msgType == "RefillResponse") || (msgType == "NewRxFollowup"))
            {
                OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                OracleCommand command = new OracleCommand();
                connection.Open();
                command.Connection = connection;
                command.CommandText = "WSSInterface.LoadCorrelatedMessage";
                command.CommandType = CommandType.StoredProcedure;
                if (msgType == "RefillResponse")
                {
                    command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = InputParams[8];
                }
                else
                {
                    command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = InputParams[5];
                }
                command.Parameters.Add(new OracleParameter("CurCorrelatedMessageInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (msgType == "RefillResponse")
                    {
                        this.msgBody.RefResp.RXReferenceNumber = reader.GetOracleString(0).IsNull ? null : reader.GetOracleString(0).ToString();
                        this.msgBody.RefResp.PrescriberOrderNumber = reader.GetOracleString(1).IsNull ? null : reader.GetOracleString(1).ToString();
                        this.msgHeader.RelatesToMessageID = reader.GetOracleString(2).IsNull ? null : reader.GetOracleString(2).ToString();
                        string temp = reader.GetOracleString(3).IsNull ? null : reader.GetOracleString(3).ToString();
                        if ((temp != null) && (temp == "7"))
                        {
                            this.msgBody.RefResp.mMedication.Substitutions = "1";
                        }
                        else
                        {
                            this.msgBody.RefResp.mMedication.Substitutions = "0";
                        }
                    }
                    else if (msgType == "NewRxFollowup")
                    {
                        this.msgBody.NewRq.RXReferenceNumber = reader.GetOracleString(0).IsNull ? null : reader.GetOracleString(0).ToString();
                        this.msgBody.NewRq.PrescriberOrderNumber = reader.GetOracleString(1).IsNull ? null : reader.GetOracleString(1).ToString();
                        this.msgHeader.RelatesToMessageID = reader.GetOracleString(2).IsNull ? null : reader.GetOracleString(2).ToString();
                    }
                }
                reader.Close();
                connection.Close();
            }
        }

        public string Save(string InternalMessageID)
        {
            string OrderNumber = "";
            string RXReferenceNumber = "";
            string PharmacyID = "";
            string PrescriberID = "";
            string MessageType = "";
            string ResponseCode = "";
            string RelatedMessageId = "";
            string PharmacyName = "";
            string PharmacyAddress = "";
            string PharmacyCity = "";
            string PharmacyState = "";
            string PharmacyZip = "";
            string PharmacyPhone = "";
            string PharmacyPhoneType = "";
            string PrescriberClinicName = "";
            string PrescriberSPI = "";
            string PrescriberSpecialtyQual = "";
            string PrescriberSpecialtyCode = "";
            string PrescriberAgentLastName = "";
            string PrescriberAgentFirstName = "";
            string PrescriberAddress = "";
            string PrescriberCity = "";
            string PrescriberState = "";
            string PrescriberZip = "";
            string PrescriberEmail = "";
            string PrescriberPhone = "";
            string PrescriberPhoneType = "";
            string PatientID = "";
            string PatientSSN = "";
            string PatientLastName = "";
            string PatientFirstName = "";
            string PatientPreFix = "";
            string PatientGender = "";
            string PatientDOB = "";
            string PatientAddress = "";
            string PatientCity = "";
            string PatientState = "";
            string PatientZip = "";
            string PatientEmail = "";
            string PatientPhone = "";
            string PatientPhoneType = "";
            string MedicationDescription = "";
            string MedicationCode = "";
            string MedicationCodeType = "";
            string MedicationQuantityQual = "";
            string MedicationQuantity = "";
            string MedicationDirections = "";
            string MedicationNote = "";
            string MedicationRefillQual = "";
            string MedicationSubstitution = "";
            string MedicationWrittenDate = "";
            string MsgContent = "";
            string ResponseNote = "";
            string MedicationRefillQuan = "";
            string PrescriberServiceLevel = "";
            if (this.msgBody.NewRq != null)
            {
                MessageType = "NewRX";
                OrderNumber = this.msgBody.NewRq.PrescriberOrderNumber;
                PharmacyID = this.msgBody.NewRq.mPharmacy.mPharmacyID.NCPDPID;
                if (this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI != null)
                {
                    PrescriberID = this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI;
                }
                RelatedMessageId = this.msgHeader.RelatesToMessageID;
                PharmacyName = this.msgBody.NewRq.mPharmacy.mStoreName;
                PharmacyAddress = this.msgBody.NewRq.mPharmacy.mPharmacyAddress.Address1;
                PharmacyCity = this.msgBody.NewRq.mPharmacy.mPharmacyAddress.City;
                if (this.msgBody.NewRq.RXReferenceNumber != null)
                {
                    RXReferenceNumber = this.msgBody.NewRq.RXReferenceNumber;
                }
                PharmacyState = this.msgBody.NewRq.mPharmacy.mPharmacyAddress.State;
                PharmacyZip = this.msgBody.NewRq.mPharmacy.mPharmacyAddress.Zip;
                PharmacyPhone = this.msgBody.NewRq.mPharmacy.mPharmacyPhone.mPhone.PhoneNumber;
                PharmacyPhoneType = this.msgBody.NewRq.mPharmacy.mPharmacyPhone.mPhone.Qualifier;
                PrescriberClinicName = this.msgBody.NewRq.mPrescriber.mClinicName;
                if (this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI != null)
                {
                    PrescriberSPI = this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI;
                }
                if (this.msgBody.NewRq.mPrescriber.mSpecialty.Qualifier != null)
                {
                    PrescriberSpecialtyQual = this.msgBody.NewRq.mPrescriber.mSpecialty.Qualifier;
                    PrescriberSpecialtyCode = this.msgBody.NewRq.mPrescriber.mSpecialty.SpecialtyCode;
                }
                if ((this.msgBody.NewRq.mPrescriber.mPrescriberAgent != null) && (this.msgBody.NewRq.mPrescriber.mPrescriberAgent.LastName != null))
                {
                    PrescriberAgentLastName = this.msgBody.NewRq.mPrescriber.mPrescriberAgent.LastName;
                    PrescriberAgentFirstName = this.msgBody.NewRq.mPrescriber.mPrescriberAgent.FirstName;
                }
                PrescriberAddress = this.msgBody.NewRq.mPrescriber.mPrescriberAddress.Address1;
                PrescriberCity = this.msgBody.NewRq.mPrescriber.mPrescriberAddress.City;
                PrescriberState = this.msgBody.NewRq.mPrescriber.mPrescriberAddress.State;
                PrescriberZip = this.msgBody.NewRq.mPrescriber.mPrescriberAddress.Zip;
                if (this.msgBody.NewRq.mPrescriber.mPrescriberEmail != null)
                {
                    PrescriberEmail = this.msgBody.NewRq.mPrescriber.mPrescriberEmail;
                }
                PrescriberPhone = this.msgBody.NewRq.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber;
                PrescriberPhoneType = this.msgBody.NewRq.mPrescriber.mPrescriberPhone.mPhone[0].Qualifier;
                PatientID = this.msgBody.NewRq.mPatient.mPatientID.FileID;
                if (this.msgBody.NewRq.mPatient.mPatientID.SocialSecurityNumber != null)
                {
                    PatientSSN = this.msgBody.NewRq.mPatient.mPatientID.SocialSecurityNumber;
                }
                PatientLastName = this.msgBody.NewRq.mPatient.mPatientName.LastName;
                PatientFirstName = this.msgBody.NewRq.mPatient.mPatientName.FirstName;
                if (this.msgBody.NewRq.mPatient.mPatientName.Prefix != null)
                {
                    PatientPreFix = this.msgBody.NewRq.mPatient.mPatientName.Prefix;
                }
                PatientGender = this.msgBody.NewRq.mPatient.mGender;
                PatientDOB = this.msgBody.NewRq.mPatient.mDateOfBirth;
                PatientAddress = this.msgBody.NewRq.mPatient.mPatientAddress.Address1;
                PatientCity = this.msgBody.NewRq.mPatient.mPatientAddress.City;
                PatientState = this.msgBody.NewRq.mPatient.mPatientAddress.State;
                PatientZip = this.msgBody.NewRq.mPatient.mPatientAddress.Zip;
                if (this.msgBody.NewRq.mPatient.mEmail != null)
                {
                    PatientEmail = this.msgBody.NewRq.mPatient.mEmail;
                }
                if ((this.msgBody.NewRq.mPatient.mPatientPhone != null) && (this.msgBody.NewRq.mPatient.mPatientPhone.mPhone != null))
                {
                    PatientPhone = this.msgBody.NewRq.mPatient.mPatientPhone.mPhone.PhoneNumber;
                    PatientPhoneType = this.msgBody.NewRq.mPatient.mPatientPhone.mPhone.Qualifier;
                }
                MedicationDescription = this.msgBody.NewRq.mMedication.DrugDescription;
                if ((this.msgBody.NewRq.mMedication.DrugCode != null) && (this.msgBody.NewRq.mMedication.DrugCode.ProductCode != null))
                {
                    MedicationCode = this.msgBody.NewRq.mMedication.DrugCode.ProductCode;
                    MedicationCodeType = "ND";
                }
                if (this.msgBody.NewRq.mMedication.Quantity != null)
                {
                    MedicationQuantityQual = this.msgBody.NewRq.mMedication.Quantity.Qualifier;
                    MedicationQuantity = this.msgBody.NewRq.mMedication.Quantity.Value;
                }
                if (this.msgBody.NewRq.mMedication.Directions != null)
                {
                    MedicationDirections = this.msgBody.NewRq.mMedication.Directions;
                }
                if (this.msgBody.NewRq.mMedication.Note != null)
                {
                    MedicationNote = this.msgBody.NewRq.mMedication.Note;
                }
                MedicationRefillQual = this.msgBody.NewRq.mMedication.Refills.Qualifier;
                if (this.msgBody.NewRq.mMedication.Refills.Qualifier != "PRN")
                {
                    MedicationRefillQuan = this.msgBody.NewRq.mMedication.Refills.Quantity;
                }
                MedicationSubstitution = this.msgBody.NewRq.mMedication.Substitutions;
                MedicationWrittenDate = this.msgBody.NewRq.mMedication.WrittenDate;
            }
            else if (this.msgBody.RefReq != null)
            {
                MessageType = "RefillRequest";
                OrderNumber = this.msgBody.RefReq.PrescriberOrderNumber;
                RXReferenceNumber = this.msgBody.RefReq.RXReferenceNumber;
                PharmacyID = this.msgBody.RefReq.mPharmacy.mPharmacyID.NCPDPID;
                PrescriberID = this.msgBody.RefReq.mPrescriber.mPrescriberID.SPI;
                this.DrugID = "";
                PharmacyName = this.msgBody.RefReq.mPharmacy.mStoreName;
                if (this.msgBody.RefReq.mPharmacy.mPharmacyAddress != null)
                {
                    PharmacyAddress = this.msgBody.RefReq.mPharmacy.mPharmacyAddress.Address1;
                    PharmacyCity = this.msgBody.RefReq.mPharmacy.mPharmacyAddress.City;
                    PharmacyState = this.msgBody.RefReq.mPharmacy.mPharmacyAddress.State;
                    PharmacyZip = this.msgBody.RefReq.mPharmacy.mPharmacyAddress.Zip;
                }
                if ((this.msgBody.RefReq.mPharmacy.mPharmacyPhone != null) && (this.msgBody.RefReq.mPharmacy.mPharmacyPhone.mPhone != null))
                {
                    PharmacyPhone = this.msgBody.RefReq.mPharmacy.mPharmacyPhone.mPhone.PhoneNumber;
                    PharmacyPhoneType = this.msgBody.RefReq.mPharmacy.mPharmacyPhone.mPhone.Qualifier;
                }
                if (this.msgBody.RefReq.mPrescriber.mClinicName != null)
                {
                    PrescriberClinicName = this.msgBody.RefReq.mPrescriber.mClinicName;
                }
                PrescriberSPI = this.msgBody.RefReq.mPrescriber.mPrescriberID.SPI;
                if (this.msgBody.RefReq.mPrescriber.mSpecialty.Qualifier != null)
                {
                    PrescriberSpecialtyQual = this.msgBody.RefReq.mPrescriber.mSpecialty.Qualifier;
                    PrescriberSpecialtyCode = this.msgBody.RefReq.mPrescriber.mSpecialty.SpecialtyCode;
                }
                if (this.msgBody.RefReq.mPrescriber.mPrescriberAgent.LastName != null)
                {
                    PrescriberAgentLastName = this.msgBody.RefReq.mPrescriber.mPrescriberAgent.LastName;
                    PrescriberAgentFirstName = this.msgBody.RefReq.mPrescriber.mPrescriberAgent.FirstName;
                }
                if (this.msgBody.RefReq.mPrescriber.mPrescriberAddress != null)
                {
                    PrescriberAddress = this.msgBody.RefReq.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.RefReq.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.RefReq.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.RefReq.mPrescriber.mPrescriberAddress.Zip;
                }
                if (this.msgBody.RefReq.mPrescriber.mPrescriberEmail != null)
                {
                    PrescriberEmail = this.msgBody.RefReq.mPrescriber.mPrescriberEmail;
                }
                if ((this.msgBody.RefReq.mPrescriber.mPrescriberPhone != null) && (this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone != null))
                {
                    PrescriberPhone = this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType = this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone[0].Qualifier;
                }
                if (this.msgBody.RefReq.mPatient.mPatientID != null)
                {
                    if (this.msgBody.RefReq.mPatient.mPatientID.FileID != null)
                    {
                        PatientID = this.msgBody.RefReq.mPatient.mPatientID.FileID;
                    }
                    if (this.msgBody.RefReq.mPatient.mPatientID.SocialSecurityNumber != null)
                    {
                        PatientSSN = this.msgBody.RefReq.mPatient.mPatientID.SocialSecurityNumber;
                    }
                }
                PatientLastName = this.msgBody.RefReq.mPatient.mPatientName.LastName;
                PatientFirstName = this.msgBody.RefReq.mPatient.mPatientName.FirstName;
                if (this.msgBody.RefReq.mPatient.mPatientName.Prefix != null)
                {
                    PatientPreFix = this.msgBody.RefReq.mPatient.mPatientName.Prefix;
                }
                PatientGender = this.msgBody.RefReq.mPatient.mGender;
                PatientDOB = this.msgBody.RefReq.mPatient.mDateOfBirth;
                if (this.msgBody.RefReq.mPatient.mPatientAddress != null)
                {
                    PatientAddress = this.msgBody.RefReq.mPatient.mPatientAddress.Address1;
                    PatientCity = this.msgBody.RefReq.mPatient.mPatientAddress.City;
                    PatientState = this.msgBody.RefReq.mPatient.mPatientAddress.State;
                    PatientZip = this.msgBody.RefReq.mPatient.mPatientAddress.Zip;
                }
                if (this.msgBody.RefReq.mPatient.mEmail != null)
                {
                    PatientEmail = this.msgBody.RefReq.mPatient.mEmail;
                }
                if ((this.msgBody.RefReq.mPatient.mPatientPhone != null) && (this.msgBody.RefReq.mPatient.mPatientPhone.mPhone != null))
                {
                    PatientPhone = this.msgBody.RefReq.mPatient.mPatientPhone.mPhone.PhoneNumber;
                    PatientPhoneType = this.msgBody.RefReq.mPatient.mPatientPhone.mPhone.Qualifier;
                }
                MedicationDescription = this.msgBody.RefReq.mMedication.DrugDescription;
                if ((this.msgBody.RefReq.mMedication.DrugCode != null) && (this.msgBody.RefReq.mMedication.DrugCode.ProductCode != null))
                {
                    MedicationCode = this.msgBody.RefReq.mMedication.DrugCode.ProductCode;
                    MedicationCodeType = "ND";
                }
                if (this.msgBody.RefReq.mMedication.Quantity != null)
                {
                    MedicationQuantityQual = this.msgBody.RefReq.mMedication.Quantity.Qualifier;
                    MedicationQuantity = this.msgBody.RefReq.mMedication.Quantity.Value;
                }
                MedicationDirections = this.msgBody.RefReq.mMedication.Directions;
                if (this.msgBody.RefReq.mMedication.Note != null)
                {
                    MedicationNote = this.msgBody.RefReq.mMedication.Note;
                }
                MedicationRefillQual = this.msgBody.RefReq.mMedication.Refills.Qualifier;
                if (this.msgBody.RefReq.mMedication.Refills.Qualifier != "PRN")
                {
                    MedicationRefillQuan = this.msgBody.RefReq.mMedication.Refills.Quantity;
                }
                MedicationSubstitution = this.msgBody.RefReq.mMedication.Substitutions;
                MedicationWrittenDate = this.msgBody.RefReq.mMedication.WrittenDate;
            }
            else if (this.msgBody.RefResp != null)
            {
                MessageType = "RefillResponse";
                OrderNumber = this.msgBody.RefResp.PrescriberOrderNumber;
                RXReferenceNumber = this.msgBody.RefResp.RXReferenceNumber;
                PharmacyID = this.msgBody.RefResp.mPharmacy.mPharmacyID.NCPDPID;
                PrescriberID = this.msgBody.RefResp.mPrescriber.mPrescriberID.SPI;
                RelatedMessageId = this.msgHeader.RelatesToMessageID;
                if (this.msgBody.RefResp.ResponseMsg.ApprovedResp != null)
                {
                    ResponseCode = "Approved";
                }
                else if (this.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp != null)
                {
                    ResponseCode = "ApprovedWithChanges";
                }
                else if (this.msgBody.RefResp.ResponseMsg.DeniedResp != null)
                {
                    ResponseCode = "Denied";
                }
                else if (this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp != null)
                {
                    ResponseCode = "DeniedNewPrescriptionToFollow";
                }
                PharmacyName = this.msgBody.RefResp.mPharmacy.mStoreName;
                if (this.msgBody.RefResp.mPharmacy.mPharmacyAddress != null)
                {
                    PharmacyAddress = this.msgBody.RefResp.mPharmacy.mPharmacyAddress.Address1;
                    PharmacyCity = this.msgBody.RefResp.mPharmacy.mPharmacyAddress.City;
                    PharmacyState = this.msgBody.RefResp.mPharmacy.mPharmacyAddress.State;
                    PharmacyZip = this.msgBody.RefResp.mPharmacy.mPharmacyAddress.Zip;
                }
                if (this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone != null)
                {
                    PharmacyPhone = this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone.PhoneNumber;
                    PharmacyPhoneType = this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone.Qualifier;
                }
                PrescriberClinicName = this.msgBody.RefResp.mPrescriber.mClinicName;
                PrescriberSPI = this.msgBody.RefResp.mPrescriber.mPrescriberID.SPI;
                if ((this.msgBody.RefResp.mPrescriber.mSpecialty != null) && (this.msgBody.RefResp.mPrescriber.mSpecialty.Qualifier != null))
                {
                    PrescriberSpecialtyQual = this.msgBody.RefResp.mPrescriber.mSpecialty.Qualifier;
                    PrescriberSpecialtyCode = this.msgBody.RefResp.mPrescriber.mSpecialty.SpecialtyCode;
                }
                if ((this.msgBody.RefResp.mPrescriber.mPrescriberAgent != null) && (this.msgBody.RefResp.mPrescriber.mPrescriberAgent.LastName != null))
                {
                    PrescriberAgentLastName = this.msgBody.RefResp.mPrescriber.mPrescriberAgent.LastName;
                    PrescriberAgentFirstName = this.msgBody.RefResp.mPrescriber.mPrescriberAgent.FirstName;
                }
                if (this.msgBody.RefResp.mPrescriber.mPrescriberAddress != null)
                {
                    PrescriberAddress = this.msgBody.RefResp.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.RefResp.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.RefResp.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.RefResp.mPrescriber.mPrescriberAddress.Zip;
                }
                if (this.msgBody.RefResp.mPrescriber.mPrescriberEmail != null)
                {
                    PrescriberEmail = this.msgBody.RefResp.mPrescriber.mPrescriberEmail;
                }
                if (this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone != null)
                {
                    PrescriberPhone = this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType = this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone[0].Qualifier;
                }
                if ((this.msgBody.RefResp.mPatient.mPatientID != null) && (this.msgBody.RefResp.mPatient.mPatientID.FileID != null))
                {
                    PatientID = this.msgBody.RefResp.mPatient.mPatientID.FileID;
                    if (this.msgBody.RefResp.mPatient.mPatientID.SocialSecurityNumber != null)
                    {
                        PatientSSN = this.msgBody.RefResp.mPatient.mPatientID.SocialSecurityNumber;
                    }
                }
                if (this.msgBody.RefResp.mPatient.mPatientName != null)
                {
                    if (this.msgBody.RefResp.mPatient.mPatientName.LastName != null)
                    {
                        PatientLastName = this.msgBody.RefResp.mPatient.mPatientName.LastName;
                    }
                    if (this.msgBody.RefResp.mPatient.mPatientName.FirstName != null)
                    {
                        PatientFirstName = this.msgBody.RefResp.mPatient.mPatientName.FirstName;
                    }
                    if (this.msgBody.RefResp.mPatient.mPatientName.Prefix != null)
                    {
                        PatientPreFix = this.msgBody.RefResp.mPatient.mPatientName.Prefix;
                    }
                }
                PatientGender = this.msgBody.RefResp.mPatient.mGender ?? "";
                PatientDOB = this.msgBody.RefResp.mPatient.mDateOfBirth ?? "";
                if (this.msgBody.RefResp.mPatient.mPatientAddress != null)
                {
                    PatientAddress = this.msgBody.RefResp.mPatient.mPatientAddress.Address1 ?? "";
                    PatientCity = this.msgBody.RefResp.mPatient.mPatientAddress.City ?? "";
                    PatientState = this.msgBody.RefResp.mPatient.mPatientAddress.State ?? "";
                    PatientZip = this.msgBody.RefResp.mPatient.mPatientAddress.Zip ?? "";
                }
                if (this.msgBody.RefResp.mPatient.mEmail != null)
                {
                    PatientEmail = this.msgBody.RefResp.mPatient.mEmail;
                }
                if ((this.msgBody.RefResp.mPatient.mPatientPhone != null) && (this.msgBody.RefResp.mPatient.mPatientPhone.mPhone != null))
                {
                    PatientPhone = this.msgBody.RefResp.mPatient.mPatientPhone.mPhone.PhoneNumber ?? "";
                    PatientPhoneType = this.msgBody.RefResp.mPatient.mPatientPhone.mPhone.Qualifier ?? "";
                }
                MedicationDescription = this.msgBody.RefResp.mMedication.DrugDescription;
                if ((this.msgBody.RefResp.mMedication.DrugCode != null) && (this.msgBody.RefResp.mMedication.DrugCode.ProductCode != null))
                {
                    MedicationCode = this.msgBody.RefResp.mMedication.DrugCode.ProductCode;
                    MedicationCodeType = "ND";
                }
                if (this.msgBody.RefResp.mMedication.Quantity != null)
                {
                    MedicationQuantityQual = this.msgBody.RefResp.mMedication.Quantity.Qualifier;
                    MedicationQuantity = this.msgBody.RefResp.mMedication.Quantity.Value;
                }
                MedicationDirections = this.msgBody.RefResp.mMedication.Directions;
                if (this.msgBody.RefResp.mMedication.Note != null)
                {
                    MedicationNote = this.msgBody.RefResp.mMedication.Note;
                }
                if (this.msgBody.RefResp.mMedication.Refills != null)
                {
                    MedicationRefillQual = this.msgBody.RefResp.mMedication.Refills.Qualifier;
                    if (this.msgBody.RefResp.mMedication.Refills.Qualifier != "PRN")
                    {
                        MedicationRefillQuan = this.msgBody.RefResp.mMedication.Refills.Quantity;
                    }
                }
                MedicationSubstitution = this.msgBody.RefResp.mMedication.Substitutions;
                MedicationWrittenDate = this.msgBody.RefResp.mMedication.WrittenDate;
                if ((this.msgBody.RefResp.ResponseMsg.ApprovedResp != null) && (this.msgBody.RefResp.ResponseMsg.ApprovedResp.Note != null))
                {
                    ResponseNote = this.msgBody.RefResp.ResponseMsg.ApprovedResp.Note.ToString();
                }
                else if ((this.msgBody.RefResp.ResponseMsg.DeniedResp != null) && (this.msgBody.RefResp.ResponseMsg.DeniedResp.DenialReason != null))
                {
                    ResponseNote = this.msgBody.RefResp.ResponseMsg.DeniedResp.DenialReason.ToString();
                }
                else if ((this.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp != null) && (this.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp.note != null))
                {
                    ResponseNote = this.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp.note.ToString();
                }
                else if ((this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp != null) && (this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp.note != null))
                {
                    ResponseNote = this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp.note.ToString();
                }
            }
            else
            {
                if (this.msgBody.Status != null)
                {
                    if (this.msgBody.Status.Code == "010")
                    {
                        this.UpdateStatus(InternalMessageID, "OK", "Submission Successful Confirmed");
                    }
                    else if (this.msgBody.Status.Code == "000")
                    {
                        this.UpdateStatus(InternalMessageID, "OK", "Submitted To Next Receiver.");
                    }
                    else
                    {
                        this.UpdateStatus(InternalMessageID, "Error", this.msgBody.Status.Code.ToString());
                    }
                    return "";
                }
                if (this.msgBody.Error != null)
                {
                    this.UpdateStatus(InternalMessageID, "Error", this.msgBody.Error.DescriptionCode.ToString() + "-" + this.msgBody.Error.Description.ToString());
                    return "";
                }
                if (this.msgBody.DirDwnld != null)
                {
                    MessageType = "DirectoryDownload";
                }
                else if (this.msgBody.DirDwnldResponse != null)
                {
                    MessageType = "DirectoryDownloadResponse";
                    ResponseCode = "OK";
                    MsgContent = this.msgBody.DirDwnldResponse.URL.ToString();
                }
                else if (this.msgBody.AddPrescriber != null)
                {
                    MessageType = "AddPrescriber";
                    if (this.msgBody.AddPrescriber.mPrescriber.mSpecialty != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.AddPrescriber.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.AddPrescriber.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                    PrescriberAddress = this.msgBody.AddPrescriber.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.AddPrescriber.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.AddPrescriber.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.AddPrescriber.mPrescriber.mPrescriberAddress.Zip;
                    PrescriberEmail = this.msgBody.AddPrescriber.mPrescriber.mPrescriberEmail;
                    PrescriberPhone = this.msgBody.AddPrescriber.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType = this.msgBody.AddPrescriber.mPrescriber.mPrescriberPhone.mPhone[0].Qualifier;
                    if ((this.msgBody.AddPrescriber.mPrescriber.mDirectoryInfo != null) && (this.msgBody.AddPrescriber.mPrescriber.mDirectoryInfo.ServiceLevel != null))
                    {
                        PrescriberServiceLevel = this.msgBody.AddPrescriber.mPrescriber.mDirectoryInfo.ServiceLevel;
                    }
                }
                else if (this.msgBody.UpdatePrescriber != null)
                {
                    MessageType = "UpdatePrescriber";
                    if (this.msgBody.UpdatePrescriber.mPrescriber.mSpecialty != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.UpdatePrescriber.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.UpdatePrescriber.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                    PrescriberAddress = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberAddress.Zip;
                    PrescriberEmail = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberEmail;
                    PrescriberPhone = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberPhone.mPhone[0].Qualifier;
                    if ((this.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo != null) && (this.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel != null))
                    {
                        PrescriberServiceLevel = this.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel;
                    }
                }
                else if (this.msgBody.AddPrescriberResp != null)
                {
                    MessageType = "AddPrescriberResp";
                    if (this.msgBody.AddPrescriberResp.mPrescriber.mSpecialty != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.AddPrescriberResp.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.AddPrescriberResp.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                    PrescriberAgentLastName = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAgent.LastName;
                    PrescriberAgentFirstName = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAgent.FirstName;
                    PrescriberAddress = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAddress.Zip;
                    PrescriberEmail = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberEmail;
                    PrescriberPhone = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberPhone.mPhone[0].Qualifier;
                    if ((this.msgBody.AddPrescriberResp.mPrescriber.mDirectoryInfo != null) && (this.msgBody.AddPrescriberResp.mPrescriber.mDirectoryInfo.ServiceLevel != null))
                    {
                        PrescriberServiceLevel = this.msgBody.AddPrescriberResp.mPrescriber.mDirectoryInfo.ServiceLevel;
                    }
                }
            }
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.SaveMessage";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = InternalMessageID ?? "";
            command.Parameters.Add(new OracleParameter("ParamPRESCRIBERORDERNUMBER", OracleType.VarChar)).Value = OrderNumber ?? "";
            command.Parameters.Add(new OracleParameter("ParamRXREFERENCENUMBER", OracleType.VarChar)).Value = RXReferenceNumber ?? "";
            command.Parameters.Add(new OracleParameter("ParamRelatedMessageId", OracleType.VarChar)).Value = RelatedMessageId ?? "";
            command.Parameters.Add(new OracleParameter("ParamPHARMACYID", OracleType.VarChar)).Value = PharmacyID ?? "";
            command.Parameters.Add(new OracleParameter("ParamPRESCRIBERID", OracleType.VarChar)).Value = PrescriberID ?? "";
            command.Parameters.Add(new OracleParameter("ParamSUPERVISORID", OracleType.VarChar)).Value = "";
            command.Parameters.Add(new OracleParameter("ParamDRUGID", OracleType.VarChar)).Value = this.DrugID ?? "";
            command.Parameters.Add(new OracleParameter("ParamMSGCONTENT", OracleType.VarChar)).Value = MsgContent ?? "";
            command.Parameters.Add(new OracleParameter("ParamMESSAGETYPE", OracleType.VarChar)).Value = MessageType ?? "";
            command.Parameters.Add(new OracleParameter("ParamRESPONSECODE", OracleType.VarChar)).Value = ResponseCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyName", OracleType.VarChar)).Value = PharmacyName.ToString().Replace("'", "") ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyAddress", OracleType.VarChar)).Value = PharmacyAddress ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyCity", OracleType.VarChar)).Value = PharmacyCity ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyState", OracleType.VarChar)).Value = PharmacyState ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyZip", OracleType.VarChar)).Value = PharmacyZip ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone", OracleType.VarChar)).Value = PharmacyPhone ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType", OracleType.VarChar)).Value = PharmacyPhoneType ?? "";
            command.Parameters.Add(new OracleParameter("paramPrescriberClinicName", OracleType.VarChar)).Value = PrescriberClinicName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberSpecialtyQual", OracleType.VarChar)).Value = PrescriberSpecialtyQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberSpecialtyCode", OracleType.VarChar)).Value = PrescriberSpecialtyCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberAgentLastName", OracleType.VarChar)).Value = PrescriberAgentLastName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberAgentFirstName", OracleType.VarChar)).Value = PrescriberAgentFirstName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberAddress", OracleType.VarChar)).Value = PrescriberAddress ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberCity", OracleType.VarChar)).Value = PrescriberCity ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberState", OracleType.VarChar)).Value = PrescriberState ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberZip", OracleType.VarChar)).Value = PrescriberZip ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberEmail", OracleType.VarChar)).Value = PrescriberEmail ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone", OracleType.VarChar)).Value = PrescriberPhone ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType", OracleType.VarChar)).Value = PrescriberPhoneType ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientID", OracleType.VarChar)).Value = PatientID ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientSSN", OracleType.VarChar)).Value = PatientSSN ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientLastName", OracleType.VarChar)).Value = PatientLastName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientFirstName", OracleType.VarChar)).Value = PatientFirstName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPreFix", OracleType.VarChar)).Value = PatientPreFix ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientGender", OracleType.VarChar)).Value = PatientGender ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientDOB", OracleType.VarChar)).Value = PatientDOB ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientAddress", OracleType.VarChar)).Value = PatientAddress ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientCity", OracleType.VarChar)).Value = PatientCity ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientState", OracleType.VarChar)).Value = PatientState ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientZip", OracleType.VarChar)).Value = PatientZip ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientEmail", OracleType.VarChar)).Value = PatientEmail ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone", OracleType.VarChar)).Value = PatientPhone ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType", OracleType.VarChar)).Value = PatientPhoneType ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDescription", OracleType.VarChar)).Value = MedicationDescription ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationCode", OracleType.VarChar)).Value = MedicationCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationCodeType", OracleType.VarChar)).Value = MedicationCodeType ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationQuantityQual", OracleType.VarChar)).Value = MedicationQuantityQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationQuantity", OracleType.VarChar)).Value = MedicationQuantity ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDirections", OracleType.VarChar)).Value = MedicationDirections ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationNote", OracleType.VarChar)).Value = MedicationNote ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationRefillQual", OracleType.VarChar)).Value = MedicationRefillQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationRefillQuan", OracleType.VarChar)).Value = MedicationRefillQuan ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationSubstitution", OracleType.VarChar)).Value = MedicationSubstitution ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationWrittenDate", OracleType.VarChar)).Value = MedicationWrittenDate ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberServiceLevel", OracleType.VarChar)).Value = PrescriberServiceLevel ?? "";
            command.Parameters.Add(new OracleParameter("ParamSenderMessageID", OracleType.VarChar)).Value = this.msgHeader.MessageID ?? "";
            command.Parameters.Add(new OracleParameter("ParamNote", OracleType.VarChar)).Value = ResponseNote ?? "";
            command.Parameters.Add(new OracleParameter("ParamSaveFlag", OracleType.VarChar, 40, string.Empty)).Direction = ParameterDirection.Output;
            SS_Logger.log("=====Message Save() Parms =====");
            foreach (OracleParameter parm in command.Parameters)
            {
                SS_Logger.log(parm.ParameterName + " " + parm.Value);
            }
            SS_Logger.log("=====End Message Save() Parms =====");
            command.ExecuteNonQuery();
            connection.Close();
            return command.Parameters["ParamSaveFlag"].Value.ToString();
        }

        public void UpdatePrescriberSPI(string DEANumber, string SPI, string serviceLevel)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.SavePrescriberInfo";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamDEA_NO", OracleType.VarChar)).Value = DEANumber;
            command.Parameters.Add(new OracleParameter("ParamSPI", OracleType.VarChar)).Value = SPI;
            command.Parameters.Add(new OracleParameter("ParamServiceLevel", OracleType.VarChar)).Value = serviceLevel;
            command.ExecuteOracleScalar();
            connection.Close();
        }

        public void UpdateStatus(string InternalMessageID, string StatusCode, string StatusDesc)
        {
            string MessageID;
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            if (this.msgBody.RefReq != null)
            {
                MessageID = InternalMessageID;
            }
            else
            {
                MessageID = this.msgHeader.RelatesToMessageID.ToString();
            }
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.UpdateStatus";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = MessageID;
            command.Parameters.Add(new OracleParameter("ParamStatusCode", OracleType.VarChar)).Value = StatusCode;
            command.Parameters.Add(new OracleParameter("ParamStatusDesc", OracleType.VarChar)).Value = StatusDesc;
            command.ExecuteOracleScalar();
            connection.Close();
        }
    }
}

