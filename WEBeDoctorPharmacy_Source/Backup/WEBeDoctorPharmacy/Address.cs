namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Address
    {
        [XmlElement("AddressLine1")]
        public string Address1;
        [XmlElement("AddressLine2")]
        public string Address2;
        [XmlElement("City")]
        public string City;
        [XmlElement("State")]
        public string State;
        [XmlElement("ZipCode")]
        public string Zip;
    }
}

