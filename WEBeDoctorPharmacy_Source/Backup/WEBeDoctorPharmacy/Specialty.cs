namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Specialty
    {
        [XmlElement("Qualifier")]
        public string Qualifier;
        [XmlElement("SpecialtyCode")]
        public string SpecialtyCode;
    }
}

