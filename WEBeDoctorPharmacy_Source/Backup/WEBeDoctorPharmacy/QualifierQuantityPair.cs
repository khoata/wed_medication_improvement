namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class QualifierQuantityPair
    {
        [XmlElement("Qualifier")]
        public string Qualifier;
        [XmlElement("Quantity")]
        public string Quantity;
    }
}

