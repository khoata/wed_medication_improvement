namespace WEBeDoctorPharmacy
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;

    [XmlRoot("PatientInfo")]
    public class PatientInfo
    {
        protected bool loadedFromDB;
        [XmlElement("DateOfBirth")]
        public string mDateOfBirth;
        [XmlElement("Email")]
        public string mEmail;
        [XmlElement("Gender")]
        public string mGender;
        private Phone mHomePhone;
        [XmlElement("Address")]
        public Address mPatientAddress;
        [XmlElement("Identification")]
        public ID mPatientID;
        [XmlElement("Name")]
        public Name mPatientName;
        [XmlElement("PhoneNumbers")]
        public PhoneNumbers mPatientPhone;

        public PatientInfo()
        {
            this.loadedFromDB = false;
        }

        public PatientInfo(string vPatientID)
        {
            this.loadedFromDB = false;
            this.Load(vPatientID);
        }

        public PatientInfo(string dummyForOverload, string refillRequestMessageId)
        {
            this.loadedFromDB = false;
            this.LoadFromRefillRequest(refillRequestMessageId);
        }

        public void Load(string PatientID)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPatientInfo";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPatientID", OracleType.VarChar)).Value = PatientID;
            command.Parameters.Add(new OracleParameter("CurPatientInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPatientID = new ID();
                this.mPatientID.FileID = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                this.mPatientID.SocialSecurityNumber = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                this.mPatientName = new Name();
                this.mPatientName.FirstName = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mPatientName.LastName = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPatientName.MiddleName = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mPatientName.Prefix = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.mPatientName.Suffix = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));
                this.mGender = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                this.mDateOfBirth = DateTime.Parse(reader.GetOracleDateTime(8).ToString()).ToString("yyyyMMdd");
                this.mPatientAddress = new Address();
                this.mPatientAddress.Address1 = reader.GetOracleString(9).IsNull ? null : ((string) reader.GetOracleString(9));
                this.mPatientAddress.City = reader.GetOracleString(10).IsNull ? null : ((string) reader.GetOracleString(10));
                this.mPatientAddress.State = reader.GetOracleString(11).IsNull ? null : ((string) reader.GetOracleString(11));
                this.mPatientAddress.Zip = reader.GetOracleString(12).IsNull ? null : ((string) reader.GetOracleString(12));
                if (this.mPatientAddress.Zip != null)
                {
                    this.mPatientAddress.Zip = this.mPatientAddress.Zip.Replace("-", "");
                }
                this.mEmail = reader.GetOracleString(13).IsNull ? null : ((string) reader.GetOracleString(13));
                this.mPatientPhone = new PhoneNumbers();
                this.mHomePhone = new Phone();
                this.mHomePhone.PhoneNumber = reader.GetOracleString(14).IsNull ? null : ((string) reader.GetOracleString(14));
                if (this.mHomePhone.PhoneNumber != null)
                {
                    this.mHomePhone.PhoneNumber = this.mHomePhone.PhoneNumber.Replace("-", "");
                    this.mHomePhone.Qualifier = "TE";
                    this.mPatientPhone.mPhone = this.mHomePhone;
                }
                this.loadedFromDB = true;
            }
            reader.Close();
            connection.Close();
        }

        public void LoadFromRefillRequest(string PatientID)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPatientFromRefReq";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageId", OracleType.VarChar)).Value = PatientID;
            command.Parameters.Add(new OracleParameter("CurPatientInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPatientID = new ID();
                this.mPatientID.FileID = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                this.mPatientID.SocialSecurityNumber = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                this.mPatientName = new Name();
                this.mPatientName.FirstName = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mPatientName.LastName = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPatientName.Prefix = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mGender = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.mDateOfBirth = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));
                this.mPatientAddress = new Address();
                this.mPatientAddress.Address1 = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                this.mPatientAddress.City = reader.GetOracleString(8).IsNull ? null : ((string) reader.GetOracleString(8));
                this.mPatientAddress.State = reader.GetOracleString(9).IsNull ? null : ((string) reader.GetOracleString(9));
                this.mPatientAddress.Zip = reader.GetOracleString(10).IsNull ? null : ((string) reader.GetOracleString(10));
                this.mEmail = reader.GetOracleString(11).IsNull ? null : ((string) reader.GetOracleString(11));
                this.mPatientPhone = new PhoneNumbers();
                this.mHomePhone = new Phone();
                this.mHomePhone.PhoneNumber = reader.GetOracleString(12).IsNull ? null : ((string) reader.GetOracleString(12)).Replace("-", "");
                this.mHomePhone.Qualifier = "TE";
                this.mPatientPhone.mPhone = this.mHomePhone;
                this.loadedFromDB = true;
            }
            reader.Close();
            connection.Close();
        }

        public void Save(string PatientID, ArrayList[] PatientInfo)
        {
        }

        public bool wasLoadedFromDb()
        {
            return this.loadedFromDB;
        }
    }
}

