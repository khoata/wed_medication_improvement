namespace WEBeDoctorPharmacy
{
    using System;

    public static class ErrorDescription
    {
        public const string InternalProcessingError = "007";
        public const string MessageIsDuplicate = "220";
        public const string ReceiverIDNotOnFile = "002";
        public const string ReceiverNoLongerActive = "209";
        public const string RequestTimedOut = "008";
        public const string SenderIDNotOnFile = "001";
        public const string SenderNoLongerActive = "208";
        public const string UIBTraceIDInvalid = "025";
        public const string ValidationError = "000";
    }
}

