namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class QualifierValuePair
    {
        [XmlElement("Qualifier")]
        public string Qualifier;
        [XmlElement("Value")]
        public string Value;
    }
}

