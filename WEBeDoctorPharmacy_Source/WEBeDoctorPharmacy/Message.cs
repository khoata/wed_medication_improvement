namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;

    public class Message
    {
        private string DrugID;
        private string MessageType;
        [XmlElement("Header")]
        public Header msgHeader;
        [XmlElement("Body")]
        public Body msgBody;
        [XmlAttribute("version")]
        public string mVersion;
        [XmlAttribute("release")]
        public string mRelease;

        public Message()
        {
        }

        public Message(string MsgType, string[] InputParams)
            : this(MsgType, InputParams, false, "", "", new string [] {})
        {
        }

        public Message(string MsgType, string[] InputParams, bool isEPCS, string scheduleCode, string dpsNumber, string[] benefitParams)
        {
            if (MsgType == "DirDwnld" || MsgType == "AddPrescriber" || MsgType == "UpdatePrescriber")
            {
                this.mVersion = "004";
                this.mRelease = "004";
            }
            else
            {
                this.mVersion = "010";
                this.mRelease = "006";
            }
            this.MessageType = MsgType;
            this.DrugID = InputParams[3] ?? "";
            this.msgHeader = new Header(MsgType);
            this.msgBody = new Body(MsgType, InputParams, isEPCS, scheduleCode, dpsNumber, benefitParams);
        }

        public void ConstructHeaderFields()
        {
            if (MessageType != "DirDwnld" &&
                MessageType != "AddPrescriber" &&
                MessageType != "UpdatePrescriber" &&
                !(msgBody.ChangeResp != null && msgBody.ChangeResp.Request != null && msgBody.ChangeResp.Request.ChangeRequestType == "P"))
            {
                this.msgHeader.PrescriberOrderNumber = Guid.NewGuid().ToString().Replace("-", "");
            }

            if (this.msgBody.NewRq != null)
            {
                this.msgHeader.To = new QualifiedMailAddressType
                {
                    Qualifier = "P",
                    Value = this.msgBody.NewRq.mPharmacy.mPharmacyID.NCPDPID
                };
                this.msgHeader.From = new QualifiedMailAddressType
                {
                    Qualifier = "D",
                    Value = this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI
                };
            }
            else if (this.msgBody.RefResp != null)
            {
                this.msgHeader.To = new QualifiedMailAddressType
                {
                    Qualifier = "P",
                    Value = this.msgBody.RefResp.mPharmacy.mPharmacyID.NCPDPID
                };
                this.msgHeader.From = new QualifiedMailAddressType
                {
                    Qualifier = "D",
                    Value = this.msgBody.RefResp.mPrescriber.mPrescriberID.SPI
                };
            }
            else if (this.msgBody.CancelReq != null)
            {
                this.msgHeader.To = new QualifiedMailAddressType
                {
                    Qualifier = "P",
                    Value = this.msgBody.CancelReq.mPharmacy.mPharmacyID.NCPDPID
                };
                this.msgHeader.From = new QualifiedMailAddressType
                {
                    Qualifier = "D",
                    Value = this.msgBody.CancelReq.mPrescriber.mPrescriberID.SPI
                };
            }
            else if (this.msgBody.ChangeResp != null)
            {
                this.msgHeader.To = new QualifiedMailAddressType
                {
                    Qualifier = "P",
                    Value = this.msgBody.ChangeResp.mPharmacy.mPharmacyID.NCPDPID
                };
                this.msgHeader.From = new QualifiedMailAddressType
                {
                    Qualifier = "D",
                    Value = this.msgBody.ChangeResp.mPrescriber.mPrescriberID.SPI
                };
            }
        }

        public void SynchronizePrescriberSPI()
        {
            if (this.msgBody.RefReq != null)
            {
                this.msgBody.RefReq.mPrescriber.mPrescriberID.SPI = this.msgHeader.To.Value;
            }
            else if (this.msgBody.RefResp != null)
            {
                this.msgBody.RefResp.mPrescriber.mPrescriberID.SPI = this.msgHeader.To.Value;
            }
            // Similar to NewRx, CancelReq.mPrescriber.mPrescriberID.SPI is loaded from "new PrescriberInfo(physicianId);"
            // CancelResp doesn't include mPrescriber information
            else if (this.msgBody.ChangeReq != null)
            {
                this.msgBody.ChangeReq.mPrescriber.mPrescriberID.SPI = this.msgHeader.To.Value;
            }
            else if (this.msgBody.ChangeResp != null)
            {
                this.msgBody.ChangeResp.mPrescriber.mPrescriberID.SPI = this.msgHeader.To.Value;
            }
        }

        public void LoadCorrelatedMessageInfo(string msgType, string[] InputParams)
        {
            if ((msgType == "RefillResponse") || (msgType == "NewRxFollowup") || (msgType == "RxChangeResponse"))
            {
                OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                OracleCommand command = new OracleCommand();
                connection.Open();
                command.Connection = connection;
                command.CommandText = "WSSInterface.LoadCorrelatedMessage";
                command.CommandType = CommandType.StoredProcedure;
                if (msgType == "RefillResponse" || msgType == "RxChangeResponse")
                {
                    command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = InputParams[8];
                }
                else
                {
                    command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = InputParams[5];
                }
                command.Parameters.Add(new OracleParameter("CurCorrelatedMessageInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    // Common for both Refill Response & NewRx Followup
                    this.msgHeader.RXReferenceNumber = reader.GetOracleString(0).IsNull ? null : reader.GetOracleString(0).ToString();
                    this.msgHeader.RelatesToMessageID = reader.GetOracleString(2).IsNull ? null : reader.GetOracleString(2).ToString();

                    // Not need to do this, because Refill Response already copy from Refill Request.
                    //if (msgType == "RefillResponse")
                    //{
                    //    string temp = reader.GetOracleString(3).IsNull ? null : reader.GetOracleString(3).ToString();
                    //    if ((temp == "1") && (temp == "7"))
                    //    {
                    //        this.msgBody.RefResp.mMedicationPrescribed.Substitutions = "1";
                    //    }
                    //    else
                    //    {
                    //        this.msgBody.RefResp.mMedicationPrescribed.Substitutions = "0";
                    //    }
                    //}
                }
                reader.Close();
                connection.Close();
            }
			else if (msgType == "CancelRx")
			{
				this.msgHeader.RelatesToMessageID = InputParams[5];
			}
        }

        public string Save(string InternalMessageID, string sPaymentNotes)
        {
            SS_Logger.log("=====1.3 =====");
            string OrderNumber = "";
            string RXReferenceNumber = "";
            string PharmacyID = "";
            string PrescriberID = "";
            string MessageType = "";
            string ResponseCode = "";
            string ChangeType = "";
            string RelatedMessageId = "";
            string PharmacyName = "";
            string PharmacyAddress = "";
            string PharmacyCity = "";
            string PharmacyState = "";
            string PharmacyZip = "";
            string[] PharmacyPhone = new string[8] { "", "", "", "", "", "", "", "" };
            string[] PharmacyPhoneType = new string[8] { "", "", "", "", "", "", "", "" };
            string PrescriberClinicName = "";
            string PrescriberSPI = "";
            string PrescriberNPI = "";
            string PrescriberDEA = "";
            string PrescriberSpecialtyQual = "";
            string PrescriberSpecialtyCode = "";
            string PrescriberAgentLastName = "";
            string PrescriberAgentFirstName = "";
            string PrescriberAgentMiddleName = "";
            string PrescriberAddress = "";
            string PrescriberCity = "";
            string PrescriberState = "";
            string PrescriberZip = "";
            string PrescriberEmail = "";
            string[] PrescriberPhone = new string[8] { "", "", "", "", "", "", "", "" };
            string[] PrescriberPhoneType = new string[8] { "", "", "", "", "", "", "", "" };
            string PatientID = "";
            string PatientSSN = "";
            string PatientLastName = "";
            string PatientFirstName = "";
            string PatientMiddleName = "";
            string PatientPreFix = "";
            string PatientGender = "";
            string PatientDOB = "";
            string PatientAddress = "";
            string PatientAddress2 = "";
            string PatientCity = "";
            string PatientState = "";
            string PatientZip = "";
            string PatientEmail = "";
            string[] PatientPhone = new string[8] { "", "", "", "", "", "", "", "" };
            string[] PatientPhoneType = new string[8] { "", "", "", "", "", "", "", "" };
            string MedicationDescription = "";
            string MedicationCode = "";
            string MedicationCodeType = "";
            string MedicationDosageForm = "";
            string MedicationStrength = "";
            string MedicationStrengthUnits = "";
            string MedicationDrugDBCode = "";
            string MedicationDrugDBCodeQual = "";
            string MedicationQuantityQual = "";
            string MedicationQuantity = "";
            string MedicationDirections = "";
            string MedicationNote = "";
            string MedicationRefillQual = "";
            string MedicationSubstitution = "";
            string MedicationWrittenDate = "";
            string MsgContent = "";
            string ResponseNote = "";
            string MedicationRefillQuan = "";
            string PrescriberServiceLevel = "";
            string MedicationLastFillDate = "";
            string MedicationDaysSupply = "";
            string MedicationClinicalInfoQual = "";
            string MedicationPrimaryQual = "";
            string MedicationPrimaryVal = "";
            string MedicationSecondaryQual = "";
            string MedicationSecondaryVal = "";
            string MedicationDispensedDescription = "";
            string MedicationDispensedCode = "";
            string MedicationDispensedCodeType = "";
            string MedicationDispensedDosageForm = "";
            string MedicationDispensedStrength = "";
            string MedicationDispensedStrengthUnits = "";
            string MedicationDispensedDrugDBCode = "";
            string MedicationDispensedDrugDBCodeQual = "";
            string MedicationDispensedQuantityQual = "";
            string MedicationDispensedQuantity = "";
            string MedicationDispensedDirections = "";
            string MedicationDispensedNote = "";
            string MedicationDispensedRefillQual = "";
            string MedicationDispensedSubstitution = "";
            string MedicationDispensedWrittenDate = "";
            string MedicationDispensedRefillQuan = "";
            string MedicationDispensedLastFillDate = "";
            string MedicationDispensedDaysSupply = "";
            string MedicationDispensedClinicalInfoQual = "";
            string MedicationDispensedPrimaryQual = "";
            string MedicationDispensedPrimaryVal = "";
            string MedicationDispensedSecondaryQual = "";
            string MedicationDispensedSecondaryVal = "";
            string DEASchedule = "";
            string DEASchedulePrescribe = "";
            int CooIncluded = 0;
            string CooPayerMutuallydefined = "";
            string CooPayerBinlocationNumber = "";
            string CooPayerProcessorIdNumber = "";
            string CooPayerId = "";
            string CooPayerName = "";
            string CooCardholderId = "";
            string CooCardholderFirstname = "";
            string CooCardholderLastname = "";
            string CooCardholderMiddlename = "";
            string CooCardholderSuffix = "";
            string CooCardholderPrefix = "";
            string CooGroupId = "";
            string MedicationDUE = "";

            // Common corelated ids
            OrderNumber = this.msgHeader.PrescriberOrderNumber ?? "";
            RXReferenceNumber = this.msgHeader.RXReferenceNumber ?? "";

            if (this.msgBody.NewRq != null)
            {
                MessageType = "NewRX";
                PharmacyID = this.msgBody.NewRq.mPharmacy.mPharmacyID.NCPDPID;
                if (this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI != null)
                {
                    PrescriberID = this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI;
                }
                RelatedMessageId = this.msgHeader.RelatesToMessageID;
                PharmacyName = this.msgBody.NewRq.mPharmacy.mStoreName;
                PharmacyAddress = this.msgBody.NewRq.mPharmacy.mPharmacyAddress.Address1;
                PharmacyCity = this.msgBody.NewRq.mPharmacy.mPharmacyAddress.City;
                PharmacyState = this.msgBody.NewRq.mPharmacy.mPharmacyAddress.State;
                PharmacyZip = this.msgBody.NewRq.mPharmacy.mPharmacyAddress.Zip;
                PharmacyPhone[0] = this.msgBody.NewRq.mPharmacy.mPharmacyPhone.mPhone[0].PhoneNumber;
                PharmacyPhoneType[0] = this.msgBody.NewRq.mPharmacy.mPharmacyPhone.mPhone[0].Qualifier;
                PrescriberClinicName = this.msgBody.NewRq.mPrescriber.mClinicName;
                if (this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI != null)
                {
                    PrescriberSPI = this.msgBody.NewRq.mPrescriber.mPrescriberID.SPI;
                }
                if (this.msgBody.NewRq.mPrescriber.mSpecialty.Qualifier != null)
                {
                    PrescriberSpecialtyQual = this.msgBody.NewRq.mPrescriber.mSpecialty.Qualifier;
                    PrescriberSpecialtyCode = this.msgBody.NewRq.mPrescriber.mSpecialty.SpecialtyCode;
                }
                if ((this.msgBody.NewRq.mPrescriber.mPrescriberAgent != null) && (this.msgBody.NewRq.mPrescriber.mPrescriberAgent.LastName != null))
                {
                    PrescriberAgentLastName = this.msgBody.NewRq.mPrescriber.mPrescriberAgent.LastName;
                    PrescriberAgentFirstName = this.msgBody.NewRq.mPrescriber.mPrescriberAgent.FirstName;
                }
                else if ((this.msgBody.NewRq.mPrescriber.mPrescriberName != null))
                {
                    if (this.msgBody.NewRq.mPrescriber.mPrescriberName.FirstName != null)
                    {
                        PrescriberAgentFirstName = this.msgBody.NewRq.mPrescriber.mPrescriberName.FirstName;
                    }
                    if (this.msgBody.NewRq.mPrescriber.mPrescriberName.LastName != null)
                    {
                        PrescriberAgentLastName = this.msgBody.NewRq.mPrescriber.mPrescriberName.LastName;
                    }
                }
                if (this.msgBody.NewRq.mPrescriber.mPrescriberID != null && !string.IsNullOrEmpty(this.msgBody.NewRq.mPrescriber.mPrescriberID.DEANumber))
                {
                    PrescriberDEA = this.msgBody.NewRq.mPrescriber.mPrescriberID.DEANumber;
                }
                PrescriberAddress = this.msgBody.NewRq.mPrescriber.mPrescriberAddress.Address1;
                PrescriberCity = this.msgBody.NewRq.mPrescriber.mPrescriberAddress.City;
                PrescriberState = this.msgBody.NewRq.mPrescriber.mPrescriberAddress.State;
                PrescriberZip = this.msgBody.NewRq.mPrescriber.mPrescriberAddress.Zip;
                if (this.msgBody.NewRq.mPrescriber.mPrescriberPhone != null &&
                    this.msgBody.NewRq.mPrescriber.mPrescriberPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.NewRq.mPrescriber.mPrescriberPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PrescriberEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }
                PrescriberPhone[0] = this.msgBody.NewRq.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber;
                PrescriberPhoneType[0] = this.msgBody.NewRq.mPrescriber.mPrescriberPhone.mPhone[0].Qualifier;
                PatientID = this.msgBody.NewRq.mPatient.mPatientID.FileID;
                if (this.msgBody.NewRq.mPatient.mPatientID.SocialSecurityNumber != null)
                {
                    PatientSSN = this.msgBody.NewRq.mPatient.mPatientID.SocialSecurityNumber;
                }
                PatientLastName = this.msgBody.NewRq.mPatient.mPatientName.LastName;
                PatientFirstName = this.msgBody.NewRq.mPatient.mPatientName.FirstName;
                if (this.msgBody.NewRq.mPatient.mPatientName.Prefix != null)
                {
                    PatientPreFix = this.msgBody.NewRq.mPatient.mPatientName.Prefix;
                }
                PatientGender = this.msgBody.NewRq.mPatient.mGender;
                // patient DOB is required
                PatientDOB = this.msgBody.NewRq.mPatient.mDateOfBirth.Item.ToString("yyyyMMdd");
                PatientAddress = this.msgBody.NewRq.mPatient.mPatientAddress.Address1;
                PatientAddress2 = this.msgBody.NewRq.mPatient.mPatientAddress.Address2 ?? "";
                PatientCity = this.msgBody.NewRq.mPatient.mPatientAddress.City;
                PatientState = this.msgBody.NewRq.mPatient.mPatientAddress.State;
                PatientZip = this.msgBody.NewRq.mPatient.mPatientAddress.Zip;
                if (this.msgBody.NewRq.mPatient.mPatientPhone != null &&
                    this.msgBody.NewRq.mPatient.mPatientPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.NewRq.mPatient.mPatientPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PatientEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }
                if ((this.msgBody.NewRq.mPatient.mPatientPhone != null) &&
                    (this.msgBody.NewRq.mPatient.mPatientPhone.mPhone != null) &&
                    (this.msgBody.NewRq.mPatient.mPatientPhone.mPhone.Length > 0))
                {
                    PatientPhone[0] = this.msgBody.NewRq.mPatient.mPatientPhone.mPhone[0].PhoneNumber;
                    PatientPhoneType[0] = this.msgBody.NewRq.mPatient.mPatientPhone.mPhone[0].Qualifier;
                }
                MedicationDescription = this.msgBody.NewRq.mMedicationPrescribed.DrugDescription;
                if ((this.msgBody.NewRq.mMedicationPrescribed.DrugCode != null) && (this.msgBody.NewRq.mMedicationPrescribed.DrugCode.ProductCode != null))
                {
                    MedicationCode = this.msgBody.NewRq.mMedicationPrescribed.DrugCode.ProductCode;
                    MedicationCodeType = "ND";
                }
                if (this.msgBody.NewRq.mMedicationPrescribed.Quantity != null)
                {
                    MedicationQuantityQual = this.msgBody.NewRq.mMedicationPrescribed.Quantity.PotencyUnitCode;
                    MedicationQuantity = this.msgBody.NewRq.mMedicationPrescribed.Quantity.Value;
                }
                if (this.msgBody.NewRq.mMedicationPrescribed.Directions != null)
                {
                    MedicationDirections = this.msgBody.NewRq.mMedicationPrescribed.Directions;
                }


                if (this.msgBody.NewRq.mMedicationPrescribed.Note != null)
                {
                    MedicationNote = "Provider Notes : " + this.msgBody.NewRq.mMedicationPrescribed.Note;

                    if (!(string.IsNullOrEmpty(sPaymentNotes)))
                    {
                        MedicationNote += " ,PDR Notes : " + sPaymentNotes;
                    }
                }
                else
                {
                    if (!(string.IsNullOrEmpty(sPaymentNotes)))
                    {
                        MedicationNote += "PDR Notes : " + sPaymentNotes;
                    }
                }


                MedicationRefillQual = this.msgBody.NewRq.mMedicationPrescribed.Refills.Qualifier;
                if (this.msgBody.NewRq.mMedicationPrescribed.Refills.Qualifier != "PRN")
                {
                    MedicationRefillQuan = this.msgBody.NewRq.mMedicationPrescribed.Refills.Value;
                }
                MedicationSubstitution = this.msgBody.NewRq.mMedicationPrescribed.Substitutions;
                MedicationWrittenDate = this.msgBody.NewRq.mMedicationPrescribed.WrittenDate.Item.ToString("yyyyMMdd");
            }
            else if (this.msgBody.RefReq != null)
            {
                MessageType = "RefillRequest";
                PharmacyID = this.msgBody.RefReq.mPharmacy.mPharmacyID.NCPDPID;
                if (this.msgBody.RefReq.mPrescriber.mPrescriberID.SPI != null)
                {
                    PrescriberID = this.msgBody.RefReq.mPrescriber.mPrescriberID.SPI;
                }
                RelatedMessageId = this.msgHeader.RelatesToMessageID;
                this.DrugID = "";
                PharmacyName = this.msgBody.RefReq.mPharmacy.mStoreName;
                if (this.msgBody.RefReq.mPharmacy.mPharmacyAddress != null)
                {
                    PharmacyAddress = this.msgBody.RefReq.mPharmacy.mPharmacyAddress.Address1;
                    PharmacyCity = this.msgBody.RefReq.mPharmacy.mPharmacyAddress.City;
                    PharmacyState = this.msgBody.RefReq.mPharmacy.mPharmacyAddress.State;
                    PharmacyZip = this.msgBody.RefReq.mPharmacy.mPharmacyAddress.Zip;
                }
                if ((this.msgBody.RefReq.mPharmacy.mPharmacyPhone != null) && (this.msgBody.RefReq.mPharmacy.mPharmacyPhone.mPhone != null))
                {
                    for (int i = 0; i <= this.msgBody.RefReq.mPharmacy.mPharmacyPhone.mPhone.GetUpperBound(0); i++)
                    {
                        PharmacyPhone[i] = this.msgBody.RefReq.mPharmacy.mPharmacyPhone.mPhone[i].PhoneNumber;
                        PharmacyPhoneType[i] = this.msgBody.RefReq.mPharmacy.mPharmacyPhone.mPhone[i].Qualifier;
                    }
                }
                if (!(this.msgBody.RefReq.mPrescriber.mClinicName == null))
                {
                    PrescriberClinicName = this.msgBody.RefReq.mPrescriber.mClinicName;
                }
                PrescriberSPI = this.msgBody.RefReq.mPrescriber.mPrescriberID.SPI;
                if (this.msgBody.RefReq.mPrescriber.mSpecialty != null)
                {
                    if (this.msgBody.RefReq.mPrescriber.mSpecialty.Qualifier != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.RefReq.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.RefReq.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                }
                if (this.msgBody.RefReq.mPrescriber.mPrescriberAgent != null)
                {
                    if (this.msgBody.RefReq.mPrescriber.mPrescriberAgent.LastName != null)
                    {
                        PrescriberAgentLastName = this.msgBody.RefReq.mPrescriber.mPrescriberAgent.LastName;
                        PrescriberAgentFirstName = this.msgBody.RefReq.mPrescriber.mPrescriberAgent.FirstName;
                        if (this.msgBody.RefReq.mPrescriber.mPrescriberAgent.MiddleName != null)
                        {
                            PrescriberAgentMiddleName = this.msgBody.RefReq.mPrescriber.mPrescriberAgent.MiddleName;
                        }
                    }
                }
                if (this.msgBody.RefReq.mPrescriber.mPrescriberName.LastName != null)
                {
                    PrescriberAgentLastName = this.msgBody.RefReq.mPrescriber.mPrescriberName.LastName;
                    PrescriberAgentFirstName = this.msgBody.RefReq.mPrescriber.mPrescriberName.FirstName;
                    if (this.msgBody.RefReq.mPrescriber.mPrescriberName.MiddleName != null)
                    {
                        PrescriberAgentMiddleName = this.msgBody.RefReq.mPrescriber.mPrescriberName.MiddleName;
                    }
                }
                if (this.msgBody.RefReq.mPrescriber.mPrescriberID != null && !string.IsNullOrEmpty(this.msgBody.RefReq.mPrescriber.mPrescriberID.DEANumber))
                {
                    PrescriberDEA = this.msgBody.RefReq.mPrescriber.mPrescriberID.DEANumber;
                }
                if (this.msgBody.RefReq.mPrescriber.mPrescriberAddress != null)
                {
                    PrescriberAddress = this.msgBody.RefReq.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.RefReq.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.RefReq.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.RefReq.mPrescriber.mPrescriberAddress.Zip;
                }
                if (this.msgBody.RefReq.mPrescriber.mPrescriberPhone != null &&
                    this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PrescriberEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }
                if ((this.msgBody.RefReq.mPrescriber.mPrescriberPhone != null) && (this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone != null))
                {
                    for (int i = 0; i <= this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone.GetUpperBound(0); i++)
                    {
                        PrescriberPhone[i] = this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone[i].PhoneNumber;
                        PrescriberPhoneType[i] = this.msgBody.RefReq.mPrescriber.mPrescriberPhone.mPhone[i].Qualifier;
                    }
                }
                if (this.msgBody.RefReq.mPatient.mPatientID != null)
                {
                    if (this.msgBody.RefReq.mPatient.mPatientID.FileID != null)
                    {
                        PatientID = this.msgBody.RefReq.mPatient.mPatientID.FileID;
                    }
                    if (this.msgBody.RefReq.mPatient.mPatientID.SocialSecurityNumber != null)
                    {
                        PatientSSN = this.msgBody.RefReq.mPatient.mPatientID.SocialSecurityNumber;
                    }
                }
                PatientLastName = this.msgBody.RefReq.mPatient.mPatientName.LastName;
                PatientFirstName = this.msgBody.RefReq.mPatient.mPatientName.FirstName;
                if (this.msgBody.RefReq.mPatient.mPatientName.MiddleName != null)
                {
                    PatientMiddleName = this.msgBody.RefReq.mPatient.mPatientName.MiddleName;
                }
                if (this.msgBody.RefReq.mPatient.mPatientName.Prefix != null)
                {
                    PatientPreFix = this.msgBody.RefReq.mPatient.mPatientName.Prefix;
                }
                PatientGender = this.msgBody.RefReq.mPatient.mGender;
                // patient DOB is required
                PatientDOB = this.msgBody.RefReq.mPatient.mDateOfBirth.Item.ToString("yyyyMMdd");
                if (this.msgBody.RefReq.mPatient.mPatientAddress != null)
                {
                    PatientAddress = this.msgBody.RefReq.mPatient.mPatientAddress.Address1;
                    PatientAddress2 = this.msgBody.RefReq.mPatient.mPatientAddress.Address2;
                    PatientCity = this.msgBody.RefReq.mPatient.mPatientAddress.City;
                    PatientState = this.msgBody.RefReq.mPatient.mPatientAddress.State;
                    PatientZip = this.msgBody.RefReq.mPatient.mPatientAddress.Zip;
                }
                if (this.msgBody.RefReq.mPatient.mPatientPhone != null &&
                    this.msgBody.RefReq.mPatient.mPatientPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.RefReq.mPatient.mPatientPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PatientEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }
                if ((this.msgBody.RefReq.mPatient.mPatientPhone != null) && (this.msgBody.RefReq.mPatient.mPatientPhone.mPhone != null))
                {
                    for (int i = 0; i <= this.msgBody.RefReq.mPatient.mPatientPhone.mPhone.GetUpperBound(0); i++)
                    {
                        PatientPhone[i] = this.msgBody.RefReq.mPatient.mPatientPhone.mPhone[i].PhoneNumber;
                        PatientPhoneType[i] = this.msgBody.RefReq.mPatient.mPatientPhone.mPhone[i].Qualifier;
                    }
                }
                MedicationDescription = this.msgBody.RefReq.mMedicationPrescribed.DrugDescription;
                if (this.msgBody.RefReq.mMedicationPrescribed.DrugCode != null)
                {
                    if (this.msgBody.RefReq.mMedicationPrescribed.DrugCode.ProductCode != null)
                    {
                        MedicationCode = this.msgBody.RefReq.mMedicationPrescribed.DrugCode.ProductCode;
                        MedicationCodeType = "ND";
                    }
                    // Not support by RxRouting 10.6
                    //if (this.msgBody.RefReq.mMedication.DrugCode.DosageForm != null)
                    //{
                    //    MedicationDosageForm = this.msgBody.RefReq.mMedication.DrugCode.DosageForm;
                    //}
                    if (this.msgBody.RefReq.mMedicationPrescribed.DrugCode.Strength != null)
                    {
                        MedicationStrength = this.msgBody.RefReq.mMedicationPrescribed.DrugCode.Strength;
                        // Not support by RxRouting 10.6
                        //MedicationStrengthUnits = this.msgBody.RefReq.mMedication.DrugCode.StrengthUnits;
                    }
                    if (this.msgBody.RefReq.mMedicationPrescribed.DrugCode.DrugDBCode != null)
                    {
                        MedicationDrugDBCode = this.msgBody.RefReq.mMedicationPrescribed.DrugCode.DrugDBCode;
                        MedicationDrugDBCodeQual = this.msgBody.RefReq.mMedicationPrescribed.DrugCode.DrugDBCodeQualifier;
                    }
                }
                if (this.msgBody.RefReq.mMedicationPrescribed.Quantity != null)
                {
                    MedicationQuantityQual = this.msgBody.RefReq.mMedicationPrescribed.Quantity.PotencyUnitCode;
                    MedicationQuantity = this.msgBody.RefReq.mMedicationPrescribed.Quantity.Value;
                }
                if (this.msgBody.RefReq.mMedicationPrescribed.DaysSupply != null)
                {
                    MedicationDaysSupply = this.msgBody.RefReq.mMedicationPrescribed.DaysSupply;
                }
                MedicationDirections = this.msgBody.RefReq.mMedicationPrescribed.Directions;

                //if (this.msgBody.RefReq.mMedicationPrescribed.Note != null)
                //{
                //    //MedicationNote = this.msgBody.RefReq.mMedicationPrescribed.Note;
                //    MedicationNote = sPaymentNotes;
                //}
                
                if (this.msgBody.RefReq.mMedicationPrescribed.Note != null)
                {
                    MedicationNote = "Provider Notes : " + this.msgBody.RefReq.mMedicationPrescribed.Note;

                    if (!(string.IsNullOrEmpty(sPaymentNotes)))
                    {
                        MedicationNote += " ,PDR Notes : " + sPaymentNotes;
                    }
                }
                else
                {
                    if (!(string.IsNullOrEmpty(sPaymentNotes)))
                    {
                        MedicationNote += "PDR Notes : " + sPaymentNotes;
                    }
                }
                
                if (this.msgBody.RefReq.mMedicationPrescribed.Refills != null)
                {
                    MedicationRefillQual = this.msgBody.RefReq.mMedicationPrescribed.Refills.Qualifier;
                    if (this.msgBody.RefReq.mMedicationPrescribed.Refills.Qualifier != "PRN")
                    {
                        MedicationRefillQuan = this.msgBody.RefReq.mMedicationPrescribed.Refills.Value;
                    }
                }

                MedicationSubstitution = this.msgBody.RefReq.mMedicationPrescribed.Substitutions;
                MedicationWrittenDate = this.msgBody.RefReq.mMedicationPrescribed.WrittenDate.Item.ToString("yyyyMMdd");
                if (this.msgBody.RefReq.mMedicationPrescribed.LastFillDate != null)
                {
                    MedicationLastFillDate = this.msgBody.RefReq.mMedicationPrescribed.LastFillDate.Item.ToString("yyyyMMdd");
                }

                // Medication dispensed
                MedicationDispensedDescription = this.msgBody.RefReq.mMedicationDispensed.DrugDescription;
                if (this.msgBody.RefReq.mMedicationDispensed.DrugCode != null)
                {
                    if (this.msgBody.RefReq.mMedicationDispensed.DrugCode.ProductCode != null)
                    {
                        MedicationDispensedCode = this.msgBody.RefReq.mMedicationDispensed.DrugCode.ProductCode;
                        MedicationDispensedCodeType = "ND";
                    }
                    // Not support by RxRouting 10.6
                    //if (this.msgBody.RefReq.mMedication.DrugCode.DosageForm != null)
                    //{
                    //    MedicationDispensedDosageForm = this.msgBody.RefReq.mMedication.DrugCode.DosageForm;
                    //}
                    if (this.msgBody.RefReq.mMedicationDispensed.DrugCode.Strength != null)
                    {
                        MedicationDispensedStrength = this.msgBody.RefReq.mMedicationDispensed.DrugCode.Strength;
                        // Not support by RxRouting 10.6
                        //MedicationDispensedStrengthUnits = this.msgBody.RefReq.mMedication.DrugCode.StrengthUnits;
                    }
                    if (this.msgBody.RefReq.mMedicationDispensed.DrugCode.DrugDBCode != null)
                    {
                        MedicationDispensedDrugDBCode = this.msgBody.RefReq.mMedicationDispensed.DrugCode.DrugDBCode;
                        MedicationDispensedDrugDBCodeQual = this.msgBody.RefReq.mMedicationDispensed.DrugCode.DrugDBCodeQualifier;
                    }
                }
                if (this.msgBody.RefReq.mMedicationDispensed.Quantity != null)
                {
                    MedicationDispensedQuantityQual = this.msgBody.RefReq.mMedicationDispensed.Quantity.PotencyUnitCode;
                    MedicationDispensedQuantity = this.msgBody.RefReq.mMedicationDispensed.Quantity.Value;
                }
                if (this.msgBody.RefReq.mMedicationDispensed.DaysSupply != null)
                {
                    MedicationDispensedDaysSupply = this.msgBody.RefReq.mMedicationDispensed.DaysSupply;
                }
                MedicationDispensedDirections = this.msgBody.RefReq.mMedicationDispensed.Directions;
                if (this.msgBody.RefReq.mMedicationDispensed.Note != null)
                {
                    MedicationDispensedNote = this.msgBody.RefReq.mMedicationDispensed.Note;
                }

                if (this.msgBody.RefReq.mMedicationDispensed.Refills != null)
                {
                    MedicationDispensedRefillQual = this.msgBody.RefReq.mMedicationDispensed.Refills.Qualifier;
                    if (this.msgBody.RefReq.mMedicationDispensed.Refills.Qualifier != "PRN")
                    {
                        MedicationDispensedRefillQuan = this.msgBody.RefReq.mMedicationDispensed.Refills.Value;
                    }
                }

                MedicationDispensedSubstitution = this.msgBody.RefReq.mMedicationDispensed.Substitutions;
                MedicationDispensedWrittenDate = this.msgBody.RefReq.mMedicationDispensed.WrittenDate.Item.ToString("yyyyMMdd");
                if (this.msgBody.RefReq.mMedicationDispensed.LastFillDate != null)
                {
                    MedicationDispensedLastFillDate = this.msgBody.RefReq.mMedicationDispensed.LastFillDate.Item.ToString("yyyyMMdd");
                }

                if (this.msgBody.RefReq.mMedicationDispensed.DrugCode != null)
                {
                    //C38046 = Unspecified
                    if (this.msgBody.RefReq.mMedicationDispensed.DrugCode.DEASchedule != DrugCodedTypeDEASchedule.C38046)
                    {
                        DEASchedule = this.msgBody.RefReq.mMedicationDispensed.DrugCode.DEASchedule.ToString();
                    }
                }

                if (this.msgBody.RefReq.mMedicationPrescribed.DrugCode != null)
                {
                    //C38046 = Unspecified
                    if (this.msgBody.RefReq.mMedicationPrescribed.DrugCode.DEASchedule != DrugCodedTypeDEASchedule.C38046)
                    {
                        DEASchedulePrescribe = this.msgBody.RefReq.mMedicationPrescribed.DrugCode.DEASchedule.ToString();
                    }
                }
            }
            else if (this.msgBody.RefResp != null)
            {
                SS_Logger.log("=====1.3.1 =====");
                MessageType = "RefillResponse";
                PharmacyID = this.msgBody.RefResp.mPharmacy.mPharmacyID.NCPDPID;
                PrescriberID = this.msgBody.RefResp.mPrescriber.mPrescriberID.SPI;
                RelatedMessageId = this.msgHeader.RelatesToMessageID;
                SS_Logger.log("=====1.3.1.5=====");
                if (this.msgBody.RefResp.ResponseMsg.ApprovedResp != null)
                {
                    ResponseCode = "Approved";
                }
                else if (this.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp != null)
                {
                    ResponseCode = "ApprovedWithChanges";
                }
                else if (this.msgBody.RefResp.ResponseMsg.DeniedResp != null)
                {
                    ResponseCode = "Denied";
                }
                else if (this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp != null)
                {
                    ResponseCode = "DeniedNewPrescriptionToFollow";
                }
                SS_Logger.log("=====1.3.2 =====");
                PharmacyName = this.msgBody.RefResp.mPharmacy.mStoreName;
                if (this.msgBody.RefResp.mPharmacy.mPharmacyAddress != null)
                {
                    PharmacyAddress = this.msgBody.RefResp.mPharmacy.mPharmacyAddress.Address1;
                    PharmacyCity = this.msgBody.RefResp.mPharmacy.mPharmacyAddress.City;
                    PharmacyState = this.msgBody.RefResp.mPharmacy.mPharmacyAddress.State;
                    PharmacyZip = this.msgBody.RefResp.mPharmacy.mPharmacyAddress.Zip;
                }

                if (this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone != null)
                {
                    SS_Logger.log(this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone.GetUpperBound(0).ToString());
                    for (int i = 0; i <= this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone.GetUpperBound(0); i++)
                    {
                        if (this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone[i] != null)
                        {
                            PharmacyPhone[i] = this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone[i].PhoneNumber.ToString();
                            PharmacyPhoneType[i] = this.msgBody.RefResp.mPharmacy.mPharmacyPhone.mPhone[i].Qualifier.ToString();
                        }
                    }
                }
                if (this.msgBody.RefResp.mPrescriber.mClinicName != null)
                {
                    PrescriberClinicName = this.msgBody.RefResp.mPrescriber.mClinicName;
                }
                PrescriberSPI = this.msgBody.RefResp.mPrescriber.mPrescriberID.SPI;
                if (this.msgBody.RefResp.mPrescriber.mSpecialty != null)
                {
                    if (this.msgBody.RefResp.mPrescriber.mSpecialty.Qualifier != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.RefResp.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.RefResp.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                }
                if (this.msgBody.RefResp.mPrescriber.mPrescriberAgent != null)
                {
                    if (this.msgBody.RefResp.mPrescriber.mPrescriberAgent.LastName != null)
                    {
                        PrescriberAgentLastName = this.msgBody.RefResp.mPrescriber.mPrescriberAgent.LastName;
                        PrescriberAgentFirstName = this.msgBody.RefResp.mPrescriber.mPrescriberAgent.FirstName;
                    }

                    if (this.msgBody.RefResp.mPrescriber.mPrescriberAgent.MiddleName != null)
                    {
                        PrescriberAgentMiddleName = this.msgBody.RefResp.mPrescriber.mPrescriberAgent.MiddleName;
                    }
                }
                if (this.msgBody.RefResp.mPrescriber.mPrescriberName.LastName != null)
                {
                    PrescriberAgentLastName = this.msgBody.RefResp.mPrescriber.mPrescriberName.LastName;
                    PrescriberAgentFirstName = this.msgBody.RefResp.mPrescriber.mPrescriberName.FirstName;
                    if (this.msgBody.RefResp.mPrescriber.mPrescriberName.MiddleName != null)
                    {
                        PrescriberAgentMiddleName = this.msgBody.RefResp.mPrescriber.mPrescriberName.MiddleName;
                    }
                }
                if (this.msgBody.RefResp.mPrescriber.mPrescriberID != null && !string.IsNullOrEmpty(this.msgBody.RefResp.mPrescriber.mPrescriberID.DEANumber))
                {
                    PrescriberDEA = this.msgBody.RefResp.mPrescriber.mPrescriberID.DEANumber;
                }
                if (this.msgBody.RefResp.mPrescriber.mPrescriberAddress != null)
                {
                    PrescriberAddress = this.msgBody.RefResp.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.RefResp.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.RefResp.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.RefResp.mPrescriber.mPrescriberAddress.Zip;
                }

                if (this.msgBody.RefResp.mPrescriber.mPrescriberPhone != null &&
                    this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PrescriberEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }

                if (this.msgBody.RefResp.mPrescriber.mPrescriberPhone != null && this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone != null)
                {
                    for (int i = 0; i <= this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone.GetUpperBound(0); i++)
                    {
                        if (this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone[i] != null)
                        {
                            SS_Logger.log("=====1.3.3 =====");
                            PrescriberPhone[i] = this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone[i].PhoneNumber;
                            PrescriberPhoneType[i] = this.msgBody.RefResp.mPrescriber.mPrescriberPhone.mPhone[i].Qualifier;
                        }
                    }
                }
                if ((this.msgBody.RefResp.mPatient.mPatientID != null) && (this.msgBody.RefResp.mPatient.mPatientID.FileID != null))
                {
                    PatientID = this.msgBody.RefResp.mPatient.mPatientID.FileID;
                    if (this.msgBody.RefResp.mPatient.mPatientID.SocialSecurityNumber != null)
                    {
                        PatientSSN = this.msgBody.RefResp.mPatient.mPatientID.SocialSecurityNumber;
                    }
                }

                if (this.msgBody.RefResp.mPatient.mPatientName != null)
                {
                    if (this.msgBody.RefResp.mPatient.mPatientName.LastName != null)
                    {
                        PatientLastName = this.msgBody.RefResp.mPatient.mPatientName.LastName;
                    }
                    if (this.msgBody.RefResp.mPatient.mPatientName.FirstName != null)
                    {
                        PatientFirstName = this.msgBody.RefResp.mPatient.mPatientName.FirstName;
                    }
                    if (this.msgBody.RefResp.mPatient.mPatientName.MiddleName != null)
                    {
                        PatientMiddleName = this.msgBody.RefResp.mPatient.mPatientName.MiddleName;
                    }
                    if (this.msgBody.RefResp.mPatient.mPatientName.Prefix != null)
                    {
                        PatientPreFix = this.msgBody.RefResp.mPatient.mPatientName.Prefix;
                    }
                }
                PatientGender = this.msgBody.RefResp.mPatient.mGender ?? "";
                // patient DOB is required
                PatientDOB = this.msgBody.RefResp.mPatient.mDateOfBirth.Item.ToString("yyyyMMdd");
                if (this.msgBody.RefResp.mPatient.mPatientAddress != null)
                {
                    PatientAddress = this.msgBody.RefResp.mPatient.mPatientAddress.Address1 ?? "";
                    PatientAddress2 = this.msgBody.RefResp.mPatient.mPatientAddress.Address2 ?? "";
                    PatientCity = this.msgBody.RefResp.mPatient.mPatientAddress.City ?? "";
                    PatientState = this.msgBody.RefResp.mPatient.mPatientAddress.State ?? "";
                    PatientZip = this.msgBody.RefResp.mPatient.mPatientAddress.Zip ?? "";
                }
                if (this.msgBody.RefResp.mPatient.mPatientPhone != null &&
                    this.msgBody.RefResp.mPatient.mPatientPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.RefResp.mPatient.mPatientPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PatientEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }
                if ((this.msgBody.RefResp.mPatient.mPatientPhone != null) && (this.msgBody.RefResp.mPatient.mPatientPhone.mPhone != null))
                {
                    for (int i = 0; i <= this.msgBody.RefResp.mPatient.mPatientPhone.mPhone.GetUpperBound(0); i++)
                    {
                        if (this.msgBody.RefResp.mPatient.mPatientPhone.mPhone[i] != null)
                        {
                            SS_Logger.log("=====1.3.4 =====");
                            PatientPhone[i] = this.msgBody.RefResp.mPatient.mPatientPhone.mPhone[i].PhoneNumber ?? "";
                            PatientPhoneType[i] = this.msgBody.RefResp.mPatient.mPatientPhone.mPhone[i].Qualifier ?? "";
                        }
                    }
                }
                MedicationDescription = this.msgBody.RefResp.mMedicationPrescribed.DrugDescription;
                if ((this.msgBody.RefResp.mMedicationPrescribed.DrugCode != null) && (this.msgBody.RefResp.mMedicationPrescribed.DrugCode.ProductCode != null))
                {
                    MedicationCode = this.msgBody.RefResp.mMedicationPrescribed.DrugCode.ProductCode;
                    MedicationCodeType = "ND";
                }
                // Not support by RxRouting 10.6
                //if ((this.msgBody.RefResp.mMedication.DrugCode != null) && (this.msgBody.RefResp.mMedication.DrugCode.DosageForm != null))
                //{
                //    MedicationDosageForm = this.msgBody.RefResp.mMedication.DrugCode.DosageForm;
                //}
                if ((this.msgBody.RefResp.mMedicationPrescribed.DrugCode != null) && (this.msgBody.RefResp.mMedicationPrescribed.DrugCode.Strength != null))
                {
                    MedicationStrength = this.msgBody.RefResp.mMedicationPrescribed.DrugCode.Strength;
                    // Not support by RxRouting 10.6
                    //MedicationStrengthUnits = this.msgBody.RefResp.mMedication.DrugCode.StrengthUnits;
                }
                if ((this.msgBody.RefResp.mMedicationPrescribed.DrugCode != null) && (this.msgBody.RefResp.mMedicationPrescribed.DrugCode.DrugDBCode != null))
                {
                    MedicationDrugDBCode = this.msgBody.RefResp.mMedicationPrescribed.DrugCode.DrugDBCode;
                    MedicationDrugDBCodeQual = this.msgBody.RefResp.mMedicationPrescribed.DrugCode.DrugDBCodeQualifier;
                }
                if (this.msgBody.RefResp.mMedicationPrescribed.Quantity != null)
                {
                    MedicationQuantityQual = this.msgBody.RefResp.mMedicationPrescribed.Quantity.PotencyUnitCode;
                    MedicationQuantity = this.msgBody.RefResp.mMedicationPrescribed.Quantity.Value;
                }
                if (this.msgBody.RefResp.mMedicationPrescribed.DaysSupply != null)
                {
                    MedicationDaysSupply = this.msgBody.RefResp.mMedicationPrescribed.DaysSupply;
                }
                MedicationDirections = this.msgBody.RefResp.mMedicationPrescribed.Directions;


                //if (this.msgBody.RefResp.mMedicationPrescribed.Note != null)
                //{
                //    //MedicationNote = this.msgBody.RefResp.mMedicationPrescribed.Note;
                //    MedicationNote = sPaymentNotes;
                //}

                if (this.msgBody.RefResp.mMedicationPrescribed.Note != null)
                {
                    MedicationNote = "Provider Notes : " + this.msgBody.RefResp.mMedicationPrescribed.Note;

                    if (!(string.IsNullOrEmpty(sPaymentNotes)))
                    {
                        MedicationNote += " ,PDR Notes : " + sPaymentNotes;
                    }
                }
                else
                {
                    if (!(string.IsNullOrEmpty(sPaymentNotes)))
                    {
                        MedicationNote += "PDR Notes : " + sPaymentNotes;
                    }
                }




                if (this.msgBody.RefResp.mMedicationPrescribed.Refills != null)
                {
                    MedicationRefillQual = this.msgBody.RefResp.mMedicationPrescribed.Refills.Qualifier;
                    if (this.msgBody.RefResp.mMedicationPrescribed.Refills.Qualifier != "PRN")
                    {
                        MedicationRefillQuan = this.msgBody.RefResp.mMedicationPrescribed.Refills.Value;
                    }
                }
                MedicationSubstitution = this.msgBody.RefResp.mMedicationPrescribed.Substitutions;
                MedicationWrittenDate = this.msgBody.RefResp.mMedicationPrescribed.WrittenDate.Item.ToString("yyyyMMdd");
                if (this.msgBody.RefResp.mMedicationPrescribed.LastFillDate != null)
                {
                    MedicationLastFillDate = this.msgBody.RefResp.mMedicationPrescribed.LastFillDate.Item.ToString("yyyyMMdd");
                }
                if ((this.msgBody.RefResp.ResponseMsg.ApprovedResp != null) && (this.msgBody.RefResp.ResponseMsg.ApprovedResp.Note != null))
                {
                    ResponseNote = this.msgBody.RefResp.ResponseMsg.ApprovedResp.Note.ToString();
                }
                else if ((this.msgBody.RefResp.ResponseMsg.DeniedResp != null) && (this.msgBody.RefResp.ResponseMsg.DeniedResp.DenialReason != null))
                {
                    ResponseNote = this.msgBody.RefResp.ResponseMsg.DeniedResp.DenialReason.ToString();
                }
                else if ((this.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp != null) && (this.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp.note != null))
                {
                    ResponseNote = this.msgBody.RefResp.ResponseMsg.ApprovedWithChangesResp.note.ToString();
                }
                else if ((this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp != null) && (this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp.note != null))
                {
                    ResponseNote = this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp.note.ToString();
                }

                // Medication dispensed
                if (this.msgBody.RefResp.mMedicationDispensed != null)
                {
                    MedicationDispensedDescription = this.msgBody.RefResp.mMedicationDispensed.DrugDescription;
                    if ((this.msgBody.RefResp.mMedicationDispensed.DrugCode != null) && (this.msgBody.RefResp.mMedicationDispensed.DrugCode.ProductCode != null))
                    {
                        MedicationDispensedCode = this.msgBody.RefResp.mMedicationDispensed.DrugCode.ProductCode;
                        MedicationDispensedCodeType = "ND";
                    }
                    // Not support by RxRouting 10.6
                    //if ((this.msgBody.RefResp.mMedication.DrugCode != null) && (this.msgBody.RefResp.mMedication.DrugCode.DosageForm != null))
                    //{
                    //    MedicationDispensedDosageForm = this.msgBody.RefResp.mMedication.DrugCode.DosageForm;
                    //}
                    if ((this.msgBody.RefResp.mMedicationDispensed.DrugCode != null) && (this.msgBody.RefResp.mMedicationDispensed.DrugCode.Strength != null))
                    {
                        MedicationDispensedStrength = this.msgBody.RefResp.mMedicationDispensed.DrugCode.Strength;
                        // Not support by RxRouting 10.6
                        //MedicationDispensedStrengthUnits = this.msgBody.RefResp.mMedication.DrugCode.StrengthUnits;
                    }
                    if ((this.msgBody.RefResp.mMedicationDispensed.DrugCode != null) && (this.msgBody.RefResp.mMedicationDispensed.DrugCode.DrugDBCode != null))
                    {
                        MedicationDispensedDrugDBCode = this.msgBody.RefResp.mMedicationDispensed.DrugCode.DrugDBCode;
                        MedicationDispensedDrugDBCodeQual = this.msgBody.RefResp.mMedicationDispensed.DrugCode.DrugDBCodeQualifier;
                    }
                    if (this.msgBody.RefResp.mMedicationDispensed.Quantity != null)
                    {
                        MedicationDispensedQuantityQual = this.msgBody.RefResp.mMedicationDispensed.Quantity.PotencyUnitCode;
                        MedicationDispensedQuantity = this.msgBody.RefResp.mMedicationDispensed.Quantity.Value;
                    }
                    if (this.msgBody.RefResp.mMedicationDispensed.DaysSupply != null)
                    {
                        MedicationDispensedDaysSupply = this.msgBody.RefResp.mMedicationDispensed.DaysSupply;
                    }
                    MedicationDispensedDirections = this.msgBody.RefResp.mMedicationDispensed.Directions;
                    if (this.msgBody.RefResp.mMedicationDispensed.Note != null)
                    {
                        MedicationDispensedNote = this.msgBody.RefResp.mMedicationDispensed.Note;
                    }
                    if (this.msgBody.RefResp.mMedicationDispensed.Refills != null)
                    {
                        MedicationDispensedRefillQual = this.msgBody.RefResp.mMedicationDispensed.Refills.Qualifier;
                        if (this.msgBody.RefResp.mMedicationDispensed.Refills.Qualifier != "PRN")
                        {
                            MedicationDispensedRefillQuan = this.msgBody.RefResp.mMedicationDispensed.Refills.Value;
                        }
                    }
                    MedicationDispensedSubstitution = this.msgBody.RefResp.mMedicationDispensed.Substitutions;
                    MedicationDispensedWrittenDate = this.msgBody.RefResp.mMedicationDispensed.WrittenDate.Item.ToString("yyyyMMdd");
                    if (this.msgBody.RefResp.mMedicationDispensed.LastFillDate != null)
                    {
                        MedicationDispensedLastFillDate = this.msgBody.RefResp.mMedicationDispensed.LastFillDate.Item.ToString("yyyyMMdd");
                    }
                }
            }
			else if (this.msgBody.CancelReq != null)
			{
				MessageType = "CancelRx";
				PharmacyID = this.msgBody.CancelReq.mPharmacy.mPharmacyID.NCPDPID;
				if (this.msgBody.CancelReq.mPrescriber.mPrescriberID.SPI != null)
				{
					PrescriberID = this.msgBody.CancelReq.mPrescriber.mPrescriberID.SPI;
				}
				RelatedMessageId = this.msgHeader.RelatesToMessageID;
				PharmacyName = this.msgBody.CancelReq.mPharmacy.mStoreName;
				PharmacyAddress = this.msgBody.CancelReq.mPharmacy.mPharmacyAddress.Address1;
				PharmacyCity = this.msgBody.CancelReq.mPharmacy.mPharmacyAddress.City;
				PharmacyState = this.msgBody.CancelReq.mPharmacy.mPharmacyAddress.State;
				PharmacyZip = this.msgBody.CancelReq.mPharmacy.mPharmacyAddress.Zip;
				PharmacyPhone[0] = this.msgBody.CancelReq.mPharmacy.mPharmacyPhone.mPhone[0].PhoneNumber;
				PharmacyPhoneType[0] = this.msgBody.CancelReq.mPharmacy.mPharmacyPhone.mPhone[0].Qualifier;
				PrescriberClinicName = this.msgBody.CancelReq.mPrescriber.mClinicName;
				if (this.msgBody.CancelReq.mPrescriber.mPrescriberID.SPI != null)
				{
					PrescriberSPI = this.msgBody.CancelReq.mPrescriber.mPrescriberID.SPI;
				}
				if (this.msgBody.CancelReq.mPrescriber.mSpecialty.Qualifier != null)
				{
					PrescriberSpecialtyQual = this.msgBody.CancelReq.mPrescriber.mSpecialty.Qualifier;
					PrescriberSpecialtyCode = this.msgBody.CancelReq.mPrescriber.mSpecialty.SpecialtyCode;
				}
				if ((this.msgBody.CancelReq.mPrescriber.mPrescriberAgent != null) && (this.msgBody.CancelReq.mPrescriber.mPrescriberAgent.LastName != null))
				{
					PrescriberAgentLastName = this.msgBody.CancelReq.mPrescriber.mPrescriberAgent.LastName;
					PrescriberAgentFirstName = this.msgBody.CancelReq.mPrescriber.mPrescriberAgent.FirstName;
				}
				else if ((this.msgBody.CancelReq.mPrescriber.mPrescriberName != null))
				{
					if (this.msgBody.CancelReq.mPrescriber.mPrescriberName.FirstName != null)
					{
						PrescriberAgentFirstName = this.msgBody.CancelReq.mPrescriber.mPrescriberName.FirstName;
					}
					if (this.msgBody.CancelReq.mPrescriber.mPrescriberName.LastName != null)
					{
						PrescriberAgentLastName = this.msgBody.CancelReq.mPrescriber.mPrescriberName.LastName;
					}
				}
				if (this.msgBody.CancelReq.mPrescriber.mPrescriberID != null && !string.IsNullOrEmpty(this.msgBody.CancelReq.mPrescriber.mPrescriberID.DEANumber))
				{
					PrescriberDEA = this.msgBody.CancelReq.mPrescriber.mPrescriberID.DEANumber;
				}
				PrescriberAddress = this.msgBody.CancelReq.mPrescriber.mPrescriberAddress.Address1;
				PrescriberCity = this.msgBody.CancelReq.mPrescriber.mPrescriberAddress.City;
				PrescriberState = this.msgBody.CancelReq.mPrescriber.mPrescriberAddress.State;
				PrescriberZip = this.msgBody.CancelReq.mPrescriber.mPrescriberAddress.Zip;
				if (this.msgBody.CancelReq.mPrescriber.mPrescriberPhone != null &&
					this.msgBody.CancelReq.mPrescriber.mPrescriberPhone.mPhone != null)
				{
					foreach (var each in this.msgBody.CancelReq.mPrescriber.mPrescriberPhone.mPhone)
					{
						if (each.Qualifier == "EM")
						{
							PrescriberEmail = each.PhoneNumber;
							break;
						}
					}
				}
				PrescriberPhone[0] = this.msgBody.CancelReq.mPrescriber.mPrescriberPhone.mPhone[0].PhoneNumber;
				PrescriberPhoneType[0] = this.msgBody.CancelReq.mPrescriber.mPrescriberPhone.mPhone[0].Qualifier;
				PatientID = this.msgBody.CancelReq.mPatient.mPatientID.FileID;
				if (this.msgBody.CancelReq.mPatient.mPatientID.SocialSecurityNumber != null)
				{
					PatientSSN = this.msgBody.CancelReq.mPatient.mPatientID.SocialSecurityNumber;
				}
				PatientLastName = this.msgBody.CancelReq.mPatient.mPatientName.LastName;
				PatientFirstName = this.msgBody.CancelReq.mPatient.mPatientName.FirstName;
				if (this.msgBody.CancelReq.mPatient.mPatientName.Prefix != null)
				{
					PatientPreFix = this.msgBody.CancelReq.mPatient.mPatientName.Prefix;
				}
				PatientGender = this.msgBody.CancelReq.mPatient.mGender;
				// patient DOB is required
				PatientDOB = this.msgBody.CancelReq.mPatient.mDateOfBirth.Item.ToString("yyyyMMdd");
				PatientAddress = this.msgBody.CancelReq.mPatient.mPatientAddress.Address1;
				PatientAddress2 = this.msgBody.CancelReq.mPatient.mPatientAddress.Address2 ?? "";
				PatientCity = this.msgBody.CancelReq.mPatient.mPatientAddress.City;
				PatientState = this.msgBody.CancelReq.mPatient.mPatientAddress.State;
				PatientZip = this.msgBody.CancelReq.mPatient.mPatientAddress.Zip;
				if (this.msgBody.CancelReq.mPatient.mPatientPhone != null &&
					this.msgBody.CancelReq.mPatient.mPatientPhone.mPhone != null)
				{
					foreach (var each in this.msgBody.CancelReq.mPatient.mPatientPhone.mPhone)
					{
						if (each.Qualifier == "EM")
						{
							PatientEmail = each.PhoneNumber;
							break;
						}
					}
				}
				if ((this.msgBody.CancelReq.mPatient.mPatientPhone != null) &&
					(this.msgBody.CancelReq.mPatient.mPatientPhone.mPhone != null) &&
					(this.msgBody.CancelReq.mPatient.mPatientPhone.mPhone.Length > 0))
				{
					PatientPhone[0] = this.msgBody.CancelReq.mPatient.mPatientPhone.mPhone[0].PhoneNumber;
					PatientPhoneType[0] = this.msgBody.CancelReq.mPatient.mPatientPhone.mPhone[0].Qualifier;
				}
				MedicationDescription = this.msgBody.CancelReq.mMedicationPrescribed.DrugDescription;
				if ((this.msgBody.CancelReq.mMedicationPrescribed.DrugCode != null) && (this.msgBody.CancelReq.mMedicationPrescribed.DrugCode.ProductCode != null))
				{
					MedicationCode = this.msgBody.CancelReq.mMedicationPrescribed.DrugCode.ProductCode;
					MedicationCodeType = "ND";
				}
				if (this.msgBody.CancelReq.mMedicationPrescribed.Quantity != null)
				{
					MedicationQuantityQual = this.msgBody.CancelReq.mMedicationPrescribed.Quantity.PotencyUnitCode;
					MedicationQuantity = this.msgBody.CancelReq.mMedicationPrescribed.Quantity.Value;
				}
				if (this.msgBody.CancelReq.mMedicationPrescribed.Directions != null)
				{
					MedicationDirections = this.msgBody.CancelReq.mMedicationPrescribed.Directions;
				}


				if (this.msgBody.CancelReq.mMedicationPrescribed.Note != null)
				{
					MedicationNote = this.msgBody.CancelReq.mMedicationPrescribed.Note;
				}

				MedicationRefillQual = this.msgBody.CancelReq.mMedicationPrescribed.Refills.Qualifier;
				if (this.msgBody.CancelReq.mMedicationPrescribed.Refills.Qualifier != "PRN")
				{
					MedicationRefillQuan = this.msgBody.CancelReq.mMedicationPrescribed.Refills.Value;
				}
				MedicationSubstitution = this.msgBody.CancelReq.mMedicationPrescribed.Substitutions;
				MedicationWrittenDate = this.msgBody.CancelReq.mMedicationPrescribed.WrittenDate.Item.ToString("yyyyMMdd");
			}
            else if (this.msgBody.CancelResp != null)
            {
                MessageType = "CancelRxResponse";
                PharmacyID = this.msgHeader.From.Value;
                PrescriberID = this.msgHeader.To.Value;
                RelatedMessageId = this.msgHeader.RelatesToMessageID;

                ResponseCode = this.msgBody.CancelResp.GetResponseStatus();
                ResponseNote = this.msgBody.CancelResp.GetResponseStatusDescription();
            }
            else if (this.msgBody.ChangeReq != null)
            {
                MessageType = "RxChangeRequest";
                this.DrugID = "";
                PharmacyID = this.msgBody.ChangeReq.mPharmacy.mPharmacyID.NCPDPID;
                RelatedMessageId = this.msgHeader.RelatesToMessageID;
                if (this.msgBody.ChangeReq.Request != null)
                    ChangeType = this.msgBody.ChangeReq.Request.ChangeRequestType;
                PharmacyName = this.msgBody.ChangeReq.mPharmacy.mStoreName;
                if (this.msgBody.ChangeReq.mPharmacy.mPharmacyAddress != null)
                {
                    PharmacyAddress = this.msgBody.ChangeReq.mPharmacy.mPharmacyAddress.Address1;
                    PharmacyCity = this.msgBody.ChangeReq.mPharmacy.mPharmacyAddress.City;
                    PharmacyState = this.msgBody.ChangeReq.mPharmacy.mPharmacyAddress.State;
                    PharmacyZip = this.msgBody.ChangeReq.mPharmacy.mPharmacyAddress.Zip;
                }
                if ((this.msgBody.ChangeReq.mPharmacy.mPharmacyPhone != null) && (this.msgBody.ChangeReq.mPharmacy.mPharmacyPhone.mPhone != null))
                {
                    for (int i = 0; i <= this.msgBody.ChangeReq.mPharmacy.mPharmacyPhone.mPhone.GetUpperBound(0); i++)
                    {
                        PharmacyPhone[i] = this.msgBody.ChangeReq.mPharmacy.mPharmacyPhone.mPhone[i].PhoneNumber;
                        PharmacyPhoneType[i] = this.msgBody.ChangeReq.mPharmacy.mPharmacyPhone.mPhone[i].Qualifier;
                    }
                }
                if (this.msgBody.ChangeReq.mPrescriber.mPrescriberID.SPI != null)
                {
                    PrescriberID = this.msgBody.ChangeReq.mPrescriber.mPrescriberID.SPI;
                }
                if (!(this.msgBody.ChangeReq.mPrescriber.mClinicName == null))
                {
                    PrescriberClinicName = this.msgBody.ChangeReq.mPrescriber.mClinicName;
                }
                PrescriberSPI = this.msgBody.ChangeReq.mPrescriber.mPrescriberID.SPI;
                if (this.msgBody.ChangeReq.mPrescriber.mSpecialty != null)
                {
                    if (this.msgBody.ChangeReq.mPrescriber.mSpecialty.Qualifier != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.ChangeReq.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.ChangeReq.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                }
                if (this.msgBody.ChangeReq.mPrescriber.mPrescriberAgent != null)
                {
                    if (this.msgBody.ChangeReq.mPrescriber.mPrescriberAgent.LastName != null)
                    {
                        PrescriberAgentLastName = this.msgBody.ChangeReq.mPrescriber.mPrescriberAgent.LastName;
                        PrescriberAgentFirstName = this.msgBody.ChangeReq.mPrescriber.mPrescriberAgent.FirstName;
                        if (this.msgBody.ChangeReq.mPrescriber.mPrescriberAgent.MiddleName != null)
                        {
                            PrescriberAgentMiddleName = this.msgBody.ChangeReq.mPrescriber.mPrescriberAgent.MiddleName;
                        }
                    }
                }
                if (this.msgBody.ChangeReq.mPrescriber.mPrescriberName.LastName != null)
                {
                    PrescriberAgentLastName = this.msgBody.ChangeReq.mPrescriber.mPrescriberName.LastName;
                    PrescriberAgentFirstName = this.msgBody.ChangeReq.mPrescriber.mPrescriberName.FirstName;
                    if (this.msgBody.ChangeReq.mPrescriber.mPrescriberName.MiddleName != null)
                    {
                        PrescriberAgentMiddleName = this.msgBody.ChangeReq.mPrescriber.mPrescriberName.MiddleName;
                    }
                }
                if (this.msgBody.ChangeReq.mPrescriber.mPrescriberID != null && !string.IsNullOrEmpty(this.msgBody.ChangeReq.mPrescriber.mPrescriberID.DEANumber))
                {
                    PrescriberDEA = this.msgBody.ChangeReq.mPrescriber.mPrescriberID.DEANumber;
                }
                if (this.msgBody.ChangeReq.mPrescriber.mPrescriberAddress != null)
                {
                    PrescriberAddress = this.msgBody.ChangeReq.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.ChangeReq.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.ChangeReq.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.ChangeReq.mPrescriber.mPrescriberAddress.Zip;
                }
                if (this.msgBody.ChangeReq.mPrescriber.mPrescriberPhone != null &&
                    this.msgBody.ChangeReq.mPrescriber.mPrescriberPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.ChangeReq.mPrescriber.mPrescriberPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PrescriberEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }
                if ((this.msgBody.ChangeReq.mPrescriber.mPrescriberPhone != null) && (this.msgBody.ChangeReq.mPrescriber.mPrescriberPhone.mPhone != null))
                {
                    for (int i = 0; i <= this.msgBody.ChangeReq.mPrescriber.mPrescriberPhone.mPhone.GetUpperBound(0); i++)
                    {
                        PrescriberPhone[i] = this.msgBody.ChangeReq.mPrescriber.mPrescriberPhone.mPhone[i].PhoneNumber;
                        PrescriberPhoneType[i] = this.msgBody.ChangeReq.mPrescriber.mPrescriberPhone.mPhone[i].Qualifier;
                    }
                }
                if (this.msgBody.ChangeReq.mPatient.mPatientID != null)
                {
                    if (this.msgBody.ChangeReq.mPatient.mPatientID.FileID != null)
                    {
                        PatientID = this.msgBody.ChangeReq.mPatient.mPatientID.FileID;
                    }
                    if (this.msgBody.ChangeReq.mPatient.mPatientID.SocialSecurityNumber != null)
                    {
                        PatientSSN = this.msgBody.ChangeReq.mPatient.mPatientID.SocialSecurityNumber;
                    }
                }
                PatientLastName = this.msgBody.ChangeReq.mPatient.mPatientName.LastName;
                PatientFirstName = this.msgBody.ChangeReq.mPatient.mPatientName.FirstName;
                if (this.msgBody.ChangeReq.mPatient.mPatientName.MiddleName != null)
                {
                    PatientMiddleName = this.msgBody.ChangeReq.mPatient.mPatientName.MiddleName;
                }
                if (this.msgBody.ChangeReq.mPatient.mPatientName.Prefix != null)
                {
                    PatientPreFix = this.msgBody.ChangeReq.mPatient.mPatientName.Prefix;
                }
                PatientGender = this.msgBody.ChangeReq.mPatient.mGender;
                // patient DOB is required
                PatientDOB = this.msgBody.ChangeReq.mPatient.mDateOfBirth.Item.ToString("yyyyMMdd");
                if (this.msgBody.ChangeReq.mPatient.mPatientAddress != null)
                {
                    PatientAddress = this.msgBody.ChangeReq.mPatient.mPatientAddress.Address1;
                    PatientAddress2 = this.msgBody.ChangeReq.mPatient.mPatientAddress.Address2;
                    PatientCity = this.msgBody.ChangeReq.mPatient.mPatientAddress.City;
                    PatientState = this.msgBody.ChangeReq.mPatient.mPatientAddress.State;
                    PatientZip = this.msgBody.ChangeReq.mPatient.mPatientAddress.Zip;
                }
                if (this.msgBody.ChangeReq.mPatient.mPatientPhone != null &&
                    this.msgBody.ChangeReq.mPatient.mPatientPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.ChangeReq.mPatient.mPatientPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PatientEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }
                if ((this.msgBody.ChangeReq.mPatient.mPatientPhone != null) && (this.msgBody.ChangeReq.mPatient.mPatientPhone.mPhone != null))
                {
                    for (int i = 0; i <= this.msgBody.ChangeReq.mPatient.mPatientPhone.mPhone.GetUpperBound(0); i++)
                    {
                        PatientPhone[i] = this.msgBody.ChangeReq.mPatient.mPatientPhone.mPhone[i].PhoneNumber;
                        PatientPhoneType[i] = this.msgBody.ChangeReq.mPatient.mPatientPhone.mPhone[i].Qualifier;
                    }
                }
                MedicationDescription = this.msgBody.ChangeReq.mMedicationPrescribed.DrugDescription;
                if (this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode != null)
                {
                    if (this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.ProductCode != null)
                    {
                        MedicationCode = this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.ProductCode;
                        MedicationCodeType = "ND";
                    }
                    // Not support by RxRouting 10.6
                    //if (this.msgBody.ChangeReq.mMedication.DrugCode.DosageForm != null)
                    //{
                    //    MedicationDosageForm = this.msgBody.ChangeReq.mMedication.DrugCode.DosageForm;
                    //}
                    if (this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.Strength != null)
                    {
                        MedicationStrength = this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.Strength;
                        // Not support by RxRouting 10.6
                        //MedicationStrengthUnits = this.msgBody.ChangeReq.mMedication.DrugCode.StrengthUnits;
                    }
                    if (this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.DrugDBCode != null)
                    {
                        MedicationDrugDBCode = this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.DrugDBCode;
                        MedicationDrugDBCodeQual = this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.DrugDBCodeQualifier;
                    }
                }
                if (this.msgBody.ChangeReq.mMedicationPrescribed.Quantity != null)
                {
                    MedicationQuantityQual = this.msgBody.ChangeReq.mMedicationPrescribed.Quantity.PotencyUnitCode;
                    MedicationQuantity = this.msgBody.ChangeReq.mMedicationPrescribed.Quantity.Value;
                }
                if (this.msgBody.ChangeReq.mMedicationPrescribed.DaysSupply != null)
                {
                    MedicationDaysSupply = this.msgBody.ChangeReq.mMedicationPrescribed.DaysSupply;
                }
                MedicationDirections = this.msgBody.ChangeReq.mMedicationPrescribed.Directions;

                //if (this.msgBody.ChangeReq.mMedicationPrescribed.Note != null)
                //{
                //    //MedicationNote = this.msgBody.ChangeReq.mMedicationPrescribed.Note;
                //    MedicationNote = sPaymentNotes;
                //}

                if (this.msgBody.ChangeReq.mMedicationPrescribed.Note != null)
                {
                    MedicationNote = this.msgBody.ChangeReq.mMedicationPrescribed.Note;
                }

                if (this.msgBody.ChangeReq.mMedicationPrescribed.Refills != null)
                {
                    MedicationRefillQual = this.msgBody.ChangeReq.mMedicationPrescribed.Refills.Qualifier;
                    if (this.msgBody.ChangeReq.mMedicationPrescribed.Refills.Qualifier != "PRN")
                    {
                        MedicationRefillQuan = this.msgBody.ChangeReq.mMedicationPrescribed.Refills.Value;
                    }
                }

                MedicationSubstitution = this.msgBody.ChangeReq.mMedicationPrescribed.Substitutions;
                MedicationWrittenDate = this.msgBody.ChangeReq.mMedicationPrescribed.WrittenDate.Item.ToString("yyyyMMdd");
                if (this.msgBody.ChangeReq.mMedicationPrescribed.LastFillDate != null)
                {
                    MedicationLastFillDate = this.msgBody.ChangeReq.mMedicationPrescribed.LastFillDate.Item.ToString("yyyyMMdd");
                }

                if (this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode != null)
                {
                    //C38046 = Unspecified
                    if (this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.DEASchedule != DrugCodedTypeDEASchedule.C38046)
                    {
                        DEASchedulePrescribe = this.msgBody.ChangeReq.mMedicationPrescribed.DrugCode.DEASchedule.ToString();
                    }
                }

                if (this.msgBody.ChangeReq.mMedicationPrescribed.DrugUseEvaluation != null)
                {
                    var sb = new StringBuilder();
                    foreach (var each in this.msgBody.ChangeReq.mMedicationPrescribed.DrugUseEvaluation)
                    {
                        sb.Append(each.ServiceReasonCode);
                        sb.Append(",");
                        sb.Append(each.ProfessionalServiceCode);
                        sb.Append(",");
                        sb.Append(each.ServiceResultCode);
                        sb.Append(",");
                        sb.Append(each.AcknowledgementReason);
                        sb.Append("|");
                    }
                    if (sb.Length > 0)
                        sb.Length--;

                    MedicationDUE = sb.ToString();
                }

                if (this.msgBody.ChangeReq.mBenefit != null)
                {
                    CooIncluded = 1;

                    if (this.msgBody.ChangeReq.mBenefit.PayerIdentification != null)
                    {
                        foreach (var id in this.msgBody.ChangeReq.mBenefit.PayerIdentification)
                        {
                            switch (id.ItemElementName)
                            {
                                case ItemChoiceType1.MutuallyDefined:
                                    CooPayerMutuallydefined = id.Item;
                                    break;
                                case ItemChoiceType1.BINLocationNumber:
                                    CooPayerBinlocationNumber = id.Item;
                                    break;
                                case ItemChoiceType1.ProcessorIdentificationNumber:
                                    CooPayerProcessorIdNumber = id.Item;
                                    break;
                                case ItemChoiceType1.PayerID:
                                    CooPayerId = id.Item;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    CooPayerName = this.msgBody.ChangeReq.mBenefit.PayerName;
                    CooCardholderId = this.msgBody.ChangeReq.mBenefit.CardholderID;

                    if (this.msgBody.ChangeReq.mBenefit.CardHolderName != null)
                    {
                        CooCardholderFirstname = this.msgBody.ChangeReq.mBenefit.CardHolderName.FirstName;
                        CooCardholderLastname = this.msgBody.ChangeReq.mBenefit.CardHolderName.LastName;
                        CooCardholderMiddlename = this.msgBody.ChangeReq.mBenefit.CardHolderName.MiddleName;
                        CooCardholderSuffix = this.msgBody.ChangeReq.mBenefit.CardHolderName.Suffix;
                        CooCardholderPrefix = this.msgBody.ChangeReq.mBenefit.CardHolderName.Prefix;
                    }
                }

                saveSurescriptsMedication("MedicationRequested", InternalMessageID, this.msgBody.ChangeReq.mMedicationRequested);
            }
            else if (this.msgBody.ChangeResp != null)
            {
                SS_Logger.log("=====1.3.1 =====");
                MessageType = "RxChangeResponse";
                PharmacyID = this.msgBody.ChangeResp.mPharmacy.mPharmacyID.NCPDPID;
                PrescriberID = this.msgBody.ChangeResp.mPrescriber.mPrescriberID.SPI;
                RelatedMessageId = this.msgHeader.RelatesToMessageID;
                SS_Logger.log("=====1.3.1.5=====");
                if (this.msgBody.ChangeResp.ResponseMsg.ApprovedResp != null)
                {
                    ResponseCode = "Approved";
                }
                else if (this.msgBody.ChangeResp.ResponseMsg.ApprovedWithChangesResp != null)
                {
                    ResponseCode = "ApprovedWithChanges";
                }
                else if (this.msgBody.ChangeResp.ResponseMsg.DeniedResp != null)
                {
                    ResponseCode = "Denied";
                }
                SS_Logger.log("=====1.3.2 =====");
                PharmacyName = this.msgBody.ChangeResp.mPharmacy.mStoreName;
                if (this.msgBody.ChangeResp.mPharmacy.mPharmacyAddress != null)
                {
                    PharmacyAddress = this.msgBody.ChangeResp.mPharmacy.mPharmacyAddress.Address1;
                    PharmacyCity = this.msgBody.ChangeResp.mPharmacy.mPharmacyAddress.City;
                    PharmacyState = this.msgBody.ChangeResp.mPharmacy.mPharmacyAddress.State;
                    PharmacyZip = this.msgBody.ChangeResp.mPharmacy.mPharmacyAddress.Zip;
                }

                if (this.msgBody.ChangeResp.mPharmacy.mPharmacyPhone.mPhone != null)
                {
                    SS_Logger.log(this.msgBody.ChangeResp.mPharmacy.mPharmacyPhone.mPhone.GetUpperBound(0).ToString());
                    for (int i = 0; i <= this.msgBody.ChangeResp.mPharmacy.mPharmacyPhone.mPhone.GetUpperBound(0); i++)
                    {
                        if (this.msgBody.ChangeResp.mPharmacy.mPharmacyPhone.mPhone[i] != null)
                        {
                            PharmacyPhone[i] = this.msgBody.ChangeResp.mPharmacy.mPharmacyPhone.mPhone[i].PhoneNumber.ToString();
                            PharmacyPhoneType[i] = this.msgBody.ChangeResp.mPharmacy.mPharmacyPhone.mPhone[i].Qualifier.ToString();
                        }
                    }
                }
                if (this.msgBody.ChangeResp.mPrescriber.mClinicName != null)
                {
                    PrescriberClinicName = this.msgBody.ChangeResp.mPrescriber.mClinicName;
                }
                PrescriberSPI = this.msgBody.ChangeResp.mPrescriber.mPrescriberID.SPI;
                if (this.msgBody.ChangeResp.mPrescriber.mSpecialty != null)
                {
                    if (this.msgBody.ChangeResp.mPrescriber.mSpecialty.Qualifier != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.ChangeResp.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.ChangeResp.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                }
                if (this.msgBody.ChangeResp.mPrescriber.mPrescriberAgent != null)
                {
                    if (this.msgBody.ChangeResp.mPrescriber.mPrescriberAgent.LastName != null)
                    {
                        PrescriberAgentLastName = this.msgBody.ChangeResp.mPrescriber.mPrescriberAgent.LastName;
                        PrescriberAgentFirstName = this.msgBody.ChangeResp.mPrescriber.mPrescriberAgent.FirstName;
                    }

                    if (this.msgBody.ChangeResp.mPrescriber.mPrescriberAgent.MiddleName != null)
                    {
                        PrescriberAgentMiddleName = this.msgBody.ChangeResp.mPrescriber.mPrescriberAgent.MiddleName;
                    }
                }
                if (this.msgBody.ChangeResp.mPrescriber.mPrescriberName.LastName != null)
                {
                    PrescriberAgentLastName = this.msgBody.ChangeResp.mPrescriber.mPrescriberName.LastName;
                    PrescriberAgentFirstName = this.msgBody.ChangeResp.mPrescriber.mPrescriberName.FirstName;
                    if (this.msgBody.ChangeResp.mPrescriber.mPrescriberName.MiddleName != null)
                    {
                        PrescriberAgentMiddleName = this.msgBody.ChangeResp.mPrescriber.mPrescriberName.MiddleName;
                    }
                }
                if (this.msgBody.ChangeResp.mPrescriber.mPrescriberID != null && !string.IsNullOrEmpty(this.msgBody.ChangeResp.mPrescriber.mPrescriberID.DEANumber))
                {
                    PrescriberDEA = this.msgBody.ChangeResp.mPrescriber.mPrescriberID.DEANumber;
                }
                if (this.msgBody.ChangeResp.mPrescriber.mPrescriberAddress != null)
                {
                    PrescriberAddress = this.msgBody.ChangeResp.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.ChangeResp.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.ChangeResp.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.ChangeResp.mPrescriber.mPrescriberAddress.Zip;
                }

                if (this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone != null &&
                    this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PrescriberEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }

                if (this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone != null && this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone.mPhone != null)
                {
                    for (int i = 0; i <= this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone.mPhone.GetUpperBound(0); i++)
                    {
                        if (this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone.mPhone[i] != null)
                        {
                            SS_Logger.log("=====1.3.3 =====");
                            PrescriberPhone[i] = this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone.mPhone[i].PhoneNumber;
                            PrescriberPhoneType[i] = this.msgBody.ChangeResp.mPrescriber.mPrescriberPhone.mPhone[i].Qualifier;
                        }
                    }
                }
                if ((this.msgBody.ChangeResp.mPatient.mPatientID != null) && (this.msgBody.ChangeResp.mPatient.mPatientID.FileID != null))
                {
                    PatientID = this.msgBody.ChangeResp.mPatient.mPatientID.FileID;
                    if (this.msgBody.ChangeResp.mPatient.mPatientID.SocialSecurityNumber != null)
                    {
                        PatientSSN = this.msgBody.ChangeResp.mPatient.mPatientID.SocialSecurityNumber;
                    }
                }

                if (this.msgBody.ChangeResp.mPatient.mPatientName != null)
                {
                    if (this.msgBody.ChangeResp.mPatient.mPatientName.LastName != null)
                    {
                        PatientLastName = this.msgBody.ChangeResp.mPatient.mPatientName.LastName;
                    }
                    if (this.msgBody.ChangeResp.mPatient.mPatientName.FirstName != null)
                    {
                        PatientFirstName = this.msgBody.ChangeResp.mPatient.mPatientName.FirstName;
                    }
                    if (this.msgBody.ChangeResp.mPatient.mPatientName.MiddleName != null)
                    {
                        PatientMiddleName = this.msgBody.ChangeResp.mPatient.mPatientName.MiddleName;
                    }
                    if (this.msgBody.ChangeResp.mPatient.mPatientName.Prefix != null)
                    {
                        PatientPreFix = this.msgBody.ChangeResp.mPatient.mPatientName.Prefix;
                    }
                }
                PatientGender = this.msgBody.ChangeResp.mPatient.mGender ?? "";
                // patient DOB is required
                PatientDOB = this.msgBody.ChangeResp.mPatient.mDateOfBirth.Item.ToString("yyyyMMdd");
                if (this.msgBody.ChangeResp.mPatient.mPatientAddress != null)
                {
                    PatientAddress = this.msgBody.ChangeResp.mPatient.mPatientAddress.Address1 ?? "";
                    PatientAddress2 = this.msgBody.ChangeResp.mPatient.mPatientAddress.Address2 ?? "";
                    PatientCity = this.msgBody.ChangeResp.mPatient.mPatientAddress.City ?? "";
                    PatientState = this.msgBody.ChangeResp.mPatient.mPatientAddress.State ?? "";
                    PatientZip = this.msgBody.ChangeResp.mPatient.mPatientAddress.Zip ?? "";
                }
                if (this.msgBody.ChangeResp.mPatient.mPatientPhone != null &&
                    this.msgBody.ChangeResp.mPatient.mPatientPhone.mPhone != null)
                {
                    foreach (var each in this.msgBody.ChangeResp.mPatient.mPatientPhone.mPhone)
                    {
                        if (each.Qualifier == "EM")
                        {
                            PatientEmail = each.PhoneNumber;
                            break;
                        }
                    }
                }
                if ((this.msgBody.ChangeResp.mPatient.mPatientPhone != null) && (this.msgBody.ChangeResp.mPatient.mPatientPhone.mPhone != null))
                {
                    for (int i = 0; i <= this.msgBody.ChangeResp.mPatient.mPatientPhone.mPhone.GetUpperBound(0); i++)
                    {
                        if (this.msgBody.ChangeResp.mPatient.mPatientPhone.mPhone[i] != null)
                        {
                            SS_Logger.log("=====1.3.4 =====");
                            PatientPhone[i] = this.msgBody.ChangeResp.mPatient.mPatientPhone.mPhone[i].PhoneNumber ?? "";
                            PatientPhoneType[i] = this.msgBody.ChangeResp.mPatient.mPatientPhone.mPhone[i].Qualifier ?? "";
                        }
                    }
                }

                if (this.msgBody.ChangeResp.mMedicationPrescribed != null)
                {
                    MedicationDescription = this.msgBody.ChangeResp.mMedicationPrescribed.DrugDescription;
                    if ((this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode != null) && (this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode.ProductCode != null))
                    {
                        MedicationCode = this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode.ProductCode;
                        MedicationCodeType = "ND";
                    }
                    if ((this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode != null) && (this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode.Strength != null))
                    {
                        MedicationStrength = this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode.Strength;
                    }
                    if ((this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode != null) && (this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode.DrugDBCode != null))
                    {
                        MedicationDrugDBCode = this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode.DrugDBCode;
                        MedicationDrugDBCodeQual = this.msgBody.ChangeResp.mMedicationPrescribed.DrugCode.DrugDBCodeQualifier;
                    }
                    if (this.msgBody.ChangeResp.mMedicationPrescribed.Quantity != null)
                    {
                        MedicationQuantityQual = this.msgBody.ChangeResp.mMedicationPrescribed.Quantity.PotencyUnitCode;
                        MedicationQuantity = this.msgBody.ChangeResp.mMedicationPrescribed.Quantity.Value;
                    }
                    if (this.msgBody.ChangeResp.mMedicationPrescribed.DaysSupply != null)
                    {
                        MedicationDaysSupply = this.msgBody.ChangeResp.mMedicationPrescribed.DaysSupply;
                    }
                    MedicationDirections = this.msgBody.ChangeResp.mMedicationPrescribed.Directions;

                    if (this.msgBody.ChangeResp.mMedicationPrescribed.Note != null)
                    {
                        MedicationNote = this.msgBody.ChangeResp.mMedicationPrescribed.Note;
                    }

                    if (this.msgBody.ChangeResp.mMedicationPrescribed.Refills != null)
                    {
                        MedicationRefillQual = this.msgBody.ChangeResp.mMedicationPrescribed.Refills.Qualifier;
                        if (this.msgBody.ChangeResp.mMedicationPrescribed.Refills.Qualifier != "PRN")
                        {
                            MedicationRefillQuan = this.msgBody.ChangeResp.mMedicationPrescribed.Refills.Value;
                        }
                    }
                    MedicationSubstitution = this.msgBody.ChangeResp.mMedicationPrescribed.Substitutions;
                    MedicationWrittenDate = this.msgBody.ChangeResp.mMedicationPrescribed.WrittenDate.Item.ToString("yyyyMMdd");
                    if (this.msgBody.ChangeResp.mMedicationPrescribed.LastFillDate != null)
                    {
                        MedicationLastFillDate = this.msgBody.ChangeResp.mMedicationPrescribed.LastFillDate.Item.ToString("yyyyMMdd");
                    }
                }

                if ((this.msgBody.ChangeResp.ResponseMsg.ApprovedResp != null) && (this.msgBody.ChangeResp.ResponseMsg.ApprovedResp.Note != null))
                {
                    ResponseNote = this.msgBody.ChangeResp.ResponseMsg.ApprovedResp.Note.ToString();
                }
                else if ((this.msgBody.ChangeResp.ResponseMsg.DeniedResp != null) && (this.msgBody.ChangeResp.ResponseMsg.DeniedResp.DenialReason != null))
                {
                    ResponseNote = this.msgBody.ChangeResp.ResponseMsg.DeniedResp.DenialReason.ToString();
                }
                else if ((this.msgBody.ChangeResp.ResponseMsg.ApprovedWithChangesResp != null) && (this.msgBody.ChangeResp.ResponseMsg.ApprovedWithChangesResp.note != null))
                {
                    ResponseNote = this.msgBody.ChangeResp.ResponseMsg.ApprovedWithChangesResp.note.ToString();
                }
            }
            else
            {
                if (this.msgBody.Status != null)
                {
                    if (this.msgBody.Status.Code == "010")
                    {
                        this.UpdateStatus(InternalMessageID, "OK", "Submission Successful Confirmed");
                    }
                    else if (this.msgBody.Status.Code == "000")
                    {
                        this.UpdateStatus(InternalMessageID, "OK", "Submitted To Next Receiver.");
                    }
                    else
                    {
                        this.UpdateStatus(InternalMessageID, "Error", this.msgBody.Status.Code.ToString());
                    }
                    return "";
                }
                if (this.msgBody.Error != null)
                {
                    this.UpdateStatus(InternalMessageID, "Error", this.msgBody.Error.DescriptionCode.ToString() + "-" + this.msgBody.Error.Description.ToString());
                    return "";
                }
                if (this.msgBody.DirDwnld != null)
                {
                    MessageType = "DirectoryDownload";
                }
                else if (this.msgBody.DirDwnldResponse != null)
                {
                    MessageType = "DirectoryDownloadResponse";
                    ResponseCode = "OK";
                    MsgContent = this.msgBody.DirDwnldResponse.URL.ToString();
                }
                else if (this.msgBody.AddPrescriber != null || this.msgBody.AddPrescriberLocation != null)
                {
                    DSPrescriber prescriber = null;
                    if (this.msgBody.AddPrescriber != null)
                    {
                        MessageType = "AddPrescriber";
                        prescriber = this.msgBody.AddPrescriber;
                    }
                    else
                    {
                        MessageType = "AddPrescriberLocation";
                        prescriber = this.msgBody.AddPrescriberLocation;
                    }

                    if (prescriber.mPrescriber.mSpecialty != null)
                    {
                        PrescriberSpecialtyQual = prescriber.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = prescriber.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                    PrescriberAddress = prescriber.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = prescriber.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = prescriber.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = prescriber.mPrescriber.mPrescriberAddress.Zip;
                    PrescriberEmail = prescriber.mPrescriber.mPrescriberEmail;
                    PrescriberPhone[0] = prescriber.mPrescriber.mDSPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType[0] = prescriber.mPrescriber.mDSPrescriberPhone.mPhone[0].Qualifier;
                    if ((prescriber.mPrescriber.mDirectoryInfo != null) && (prescriber.mPrescriber.mDirectoryInfo.ServiceLevel != null))
                    {
                        PrescriberServiceLevel = prescriber.mPrescriber.mDirectoryInfo.ServiceLevel;
                    }
                }
                else if (this.msgBody.UpdatePrescriber != null)
                {
                    MessageType = "UpdatePrescriber";
                    if (this.msgBody.UpdatePrescriber.mPrescriber.mSpecialty != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.UpdatePrescriber.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.UpdatePrescriber.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                    PrescriberAddress = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberAddress.Zip;
                    PrescriberEmail = this.msgBody.UpdatePrescriber.mPrescriber.mPrescriberEmail;
                    PrescriberPhone[0] = this.msgBody.UpdatePrescriber.mPrescriber.mDSPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType[0] = this.msgBody.UpdatePrescriber.mPrescriber.mDSPrescriberPhone.mPhone[0].Qualifier;
                    if ((this.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo != null) && (this.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel != null))
                    {
                        PrescriberServiceLevel = this.msgBody.UpdatePrescriber.mPrescriber.mDirectoryInfo.ServiceLevel;
                    }
                }
                else if (this.msgBody.AddPrescriberResp != null)
                {
                    MessageType = "AddPrescriberResp";
                    if (this.msgBody.AddPrescriberResp.mPrescriber.mSpecialty != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.AddPrescriberResp.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.AddPrescriberResp.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                    PrescriberAgentLastName = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAgent.LastName;
                    PrescriberAgentFirstName = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAgent.FirstName;
                    PrescriberAddress = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberAddress.Zip;
                    PrescriberEmail = this.msgBody.AddPrescriberResp.mPrescriber.mPrescriberEmail;
                    PrescriberPhone[0] = this.msgBody.AddPrescriberResp.mPrescriber.mDSPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType[0] = this.msgBody.AddPrescriberResp.mPrescriber.mDSPrescriberPhone.mPhone[0].Qualifier;
                    if ((this.msgBody.AddPrescriberResp.mPrescriber.mDirectoryInfo != null) && (this.msgBody.AddPrescriberResp.mPrescriber.mDirectoryInfo.ServiceLevel != null))
                    {
                        PrescriberServiceLevel = this.msgBody.AddPrescriberResp.mPrescriber.mDirectoryInfo.ServiceLevel;
                    }
                }
                else if (this.msgBody.AddPrescriberLocationResponse != null)
                {
                    MessageType = "AddPrescriberLocationResponse";
                    if (this.msgBody.AddPrescriberLocationResponse.mPrescriber.mSpecialty != null)
                    {
                        PrescriberSpecialtyQual = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mSpecialty.Qualifier;
                        PrescriberSpecialtyCode = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mSpecialty.SpecialtyCode;
                    }
                    PrescriberAgentLastName = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberAgent.LastName;
                    PrescriberAgentFirstName = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberAgent.FirstName;
                    PrescriberAddress = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberAddress.Address1;
                    PrescriberCity = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberAddress.City;
                    PrescriberState = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberAddress.State;
                    PrescriberZip = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberAddress.Zip;
                    PrescriberEmail = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mPrescriberEmail;
                    PrescriberPhone[0] = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mDSPrescriberPhone.mPhone[0].PhoneNumber;
                    PrescriberPhoneType[0] = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mDSPrescriberPhone.mPhone[0].Qualifier;
                    if ((this.msgBody.AddPrescriberLocationResponse.mPrescriber.mDirectoryInfo != null) && (this.msgBody.AddPrescriberLocationResponse.mPrescriber.mDirectoryInfo.ServiceLevel != null))
                    {
                        PrescriberServiceLevel = this.msgBody.AddPrescriberLocationResponse.mPrescriber.mDirectoryInfo.ServiceLevel;
                    }
                }
            }
            SS_Logger.log("=====1.4 =====");
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.SaveMessage";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = InternalMessageID ?? "";
            command.Parameters.Add(new OracleParameter("ParamPRESCRIBERORDERNUMBER", OracleType.VarChar)).Value = OrderNumber ?? "";
            command.Parameters.Add(new OracleParameter("ParamRXREFERENCENUMBER", OracleType.VarChar)).Value = RXReferenceNumber ?? "";
            command.Parameters.Add(new OracleParameter("ParamRelatedMessageId", OracleType.VarChar)).Value = RelatedMessageId ?? "";
            command.Parameters.Add(new OracleParameter("ParamPHARMACYID", OracleType.VarChar)).Value = PharmacyID ?? "";
            command.Parameters.Add(new OracleParameter("ParamPRESCRIBERID", OracleType.VarChar)).Value = PrescriberID ?? "";
            command.Parameters.Add(new OracleParameter("ParamSUPERVISORID", OracleType.VarChar)).Value = "";
            command.Parameters.Add(new OracleParameter("ParamDRUGID", OracleType.VarChar)).Value = this.DrugID ?? "";
            command.Parameters.Add(new OracleParameter("ParamMSGCONTENT", OracleType.VarChar)).Value = MsgContent ?? "";
            command.Parameters.Add(new OracleParameter("ParamMESSAGETYPE", OracleType.VarChar)).Value = MessageType ?? "";
            command.Parameters.Add(new OracleParameter("ParamRESPONSECODE", OracleType.VarChar)).Value = ResponseCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamCHANGETYPE", OracleType.VarChar)).Value = ChangeType ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyName", OracleType.VarChar)).Value = PharmacyName.ToString().Replace("'", "") ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyAddress", OracleType.VarChar)).Value = PharmacyAddress ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyCity", OracleType.VarChar)).Value = PharmacyCity ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyState", OracleType.VarChar)).Value = PharmacyState ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyZip", OracleType.VarChar)).Value = PharmacyZip ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone", OracleType.VarChar)).Value = PharmacyPhone[0] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType", OracleType.VarChar)).Value = PharmacyPhoneType[0] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone2", OracleType.VarChar)).Value = PharmacyPhone[1] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType2", OracleType.VarChar)).Value = PharmacyPhoneType[1] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone3", OracleType.VarChar)).Value = PharmacyPhone[2] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType3", OracleType.VarChar)).Value = PharmacyPhoneType[2] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone4", OracleType.VarChar)).Value = PharmacyPhone[3] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType4", OracleType.VarChar)).Value = PharmacyPhoneType[3] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone5", OracleType.VarChar)).Value = PharmacyPhone[4] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType5", OracleType.VarChar)).Value = PharmacyPhoneType[4] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone6", OracleType.VarChar)).Value = PharmacyPhone[5] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType6", OracleType.VarChar)).Value = PharmacyPhoneType[5] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone7", OracleType.VarChar)).Value = PharmacyPhone[6] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType7", OracleType.VarChar)).Value = PharmacyPhoneType[6] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhone8", OracleType.VarChar)).Value = PharmacyPhone[7] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPharmacyPhoneType8", OracleType.VarChar)).Value = PharmacyPhoneType[7] ?? "";
            command.Parameters.Add(new OracleParameter("paramPrescriberClinicName", OracleType.VarChar)).Value = PrescriberClinicName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberSpecialtyQual", OracleType.VarChar)).Value = PrescriberSpecialtyQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberSpecialtyCode", OracleType.VarChar)).Value = PrescriberSpecialtyCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberAgentLastName", OracleType.VarChar)).Value = PrescriberAgentLastName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberAgentFirstName", OracleType.VarChar)).Value = PrescriberAgentFirstName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberAddress", OracleType.VarChar)).Value = PrescriberAddress ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberCity", OracleType.VarChar)).Value = PrescriberCity ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberState", OracleType.VarChar)).Value = PrescriberState ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberZip", OracleType.VarChar)).Value = PrescriberZip ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberEmail", OracleType.VarChar)).Value = PrescriberEmail ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone", OracleType.VarChar)).Value = PrescriberPhone[0] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType", OracleType.VarChar)).Value = PrescriberPhoneType[0] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone2", OracleType.VarChar)).Value = PrescriberPhone[1] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType2", OracleType.VarChar)).Value = PrescriberPhoneType[1] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone3", OracleType.VarChar)).Value = PrescriberPhone[2] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType3", OracleType.VarChar)).Value = PrescriberPhoneType[2] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone4", OracleType.VarChar)).Value = PrescriberPhone[3] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType4", OracleType.VarChar)).Value = PrescriberPhoneType[3] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone5", OracleType.VarChar)).Value = PrescriberPhone[4] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType5", OracleType.VarChar)).Value = PrescriberPhoneType[4] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone6", OracleType.VarChar)).Value = PrescriberPhone[5] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType6", OracleType.VarChar)).Value = PrescriberPhoneType[5] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone7", OracleType.VarChar)).Value = PrescriberPhone[6] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType7", OracleType.VarChar)).Value = PrescriberPhoneType[6] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhone8", OracleType.VarChar)).Value = PrescriberPhone[7] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberPhoneType8", OracleType.VarChar)).Value = PrescriberPhoneType[7] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientID", OracleType.VarChar)).Value = PatientID ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientSSN", OracleType.VarChar)).Value = PatientSSN ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientLastName", OracleType.VarChar)).Value = PatientLastName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientFirstName", OracleType.VarChar)).Value = PatientFirstName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPreFix", OracleType.VarChar)).Value = PatientPreFix ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientGender", OracleType.VarChar)).Value = PatientGender ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientDOB", OracleType.VarChar)).Value = PatientDOB ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientAddress", OracleType.VarChar)).Value = PatientAddress ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientAddress2", OracleType.VarChar)).Value = PatientAddress2 ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientCity", OracleType.VarChar)).Value = PatientCity ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientState", OracleType.VarChar)).Value = PatientState ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientZip", OracleType.VarChar)).Value = PatientZip ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientEmail", OracleType.VarChar)).Value = PatientEmail ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone", OracleType.VarChar)).Value = PatientPhone[0] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType", OracleType.VarChar)).Value = PatientPhoneType[0] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone2", OracleType.VarChar)).Value = PatientPhone[1] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType2", OracleType.VarChar)).Value = PatientPhoneType[1] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone3", OracleType.VarChar)).Value = PatientPhone[2] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType3", OracleType.VarChar)).Value = PatientPhoneType[2] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone4", OracleType.VarChar)).Value = PatientPhone[3] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType4", OracleType.VarChar)).Value = PatientPhoneType[3] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone5", OracleType.VarChar)).Value = PatientPhone[4] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType5", OracleType.VarChar)).Value = PatientPhoneType[4] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone6", OracleType.VarChar)).Value = PatientPhone[5] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType6", OracleType.VarChar)).Value = PatientPhoneType[5] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone7", OracleType.VarChar)).Value = PatientPhone[6] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType7", OracleType.VarChar)).Value = PatientPhoneType[6] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhone8", OracleType.VarChar)).Value = PatientPhone[7] ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientPhoneType8", OracleType.VarChar)).Value = PatientPhoneType[7] ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDescription", OracleType.VarChar)).Value = MedicationDescription ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationCode", OracleType.VarChar)).Value = MedicationCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationCodeType", OracleType.VarChar)).Value = MedicationCodeType ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationQuantityQual", OracleType.VarChar)).Value = MedicationQuantityQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationQuantity", OracleType.VarChar)).Value = MedicationQuantity ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDirections", OracleType.VarChar)).Value = MedicationDirections ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationNote", OracleType.VarChar)).Value = MedicationNote ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationRefillQual", OracleType.VarChar)).Value = MedicationRefillQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationRefillQuan", OracleType.VarChar)).Value = MedicationRefillQuan ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationSubstitution", OracleType.VarChar)).Value = MedicationSubstitution ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationWrittenDate", OracleType.VarChar)).Value = MedicationWrittenDate ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberServiceLevel", OracleType.VarChar)).Value = PrescriberServiceLevel ?? "";
            command.Parameters.Add(new OracleParameter("ParamSenderMessageID", OracleType.VarChar)).Value = this.msgHeader.MessageID ?? "";
            command.Parameters.Add(new OracleParameter("ParamNote", OracleType.VarChar)).Value = ResponseNote ?? "";
            command.Parameters.Add(new OracleParameter("ParamPatientMiddleName", OracleType.VarChar)).Value = PatientMiddleName ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberMiddleName", OracleType.VarChar)).Value = PrescriberAgentMiddleName ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDosageForm", OracleType.VarChar)).Value = MedicationDosageForm ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationStrength", OracleType.VarChar)).Value = MedicationStrength ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationStrengthUnits", OracleType.VarChar)).Value = MedicationStrengthUnits ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDrugDBCode", OracleType.VarChar)).Value = MedicationDrugDBCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDrugDBCodeQual", OracleType.VarChar)).Value = MedicationDrugDBCodeQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDaysSupply", OracleType.VarChar)).Value = MedicationDaysSupply ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationLastFillDate", OracleType.VarChar)).Value = MedicationLastFillDate ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationCliInfoQual", OracleType.VarChar)).Value = MedicationLastFillDate ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationPrimaryQual", OracleType.VarChar)).Value = MedicationPrimaryQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationPrimaryVal", OracleType.VarChar)).Value = MedicationPrimaryVal ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationSecondaryQual", OracleType.VarChar)).Value = MedicationSecondaryQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationSecondaryVal", OracleType.VarChar)).Value = MedicationSecondaryVal ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseDescription", OracleType.VarChar)).Value = MedicationDispensedDescription ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseCode", OracleType.VarChar)).Value = MedicationDispensedCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseCodeType", OracleType.VarChar)).Value = MedicationDispensedCodeType ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseQuantityQual", OracleType.VarChar)).Value = MedicationDispensedQuantityQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseQuantity", OracleType.VarChar)).Value = MedicationDispensedQuantity ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseDirections", OracleType.VarChar)).Value = MedicationDispensedDirections ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseNote", OracleType.VarChar)).Value = MedicationDispensedNote ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseRefillQual", OracleType.VarChar)).Value = MedicationDispensedRefillQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseRefillQuan", OracleType.VarChar)).Value = MedicationDispensedRefillQuan ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseSubstitution", OracleType.VarChar)).Value = MedicationDispensedSubstitution ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseWrittenDate", OracleType.VarChar)).Value = MedicationDispensedWrittenDate ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseDosageForm", OracleType.VarChar)).Value = MedicationDispensedDosageForm ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseStrength", OracleType.VarChar)).Value = MedicationDispensedStrength ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseStrengthUnits", OracleType.VarChar)).Value = MedicationDispensedStrengthUnits ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseDrugDBCode", OracleType.VarChar)).Value = MedicationDispensedDrugDBCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseDrugDBCodeQual", OracleType.VarChar)).Value = MedicationDispensedDrugDBCodeQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseDaysSupply", OracleType.VarChar)).Value = MedicationDispensedDaysSupply ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseLastFillDate", OracleType.VarChar)).Value = MedicationDispensedLastFillDate ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseCliInfoQual", OracleType.VarChar)).Value = MedicationDispensedLastFillDate ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispensePrimaryQual", OracleType.VarChar)).Value = MedicationDispensedPrimaryQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispensePrimaryVal", OracleType.VarChar)).Value = MedicationDispensedPrimaryVal ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseSecondaryQual", OracleType.VarChar)).Value = MedicationDispensedSecondaryQual ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedDispenseSecondaryVal", OracleType.VarChar)).Value = MedicationDispensedSecondaryVal ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberNPI", OracleType.VarChar)).Value = PrescriberNPI ?? "";
            command.Parameters.Add(new OracleParameter("ParamPrescriberDEA", OracleType.VarChar)).Value = PrescriberDEA ?? "";
            command.Parameters.Add(new OracleParameter("ParamDEASchedule", OracleType.VarChar)).Value = DEASchedule ?? "";
            command.Parameters.Add(new OracleParameter("ParamDEASchedulePrescribe", OracleType.VarChar)).Value = DEASchedulePrescribe ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooIncluded", OracleType.Number)).Value = CooIncluded;
            command.Parameters.Add(new OracleParameter("ParamCooPayerMutuallydefined", OracleType.VarChar)).Value = CooPayerMutuallydefined ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooPayerBinlocationNumber", OracleType.VarChar)).Value = CooPayerBinlocationNumber ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooPayerProcessorIdNumber", OracleType.VarChar)).Value = CooPayerProcessorIdNumber ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooPayerId", OracleType.VarChar)).Value = CooPayerId ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooPayerName", OracleType.VarChar)).Value = CooPayerName ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooCardholderId", OracleType.VarChar)).Value = CooCardholderId ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooCardholderFirstname", OracleType.VarChar)).Value = CooCardholderFirstname ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooCardholderLastname", OracleType.VarChar)).Value = CooCardholderLastname ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooCardholderMiddlename", OracleType.VarChar)).Value = CooCardholderMiddlename ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooCardholderSuffix", OracleType.VarChar)).Value = CooCardholderSuffix ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooCardholderPrefix", OracleType.VarChar)).Value = CooCardholderPrefix ?? "";
            command.Parameters.Add(new OracleParameter("ParamCooGroupId", OracleType.VarChar)).Value = CooGroupId ?? "";
            command.Parameters.Add(new OracleParameter("ParamMedicationDUE", OracleType.VarChar)).Value = MedicationDUE ?? "";
            command.Parameters.Add(new OracleParameter("ParamSaveFlag", OracleType.VarChar, 40, string.Empty)).Direction = ParameterDirection.Output;
            SS_Logger.log("=====Message Save() Parms =====");
            foreach (OracleParameter parm in command.Parameters)
            {
                SS_Logger.log(parm.ParameterName + "=|" + parm.Value + "|");
            }
            SS_Logger.log("=====End Message Save() Parms =====");

            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                SS_Logger.log("Error in message save: " + e.StackTrace);

                if (e.InnerException != null)
                {
                    SS_Logger.log("Inner Error in message save: " + e.InnerException.StackTrace);

                }

                throw e;
            }
            finally
            {
                connection.Close();
            }
            return command.Parameters["ParamSaveFlag"].Value.ToString();
        }

        public void UpdatePrescriberSPI(string prescriberID, string SPI, string serviceLevel)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.SavePrescriberInfo";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPrescriberID", OracleType.VarChar)).Value = prescriberID;
            command.Parameters.Add(new OracleParameter("ParamSPI", OracleType.VarChar)).Value = SPI;
            command.Parameters.Add(new OracleParameter("ParamServiceLevel", OracleType.VarChar)).Value = serviceLevel;
            command.ExecuteOracleScalar();
            connection.Close();
        }

        public void UpdateStatus(string InternalMessageID, string StatusCode, string StatusDesc)
        {
            string MessageID;
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            if (this.msgBody.RefReq != null ||
                this.msgBody.CancelResp != null ||
                this.msgBody.ChangeReq != null)
            {
                MessageID = InternalMessageID;
            }
            else
            {
                MessageID = this.msgHeader.RelatesToMessageID.ToString();
            }
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.UpdateStatus";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = MessageID;
            command.Parameters.Add(new OracleParameter("ParamStatusCode", OracleType.VarChar)).Value = StatusCode;
            command.Parameters.Add(new OracleParameter("ParamStatusDesc", OracleType.VarChar)).Value = StatusDesc;
            command.ExecuteOracleScalar();
            connection.Close();
        }

        public void UpdateCancelStatus(string statusCode, string statusDesc)
        {
            string cancelRequestId = string.Empty;
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();

            if (this.msgBody.CancelReq != null)
            {
                cancelRequestId = this.msgHeader.MessageID;
            }
            else if (this.msgBody.Status != null ||
                this.msgBody.Error != null ||
                this.msgBody.Verify != null ||
                this.msgBody.CancelResp != null)
            {
                cancelRequestId = this.msgHeader.RelatesToMessageID;
            }

            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.UpdateCancelStatus";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamCancelReqID", OracleType.VarChar)).Value = cancelRequestId;
            command.Parameters.Add(new OracleParameter("ParamStatusCode", OracleType.VarChar)).Value = statusCode;
            command.Parameters.Add(new OracleParameter("ParamStatusDesc", OracleType.VarChar)).Value = statusDesc;
            command.ExecuteOracleScalar();
            connection.Close();
        }

        public void SaveEPCSPrescription(string drugId, string visitkey, string doctorId, string pharmacyNCPDPID, string patientId, string status, string ssMessageId, byte[] encryptedContent, byte[] encryptedKey, byte[] encryptedIV, byte[] signature)
        {
            string MessageID;
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            if (this.msgBody.RefReq != null)
            {
                MessageID = ssMessageId;
            }
            else
            {
                MessageID = this.msgHeader.RelatesToMessageID.ToString();
            }
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSEPCS.SaveEPCSPrescription";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamDrugId", OracleType.VarChar)).Value = drugId;
            command.Parameters.Add(new OracleParameter("ParamVisitKey", OracleType.VarChar)).Value = visitkey;
            command.Parameters.Add(new OracleParameter("ParamNCPDPID", OracleType.VarChar)).Value = pharmacyNCPDPID;
            command.Parameters.Add(new OracleParameter("ParamSSMessageId", OracleType.VarChar)).Value = MessageID;
            command.Parameters.Add(new OracleParameter("ParamStatus", OracleType.VarChar)).Value = status;
            command.Parameters.Add(new OracleParameter("ParamDoctorId", OracleType.VarChar)).Value = doctorId;
            command.Parameters.Add(new OracleParameter("ParamPatientId", OracleType.VarChar)).Value = patientId;
            command.Parameters.Add(new OracleParameter("ParamEncryptedContent", OracleType.Blob)).Value = encryptedContent;
            command.Parameters.Add(new OracleParameter("ParamEncryptedKey", OracleType.Blob)).Value = encryptedKey;
            command.Parameters.Add(new OracleParameter("ParamEncryptedIV", OracleType.Blob)).Value = encryptedIV;
            command.Parameters.Add(new OracleParameter("ParamSignature", OracleType.Blob)).Value = signature;
            command.ExecuteOracleScalar();
            connection.Close();
        }

        public static void SaveEPCSAuditLog(string eventType, string action, string author, string role, string detail, string result)
        {
            string MessageID;
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSEPCS.SaveEPCSAuditLog";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamEventType", OracleType.VarChar)).Value = eventType;
            command.Parameters.Add(new OracleParameter("ParamAction", OracleType.VarChar)).Value = action;
            command.Parameters.Add(new OracleParameter("ParamAuthor", OracleType.VarChar)).Value = author;
            command.Parameters.Add(new OracleParameter("ParamRole", OracleType.VarChar)).Value = role;
            command.Parameters.Add(new OracleParameter("ParamDetail", OracleType.VarChar)).Value = detail;
            command.Parameters.Add(new OracleParameter("ParamResult", OracleType.VarChar)).Value = result;
            command.ExecuteOracleScalar();
            connection.Close();
        }

        public void UpdateUnreadStatusOfRelatedMessage(bool isUnread)
        {
            string messageID = this.msgHeader.RelatesToMessageID.ToString();
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.UpdateUnreadStatus";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = messageID;
            command.Parameters.Add(new OracleParameter("ParamIsUnread", OracleType.Char)).Value = isUnread ? "1" : null;
            command.ExecuteNonQuery();
            connection.Close();
        }

        public string CheckForControlledSubstance(string NDCCode)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.CheckForControlledSubstance";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamNDCCode", OracleType.VarChar)).Value = NDCCode ?? "";
            command.Parameters.Add(new OracleParameter("ParamCSASCHEDULE", OracleType.VarChar, 40, string.Empty)).Direction = ParameterDirection.Output;

            command.ExecuteNonQuery();
            connection.Close();

            return command.Parameters["ParamCSASCHEDULE"].Value.ToString();

        }

        public void SetRefillResponseFromClone(string[] inputParams)
        {
            this.SetRefillResponseFromClone(inputParams, false, "");
        }
        public void SetRefillResponseFromClone(string[] inputParams, bool isEPCS, string dpsNumber)
        {
            if (inputParams[3] != null)
            {
                this.DrugID = inputParams[3];
            }
            if (inputParams[6] != null)
            {
                this.msgBody.RefResp.ResponseMsg = new ResponseType(inputParams);
            }

            if (inputParams[9] != null)
            {
                this.msgBody.RefResp.mMedicationPrescribed.Refills = new QualifierValuePair();
                this.msgBody.RefResp.mMedicationPrescribed.Refills.Value = inputParams[9];
                if (this.msgBody.RefResp.mMedicationPrescribed.Refills.Value == "999")
                {
                    this.msgBody.RefResp.mMedicationPrescribed.Refills.Qualifier = "PRN";
                    this.msgBody.RefResp.mMedicationPrescribed.Refills.Value = null;
                }
                else
                {
                    this.msgBody.RefResp.mMedicationPrescribed.Refills.Qualifier = "A";
                    if (this.msgBody.RefResp.mMedicationPrescribed.Refills.Value == "0")
                    {
                        this.msgBody.RefResp.mMedicationPrescribed.Refills.Value = null;
                    }
                }

                // all Deny and DenyWithNewRxToFollow should have refills = A0
                if (this.msgBody.RefResp.ResponseMsg.DeniedNewPrescripResp != null ||
                    this.msgBody.RefResp.ResponseMsg.DeniedResp != null)
                {
                    this.msgBody.RefResp.mMedicationPrescribed.Refills.Qualifier = "A";
                    this.msgBody.RefResp.mMedicationPrescribed.Refills.Value = "0";
                }
            }

            this.msgBody.RefResp.mMedicationPrescribed.WrittenDate = new DateType { Item = DateTime.UtcNow, ItemElementName = ItemChoiceType.Date };

            // Refill and WrittenDate are the same for Prescribed/Dispensed medication
            this.msgBody.RefResp.mMedicationDispensed.Refills = this.msgBody.RefResp.mMedicationPrescribed.Refills;
            this.msgBody.RefResp.mMedicationDispensed.WrittenDate = this.msgBody.RefResp.mMedicationPrescribed.WrittenDate;

            if (isEPCS)
            {
                this.msgBody.RefResp.mMedicationPrescribed.DrugCoverageStatusCode = new string[1] { "SI" };
                //this.msgBody.RefResp.mMedicationDispensed.DrugCoverageStatusCode = new string[1] { "SI" };              
            }

            if (string.IsNullOrEmpty(this.msgBody.RefResp.mPrescriber.mPrescriberID.StateLicenseNumber) && !string.IsNullOrEmpty(dpsNumber))
            {
                this.msgBody.RefResp.mPrescriber.mPrescriberID.StateLicenseNumber = dpsNumber;
            }

            // Remove BenefitsCoordination segment from refill response
            this.msgBody.RefResp.mBenefit = null;
        }

        public void SetChangeResponseFromClone(string[] inputParams)
        {
            this.SetChangeResponseFromClone(inputParams, false, "");
        }
        public void SetChangeResponseFromClone(string[] inputParams, bool isEPCS, string dpsNumber)
        {
            if (inputParams[6] != null)
            {
                this.msgBody.ChangeResp.ResponseMsg = new ResponseType(inputParams);
            }

            string respType = inputParams[5];
            if (respType == "Denied")
            {
                this.msgBody.ChangeResp.mMedicationPrescribed = null;
            }
            else
            {
                // Load selected medication or leave it by default
                string selectedMid = inputParams[10];
                if (!string.IsNullOrEmpty(selectedMid))
                {
                    var mPrescribed = new MedicationInfo();
                    mPrescribed.LoadFromSurescriptsMedication(selectedMid);
                    this.msgBody.ChangeResp.mMedicationPrescribed = mPrescribed;

                    if (inputParams[9] != null)
                    {
                        this.msgBody.ChangeResp.mMedicationPrescribed.Refills = new QualifierValuePair();
                        this.msgBody.ChangeResp.mMedicationPrescribed.Refills.Qualifier = "R";
                        this.msgBody.ChangeResp.mMedicationPrescribed.Refills.Value = inputParams[9];
                        if (this.msgBody.ChangeResp.mMedicationPrescribed.Refills.Value == "999")
                        {
                            this.msgBody.ChangeResp.mMedicationPrescribed.Refills.Qualifier = "PRN";
                            this.msgBody.ChangeResp.mMedicationPrescribed.Refills.Value = null;
                        }
                    }

                    this.msgBody.ChangeResp.mMedicationPrescribed.WrittenDate = new DateType { Item = DateTime.UtcNow, ItemElementName = ItemChoiceType.Date };
                }

                if (inputParams.Length > 11 && inputParams[11] != null)
                {
                    this.msgBody.ChangeResp.mMedicationPrescribed.PriorAuthorization = new PriorAuthorization
                    {
                        Value = inputParams[11],
                        Qualifier = "G1"
                    };
                }

                if (isEPCS)
                {
                    this.msgBody.ChangeResp.mMedicationPrescribed.DrugCoverageStatusCode = new string[1] { "SI" };
                }

                if (string.IsNullOrEmpty(this.msgBody.ChangeResp.mPrescriber.mPrescriberID.StateLicenseNumber) && !string.IsNullOrEmpty(dpsNumber))
                {
                    this.msgBody.ChangeResp.mPrescriber.mPrescriberID.StateLicenseNumber = dpsNumber;
                }
            }
        }

        public void SetMessageHeaderFromClone()
        {
            Guid MessageGuid = Guid.NewGuid();

            this.msgHeader.MessageID = MessageGuid.ToString().Replace("-", "");
            //this.msgHeader.SentTime = DateTime.Now.AddHours(7.0).ToString("yyyy-MM-ddTHH:mm:ss.0Z");
            this.msgHeader.SentTime = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.0Z");

        }

        public static string GetDefaultNamespace(string msgType)
        {
            if (msgType == "DirDwnld" || msgType == "AddPrescriber" || msgType == "UpdatePrescriber")
            {
                return GetNamespaceOfDirectoryMessage();
            }
            else
            {
                return GetNamespaceOfRxRoutingMessage();
            }
        }

        public static string GetNamespaceOfDirectoryMessage()
        {
            return "http://www.surescripts.com/messaging";
        }

        public static string GetNamespaceOfRxRoutingMessage()
        {
            return "http://www.ncpdp.org/schema/SCRIPT";
        }

        private void saveSurescriptsMedication(string medicationType, string messageId, MedicationInfo[] medications)
        {
            if (medications != null && medications.Length > 0)
            {
                string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (OracleConnection connection = new OracleConnection(connectionString))
                {
                    connection.Open();
                    OracleCommand command = connection.CreateCommand();
                    OracleTransaction tx = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    command.Transaction = tx;

                    try
                    {
                        string surescriptsMedicationDescription = "";
                        string surescriptsMedicationCode = "";
                        string surescriptsMedicationCodeType = "";
                        string surescriptsMedicationDosageForm = "";
                        string surescriptsMedicationStrength = "";
                        string surescriptsMedicationStrengthUnits = "";
                        string surescriptsMedicationDrugDBCode = "";
                        string surescriptsMedicationDrugDBCodeQual = "";
                        string surescriptsMedicationQuantityQual = "";
                        string surescriptsMedicationQuantity = "";
                        string surescriptsMedicationDirections = "";
                        string surescriptsMedicationNote = "";
                        string surescriptsMedicationRefillQual = "";
                        string surescriptsMedicationSubstitution = "";
                        string surescriptsMedicationWrittenDate = "";
                        string surescriptsMedicationRefillQuan = "";
                        string surescriptsMedicationLastFillDate = "";
                        string surescriptsMedicationDaysSupply = "";
                        string surescriptsMedicationClinicalInfoQual = "";
                        string surescriptsMedicationPrimaryQual = "";
                        string surescriptsMedicationPrimaryVal = "";
                        string surescriptsMedicationSecondaryQual = "";
                        string surescriptsMedicationSecondaryVal = "";
                        string surescriptsRawContent = "";

                        foreach (var each in medications)
                        {
                            string id = Guid.NewGuid().ToString().Replace("-", "");
                            // Medication dispensed
                            surescriptsMedicationDescription = each.DrugDescription;
                            if (each.DrugCode != null)
                            {
                                if (each.DrugCode.ProductCode != null)
                                {
                                    surescriptsMedicationCode = each.DrugCode.ProductCode;
                                    surescriptsMedicationCodeType = "ND";
                                }
                                // Not support by RxRouting 10.6
                                //if (this.msgBody.ChangeReq.mMedication.DrugCode.DosageForm != null)
                                //{
                                //    surescriptsMedicationDosageForm = this.msgBody.ChangeReq.mMedication.DrugCode.DosageForm;
                                //}
                                if (each.DrugCode.Strength != null)
                                {
                                    surescriptsMedicationStrength = each.DrugCode.Strength;
                                    // Not support by RxRouting 10.6
                                    //surescriptsMedicationStrengthUnits = this.msgBody.ChangeReq.mMedication.DrugCode.StrengthUnits;
                                }
                                if (each.DrugCode.DrugDBCode != null)
                                {
                                    surescriptsMedicationDrugDBCode = each.DrugCode.DrugDBCode;
                                    surescriptsMedicationDrugDBCodeQual = each.DrugCode.DrugDBCodeQualifier;
                                }
                            }
                            if (each.Quantity != null)
                            {
                                surescriptsMedicationQuantityQual = each.Quantity.PotencyUnitCode;
                                surescriptsMedicationQuantity = each.Quantity.Value;
                            }
                            if (each.DaysSupply != null)
                            {
                                surescriptsMedicationDaysSupply = each.DaysSupply;
                            }
                            surescriptsMedicationDirections = each.Directions;
                            if (each.Note != null)
                            {
                                surescriptsMedicationNote = each.Note;
                            }

                            if (each.Refills != null)
                            {
                                surescriptsMedicationRefillQual = each.Refills.Qualifier;
                                if (each.Refills.Qualifier != "PRN")
                                {
                                    surescriptsMedicationRefillQuan = each.Refills.Value;
                                }
                            }

                            surescriptsMedicationSubstitution = each.Substitutions;
                            surescriptsMedicationWrittenDate = each.WrittenDate.Item.ToString("yyyyMMdd");
                            if (each.LastFillDate != null)
                            {
                                surescriptsMedicationLastFillDate = each.LastFillDate.Item.ToString("yyyyMMdd");
                            }

                            XmlSerializer xmlSerializer = new XmlSerializer(each.GetType(), Message.GetNamespaceOfRxRoutingMessage());
                            using (StringWriter textWriter = new StringWriter())
                            {
                                xmlSerializer.Serialize(textWriter, each);
                                surescriptsRawContent = textWriter.ToString();
                            }

                            command.CommandText = "WSSInterface.SaveSurescriptsMedication";
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new OracleParameter("ParamID", OracleType.VarChar)).Value = id;
                            command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = messageId ?? "";
                            command.Parameters.Add(new OracleParameter("ParamMedicationType", OracleType.VarChar)).Value = medicationType ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedDescription", OracleType.VarChar)).Value = surescriptsMedicationDescription ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedCode", OracleType.VarChar)).Value = surescriptsMedicationCode ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedCodeType", OracleType.VarChar)).Value = surescriptsMedicationCodeType ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedQuantityQual", OracleType.VarChar)).Value = surescriptsMedicationQuantityQual ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedQuantity", OracleType.VarChar)).Value = surescriptsMedicationQuantity ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedDirections", OracleType.VarChar)).Value = surescriptsMedicationDirections ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedNote", OracleType.VarChar)).Value = surescriptsMedicationNote ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedRefillQual", OracleType.VarChar)).Value = surescriptsMedicationRefillQual ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedRefillQuan", OracleType.VarChar)).Value = surescriptsMedicationRefillQuan ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedSubstitution", OracleType.VarChar)).Value = surescriptsMedicationSubstitution ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedWrittenDate", OracleType.VarChar)).Value = surescriptsMedicationWrittenDate ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedDosageForm", OracleType.VarChar)).Value = surescriptsMedicationDosageForm ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedStrength", OracleType.VarChar)).Value = surescriptsMedicationStrength ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedStrengthUnits", OracleType.VarChar)).Value = surescriptsMedicationStrengthUnits ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedDrugDBCode", OracleType.VarChar)).Value = surescriptsMedicationDrugDBCode ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedDrugDBCodeQual", OracleType.VarChar)).Value = surescriptsMedicationDrugDBCodeQual ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedDaysSupply", OracleType.VarChar)).Value = surescriptsMedicationDaysSupply ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedLastFillDate", OracleType.VarChar)).Value = surescriptsMedicationLastFillDate ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedCliInfoQual", OracleType.VarChar)).Value = surescriptsMedicationLastFillDate ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedPrimaryQual", OracleType.VarChar)).Value = surescriptsMedicationPrimaryQual ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedPrimaryVal", OracleType.VarChar)).Value = surescriptsMedicationPrimaryVal ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedSecondaryQual", OracleType.VarChar)).Value = surescriptsMedicationSecondaryQual ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedSecondaryVal", OracleType.VarChar)).Value = surescriptsMedicationSecondaryVal ?? "";
                            command.Parameters.Add(new OracleParameter("ParamSSMedRawContent", OracleType.VarChar)).Value = surescriptsRawContent ?? "";

                            SS_Logger.log("=====Save Surescripts Medication Parms =====");
                            foreach (OracleParameter parm in command.Parameters)
                            {
                                SS_Logger.log(parm.ParameterName + "=|" + parm.Value + "|");
                            }
                            SS_Logger.log("=====End Save Surescripts Medication Parms =====");

                            command.ExecuteNonQuery();
                            command.Parameters.Clear();
                        }

                        tx.Commit();
                        SS_Logger.log("=====Save Surescripts Medication Success =====");
                    }
                    catch (Exception e)
                    {
                        tx.Rollback();
                        SS_Logger.log("=====Save Surescripts Medication Errors =====");
                        SS_Logger.log(e.ToString());
                        throw e;
                    }
                }
            }
        }
    }
}

