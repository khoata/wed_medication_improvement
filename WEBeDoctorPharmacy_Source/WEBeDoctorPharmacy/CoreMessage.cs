namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;
    using System.Collections.Generic;

    public class CoreMessage
    {
        [XmlElement("Request")]
        public ShortRequestType Request;
        [XmlElement("Response")]
        public ResponseType ResponseMsg;
        [XmlElement("Pharmacy")]
        public PharmacyInfo mPharmacy;
        [XmlElement("Prescriber")]
        public PrescriberInfo mPrescriber;
        [XmlElement("Supervisor")]
        public SupervisorType mSupervisor;
        [XmlElement("Facility")]
        public MandatoryFaciltyType mFacility;
        [XmlElement("Patient")]
        public PatientInfo mPatient;
        [XmlElement("MedicationPrescribed")]
        public MedicationInfo mMedicationPrescribed;
        [XmlElement("MedicationDispensed")]
        public MedicationInfo mMedicationDispensed;
        [XmlElement("Observation")]
        public ObservationType mObservation;
        [XmlElement("BenefitsCoordination")]
        public BenefitsCoordinationType mBenefit;

        public CoreMessage()
        {
        }

        public CoreMessage(string messageType, string[] inputParams)
            : this(messageType, inputParams, false, "", "", new string[] {})
        {
        }

        public CoreMessage(string messageType, string[] inputParams, bool isEPCS, string scheduleCode, string dpsNumber, string[] benefitParams)
        {
            if (messageType != "RefillResponse")
            {
                SS_Logger.log("=====1.1A =====");
                if (inputParams[6] != null)
                {
                    this.ResponseMsg = new ResponseType(inputParams);
                }
                this.mPharmacy = new PharmacyInfo(inputParams[0]);
                SS_Logger.log("=====1.2A =====");
                this.mPrescriber = new PrescriberInfo(inputParams[1]);
                SS_Logger.log("=====1.3A =====");
                if ((inputParams.Length > 9) && ((inputParams[8] ?? "").Length > 0))
                {
                    this.mPatient = new PatientInfo(null, inputParams[8]);
                }
                SS_Logger.log("=====1.4A =====");
                if ((((this.mPatient == null) || !this.mPatient.wasLoadedFromDb()) && (inputParams[2] != null)) && (inputParams[2].Length > 0))
                {
                    this.mPatient = new PatientInfo(inputParams[2]);
                }
                SS_Logger.log("=====1.5A =====");
                this.mMedicationPrescribed = new MedicationInfo(inputParams[3], inputParams[4], isEPCS, scheduleCode);
                SS_Logger.log("=====1.6A =====");
                //this.mPrescriber.mPrescriberAgent = new Name();
                //this.mPrescriber.mPrescriberAgent.LastName = this.mPrescriber.mPrescriberName.LastName;
                //this.mPrescriber.mPrescriberAgent.FirstName = this.mPrescriber.mPrescriberName.FirstName;
                if (!string.IsNullOrEmpty(dpsNumber))
                {
                    this.mPrescriber.mPrescriberID.StateLicenseNumber = dpsNumber;
                }
                SS_Logger.log("=====1.7A =====");

                //Benefit segment
                if (string.Equals(messageType, "NewRX"))
                {
                    
                    if (!(string.IsNullOrEmpty (Convert.ToString (inputParams[7]))))
                    {
                        //this.mMedicationPrescribed.Note = Convert.ToString(inputParams[7]);
                        this.mMedicationPrescribed.Note += ", PDR Notes: " +  Convert.ToString(inputParams[7]);
                    }



                    SS_Logger.log("MedNotes:" + Convert.ToString(inputParams[7]));

                    if (benefitParams.Length > 0 && benefitParams[0] == "Y")
                    {
                        this.mBenefit = new BenefitsCoordinationType();

                        //PayerID
                        List<PayerIDType> listType = new List<PayerIDType>();
                        if (!string.IsNullOrEmpty(benefitParams[1]))
                        {
                            listType.Add(new PayerIDType()
                            {
                                ItemElementName = ItemChoiceType1.PayerID,
                                Item = benefitParams[1]
                            });
                        }
                        //BINLocationNumber
                        if (!string.IsNullOrEmpty(benefitParams[3]))
                        {
                            listType.Add(new PayerIDType()
                            {
                                ItemElementName = ItemChoiceType1.BINLocationNumber,
                                Item = benefitParams[3]
                            });
                        }
                        //ProcessorControlNumber
                        if (!string.IsNullOrEmpty(benefitParams[4]))
                        {
                            listType.Add(new PayerIDType()
                            {
                                ItemElementName = ItemChoiceType1.ProcessorIdentificationNumber,
                                Item = benefitParams[4]
                            });
                        }
                        //MutuallyDefined
                        if (!string.IsNullOrEmpty(benefitParams[10]))
                        {
                            listType.Add(new PayerIDType()
                            {
                                ItemElementName = ItemChoiceType1.MutuallyDefined,
                                Item = benefitParams[10]
                            });
                        }
                        this.mBenefit.PayerIdentification = listType.ToArray();

                        //PayerName
                        this.mBenefit.PayerName = benefitParams[2];
                        //GroupID
                        this.mBenefit.GroupID = benefitParams[5];
                        //CardholderID
                        this.mBenefit.CardholderID = benefitParams[7];
                        //CardholderName
                        this.mBenefit.CardHolderName = new OptionalNameType() {
                            FirstName = !string.IsNullOrEmpty(benefitParams[8]) ? benefitParams[8] : null,
                            LastName = !string.IsNullOrEmpty(benefitParams[9]) ? benefitParams[9] : null
                        };

                        
                    }
                }
            }
            else
            {
                if (inputParams[6] != null)
                {
                    this.ResponseMsg = new ResponseType(inputParams);
                }

                if (!(string.IsNullOrEmpty(Convert.ToString(inputParams[7]))))
                {
                    //this.mMedicationPrescribed.Note = Convert.ToString(inputParams[7]);
                    this.mMedicationPrescribed.Note += ", PDR Notes: " + Convert.ToString(inputParams[7]);
                }


                SS_Logger.log("MedNotes:" + Convert.ToString(inputParams[7]));
                
                if ((inputParams[8] ?? "").Length > 0)
                {
                    this.mPharmacy = new PharmacyInfo(inputParams[8], null);

                    this.mPrescriber = new PrescriberInfo(null, inputParams[8]);
                    if (!string.IsNullOrEmpty(dpsNumber))
                    {
                        this.mPrescriber.mPrescriberID.StateLicenseNumber = dpsNumber;
                    }
                    this.mPatient = new PatientInfo(null, inputParams[8]);
                    this.mMedicationPrescribed = new MedicationInfo(inputParams[8], inputParams[10], "", isEPCS, scheduleCode);
                    SS_Logger.log("=====1.1 =====");
                    if (inputParams[9] != null)
                    {
                        

                        this.mMedicationPrescribed.Refills = new QualifierValuePair();
                        this.mMedicationPrescribed.Refills.Value = inputParams[9];
                        if (this.mMedicationPrescribed.Refills.Value == "999")
                        {
                            this.mMedicationPrescribed.Refills.Qualifier = "PRN";
                            this.mMedicationPrescribed.Refills.Value = null;
                        }
                        else
                        {
                            this.mMedicationPrescribed.Refills.Qualifier = "R";
                            if (this.mMedicationPrescribed.Refills.Value == "0")
                            {
                                this.mMedicationPrescribed.Refills.Value = null;
                            }
                        }

                        // all Deny and DenyWithNewRxToFollow should have refills = R0
                        if (this.ResponseMsg.DeniedNewPrescripResp != null ||
                             this.ResponseMsg.DeniedResp != null)
                        {
                            this.mMedicationPrescribed.Refills.Qualifier = "R";
                            this.mMedicationPrescribed.Refills.Value = "0";
                        }
                    }
                    SS_Logger.log("=====1.2 =====");
                }
            }
        }
    }
}

