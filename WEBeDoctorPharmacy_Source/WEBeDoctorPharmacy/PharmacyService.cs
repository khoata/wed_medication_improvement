﻿namespace WEBeDoctorPharmacy
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.IO;
    using System.Xml;
    using System.Globalization;
    using System.Data.OracleClient;
    using System.Configuration;
    using System.Data;

    public class PharmacyService
    {
        private static List<ParsingField> GetFields(string mappingFile)
        {
            if (!File.Exists(mappingFile))
                throw new Exception("Mapping file '" + mappingFile + "' not found.");

            List<ParsingField> fields = new List<ParsingField>();
            XmlDocument map = new XmlDocument();
            map.Load(mappingFile);
            XmlNodeList fieldNodes = map.SelectNodes("/FileMap/Field");

            foreach (XmlNode fieldNode in fieldNodes)
            {
                ParsingField field = new ParsingField();

                field.Name = fieldNode.Attributes["Name"].Value;
                field.Length = Convert.ToInt32(fieldNode.Attributes["Length"].Value);
                field.Start = Convert.ToInt32(fieldNode.Attributes["Start"].Value) - 1;
                field.Required = Convert.ToBoolean(fieldNode.Attributes["Required"].Value);
                fields.Add(field);
            }

            return fields;
        }

        private static List<List<ParsingField>> ParseInputStream(StreamReader streamReader, string mappingFile)
        {
            List<ParsingField> mappedFields = GetFields(mappingFile);
            List<List<ParsingField>> results = new List<List<ParsingField>>();
            string line = streamReader.ReadLine();
            int lineCount = 0;
            while (line != null)
            {
                lineCount++;

                List<ParsingField> record = new List<ParsingField>();

                foreach (ParsingField field in mappedFields)
                {
                    ParsingField fileField = new ParsingField();
                    fileField.Value = line.Substring(field.Start, field.Length);

                    if (field.Required && String.IsNullOrEmpty(fileField.Value))
                    {
                        //this line fails to meet the validation
                        SS_Logger.log("Property " + field.Name + " in line# " + lineCount + " fails to meet Required requirement");
                        continue;
                    }
                    fileField.Name = field.Name;
                    record.Add(fileField);
                }

                results.Add(record);

                line = streamReader.ReadLine();
            }
            return results;
        }

        public static List<List<ParsingField>> ParseFile(string inputFile, string mappingFile)
        {
            if (!File.Exists(inputFile))
                throw new Exception("Input file '" + inputFile + "' not found.");

            using (StreamReader reader = new StreamReader(inputFile))
            {
                return ParseInputStream(reader, mappingFile);
            }
        }

        public static void UpdatePharmacies(List<List<ParsingField>> pharmacies, String downloadType)
        {
            using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();
                OracleTransaction trans = connection.BeginTransaction();

                string errMsg = "";
                SS_Logger.log("=====" + DateTime.Now.ToString("M/dd/yyyy h:mm:ss tt") + ": Begin Update Pharmacies =====");
                if (downloadType.Equals("full"))
                {
                    SS_Logger.log("=====Set All Pharmacies Inactive =====");
                    OracleCommand command = new OracleCommand();
                    command.Connection = connection;
                    command.Transaction = trans;
                    command.CommandText = "UPDATE SS_PHARMACY SET ISACTIVE = '0'";
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }

                foreach (List<ParsingField> pharmacy in pharmacies)
                {
                    DateTime now = DateTime.Now;
                    int serviceLevel = 0;
                    bool isServiceLevelValid = false;
                    string isActive = "0";

                    ParsingField ncpdpidField = pharmacy.Find(x => x.Name.Equals("NCPDPID"));
                    string ncpdpid = ncpdpidField.Value;
                    ParsingField serviceLevelField = pharmacy.Find(x => x.Name.Equals("ServiceLevel"));
                    isServiceLevelValid = int.TryParse(serviceLevelField.Value, out serviceLevel);
                    ParsingField startTimeField = pharmacy.Find(x => x.Name.Equals("ActiveStartTime"));
                    DateTime startTime = DateTime.Parse(startTimeField.Value, CultureInfo.InvariantCulture, DateTimeStyles.None).ToLocalTime();
                    ParsingField endTimeField = pharmacy.Find(x => x.Name.Equals("ActiveEndTime"));
                    DateTime endTime = DateTime.Parse(endTimeField.Value, CultureInfo.InvariantCulture, DateTimeStyles.None).ToLocalTime();

                    if (isServiceLevelValid && (serviceLevel > 0) && (DateTime.Compare(now, endTime) < 0))
                    {
                        isActive = "1";
                    }
                    else
                    {
                        isActive = "0";
                    }

                    //SS_Logger.log("Insert/Update Pharmacy " + ncpdpid);
                    OracleCommand command = new OracleCommand();
                    command.Connection = connection;
                    command.Transaction = trans;
                    command.CommandText = "WSSInterface.SavePharmacy";
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (ParsingField parsingField in pharmacy)
                    {
                        string fieldName = parsingField.Name;
                        string fieldValue = parsingField.Value.Trim();
                        if (fieldName.Equals("ActiveStartTime"))
                        {
                            fieldValue = startTime.ToString("dd-MMM-yy");
                        }
                        else if (fieldName.Equals("ActiveEndTime"))
                        {
                            fieldValue = endTime.ToString("dd-MMM-yy");
                        }
                        command.Parameters.Add(new OracleParameter("Param" + fieldName, OracleType.VarChar)).Value = fieldValue;
                    }
                    command.Parameters.Add(new OracleParameter("ParamIsActive", OracleType.VarChar)).Value = isActive;
                    command.Parameters.Add(new OracleParameter("ParamErrorMsg", OracleType.VarChar)).Value = "";
                    command.Parameters["ParamErrorMsg"].Direction = ParameterDirection.Output;

                    /*SS_Logger.log("=====Pharmacy Save Parms =====");
                    foreach (OracleParameter parm in command.Parameters)
                    {
                        SS_Logger.log(parm.ParameterName + "=|" + parm.Value + "|");
                    }
                    SS_Logger.log("=====End Pharmacy Save Parms =====");*/

                    command.ExecuteNonQuery();

                    errMsg = command.Parameters["ParamErrorMsg"].Value.ToString();
                    if (!errMsg.Equals(""))
                    {
                        SS_Logger.log("ERROR When Insert/Update Pharmacy " + ncpdpid + ": " + errMsg);
                        break;
                    }
                    command.Dispose();
                }

                if (errMsg.Equals(""))
                {
                    trans.Commit();
                    SS_Logger.log("=====" + DateTime.Now.ToString("M/dd/yyyy h:mm:ss tt") + ": End Update Pharmacies: " + pharmacies.Count + " items have been inserted/updated =====");
                }
                else
                {
                    trans.Rollback();
                    SS_Logger.log("=====" + DateTime.Now.ToString("M/dd/yyyy h:mm:ss tt") + ": End Update Pharmacies: ROLL BACK =====");
                }
                trans.Dispose();
                //connection.Close();
            }
        }

        public static void LoadPharmaciesFromFile(string inputFile, string mappingFile, string downloadType)
        {
            UpdatePharmacies(ParseFile(inputFile, mappingFile), downloadType);
        }
    }
}
