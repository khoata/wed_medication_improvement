﻿namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;

    public class BenefitInfo
    {
        [XmlElement("PayerIdentification")]
        public PayerIdentification[] PayerIdentification;
        [XmlElement("PayerName")]
        public string PayerName;
        [XmlElement("CardholderID")]
        public string CardHolderId;
        [XmlElement("CardHolderName")]
        public CardHolderName CardHolderName;
        [XmlElement("GroupID")]
        public string GroupID;
        [XmlElement("GroupName")]
        public string GroupName;
        [XmlElement("Address")]
        public Address Address;
        [XmlElement("InsuranceTypeCode")]
        public string InsuranceTypeCode;
        [XmlElement("CommunicationNumbers")]
        public CommunicationNumbers CommunicationNumbers;
    }

    public class PayerIdentification
    {
        [XmlElement("PayerID")]
        public string PayerID;
        [XmlElement("BINLocationNumber")]
        public string BINLocationNumber;
        [XmlElement("ProcessorIdentificationNumber")]
        public string ProcessorIdentificationNumber;
    }

    public class CardHolderName
    {
        [XmlElement("LastName")]
        public string LastName;
        [XmlElement("FirstName")]
        public string FirstName;
        [XmlElement("MiddleName")]
        public string MiddleName;
        [XmlElement("Suffix")]
        public string Suffix;
        [XmlElement("Prefix")]
        public string Prefix;
    }

    public class CommunicationNumbers
    {
        [XmlElement("Number")]
        public string Number;
        [XmlElement("Qualifier")]
        public string Qualifier;
    }
}
