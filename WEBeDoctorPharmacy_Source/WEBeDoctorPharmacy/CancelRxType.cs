﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WEBeDoctorPharmacy
{
    public partial class CancelRxType
    {
        [XmlElement("Pharmacy")]
        public PharmacyInfo mPharmacy;

        [XmlElement("Prescriber")]
        public PrescriberInfo mPrescriber;

        [XmlElement("Patient")]
        public PatientInfo mPatient;

        [XmlElement("MedicationPrescribed")]
        public MedicationInfo mMedicationPrescribed;

        public CancelRxType()
        {
        }

        public CancelRxType(string[] inputParams)
        {
            string ncpdpId = inputParams[0];
            string physicianId = inputParams[1];
            string patientId = inputParams[2];
            string selectedMid = inputParams[3];
            string visitKey = inputParams[5];

            SS_Logger.log("=====1.1A =====");
            this.mPharmacy = new PharmacyInfo(ncpdpId);
            SS_Logger.log("=====1.2A =====");
            this.mPrescriber = new PrescriberInfo(physicianId);
            SS_Logger.log("=====1.3A =====");
            this.mPatient = new PatientInfo(patientId);
            SS_Logger.log("=====1.4A =====");
            this.mMedicationPrescribed = new MedicationInfo(selectedMid, visitKey);
        }
    }
}
