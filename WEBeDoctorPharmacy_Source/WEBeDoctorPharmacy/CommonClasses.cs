namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml;
    using System.Xml.Serialization;
    using System.IO;
    using System.Collections;
    using System.Configuration;

    public class Address
    {
        [XmlElement("AddressLine1")]
        public string Address1;
        [XmlElement("AddressLine2")]
        public string Address2;
        [XmlElement("City")]
        public string City;
        [XmlElement("State")]
        public string State;
        [XmlElement("ZipCode")]
        public string Zip;
        [XmlElement("PlaceLocationQualifier")]
        public string PlaceLocationQualifier;
    }
    public class Approved
    {
        [XmlElement("Note")]
        public string Note;

        public Approved()
        {
            Note = null;
        }
    }
    public class ApprovedWithChanges
    {
        [XmlElement("Note")]
        public string note;

        public ApprovedWithChanges()
        {
            note = null;
        }
    }
    public class Denied
    {
        [XmlElement("DenialReasonCode")]
        public string DenialReasonCode;

        [XmlElement("DenialReason")]
        public string DenialReason;

        public Denied()
        {
            DenialReasonCode = null;
            DenialReason = null;
        }
    }
    public class DeniedNewPrescriptionToFollow
    {
        [XmlElement("DenialReason")]
        public string note;

        public DeniedNewPrescriptionToFollow()
        {
            note = null;
        }
    }
    public class Diagnosis
    {
        [XmlElement("ClinicalInformationQualifier")]
        public string ClinicalInformationQualifier;
        [XmlElement("Primary")]
        public QualifierValuePair Primary;
        [XmlElement("Secondary")]
        public QualifierValuePair Secondary;
    }
    public class DrugCoded
    {
        [XmlElement("ProductCode")]
        public string ProductCode;
        [XmlElement("ProductCodeQualifier")]
        public string ProductCodeQualifier;
        [XmlElement("Strength")]
        public string Strength;
        [XmlElement("DrugDBCode")]
        public string DrugDBCode;
        [XmlElement("DrugDBCodeQualifier")]
        public string DrugDBCodeQualifier;
        [XmlElement("FormSourceCode")]
        public string FormSourceCode;
        [XmlElement("FormCode")]
        public string FormCode;
        [XmlElement("StrengthSourceCode")]
        public string StrengthSourceCode;
        [XmlElement("StrengthCode")]
        public string StrengthCode;
        [XmlElement("DEASchedule")]
        public DrugCodedTypeDEASchedule DEASchedule;
        [XmlIgnore()]
        public bool DEAScheduleSpecified;
    }
    public class DSPrescriber
    {
        [XmlElement("Response")]
        public ResponseType mResponse;
        [XmlElement("Prescriber")]
        public DSPrescriberInfo mPrescriber;

        public DSPrescriber()
        {
        }

        public DSPrescriber(string PrescriberID)
        {
            this.mResponse = new ResponseType();
            this.mPrescriber = new DSPrescriberInfo(PrescriberID);
        }

        public DSPrescriber(string PrescriberID, int ServiceLevel)
        {
            this.mPrescriber = new DSPrescriberInfo(PrescriberID, ServiceLevel);
        }
    }
    public static class ErrorDescription
    {
        public const string InternalProcessingError = "007";
        public const string MessageIsDuplicate = "220";
        public const string ReceiverIDNotOnFile = "002";
        public const string ReceiverNoLongerActive = "209";
        public const string RequestTimedOut = "008";
        public const string SenderIDNotOnFile = "001";
        public const string SenderNoLongerActive = "208";
        public const string UIBTraceIDInvalid = "025";
        public const string ValidationError = "000";
    }
    public class ErrorStatus : GenericStatus
    {
        [XmlElement("Description")]
        public string Description;
        [XmlElement("DescriptionCode")]
        public string DescriptionCode;

        public ErrorStatus()
        {
        }

        public ErrorStatus(string code, string descCode, string desc)
            : base(code)
        {
            this.DescriptionCode = descCode;
            this.Description = desc;
        }
    }
    public class VerifyStatus
    {
        [XmlElement("VerifyStatus")]
        public GenericStatus Status;
    }
    public class GenericStatus
    {
        [XmlElement("Code")]
        public string Code;

        public GenericStatus()
        {
        }

        public GenericStatus(string InputCode)
        {
            this.Code = InputCode;
        }
    }
    public class ID
    {
        [XmlElement("DEANumber")]
        public string DEANumber;
        [XmlElement("DentistLicenseNumber")]
        public string DentistLicenseNumber;
        [XmlElement("FileID")]
        public string FileID;
        [XmlElement("MedicaidNumber")]
        public string MedicaidNumber;
        [XmlElement("MedicareNumber")]
        public string MedicareNumber;
        [XmlElement("MutuallyDefined")]
        public string MutuallyDefined;
        [XmlElement("NCPDPID")]
        public string NCPDPID;
        [XmlElement("NPI")]
        public string NPI;
        [XmlElement("PayerID")]
        public string PayerID;
        [XmlElement("PPONumber")]
        public string PPONumber;
        [XmlElement("PriorAuthorization")]
        public string PriorAuthorization;
        [XmlElement("SocialSecurity")]
        public string SocialSecurityNumber;
        [XmlIgnore()]   // NOT supported in RxRouting 10.6
        public string SPI;
        [XmlElement("SPI")]   // supported in Directory 4.4
        public string DSSPI;
        [XmlElement("StateLicenseNumber")]
        public string StateLicenseNumber;
        [XmlElement("UPIN")]
        public string UPIN;
    }
    public class Name
    {
        [XmlElement("LastName")]
        public string LastName;
        [XmlElement("FirstName")]
        public string FirstName;
        [XmlElement("MiddleName")]
        public string MiddleName;
        [XmlElement("Suffix")]
        public string Suffix;
        [XmlElement("Prefix")]
        public string Prefix;
    }
    public class Phone
    {
        [XmlElement("Number")]
        public string PhoneNumber;
        [XmlElement("Qualifier")]
        public string Qualifier;
    }
    public class QualifierQuantityPair
    {
        [XmlElement("Qualifier")]
        public string Qualifier;
        [XmlElement("Quantity")]
        public string Quantity;
    }
    public class QualifierValuePair
    {
        [XmlElement("Qualifier")]
        public string Qualifier;
        [XmlElement("Value")]
        public string Value;
    }
    public class Security
    {
        [XmlElement("UsernameToken")]
        public UserNameToken mUsernameToken;

        public class UserNameToken
        {
            [XmlElement("Username")]
            public string mUsername;
            [XmlElement("Password")]
            public Password mPassword;
            [XmlElement("Nonce")]
            public string mNonce;
            [XmlElement("Created")]
            public string mCreated;

            public class Password
            {
                [XmlText]
                public string mmPassword;
            }
        }
    }
    public class Specialty
    {
        [XmlElement("Qualifier")]
        public string Qualifier;
        [XmlElement("SpecialtyCode")]
        public string SpecialtyCode;
    }
    public class SS_Logger
    {
        public static void log(string text)
        {
            StreamWriter sw = null;
            string LogFlag = ConfigurationManager.AppSettings["LogFlag"].ToString();
            string LogPath = ConfigurationManager.AppSettings["LogPath"].ToString();
            try
            {
                if (LogFlag == "Y")
                {
                    if (!File.Exists(LogPath))
                    {
                        File.Create(LogPath);
                    }
                    sw = File.AppendText(LogPath);
                    sw.WriteLine(text);
                }
            }
            finally
            {
                sw.Close();
            }
        }
    }
    public static class StatusCode
    {
        public const string CommunicationError = "600";
        public const string ReceiverSystemError = "602";
        public const string ReceiverUnableToProcess = "601";
        public const string TransactionRejected = "900";
        public const string TransactionSuccessfulPending = "000";
        public const string TransactionSuccessfulVerified = "010";
    }
    public class PhoneNumbers
    {
        [XmlElement("Phone")]
        public Phone mPhone;
    }
    public class PhysicianPhoneNumbers
    {
        [XmlElement("Communication")]
        public Phone[] mPhone;
    }
    public class DSPhysicianPhoneNumbers
    {
        [XmlElement("Phone")]
        public Phone[] mPhone;
    }

    public class PriorAuthorization
    {
        [XmlElement("Qualifier")]
        public string Qualifier;
        [XmlElement("Value")]
        public string Value;
    }

    public partial class QualifiedMailAddressType
    {
        [XmlAttribute("Qualifier")]
        public string Qualifier;
        [XmlText()]
        public string Value;
    }

    public partial class DateType
    {

        [XmlElement("Date", typeof(System.DateTime), DataType = "date")]
        [XmlElement("DateTime", typeof(System.DateTime))]
        [XmlChoiceIdentifier("ItemElementName")]
        public System.DateTime Item;

        [XmlIgnore()]
        public ItemChoiceType ItemElementName;
    }

    public enum ItemChoiceType
    {
        Date,
        DateTime,
    }

    public class QuantityType
    {
        [XmlElement("Value")]
        public string Value;
        [XmlElement("CodeListQualifier")]
        public string CodeListQualifier;
        [XmlElement("UnitSourceCode")]
        public string UnitSourceCode;
        [XmlElement("PotencyUnitCode")]
        public string PotencyUnitCode;
    }

    public class XmlPruner
    {
        public XmlDocument pruneXml(XmlDocument input, ArrayList whiteList)
        {

            foreach (XmlNode child in input.ChildNodes)
            {
                if (!pruneElement(child, "/" + child.Name, whiteList))
                {
                    input.RemoveChild(child);
                }
            }

            return input;
        }

        protected bool pruneElement(XmlNode input, String path, ArrayList whiteList)
        {

            ArrayList foo = new ArrayList();

            foreach (XmlNode child in input.ChildNodes)
            {
                if (!pruneElement(child, path + "/" + child.Name, whiteList))
                {
                    foo.Add(child);
                }
            }

            foreach (XmlNode child in foo)
            {
                input.RemoveChild(child);
            }

            if (input.ChildNodes.Count == 0 
                && (input.Value == null || input.Value.ToString().Length == 0))
            {
                if (!(whiteList.Contains(path)))
                {
                    SS_Logger.log("pruning: " + path);
                }
                else
                {
                    SS_Logger.log("ignoring from whitelist: " + path);
                    return true;
                }

                
                return false;
            }

            return true;
        }
    }

    public class ParsingField
    {
        public ParsingField()
        {
        }
        public string Name { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public string Value { get; set; }
        public bool Required { get; set; }
    }


}