﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WEBeDoctorPharmacy
{
    public class RxChangeResponseType
    {
        [XmlElement("Request")]
        public RequestType Request;

        [XmlElement("Response")]
        public ResponseType ResponseMsg;

        [XmlElement("Pharmacy")]
        public PharmacyInfo mPharmacy;

        [XmlElement("Prescriber")]
        public PrescriberInfo mPrescriber;

        [XmlElement("Patient")]
        public PatientInfo mPatient;

        [XmlElement("MedicationPrescribed")]
        public MedicationInfo mMedicationPrescribed;

        [XmlElement("Observation")]
        public ObservationType mObservation;
    }
}
