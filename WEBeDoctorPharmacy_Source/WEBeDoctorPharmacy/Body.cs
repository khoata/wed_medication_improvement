namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class Body
    {
        [XmlElement("AddPrescriber")]
        public DSPrescriber AddPrescriber;
        [XmlElement("AddPrescriberLocation")]
        public DSPrescriber AddPrescriberLocation;
        [XmlElement("AddPrescriberResponse")]
        public DSPrescriber AddPrescriberResp;
        [XmlElement("AddPrescriberLocationResponse")]
        public DSPrescriber AddPrescriberLocationResponse;
        [XmlElement("DirectoryDownload")]
        public DirectoryDownload DirDwnld;
        [XmlElement("DirectoryDownloadResponse")]
        public DirectoryDownloadResponse DirDwnldResponse;
        [XmlElement("Error")]
        public ErrorStatus Error;
        [XmlElement("NewRx")]
        public CoreMessage NewRq;
        [XmlElement("RefillRequest")]
        public CoreMessage RefReq;
        [XmlElement("RefillResponse")]
        public CoreMessage RefResp;
		[XmlElement("CancelRx")]
		public CancelRxType CancelReq;
		[XmlElement("CancelRxResponse")]
		public CancelRxResponseType CancelResp;
        [XmlElement("RxChangeRequest")]
        public RxChangeRequestType ChangeReq;
        [XmlElement("RxChangeResponse")]
        public RxChangeResponseType ChangeResp;
        [XmlElement("Status")]
        public GenericStatus Status;
        [XmlElement("UpdatePrescriberLocation")]
        public DSPrescriber UpdatePrescriber;
        [XmlElement("Verify")]
        public VerifyStatus Verify;

        public Body()
        {
        }

        public Body(string MessageType, string[] InputParams)
            : this(MessageType, InputParams, false, "", "", new string[] {})
        { }

        public Body(string MessageType, string[] InputParams, bool isEPCS, string scheduleCode, string dpsNumber, string[] benefitParams)
        {
            switch (MessageType)
            {
                case "NewRX":
                    this.NewRq = new CoreMessage(MessageType, InputParams, isEPCS, scheduleCode, dpsNumber, benefitParams);
                    break;

                case "Refillrequest":
                    this.RefReq = new CoreMessage(MessageType, InputParams);
                    break;

                case "RefillResponse":
                    this.RefResp = new CoreMessage(MessageType, InputParams, isEPCS, scheduleCode, dpsNumber, benefitParams);
                    break;

				case "CancelRx":
					this.CancelReq = new CancelRxType(InputParams);
					break;

                case "Verify":
                    this.Verify = new VerifyStatus { Status = new GenericStatus("010") };
                    break;

                case "Status":
                    this.Status = new GenericStatus("010");
                    break;

                case "Error":
                    if (!(InputParams[0] == "5"))
                    {
                        if (InputParams[0] == "1")
                        {
                            this.Error = new ErrorStatus("900", "000", "Physician not found.");
                        }
                        else if (InputParams[0] == "2")
                        {
                            this.Error = new ErrorStatus("900", "000", "Correlated PON was found, but had a different Patient ID or Patient Name.");
                        }
                        else if (InputParams[0] == "3")
                        {
                            this.Error = new ErrorStatus("900", "000", "Prescription not matching.");
                        }
                        else if (InputParams[0] == "4")
                        {
                            this.Error = new ErrorStatus("900", null, "Compounds or non-drug supply items are not supported.");
                        }
                        break;
                    }
                    this.Error = new ErrorStatus("900", "000", "Validation Error.");
                    break;

                case "DirDwnld":
                    this.DirDwnld = new DirectoryDownload(InputParams);
                    break;

                case "DirDwnldResponse":
                    this.DirDwnldResponse = new DirectoryDownloadResponse();
                    break;

                case "AddPrescriber":
                    DSPrescriber prescriber = new DSPrescriber(InputParams[1], int.Parse(InputParams[2]));
                    string rootSPI = null;
                    if (!prescriber.mPrescriber.HasOtherSurescriptsPrescriberWithSameNPI(out rootSPI))
                    {
                        this.AddPrescriber = prescriber;
                        if ((this.AddPrescriber.mPrescriber.mClinicName ?? "").Length > 0x23)
                        {
                            this.AddPrescriber.mPrescriber.mClinicName = this.AddPrescriber.mPrescriber.mClinicName.Substring(0, 0x23);
                        }
                        if ((this.AddPrescriber.mPrescriber.mPrescriberAddress != null) && (this.AddPrescriber.mPrescriber.mPrescriberAddress.Zip != null))
                        {
                            this.AddPrescriber.mPrescriber.mPrescriberAddress.Zip = this.AddPrescriber.mPrescriber.mPrescriberAddress.Zip.Replace("-", "");
                        }
                    }
                    else
                    {
                        prescriber.mPrescriber.mPrescriberID.DSSPI = rootSPI;
                        this.AddPrescriberLocation = prescriber;
                        if ((this.AddPrescriberLocation.mPrescriber.mClinicName ?? "").Length > 0x23)
                        {
                            this.AddPrescriberLocation.mPrescriber.mClinicName = this.AddPrescriberLocation.mPrescriber.mClinicName.Substring(0, 0x23);
                        }
                        if ((this.AddPrescriberLocation.mPrescriber.mPrescriberAddress != null) && (this.AddPrescriberLocation.mPrescriber.mPrescriberAddress.Zip != null))
                        {
                            this.AddPrescriberLocation.mPrescriber.mPrescriberAddress.Zip = this.AddPrescriberLocation.mPrescriber.mPrescriberAddress.Zip.Replace("-", "");
                        }
                    }
                    break;

                case "AddPrescriberResp":
                    this.AddPrescriberResp = new DSPrescriber(InputParams[1]);
                    break;

                case "AddPrescriberLocationResponse":
                    this.AddPrescriberLocationResponse = new DSPrescriber(InputParams[1]);
                    break;

                case "UpdatePrescriber":
                    this.UpdatePrescriber = new DSPrescriber(InputParams[1], int.Parse(InputParams[2]));
                    if ((this.UpdatePrescriber.mPrescriber.mClinicName ?? "").Length > 0x23)
                    {
                        this.UpdatePrescriber.mPrescriber.mClinicName = this.UpdatePrescriber.mPrescriber.mClinicName.Substring(0, 0x23);
                    }
                    if ((this.UpdatePrescriber.mPrescriber.mPrescriberAddress != null) && (this.UpdatePrescriber.mPrescriber.mPrescriberAddress.Zip != null))
                    {
                        this.UpdatePrescriber.mPrescriber.mPrescriberAddress.Zip = this.UpdatePrescriber.mPrescriber.mPrescriberAddress.Zip.Replace("-", "");
                    }
                    break;
            }
        }
    }
}

