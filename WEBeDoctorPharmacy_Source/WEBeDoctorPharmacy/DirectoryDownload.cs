namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Xml.Serialization;

    public class DirectoryDownload
    {
        [XmlElement("AccountID")]
        public string mSSAccountID = ConfigurationManager.AppSettings["SSAccountID"].ToString();
        [XmlElement("VersionID")]
        public string mSSVersionID = ConfigurationManager.AppSettings["SSVersionID"].ToString();
        [XmlElement("Taxonomy")]
        public Taxonomy mTaxonomy = new Taxonomy();
        [XmlElement("DirectoryDate")]
        public string mDirectoryDate;

        public DirectoryDownload() { }
        
        public DirectoryDownload(string[] InputParams)
        {
            this.mTaxonomy.TaxonomyCode = ConfigurationManager.AppSettings["SSTaxonomyCode"].ToString();
            if (InputParams[0] != "full")
            {
                this.mDirectoryDate = DateTime.Now.AddDays(-1.0).ToString("yyyyMMdd");
            }
        }

        public class Taxonomy
        {
            [XmlElement("TaxonomyCode")]
            public string TaxonomyCode;
        }

    }
}

