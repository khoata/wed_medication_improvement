namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;

    public static class MessageFuncs
    {
        public static string[] LoadNewRxParams(string Drug_ID, string PrescriberOrderNumber)
        {
            string[] NewRXParams = new string[8];
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.LoadCorrelatedNewRxParams";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamDrugID", OracleType.VarChar)).Value = Drug_ID;
            command.Parameters.Add(new OracleParameter("ParamPRESCRIBERORDERNUMBER", OracleType.VarChar)).Value = PrescriberOrderNumber;
            command.Parameters.Add(new OracleParameter("CurNewRxParams", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                NewRXParams[0] = reader.GetOracleString(0).IsNull ? null : reader.GetOracleString(0).ToString();
                NewRXParams[1] = reader.GetOracleString(1).IsNull ? null : reader.GetOracleString(1).ToString();
                NewRXParams[2] = reader.GetOracleString(2).IsNull ? null : reader.GetOracleString(2).ToString();
                NewRXParams[3] = reader.GetOracleString(3).IsNull ? null : reader.GetOracleString(3).ToString();
                NewRXParams[4] = reader.GetOracleString(4).IsNull ? null : reader.GetOracleString(4).ToString();
            }
            reader.Close();
            connection.Close();
            return NewRXParams;
        }

        public static bool DirectoryServicesLoad()
        {
            string SuccessFlag = "0";

            OracleConnection connection = new OracleConnection("Data Source=MDM3; User Id=Medical; Password=123456");
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSDSLOAD.CheckImportTable";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("EmptyFlag", OracleType.VarChar, 1, string.Empty)).Direction = ParameterDirection.Output;
            try
            {
                command.ExecuteNonQuery();

                if (command.Parameters["EmptyFlag"].Value.ToString() == "1")
                {
                    OracleCommand command1 = new OracleCommand();
                    command1.Connection = connection;
                    command1.CommandText = "WSSDSLOAD.InsertUpdatePharmacy";
                    command1.CommandType = CommandType.StoredProcedure;
                    command1.Parameters.Add(new OracleParameter("SuccessFlag", OracleType.VarChar, 1, string.Empty)).Direction = ParameterDirection.Output;
                    command1.ExecuteNonQuery();

                    SuccessFlag = command1.Parameters["SuccessFlag"].Value.ToString();
                }
            }
            catch (Exception e)
            {
                //SS_Logger.log("Error in DIRECTORYSERVICELOAD: " + e.StackTrace);

                //if (e.InnerException != null)
                //{
                //    SS_Logger.log("Inner Error in DIRECTORYSERVICELOAD: " + e.InnerException.StackTrace);

                //}
                throw(e);
            }


            finally
            {
                connection.Close();
            }

            if (SuccessFlag == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

