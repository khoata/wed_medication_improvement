namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;
    using System.Configuration;

    public class DirectoryInformation
    {
        [XmlElement("PortalID")]
        public string PortalID = ConfigurationManager.AppSettings["SSPortalID"].ToString();
        [XmlElement("AccountID")]
        public string AccountID = ConfigurationManager.AppSettings["SSAccountID"].ToString();
        [XmlElement("BackupPortalID")]
        public string BackupPortalID = ConfigurationManager.AppSettings["SSBackupPortalID"].ToString();
        [XmlElement("ServiceLevel")]
        public string ServiceLevel = ConfigurationManager.AppSettings["SSServiceLevel"].ToString();
        [XmlElement("ActiveStartTime")]
        public string ActiveStartTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.0Z");
        [XmlElement("ActiveEndTime")]
        public string ActiveEndTime = DateTime.Now.AddYears(5).ToString("yyyy-MM-ddTHH:mm:ss.0Z");

    }
}

