namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;
    using System.Collections.Generic;

    public class PrescriberInfo
    {
        [XmlElement("DirectoryInformation")]
        public DirectoryInformation mDirectoryInfo;
        [XmlElement("Identification")]
        public ID mPrescriberID;
        [XmlElement("Specialty")]
        public string Specialty;
        [XmlElement("ClinicName")]
        public string mClinicName;
        [XmlElement("Name")]
        public Name mPrescriberName;
        [XmlIgnore()]   // Specialty conflict, Prescription routing vs Directory
        public Specialty mSpecialty;
        [XmlElement("Address")]
        public Address mPrescriberAddress;
        [XmlElement("CommunicationNumbers")]
        public PhysicianPhoneNumbers mPrescriberPhone;
        [XmlElement("PrescriberAgent")]
        public Name mPrescriberAgent;


        public PrescriberInfo()
        {

            //KEN, I added this back
            //this.mDirectoryInfo = new DirectoryInformation();
            //this.mPrescriberID = new ID();
            //this.mPrescriberName = new Name();
            //this.mSpecialty = new Specialty();
            //this.mPrescriberAgent = new Name();
            //this.mPrescriberAddress = new Address();
            //this.mPrescriberPhone = new PhysicianPhoneNumbers();
            //this.mPrescriberPhone.mPhone = new Phone[1];
        }

        public PrescriberInfo(string PrescriberID)
        {
            this.Load(PrescriberID);
        }

        public PrescriberInfo(string PrescriberID, int ServiceLevel)
        {
            this.mDirectoryInfo = new DirectoryInformation();
            this.mDirectoryInfo.PortalID = ConfigurationManager.AppSettings["SSPortalID"].ToString();
            this.mDirectoryInfo.AccountID = ConfigurationManager.AppSettings["SSAccountID"].ToString();
            this.mDirectoryInfo.BackupPortalID = ConfigurationManager.AppSettings["SSBackupPortalID"].ToString();
            this.mDirectoryInfo.ServiceLevel = ServiceLevel.ToString();
            this.mDirectoryInfo.ActiveStartTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.0Z");
            if (ServiceLevel.ToString() == "0")
            {
                this.mDirectoryInfo.ActiveEndTime = this.mDirectoryInfo.ActiveStartTime;
            }
            else
            {
                this.mDirectoryInfo.ActiveEndTime = DateTime.Now.AddYears(5).ToString("yyyy-MM-ddTHH:mm:ss.0Z");
            }
            this.Load(PrescriberID);
        }

        public PrescriberInfo(string dummyOverload, string MessageId)
        {
            this.LoadFromRefillRequest(MessageId);
        }

        public virtual void Load(string PrescriberID)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPrescriberInfo";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPrescriberID", OracleType.VarChar)).Value = PrescriberID;
            command.Parameters.Add(new OracleParameter("CurPrescriberInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPrescriberID = new ID();
                this.mPrescriberID.SPI = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                this.mPrescriberID.NPI = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                this.mPrescriberID.DEANumber = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mClinicName = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPrescriberName = new Name();
                this.mPrescriberName.LastName = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mPrescriberName.FirstName = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.mSpecialty = new Specialty();
                this.mSpecialty.Qualifier = "AM";
                this.mSpecialty.SpecialtyCode = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));
                if (!reader.GetOracleString(6).IsNull)
                {
                    this.mPrescriberAddress = new Address();
                    this.mPrescriberAddress.Address1 = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                    this.mPrescriberAddress.City = reader.GetOracleString(8).IsNull ? null : ((string) reader.GetOracleString(8));
                    this.mPrescriberAddress.State = reader.GetOracleString(9).IsNull ? null : ((string) reader.GetOracleString(9));
                    this.mPrescriberAddress.Zip = reader.GetOracleString(10).IsNull ? null : ((string) reader.GetOracleString(10));
                }

                List<Phone> phones = new List<Phone>();
                if (!reader.GetOracleString(12).IsNull && !reader.GetOracleString(13).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = ((string)reader.GetOracleString(12)).Replace("-", ""),
                        Qualifier = (string)reader.GetOracleString(13),
                    });
                }
                if (!reader.GetOracleString(14).IsNull && !reader.GetOracleString(15).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = ((string)reader.GetOracleString(14)).Replace("-", ""),
                        Qualifier = (string)reader.GetOracleString(15),
                    });
                }
                // Put email at the end of the list because origial design always treats first communication number is a phone
                if (!reader.GetOracleString(11).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = (string)reader.GetOracleString(11),
                        Qualifier = "EM",
                    });
                }
                this.mPrescriberPhone = new PhysicianPhoneNumbers() { mPhone = phones.ToArray()};
                this.mPrescriberAddress.Address2 = reader.GetOracleString(16).IsNull ? null : ((string)reader.GetOracleString(16));
                if (!string.IsNullOrEmpty(this.mPrescriberAddress.Address2))
                {
                    this.mPrescriberAddress.PlaceLocationQualifier = "AD2";
                }
            }
            reader.Close();
            connection.Close();
        }

        public virtual void LoadFromRefillRequest(string MessageId)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPrescriberFromRefReq";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageId", OracleType.VarChar)).Value = MessageId;
            command.Parameters.Add(new OracleParameter("CurPrescriberInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPrescriberID = new ID();
                this.mPrescriberID.SPI = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                if (!reader.GetOracleString(1).IsNull && reader.GetOracleString(1) != "")
                {
                    this.mPrescriberID.NPI = reader.GetOracleString(1).IsNull ? null : ((string)reader.GetOracleString(1));
                }
                if (!reader.GetOracleString(2).IsNull && reader.GetOracleString(2) != "")
                {
                    this.mPrescriberID.DEANumber = reader.GetOracleString(2).IsNull ? null : ((string)reader.GetOracleString(2));
                }
                this.mClinicName = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPrescriberName = new Name();
                this.mPrescriberName.LastName = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mPrescriberName.FirstName = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                if (!reader.GetOracleString(28).IsNull)
                {
                    this.mPrescriberName.MiddleName = reader.GetOracleString(28).IsNull ? null : ((string)reader.GetOracleString(28));
                }
                //this.mPrescriberAgent = new Name();
                //this.mPrescriberAgent.LastName = this.mPrescriberName.LastName;
                //this.mPrescriberAgent.FirstName = this.mPrescriberName.FirstName;
                //if (this.mPrescriberName.MiddleName != null)
                //{
                //    this.mPrescriberAgent.MiddleName = this.mPrescriberName.MiddleName;
                //}
                if (!reader.GetOracleString(6).IsNull && reader.GetOracleString(6) != "")
                {
                    this.mSpecialty = new Specialty();
                    this.mSpecialty.Qualifier = reader.GetOracleString(7).IsNull ? null : ((string)reader.GetOracleString(7));
                    this.mSpecialty.SpecialtyCode = reader.GetOracleString(6).IsNull ? null : ((string)reader.GetOracleString(6));
                }
                if (!reader.GetOracleString(8).IsNull)
                {
                    this.mPrescriberAddress = new Address();
                    this.mPrescriberAddress.Address1 = reader.GetOracleString(8).IsNull ? null : ((string)reader.GetOracleString(8));
                    this.mPrescriberAddress.City = reader.GetOracleString(9).IsNull ? null : ((string) reader.GetOracleString(9));
                    this.mPrescriberAddress.State = reader.GetOracleString(10).IsNull ? null : ((string) reader.GetOracleString(10));
                    this.mPrescriberAddress.Zip = reader.GetOracleString(11).IsNull ? null : ((string) reader.GetOracleString(11));
                }

                List<Phone> phones = new List<Phone>();
                int startPhoneIndex = 13;
                while (startPhoneIndex < 28)
                {
                    if (!reader.GetOracleString(startPhoneIndex).IsNull && !reader.GetOracleString(startPhoneIndex + 1).IsNull)
                    {
                        phones.Add(new Phone
                        {
                            PhoneNumber = ((string)reader.GetOracleString(startPhoneIndex)).Replace("-", ""),
                            Qualifier = (string)reader.GetOracleString(startPhoneIndex + 1)
                        });
                    }
                    startPhoneIndex += 2;
                }
                // Put email at the end of the list because origial design always treats first communication number is a phone
                if (!reader.GetOracleString(12).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = (string)reader.GetOracleString(12),
                        Qualifier = "EM",
                    });
                }
                this.mPrescriberPhone = new PhysicianPhoneNumbers { mPhone = phones.ToArray()};
            }
            reader.Close();
            connection.Close();
        }

        public virtual bool HasOtherSurescriptsPrescriberWithSameNPI(out string rootSPI)
        {
            using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            using (OracleCommand command = new OracleCommand())
            {
                rootSPI = null;
                connection.Open();
                command.Connection = connection;
                command.CommandText = "WSSInterface.GetPrescriberByNPI";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new OracleParameter("ParamNPI", OracleType.VarChar)).Value = mPrescriberID.NPI;
                command.Parameters.Add(new OracleParameter("CurPrescriberInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    string spi = reader.GetOracleString(1).IsNull ? null : ((string)reader.GetOracleString(1));
                    if (IsOtherSurescriptsPrescriber(spi))
                    {
                        rootSPI = spi.Substring(0, 10);
                        return true;
                    }
                }
                return false;
            }
        }

        private bool IsOtherSurescriptsPrescriber(string SPI)
        {
            if ((mPrescriberID.SPI != SPI) && !string.IsNullOrEmpty(SPI))
            {
                return true;
            }

            return false;
        }
    }
}

