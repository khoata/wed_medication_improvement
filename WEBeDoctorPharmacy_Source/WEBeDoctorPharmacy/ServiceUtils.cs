﻿namespace WEBeDoctorPharmacy
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Collections;
    using System.IO;
    using ICSharpCode.SharpZipLib.Zip;


    public class ServiceUtils
    {
        public static ArrayList unzipFile(string zipFileLocation, string outputFolder)
        {
            // Perform simple parameter checking.
            if (string.IsNullOrEmpty(zipFileLocation))
            {
                throw new ArgumentException(string.Format("Empty zipfile location: {0}", zipFileLocation));
            }

            if (!File.Exists(zipFileLocation))
            {
                throw new FileNotFoundException(string.Format("Cannot find file: {0}", zipFileLocation));
            }

            ArrayList outputFiles = null;

            try
            {
                using (ZipInputStream s = new ZipInputStream(File.OpenRead(zipFileLocation)))
                {
                    outputFiles = new ArrayList();

                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        string filePath = outputFolder + theEntry.Name;
                        SS_Logger.log("Unzip file " + theEntry.Name);
                        if (File.Exists(filePath))
                        {
                            SS_Logger.log("File " + filePath + " already exists. Deleting to place a new file");
                            File.Delete(filePath);
                        }
                        using (FileStream streamWriter = File.Create(filePath))
                        {
                            int size = 2048;
                            byte[] data = new byte[2048];
                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);
                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        outputFiles.Add(filePath);
                    }

                }
            }
            catch (Exception e)
            {
                throw new ZipException(string.Format("Can not unzip file: {0}", e.StackTrace));
            }
            return outputFiles;

        }

    }
}
