namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.IO;
    using System.Xml.Serialization;

    public class MedicationInfo
    {
        [XmlElement("DrugDescription")]
        public string DrugDescription;
        [XmlElement("DrugCoded")]
        public DrugCoded DrugCode;
        [XmlElement("Quantity")]
        public QuantityType Quantity;
        [XmlElement("DaysSupply")]
        public string DaysSupply;
        [XmlElement("Directions")]
        public string Directions;
        [XmlElement("Note")]
        public string Note;
        [XmlElement("Refills")]
        public QualifierValuePair Refills;
        [XmlElement("Substitutions")]
        public string Substitutions;
        [XmlElement("WrittenDate")]
        public DateType WrittenDate;
        [XmlElement("LastFillDate")]
        public DateType LastFillDate;
        [XmlElement("ExpirationDate")]
        public DateType ExpirationDate;
        [XmlElement("EffectiveDate")]
        public DateType EffectiveDate;
        [XmlElement("PeriodEnd")]
        public DateType PeriodEnd;
        [XmlElement("DeliveredOnDate")]
        public DateType DeliveredOnDate;
        [XmlElement("DateValidated")]
        public DateType DateValidated;
        [XmlElement("Diagnosis")]
        public Diagnosis[] Diagnosis1;
        [XmlElement("PriorAuthorization")]
        public PriorAuthorization PriorAuthorization;
        [XmlElement("DrugUseEvaluation")]
        public DrugUseEvaluationType[] DrugUseEvaluation;
        [XmlElement("DrugCoverageStatusCode")]
        public string[] DrugCoverageStatusCode;
        [XmlElement("PriorAuthorizationStatus")]
        public string PriorAuthorizationStatus;
        [XmlElement("StructuredSIG")]
        public SIGType[] StructuredSIG;

        public MedicationInfo()
        {
        }

        public MedicationInfo(string DrugID, string visitKey)
            : this(DrugID, visitKey, false, "")
        {
        }

        public MedicationInfo(string DrugID, string visitKey, bool isEPCS, string scheduleCode)
        {
            this.Load(DrugID, visitKey, isEPCS, scheduleCode);
        }

        public MedicationInfo(string MessageId, string subid, string dummyOverload)
            : this(MessageId, subid, dummyOverload, false, "")
        {
        }

        public MedicationInfo(string MessageId, string subid, string dummyOverload, bool isEPCS, string scheduleCode)
        {
            this.LoadFromRefillRequest(MessageId, subid, isEPCS, scheduleCode);
        }

        public void Load(string drugID, string visitKey)
        {
            this.Load(drugID, visitKey, false, ""); 
        }
        public void Load(string drugID, string visitKey, bool isEPCS, string scheduleCode)
        {
            string strConn = ConfigurationManager.ConnectionStrings["OraDBConn"].ConnectionString;
            OracleConnection connection = new OracleConnection(strConn);

            //OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetMedicationPrescribed";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPrescriptionID", OracleType.VarChar)).Value = drugID;
            command.Parameters.Add(new OracleParameter("ParamVisitKey", OracleType.VarChar)).Value = visitKey;
            command.Parameters.Add(new OracleParameter("CurPrescription", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.DrugDescription = reader.GetOracleString(0).IsNull ? null : ((reader.GetOracleString(0).ToString().Length > 0x69) ? reader.GetOracleString(0).ToString().Substring(0, 0x69) : reader.GetOracleString(0).ToString());
                this.DrugDescription = this.DrugDescription.Trim();
                if (!reader.GetOracleString(1).IsNull)
                {
                    this.DrugCode = new DrugCoded();
                    this.DrugCode.ProductCode = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                    this.DrugCode.ProductCodeQualifier = (string) reader.GetOracleString(2);
                    if (!reader.GetOracleString(12).IsNull && !reader.GetOracleString(13).IsNull)
                    {
                        if ((string)reader.GetOracleString(13) == "59")
                        {
                            this.DrugCode.DrugDBCodeQualifier = "SCD";
                        }
                        else if ((string)reader.GetOracleString(13) == "60")
                        {
                            this.DrugCode.DrugDBCodeQualifier = "SBD";
                        }
                        if (!string.IsNullOrEmpty(this.DrugCode.DrugDBCodeQualifier))
                        {
                            this.DrugCode.DrugDBCode = (string)reader.GetOracleString(12);
                        }
                    }
                }
                if (!reader.GetOracleString(14).IsNull && reader.GetOracleString(14) == "SP")
                {
                    this.DrugCoverageStatusCode = new string[1] { "SP" };
                }
                this.Quantity = new QuantityType()
                {
                    Value = reader.GetOracleString(3).IsNull ? null : ((string)reader.GetOracleString(3)),
                    CodeListQualifier = "38",
                    UnitSourceCode = "AC",
                    PotencyUnitCode = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4))
                };
                //this.Quantity.Value = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                //this.Quantity.Qualifier = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.Directions = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.Note = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));

                if (this.Note != null)
                {
                    this.Note = this.Note.Replace('\r', ' ').Replace('\n', ' ');
                }
                this.Refills = new QualifierValuePair();
                this.Refills.Value = null;
                this.Refills.Qualifier = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                if (this.Refills.Qualifier != "PRN")
                {
                    this.Refills.Value = reader.GetOracleValue(8).ToString();
                }
                this.Substitutions = reader.GetOracleString(9).IsNull ? "0" : ((string) reader.GetOracleString(9));
                this.WrittenDate = new DateType { Item = DateTime.Now, ItemElementName = ItemChoiceType.Date};
                if (isEPCS)
                {
                    this.DrugCoverageStatusCode = new string[1] { "SI" };
                    
                    switch (scheduleCode)
                    {
                        case "C48675":
                            this.DrugCode.DEASchedule = DrugCodedTypeDEASchedule.C48675;
                            this.DrugCode.DEAScheduleSpecified = true;
                            break;
                        case "C48676":
                            this.DrugCode.DEASchedule = DrugCodedTypeDEASchedule.C48676;
                            this.DrugCode.DEAScheduleSpecified = true;
                            break;
                        case "C48677":
                            this.DrugCode.DEASchedule = DrugCodedTypeDEASchedule.C48677;
                            this.DrugCode.DEAScheduleSpecified = true;
                            break;
                        case "C48679":
                            this.DrugCode.DEASchedule = DrugCodedTypeDEASchedule.C48679;
                            this.DrugCode.DEAScheduleSpecified = true;
                            break;
                    }
                    
                }
            }
            reader.Close();
            connection.Close();
        }

        public void LoadFromRefillRequest(string MessageId, string subid)
        {
            this.LoadFromRefillRequest(MessageId, subid, false, "");
        }
        public void LoadFromRefillRequest(string MessageId, string subid, bool isEPCS, string scheduleCode)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetMedFromRefReq";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageID", OracleType.VarChar)).Value = MessageId;
            command.Parameters.Add(new OracleParameter("ParamSubstitutionID", OracleType.VarChar)).Value = subid ?? "";
            command.Parameters.Add(new OracleParameter("CurPrescription", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.DrugDescription = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                if (!reader.GetOracleString(1).IsNull)
                {
                    this.DrugCode = new DrugCoded();
                    this.DrugCode.ProductCode = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                    this.DrugCode.ProductCodeQualifier = (string) reader.GetOracleString(2);
                    // Not support by RxRouting 10.6
                    //if (! reader.GetOracleString(11).IsNull)
                    //{
                    //    this.DrugCode.DosageForm = reader.GetOracleString(11).IsNull ? null : ((string)reader.GetOracleString(11));
                    //}
                    if (!reader.GetOracleString(12).IsNull)
                    {
                        this.DrugCode.Strength = reader.GetOracleString(12).IsNull ? null : ((string)reader.GetOracleString(12));
                        // Not support by RxRouting 10.6
                        //this.DrugCode.StrengthUnits = reader.GetOracleString(13).IsNull ? null : ((string)reader.GetOracleString(13));
                    }
                    if (!reader.GetOracleString(14).IsNull)
                    {
                        this.DrugCode.DrugDBCode = reader.GetOracleString(14).IsNull ? null : ((string)reader.GetOracleString(14));
                        this.DrugCode.DrugDBCodeQualifier = reader.GetOracleString(15).IsNull ? null : ((string)reader.GetOracleString(15));
                    }
                }
                this.Quantity = new QuantityType
                {
                    Value = reader.GetOracleString(3).IsNull ? null : ((string)reader.GetOracleString(3)),
                    CodeListQualifier = "38",
                    UnitSourceCode = "AC",
                    PotencyUnitCode = reader.GetOracleString(4).IsNull ? null : ((string)reader.GetOracleString(4)),
                };
                //this.Quantity.Value = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                //this.Quantity.Qualifier = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                if (!reader.GetOracleString(16).IsNull && reader.GetOracleString(16) != "")
                {
                    this.DaysSupply = reader.GetOracleString(16).IsNull ? null : ((string)reader.GetOracleString(16));
                }
                this.Directions = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.Note = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));

                if (this.Note != null)
                {
                    this.Note = this.Note.Replace('\r', ' ').Replace('\n', ' ');
                }

                this.Refills = new QualifierValuePair();
                this.Refills.Value = null;
                this.Refills.Qualifier = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                if (this.Refills.Qualifier != "PRN")
                {
                    this.Refills.Value = reader.GetOracleValue(8).ToString();
                }
                if (((this.Refills.Value ?? "0") == "0") && (this.Refills.Qualifier == "R"))
                {
                    this.Refills.Value = null;
                }
                this.Substitutions = reader.GetOracleString(9).IsNull ? "0" : ((string) reader.GetOracleString(9));
                this.WrittenDate = new DateType { Item = DateTime.Now, ItemElementName = ItemChoiceType.Date};
                if (!reader.GetOracleString(17).IsNull && reader.GetOracleString(17) != "")
                {
                    this.LastFillDate = new DateType { Item = DateTime.Parse((string)reader.GetOracleString(17)), ItemElementName = ItemChoiceType.Date };
                }
                //if (!reader.GetOracleString(17).IsNull && reader.GetOracleString(18) != "")
                //{

                //}
                if (isEPCS)
                {
                    this.DrugCoverageStatusCode = new string[1] { "SI" };

                    switch (scheduleCode)
                    {
                        case "C48675":
                            this.DrugCode.DEASchedule = DrugCodedTypeDEASchedule.C48675;
                            this.DrugCode.DEAScheduleSpecified = true;
                            break;
                        case "C48676":
                            this.DrugCode.DEASchedule = DrugCodedTypeDEASchedule.C48676;
                            this.DrugCode.DEAScheduleSpecified = true;
                            break;
                        case "C48677":
                            this.DrugCode.DEASchedule = DrugCodedTypeDEASchedule.C48677;
                            this.DrugCode.DEAScheduleSpecified = true;
                            break;
                        case "C48679":
                            this.DrugCode.DEASchedule = DrugCodedTypeDEASchedule.C48679;
                            this.DrugCode.DEAScheduleSpecified = true;
                            break;
                    }
                }
            }
            reader.Close();
            connection.Close();
        }

        public void LoadFromSurescriptsMedication(string id)
        {
            using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            using (OracleCommand command = new OracleCommand())
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText = "WSSInterface.GetSSMessageMedication";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new OracleParameter("ParamID", OracleType.VarChar)).Value = id;
                command.Parameters.Add(new OracleParameter("CurMedicationInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
                using (OracleDataReader reader = command.ExecuteReader())
                {
                    MedicationInfo mInfo = null;

                    while (reader.Read())
                    {
                        var rawContent = reader.GetOracleString(23).IsNull ? string.Empty
                            : (string)reader.GetOracleString(23);

                        var serializer = new XmlSerializer(typeof(MedicationInfo), Message.GetNamespaceOfRxRoutingMessage());
                        using (TextReader treader = new StringReader(rawContent))
                        {
                            mInfo = serializer.Deserialize(treader) as MedicationInfo;
                            if (mInfo != null)
                            {
                                DrugDescription = mInfo.DrugDescription;
                                DrugCode = mInfo.DrugCode;
                                Quantity = mInfo.Quantity;
                                DaysSupply = mInfo.DaysSupply;
                                Directions = mInfo.Directions;
                                Note = mInfo.Note;
                                Refills = mInfo.Refills;
                                Substitutions = mInfo.Substitutions;
                                WrittenDate = mInfo.WrittenDate;
                                LastFillDate = mInfo.LastFillDate;
                                ExpirationDate = mInfo.ExpirationDate;
                                EffectiveDate = mInfo.EffectiveDate;
                                PeriodEnd = mInfo.PeriodEnd;
                                DeliveredOnDate = mInfo.DeliveredOnDate;
                                DateValidated = mInfo.DateValidated;
                                Diagnosis1 = mInfo.Diagnosis1;
                                PriorAuthorization = mInfo.PriorAuthorization;
                                DrugUseEvaluation = mInfo.DrugUseEvaluation;
                                DrugCoverageStatusCode = mInfo.DrugCoverageStatusCode;
                                PriorAuthorizationStatus = mInfo.PriorAuthorizationStatus;
                                StructuredSIG = mInfo.StructuredSIG;
                            }
                        }
                    }

                    if (mInfo == null)
                        throw new Exception(string.Format("No Surescripts medication with id {0} found", id));
                }
            }
        }
    }
}

