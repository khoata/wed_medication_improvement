﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WEBeDoctorPharmacy
{
    public class DSPrescriberInfo : PrescriberInfo
    {
        [XmlElement("Email")]
        public string mPrescriberEmail;

        [XmlElement("PhoneNumbers")]
        public DSPhysicianPhoneNumbers mDSPrescriberPhone;

        public DSPrescriberInfo()
            : base()
        {
        }

        public DSPrescriberInfo(string PrescriberID)
            : base(PrescriberID)
        {
        }

        public DSPrescriberInfo(string PrescriberID, int ServiceLevel)
            : base(PrescriberID, ServiceLevel)
        {
        }

        public override void Load(string PrescriberID)
        {
            base.Load(PrescriberID);
            OrganizeAddress();
            OrganizeCommnunicationNumber();
            OrganizeSPI();
        }

        public override void LoadFromRefillRequest(string MessageId)
        {
            base.LoadFromRefillRequest(MessageId);
            OrganizeAddress();
            OrganizeCommnunicationNumber();
            OrganizeSPI();
        }

        private void OrganizeAddress()
        {
            mPrescriberAddress.PlaceLocationQualifier = null;
        }

        private void OrganizeCommnunicationNumber()
        {
            if (mPrescriberPhone != null &&
                mPrescriberPhone.mPhone != null &&
                mPrescriberPhone.mPhone.Length > 0)
            {
                string email = null;
                List<Phone> phones = new List<Phone>();
                foreach (var each in mPrescriberPhone.mPhone)
                {
                    if (each.Qualifier == "EM")
                    {
                        email = each.PhoneNumber;
                    }
                    else
                    {
                        phones.Add(each);
                    }
                }

                mPrescriberEmail = email;
                base.mPrescriberPhone = null;
                mDSPrescriberPhone = new DSPhysicianPhoneNumbers
                {
                    mPhone = phones.ToArray(),
                };
            }
        }

        private void OrganizeSPI()
        {
            mPrescriberID.DSSPI = mPrescriberID.SPI;
        }
    }
}
