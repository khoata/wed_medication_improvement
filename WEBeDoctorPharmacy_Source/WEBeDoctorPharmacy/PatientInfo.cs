namespace WEBeDoctorPharmacy
{
    using System;
    using System.Collections;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    using System.Globalization;

    [XmlRoot("PatientInfo")]
    public class PatientInfo
    {
        protected bool loadedFromDB;

        [XmlElement("PatientRelationship")]
        public string mPatientRelationship;
        [XmlElement("Identification")]
        public ID mPatientID;
        [XmlElement("Name")]
        public Name mPatientName;
        [XmlElement("Gender")]
        public string mGender;
        [XmlElement("DateOfBirth")]
        public DateType mDateOfBirth;
        [XmlElement("Address")]
        public Address mPatientAddress;
        [XmlElement("CommunicationNumbers")]
        public PhysicianPhoneNumbers mPatientPhone;

        public PatientInfo()
        {
            this.loadedFromDB = false;
        }

        public PatientInfo(string vPatientID)
        {
            this.loadedFromDB = false;
            this.Load(vPatientID);
        }

        public PatientInfo(string dummyForOverload, string refillRequestMessageId)
        {
            this.loadedFromDB = false;
            this.LoadFromRefillRequest(refillRequestMessageId);
        }

        public void Load(string PatientID)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPatientInfo";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPatientID", OracleType.VarChar)).Value = PatientID;
            command.Parameters.Add(new OracleParameter("CurPatientInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPatientID = new ID();
                this.mPatientID.FileID = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                this.mPatientID.SocialSecurityNumber = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));
                this.mPatientName = new Name();
                this.mPatientName.FirstName = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mPatientName.LastName = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPatientName.MiddleName = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mPatientName.Prefix = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.mPatientName.Suffix = reader.GetOracleString(6).IsNull ? null : ((string) reader.GetOracleString(6));
                this.mGender = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                this.mDateOfBirth = new DateType { Item = DateTime.Parse(reader.GetOracleDateTime(8).ToString()) , ItemElementName = ItemChoiceType.Date };
                this.mPatientAddress = new Address();
                this.mPatientAddress.Address1 = reader.GetOracleString(9).IsNull ? null : ((string) reader.GetOracleString(9));
                this.mPatientAddress.City = reader.GetOracleString(10).IsNull ? null : ((string) reader.GetOracleString(10));
                this.mPatientAddress.State = reader.GetOracleString(11).IsNull ? null : ((string) reader.GetOracleString(11));
                this.mPatientAddress.Zip = reader.GetOracleString(12).IsNull ? null : ((string) reader.GetOracleString(12));
                if (this.mPatientAddress.Zip != null)
                {
                    this.mPatientAddress.Zip = this.mPatientAddress.Zip.Replace("-", "");
                }

                List<Phone> phones = new List<Phone>();
                if (!reader.GetOracleString(14).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = ((string)reader.GetOracleString(14)).Replace("-", ""),
                        Qualifier = "TE",
                    });
                }
                // Put email at the end of the list because origial design always treats first communication number is a phone
                if (!reader.GetOracleString(13).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = (string)reader.GetOracleString(13),
                        Qualifier = "EM",
                    });
                }
                this.mPatientPhone = new PhysicianPhoneNumbers { mPhone = phones.ToArray()};
                this.mPatientAddress.Address2 = reader.GetOracleString(18).IsNull ? null : ((string)reader.GetOracleString(18));
                if (!string.IsNullOrEmpty(this.mPatientAddress.Address2))
                {
                    this.mPatientAddress.PlaceLocationQualifier = "AD2";
                }
                this.loadedFromDB = true;
            }
            reader.Close();
            connection.Close();
        }

        public void LoadFromRefillRequest(string PatientID)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPatientFromRefReq";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamMessageId", OracleType.VarChar)).Value = PatientID;
            command.Parameters.Add(new OracleParameter("CurPatientInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPatientID = new ID();
                this.mPatientID.FileID = reader.GetOracleString(0).IsNull ? null : ((string) reader.GetOracleString(0));
                this.mPatientID.SocialSecurityNumber = reader.GetOracleString(1).IsNull ? null : ((string) reader.GetOracleString(1));

                if (mPatientID.FileID == null && mPatientID.SocialSecurityNumber == null)
                {
                    this.mPatientID = null;
                }

                this.mPatientName = new Name();
                this.mPatientName.FirstName = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mPatientName.LastName = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                if (!reader.GetOracleString(28).IsNull)
                {
                    this.mPatientName.MiddleName = reader.GetOracleString(28).IsNull ? null : ((string)reader.GetOracleString(28));
                }
                this.mPatientName.Prefix = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mGender = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));
                this.mDateOfBirth = reader.GetOracleString(6).IsNull
                    ? null
                    : new DateType { Item = DateTime.ParseExact((string)reader.GetOracleString(6), "yyyyMMdd", CultureInfo.InvariantCulture), ItemElementName = ItemChoiceType.Date };
                this.mPatientAddress = new Address();
                this.mPatientAddress.Address1 = reader.GetOracleString(7).IsNull ? null : ((string) reader.GetOracleString(7));
                this.mPatientAddress.City = reader.GetOracleString(8).IsNull ? null : ((string) reader.GetOracleString(8));
                this.mPatientAddress.State = reader.GetOracleString(9).IsNull ? null : ((string) reader.GetOracleString(9));
                this.mPatientAddress.Zip = reader.GetOracleString(10).IsNull ? null : ((string) reader.GetOracleString(10));

                if (this.mPatientAddress.Address1 == null && this.mPatientAddress.City == null && this.mPatientAddress.State == null && this.mPatientAddress.Zip == null)
                {
                    this.mPatientAddress = null;
                }

                List<Phone> phones = new List<Phone>();
                int startPhoneIndex = 12;
                while (startPhoneIndex < 27)
                {
                    if (!reader.GetOracleString(startPhoneIndex).IsNull && !reader.GetOracleString(startPhoneIndex + 1).IsNull)
                    {
                        phones.Add(new Phone
                        {
                            PhoneNumber = ((string)reader.GetOracleString(12)).Replace("-", ""),
                            Qualifier = (string)reader.GetOracleString(13),
                        });
                    }
                    startPhoneIndex += 2;
                }
                // Put email at the end of the list because origial design always treats first communication number is a phone
                if (!reader.GetOracleString(11).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = (string)reader.GetOracleString(11),
                        Qualifier = "EM",
                    });
                }
                this.mPatientPhone = new PhysicianPhoneNumbers { mPhone = phones.ToArray()};
                this.loadedFromDB = true;
            }
            reader.Close();
            connection.Close();
        }

        public void Save(string PatientID, ArrayList[] PatientInfo)
        {
        }

        public bool wasLoadedFromDb()
        {
            return this.loadedFromDB;
        }
    }
}

