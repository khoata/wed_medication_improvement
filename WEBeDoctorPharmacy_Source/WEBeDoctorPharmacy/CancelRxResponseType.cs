﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WEBeDoctorPharmacy
{
    public partial class CancelRxResponseType
    {
        private Dictionary<string, string> _codeDescription = new Dictionary<string, string>
        {
            { "AA", "Patient unknown to the prescriber" },
            { "AB", "Patient never under provider care" },
            { "AC", "Patient no longer under provider care" },
            { "AD", "Refill too soon" },
            { "AE", "Medication never prescribed for patient" },
            { "AF", "Patient should contact provider" },
            { "AG", "Refill not appropriate" },
            { "AH", "Patient has picked up prescription" },
            { "AJ", "Patient has picked up partial fill of prescription" },
            { "AK", "Patient has not picked up prescription, drug returned to stock" },
            { "AL", "Change not appropriate" },
            { "AM", "Patient needs appointment" },
            { "AN", "Prescriber not associated with this practice or location" },
            { "AO", "No attempt will be made to obtain Prior Authorization" },
            { "AP", "Request already responded to by other means (e.g. phone or fax)" }
        };

        [XmlElement("Request")]
        public CancelRequestType Request;

        [XmlElement("Response")]
        public ChangeResponseType Response;

        public CancelRxResponseType()
        {
        }

        public string GetResponseStatus()
        {
            if (Response != null && Response.Item is ApprovedType)
                return "Approved";
            else if (Response != null && Response.Item is DeniedType)
                return "Denied";
            else
                return "Response Format Error";
        }

        public string GetResponseStatusDescription()
        {
            if (Response != null && Response.Item is ApprovedType)
            {
                var approved = (ApprovedType)Response.Item;
                string codeDes = string.Empty;
                if (approved.ApprovalReasonCode != null && approved.ApprovalReasonCode.Length > 0)
                    codeDes = string.Concat(_codeDescription[approved.ApprovalReasonCode[0]], ". ");

                if (!string.IsNullOrEmpty(approved.Note))
                    return string.Concat(codeDes, approved.Note);
                else
                    return codeDes;
            }
            else if (Response != null && Response.Item is DeniedType)
            {
                var denied = (DeniedType)Response.Item;
                string codeDes = denied.DenialReasonCode.Length > 0
                    ? string.Concat(_codeDescription[denied.DenialReasonCode[0]], ". ")
                    : string.Empty;

                if (!string.IsNullOrEmpty(denied.DenialReason))
                    return string.Concat(codeDes, denied.DenialReason);
                else
                    return codeDes;
            }
            else
            {
                return "No approval or denial information";
            }
        }
    }
}
