namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.OracleClient;
    using System.Xml.Serialization;
    using System.Collections.Generic;

    public class PharmacyInfo
    {
        [XmlElement("Identification")]
        public ID mPharmacyID;
        [XmlElement("Specialty")]
        public string mSpecialty;
        [XmlElement("Pharmacist")]
        public Name mPharmacist;
        [XmlElement("StoreName")]
        public string mStoreName;
        [XmlElement("Address")]
        public Address mPharmacyAddress;
        [XmlElement("CommunicationNumbers")]
        public PhysicianPhoneNumbers mPharmacyPhone;



        public PharmacyInfo()
        {
        }

        public PharmacyInfo(string vPharmacyID)
        {
            this.Load(vPharmacyID);
        }
        public PharmacyInfo(string vMessageId, string dummyOverload)
        {
            this.LoadFromRefReq(vMessageId);
        }

        public void Load(string PharmacyID)
        {
            OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            OracleCommand command = new OracleCommand();
            connection.Open();
            command.Connection = connection;
            command.CommandText = "WSSInterface.GetPharmacyInfo";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new OracleParameter("ParamPharmacyID", OracleType.VarChar)).Value = PharmacyID;
            command.Parameters.Add(new OracleParameter("CurPharmacyInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.mPharmacyID = new ID();
                this.mPharmacyID.NCPDPID = (string) reader.GetOracleString(0);
                this.mStoreName = (string) reader.GetOracleString(1);
                this.mPharmacyAddress = new Address();
                this.mPharmacyAddress.Address1 = reader.GetOracleString(2).IsNull ? null : ((string) reader.GetOracleString(2));
                this.mPharmacyAddress.City = reader.GetOracleString(3).IsNull ? null : ((string) reader.GetOracleString(3));
                this.mPharmacyAddress.State = reader.GetOracleString(4).IsNull ? null : ((string) reader.GetOracleString(4));
                this.mPharmacyAddress.Zip = reader.GetOracleString(5).IsNull ? null : ((string) reader.GetOracleString(5));

                List<Phone> phones = new List<Phone>();
                if (!reader.GetOracleString(7).IsNull && !reader.GetOracleString(8).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = (string)reader.GetOracleString(7),
                        Qualifier = (string)reader.GetOracleString(8),
                    });
                }
                // Put email at the end of the list because origial design always treats first communication number is a phone
                if (!reader.GetOracleString(6).IsNull)
                {
                    phones.Add(new Phone
                    {
                        PhoneNumber = (string)reader.GetOracleString(6),
                        Qualifier = "EM",
                    });
                }
                this.mPharmacyPhone = new PhysicianPhoneNumbers { mPhone = phones.ToArray() };
                this.mPharmacyID.NPI = reader.GetOracleString(9).IsNull ? null : (string)reader.GetOracleString(9);
                this.mPharmacyAddress.Address2 = reader.GetOracleString(10).IsNull ? null : (string)reader.GetOracleString(10);
                if (!string.IsNullOrEmpty(this.mPharmacyAddress.Address2))
                {
                    this.mPharmacyAddress.PlaceLocationQualifier = "AD2";
                }
            }
            reader.Close();
            connection.Close();
        }

        public void LoadFromRefReq(string messageid)
        {
            try
            {

                SS_Logger.log("loading pharmacy from ss_message_content for " + messageid);


                OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                OracleCommand command = new OracleCommand();
                connection.Open();
                command.Connection = connection;
                command.CommandText = "WSSInterface.GetPharmacyFromRefReq";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new OracleParameter("ParammessageID", OracleType.VarChar)).Value = messageid;
                command.Parameters.Add(new OracleParameter("CurPharmacyInfo", OracleType.Cursor)).Direction = ParameterDirection.Output;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    this.mPharmacyID = new ID();
                    this.mPharmacyID.NCPDPID = (string)reader.GetOracleString(0);
                    this.mStoreName = (string)reader.GetOracleString(1);
                    this.mPharmacyAddress = new Address();
                    this.mPharmacyAddress.Address1 = reader.GetOracleString(2).IsNull ? null : ((string)reader.GetOracleString(2));
                    this.mPharmacyAddress.City = reader.GetOracleString(3).IsNull ? null : ((string)reader.GetOracleString(3));
                    this.mPharmacyAddress.State = reader.GetOracleString(4).IsNull ? null : ((string)reader.GetOracleString(4));
                    this.mPharmacyAddress.Zip = reader.GetOracleString(5).IsNull ? null : ((string)reader.GetOracleString(5));
                    //if (!reader.GetOracleString(6).IsNull && reader.GetOracleString(6) != "")
                    //{
                    //    this.mPharmacyEmail = reader.GetOracleString(6).IsNull ? null : ((string)reader.GetOracleString(6));
                    //}
                    if (!reader.GetOracleString(7).IsNull)
                    {
                        this.mPharmacyPhone = new PhysicianPhoneNumbers();
                        this.mPharmacyPhone.mPhone = new Phone[8];
                        this.mPharmacyPhone.mPhone[0] = new Phone();
                        this.mPharmacyPhone.mPhone[0].PhoneNumber = reader.GetOracleString(7).IsNull ? null : ((string)reader.GetOracleString(7));
                        this.mPharmacyPhone.mPhone[0].Qualifier = reader.GetOracleString(8).IsNull ? null : ((string)reader.GetOracleString(8));
                        if (!reader.GetOracleString(9).IsNull)
                        {
                            this.mPharmacyPhone.mPhone[1] = new Phone();
                            this.mPharmacyPhone.mPhone[1].PhoneNumber = reader.GetOracleString(9).IsNull ? null : ((string)reader.GetOracleString(9));
                            this.mPharmacyPhone.mPhone[1].Qualifier = reader.GetOracleString(10).IsNull ? null : ((string)reader.GetOracleString(10));

                            if (!reader.GetOracleString(11).IsNull)
                            {
                                this.mPharmacyPhone.mPhone[2] = new Phone();
                                this.mPharmacyPhone.mPhone[2].PhoneNumber = reader.GetOracleString(11).IsNull ? null : ((string)reader.GetOracleString(11));
                                this.mPharmacyPhone.mPhone[2].Qualifier = reader.GetOracleString(12).IsNull ? null : ((string)reader.GetOracleString(12));

                                if (!reader.GetOracleString(13).IsNull)
                                {
                                    this.mPharmacyPhone.mPhone[3] = new Phone();
                                    this.mPharmacyPhone.mPhone[3].PhoneNumber = reader.GetOracleString(13).IsNull ? null : ((string)reader.GetOracleString(13));
                                    this.mPharmacyPhone.mPhone[3].Qualifier = reader.GetOracleString(14).IsNull ? null : ((string)reader.GetOracleString(14));

                                    if (!reader.GetOracleString(15).IsNull)
                                    {
                                        this.mPharmacyPhone.mPhone[4] = new Phone();
                                        this.mPharmacyPhone.mPhone[4].PhoneNumber = reader.GetOracleString(15).IsNull ? null : ((string)reader.GetOracleString(15));
                                        this.mPharmacyPhone.mPhone[4].Qualifier = reader.GetOracleString(16).IsNull ? null : ((string)reader.GetOracleString(16));

                                        if (!reader.GetOracleString(17).IsNull)
                                        {
                                            this.mPharmacyPhone.mPhone[5] = new Phone();
                                            this.mPharmacyPhone.mPhone[5].PhoneNumber = reader.GetOracleString(17).IsNull ? null : ((string)reader.GetOracleString(17));
                                            this.mPharmacyPhone.mPhone[5].Qualifier = reader.GetOracleString(18).IsNull ? null : ((string)reader.GetOracleString(18));

                                            if (!reader.GetOracleString(19).IsNull)
                                            {
                                                this.mPharmacyPhone.mPhone[6] = new Phone();
                                                this.mPharmacyPhone.mPhone[6].PhoneNumber = reader.GetOracleString(19).IsNull ? null : ((string)reader.GetOracleString(19));
                                                this.mPharmacyPhone.mPhone[6].Qualifier = reader.GetOracleString(20).IsNull ? null : ((string)reader.GetOracleString(20));

                                                if (!reader.GetOracleString(21).IsNull)
                                                {
                                                    this.mPharmacyPhone.mPhone[7] = new Phone();
                                                    this.mPharmacyPhone.mPhone[7].PhoneNumber = reader.GetOracleString(21).IsNull ? null : ((string)reader.GetOracleString(21));
                                                    this.mPharmacyPhone.mPhone[7].Qualifier = reader.GetOracleString(22).IsNull ? null : ((string)reader.GetOracleString(22));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                reader.Close();
                connection.Close();
            }
            catch (Exception e)
            {
                SS_Logger.log(e.Message);
                SS_Logger.log(e.StackTrace);
            }

            SS_Logger.log("finished loading pharmacy");
        }



    }
}

