﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace WEBeDoctorPharmacy
{
    public class RxChangeRequestType
    {
        [XmlElement("Request")]
        public RequestType Request;

        [XmlElement("Pharmacy")]
        public PharmacyInfo mPharmacy;

        [XmlElement("Prescriber")]
        public PrescriberInfo mPrescriber;

        [XmlElement("Patient")]
        public PatientInfo mPatient;

        [XmlElement("MedicationPrescribed")]
        public MedicationInfo mMedicationPrescribed;

        [XmlElement("MedicationRequested")]
        public MedicationInfo[] mMedicationRequested;

        [XmlElement("Observation")]
        public ObservationType mObservation;

        [XmlElement("BenefitsCoordination")]
        public BenefitsCoordinationType mBenefit;
    }
}
