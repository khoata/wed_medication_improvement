namespace WEBeDoctorPharmacy
{
    using System;
    using System.Xml.Serialization;

    public class ResponseType
    {
        [XmlElement("Approved")]
        public Approved ApprovedResp;
        [XmlElement("ApprovedWithChanges")]
        public ApprovedWithChanges ApprovedWithChangesResp;
        [XmlElement("Denied")]
        public Denied DeniedResp;
        [XmlElement("DeniedNewPrescriptionToFollow")]
        public DeniedNewPrescriptionToFollow DeniedNewPrescripResp;

        public ResponseType()
        {
        }

        public ResponseType(string[] inputParams)
        {
            string RespType = inputParams[5];
            if (RespType != null)
            {
                if (!(RespType == "Approved"))
                {
                    if (RespType == "Denied")
                    {
                        this.DeniedResp = new Denied();
                        this.DeniedResp.DenialReasonCode = inputParams[6];
                        if (inputParams[7] != "")
                        {
                            this.DeniedResp.DenialReason = inputParams[7];
                        }
                    }
                    else if (RespType == "ApprovedWithChanges")
                    {
                        this.ApprovedWithChangesResp = new ApprovedWithChanges();
                        if (inputParams[7] != "")
                        {
                            this.ApprovedWithChangesResp.note = inputParams[7];
                        }
                    }
                    else if (RespType == "DeniedNewPrescriptionToFollow")
                    {
                        this.DeniedNewPrescripResp = new DeniedNewPrescriptionToFollow();
                        if ( inputParams[7] != "")
                        {
                            this.DeniedNewPrescripResp.note = inputParams[7];
                        }

                    }
                }
                else
                {
                    this.ApprovedResp = new Approved();
                    if (inputParams[7] != "")
                    {
                        this.ApprovedResp.Note = inputParams[7];
                    }
                }
            }
        }
    }
}

