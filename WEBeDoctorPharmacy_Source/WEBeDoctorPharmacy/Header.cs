namespace WEBeDoctorPharmacy
{
    using System;
    using System.Configuration;
    using System.Security.Cryptography;
    using System.Text;
    using System.Xml.Serialization;

    public class Header
    {
        [XmlElement("To")]
        public QualifiedMailAddressType To;
        [XmlElement("From")]
        public QualifiedMailAddressType From;
        private Guid MessageGuid;
        [XmlElement("MessageID")]
        public string MessageID;
        private WEBeDoctorPharmacy.Security.UserNameToken.Password mPassword;
        [XmlElement("RelatesToMessageID")]
        public string RelatesToMessageID;
        [XmlElement("SentTime")]
        public string SentTime;
        [XmlElement("Security")]
        public WEBeDoctorPharmacy.Security Security;
        [XmlElement("TestMessage")]
        public string TestMessage;
        [XmlElement("RxReferenceNumber")]
        public string RXReferenceNumber;
        [XmlElement("PrescriberOrderNumber")]
        public string PrescriberOrderNumber;

        private WEBeDoctorPharmacy.Security.UserNameToken UserNameToken;

        public Header()
        {
        }

        public Header(string MsgType)
        {
            // RxRouting message: construct from/to fields by Message.ConstructHeaderFields()
            this.MessageGuid = Guid.NewGuid();
            this.MessageID = this.MessageGuid.ToString().Replace("-", "");
            //this.SentTime = DateTime.Now.AddHours(7.0).ToString("yyyy-MM-ddTHH:mm:ss.0Z");
            this.SentTime = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.0Z");
            if ((((MsgType == "DirDwnld") || (MsgType == "AddPrescriber")) || (MsgType == "AddPrescriberResp") || (MsgType == "AddPrescriberLocationResp")) || (MsgType == "UpdatePrescriber"))
            {
                this.To = new QualifiedMailAddressType { Value = "mailto:SSDR44.dp@surescripts.com" };
                this.From = new QualifiedMailAddressType { Value = "mailto:WEBDOC.dp@surescripts.com" };
                this.Security = new WEBeDoctorPharmacy.Security();
                this.UserNameToken = new WEBeDoctorPharmacy.Security.UserNameToken();
                this.UserNameToken.mUsername = ConfigurationManager.AppSettings["DSLogin"].ToString();
                this.mPassword = new WEBeDoctorPharmacy.Security.UserNameToken.Password();
                SHA1CryptoServiceProvider cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
                this.mPassword.mmPassword = Convert.ToBase64String(cryptoTransformSHA1.ComputeHash(Encoding.Unicode.GetBytes(ConfigurationManager.AppSettings["DSPassword"].ToString().ToUpper().ToString())));
                this.UserNameToken.mPassword = this.mPassword;
                this.UserNameToken.mNonce = "1";
                this.UserNameToken.mCreated = this.SentTime;
                this.Security.mUsernameToken = this.UserNameToken;
            }
        }
    }
}

