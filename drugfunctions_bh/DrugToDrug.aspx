<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DrugToDrug.aspx.cs" Inherits="_Default"
    EnableViewStateMac="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="tab" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="tab" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Drug To Drug Interactions</title>
    <!--<link rel='stylesheet' href='StyleSheet.css' type='text/css' title='Normal'>-->
    <style type="text/css">
        .ajax__tab_xp .ajax__tab_tab {
            height: 25px !important;
        }
    </style>
    <style type="text/css">
        .style3 {
            width: 90px;
        }

        body {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #333333;
        }

        .tableClass {
            border-collapse: collapse;
            margin: 0px;
            padding: 0px;
        }

            .tableClass td {
                /*padding: 4px 5px;*/
                text-align: left;
                border: 1px solid #CCCCCC;
                width: auto;
            }

            .tableClass th {
                padding: 2px 5px;
                text-align: left;
                background-color: #CCCCCC;
                border: 1px solid #CCCCCC;
            }

            .tableClass td:first-child {
                padding-left: 1px;
                width: 220px;
            }

        .autowidth {
            width: 100% !important;
            overflow-y: scroll;
            padding-right: 34px;
        }

        .tablescroll, .tablescroll:first-child {
            table-layout: fixed!important;
            width: 100% !important;
            border-bottom: 0px;
        }

            .tablescroll td {
                padding: 4px 5px;
                text-align: left;
                border: 1px solid #CCCCCC;
                width: auto;
            }

            .tablescroll th {
                padding: 2px 5px;
                text-align: left;
                border: 1px solid #CCCCCC;
            }


        .tablescrollAdd, .tablescrollAdd:first-child {
            table-layout: fixed!important;
            width: 100% !important;
            border-bottom: 0px;
        }

            .tablescrollAdd td {
                padding: 4px 5px;
                text-align: left;
                border: 1px solid #CCCCCC;
            }

            .tablescrollAdd th {
                padding: 2px 5px;
                text-align: left;
                border: 1px solid #CCCCCC;
            }
    </style>
    <link rel='stylesheet' href='../library/global.css' type='text/css' title='Normal' />
    <link rel='alternate stylesheet' href='../library/global_bold.css' type='text/css'
        media='screen' charset='ISO-8859-1' title='Bold' />
    <script type="text/javascript">
        function collapseExpand(obj) {
            var gvObject = document.getElementById(obj);
            var imageID = document.getElementById('image' + obj);

            if (gvObject.style.display == "none") {
                gvObject.style.display = "inline";
                imageID.src = "../images/practice/minus.png";
            }
            else {
                gvObject.style.display = "none";
                imageID.src = "../images/practice/plus.png";
            }
        }


        function popScrollWindowMax(action, winwidth, winheight) {

            var patientid = document.getElementById("patientid").value;

            var pw = null;
            pw = window.open("", "", "toolbar=no,width=" + winwidth + ",height=" + winheight + ",directories=no,status=no,scrollbars=yes,resizable=yes,menubar=no");
            if (pw != null) {
                if (pw.opener == null) {
                    pw.opener = self;
                }
                pw.location.href = action + "?patientid=" + patientid;
            }
        }

        function popScrollWindow(action, winwidth, winheight) {


            var pw = null;
            pw = window.open("", "", "toolbar=no,width=" + winwidth + ",height=" + winheight + ",directories=no,status=no,scrollbars=yes,resizable=yes,menubar=no");
            if (pw != null) {
                if (pw.opener == null) {
                    pw.opener = self;
                }
                pw.location.href = action;
            }
        }
    </script>

</head>
<body>

    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Label"><h3>Drug Validations</h3></asp:Label>
        </div>
        <div>
            <asp:Label ID="Label2" runat="server" Text="Label" Visible="false"><h6></h6></asp:Label>
        </div>
        <div>
            <%--        <asp:ScriptManager runat="server">
        </asp:ScriptManager>--%>
            <tab:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </tab:ToolkitScriptManager>
            <tab:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                <tab:TabPanel ID="DrugToDrugTab" HeaderText="Drug To Drug Interactions" runat="server">
                    <ContentTemplate>
                        <tab:UpdatePanel ID="drugtodrugPanel" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="DrugToDrugList" runat="server" AlternatingRowStyle-BackColor="#dddddd" AutoGenerateColumns="false" CellPadding="4" Font-Name="Verdana" Font-Size="10pt" HeaderStyle-BackColor="#444444" HeaderStyle-ForeColor="White">
                                    <%-- <HeaderStyle BackColor="AliceBlue" />--%>
                                    <Columns>
                                        <asp:BoundField DataField="GenDrugID" HeaderText="Generic Drug ID" InsertVisible="False" ReadOnly="True" SortExpression="GenDrugID" />
                                        <asp:BoundField DataField="GenericName" HeaderText="Generic Drug Name" InsertVisible="False" ReadOnly="True" SortExpression="GenDrugID" />
                                        <asp:BoundField DataField="HalfLifeHours" HeaderText="Half Life Hours" InsertVisible="False" ReadOnly="True" SortExpression="GenDrugID" />
                                    </Columns>
                                </asp:GridView>
                                <asp:GridView ID="DrugToDrugInteractionList" runat="server" AlternatingRowStyle-BackColor="#dddddd" AutoGenerateColumns="false" CellPadding="4" Font-Name="Verdana" Font-Size="10pt" HeaderStyle-BackColor="#444444" HeaderStyle-ForeColor="White">
                                    <%--<HeaderStyle BackColor="AliceBlue" />--%>
                                    <Columns>
                                        <asp:BoundField DataField="Drug1" HeaderText="Drug 1" InsertVisible="False" ReadOnly="True" />
                                        <asp:BoundField DataField="Drug2" HeaderText="Drug 2" InsertVisible="False" ReadOnly="True" />
                                        <asp:BoundField DataField="InteractionDescription" HeaderText="Interaction Description" InsertVisible="False" ReadOnly="True" />
                                        <asp:BoundField DataField="SeverityDescription" HeaderText="Severity" InsertVisible="False" ReadOnly="True" />
                                        <%--            <asp:BoundField ReadOnly="True" 
                      HeaderText="Last Revised" InsertVisible="False" 
                      DataField="LocalLastRevised"></asp:BoundField>   --%>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="InteractionStatus" runat="server" Text=""></asp:Label>
                                <p>
                                <asp:Label ID="lblInterventionDeveloperInformation_DD" runat="server" Text = ""></asp:Label>
                                </p>
                            </ContentTemplate>
                        </tab:UpdatePanel>
                    </ContentTemplate>
                </tab:TabPanel>
                <tab:TabPanel ID="DrugAllergyTab" HeaderText="Drug Allergy" runat="server">
                    <ContentTemplate>
                        <asp:Label runat="server" ID="DrugAllergyLbl" Visible="false"></asp:Label>
                        <asp:GridView ID="AllergyListView" runat="server" AutoGenerateColumns="false" Font-Name="Verdana"
                            Font-Size="10pt" CellPadding="4" HeaderStyle-BackColor="#444444" HeaderStyle-ForeColor="White"
                            AlternatingRowStyle-BackColor="#dddddd">
                            <%-- <HeaderStyle BackColor="AliceBlue" />--%>
                            <Columns>
                                <asp:BoundField ReadOnly="True" HeaderText="Allergy Class" InsertVisible="False"
                                    DataField="DescriptionPlural" SortExpression="DescriptionPlural"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" HeaderText="Reaction" InsertVisible="False" DataField="Reaction"
                                    SortExpression="Reaction"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" HeaderText="Severity" InsertVisible="False" DataField="AllergySeverity"
                                    SortExpression="GenDrugID"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:GridView ID="DrugToAllergyList" runat="server" AutoGenerateColumns="false" Font-Name="Verdana"
                            Font-Size="10pt" CellPadding="4" HeaderStyle-BackColor="#444444" HeaderStyle-ForeColor="White"
                            AlternatingRowStyle-BackColor="#dddddd">
                            <Columns>
                                <asp:BoundField ReadOnly="True" HeaderText="Drug" InsertVisible="False" DataField="Drug"
                                    SortExpression="Drug"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" HeaderText="Allergy Class" InsertVisible="False"
                                    DataField="ClassDescription" SortExpression="ClassDescription"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" HeaderText="Match Type" InsertVisible="False" DataField="MatchTypeDescription"
                                    SortExpression="MatchTypeDescription"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" HeaderText="Message" InsertVisible="False" DataField="AllergyMessage"
                                    SortExpression="AllergyMessage"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" HeaderText="Severity" InsertVisible="False" DataField="AllergySeverity"
                                    SortExpression="AllergySeverity"></asp:BoundField>
                                <asp:BoundField ReadOnly="True" HeaderText="Reaction" InsertVisible="False" DataField="Reaction"
                                    SortExpression="Reaction"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <p>
                        <asp:Label ID="lblInterventionDeveloperInformation_DA" runat="server" Text = ""></asp:Label>
                        </p>
                    </ContentTemplate>
                </tab:TabPanel>
                <tab:TabPanel ID="tabPrescriptionBenefits" HeaderText="Prescription Benefits" runat="server">
                    <ContentTemplate>
                        <table style="width: 98%;" class="tableClass">
                            <tr runat="server" id="trPayer" visible="false">
                                <td style="font-weight: bold;">Payer Name : -
                                </td>
                                <td>
                                    <asp:Label ID="lblPayername" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblPayerId" style="display: none;" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblBINLocationNumber" style="display: none;" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblProcessorControlNumber" style="display: none;" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblGroupId" style="display: none;" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblGroupName" style="display: none;" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblCardHolderId" style="display: none;" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblCardHolderFirstName" style="display: none;" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblCardHolderLastName" style="display: none;" Text="" runat="server"></asp:Label>
									<asp:Label ID="lblMutuallyDefined" style="display: none;" Text="" runat="server"></asp:Label>
                                </td>
                                <td rowspan="10" id="trCopayDetail" runat="server" visible="false" style="vertical-align: top">
                                    <table width="100%" class="tableClass">
                                        <tr>
                                            <td class="titleBox">Copay Information
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label runat="server" ID="lblCopayType"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trCopaygrd" runat="server" visible="false">
                                            <td colspan="2">
                                                <asp:GridView ID="grdCopay" runat="server" ShowHeaderWhenEmpty="true" AllowPaging="false" Font-Size="Small"
                                                    BorderColor="Black" RowStyle-BorderColor="Black" Width="100%" OnRowDataBound="grdCopay_RowDataBound">
                                                </asp:GridView>
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr runat="server" id="trHealthplan" visible="true">
                                <td style="font-weight: bold;">Health Plan Name : -
                                </td>
                                <td>
                                    <asp:DropDownList ID="drCoverageList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drCoverageList_SelectedIndexChanged"
                                        Visible="false">
                                    </asp:DropDownList>
                                   <%--  <asp:Label ID="lblwait" runat="server" Font-Bold="True" Font-Size="Medium" 
            ForeColor="#FF3300" Text="Please wait..." Visible="false"  ></asp:Label>--%>
                                    <asp:Label ID="lblHealthPlanName" Text="" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr runat="server" id="trDemographic" visible="false">
                                <td style="font-weight: bold;" colspan="2">
                                    <asp:Label ID="lblDemographic" Text="" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="titleBox" colspan="2">Pharmacy coverage types with eligibility status
                                </td>
                            </tr>
                            <tr runat="server" id="trMail" visible="false">
                                <td style="font-weight: bold;">Coverage for Mail
                                </td>
                                <td>
                                    <asp:Label ID="lblCoverageMail" Text="" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr runat="server" id="trretail" visible="false">
                                <td style="font-weight: bold;">Coverage for Retail
                                </td>
                                <td>
                                    <asp:Label ID="lblCoverageRetail" Text="" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="titleBox">Demographic Information
                                <asp:LinkButton runat="server" ID="lnkPatientDemographicInfo" NavigateUrl="#" OnClientClick="popScrollWindowMax('/DrugFunctions_bh/PatientDemographicInfo.aspx','500','300'); return false" Text="Click"></asp:LinkButton>
                                </td>
                                <td class="titleBox">Eligibilty Information
                                <asp:LinkButton runat="server" ID="lnkPatientEligibilityInfo" NavigateUrl="#" OnClientClick="popScrollWindowMax('/DrugFunctions_bh/PatientEligibilityInfo.aspx','800','500'); return false" Text="Click"></asp:LinkButton>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <b>Brand/Generic : </b>
                                    <asp:Label ID="lblBrandGeneric" runat="server"></asp:Label>
                                </td>
                                <td><b>Rx/OTC :  </b>
                                    <asp:Label ID="lblRXOTC" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="titleBox" colspan="2">Formulary Status
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblProductCoverageText" runat="server" Visible="false">Product Coverage Exclusion:</asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblProductCoverage" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFormularyStatusText" runat="server">Formulary Status:</asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblFormularyStatus" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="titleBox" colspan="4" runat="server" id="trCoverage" visible="false">Coverage Information
                                </td>
                            </tr>
                            <tr id="genderText" runat="server" visible="false">
                                <td>Gender Limit:
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblgenderLimit" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr id="quantityText" runat="server" visible="false">
                                <td>Quantity Limit:
                                </td>
                                <td colspan="3">
                                    <asp:GridView ID="grdQuantityLimit" runat="server" ShowHeader="false" GridLines="None" AutoGenerateColumns="true"
                                        BorderWidth="0" RowStyle-HorizontalAlign="Right" Width="100%">
                                       <%-- <Columns>
                                            <asp:TemplateField HeaderText="Sr. No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" Width="10px" />
                                                <ItemStyle HorizontalAlign="Left" Width="10px" />
                                            </asp:TemplateField>
                                        </Columns>--%>
                                    </asp:GridView>
                                    <%-- <asp:Label ID="lblQuantityLimit" runat="server"></asp:Label>--%>
                                </td>
                            </tr>
                            <tr id="age" runat="server" visible="false">
                                <td>Age Limit:
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblAge" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr id="Summaryurl" runat="server" visible="false">
                                <td>Summary URL :
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblSummaryUrl" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr id="drugSpecificUrl" runat="server" visible="false">
                                <td>Drug Specific URL :
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblDrugSpecificUrl" runat="server"></asp:Label>
                                    <asp:GridView ID="grddrugspecific" runat="server" ShowHeaderWhenEmpty="true" AllowPaging="false"
                                        ShowHeader="false" BorderColor="Black" RowStyle-BorderColor="Black" RowStyle-HorizontalAlign="Right" Width="100%">
                                       <%-- <Columns>
                                            <asp:TemplateField HeaderText="Sr. No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" Width="10px" />
                                                <ItemStyle HorizontalAlign="Left" Width="10px" />
                                            </asp:TemplateField>
                                        </Columns>--%>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="message" runat="server" visible="false" align="left" >
                                <td>Text Message:
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                    <asp:GridView ID="grdmessage" runat="server" ShowHeaderWhenEmpty="true" AllowPaging="false"
                                        ShowHeader="false" BorderColor="Black" RowStyle-BorderColor="Black" RowStyle-HorizontalAlign="Right" Width="100%" >
                                        <%--<Columns>
                                            <asp:TemplateField HeaderText="Sr. No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" Width="10px" />
                                                <ItemStyle HorizontalAlign="Left" Width="10px" />
                                            </asp:TemplateField>
                                        </Columns>--%>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="stepMedication" runat="server" visible="false">
                                <td>Step Medications:
                                </td>
                                <td colspan="3">
                                    <asp:GridView ID="grdViewStepMedication" runat="server" CellPadding="4" OnRowDataBound="grdViewStepMedication_RowDataBound"
                                        ForeColor="#333333" GridLines="None" RowStyle-HorizontalAlign="Right" Width="100%">
                                        <AlternatingRowStyle BackColor="White" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#EFF3FB" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />

                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="productCoverageExc" runat="server" visible="false">
                                <td>Product Coverage Exclusion :
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblProductCoverageExc" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr id="medicalnecessity" runat="server" visible="false">
                                <td>Medical Necessity :
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblMedicalNecessity" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr id="StepTh" runat="server" visible="false">
                                <td>Step Therapy :
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblStepTh" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr id="PriorAuth" runat="server" visible="false">
                                <td>Prior Authorization :
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblPriorAuth" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr id="trAlternativegrd" runat="server" visible="false">
                                <td class="titleBox" colspan="4">Payer Specified Alternatives<span style="color: Red;">*Higher The Preference Level Higher
                                    Is The Drug Preference</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <asp:GridView ID="grdAlternatives" runat="server" BorderColor="Black" RowStyle-BorderColor="Black"
                                        HeaderStyle-Font-Size="X-Small" AutoGenerateColumns="false" OnRowDataBound="grdAlternatives_RowDataBound">
                                        <Columns>
                                            <%-- <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="javascript:collapseExpand('PRODUCT_ID_ALT-<%# Eval("PRODUCT_ID_ALT") %>');">
                                                    <img id='imagePRODUCT_ID_ALT-<%# Eval("PRODUCT_ID_ALT") %>' alt="Click to show/hide Copay details"
                                                        border="0" src="../images/practice/plus.png" title="Click to show/hide Copay details" /></a>
                                            </ItemTemplate>
                                        </asp:TemplateField> --%>
                                            <%-- <asp:BoundField DataField="PRODUCT_ID_ALT" HeaderText="Alternate Drug ID" />--%>
                                            <asp:TemplateField HeaderText="Sr. No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" Width="10px" />
                                                <ItemStyle HorizontalAlign="Left" Width="10px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="BRAND_DESCRIPTION" HeaderText="Drug Name" />
                                            <asp:BoundField DataField="PREFERENCE_LEVEL" HeaderText="Formulary Status" />
                                            <asp:BoundField DataField="BRAND_GENERIC_CODE" HeaderText="Brand/Generic" />
                                            <asp:BoundField DataField="RX_OTC_STATUS_CODE" HeaderText="RX/OTC" />
                                              <asp:TemplateField HeaderText="Copay">
                                                <ItemTemplate>                                                  
                                                    <div id='PRODUCT_ID_ALT-<%# Eval("PRODUCT_ID_ALT") %>'>
                                                        <asp:GridView runat="server" ID="grdAlternativesDrugs" Font-Size="Small">
                                                        </asp:GridView>
                                                    </div>                                                 
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="All Strength and Forms for Drug ">
                                                <ItemTemplate>
                                                    <div id='Drug_Id-<%# Eval("DRUG_ID") %>'>
                                                        <asp:GridView runat="server" ID="grdStrengthForms" HeaderStyle-Font-Size="X-Small"  ShowHeader="true" Font-Size="Small" AutoGenerateColumns="false" OnRowDataBound="grdStrengthForms_RowDataBound">
                                                            <Columns>
                                                                 <asp:BoundField DataField="BRAND_DESCRIPTION" HeaderText="Drug Name" ItemStyle-Width="70px"/>
                                                                <%--<asp:TemplateField HeaderText="Drug Name" >
                                                                    <ItemTemplate>
                                                                      
                                                                       href="#" onclick="popScrollWindow('/DrugFunctions_bh/AllStrengthAndForms.aspx?drug_Desc=<%# Eval("BRAND_DESCRIPTION") %>&drug_ID=<%# Eval("GENPRODUCT_ID") %>&trade_Name=<%# Eval("TRADE_NAME") %>','500','300')"><%# Eval("BRAND_DESCRIPTION") %></a>
                                                                             
                                                                             
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                  <asp:TemplateField HeaderText="Formulary Status" >
                                                                    <ItemTemplate>
                                                                         <asp:Label runat="server" ID="lblFormStatus"></asp:Label>
                                                                     </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Copay" >
                                                                    <ItemTemplate>
                                                                         <asp:GridView runat="server" ID="grdStrengthFormsCopay" Font-Size="Small">
                                                                                    </asp:GridView>
                                                                     </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:BoundField DataField="GENPRODUCT_ID" HeaderText="GENPRODUCT_ID"/>
                                                                  <asp:BoundField DataField="TRADE_NAME" HeaderText="TRADE_NAME" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                          
                                            <asp:BoundField DataField="TRADE_NAME" HeaderText="TRADE_NAME" />

                                        </Columns>
                                        <RowStyle VerticalAlign="Top" />
                                    </asp:GridView>
                                </td>
                            </tr>

                            <tr>
                                <td class="titleBox" colspan="4" id="trTherapgrd" runat="server" visible="false">Therapeutic Alternatives
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:GridView ID="grdTherapeuticAlternatives" runat="server" ShowHeader="true" GridLines="None" AutoGenerateColumns="false"
                                        OnRowDataBound="grdTherapeuticAlternatives_RowDataBound" BorderWidth="0" BorderColor="Black" RowStyle-BorderColor="Black"
                                        HeaderStyle-Font-Size="X-Small" Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr. No.">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" Width="10px" />
                                                <ItemStyle HorizontalAlign="Left" Width="10px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Drug_Name" HeaderText="Drug Name" />
                                            <asp:BoundField DataField="Preference_Level" HeaderText="Formulary Status" />

                                            <asp:TemplateField HeaderText="Copay">
                                                <ItemTemplate>
                                                    <div id='PRODUCT_ID_ALT-<%# Eval("PRODUCT_ID_ALT") %>'>
                                                        <asp:GridView runat="server" ID="grdTherapeuticAlternativesDrugs" Font-Size="Small">
                                                        </asp:GridView>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lblText" runat="server" Text="" Visible="true">
                        </asp:Label>
                    </ContentTemplate>
                </tab:TabPanel>
                <tab:TabPanel ID="tabMedicationHistory" HeaderText="Medication History" runat="server"
                    ScrollBars="Auto" Visible="true">
                    <ContentTemplate>
                        <asp:DropDownList ID="drCoverageListforMedicationHistory" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="drCoverageListforMedicationHistory_SelectedIndexChanged"
                            Visible="false">
                        </asp:DropDownList>
                        <div id="divMedicationhistoryDetail" runat="server" >
                            <asp:GridView   ID="grdMedicationDispensed"
                                runat="server" Font-Size="Small" 
                                AutoGenerateColumns="false" Width="100%"
                                HeaderStyle-BackColor="#3366cc" HeaderStyle-ForeColor="White"
                                AlternatingRowStyle-BackColor="#eeeeee"  >

                                <Columns>
                                    <asp:TemplateField HeaderText="Sr. No."  >
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>                                        
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DrugDescription" HeaderText="Drug Description" >
                                     <ItemStyle HorizontalAlign="Left" Width="100px" />
                                     </asp:BoundField>
                                    <asp:BoundField DataField="DrugProductCode" HeaderText="Drug Product Code" />
                                    <%--<asp:BoundField DataField="DrugProductCodeQualifier" HeaderText="Drug Product Code Qualifier" />
                            <asp:BoundField DataField="UnitSourceCode" HeaderText="Unit Source Code" />--%>
                                    <asp:BoundField DataField="QuantityValue" HeaderText="Quantity Value" />
                                    <%-- <asp:BoundField DataField="CodelistQualifier" HeaderText="Quantity Code List Qualifier" />
                            <asp:BoundField DataField="PotencyUnitCode" HeaderText="Potency Unit Code" />--%>
                                    <asp:BoundField DataField="DaysSupply" HeaderText="Days Supply" />
                                    
                                       <asp:BoundField DataField="LastFillDate" HeaderText="Last Fill Date"  >                                
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="PrimaryValue" HeaderText="Primary Value (ICD9/10 Code)" />
                                    <%--<asp:BoundField DataField="PrimaryQualifier" HeaderText="Primary Qualifier" />
                                    
                                    <asp:BoundField DataField="RefillsQualifier" HeaderText="Refill Qualifier" />--%>

                                    <asp:BoundField DataField="RefillsValue" HeaderText="Refill Value" />
                                    <asp:BoundField DataField="sigfreetext" HeaderText="Sig Free Text" ItemStyle-HorizontalAlign="Left"   />

                                                              <asp:BoundField DataField="PriorAuthorizationStatus" HeaderText="Prior Authorization Status" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FreeText" HeaderText="Free Text" ItemStyle-HorizontalAlign="Left"   />
                           <%--           <asp:TemplateField HeaderText="Sig Free Text">
            <ItemTemplate>
                <asp:TextBox ID="txtsigfreetext"  Text='<%# Eval("sigfreetext")%>' runat="server" Visible="true" ></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>--%>
                                     <%--<asp:BoundField DataField="deano" HeaderText="Prescription No." Visible="true" />--%>
                                    <%--<asp:BoundField DataField="WrittenDate" HeaderText="Written Date" />--%>
                                    
                                    <%--<asp:BoundField DataField="PharmacyIdentification" HeaderText="Pharmacy Identification" />--%>
                                    <asp:BoundField DataField="PharmacyStoreName" HeaderText="Pharmacy Store Name" >
                                     <ItemStyle HorizontalAlign="Left" Width="100px" />
                                     </asp:BoundField>

                                    <%--<asp:BoundField DataField="PharmacyAddressLine1" HeaderText="Pharmacy Address Line1" />
                                    <asp:BoundField DataField="PharmacyCity" HeaderText="Pharmacy City" />
                                    <asp:BoundField DataField="PharmacyState" HeaderText="Pharmacy State" />
                                    <asp:BoundField DataField="PharmacyZipCode" HeaderText="Pharmacy Zip Code" />
                                    <asp:BoundField DataField="PharmacyPhoneNo" HeaderText="Pharmacy PhoneNo" />
                                    <asp:BoundField DataField="PharmacyPhoneNoQualifier" HeaderText="Pharmacy PhoneNo Qualifier" />
                                    <asp:BoundField DataField="PrescriberIdentification" HeaderText="Prescriber Identification" />--%>
                                    <asp:BoundField DataField="PrescriberFirstName" HeaderText="Prescriber First Name" >
                                     <ItemStyle HorizontalAlign="Left" Width="100px" />
                                     </asp:BoundField>

                                    <asp:BoundField DataField="PrescriberLastName" HeaderText="Prescriber Last Name" >
                                     <ItemStyle HorizontalAlign="Left" Width="100px" />
                                     </asp:BoundField>                                    
                                    <%--<asp:BoundField DataField="PrescriberMidleName" HeaderText="Prescriber MidleName" />
                                    <asp:BoundField DataField="PrescriberAddressline1" HeaderText="Prescriber Address Line1" />
                                    <asp:BoundField DataField="PrescriberCity" HeaderText="Prescriber City" />
                                    <asp:BoundField DataField="PrescriberState" HeaderText="Prescriber State" />
                                    <asp:BoundField DataField="PrescriberZipCode" HeaderText="Prescriber Zip Code" />--%>
                                </Columns>
                                <RowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:GridView>


                        </div>
                        <br />
                        <div visible="false" runat="server" id="divAdditionalHistory">
                            <asp:Label ID="Label33" runat="server" Text="Label"><h4>Additional Drug History</h4></asp:Label>
                        <asp:GridView ID="grdAdditionalMedicationDispensed"  runat="server"
                            HeaderStyle-BackColor="#3366cc" HeaderStyle-ForeColor="White"
                            AlternatingRowStyle-BackColor="#eeeeee" Font-Size="Small" AutoGenerateColumns="false" Width="100%" >
                            <Columns>
                                <asp:TemplateField HeaderText="Sr. No.">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                 <asp:BoundField DataField="DrugDescription" HeaderText="Drug Description" >
                                     <ItemStyle HorizontalAlign="Left" Width="100px" />
                                     </asp:BoundField>
                                <asp:BoundField DataField="DrugProductCode" HeaderText="Drug Product Code" />
                                <%--    <asp:BoundField DataField="DrugProductCodeQualifier" HeaderText="Drug Product Code Qualifier" />
                            <asp:BoundField DataField="UnitSourceCode" HeaderText="Unit Source Code" />--%>
                                <asp:BoundField DataField="QuantityValue" HeaderText="Quantity Value" />
                                <%--<asp:BoundField DataField="CodelistQualifier" HeaderText="Quantity Code List Qualifier" />
                            <asp:BoundField DataField="PotencyUnitCode" HeaderText="Potency Unit Code" />--%>
                                <asp:BoundField DataField="DaysSupply" HeaderText="Days Supply" />
                                <%--<asp:BoundField DataField="LastFillDate" HeaderText="Last Fill Date">
                                <ItemStyle HorizontalAlign="Left" Width="350px" />
                                </asp:BoundField>--%>

                                  <asp:BoundField DataField="LastFillDate" HeaderText="Last Fill Date"  >                                
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="PrimaryValue" HeaderText="Primary Value (ICD9/10 Code)" />

<%--                                <asp:BoundField DataField="PrimaryQualifier" HeaderText="Primary Qualifier" />
                                
                                <asp:BoundField DataField="RefillsQualifier" HeaderText="Refill Qualifier" />
--%>                                <asp:BoundField DataField="RefillsValue" HeaderText="Refill Value" />
                                <asp:BoundField DataField="sigfreetext" HeaderText="Sig Free Text" />

                                <asp:BoundField DataField="PriorAuthorizationStatus" HeaderText="Prior Authorization Status" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FreeText" HeaderText="Free Text" ItemStyle-HorizontalAlign="Left"   />


                                <%--<asp:BoundField DataField="deano" HeaderText="Prescription No." Visible="true" />--%>
                                <%--<asp:BoundField DataField="WrittenDate" HeaderText="Written Date" />--%>
                                <%--<asp:BoundField DataField="PharmacyIdentification" HeaderText="Pharmacy Identification" />--%>
                            <asp:BoundField DataField="PharmacyStoreName" HeaderText="Pharmacy Store Name" >
                                     <ItemStyle HorizontalAlign="Left" Width="100px" />
                                     </asp:BoundField>
                               <%-- <asp:BoundField DataField="PharmacyAddressLine1" HeaderText="Pharmacy Address Line1" />
                                <asp:BoundField DataField="PharmacyCity" HeaderText="Pharmacy City" />
                                <asp:BoundField DataField="PharmacyState" HeaderText="Pharmacy State" />
                                <asp:BoundField DataField="PharmacyZipCode" HeaderText="Pharmacy Zip Code" />
                                <asp:BoundField DataField="PharmacyPhoneNo" HeaderText="Pharmacy PhoneNo" />
                                <asp:BoundField DataField="PharmacyPhoneNoQualifier" HeaderText="Pharmacy PhoneNo Qualifier" />
                                <asp:BoundField DataField="PrescriberIdentification" HeaderText="Prescriber Identification" />--%>
                                    <asp:BoundField DataField="PrescriberFirstName" HeaderText="Prescriber First Name" >
                                         <ItemStyle HorizontalAlign="Left" Width="100px" />
                                     </asp:BoundField>
                                     <asp:BoundField DataField="PrescriberLastName" HeaderText="Prescriber Last Name" >
                                     <ItemStyle HorizontalAlign="Left" Width="100px" />
                                     </asp:BoundField>

<%--                                <asp:BoundField DataField="PrescriberMidleName" HeaderText="Prescriber MidleName" />
                                <asp:BoundField DataField="PrescriberAddressline1" HeaderText="Prescriber Address Line1" />
                                <asp:BoundField DataField="PrescriberCity" HeaderText="Prescriber City" />
                                <asp:BoundField DataField="PrescriberState" HeaderText="Prescriber State" />
                                <asp:BoundField DataField="PrescriberZipCode" HeaderText="Prescriber Zip Code" />--%>
                            </Columns>
                            <RowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>
                        </div>
                        <div id="divMedicationResult" runat="server" visible="false">
                            <asp:Label ID="lblHistoryMessage" runat="server" Text=""></asp:Label>
                        </div>
                        <div id="divMedicationError" runat="server">
                            <asp:Label ID="lblmedicationError" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblMedicationdaniel" runat="server" Visible="false"></asp:Label>
                        </div>
                        <br />
                        <asp:Label ID="lblDisclaimer" runat="server"
                            Text="Disclaimer: Certain information may not be available or accurate in this report, including items that the patient asked not be disclosed due to patient privacy concerns, over-the-counter medications, low cost prescriptions, prescriptions paid for by the patient or non-participating sources, or errors in insurance claims information. The provider should independently verify medication history with the patient."></asp:Label>
                    </ContentTemplate>
                </tab:TabPanel>
            </tab:TabContainer>
        </div>
        <%-- <asp:Label ID="InteractionStatus" runat="server" Text=""></asp:Label>--%>
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="patientid" runat="server" />
        <p>
            <%--  <button onclick="javascript:history.back(1);" name="backBtn" value="Back">
            Back</button>
        <asp:Button ID="AddMed" runat="server" Text="Proceed To Add Medication" OnClick="AddMed_Click"
            Visible="false" />--%>
            <a href="#" onclick="javascript:history.go(-1);return false;" style="text-decoration:underline;">Back</a>
        </p>


        <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
        <script src="Scripts/ScrollableGridViewPlugin_ASP.NetAJAXmin.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('.tablescroll').Scrollable({
                    ScrollHeight: 200,
                    IsInUpdatePanel: false
                });

                $('.tablescrollAdd').Scrollable({
                    ScrollHeight: 100,
                    IsInUpdatePanel: false
                });

                $('.tablescroll').parent().addClass('autowidth');
                $('.tablescroll').parent().parent().css('width', '770px');
                $('.tablescrollAdd').parent().addClass('autowidth');
                $('.tablescrollAdd').parent().parent().css('width', '770px');
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#<%= drCoverageList.ClientID %>").change(function () {
                    document.body.style.cursor = 'wait';
                    parent.frames[1].document.forms[0].submit_button.disabled = true;
                });
            });
</script>

    </form>
</body>
</html>
