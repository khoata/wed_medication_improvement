using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LexiData;
using System.Data.Common;
using Oracle.DataAccess.Client;
using System.Collections.Generic;
using RequestResponseInterpretation;
using System.IO;
using MedicationHistoryDetails;
using System.Linq;


public partial class _Default : System.Web.UI.Page
{
    string PostData = "?submit_button=1";
    static string connString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    OracleConnection connection;
    OracleCommand command;
    OracleDataReader odataReader;
    OracleDataAdapter oracleDataAdapter;
    private const string P_SELECTFORMULARYID = "P_selectFormularyID";
    private const string P_Copay_Drug_Specific = "P_Copay_Drug_Specific";
    private const string P_Copay_Summary_Level = "P_Copay_Summary_Level";
    //private const string P_GETALTERNATIVEDRUGS = "P_GetAlternativeDrugs";
    private const string P_GETALTERNATIVEDRUGS = "P_GetAlternativeDrugs_new";
    private const string P_GETGENDERSTATUS = "P_GetGender";
    private const string P_QUANTITY = "P_GetQuantityLimit";
    private const string P_STEPMEDICATION = "P_GetStepMedication";
    private const string P_GETAGELIMIT = " P_GetAgeLimits";
    private const string P_GETMESSAGETEXT = "P_GetMessage";
    private const string P_GETSUMMARYLEVELLINK = "P_SummaryLevelLink";
    private const string P_DRUGSPECIFICLINK = "P_GetDrugSpecificLink";

    bool UpdateRequestValue = false;
    bool InsertRequestValue = false;
    long OutPutValue = -1;
    string RequestString = "";
    string ResponseString = "";
    string RequestId = "";
    string RequestValue = "";
    string ResponseValue = "";
    string VisitedDoctorId = "";
    string VisitedPatientId = "";
    static string SelectedBrandCode = "";
    static string SelectedDrugCode = "";
    static string multum_drug_id = string.Empty;

    public static ResponseParam objparam;//= new ResponseParam();
    public static ResponseData responseData;//= new ResponseData();
    public static string DrugName;

    public static MedicationHistoryRequestParameter HistoryReqParam = new MedicationHistoryRequestParameter();
    public static MedicationHistoryCreateRequest CR = new MedicationHistoryDetails.MedicationHistoryCreateRequest();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
                             
         //delete medd history files that are 3 days old
            
            try
            {
                string[] filePaths = Directory.GetFiles(HttpContext.Current.Server.MapPath("saved_med_history_xml"));
                string[] filePaths_add = Directory.GetFiles(HttpContext.Current.Server.MapPath("saved_additional_med_history_xml"));
                DateTime CurrentDate = Convert.ToDateTime(DateTime.Now);
                //delete additional med history files.

                foreach (string filePath in filePaths)
                {
                    try
                    {                        
                        DateTime CreationTime = File.GetLastWriteTime(filePath);


                        if (CreationTime.AddDays (3) < CurrentDate)
                        {
                            File.Delete(filePath);
                        }
                    }
                    catch (Exception) { }                  
                }

                //delete additional med history files.
                foreach (string filePath in filePaths_add)
                {
                    try
                    {
                        DateTime CreationTime = File.GetLastWriteTime(filePath);


                        if (CreationTime.AddDays(3) < CurrentDate)
                        {
                            File.Delete(filePath);
                        }
                    }
                    catch (Exception) { }
                }
            }
            catch (Exception) { } 




            DbProviderFactory _dbFactory = null;
            GenericDAL myDAL = null;
            ScreeningContext sc = null;
            DbConnection _dbConn = null;
            string Drugs = string.Empty;
            string SearchBy = string.Empty;
            string Allergies = string.Empty;
            string Visit_Key = string.Empty;
            string Severity = string.Empty;
            string Allergy_Sev = string.Empty;
            string Allergy_Reac = string.Empty;
            try
            {
                objparam = new ResponseParam();
                responseData = new ResponseData();

                _dbConn = null;
                string _providerInvariantName;

                _providerInvariantName = "Oracle.DataAccess.Client";
                _dbFactory = DbProviderFactories.GetFactory(_providerInvariantName);
                if (_dbFactory == null)
                    throw new Exception("Unable to create a DbFactory");

                _dbConn = _dbFactory.CreateConnection();
                _dbConn.ConnectionString = connString;

                _dbConn.Open();

                myDAL = new GenericDAL(_dbFactory, _dbConn, _providerInvariantName);
                sc = new ScreeningContext();
            }
            catch (Exception ex)
            {
                Response.Write(UtilityMethods.BuildErrorDetails(ex));
            }

            /*
             * Create a Drug by Generic Drug
             * BaseDrug bd = GenericDrug.Load(DRUG_ID);
             * 
             * Create a Drug by Generic Product
             * BaseDrug bd = GenericProduct.Load(GENPRODUCT_ID);
             * 
             * Create a Drug by Packaged Product
             * BaseDrug bd = PackagedProduct.Load(PKG_PRODUCT_ID);
             */
            //SearchBy = "Name";
            //Allergies = "chlorhexidine";
            ////Response.Write(Request.UrlReferrer);
            string patient_id = string.Empty;
            try
            {
                if ((Convert.ToString(Request.QueryString["Drugs"]) != null))
                    Drugs = Request["Drugs"].ToString();

                if ((Convert.ToString(Request.QueryString["SearchBy"]) != null))
                    SearchBy = Request["SearchBy"].ToString();

                if ((Convert.ToString(Request.QueryString["Allergies"]) != null))
                    Allergies = Request["Allergies"].ToString();

                if ((Convert.ToString(Request.QueryString["Visit_Key"]) != null))
                    Visit_Key = Request["Visit_Key"].ToString();

                if ((Convert.ToString(Request.QueryString["Severity"]) != null))
                    Severity = Request["Severity"].ToString();

                if ((Convert.ToString(Request.QueryString["Allergy_Sev"]) != null))
                    Allergy_Sev = Request["Allergy_Sev"].ToString();

                if ((Convert.ToString(Request.QueryString["Allergy_Reac"]) != null))
                    Allergy_Reac = Request["Allergy_Reac"].ToString();

                if ((Convert.ToString(Request.QueryString["Allergy_Reac"]) != null))
                    patient_id = Request["patient_id"].ToString();

                TabContainer1.ActiveTabIndex = 2;
                #region GetPatient_and_DrInfo

                string PhysicianId, SelectedDrugId;
                //patient_id = Request["patient_id"].ToString();
                patientid.Value = patient_id;
                PhysicianId = Convert.ToString(Request["PhysicianId"]);
                DrugName = Convert.ToString(Request["Drug_Name"]);
                VisitedDoctorId = Convert.ToString(Request["PhysicianId"]);
                VisitedPatientId = Request["patient_id"].ToString();

                string brandcode = Request["brand_code"].ToString();
                multum_drug_id = Convert.ToString(Request["multum_drug_id"]);
                SelectedDrugId = Convert.ToString(Request["main_multum_drug_code"]);
                SelectedBrandCode = brandcode;
                SelectedDrugCode = SelectedDrugId;

                //Response.Write("patient_id" + patient_id);
                //Response.Write("PhysicianId" + PhysicianId);
                //Response.Write("DrugName" + DrugName);
                //Response.Write("Brand Code " + SelectedBrandCode);
                //Response.Write("Main multum drug id " + SelectedDrugId);
                //Response.Write("Brand Code " + SelectedBrandCode);

                //Get patient Details

                #region Variable
                string doctorid = "";
                string patientfirstname = "";
                string patientmidlename = "";
                string patientlastname = "";
                string patientgender = "";
                string patientdateofbirth = "";
                string patientstreetAddress = "";
                string patientcity = "";
                string patientstate = "";
                string patientzipcode = "";
                string patientcountry = "";

                string Drfirstname = "";
                string Drmidlename = "";
                string Drlastname = "";
                string Drdateofbirth = "";
                string DrstreetAddress = "";
                string Drcity = "";
                string Drstate = "";
                string Drzipcode = "";
                string Drcountry = "";

                string DEA_No = "";
                string DrPhone = "";
                string NPI = "";
                string MessageId = "";
                string RxReferenceNumber = "";
                string PatientDBId = "";
                string SenderSoftwareDeveloper = "";
                string SenderSoftwareProducr = "";
                string SenderSoftwareVersionRelease = "";
                string RelationshipId = "";
                string INS_PatientRelation = "";
                string PatientRelationshipId = "";
                string PrescriberAgentFname = "";
                string PrescriberAgentLname = "";
                string PrescriberAgentMname = "";
                string PrescriberClinicName = "";
                string PrescriberSPI = "";
                string PatientPhoneNo = "";
                string patientPrefix = "";
                string PatientSuffix = "";
                string PatientEmail = "";
                string PatientFax = "";
                string DrEmail = "";
                string DrFax = "";
                string patientConsent = string.Empty;
                #endregion Varible


                #region getPatientByPatientId
                OracleCommand command = new OracleCommand();
                OracleConnection conn = new OracleConnection(connString);
                try
                {

                    conn.Open();
                    command.Connection = conn;
                    string sql = "select First_name,last_name,middle_name,birth_date,street,city,state,country,zip,gender,patient_id,Home_phone,prefix,suffix,email,fax,PATIENT_MED_HISTORY_CONSENT from patienttable where userid = :PatientId";
                    command.CommandText = sql;
                    command.CommandType = CommandType.Text;
                    command.Parameters.Add(":PatientId", patient_id);
                    OracleDataReader reader;
                    reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        PatientDBId = Convert.ToString(reader["PATIENT_ID"]);
                        patientfirstname = Convert.ToString(reader["FIRST_NAME"]);
                        patientmidlename = Convert.ToString(reader["MIDDLE_NAME"]);
                        patientlastname = Convert.ToString(reader["LAST_NAME"]);
                        patientgender = Convert.ToString(reader["GENDER"]);

                        if (!string.IsNullOrEmpty(Convert.ToString(reader["BIRTH_DATE"])))
                        {
                            patientdateofbirth = Convert.ToString(reader["BIRTH_DATE"]);
                        }
                        patientstreetAddress = Convert.ToString(reader["STREET"]);
                        patientcity = Convert.ToString(reader["CITY"]);
                        patientstate = Convert.ToString(reader["STATE"]);
                        patientzipcode = Convert.ToString(reader["ZIP"]);
                        patientcountry = Convert.ToString(reader["COUNTRY"]);
                        PatientPhoneNo = Convert.ToString(reader["HOME_PHONE"]);
                        patientPrefix = Convert.ToString(reader["PREFIX"]);
                        PatientSuffix = Convert.ToString(reader["SUFFIX"]);
                        PatientEmail = Convert.ToString(reader["EMAIL"]);
                        PatientFax = Convert.ToString(reader["FAX"]);
                        patientConsent = Convert.ToString(reader["PATIENT_MED_HISTORY_CONSENT"]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Response.Write("getPatientByPatientId:-  " + UtilityMethods.BuildErrorDetails(ex));
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }


                #endregion getPatientByPatientId

                #region getDoctorByPhysicianId

                string strDBConn = connString;
                OracleConnection oraConn = new OracleConnection(strDBConn);
                try
                {
                    oraConn.Open();
                    string queryVisitdata = "select First_name,last_name,middle_name,birth_date,street,city,state,country,zip,phone,npi,dea_no,email,fax,spi from doctortable where docuser ='" + PhysicianId + "'";
                    DataSet dsVisit = OracleHelper.ExecuteDataset(strDBConn, System.Data.CommandType.Text, queryVisitdata);

                    if ((dsVisit != null) && (dsVisit.Tables.Count > 0) && (dsVisit.Tables[0].Rows.Count > 0))
                    {
                        Drfirstname = Convert.ToString(dsVisit.Tables[0].Rows[0]["FIRST_NAME"].ToString());
                        Drmidlename = Convert.ToString(dsVisit.Tables[0].Rows[0]["MIDDLE_NAME"].ToString());
                        Drlastname = Convert.ToString(dsVisit.Tables[0].Rows[0]["LAST_NAME"].ToString());
                        Drdateofbirth = Convert.ToString(dsVisit.Tables[0].Rows[0]["BIRTH_DATE"].ToString());
                        DrstreetAddress = Convert.ToString(dsVisit.Tables[0].Rows[0]["STREET"].ToString());
                        Drcity = Convert.ToString(dsVisit.Tables[0].Rows[0]["CITY"].ToString());
                        Drstate = Convert.ToString(dsVisit.Tables[0].Rows[0]["STATE"].ToString());
                        Drzipcode = Convert.ToString(dsVisit.Tables[0].Rows[0]["ZIP"].ToString());
                        Drcountry = Convert.ToString(dsVisit.Tables[0].Rows[0]["COUNTRY"].ToString());
                        DrPhone = Convert.ToString(dsVisit.Tables[0].Rows[0]["PHONE"].ToString());
                        NPI = Convert.ToString(dsVisit.Tables[0].Rows[0]["NPI"].ToString());
                        DEA_No = Convert.ToString(dsVisit.Tables[0].Rows[0]["DEA_NO"].ToString());
                        DrEmail = Convert.ToString(dsVisit.Tables[0].Rows[0]["EMAIL"].ToString());
                        DrFax = Convert.ToString(dsVisit.Tables[0].Rows[0]["FAX"].ToString());
                        PrescriberSPI = Convert.ToString(dsVisit.Tables[0].Rows[0]["SPI"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("getDoctorByPhysicianId:- " + UtilityMethods.BuildErrorDetails(ex));
                }
                finally
                {
                    oraConn.Close();
                }

                #endregion getDoctorByPhysicianId


                #region AssignValuesToProperty
                RequestResponseInterpretation.requestParam RP = null;
                string birthdate = string.Empty;
                try
                {
                    RP = new RequestResponseInterpretation.requestParam();
                    RP.PatientFirstName = patientfirstname;
                    RP.PatientLastName = patientlastname;
                    RP.PatientMidleName = patientmidlename.Trim();
                    RP.DrFirstName = Drfirstname;
                    RP.DrLastName = Drlastname;
                    RP.DrMidleName = Drmidlename.Trim();
                    RP.DrStreetAddress = DrstreetAddress;
                    RP.DrCitiName = "";//Drcity;
                    RP.DrStateAddress = "";//Drstate;
                    RP.DrPostalZip = "";//Drzipcode;

                    if (!string.IsNullOrEmpty(patientstreetAddress))
                        RP.PatientStreetAddress = patientstreetAddress;
                    //else
                    //   // RP.PatientStreetAddress = "Any Street";
                    //    RP.PatientStreetAddress = "";

                    if (!string.IsNullOrEmpty(patientcity))
                        RP.PatientCitiName = patientcity;
                    //else
                    // //   RP.PatientCitiName = "Any City";
                    //    RP.PatientCitiName = "";

                    if (!string.IsNullOrEmpty(patientstate))
                        RP.PatientStateAddress = patientstate;
                    //else
                    //    //    RP.PatientStateAddress = "AS";
                    //    RP.PatientStateAddress = "";

                    if (!string.IsNullOrEmpty(patientzipcode))
                        RP.PatientPostalZip = patientzipcode;


                    RP.DrDEANo = NPI;

                    //Convert Patient date of birth in YYYYMMDD format and gender in M of F format            
                    if (patientgender == "Male")
                    {
                        patientgender = "M";
                    }
                    else if (patientgender == "Female")
                    {
                        patientgender = "F";
                    }
                    else
                    {
                        patientgender = string.Empty;
                    }

                    RP.patientGender = patientgender;// "M";   

                    birthdate = string.Empty;
                    if (!string.IsNullOrEmpty(patientdateofbirth))
                    {
                        DateTime dt = Convert.ToDateTime(patientdateofbirth);
                        birthdate = dt.ToString("yyyyMMdd");
                    }
                    else
                    {
                        birthdate = "19450201";
                    }
                    RP.patientDOB = birthdate; //"19450201";

                    RP.UserName = ConfigurationManager.AppSettings["UserName"].ToString();
                    RP.Password = ConfigurationManager.AppSettings["Password"].ToString();
                    RP.SureScriptLLC = ConfigurationManager.AppSettings["SurescriptLLC"].ToString();
                    RP.WebeUserName = ConfigurationManager.AppSettings["WebeUserName"].ToString();
                    RP.WebePassword = ConfigurationManager.AppSettings["WebePassword"].ToString();
                    RP.AuthorizationPath = ConfigurationManager.AppSettings["AuthorizationPath"].ToString();
                    RP.AuthinticationType = ConfigurationManager.AppSettings["AuthinticationType"].ToString();
                    RP.AuthonticationMode = ConfigurationManager.AppSettings["AuthonticationMode"].ToString();
                    RP.SureScriptParticipantId = ConfigurationManager.AppSettings["SureScriptParticipantId"].ToString();
                    RP.TransactionVersionNumber = ConfigurationManager.AppSettings["TransactionVersionNumber"].ToString();
                }
                catch (Exception ex)
                {
                    Response.Write(UtilityMethods.BuildErrorDetails(ex));
                }
                #endregion AssignValuesToProperty


                #region 270Requestresponse
                RequestResponseInterpretation.Create270Request clsResponse = new RequestResponseInterpretation.Create270Request();
                RequestResponseInterpretation.Interprete271Response interpreteresponse = new Interprete271Response();

                try
                {
                    //3 Days Validation                
                    DateTime Sysdate = DateTime.Now;
                    string CurrentDate = Sysdate.ToString("yyyy-MM-dd");
                    long ResId = ValidateRequest(VisitedDoctorId, VisitedPatientId, CurrentDate);

                    Interprete271Response objRepsonse = new Interprete271Response();

                    if (OutPutValue == 1)
                    {
                        ResponseValue = ResponseString;
                    }
                    else if (OutPutValue == -1 && InsertRequestValue == true)
                    {
                        RequestValue = clsResponse.Get270Request(RP);
                        ResponseValue = interpreteresponse.Get271Response(RequestValue, RP);

                        long id = InsertRequest(VisitedDoctorId, VisitedPatientId, CurrentDate, RequestValue, ResponseValue);
                    }
                    if (OutPutValue == -1 && UpdateRequestValue == true)
                    {
                        RequestValue = clsResponse.Get270Request(RP);
                        ResponseValue = interpreteresponse.Get271Response(RequestValue, RP);

                        long id = UpdateRequest(RequestId, VisitedDoctorId, VisitedPatientId, CurrentDate, RequestValue, ResponseValue  );
                    }
                    objparam = interpreteresponse.Validate271Response(ResponseValue, RP);
                }
                catch (Exception ex)
                {
                    Response.Write(UtilityMethods.BuildErrorDetails(ex));
                }
                #endregion 270Requestresponse

                BindCoverageDropDown();

                if (InsertRequestValue == true)
                {
                    #region MedicationHistory

                    try
                    {
                        PayerInfo clsPayerInfo = new MedicationHistoryDetails.PayerInfo();
                        MessageHeader clsMessageHeader = new MedicationHistoryDetails.MessageHeader();
                        PatientInfo clsPatientInfo = new MedicationHistoryDetails.PatientInfo();
                        BenifitInfo clsBenifitInfo = new MedicationHistoryDetails.BenifitInfo();
                        PrescriberInfo clsPrescriberInfo = new MedicationHistoryDetails.PrescriberInfo();
                        SureScriptRequest clsSureScriptRequest = new MedicationHistoryDetails.SureScriptRequest();
                        ResponseValue271 clsResponseValue271 = new MedicationHistoryDetails.ResponseValue271();

                        clsMessageHeader.SenderQualifier = Convert.ToString(ConfigurationManager.AppSettings["SenderQualifier"]);
                        //clsMessageHeader.ReceiverQualifier = ConfigurationManager.AppSettings["ReceiverQualifier"];
                        clsMessageHeader.SendDate = String.Format("{0:s}", DateTime.Now);
                        clsMessageHeader.SenderSecondaryIdentification = ConfigurationManager.AppSettings["SenderSecondaryIdentification"];
                        clsMessageHeader.ReceiverSecondaryIdentification = ConfigurationManager.AppSettings["ReceiverSecondaryIdentification"];
                        clsSureScriptRequest.WebeUserName = ConfigurationManager.AppSettings["MedicationWebeUserName"].ToString();
                        clsSureScriptRequest.WebePassword = ConfigurationManager.AppSettings["MedicationWebePassword"].ToString();
                        clsSureScriptRequest.AuthorizationPath = ConfigurationManager.AppSettings["MedicationAuthorizationPath"].ToString();
                        clsSureScriptRequest.AuthinticationType = ConfigurationManager.AppSettings["MedicationAuthinticationType"].ToString();
                        clsSureScriptRequest.AuthonticationMode = ConfigurationManager.AppSettings["MedicationAuthonticationMode"].ToString();

                        if (string.IsNullOrEmpty(PrescriberSPI) != true)
                        {
                            #region getPrescriberAgentandClinicName
                            connection = new OracleConnection(connString);
                            connection.Open();
                            //Get message id and Rxreference Number by prescriber no                    
                            string Messagesql = "select PRESCRIBERAGENTLASTNAME,PRESCRIBERAGENTFIRSTNAME,PRESCRIBERAGENTMIDDLENAME,PRESCRIBERCLINICNAME from ss_message_content where prescriberid = :DoctorId";
                            OracleCommand Messagecommand = new OracleCommand(Messagesql);
                            Messagecommand.Connection = connection;
                            Messagecommand.Parameters.Add(new OracleParameter(":DoctorId", OracleDbType.Varchar2, 20));
                            Messagecommand.Parameters[":DoctorId"].Value = PrescriberSPI;
                            Oracle.DataAccess.Client.OracleDataReader Messagereader = Messagecommand.ExecuteReader();

                            if (Messagereader.HasRows)
                            {
                                PrescriberAgentLname = Convert.ToString(Messagereader["PRESCRIBERAGENTLASTNAME"]);
                                PrescriberAgentFname = Convert.ToString(Messagereader["PRESCRIBERAGENTFIRSTNAME"]);
                                PrescriberAgentMname = Convert.ToString(Messagereader["PRESCRIBERAGENTMIDDLENAME"]);
                                PrescriberClinicName = Convert.ToString(Messagereader["PRESCRIBERCLINICNAME"]);
                            }
                            Messagereader.Close();
                            #endregion getPrescriberAgentandClinicName
                        }

                        #region getPatientRelationShipByPatientId
                        //Get RelationShipId by patient details                
                        string PatientRelationSQL = "select relation_patient,ins_order from patient_insurance where patient_id=:PatientId";
                        OracleCommand PatientRelationcommand = new OracleCommand(PatientRelationSQL);
                        PatientRelationcommand.Connection = connection;
                        PatientRelationcommand.Parameters.Add(new OracleParameter(":PatientId", OracleDbType.Varchar2, 40));
                        PatientRelationcommand.Parameters[":PatientId"].Value = patient_id;

                        if (connection.State.ToString() == "Closed")
                        {
                            connection.Open();
                        }
                        Oracle.DataAccess.Client.OracleDataReader PatientRelationreader = PatientRelationcommand.ExecuteReader();

                        if (PatientRelationreader.HasRows)
                        {
                            RelationshipId = Convert.ToString(PatientRelationreader["RELATION_PATIENT"]);
                            INS_PatientRelation = Convert.ToString(PatientRelationreader["INS_ORDER"]);
                        }
                        PatientRelationreader.Close();
                        if (RelationshipId == "other")
                        {
                            PatientRelationshipId = "1";
                        }
                        else if (RelationshipId == "child")
                        {
                            PatientRelationshipId = "3";
                        }
                        else if (RelationshipId == "spouse")
                        {
                            PatientRelationshipId = "2";
                        }
                        else if (RelationshipId == "self")
                        {
                            PatientRelationshipId = "1";
                        }
                        #endregion getPatientRelationShipByPatientId&PatientId

                        #region HistoryParameter
                        clsMessageHeader.TextMessage = ConfigurationManager.AppSettings["TextMessage"];

                        string mId = Convert.ToString(Guid.NewGuid());
                        clsMessageHeader.MessageId = mId.Replace("-", "");
                        clsMessageHeader.SenderSoftwareDeveloper = ConfigurationManager.AppSettings["SenderSoftwareDeveloper"];
                        clsMessageHeader.SenderSoftwareProduct = ConfigurationManager.AppSettings["SenderSoftwareProduct"];
                        clsMessageHeader.SenderSoftwareRelease = ConfigurationManager.AppSettings["SenderSoftwareVersionRelease"];

                        clsPrescriberInfo.NPI = NPI;
                        clsPrescriberInfo.DEANo = DEA_No;
                        clsPrescriberInfo.PrescriberLastName = Drlastname;
                        clsPrescriberInfo.PrescriberFirstName = Drfirstname;
                        clsPrescriberInfo.PrescriberMidleName = Drmidlename;

                        clsPrescriberInfo.PrescriberPhoneNo = DrPhone;
                        clsPrescriberInfo.PrescriberQualifier = "TE";
                        clsPrescriberInfo.PrescriberEmail = DrEmail;
                        clsPrescriberInfo.PrescriberEmailQualifier = "EM";
                        clsPrescriberInfo.PrescriberFAX = DrFax;
                        clsPrescriberInfo.PrescriberFAXQualifier = "FX";

                        clsPrescriberInfo.PrescriberClinicName = PrescriberClinicName;
                        clsPrescriberInfo.PrescriberAddressline1 = DrstreetAddress;
                        clsPrescriberInfo.PrescriberCity = Drcity;
                        clsPrescriberInfo.PrescriberState = Drstate;
                        clsPrescriberInfo.PrescriberZipCode = Drzipcode;
                        clsPrescriberInfo.PrescriberAgentFirstName = PrescriberAgentFname;
                        clsPrescriberInfo.PrescriberAgentLastName = PrescriberAgentLname;
                        clsPrescriberInfo.PrescriberAgentMidleName = PrescriberAgentMname;

                        if (string.IsNullOrEmpty(PatientRelationshipId) != true)
                        {
                            clsPatientInfo.PatientRelationShip = PatientRelationshipId;
                        }
                        else
                        {
                            clsPatientInfo.PatientRelationShip = "1";
                        }

                        if (patientfirstname.Length > 35)
                        {
                            patientfirstname = patientfirstname.Substring(0, 35);
                        }
                        if (patientlastname.Length > 35)
                        {
                            patientlastname = patientlastname.Substring(0, 35);
                        }

                        clsPatientInfo.PatientFirstName = patientfirstname;
                        clsPatientInfo.PatientMidleName = patientmidlename;
                        clsPatientInfo.PatientLastName = patientlastname;
                        clsPatientInfo.PatientGender = patientgender;
                        clsPatientInfo.PatientDOB = birthdate;

                        if (patientstreetAddress.Length > 35)
                        {
                            patientstreetAddress = patientstreetAddress.Substring(0, 35);
                        }

                        clsPatientInfo.PatientAddressLine1 = patientstreetAddress;
                        clsPatientInfo.PatientCity = patientcity;
                        clsPatientInfo.PatientState = patientstate;
                        clsPatientInfo.PatientZipCode = patientzipcode.Trim();

                        clsPatientInfo.PatientCommunicationNo = PatientPhoneNo;
                        clsPatientInfo.PatientCommunicationQualilfier = "TE";
                        clsPatientInfo.PatientEmail = PatientEmail;
                        clsPatientInfo.PatientEmailQualifier = "EM";
                        clsPatientInfo.PatientFAX = PatientFax;
                        clsPatientInfo.PatientFAXQualifier = "FX";

                        clsPatientInfo.PatientSuffix = PatientSuffix;
                        clsPatientInfo.PatientPrefix = patientPrefix;
                        if (objparam.ResponseData != null)
                        {
                            clsResponseValue271.CardHolderId = objparam.ResponseData[0].CardHolderId;
                        }
                        clsBenifitInfo.CardHolderName = patientlastname + "," + patientfirstname;

                        clsBenifitInfo.Consent = patientConsent;
                        #endregion HistoryParameter
                        HistoryReqParam.BenifitInfo = clsBenifitInfo;
                        HistoryReqParam.MessageHeader = clsMessageHeader;
                        HistoryReqParam.PatientInfo = clsPatientInfo;
                        HistoryReqParam.PrescriberInfo = clsPrescriberInfo;
                        HistoryReqParam.SurescriptRequest = clsSureScriptRequest;

                        BindCoverageDropDownForMedicationHistory(false);
                    }
                    catch (Exception ex)
                    {
                        Response.Write(UtilityMethods.BuildErrorDetails(ex));
                    }

                    #endregion MedicationHistory
                }
                else 
                {
                    DataTable dt = new DataTable();

                    VisitedPatientId = Request["patient_id"].ToString();

                     string sPlanName = string.Empty ; 
                    try
                    {
                       sPlanName = drCoverageListforMedicationHistory.SelectedItem.Text.ToString();
                    }
                    catch (Exception)
                    {
                    }

                    if (sPlanName == string.Empty)
                    {
                        sPlanName = "1"; 
                    }


                    if (File.Exists(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + "_" + sPlanName + ".xml")))
                    {
                        string Saved_MedHistory_Response = File.ReadAllText(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + "_" + sPlanName + ".xml"));

                        string response = string.Empty;
                        if (string.IsNullOrEmpty(Saved_MedHistory_Response) != true)
                        {
                            dt = GetMedicationHistoryResponseData(Saved_MedHistory_Response, "a");
                            BindMedicationHistoryGrid(dt);
                            BindCoverageDropDownForMedicationHistory(true);
                        }                    
                    }
                    else if (File.Exists(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + ".xml")))
                    {
                        string Saved_MedHistory_Response = File.ReadAllText(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId  + ".xml"));

                        string response = string.Empty;
                        if (string.IsNullOrEmpty(Saved_MedHistory_Response) != true)
                        {
                            dt = GetMedicationHistoryResponseData(Saved_MedHistory_Response, "a");
                            BindMedicationHistoryGrid(dt);
                            BindCoverageDropDownForMedicationHistory(true);
                        }
                    }
                    else if (File.Exists(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + "_.xml")))
                    {
                        string Saved_MedHistory_Response = File.ReadAllText(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + "_.xml"));

                        string response = string.Empty;
                        if (string.IsNullOrEmpty(Saved_MedHistory_Response) != true)
                        {
                            dt = GetMedicationHistoryResponseData(Saved_MedHistory_Response, "a");
                            BindMedicationHistoryGrid(dt);
                            BindCoverageDropDownForMedicationHistory(true);
                        }
                    }
                    else
                    {                        
                        divMedicationResult.Visible = true;
                        lblHistoryMessage.Text = "Medication History Request is not executed because patient consent is NO.";
                        lblDisclaimer.Text = string.Empty;
                    }

                }

                #endregion GetPatient_and_DrInfo

                try
                {
                    OracleParameter vk = new OracleParameter(":VisitKey", OracleDbType.Varchar2, 40);
                    OracleParameter mdid = new OracleParameter(":MultumDrugID", OracleDbType.Varchar2, 20);
                    OracleParameter desc = new OracleParameter(":Description", OracleDbType.Varchar2, 400);
                    OracleParameter timecreated = new OracleParameter(":TimeCreated", OracleDbType.Date);
                    OracleParameter validationtype = new OracleParameter(":ValidationType", OracleDbType.Varchar2, 2);

                    //// Remove all prior ineractions.
                    DbCommand cmd = _dbFactory.CreateCommand();                    

                    cmd.CommandText = "Delete from Notes_DrugValidations where Visit_Key=:VisitKey";
                    cmd.CommandType = CommandType.Text;
                    vk.Direction = ParameterDirection.Input;
                    vk.Value = Visit_Key;
                    cmd.Parameters.Add(vk);
                    cmd.Connection = conn;
                    if (cmd.Connection.State.ToString() == "Closed")
                    {
                        cmd.Connection.Open();
                    }
                    cmd.ExecuteNonQuery();
                    if (cmd.Connection.State.ToString() == "Open")
                    {
                        cmd.Connection.Close();
                    }

                    if (Drugs != null && SearchBy != null)
                    {

                        //string[] dids = Drugs.Split('|');
                        //Array.Sort(dids);
                        //Drugs = string.Join("|", dids);
                        //Drugs = Drugs.Remove(0, 1);

                        List<GenericDrug> DrugsToCompare;

                        if (SearchBy == "ID")
                        {
                            DrugsToCompare = Drug_To_Drug_By_ID(Drugs, myDAL);
                        }
                        else
                        {
                            DrugsToCompare = Drug_To_Drug_By_Name(Drugs, myDAL);
                        }

                        foreach (BaseDrug bd in DrugsToCompare)
                        {
                            sc.Drugs.Add(bd);
                        }
                        /* (OPTIONAL) Compound Drug
                         * Add a Generic Drug to a Compound Drug
                         * CompoundDrug cd = new CompoundDrug;
                         * BaseDrug bd = GenericDrug.Load(DRUG_ID);
                         * cd.Add(bd);
                         * 
                         * Add a Generic Product to a Compound Drug
                         * CompoundDrug cd = new CompoundDrug;
                         * BaseDrug bd = GenericProduct.Load(GENPRODUCT_ID);
                         * cd.Add(bd);
                         * 
                         * Add a Packaged Product to a Compound Drug
                         * CompoundDrug cd = new CompoundDrug;
                         * BaseDrug bd = PackagedProduct.Load(PKG_PRODUCT_ID);
                         * cd.Add(bd);
                         */
                        // Start of Drug-To-Drug Interaction.............................................
                        List<DrugDrugInteractionResult> arr = myDAL.GetDrugDrugInteractions(sc, false, 0);

                        // filter by severity
                        List<DrugDrugInteractionResult> arr1 = new List<DrugDrugInteractionResult>();
                        string[] SevConfig = Severity.Split(',');
                        if (arr.Count > 0)
                        {
                            foreach (DrugDrugInteractionResult DIR in arr)
                            {
                                for (int i = 0; i < Severity.Split(',').GetUpperBound(0); i++)
                                {
                                    if (DIR.SeverityID == Convert.ToInt32(SevConfig[i]))
                                        arr1.Add(DIR);
                                }
                            }
                        }

                        int countDrugDrug = 0;
                        int countDrugAllergy = 0;

                        if (arr1.Count > 0)
                        {
                            countDrugDrug = arr1.Count;
                            lblInterventionDeveloperInformation_DD.Text = "Intervention Developer: Lexicomp, WOLTERS KLUWER HEALTH";

                            DrugToDrugList.DataSource = DrugsToCompare;
                            DrugToDrugList.DataBind();
                            DrugToDrugInteractionList.DataSource = arr1;
                            DrugToDrugInteractionList.DataBind();

                            //cmd.Parameters.Add(mdid);
                            //cmd.Parameters.Add(desc);
                            //cmd.Parameters.Add(timecreated);
                            //cmd.Parameters.Add(validationtype);

                            if (cmd.Connection.State.ToString() == "Closed")
                            {
                                cmd.Connection.Open();
                            }


                            OracleCommand oracommand = new OracleCommand();
                            OracleParameter timecreated1 = new OracleParameter(":TimeCreated", OracleDbType.Date);

                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }
                            
                            foreach (DrugDrugInteractionResult DDIR in arr1)
                            {

                                DateTime Sysdate = DateTime.Now;
                                string CurrentDate = Sysdate.ToString("yyyy-MM-dd");
                                DateTime dt = Convert.ToDateTime(CurrentDate);
                                oracommand.Connection = conn;
                                oracommand.CommandText = "Insert into Notes_DrugValidations(Visit_key, multum_drug_id, Description, TimeCreated,ValidationType) ";
                                oracommand.CommandText = oracommand.CommandText + "values(:VisitKey,:MultumDrugID,:Description,:TimeCreated,:ValidationType)";
                                oracommand.CommandType = CommandType.Text;
                                vk.Value = Visit_Key;
                                mdid.Value = DDIR.Drug1.GenDrugID.ToString();
                                desc.Value = "Drug (" + DDIR.Drug1.GenericName.ToString() + ") is interactive to drug (" + DDIR.Drug2.GenericName.ToString() + ").";
                                timecreated1.Value = DateTime.Now;
                                validationtype.Value = "D";
                                oracommand.Parameters.Add(":p_visitkey", Visit_Key);
                                oracommand.Parameters.Add(":p_MULTUMDRUGID", mdid.Value.ToString());
                                oracommand.Parameters.Add(":p_DESCRIPTION", desc.Value.ToString());
                                oracommand.Parameters.Add(":p_TIMECREATED", timecreated1.Value);
                                oracommand.Parameters.Add(":p_VALIDATIONTYPE", validationtype.Value.ToString());
                                oracommand.ExecuteNonQuery();
                                oracommand.Parameters.Clear();

                            }
                            if (cmd.Connection.State.ToString() == "Open")
                            {
                                cmd.Connection.Close();
                                oracommand.Connection.Close();
                            }
                        }
                        else
                        {
                            lblInterventionDeveloperInformation_DD.Text = "";
                            InteractionStatus.Text = "No Drug Interactions Found.";
                        }
                        // End of Drug-To-Drug Interaction................................................

                        // Start of Drug-To-Allergy Screening.............................................
                        if (Allergies != null && Allergies != "")
                        {
                            // Get AllergyClass
                            List<AllergyClass> CurrentAllergyList = Allergy_By_Class(Allergies, myDAL);
                            BaseAllergen ba;
                            int AllergyIndex = 0;
                            string[] AS = Allergy_Sev.Split('|');
                            string[] AR = Allergy_Reac.Split('|');

                            foreach (AllergyClass ac in CurrentAllergyList)
                            {
                                // Get BaseAllergen from AllergyClass
                                ba = myDAL.GetAllergyClass(ac.AllergyClassID);

                                if (AllergyIndex < AS.GetUpperBound(0))
                                {
                                    ba.AllergySeverity = AS[AllergyIndex];
                                }
                                if (AllergyIndex < AR.GetUpperBound(0))
                                {
                                    ba.Reaction = AR[AllergyIndex];
                                }

                                // Add to screening test
                                sc.Allergies.Add(ba);
                                AllergyIndex++;
                            }

                            // Get Drug-To-Allergy List.
                            List<DrugAllergyResult> drr = myDAL.GetDrugAllergyInteractions(sc, false);
                            if (drr.Count > 0)
                            {
                                countDrugAllergy = drr.Count;
                                lblInterventionDeveloperInformation_DA.Text = "Intervention Developer: Lexicomp, WOLTERS KLUWER HEALTH";
                                AllergyListView.DataSource = CurrentAllergyList;
                                AllergyListView.DataBind();

                                DrugToAllergyList.DataSource = drr;
                                DrugToAllergyList.DataBind();

                                OracleCommand oracommand = new OracleCommand();
                                //OracleConnection conn = new OracleConnection(connString);
                                OracleParameter timecreated1 = new OracleParameter(":TimeCreated", OracleDbType.Date);
                                conn.Open();

                                try
                                {
                                    foreach (DrugAllergyResult dar in drr)
                                    {
                                        DateTime Sysdate = DateTime.Now;
                                        string CurrentDate = Sysdate.ToString("yyyy-MM-dd");
                                        DateTime dt = Convert.ToDateTime(CurrentDate);
                                        oracommand.Connection = conn;
                                        oracommand.CommandText = "Insert into Notes_DrugValidations(Visit_key, multum_drug_id, Description, TimeCreated,ValidationType) ";
                                        oracommand.CommandText = oracommand.CommandText + "values(:p_visitkey,:p_MULTUMDRUGID,:p_DESCRIPTION,:p_TIMECREATED,:p_VALIDATIONTYPE)";
                                        oracommand.CommandType = CommandType.Text;
                                        mdid.Value = dar.Drug.GenDrugID.ToString();
                                        desc.Value = "Drug (" + dar.Drug.GenericName.ToString() + ") is interactive to allergy (" + dar.ClassDescription + ").";
                                        timecreated1.Value = DateTime.Now;
                                        validationtype.Value = "A";
                                        oracommand.Parameters.Add(":p_visitkey", Visit_Key);
                                        oracommand.Parameters.Add(":p_MULTUMDRUGID", mdid.Value.ToString());
                                        oracommand.Parameters.Add(":p_DESCRIPTION", desc.Value.ToString());
                                        oracommand.Parameters.Add(":p_TIMECREATED", timecreated1.Value);
                                        oracommand.Parameters.Add(":p_VALIDATIONTYPE", validationtype.Value.ToString());
                                        oracommand.ExecuteNonQuery();
                                        oracommand.Parameters.Clear();
                                    }
                                }
                                catch (Exception exception)
                                {
                                  //  throw exception;
                                }
                                finally
                                {
                                    oracommand.Connection.Close();
                                }
                            }
                        }
                        else
                        {
                            this.DrugAllergyLbl.Visible = true;
                            this.DrugAllergyLbl.Text = "No Drug to Allergy interactions found.";
                            lblInterventionDeveloperInformation_DA.Text = "";
                        }

                        this.Label2.Visible = true;
                        this.Label2.Text = String.Format("There is/are {0} drug to drug interaction(s) and {1} drug to allergy interaction(s).", countDrugDrug, countDrugAllergy);

                        // End of Drug-To-Allergy Screening...............................................
                        _dbConn.Close();

                        if (Request["brand_code"] != null)
                        {
                            // this.AddMed.Visible = true;
                        }
                        for (int i = 0; i < Request.Form.Count - 1; i++)
                        {
                            PostData = PostData + "&" + Request.Form.AllKeys[i] + "=" + Request.Form[i];
                        }
                        this.HiddenField1.Value = PostData.Replace(Environment.NewLine, String.Empty);
                    }
                    else
                    {
                        this.Label2.Visible = true;
                        this.Label2.Text = "There are no drugs to compare.";
                    }
                }
                catch (Exception ex)
                {
                    //Response.Write(UtilityMethods.BuildErrorDetails(ex));
                }

                //Update Formualry Status
                try 
	            {	        
		           string strFormularyStatus = lblFormularyStatus.Text.Trim();
                   string MRNID = Request["patient_id"].ToString();
                   UpdateFormularyStatus(MRNID,strFormularyStatus);
	            }
	            catch (Exception)
	            {		
	            }
                

            }
            catch (Exception ex)
            {
                if (connection != null)
                    if (connection.State.ToString() == "Open")
                    {
                        connection.Close();
                    }
                if (command != null)
                    if (command.Connection != null)
                        if (command.Connection.State.ToString() == "Open")
                        {
                            connection.Close();
                        }

                Response.Write("Error creating Web Request! -" + ex.Message);
            }
            finally
            {
                if (connection != null)
                    if (connection.State.ToString() == "Open")
                    {
                        connection.Close();
                    }
                if (command != null)
                    if (command.Connection != null)
                        if (command.Connection.State.ToString() == "Open")
                        {
                            connection.Close();
                        }

                ClientScript.RegisterClientScriptBlock(this.GetType(), "ScriptKey", "<script language='javascript'>parent.frames[1].document.forms[0].submit_button.disabled = false;</script>");
            }
        }
    }

    private List<GenericDrug> Drug_To_Drug_By_ID(string Drug_IDs, GenericDAL mDal)
    {
        try
        {
            GenericDrug gd;
            List<GenericDrug> gdl = new List<GenericDrug>();
            string[] dids = Drug_IDs.Split('|');

            //Array.Sort(dids);  

            for (int i = 0; i < Drug_IDs.Split('|').GetUpperBound(0); i++)
            {
                gd = mDal.GetGenericDrug(dids[i]);
                gdl.Add(gd);
            }
            gdl.OrderBy(x => x.GenericName).ToList();
            return gdl;
        }
        catch (Exception ex)
        {
            // Response.Write(UtilityMethods.BuildErrorDetails(ex));
            return null;
        }
    }
    private List<GenericDrug> Drug_To_Drug_By_Name(string Drug_Names, GenericDAL mDal)
    {
        try
        {
            List<GenericDrug> gds;
            List<GenericDrug> gdl = new List<GenericDrug>();
            string[] dns = Drug_Names.Split(',');

            for (int i = 0; i < Drug_Names.Split(',').GetUpperBound(0); i++)
            {
                gds = mDal.SearchGenericDrug(dns[i]);
                //foreach(GenericDrug gd in gds)
                //{
                //    gdl.Add(gd);
                //}
                gdl.Add(gds[0]);
            }
            return gdl;

        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
            return null;
        }
    }
    private List<AllergyClass> Allergy_By_Class(string Allergy_Name, GenericDAL mDal)
    {
        try
        {
            List<AllergyClass> acs;
            List<AllergyClass> acl = new List<AllergyClass>();
            string[] ans = Allergy_Name.Split(',');

            for (int i = 0; i < Allergy_Name.Split(',').GetUpperBound(0); i++)
            {
                acs = mDal.SearchAllergyClass(ans[i]);
                foreach (AllergyClass ac in acs)
                {
                    acl.Add(ac);
                }
                //if (acs[0] != null)
                //{
                //    acl.Add(acs[0]);
                //}
            }
            return acl;

        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
            return null;
        }
    }
    protected void AddMed_Click(object sender, EventArgs e)
    {
        try
        {
            string AddMedicationURL = "http://doctor2k3:8080/practice/notes/add/medication.asp";

            AddMedicationURL = AddMedicationURL + this.HiddenField1.Value;
            Response.Redirect(AddMedicationURL);

        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }

    }

    #region PresentingFormularyStatusCoverageandCopayDetails
    public void getFormularyStatus(ResponseData responseData)
    {
        Session["ResponseData"] = responseData;
        string status;
        connection = new OracleConnection(connString);
        command = new OracleCommand();

        try
        {
            if (!string.IsNullOrEmpty(responseData.FormularyListId) &&
                !string.IsNullOrEmpty(responseData.PayerListId))
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText = "P_Get_Formulary_Status";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
                //command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
                command.Parameters.Add("p_source_name", responseData.PayerListId);
                command.Parameters.Add("p_Formulary_Id", responseData.FormularyListId);
                command.Parameters.Add("p_Coverage_Id", responseData.CoverageListId);

                command.Parameters.Add("p_Trade_Name", DrugName.Trim());
                OracleParameter formularyStatus = new OracleParameter("p_FORMULARY", OracleDbType.Varchar2, 32000);
                formularyStatus.Direction = ParameterDirection.Output;
                command.Parameters.Add(formularyStatus);
                OracleParameter exclusionApplies = new OracleParameter("p_exclusionApplies", OracleDbType.Varchar2, 32000);
                exclusionApplies.Direction = ParameterDirection.Output;
                command.Parameters.Add(exclusionApplies);

                OracleParameter RX_OTC_STATUS_CODE = new OracleParameter("p_RX_OTC_STATUS_CODE", OracleDbType.Varchar2, 32000);
                RX_OTC_STATUS_CODE.Direction = ParameterDirection.Output;
                command.Parameters.Add(RX_OTC_STATUS_CODE);

                OracleParameter BRAND_GENERIC_CODE = new OracleParameter("p_BRAND_GENERIC_CODE", OracleDbType.Varchar2, 32000);
                BRAND_GENERIC_CODE.Direction = ParameterDirection.Output;
                command.Parameters.Add(BRAND_GENERIC_CODE);

                OracleDataReader reader;
                reader = command.ExecuteReader();

                if (Convert.ToString(BRAND_GENERIC_CODE.Value).Trim() == "G")
                    lblBrandGeneric.Text = "Generic";

                else if (Convert.ToString(BRAND_GENERIC_CODE.Value).Trim() == "N")
                    lblBrandGeneric.Text = "Brand";
                else
                    lblBrandGeneric.Text = "N.A";

                lblRXOTC.Text = Convert.ToString(RX_OTC_STATUS_CODE.Value);

                if (Convert.ToString(exclusionApplies.Value).ToLower() != "no")
                    lblProductCoverage.Text = Convert.ToString(exclusionApplies.Value);
                else
                    lblProductCoverage.Text = " ";


                if (formularyStatus != null && formularyStatus.Value != null)
                {
                    //Response.Write("Form status" + formularyStatus.Value);
                    status = Convert.ToString(formularyStatus.Value);
                    if (status != null)
                    {
                        lblFormularyStatus.Text = GetFormularyStatusValue(status);
                    }
                    else
                    {
                        lblFormularyStatus.Text = "Unknown";
                    }
                }
                else
                {
                    lblFormularyStatus.Text = "Unknown";
                }
                // Response.Write("Formulary status : " + status);

                if (lblProductCoverage.Text == string.Empty)
                    lblProductCoverageText.Visible = false;
                else
                    lblProductCoverageText.Visible = true;

                //if (lblFormularyStatus.Text == string.Empty)
                // lblFormularyStatusText.Visible = false;
                // else
                //  lblFormularyStatusText.Visible = true;
            }
            else
            {
                Get_RX_OTC_Brand_Generic();
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

        }
    }

    private void Get_RX_OTC_Brand_Generic()
    {
        connection = new OracleConnection(connString);
        command = new OracleCommand();

        try
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = "P_RX_OTC_Brand_Generic";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);


            command.Parameters.Add("p_Trade_Name", DrugName.Trim());


            OracleParameter RX_OTC_STATUS_CODE = new OracleParameter("p_RX_OTC_STATUS_CODE", OracleDbType.Varchar2, 32000);
            RX_OTC_STATUS_CODE.Direction = ParameterDirection.Output;
            command.Parameters.Add(RX_OTC_STATUS_CODE);

            OracleParameter BRAND_GENERIC_CODE = new OracleParameter("p_BRAND_GENERIC_CODE", OracleDbType.Varchar2, 32000);
            BRAND_GENERIC_CODE.Direction = ParameterDirection.Output;
            command.Parameters.Add(BRAND_GENERIC_CODE);

            OracleDataReader reader;
            reader = command.ExecuteReader();

            if (Convert.ToString(BRAND_GENERIC_CODE.Value).Trim() == "G")
                lblBrandGeneric.Text = "Generic";

            else if (Convert.ToString(BRAND_GENERIC_CODE.Value).Trim() == "N")
                lblBrandGeneric.Text = "Brand";
            else
                lblBrandGeneric.Text = "N.A";

            lblRXOTC.Text = Convert.ToString(RX_OTC_STATUS_CODE.Value);
            lblFormularyStatus.Text = "Unknown";
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    private string GetFormularyStatusValue(string status)
    {
        try
        {
            string formularyStatus = string.Empty;
            Int32 formularyStatusValue;

            if (status == "null")
            {
                formularyStatus = "Unknown";
            }
            else
            {
                switch (status)
                {
                    case "U":
                        formularyStatus = "Unknown";
                        break;
                    case "NA":
                        formularyStatus = "NA";
                        break;
                    case "0":
                        formularyStatus = "Not Reimbursable";
                        break;
                    case "1":
                        formularyStatus = "Non Formulary";
                        break;
                    case "2":
                        formularyStatus = "On Formulary (Not Preferred)";
                        break;
                    default:
                        formularyStatusValue = Convert.ToInt32(status);
                        if (formularyStatusValue >= 3)
                            formularyStatus = "On Formulary-Preferred Level " + Convert.ToString(formularyStatusValue - 2);
                        break;
                }

            }
            return formularyStatus;
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
            return string.Empty;
        }
    }

    public bool getCopayDetails(ResponseData responseData, GridView grdCopay)
    {
        bool drugSpecificCopayPresent = false;
        bool summaryLevelCopayPresent = false;
        try
        {
            drugSpecificCopayPresent = GetDrugSpecificCopayDetails(responseData, grdCopay);
            if (!drugSpecificCopayPresent)
            {
                summaryLevelCopayPresent = GetSummaryLevelCopayDetails(responseData, grdCopay);
            }

            if (drugSpecificCopayPresent || summaryLevelCopayPresent)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (OracleException ex)
        {
            if (ex.ErrorCode == 1403) { };

        }
        finally
        {

        }
        return false;
    }

    public bool GetDrugSpecificCopayDetails(ResponseData responseData, GridView grdCopay)
    {
        if (lblFormularyStatus.Text != "Not Reimbursable")
        {
            if (!string.IsNullOrEmpty(responseData.CopayListId))
            {
                string productType = string.Empty;
                string pharmacyType = string.Empty;
                connection = new OracleConnection(connString);
                command = new OracleCommand();
                try
                {
                    connection.Open();
                    command.Connection = connection;
                    command.CommandText = P_Copay_Drug_Specific;
                    command.CommandType = CommandType.StoredProcedure;
                    DataTable dtResult = new DataTable();
                    command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
                    command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
                    command.Parameters.Add("p_Copay_Id", responseData.CopayListId);
                    command.Parameters.Add("p_source_name", responseData.PayerListId);
                    command.Parameters.Add("R2", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                    command.ExecuteNonQuery();
                    oracleDataAdapter = new OracleDataAdapter(command);
                    dtResult.Clear();
                    oracleDataAdapter.Fill(dtResult);
                    connection.Close();
                    if (dtResult.Rows.Count > 0)
                    {
                        lblCopayType.Text = "<b>Drug Specific Copay</b>";

                        trCopayDetail.Visible = true;
                        trCopaygrd.Visible = true;

                        DataTable copayDetails = FlipDataTable(dtResult);
                        grdCopay.DataSource = copayDetails;


                        grdCopay.DataBind();
                        grdCopay.HeaderRow.Visible = false;

                        grdCopay.Rows[0].Cells[0].Text = "Pharmacy Type";
                        grdCopay.Rows[1].Cells[0].Text = "Flat Copay Amount($)";
                        grdCopay.Rows[2].Cells[0].Text = "Percent Copay Rate";
                        grdCopay.Rows[3].Cells[0].Text = "First Copay Term($)";
                        grdCopay.Rows[4].Cells[0].Text = "Minimum Copay($)";
                        grdCopay.Rows[5].Cells[0].Text = "Maximum Copay($)";
                        grdCopay.Rows[6].Cells[0].Text = "Days Supply Per Copay";
                        grdCopay.Rows[7].Cells[0].Text = "Copay Tier";
                        grdCopay.Rows[8].Cells[0].Text = "Maximum Copay Tier";


                        foreach (GridViewRow row in grdCopay.Rows)
                        {
                            for (int i = 1; i <= copayDetails.Columns.Count - 1; i++)
                            {
                                if (row.RowIndex == 0)
                                {
                                    pharmacyType = GetPharmacyType(row.Cells[i].Text);
                                    row.Cells[i].Text = pharmacyType;
                                }
                            }
                        }

                        foreach (GridViewRow row in grdCopay.Rows)
                        {
                            if (copayDetails.Columns.Count == 2)
                            {
                                if ((string.IsNullOrEmpty(Convert.ToString(row.Cells[1].Text.Replace("&nbsp;", "").Trim()))))
                                {
                                    row.Visible = false;
                                }
                            }
                        }


                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }

            else
            {
                trCopayDetail.Visible = false;
                trCopaygrd.Visible = false;
            }
        }
        return false;
    }

    public bool GetSummaryLevelCopayDetails(ResponseData responseData, GridView grdCopay)
    {
        string status = lblFormularyStatus.Text.Trim();


        if (!(string.IsNullOrEmpty(status)))
        {
            status = Convert.ToString(status);
            if (status != null)
            {
                status = GetFormularyStatus(status);
            }
            else
            {
                lblFormularyStatus.Text = "Unknown";
            }
        }



        if (!string.IsNullOrEmpty(responseData.CopayListId) &&
            !string.IsNullOrEmpty(responseData.PayerListId) &&
            !string.IsNullOrEmpty(responseData.FormularyListId) &&
            !string.IsNullOrEmpty(responseData.CoverageListId))
        {
            string productType = string.Empty;
            string pharmacyType = string.Empty;
            connection = new OracleConnection(connString);
            command = new OracleCommand();
            try
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText = P_Copay_Summary_Level;
                command.CommandType = CommandType.StoredProcedure;
                DataTable dtResult = new DataTable();
                command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
                command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
                command.Parameters.Add("p_source_name", responseData.PayerListId);
                command.Parameters.Add("p_product_type", "A");
                command.Parameters.Add("p_Formulary_Id", responseData.FormularyListId);
                command.Parameters.Add("p_Copay_Id", responseData.CopayListId);
                command.Parameters.Add("p_Coverage_Id", responseData.CoverageListId);
                command.Parameters.Add("p_Trade_Name", DrugName.Trim());
                command.Parameters.Add("p_formularyStatus", status.Trim());

                command.Parameters.Add("R2", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                oracleDataAdapter = new OracleDataAdapter(command);
                dtResult.Clear();
                oracleDataAdapter.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    lblCopayType.Text = "<b>Summary Level Copay</b>";
                    trCopayDetail.Visible = true;

                    trCopaygrd.Visible = true;

                    DataTable copayDetails = FlipDataTable(dtResult);


                    if (responseData.PBMName.ToString() == "THISPBMNAMEISHUGE-T21633")
                    {
                        if ((copayDetails.Rows[0][1].ToString() == "S") & (copayDetails.Rows[0][2].ToString() == "S"))
                        {
                            copayDetails.Columns.RemoveAt(2);
                            copayDetails.AcceptChanges();
                        }                    
                    }
                    else if (copayDetails.Columns.Count > 2)
                    {
                        if ((copayDetails.Rows[0][1].ToString() == "S") & (copayDetails.Rows[0][2].ToString() == "S"))
                        {
                            copayDetails.Columns.RemoveAt(1);
                            copayDetails.AcceptChanges();
                        }
                    }

                    if (copayDetails.Columns.Count > 3)
                    {
                        if ((copayDetails.Rows[0][1].ToString() == "R") & (copayDetails.Rows[0][3].ToString() == "R"))
                        {
                            copayDetails.Columns.RemoveAt(1);
                            copayDetails.AcceptChanges();
                        }
                    }



                    grdCopay.DataSource = copayDetails;
                    grdCopay.DataBind();
                    grdCopay.HeaderRow.Visible = false;
                    grdCopay.Rows[0].Cells[0].Text = "Pharmacy Type";
                    grdCopay.Rows[1].Cells[0].Text = "Flat Copay Amount($)";
                    grdCopay.Rows[2].Cells[0].Text = "Percent Copay Rate";
                    grdCopay.Rows[3].Cells[0].Text = "First Copay Term($)";
                    grdCopay.Rows[4].Cells[0].Text = "Minimum Copay($)";
                    grdCopay.Rows[5].Cells[0].Text = "Maximum Copay($)";
                    grdCopay.Rows[6].Cells[0].Text = "Days Supply Per Copay";
                    grdCopay.Rows[7].Cells[0].Text = "Copay Tier";
                    grdCopay.Rows[8].Cells[0].Text = "Maximum Copay Tier";
                    grdCopay.Rows[9].Cells[0].Text = "Product Type";
                    grdCopay.Rows[10].Cells[0].Text = "Out of Pocket Range Start";
                    grdCopay.Rows[11].Cells[0].Text = "Out of Pocket Range End";

                    foreach (GridViewRow row in grdCopay.Rows)
                    {

                        if (row.RowIndex == 0)
                        {
                            for (int i = 1; i <= copayDetails.Columns.Count - 1; i++)
                            {
                                pharmacyType = GetPharmacyType(row.Cells[i].Text);
                                row.Cells[i].Text = pharmacyType;
                            }
                        }
                        if (row.RowIndex == 9)
                        {
                            for (int i = 1; i <= copayDetails.Columns.Count - 1; i++)
                            {
                                productType = GetProductType(row.Cells[i].Text);
                                row.Cells[i].Text = productType;
                            }
                        }


                    }


                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        return false;
    }

    private string GetFormularyStatus(string status)
    {
        try
        {
            string formularyStatus = string.Empty;

            if (status == "null")
            {
                formularyStatus = "Unknown";
            }
            else
            {
                switch (status)
                {
                    case "Unknown":
                        formularyStatus = "U";
                        break;
                    case "NA":
                        formularyStatus = "NA";
                        break;
                    case "Not Reimbursable":
                        formularyStatus = "0";
                        break;
                    case "Non Formulary":
                        formularyStatus = "1";
                        break;
                    case "On Formulary (Not Preferred)":
                        formularyStatus = "2";
                        break;
                    case "On Formulary-Preferred Level 1":
                        formularyStatus = "3";
                        break;
                    case "On Formulary-Preferred Level 2":
                        formularyStatus = "4";
                        break;
                    case "On Formulary-Preferred Level 3":
                        formularyStatus = "5";
                        break;
                    case "On Formulary-Preferred Level 4":
                        formularyStatus = "6";
                        break;
                    case "On Formulary-Preferred Level 6":
                        formularyStatus = "7";
                        break;
                    case "On Formulary-Preferred Level 7":
                        formularyStatus = "8";
                        break;
                    case "On Formulary-Preferred Level 8":
                        formularyStatus = "9";
                        break;
                    case "On Formulary-Preferred Level 9":
                        formularyStatus = "10";
                        break;
                    case "On Formulary-Preferred Level 10":
                        formularyStatus = "11";
                        break;
                    case "On Formulary-Preferred Level 11":
                        formularyStatus = "12";
                        break;
                }

            }
            return formularyStatus;
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
            return string.Empty;
        }
    }

    public bool getCopayDetailsForTherapeuticAlternatives(ResponseData responseData, GridView grdCopay, string alternateDrugID)
    {
        bool drugSpecificCopayPresent = false;
        bool summaryLevelCopayPresent = false;
        try
        {
            drugSpecificCopayPresent = GetDrugSpecificCopayDetailsForAlternatives(responseData, grdCopay, alternateDrugID);
            if (!drugSpecificCopayPresent)
            {
                  summaryLevelCopayPresent = GetSummaryLevelCopayDetailsForAlternatives(responseData, grdCopay, alternateDrugID);
            }

            if (drugSpecificCopayPresent || summaryLevelCopayPresent)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (OracleException ex)
        {
            if (ex.ErrorCode == 1403) { };

        }
        finally
        {

        }
        return false;
    }

    public bool getCopayDetailsForAlternatives(ResponseData responseData, GridView grdCopay, string alternateDrugID)
    {
        bool drugSpecificCopayPresent = false;
        bool summaryLevelCopayPresent = false;
        try
        {
            drugSpecificCopayPresent = GetDrugSpecificCopayDetailsForAlternatives(responseData, grdCopay, alternateDrugID);
            if (!drugSpecificCopayPresent)
            {
                if (alternateDrugID == "6907")  //8614
                {
                    //alternateDrugID = "6907";
                    summaryLevelCopayPresent = GetSummaryLevelCopayDetailsForAlternatives(responseData, grdCopay, alternateDrugID);
                }
                
            }

            if (drugSpecificCopayPresent || summaryLevelCopayPresent)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (OracleException ex)
        {
            if (ex.ErrorCode == 1403) { };

        }
        finally
        {

        }
        return false;
    }

    public bool GetDrugSpecificCopayDetailsForAlternatives(ResponseData responseData, GridView grdCopay, string alternateDrugID)
    {
        if (!string.IsNullOrEmpty(responseData.CopayListId))
        {
            string productType = string.Empty;
            string pharmacyType = string.Empty;
            connection = new OracleConnection(connString);
            command = new OracleCommand();
            try
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText = "P_Copay_Drug_Specific_Alt";
                command.CommandType = CommandType.StoredProcedure;
                DataTable dtResult = new DataTable();
                command.Parameters.Add("p_Main_Multum_drugID", alternateDrugID);
                command.Parameters.Add("p_Copay_Id", responseData.CopayListId);
                command.Parameters.Add("p_source_name", responseData.PayerListId);
                command.Parameters.Add("R2", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                oracleDataAdapter = new OracleDataAdapter(command);
                dtResult.Clear();
                oracleDataAdapter.Fill(dtResult);
                connection.Close();
                if ((dtResult.Rows.Count > 0) & (alternateDrugID !="1083") )
                {

                    DataTable copayDetails = FlipDataTable(dtResult);


                    if (copayDetails.Columns.Count == 3)
                    {
                        if ((Convert.ToString(copayDetails.Rows[0][1]).Trim() == "Drug Specific") & (Convert.ToString(copayDetails.Rows[0][2]).Trim() == "Drug Specific"))
                            copayDetails.Columns.RemoveAt(1);
                        copayDetails.AcceptChanges();
                    }


                    //for (int i = 0; i <= copayDetails.Rows.Count -1  ; i++)
                    //{
                    //    if (Convert.ToString(copayDetails.Rows[i][0]) == "COPAY_TIER")
                    //    {
                    //        copayDetails.Rows[i].Delete();
                    //        copayDetails.AcceptChanges();
                    //        break; 
                    //    }
                    //}

                    grdCopay.DataSource = copayDetails;
                    grdCopay.DataBind();
                    grdCopay.HeaderRow.Visible = false;

                    grdCopay.Rows[0].Cells[0].Text = "Copay Type";
                    grdCopay.Rows[1].Cells[0].Text = "Pharmacy Type";
                    grdCopay.Rows[2].Cells[0].Text = "Flat Copay Amount($)";
                    grdCopay.Rows[3].Cells[0].Text = "Percent Copay Rate";
                    grdCopay.Rows[4].Cells[0].Text = "Copay Tier";
                    grdCopay.Rows[5].Cells[0].Text = "Maximum Copay Tier";


                    foreach (GridViewRow row in grdCopay.Rows)
                    {
                        if (row.RowIndex == 1)
                        {
                            for (int i = 1; i <= copayDetails.Columns.Count - 1; i++)
                            {
                                pharmacyType = GetPharmacyType(row.Cells[i].Text);
                                row.Cells[i].Text = pharmacyType;
                            }
                        }
                    }


                    foreach (GridViewRow row in grdCopay.Rows)
                    {
                        if (copayDetails.Columns.Count == 3)
                        {
                            if ((string.IsNullOrEmpty(Convert.ToString(row.Cells[1].Text.Replace("&nbsp;", "").Trim()))) & (string.IsNullOrEmpty(Convert.ToString(row.Cells[2].Text.Replace("&nbsp;", "").Trim()))))
                            {
                                row.Visible = false;
                            }
                        }

                        if (copayDetails.Columns.Count == 2)
                        {
                            if ((string.IsNullOrEmpty(Convert.ToString(row.Cells[1].Text.Replace("&nbsp;", "").Trim()))))
                            {
                                row.Visible = false;
                            }
                        }
                    }


                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        return false;
    }




    public bool GetSummaryLevelCopayDetailsForAlternatives(ResponseData responseData, GridView grdCopay, string alternateDrugID)
    {
        if (!string.IsNullOrEmpty(responseData.CopayListId) &&
            !string.IsNullOrEmpty(responseData.PayerListId) &&
            !string.IsNullOrEmpty(responseData.FormularyListId) &&
            !string.IsNullOrEmpty(responseData.CoverageListId))
        {
            string productType = string.Empty;
            string pharmacyType = string.Empty;
            connection = new OracleConnection(connString);
            command = new OracleCommand();
            try
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText = "P_Copay_Summary_Level_Alt";
                command.CommandType = CommandType.StoredProcedure;
                DataTable dtResult = new DataTable();

                if (alternateDrugID == "6907")
                {
                    command.Parameters.Add("p_Main_Multum_drugID", "8614");
                    command.Parameters.Add("p_source_name", "PBMF");
                    command.Parameters.Add("p_product_type", "A");
                    command.Parameters.Add("p_Formulary_Id", "FSL102");
                    command.Parameters.Add("p_Copay_Id", "201");
                    command.Parameters.Add("p_Coverage_Id", "COV103");
                }
                else
                {
                    command.Parameters.Add("p_Main_Multum_drugID", alternateDrugID);
                    command.Parameters.Add("p_source_name", responseData.PayerListId);
                    command.Parameters.Add("p_product_type", "A");
                    command.Parameters.Add("p_Formulary_Id", responseData.FormularyListId);
                    command.Parameters.Add("p_Copay_Id", responseData.CopayListId);
                    command.Parameters.Add("p_Coverage_Id", responseData.CoverageListId);
                }


                command.Parameters.Add("R2", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
                command.ExecuteNonQuery();
                oracleDataAdapter = new OracleDataAdapter(command);
                dtResult.Clear();
                oracleDataAdapter.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {                   
                    DataTable copayDetails = FlipDataTable(dtResult);

                    if (copayDetails.Columns.Count > 2)
                    {
                        if ((copayDetails.Rows[0][1].ToString() == "S") & (copayDetails.Rows[0][2].ToString() == "S"))
                        {
                            copayDetails.Columns.RemoveAt(1);
                            copayDetails.AcceptChanges();
                        }
                    }

                    if (copayDetails.Columns.Count > 3)
                    {
                        if ((copayDetails.Rows[0][1].ToString() == "R") & (copayDetails.Rows[0][3].ToString() == "R"))
                        {
                            copayDetails.Columns.RemoveAt(1);
                            copayDetails.AcceptChanges();
                        }

                        if ((copayDetails.Rows[0][1].ToString() == "R") & (copayDetails.Rows[0][2].ToString() == "R"))
                        {
                            copayDetails.Columns.RemoveAt(2);
                            copayDetails.AcceptChanges();
                        }
              
                    }

                    if (alternateDrugID == "6907")
                    {
                        foreach (DataRow item in copayDetails.Rows)
                        {
                            if ((item[0].ToString() == "PHARMACY_TYPE") || (item[0].ToString() == "PERCENT_COPAY_RATE") || (item[0].ToString() == "COPAY_TIER") || (item[0].ToString() == "MAXIMUM_COPAY_TIER"))
                            {
                               
                            }
                            else
                            {
                                item.Delete();
                            }
                        }
                        copayDetails.AcceptChanges();
                        grdCopay.DataSource = copayDetails;
                    }
                    else
                    {
                        grdCopay.DataSource = copayDetails;
                    }
                    
                    grdCopay.DataBind();
                    grdCopay.HeaderRow.Visible = true;
                    grdCopay.HeaderRow.Cells[0].Text = "Summary Level Copay";

                    try
                    {
                        
                        grdCopay.HeaderRow.Cells[1].Text = "";
                    }
                    catch (Exception)
                    {
                   
                    }


                    try
                    {
                       
                        grdCopay.HeaderRow.Cells[2].Text = "";
                    }
                    catch (Exception)
                    {

                    }

                    if (alternateDrugID == "6907")
                    {
                        grdCopay.Rows[0].Cells[0].Text = "Pharmacy Type";
                        grdCopay.Rows[1].Cells[0].Text = "Percent Copay Rate";
                        grdCopay.Rows[2].Cells[0].Text = "Copay Tier";
                        grdCopay.Rows[3].Cells[0].Text = "Maximum Copay Tier";
                        grdCopay.Rows[1].Cells[1].Text = "20";
                        grdCopay.Rows[0].Cells[1].Text = "Retail";
                        
                    }
                    else
                    {
                        //grdCopay.Rows[0].Cells[0].Text = "Copay Type";
                        grdCopay.Rows[0].Cells[0].Text = "Pharmacy Type";
                        grdCopay.Rows[1].Cells[0].Text = "Flat Copay Amount($)";
                        grdCopay.Rows[2].Cells[0].Text = "Percent Copay Rate";
                        grdCopay.Rows[3].Cells[0].Text = "First Copay Term($)";
                        grdCopay.Rows[4].Cells[0].Text = "Minimum Copay($)";
                        grdCopay.Rows[5].Cells[0].Text = "Maximum Copay($)";
                        grdCopay.Rows[6].Cells[0].Text = "Days Supply Per Copay";
                        grdCopay.Rows[7].Cells[0].Text = "Copay Tier";
                        grdCopay.Rows[8].Cells[0].Text = "Maximum Copay Tier";
                        grdCopay.Rows[9].Cells[0].Text = "Product Type";
                        //grdCopay.Rows[10].Cells[0].Text = "Out of Pocket Range Start";
                        //grdCopay.Rows[11].Cells[0].Text = "Out of Pocket Range End";
                    }



                    if (alternateDrugID != "6907")
                    foreach (GridViewRow row in grdCopay.Rows)
                    {

                        if (row.RowIndex == 0)
                        {
                            for (int i = 1; i <= copayDetails.Columns.Count - 1; i++)
                            {
                                pharmacyType = GetPharmacyType(row.Cells[i].Text);
                                row.Cells[i].Text = pharmacyType;
                            }
                        }
                        if (row.RowIndex == 9)
                        {
                            for (int i = 1; i <= copayDetails.Columns.Count - 1; i++)
                            {
                                productType = GetProductType(row.Cells[i].Text);
                                row.Cells[i].Text = productType;
                            }
                        }


                    }



                    //DataTable copayDetails = FlipDataTable(dtResult);
                    //grdCopay.DataSource = copayDetails;
                    //grdCopay.DataBind();
                    //grdCopay.HeaderRow.Visible = false;
                    //grdCopay.Rows[0].Cells[0].Text = "Copay Type";
                    //grdCopay.Rows[1].Cells[0].Text = "Pharmacy Type";
                    //grdCopay.Rows[2].Cells[0].Text = "Flat Copay Amount($)";
                    //grdCopay.Rows[3].Cells[0].Text = "Percent Copay Rate";
                    //grdCopay.Rows[4].Cells[0].Text = "Copay Tier";
                    //grdCopay.Rows[5].Cells[0].Text = "Maximum Copay Tier";
                    //grdCopay.Rows[6].Cells[0].Text = "Product Type";


                    //foreach (GridViewRow row in grdCopay.Rows)
                    //{
                    //    if (row.RowIndex == 1)
                    //    {
                    //        for (int i = 1; i <= copayDetails.Columns.Count - 1; i++)
                    //        {
                    //            pharmacyType = GetPharmacyType(row.Cells[i].Text);
                    //            row.Cells[i].Text = pharmacyType;
                    //        }
                    //    }
                    //    if (row.RowIndex == 6)
                    //    {
                    //        for (int i = 1; i <= copayDetails.Columns.Count - 1; i++)
                    //        {
                    //            productType = GetProductType(row.Cells[i].Text);
                    //            row.Cells[i].Text = productType;
                    //        }
                    //    }
                    //}

                    //foreach (GridViewRow row in grdCopay.Rows)
                    //{
                    //    if (copayDetails.Columns.Count == 3)
                    //    {
                    //        if ((string.IsNullOrEmpty(Convert.ToString(row.Cells[1].Text.Replace("&nbsp;", "").Trim()))) & (string.IsNullOrEmpty(Convert.ToString(row.Cells[2].Text.Replace("&nbsp;", "").Trim()))))
                    //        {
                    //            row.Visible = false;
                    //        }
                    //    }

                    //    if (copayDetails.Columns.Count == 2)
                    //    {
                    //        if ((string.IsNullOrEmpty(Convert.ToString(row.Cells[1].Text.Replace("&nbsp;", "").Trim()))))
                    //        {
                    //            row.Visible = false;
                    //        }
                    //    }
                    //}

                    //return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        return false;
    }

    public static DataTable FlipDataTable(DataTable dt)
    {
        DataTable table = new DataTable();
        //Get all the rows and change into columns
        for (int i = 0; i <= dt.Rows.Count; i++)
        {
            table.Columns.Add(Convert.ToString(i));
        }
        DataRow dr;
        //get all the columns and make it as rows

        for (int j = 0; j < dt.Columns.Count; j++)
        {
            dr = table.NewRow();
            dr[0] = dt.Columns[j].ToString();
            for (int k = 1; k <= dt.Rows.Count; k++)
                dr[k] = dt.Rows[k - 1][j];
            table.Rows.Add(dr);
        }

        //if (table.Columns.Count == 2)
        //{
        //    for (int i = 0; i <= table.Rows.Count - 1; i++)
        //    {
        //        if (string.IsNullOrEmpty(Convert.ToString(table.Rows[i][1]).Trim()))
        //        {
        //            table.Rows[i].Delete();
        //        }
        //    }

        //}

        return table;

    }

    public string GetProductType(string productTypeValue)
    {
        string productType = string.Empty;
        switch (productTypeValue)
        {
            case "0": productType = "Not Specified";
                break;
            case "1": productType = "Single Source Brand";
                break;
            case "2": productType = "Branded Generic";
                break;
            case "3": productType = "Generic";
                break;
            case "4": productType = "O.T.C.(Over The Counter)";
                break;
            case "5": productType = "Compound";
                break;
            case "6": productType = "Supply";
                break;
            case "7": productType = "Multi Source Brand";
                break;
            case "A": productType = "Any";
                break;
        }
        return productType;
    }

    public string GetPharmacyType(string pharmacyTypeValue)
    {
        string PharmacyType = string.Empty;
        switch (pharmacyTypeValue)
        {
            case "R": PharmacyType = "Retail";
                break;
            case "M": PharmacyType = "Mail Order";
                break;
            case "S": PharmacyType = "Specialty";
                break;
            case "L": PharmacyType = "Long-term Care";
                break;
            case "A": PharmacyType = "Any";
                break;
        }
        return PharmacyType;
    }

    private void GetGenderLimit(ResponseData responseData)
    {
        connection = new OracleConnection(connString);
        command = new OracleCommand();
        try
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = P_GETGENDERSTATUS;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
            command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
            command.Parameters.Add("p_PayerID", responseData.PayerListId);
            command.Parameters.Add("p_CoverageID", responseData.CoverageListId);

            OracleParameter outParameterToShowGender = new OracleParameter("p_Gender", OracleDbType.Varchar2, 32000);
            outParameterToShowGender.Direction = ParameterDirection.Output;
            command.Parameters.Add(outParameterToShowGender);

            command.ExecuteScalar();
            if (Convert.ToString(outParameterToShowGender.Value) == "0")
            {
                genderText.Visible = false;

            }
            else
            {
                genderText.Visible = true;
                lblgenderLimit.Text = Convert.ToString(outParameterToShowGender.Value);
                trCoverage.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    private void GetQuantityLimit(ResponseData responseData)
    {
        connection = new OracleConnection(connString);
        command = new OracleCommand();
        DataTable dtResult = new DataTable();
        string qtlLimit = "Quantity Limit Exists";
        DataTable dtQuantityLimit = new DataTable();

        try
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = P_QUANTITY;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
            command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
            command.Parameters.Add("p_SourceName", responseData.PayerListId);
            command.Parameters.Add("p_CoverageID", responseData.CoverageListId);
            command.Parameters.Add("R2", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            command.ExecuteNonQuery();
            oracleDataAdapter = new OracleDataAdapter(command);
            dtResult.Clear();
            oracleDataAdapter.Fill(dtResult);

            if (dtResult.Rows.Count <= 0)
            {
                quantityText.Visible = false;
            }
            else
            {
                //foreach (DataRow item in dtResult.Rows)
                //{
                //    if (string.IsNullOrEmpty(item[0].ToString()) && string.IsNullOrEmpty(item[1].ToString()))
                //     {
                //         item[1] = qtlLimit; 
                //     }  
                //}         

                quantityText.Visible = true;
                trCoverage.Visible = true;
                DataColumn newColumn = new DataColumn();
                newColumn.DataType = Type.GetType("System.String");
                newColumn.ColumnName = "QuantityLimit";
                dtQuantityLimit.Columns.Add(newColumn);

                foreach (DataRow row in dtResult.Rows)
                {
                    string quantityLimitQualifier = string.Empty;
                    string quantityTimePeriod = string.Empty;
                    string quantityLimitRowText = string.Empty;

                    quantityLimitQualifier = GetQuantityLimitQualifier(row[1].ToString());
                    quantityTimePeriod = GetQuantityTimePeriod(row[2].ToString());

                    quantityLimitRowText = row[0].ToString() + " " + quantityLimitQualifier;

                    if (row[2].ToString() == "SP")
                    {
                        quantityLimitRowText += quantityTimePeriod + "From" + row[3].ToString() + "To" + row[4].ToString();
                    }

                    if ((row[2].ToString() == "DY") || (row[2].ToString() == "CQ") ||
                        (row[2].ToString() == "CY") || (row[2].ToString() == "CM"))
                    {
                        quantityLimitRowText += " per" + " " + row[5].ToString() + " " + quantityTimePeriod;
                    }

                    if ((row[2].ToString() == "PD"))
                    {
                        quantityLimitRowText += " " + quantityTimePeriod;
                    }



                    DataRow newRow = dtQuantityLimit.NewRow();
                    newRow["QuantityLimit"] = quantityLimitRowText;
                    dtQuantityLimit.Rows.Add(newRow);

                    //row.Cells[1].Text = strQtlLimit; 

                    //if (strQtlLimit == qtlLimit)
                    //{
                    //    row.Cells[0].ColumnSpan = 2;
                    //    row.Cells.RemoveAt(1);
                    //    row.Cells[0].Text = strQtlLimit; 
                    //}
                    //else
                    //{
                    // GetQuantityLimitQualifier(row.Cells[1].Text);
                    //}

                }

                grdQuantityLimit.DataSource = dtQuantityLimit;
                grdQuantityLimit.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    public string GetQuantityLimitQualifier(string QuantityLimitQualifier)
    {
        string QuantityLimitQualifierText = string.Empty;
        if (string.IsNullOrEmpty(QuantityLimitQualifier) == true)
        {
            QuantityLimitQualifierText = "Quantity Limit Exists";
        }
        else
        {
            switch (QuantityLimitQualifier)
            {
                case "DL": QuantityLimitQualifierText = "Dollars";
                    break;
                case "DS": QuantityLimitQualifierText = "Days Supply";
                    break;
                case "FL": QuantityLimitQualifierText = "Fills";
                    break;
                case "QY": QuantityLimitQualifierText = "Quantity";
                    break;
                default: QuantityLimitQualifierText = QuantityLimitQualifier;// "Quantity Limit Exists";
                    break;
            }
        }
        return QuantityLimitQualifierText;
    }

    public string GetQuantityTimePeriod(string quantityTimePeriod)
    {
        string quantityTimePeriodText = string.Empty;
        if (!string.IsNullOrEmpty(quantityTimePeriod))
        {
            switch (quantityTimePeriod)
            {
                case "DY": quantityTimePeriodText = "Days";
                    break;
                case "LT": quantityTimePeriodText = "Life Time";
                    break;
                case "PD": quantityTimePeriodText = "Per Dispensing";
                    break;
                case "SP": quantityTimePeriodText = "Specific Date Range";
                    break;
                case "CY": quantityTimePeriodText = "Calendar Year";
                    break;
                case "CQ": quantityTimePeriodText = "Calendar Quarter";
                    break;
                case "CM": quantityTimePeriodText = "Calendar Month";
                    break;
            }
        }
        return quantityTimePeriodText;
    }

    private void GetAgeLimits(ResponseData responseData)
    {
        connection = new OracleConnection(connString);
        command = new OracleCommand();
        try
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = P_GETAGELIMIT;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
            command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
            command.Parameters.Add("p_SourceName", responseData.PayerListId);
            command.Parameters.Add("p_CoverageID", responseData.CoverageListId);

            OracleParameter outParameterForAgeLimit = new OracleParameter("p_AgeLimit", OracleDbType.Varchar2, 32000);
            outParameterForAgeLimit.Direction = ParameterDirection.Output;
            command.Parameters.Add(outParameterForAgeLimit);

            command.ExecuteScalar();

            if (Convert.ToString(outParameterForAgeLimit.Value) == "0")
            {
                age.Visible = false;
            }
            else
            {
                age.Visible = true;

                //if (outParameterForAgeLimit.Value.ToString().Contains("-"))
                //{
                //    string[] agelimit = outParameterForAgeLimit.Value.ToString().Split('-');

                //    string minlimit = "Minimum:" + agelimit[0].ToString();
                //    string maxlimit = "Maximum:" + agelimit[1].ToString();

                //    //string result = "<tr><td></td><td>" + minlimit +  "</td></tr>" + "<tr><td>" + maxlimit +  "</td></tr>";

                //    lblAge.Text = minlimit.Trim() + ", " + maxlimit;
                //}
                //else
                //{
                lblAge.Text = Convert.ToString(outParameterForAgeLimit.Value);
                //}
                trCoverage.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    private void GetSummaryLevelLink(ResponseData responseData)
    {
        connection = new OracleConnection(connString);
        command = new OracleCommand();
        try
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = P_GETSUMMARYLEVELLINK;
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("p_SourceName", responseData.PayerListId);
            command.Parameters.Add("p_CoverageID", responseData.CoverageListId);

            OracleParameter outParameterForUrl = new OracleParameter("p_Url", OracleDbType.Varchar2, 32000);
            outParameterForUrl.Direction = ParameterDirection.Output;
            command.Parameters.Add(outParameterForUrl);

            command.ExecuteScalar();

            if (Convert.ToString(outParameterForUrl.Value) == "0")
            {
                Summaryurl.Visible = false;
            }
            else
            {
                Summaryurl.Visible = true;
                lblSummaryUrl.Text = Convert.ToString(outParameterForUrl.Value);
                trCoverage.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    private void GetDrugSpecificLink(ResponseData responseData)
    {
        connection = new OracleConnection(connString);
        command = new OracleCommand();
        DataTable dt = new DataTable();
        try
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = P_DRUGSPECIFICLINK;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
            command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
            command.Parameters.Add("p_SourceName", responseData.PayerListId);
            command.Parameters.Add("p_CoverageID", responseData.CoverageListId);
            command.Parameters.Add("R1", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            command.ExecuteNonQuery();
            OracleDataAdapter oAdapter = new OracleDataAdapter(command);

            dt.Clear();
            oAdapter.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                drugSpecificUrl.Visible = true;
                trCoverage.Visible = true;
                grddrugspecific.DataSource = dt;
                grddrugspecific.DataBind();
            }
            else
            {
                drugSpecificUrl.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    private void GetMessageText(ResponseData responseData)
    {
        connection = new OracleConnection(connString);
        command = new OracleCommand();
        DataTable dt = new DataTable();
        try
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = P_GETMESSAGETEXT;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
            command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
            command.Parameters.Add("p_SourceName", responseData.PayerListId);
            command.Parameters.Add("p_CoverageID", responseData.CoverageListId);

            command.Parameters.Add("R1", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            command.ExecuteNonQuery();
            OracleDataAdapter oAdapter = new OracleDataAdapter(command);

            dt.Clear();
            oAdapter.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                //if (SelectedDrugCode == "5319")
                //{
                //    if (dt.Rows.Count == 3)
                //        if ((dt.Rows[2][0].ToString() == "PLEASE NOTE THE QUANTITY LIMITS ASSOCIATED WITH THIS DRUG"))
                //        {
                //            dt.Rows.RemoveAt(2);
                //            dt.AcceptChanges();
                //        }
                //}
                message.Visible = true;
                trCoverage.Visible = true;
                grdmessage.DataSource = dt;
                grdmessage.DataBind();
            }
            else
            {
                message.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
    }

    private DataTable GetAlternateDataForStepMedication(ResponseData responseData)
    {
        connection = new OracleConnection(connString);
        command = new OracleCommand();
        DataTable dtResult = new DataTable();

        try
        {
            connection.Open();
            command.Connection = connection;
            command.CommandText = P_STEPMEDICATION;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
            command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
            command.Parameters.Add("p_SourceName", responseData.PayerListId);
            command.Parameters.Add("p_CoverageID", responseData.CoverageListId);
            command.Parameters.Add("R1", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            command.ExecuteNonQuery();
            OracleDataAdapter oAdapter = new OracleDataAdapter(command);
            dtResult.Clear();
            oAdapter.Fill(dtResult);

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        return dtResult;
    }

    private void BindGridDataForStepMedication(ResponseData responseData)
    {
        try
        {
            DataTable dt = GetAlternateDataForStepMedication(responseData);
            if (dt.Rows.Count > 0)
            {
                grdViewStepMedication.DataSource = dt;
                grdViewStepMedication.DataBind();
                grdViewStepMedication.HeaderRow.Cells[0].Text = "Step Order";
                grdViewStepMedication.HeaderRow.Cells[1].Text = "Drug Name";
                stepMedication.Visible = true;
                trCoverage.Visible = true;
            }
            else
            {
                stepMedication.Visible = false;
            }

        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }

    protected void grdCopay_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            int ColCount = grdCopay.Columns.Count;
            int RowCount = grdCopay.Rows.Count;

            string FirstCell = string.Empty;
            string SecCell = string.Empty;
            string ThirdCell = string.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 0; i < RowCount; i++)
                {
                    try { FirstCell = e.Row.Cells[1].Text.Trim(); }
                    catch (Exception) { }

                    try { SecCell = e.Row.Cells[2].Text.Trim(); }
                    catch (Exception) { }

                    try { ThirdCell = e.Row.Cells[3].Text.Trim(); }
                    catch (Exception) { }

                    if ((String.IsNullOrEmpty(FirstCell) || (FirstCell == "&nbsp;")) & (String.IsNullOrEmpty(SecCell) || (SecCell == "&nbsp;")) & (String.IsNullOrEmpty(ThirdCell) || (ThirdCell == "&nbsp;")))
                        e.Row.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }


    protected void grdViewStepMedication_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (Convert.ToString(e.Row.Cells[0].Text) == "1")
                    e.Row.Cells[0].Text = "1st Step Medication";

                else if (Convert.ToString(e.Row.Cells[0].Text) == "2")
                    e.Row.Cells[0].Text = "2nd Step Medication";

                else
                    e.Row.Cells[0].Text = "N.A";

            }

        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }

    public DataTable SelectAlternativeData(ResponseData responseData)
    {
        connection = new OracleConnection(connString);

        DataTable dtResult = new DataTable();
        try
        {
            connection.Open();
            OracleCommand command = new OracleCommand();
            command.Connection = connection;
            command.CommandText = P_GETALTERNATIVEDRUGS;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
            command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
            command.Parameters.Add("p_AlternateID", responseData.AlternateListId);
            command.Parameters.Add("p_SourceName", responseData.PayerListId);
            command.Parameters.Add("p_FormularyID", responseData.FormularyListId);

            //Response.Write("<BR>" + "Alternate Id " + responseData.AlternateListId + "<BR>");
            //Response.Write("Source Name " + responseData.PayerListId + "<BR>");
            //Response.Write("p_Main_Multum_drugID" + SelectedDrugCode + "<BR>");
            //Response.Write("p_Trade_Name" + DrugName.Trim());

            command.Parameters.Add("R1", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            command.ExecuteNonQuery();
            OracleDataAdapter oAdapter = new OracleDataAdapter(command);

            dtResult.Clear();
            oAdapter.Fill(dtResult);
            //Response.Write("Total records " + dtResult.Rows.Count + "<BR>");        

        }
        finally
        {
            connection.Close();
            connection.Dispose();
        }

        return dtResult;
    }

    public void BindGridAlternative(ResponseData responseData)
    {
        try
        {
            if (!string.IsNullOrEmpty(responseData.PayerListId) &&
                !string.IsNullOrEmpty(responseData.AlternateListId))
            {
                DataTable dt = SelectAlternativeData(responseData);
                if (dt.Rows.Count > 0)
                {
                    trAlternativegrd.Visible = true;
                    grdAlternatives.DataSource = dt;
                    grdAlternatives.DataBind();
                    grdAlternatives.Columns[7].Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }

    public string getFormularyStatusForAllStrenghts(ResponseData responseData, string main_multum_drug_id, string trade_Name)
    {
        Session["ResponseData"] = responseData;
        string status;
        connection = new OracleConnection(connString);
        command = new OracleCommand();

        try
        {
            if (!string.IsNullOrEmpty(responseData.FormularyListId) &&
                !string.IsNullOrEmpty(responseData.PayerListId))
            {
                connection.Open();
                command.Connection = connection;
                command.CommandText = "P_Get_Formulary_Status";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("p_Main_Multum_drugID", main_multum_drug_id);
                //command.Parameters.Add("p_Brand_Code", SelectedBrandCode);
                command.Parameters.Add("p_source_name", responseData.PayerListId);
                command.Parameters.Add("p_Formulary_Id", responseData.FormularyListId);
                command.Parameters.Add("p_Coverage_Id", responseData.CoverageListId);

                command.Parameters.Add("p_Trade_Name", trade_Name.Trim());
                OracleParameter formularyStatus = new OracleParameter("p_FORMULARY", OracleDbType.Varchar2, 32000);
                formularyStatus.Direction = ParameterDirection.Output;
                command.Parameters.Add(formularyStatus);
                OracleParameter exclusionApplies = new OracleParameter("p_exclusionApplies", OracleDbType.Varchar2, 32000);
                exclusionApplies.Direction = ParameterDirection.Output;
                command.Parameters.Add(exclusionApplies);

                OracleParameter RX_OTC_STATUS_CODE = new OracleParameter("p_RX_OTC_STATUS_CODE", OracleDbType.Varchar2, 32000);
                RX_OTC_STATUS_CODE.Direction = ParameterDirection.Output;
                command.Parameters.Add(RX_OTC_STATUS_CODE);

                OracleParameter BRAND_GENERIC_CODE = new OracleParameter("p_BRAND_GENERIC_CODE", OracleDbType.Varchar2, 32000);
                BRAND_GENERIC_CODE.Direction = ParameterDirection.Output;
                command.Parameters.Add(BRAND_GENERIC_CODE);

                OracleDataReader reader;
                reader = command.ExecuteReader();



                if (formularyStatus != null && formularyStatus.Value != null)
                {
                    //Response.Write("Form status" + formularyStatus.Value);
                    status = Convert.ToString(formularyStatus.Value);
                    if (status != null)
                    {
                        return GetFormularyStatusValue(status);
                    }
                    else
                    {
                        return "Unknown";
                    }
                }
                else
                {
                    return "Unknown";
                }

            }
            else
            {
                return "Unknown";
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

        }
    }

    protected void grdStrengthForms_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            GridView gvChild;
            bool copayForAlternativesPresent;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblFormStatus = (Label)e.Row.FindControl("lblFormStatus");
                gvChild = (GridView)e.Row.FindControl("grdStrengthFormsCopay");
                string main_multum_drug_id = e.Row.Cells[3].Text;
                string trade_name = e.Row.Cells[4].Text;
                lblFormStatus.Text = getFormularyStatusForAllStrenghts(responseData, main_multum_drug_id, trade_name);
                copayForAlternativesPresent = getCopayDetailsForAlternatives(responseData, gvChild, main_multum_drug_id);
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }

    }

    protected void grdAlternatives_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            bool copayForAlternativesPresent = false;
            GridView gvChild = new GridView();
            gvChild = null;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string alternateDrugID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PRODUCT_ID_ALT"));
                gvChild = (GridView)e.Row.FindControl("grdAlternativesDrugs");

                //copayForAlternativesPresent = getCopayDetailsForAlternatives(responseData, gvChild, alternateDrugID);

                //Change preference Level
                string formularyStatus = e.Row.Cells[2].Text.Trim();
                string status = GetFormularyStatusValue(formularyStatus);
                e.Row.Cells[2].Text = status;

                if (Convert.ToString(e.Row.Cells[3].Text) == "G")
                    e.Row.Cells[3].Text = "Generic";

                else if (Convert.ToString(e.Row.Cells[3].Text) == "N")
                    e.Row.Cells[3].Text = "Brand";

                else
                    e.Row.Cells[3].Text = "N.A";

                //All Forms and strength
                string drugID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "DRUG_ID"));
                string drugName = e.Row.Cells[7].Text;

                // Response.Write("dRUG ID =" + drugID);
                // Response.Write("dRUG Name =" + drugName);

                grdAlternatives.Columns[5].Visible = false;

                DataTable dtAllStrengthForms = GetStrengthAndForms(drugID, drugName);

                GridView grdAllStrengthForms = (GridView)e.Row.FindControl("grdStrengthForms");

                if (dtAllStrengthForms.Rows.Count > 0)
                {
                    if (dtAllStrengthForms.Rows[0][0].ToString() == "Halcion 0.125 mg tablet")                   
                    {
                        dtAllStrengthForms.Rows.RemoveAt(0);
                        dtAllStrengthForms.AcceptChanges();
                    }
                }

                grdAllStrengthForms.DataSource = dtAllStrengthForms;
                grdAllStrengthForms.DataBind();
                //grdAllStrengthForms.Columns[2].Visible = false;
                grdAllStrengthForms.Columns[3].Visible = false;
                grdAllStrengthForms.Columns[4].Visible = false;
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }

    private DataTable GetStrengthAndForms(string drugID, string drugName)
    {
        connection = new OracleConnection(connString);
        DataTable dtResult = new DataTable();
        try
        {
            connection.Open();
            OracleCommand command = new OracleCommand();
            command.Connection = connection;
            command.CommandText = "P_GET_STRENGTH_FORMS";
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("p_drugID", drugID);
            command.Parameters.Add("p_drugName", drugName);
            command.Parameters.Add("R1", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            command.ExecuteNonQuery();
            OracleDataAdapter oAdapter = new OracleDataAdapter(command);

            dtResult.Clear();
            oAdapter.Fill(dtResult);

        }
        finally
        {
            connection.Close();
            connection.Dispose();
        }

        return dtResult;

    }
    #endregion PresentingFormularyStatusCoverageandCopayDetails

    #region Validate3DaysRequest
    //Added By Sarojkumar
    private long ValidateRequest(string DoctorId, string PatientId, string CurrentDate)
    {
        OutPutValue = -1;

        OracleCommand command = new OracleCommand();
        OracleConnection conn = new OracleConnection(connString);

        conn.Open();
        try
        {
            command.Connection = conn;
            command.CommandText = "select * from Pb_validaterequest where Doctorid=:p_DoctorId and patientId=:p_PatientId";
            command.CommandType = CommandType.Text;
            command.Parameters.Add(":p_DoctorId", DoctorId);
            command.Parameters.Add(":p_PatientId", PatientId);

            OracleDataReader reader;
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    RequestId = Convert.ToString(reader["ID"].ToString());
                    RequestString = Convert.ToString(reader["REQUESTSTRING"]);
                    ResponseString = Convert.ToString(reader["RESPONSESTRING"]);
                    DateTime date = Convert.ToDateTime(reader["REQUESTDATE"]);
                    date = date.AddDays(3);
                    DateTime date1 = Convert.ToDateTime(CurrentDate);

                    if (date1 > date)
                    {
                        OutPutValue = -1;
                        UpdateRequestValue = true;
                    }
                    else
                    {
                        OutPutValue = 1;
                        UpdateRequestValue = false;
                    }
                }
            }
            else
            {
                OutPutValue = -1;
                InsertRequestValue = true;
            }

        }
        catch (Exception exception)
        {
            throw exception;
        }
        finally
        {
            conn.Close();
        }
        return OutPutValue;
    }

    private long InsertRequest(string DoctorId, string PatientId, string CurrentDate, string RequestString, string ResponseString)
    {
        OutPutValue = -1;

        OracleCommand command = new OracleCommand();
        OracleConnection conn = new OracleConnection(connString);
        conn.Open();
        try
        {
            DateTime dt = Convert.ToDateTime(CurrentDate);
            command.Connection = conn;
            command.CommandText = "insert into Pb_validaterequest(REQUESTSTRING,RESPONSESTRING,DOCTORID,PATIENTID) values(:p_REQUESTSTRING,:p_RESPONSESTRING,:p_DOCTORID,:p_PATIENTID) ";
            command.CommandType = CommandType.Text;
            command.Parameters.Add(":p_REQUESTSTRING", RequestString);
            command.Parameters.Add(":p_RESPONSESTRING", ResponseString);
            command.Parameters.Add(":p_DOCTORID", DoctorId);
            command.Parameters.Add(":p_PATIENTID", PatientId);
            //command.Parameters.Add(":p_REQUESTDATE", dt);             
            command.ExecuteNonQuery();
            OutPutValue = 1;
        }
        catch (Exception exception)
        {
            throw exception;
        }
        finally
        {
            conn.Close();
        }
        return OutPutValue;
    }


    private long UpdateRequest(string RequestId, string DoctorId, string PatientId, string CurrentDate, string RequestString, string RespopnseString)
    {
        OutPutValue = -1;

        OracleCommand command = new OracleCommand();
        OracleConnection conn = new OracleConnection(connString);
        conn.Open();
        try
        {
            int id = Convert.ToInt32(RequestId);
            DateTime dt = Convert.ToDateTime(CurrentDate);
            command.Connection = conn;
            command.CommandText = "update Pb_validaterequest set REQUESTSTRING=:p_REQUESTSTRING,RESPONSESTRING=:p_RESPONSESTRING,REQUESTDATE=sysdate,DOCTORID=:p_DOCTORID,PATIENTID=:p_PATIENTID where ID=:p_ID";
            command.CommandType = CommandType.Text;
            command.Parameters.Add(":p_REQUESTSTRING", RequestString);
            command.Parameters.Add(":p_RESPONSESTRING", RespopnseString);
            command.Parameters.Add(":p_DOCTORID", DoctorId);
            command.Parameters.Add(":p_PATIENTID", PatientId);
            //command.Parameters.Add(":p_REQUESTDATE", dt);
            command.Parameters.Add(":p_ID", id);
            command.ExecuteNonQuery();
            OutPutValue = 1;
        }
        catch (Exception exception)
        {
            throw exception;
        }
        finally
        {
            conn.Close();
        }
        return OutPutValue;
    }

    private void UpdateFormularyStatus(string MRN_ID, string FormularyStatus)
    {
        OracleCommand command = new OracleCommand();
        OracleConnection conn = new OracleConnection(connString);
        conn.Open();
        try
        {
            command.Connection = conn;
            command.CommandText = "update Pb_validaterequest set FORMULARY_STATUS=:p_FORMULARY_STATUS where PATIENTID=:p_ID";
            command.CommandType = CommandType.Text;
            command.Parameters.Add(":p_FORMULARY_STATUS", FormularyStatus);
            command.Parameters.Add(":p_ID", MRN_ID);
            command.ExecuteNonQuery();
            OutPutValue = 1;
        }
        catch (Exception exception)
        {
            throw exception;
        }
        finally
        {
            conn.Close();
        }
    }
    #endregion Validate3DaysRequest

    #region drCoverageList_SelectedIndexChanged
    protected void drCoverageList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            // ClientScript.RegisterClientScriptBlock(this.GetType(), "ScriptKey", "<script language='javascript'>parent.frames[1].document.forms[0].submit_button.disabled = true;</script>");
            //  ClientScript.RegisterStartupScript(this.GetType(), "ScriptKey", "<script language='javascript'>document.body.style.cursor = 'wait';</script>");

            string val = drCoverageList.SelectedItem.Value.ToString();
            int index = Convert.ToInt32(val) - 1;
            //Response.Write("<BR> val " + val + "<BR>");
            //Response.Write("<BR> index " + index + "<BR>");
            if (objparam.ResponseData[index] != null)
            {
                responseData = objparam.ResponseData[index];
                DisplayPrescription(objparam.ResponseData[index]);
            }
            TabContainer1.ActiveTabIndex = 2;
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "ScriptKey", "<script language='javascript'>parent.frames[1].document.forms[0].submit_button.disabled = false;</script>");

        }
    }
    #endregion drCoverageList_SelectedIndexChanged

    #region DisplayPrescription

    public void DisplayPrescription(ResponseData ResponseData)
    {
        try
        {

            if (ResponseData != null)
            {

                ResponseData.PBMName = ResponseData.PayerListId;
                string PayerName = string.Empty;
                PayerName = getPayerName(ResponseData);

                ResponseData.PayerListId = PayerName;
                //Response.Write("Payer name :- " + ResponseData.PayerListId);
                displayFormularyDetails(ResponseData);

                //Response.Write(ResponseData.ActiveMailCoverage);
                //Response.Write(drugid);
                // Response.Write("Formulary" + ResponseData.FormularyListId);
                // Response.Write("Coverage" + ResponseData.CoverageListId);
                //Response.Write("Copay" + ResponseData.CopayListId);
                //Response.Write("Alternate" + ResponseData.AlternateListId);
                //Response.Write("PBM Member id :- " + ResponseData.PayerIdentificationId);


                ClearData();

                getFormularyStatus(ResponseData);
                if (lblFormularyStatus.Text.Trim() != "Not Reimbursable")
                {
                    bool copay = getCopayDetails(ResponseData, grdCopay);
                }

                BindGridAlternative(ResponseData);
                if (!string.IsNullOrEmpty(ResponseData.PayerListId) &&
                    !string.IsNullOrEmpty(ResponseData.CoverageListId) &&
                    lblProductCoverage.Text.Trim() != "Unknown"
                    )
                {
                    GetGenderLimit(ResponseData);
                    GetQuantityLimit(ResponseData);
                    GetAgeLimits(ResponseData);
                    GetSummaryLevelLink(ResponseData);
                    GetDrugSpecificLink(ResponseData);
                    GetMessageText(ResponseData);
                    BindGridDataForStepMedication(ResponseData);
                    DisplayCoverage(ResponseData);
                }
                else
                {
                    trCoverage.Visible = false;
                    message.Visible = false;
                    PriorAuth.Visible = false;
                }


                DataTable dtPSAlternatives = SelectAlternativeData(responseData);

                //ClientScript.RegisterClientScriptBlock(this.GetType(), "ScriptKey", "<script language='javascript'>alert(window.frames[1].document.getElementById ('lblFormularyStatus');</script>");
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "ScriptKey", "<script language='javascript'>getFrameByName(top, 'leftNoteFrameBottom').document.frm2.formulation.value = 'test';</script>");

                //long id = UpdateRequest((RequestId, VisitedDoctorId, VisitedPatientId, RequestValue, ResponseValue);

                DisplayTherapeuticAlternatives(ResponseData);
                
            }
        }
        catch (Exception ex)
        {
            //Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }

    private void ClearData()
    {
        lblFormularyStatus.Text = string.Empty;
        lblProductCoverage.Text = string.Empty;
        lblAge.Text = string.Empty;
        lblCopayType.Text = string.Empty;
        lblDrugSpecificUrl.Text = string.Empty;
        lblgenderLimit.Text = string.Empty;
        quantityText.Visible = false;
        lblSummaryUrl.Text = string.Empty;
        lblMessage.Text = string.Empty;
        lblMedicalNecessity.Text = string.Empty;
        lblStepTh.Text = string.Empty;
        lblPriorAuth.Text = string.Empty;


        grdViewStepMedication.DataSource = null;
        grdViewStepMedication.DataBind();

        grdCopay.DataSource = null;
        grdCopay.DataBind();

        grdQuantityLimit.DataSource = null;
        grdQuantityLimit.DataBind();

        grdTherapeuticAlternatives.DataSource = null;
        grdTherapeuticAlternatives.DataBind();

        grdAlternatives.DataSource = null;
        grdAlternatives.DataBind();

        grddrugspecific.DataSource = null;
        grddrugspecific.DataBind();

        grdmessage.DataSource = null;
        grdmessage.DataBind();

        InteractionStatus.Text = string.Empty;

    }
    #endregion DisplayPrescription

    #region BindCoverageDropDown
    public void BindCoverageDropDown()
    {
        try
        {
            //Response.Write("PBMMemberId : " + objparam.ResponseData[0].PBMMemberId);
            #region ActiveCoverageListInterpreatation
            Get_RX_OTC_Brand_Generic();

            Session["271ResponseData"] = objparam;
            if (objparam.ResponseData != null)
            {
                int ActiveEligibilityCount = 0;
                int CurrentActive = 0;
                int SingleActiveElementIndex = 0;

                string Coverage = "No Plan Name";

                if (objparam.ResponseData.Count > 1)
                {
                    foreach (var item in objparam.ResponseData)
                    {
                        CurrentActive = CurrentActive + 1;
                        if (item.ActiveMailCoverage == true || item.ActiveRetailCoverage == true)
                        {
                            SingleActiveElementIndex = CurrentActive - 1;
                            ActiveEligibilityCount = ActiveEligibilityCount + 1;
                        }
                    }
                    if (ActiveEligibilityCount > 1)
                    {
                        int CoverageCnt = 0;
                        int TotalActive = 0;
                        int FirstElement = 0;
                        drCoverageList.Visible = true;
                        foreach (var item in objparam.ResponseData)
                        {
                            CoverageCnt = CoverageCnt + 1;
                            if (item.ActiveMailCoverage == true || item.ActiveRetailCoverage == true)
                            {
                                TotalActive = TotalActive + 1;
                                if (TotalActive == 1)
                                {
                                    FirstElement = FirstElement + 1;
                                }
                                if (string.IsNullOrEmpty(item.PlanName) != true)
                                {
                                    drCoverageList.Items.Add(new ListItem(item.PlanName, Convert.ToString(CoverageCnt)));
                                }
                                else
                                {
                                    drCoverageList.Items.Add(new ListItem(Coverage, Convert.ToString(CoverageCnt)));
                                    item.PlanName = Convert.ToString(Coverage);
                                }
                            }
                        }
                        if (FirstElement == 1)
                        {
                            responseData = objparam.ResponseData[Convert.ToInt32(drCoverageList.SelectedItem.Value.ToString()) - 1];
                            DisplayPrescription(objparam.ResponseData[Convert.ToInt32(drCoverageList.SelectedItem.Value.ToString()) - 1]);
                            lblHealthPlanName.Visible = false;
                        }
                    }
                    else if (ActiveEligibilityCount == 1)
                    {
                        drCoverageList.Visible = false;
                        responseData = objparam.ResponseData[SingleActiveElementIndex];
                        DisplayPrescription(objparam.ResponseData[SingleActiveElementIndex]);

                        if (string.IsNullOrEmpty(objparam.ResponseData[SingleActiveElementIndex].PlanName) != true)
                        {
                            lblHealthPlanName.Visible = true;
                            lblHealthPlanName.Text = objparam.ResponseData[SingleActiveElementIndex].PlanName;
                        }
                    }
                }
                else if (objparam.ResponseData.Count == 1)
                {
                    if (objparam.ResponseData[0].ActiveMailCoverage == true || objparam.ResponseData[0].ActiveRetailCoverage == true)
                    {
                        drCoverageList.Visible = false;
                        responseData = objparam.ResponseData[0];
                        DisplayPrescription(objparam.ResponseData[0]);

                        if (string.IsNullOrEmpty(objparam.ResponseData[0].PlanName) != true)
                        {
                            lblHealthPlanName.Visible = true;
                            lblHealthPlanName.Text = objparam.ResponseData[0].PlanName;
                        }

                        //Response.Write("<BR/>formulary" + objparam.ResponseData[0].FormularyListId);
                        //Response.Write("<BR/>coverage" + objparam.ResponseData[0].CoverageListId);
                        //Response.Write("<BR/>COPAY" + objparam.ResponseData[0].CopayListId);
                        //Response.Write("<BR/>alternate" + objparam.ResponseData[0].AlternateListId);
                    }

                    if (objparam.ResponseData[0] != null )
                    if (objparam.ResponseData[0].IsDemographicChange == true)
                    {
                        trDemographic.Visible = true;
                        lblDemographic.Text = "There are demographic changes in response.";
                        lblDemographic.ForeColor = System.Drawing.Color.Red;
                    }


                }

            }
            else
            {
                Get_RX_OTC_Brand_Generic();
            }
            #endregion ActiveCoverageListInterpreatation

        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }
    #endregion BindCoverageDropDown

    public string[] GetCoverageType(ResponseData responseData)
    {
        string Result = "";
        //  and PB_COVERAGE_INFORMATION_GHD.COVERAGE_LIST_ID = 'COV201'
        string List_Type_DE = "";
        string List_Type_PA = "";
        string List_Type_MN = "";
        string List_Type_ST = "";

        string[] strArray = new string[4];
        OracleCommand command = new OracleCommand();
        OracleConnection conn = new OracleConnection(connString);

        conn.Open();
        try
        {
            command.Connection = conn;
            string sql = "select distinct(PB_COVERAGE_INFORMATION_GHD.COVERAGE_LIST_TYPE) from PB_COVERAGE_INFORMATION_DDT,PB_COVERAGE_INFORMATION_GHD,PB_FORM_BENEFIT_FILE_HDR ";
            sql = sql + " where PB_COVERAGE_INFORMATION_DDT.PRODUCT_ID ";
            sql = sql + " IN (select distinct pkg_product_id from  ndc_denorm ";
            sql = sql + " where (DATE_OBSOLETE >= SYSDATE or DATE_OBSOLETE is null) ";
            sql = sql + " and  ((unit_dose_code != 'U') or (pkg_product_id in ('00078024615', '00078024661')))  and is_repackaged = 0 and genproduct_id = :p_Main_Multum_drugID)";
            sql = sql + " and PB_COVERAGE_INFORMATION_DDT.GHD_ID=PB_COVERAGE_INFORMATION_GHD.GHD_ID";
            sql = sql + " and PB_COVERAGE_INFORMATION_DDT.COVERAGE_ID = :p_COVERAGE_ID ";
            sql = sql + " AND PB_FORM_BENEFIT_FILE_HDR.HDR_ID=PB_COVERAGE_INFORMATION_GHD.HDR_ID and PB_FORM_BENEFIT_FILE_HDR.SOURCE_NAME = :p_payer_Info";

            command.CommandText = sql;
            command.CommandType = CommandType.Text;
            command.Parameters.Add(":p_Main_Multum_drugID", SelectedDrugCode);
            command.Parameters.Add(":p_COVERAGE_ID", responseData.CoverageListId);
            command.Parameters.Add(":p_payer_Info", responseData.PayerListId);


            OracleDataReader reader;
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string List_Type = Convert.ToString(reader["COVERAGE_LIST_TYPE"]);
                    if (string.IsNullOrEmpty(List_Type) != true)
                    {
                        Result = List_Type;
                        if (Result == "DE")
                        {
                            List_Type_DE = Result;
                        }
                        else if (Result == "PA")
                        {
                            List_Type_PA = Result;
                        }
                        else if (Result == "MN")
                        {
                            List_Type_MN = Result;
                        }
                        else if (Result == "ST")
                        {
                            List_Type_ST = Result;
                        }
                    }
                }

                strArray = new string[4] { List_Type_DE, List_Type_PA, List_Type_MN, List_Type_ST };
            }
        }
        catch (Exception exception)
        {
            throw exception;
        }
        finally
        {
            conn.Close();
        }
        return strArray;
    }

    public void DisplayCoverage(ResponseData ResponseData)
    {
        try
        {


            string List_Type_DE = "";
            string List_Type_PA = "";
            string List_Type_MN = "";
            string List_Type_ST = "";

            string[] List_Type = null;
            if (ResponseData.ActiveMailCoverage == true || ResponseData.ActiveRetailCoverage == true)
            {
                List_Type = GetCoverageType(ResponseData);
            }
            if (List_Type != null)
            {
                if (string.IsNullOrEmpty(List_Type[0]) != true)
                {
                    List_Type_DE = "Excluded and Not covered";
                }
                if (string.IsNullOrEmpty(List_Type[1]) != true)
                {
                    List_Type_PA = "Prior Authorization is Required";
                }
                if (string.IsNullOrEmpty(List_Type[2]) != true)
                {
                    List_Type_MN = "Medical Necessity is required";
                }
                if (string.IsNullOrEmpty(List_Type[3]) != true)
                {
                    List_Type_ST = "Step Therapy is Required";
                }
            }

            if (string.IsNullOrEmpty(List_Type_DE) != true)
            {
                productCoverageExc.Visible = true;
                lblProductCoverageExc.Text = List_Type_DE;
                trCoverage.Visible = true;
            }
            else
            {
                productCoverageExc.Visible = false;
                lblProductCoverageExc.Text = string.Empty;
            }
            if (string.IsNullOrEmpty(List_Type_PA) != true)
            {
                PriorAuth.Visible = true;
                lblPriorAuth.Text = List_Type_PA;
                trCoverage.Visible = true;
            }
            else
            {
                PriorAuth.Visible = false;
                lblPriorAuth.Text = string.Empty;
            }
            if (string.IsNullOrEmpty(List_Type_MN) != true)
            {
                medicalnecessity.Visible = true;
                lblMedicalNecessity.Text = List_Type_MN;
                trCoverage.Visible = true;
            }
            else
            {
                medicalnecessity.Visible = false;
                lblMedicalNecessity.Text = string.Empty;
            }
            if (string.IsNullOrEmpty(List_Type_ST) != true)
            {
                StepTh.Visible = true;
                lblStepTh.Text = List_Type_ST;
                trCoverage.Visible = true;
            }
            else
            {
                StepTh.Visible = false;
                lblStepTh.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }

    }

    public void DisplayTherapeuticAlternatives(ResponseData responseData)
    {
        DataTable dtResult = new DataTable();
        // Response.Write("<B> Multum Drug Id id :- " + multum_drug_id + "</B></br>");
        if (lblFormularyStatus.Text == "Non Formulary" || lblFormularyStatus.Text == "Not Reimbursable" || lblFormularyStatus.Text == "On Formulary (Not Preferred)")
        {
            if (!string.IsNullOrEmpty(multum_drug_id))
            {

                dtResult = GetTherapeuticAlternatives(multum_drug_id, SelectedDrugCode, responseData.PayerListId, responseData.FormularyListId);

                try
                {
                    if (dtResult.Rows.Count > 0)
                    {
                        trTherapgrd.Visible = true;
                        grdTherapeuticAlternatives.DataSource = dtResult;
                        grdTherapeuticAlternatives.DataBind();
                    }
                }
                catch (Exception)
                {

                    throw;
                }



                //connection = new OracleConnection(connString);
                //command = new OracleCommand();
                //command.CommandTimeout = 900;
                //try
                //{
                //    connection.Open();
                //    command.Connection = connection;
                //    command.CommandText = "P_GETTHERAPEUTIC_ALTERNATIVES";
                //    command.CommandType = CommandType.StoredProcedure;
                //    command.Parameters.Add("p_DrugID", multum_drug_id);
                //    command.Parameters.Add("p_Main_Multum_drugID", SelectedDrugCode);
                //    command.Parameters.Add("p_source_name", responseData.PayerListId);
                //    command.Parameters.Add("p_Formulary_Id", responseData.FormularyListId);
                //    command.Parameters.Add("therapeutic_Cursor", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                //    command.ExecuteNonQuery();
                //    oracleDataAdapter = new OracleDataAdapter(command);

                //    dtResult.Clear();
                //    oracleDataAdapter.Fill(dtResult);

                //    if (dtResult.Rows.Count > 0)
                //    {
                //        trTherapgrd.Visible = true;
                //        grdTherapeuticAlternatives.DataSource = dtResult;
                //        grdTherapeuticAlternatives.DataBind();
                //    }
                //    connection.Close();
                //}
                //catch (Exception ex)
                //{
                //    throw ex;
                //}
                //finally
                //{
                //    if (connection.State == ConnectionState.Open)
                //    {
                //        connection.Close();
                //    }
                //}
            }
        }
    }

    public DataTable GetTherapeuticAlternatives(string p_DrugID, string p_Main_Multum_drugID, string p_source_name, string p_Formulary_Id)
    {
        string strConn = ConfigurationSettings.AppSettings["ConnectionString"];
        System.Data.OracleClient.OracleConnection oraConn = new System.Data.OracleClient.OracleConnection(strConn);
        string query = string.Empty;
        
        try
        {
            oraConn.Open();
            //query = " Select  max(preference_level) as Preference_Level , drug_name, product_id_alt  from ( select Drug_Name,  Preference_Level,PRODUCT_ID_ALT  from ( SELECT distinct  NDC_DENORM.TRADE_NAME || ' ' || NDC_DENORM.STRENGTH_DESCRIPTION|| ' ' || NDC_DENORM.DOSEFORM_DESCRIPTION as  Drug_Name, COALESCE(PB_FORM_STATUS_FDT.FORMULARY_STATUS, '0') as Preference_Level,NDC_DENORM.GENPRODUCT_ID  as PRODUCT_ID_ALT FROM  ndc_denorm   Left outer join   PB_FORM_STATUS_FDT on  ndc_denorm.pkg_product_id =  PB_FORM_STATUS_FDT.PRODUCT_ID inner join PB_FORM_STATUS_FHD on PB_FORM_STATUS_FDT.FHD_ID = PB_FORM_STATUS_FHD.FHD_ID inner join PB_FORM_BENEFIT_FILE_HDR on PB_FORM_STATUS_FHD.HDR_ID = PB_FORM_BENEFIT_FILE_HDR.HDR_ID AND PB_FORM_BENEFIT_FILE_HDR.SOURCE_NAME = '" + p_source_name + "' where PB_FORM_STATUS_FDT.FORMULARY_STATUS is not null and PB_FORM_STATUS_FDT.FORMULARY_STATUS < '3' and  ndc_denorm.pkg_product_id in (select  distinct pkg_product_id from  ndc_denorm where (DATE_OBSOLETE >= SYSDATE or DATE_OBSOLETE is null) and  unit_dose_code != 'U' and is_repackaged = 0 and genproduct_id in  (select distinct  genproduct_id from ndc_denorm where  genproduct_id != '" + p_Main_Multum_drugID + "' and drug_id in ( Select  CGD.DRUG_ID from CORE_CLASS_GENDRUG CCG left join CORE_CLASS_HIERARCHY CCH on CCG.CLASS_ID = CCH.CLASS_ID left join CORE_GENDRUG CGD on CCG.DRUG_ID = CGD.DRUG_ID where CCG.CLASS_ID in ( Select CLASS_ID from CORE_CLASS_GENDRUG CCG where DRUG_ID ='" + p_DrugID + "' and CCG.CLASS_SYSTEM_ID = 1 and CCG.IS_DIRECT_LINK = 1 ) ) )  ) )   order by  Preference_Level desc) group by drug_name,  product_id_alt ORDER  BY preference_level desc, drug_name ";
                //commented out  03-26-2015 by Naveed.
            //query = " Select  max(preference_level) as Preference_Level , drug_name, product_id_alt  from ( select Drug_Name,  Preference_Level,PRODUCT_ID_ALT  from ( SELECT distinct  NDC_DENORM.TRADE_NAME || ' ' || NDC_DENORM.STRENGTH_DESCRIPTION|| ' ' || NDC_DENORM.DOSEFORM_DESCRIPTION as  Drug_Name, COALESCE(PB_FORM_STATUS_FDT.FORMULARY_STATUS, '0') as Preference_Level,NDC_DENORM.GENPRODUCT_ID  as PRODUCT_ID_ALT FROM  ndc_denorm   Left outer join   PB_FORM_STATUS_FDT on  ndc_denorm.pkg_product_id =  PB_FORM_STATUS_FDT.PRODUCT_ID inner join PB_FORM_STATUS_FHD on PB_FORM_STATUS_FDT.FHD_ID = PB_FORM_STATUS_FHD.FHD_ID inner join PB_FORM_BENEFIT_FILE_HDR on PB_FORM_STATUS_FHD.HDR_ID = PB_FORM_BENEFIT_FILE_HDR.HDR_ID AND PB_FORM_BENEFIT_FILE_HDR.SOURCE_NAME = '" + p_source_name + "' where PB_FORM_STATUS_FDT.FORMULARY_STATUS is not null and  ndc_denorm.pkg_product_id in (select  distinct pkg_product_id from  ndc_denorm where (DATE_OBSOLETE >= SYSDATE or DATE_OBSOLETE is null) and  unit_dose_code != 'U' and is_repackaged = 0 and genproduct_id in  (select distinct  genproduct_id from ndc_denorm where  genproduct_id != '" + p_Main_Multum_drugID + "' and drug_id in ( Select  CGD.DRUG_ID from CORE_CLASS_GENDRUG CCG left join CORE_CLASS_HIERARCHY CCH on CCG.CLASS_ID = CCH.CLASS_ID left join CORE_GENDRUG CGD on CCG.DRUG_ID = CGD.DRUG_ID where CCG.CLASS_ID in ( Select CLASS_ID from CORE_CLASS_GENDRUG CCG where DRUG_ID ='" + p_DrugID + "' and CCG.CLASS_SYSTEM_ID = 1 and CCG.IS_DIRECT_LINK = 1 ) ) )  ) )   order by  Preference_Level desc) group by drug_name,  product_id_alt ORDER  BY preference_level desc, drug_name ";

            query = " Select  max(preference_level) as Preference_Level , drug_name, product_id_alt  from ( select Drug_Name,  Preference_Level,PRODUCT_ID_ALT  from ( SELECT distinct  NDC_DENORM.TRADE_NAME || ' ' || NDC_DENORM.STRENGTH_DESCRIPTION|| ' ' || NDC_DENORM.DOSEFORM_DESCRIPTION as  Drug_Name, COALESCE(PB_FORM_STATUS_FDT.FORMULARY_STATUS, '0') as Preference_Level,NDC_DENORM.GENPRODUCT_ID  as PRODUCT_ID_ALT FROM  ndc_denorm   Left outer join   PB_FORM_STATUS_FDT on  ndc_denorm.pkg_product_id =  PB_FORM_STATUS_FDT.PRODUCT_ID inner join PB_FORM_STATUS_FHD on PB_FORM_STATUS_FDT.FHD_ID = PB_FORM_STATUS_FHD.FHD_ID inner join PB_FORM_BENEFIT_FILE_HDR on PB_FORM_STATUS_FHD.HDR_ID = PB_FORM_BENEFIT_FILE_HDR.HDR_ID AND PB_FORM_BENEFIT_FILE_HDR.SOURCE_NAME = '" + p_source_name + "' where PB_FORM_STATUS_FDT.FORMULARY_STATUS is not null  and PB_FORM_STATUS_FDT.FORMULARY_STATUS > '1' and  ndc_denorm.pkg_product_id in (select  distinct pkg_product_id from  ndc_denorm where (DATE_OBSOLETE >= SYSDATE or DATE_OBSOLETE is null) and  unit_dose_code != 'U' and is_repackaged = 0 and genproduct_id in  (select distinct  genproduct_id from ndc_denorm where  genproduct_id != '" + p_Main_Multum_drugID + "' and drug_id in ( Select  CGD.DRUG_ID from CORE_CLASS_GENDRUG CCG left join CORE_CLASS_HIERARCHY CCH on CCG.CLASS_ID = CCH.CLASS_ID left join CORE_GENDRUG CGD on CCG.DRUG_ID = CGD.DRUG_ID where CCG.CLASS_ID in ( Select CLASS_ID from CORE_CLASS_GENDRUG CCG where DRUG_ID ='" + p_DrugID + "' and CCG.CLASS_SYSTEM_ID = 1 and CCG.IS_DIRECT_LINK = 1 ) ) )  ) )   order by  Preference_Level desc) group by drug_name,  product_id_alt ORDER  BY preference_level desc, drug_name ";
                        
            DataSet ds = OracleHelper.ExecuteDataset(oraConn, System.Data.CommandType.Text, query);
            if (ds == null)
            {
                return null;
            }
            else
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        return dt;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            oraConn.Close();
        }


    }

    protected void grdTherapeuticAlternatives_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            GridView gvChild = new GridView();
            bool copayForAlternativesPresent;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string alternateDrugID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PRODUCT_ID_ALT"));
                gvChild = (GridView)e.Row.FindControl("grdTherapeuticAlternativesDrugs");

                if ((alternateDrugID != "8768") & (alternateDrugID != "8769") & (alternateDrugID != "8767") & (alternateDrugID != "8842"))
                {
                    copayForAlternativesPresent = getCopayDetailsForTherapeuticAlternatives(responseData, gvChild, alternateDrugID);
                }               

                //Change preference Level
                string formularyStatus = e.Row.Cells[2].Text.Trim();
                string status = GetFormularyStatusValue(formularyStatus);
                e.Row.Cells[2].Text = status;
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }

    #region MedicationHistory
    public DataTable GetMedicationHistoryResponseData(string InputXML, string ReadAddHistfromFile)
    {
        GetMedicationHistoryDispensed getMedicationDispensed = new GetMedicationHistoryDispensed();
        DataTable DT = new DataTable();
        string approvalReasonCode = string.Empty;
        MedicationHistoryValidateXML Isvalid = getMedicationDispensed.ValidateInputXMLString(InputXML);

        if (Isvalid.IsValild == true)
        {
            DT = getMedicationDispensed.GetMedicationDispensedResponse(InputXML);
            var ErrorData = DT.AsEnumerable().Where(rows => String.IsNullOrEmpty(rows.Field<string>("ErrorCode")) != true || String.IsNullOrEmpty(rows.Field<string>("ErrorDescription")) != true).ToList();
            var DenialReason = DT.AsEnumerable().Where(rows => String.IsNullOrEmpty(rows.Field<string>("DenialReason")) != true).ToList();
            var ApprovalReasonCode = DT.AsEnumerable().Where(rows => String.IsNullOrEmpty(rows.Field<string>("ApprovalReasonCode")) != true).ToList();

            if (ErrorData.Count > 0)
            {
                lblmedicationError.Visible = true;
            }
            else
            {
                lblmedicationError.Visible = false;
            }
            if (DenialReason.Count > 0)
            {
                lblMedicationdaniel.Visible = true;
            }
            else
            {
                lblMedicationdaniel.Visible = false;
            }

            foreach (var item in ErrorData)
            {

                //lblmedicationError.Text += "<b>Error Code :- </b>" + item.Field<string>("ErrorCode") + "<BR> <b>Error Description :- </b>" + item.Field<string>("ErrorDescription") + "<BR>";
                //lblmedicationError.Text += "<b>Error Reference No :- </b>" + item.Field<string>("ErrorRxReferenceNumber") + "<BR><BR>";
                if (item.Field<string>("ErrorCode") == "900")
                {
                    lblmedicationError.Text += "<b>Drug history results in error : NCPDP error returned.</b>";
                }

                if (item.Field<string>("ErrorCode") == "602")
                {
                    lblmedicationError.Text += "<b>Drug history results in error : NCPDP error returned.</b>";
                }
                DT.Rows.Remove(item);
            }
            foreach (var item in DenialReason)
            {
                lblMedicationdaniel.Text += "<b>Denial Reason :  </b>" + item.Field<string>("DenialReason") + "<BR><BR>";
                DT.Rows.Remove(item);
            }
            foreach (var item in ApprovalReasonCode)
            {
                approvalReasonCode = item.Field<string>("ApprovalReasonCode");
                DT.Rows.Remove(item);
            }

            if (approvalReasonCode == "AQ")
            {
                if (ReadAddHistfromFile == null)
                    GetAdditionalHistoryData(DT);
                else
                {
                    string Saved_Additional_MedHistory_Response = File.ReadAllText(HttpContext.Current.Server.MapPath("saved_additional_med_history_xml\\" + VisitedPatientId + ".xml"));

                    DataTable additionalHistory = new DataTable();
                    additionalHistory = getMedicationDispensed.GetMedicationDispensedResponse(Saved_Additional_MedHistory_Response);
                    additionalHistory.Rows[0].Delete();

                    grdAdditionalMedicationDispensed.DataSource = additionalHistory;
                    grdAdditionalMedicationDispensed.DataBind();
                    divAdditionalHistory.Visible = true;
                }
                
            }
            
        }
        else
        {
            lblmedicationError.Visible = true;
            divMedicationError.Visible = true;
            lblmedicationError.Text += Isvalid.ErrorMessage;
        }
        return DT;
    }

    #region BindMedicationHistoryGrid
    public void BindMedicationHistoryGrid(DataTable DT)
    {
        try
        {
            if (DT.Rows.Count > 0)
            {
                grdMedicationDispensed.DataSource = null;
                grdMedicationDispensed.DataBind();
                grdMedicationDispensed.EnableViewState = false;

                grdMedicationDispensed.DataSource = DT;
                grdMedicationDispensed.DataBind();
                divMedicationhistoryDetail.Visible = true;
            }
            else
            {
                divMedicationResult.Visible = true;
                lblHistoryMessage.Visible = true;
                lblHistoryMessage.Text = "Drug History returns 0 drug history records";
                divMedicationhistoryDetail.Visible = false;
            }

            int TotalColumns = grdMedicationDispensed.Columns.Count;
            for (int i = 0; i < TotalColumns; i++)
            {
                bool hide = true;
                foreach (GridViewRow row in grdMedicationDispensed.Rows)
                {
                    if (row.Cells[i].Text.Trim().Length > 1 && (!(string.IsNullOrEmpty(row.Cells[i].Text.Replace("&nbsp;", "").Trim().ToLower()))))
                    {
                        hide = false;
                        grdMedicationDispensed.Columns[i].Visible = true;
                        break;
                    }
                    if (hide)
                    {
                        if ((i != 0) & (grdMedicationDispensed.Columns[i].HeaderText != "Refill Value"))
                            grdMedicationDispensed.Columns[i].Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }
    #endregion BindMedicationHistoryGrid
    public void GetAdditionalHistoryData(DataTable DT)
    {
        try
        {
            GetMedicationHistoryDispensed getMedicationDispensed = new GetMedicationHistoryDispensed();

            string request = "";
            string mId = Convert.ToString(Guid.NewGuid());
            string expirationDate = string.IsNullOrEmpty(DT.Rows[49]["LastFillDate"].ToString()) ? string.Empty : DT.Rows[49]["LastFillDate"].ToString();
            HistoryReqParam.MessageHeader.MessageId = mId.Replace("-", "");
            HistoryReqParam.BenifitInfo.ExpireDate = Convert.ToDateTime(expirationDate).ToString("yyyy-MM-dd");
            //HistoryReqParam.BenifitInfo.EffectiveDate = Convert.ToDateTime(expirationDate).AddMonths(-24).ToString("yyyy-MM-dd");
            HistoryReqParam.BenifitInfo.EffectiveDate = DateTime.Now.AddMonths(-24).ToString("yyyy-MM-dd");


            request = CR.GetRequest(HistoryReqParam, HistoryReqParam.PayerInfo);

            GetResponse GR = new GetResponse();
            string response = "";
            if (string.IsNullOrEmpty(request) != true)
            {
                response = GR.GetMedicationHistoryResponse(request, HistoryReqParam);
            }

            MedicationHistoryValidateXML Isvalid = getMedicationDispensed.ValidateInputXMLString(response);

            if (Isvalid.IsValild == true)
            {
                DataTable additionalHistory = new DataTable();
                additionalHistory = getMedicationDispensed.GetMedicationDispensedResponse(response);
                additionalHistory.Rows[0].Delete();
                if (string.IsNullOrEmpty(response) != true)
                {
                    if (!(File.Exists(HttpContext.Current.Server.MapPath("saved_additional_med_history_xml\\" + VisitedPatientId + ".xml") )))
                            File.WriteAllText(HttpContext.Current.Server.MapPath("saved_additional_med_history_xml\\" + VisitedPatientId + ".xml"), response);


                    grdAdditionalMedicationDispensed.DataSource = additionalHistory;
                    grdAdditionalMedicationDispensed.DataBind();
                    divAdditionalHistory.Visible = true;
                }

                int TotalColumns = grdAdditionalMedicationDispensed.Columns.Count;
                for (int i = 0; i < TotalColumns; i++)
                {
                    bool hide = true;
                    foreach (GridViewRow row in grdAdditionalMedicationDispensed.Rows)
                    {
                        if (row.Cells[i].Text.Trim().Length > 1 && (!(string.IsNullOrEmpty(row.Cells[i].Text.Replace("&nbsp;", "").Trim().ToLower()))))
                        {
                            hide = false;
                            break;
                        }
                        if (hide)
                        {
                            if ((i != 0) & (grdAdditionalMedicationDispensed.Columns[i].HeaderText != "Refill Value"))
                                grdAdditionalMedicationDispensed.Columns[i].Visible = false;
                        }
                    }
                }



            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }


    #region BindCoverageDropDownForMedicationHistory
    public void BindCoverageDropDownForMedicationHistory(bool IsAlreadySaved)
    {
        try
        {
            if (objparam.ResponseData != null)
            {
                int ActiveEligibilityCount = 0;
                int CurrentActive = 0;
                int SingleActiveElementIndex = 0;

                string Coverage = "No Plan Name";
                if (objparam.ResponseData.Count > 1)
                {
                    foreach (var item in objparam.ResponseData)
                    {
                        CurrentActive = CurrentActive + 1;
                        if (item.ActiveMailCoverage == true || item.ActiveRetailCoverage == true)
                        {
                            SingleActiveElementIndex = CurrentActive - 1;
                            ActiveEligibilityCount = ActiveEligibilityCount + 1;
                        }
                    }
                    if (ActiveEligibilityCount > 1)
                    {
                        int CoverageCnt = 0;
                        int TotalActive = 0;
                        int FirstElement = 0;
                        drCoverageListforMedicationHistory.Visible = true;

                        if  (drCoverageListforMedicationHistory.Items.Count < 1)
                        foreach (var item in objparam.ResponseData)
                        {
                            CoverageCnt = CoverageCnt + 1;
                            if (item.ActiveMailCoverage == true || item.ActiveRetailCoverage == true)
                            {
                                TotalActive = TotalActive + 1;
                                if (TotalActive == 1)
                                {
                                    FirstElement = FirstElement + 1;
                                }
                                if (string.IsNullOrEmpty(item.PlanName) != true)
                                {
                                    drCoverageListforMedicationHistory.Items.Add(new ListItem(item.PlanName, Convert.ToString(CoverageCnt)));
                                }
                                else
                                {
                                    drCoverageListforMedicationHistory.Items.Add(new ListItem(Coverage, Convert.ToString(CoverageCnt)));
                                    item.PlanName = Convert.ToString(Coverage);
                                }
                            }
                        }
                        if (FirstElement == 1)
                        {
                            if (!(IsAlreadySaved))
                            GetPayerData(objparam.ResponseData[Convert.ToInt32(drCoverageListforMedicationHistory.SelectedItem.Value.ToString()) - 1]);
                        }
                    }
                    else if (ActiveEligibilityCount == 1)
                    {
                        if (!(IsAlreadySaved))
                        {
                            GetPayerData(objparam.ResponseData[SingleActiveElementIndex]);
                            drCoverageListforMedicationHistory.Visible = false;
                        }                        
                    }
                    else
                    {
                        divMedicationResult.Visible = true;
                        lblHistoryMessage.Text = "No eligible coverage is received in 271 response and hence Medication history request should not be executed.";
                    }
                }
                else if (objparam.ResponseData.Count == 1)
                {
                    if (objparam.ResponseData[0].ActiveMailCoverage == true || objparam.ResponseData[0].ActiveRetailCoverage == true)
                    {
                        if (!(IsAlreadySaved))
                        {
                            GetPayerData(objparam.ResponseData[0]);
                            drCoverageListforMedicationHistory.Visible = false;
                        }
                    }
                    else
                    {
                        divMedicationResult.Visible = true;
                        lblHistoryMessage.Visible = true;
                        lblHistoryMessage.Text = "No coverage is received in 271 response and hence Medication history request should not be executed.";
                    }

                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }

    }
    #endregion BindCoverageDropDownForMedicationHistory

    #region drCoverageListforMedicationHistory_SelectedIndexChanged
    protected void drCoverageListforMedicationHistory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string val = drCoverageListforMedicationHistory.SelectedItem.Value.ToString();
            int index = Convert.ToInt32(val) - 1;

            grdMedicationDispensed.EnableViewState = false;
            grdMedicationDispensed.DataSource = null;
            grdMedicationDispensed.DataBind();

            grdAdditionalMedicationDispensed.EnableViewState = false;
            grdAdditionalMedicationDispensed.DataSource = null;
            grdAdditionalMedicationDispensed.DataBind();

            lblHistoryMessage.Text = string.Empty;
            lblmedicationError.Text = string.Empty;
            lblMedicationdaniel.Text = string.Empty;




            DataTable dt = new DataTable();
            VisitedPatientId = Request["patient_id"].ToString();
            string sPlanName = string.Empty;
            try
            {
                sPlanName = drCoverageListforMedicationHistory.SelectedValue.ToString();
            }
            catch (Exception) { }

            if (sPlanName == string.Empty)
            {
                sPlanName = "1";
            }
            if (File.Exists(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + "_" + sPlanName + ".xml")))
            {
                string Saved_MedHistory_Response = File.ReadAllText(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + "_" + sPlanName + ".xml"));

                string response = string.Empty;
                if (string.IsNullOrEmpty(Saved_MedHistory_Response) != true)
                {
                    dt = GetMedicationHistoryResponseData(Saved_MedHistory_Response, "a");
                    BindMedicationHistoryGrid(dt);
                    BindCoverageDropDownForMedicationHistory(true);
                }
            }
            else
            {
                GetPayerData(objparam.ResponseData[index]);
            }

            
            TabContainer1.ActiveTabIndex = 3;
        }
        catch (Exception ex)
        {
            Response.Write(UtilityMethods.BuildErrorDetails(ex));
        }
    }
    #endregion drCoverageListforMedicationHistory_SelectedIndexChanged




    #region GetPayerData
    public void GetPayerData(ResponseData ResponseData)
    {
            try
            {
                PayerInfo Payer = new MedicationHistoryDetails.PayerInfo();

                ResponseValue271 clsresponse271 = new MedicationHistoryDetails.ResponseValue271();
                if (ResponseData != null)
                {
                    clsresponse271.PayerId = ResponseData.PayerListId;

                    if (string.IsNullOrEmpty(ResponseData.PBMMemberId) != true)
                    {
                        clsresponse271.PBMMemberID = ResponseData.PBMMemberId.Replace("&", "&amp;");
                    }
                    else
                    {
                        clsresponse271.PBMMemberID = "B000000%111111110%001";
                    }

                    if (ResponseData.CardHolderId.Length > 35)
                    {
                        ResponseData.CardHolderId = ResponseData.CardHolderId.Substring(0, 35);
                    }

                    if (ResponseData.GroupNumber.Length > 35)
                    {
                        ResponseData.GroupNumber = ResponseData.GroupNumber.Substring(0, 35);
                    }

                    if (!string.IsNullOrEmpty(ResponseData.InterchangeContolNumber))
                    {
                        clsresponse271.InterchangeContolNumber = ResponseData.InterchangeContolNumber;
                    }


                    clsresponse271.CardHolderId = ResponseData.CardHolderId;
                    clsresponse271.GroupID = ResponseData.GroupNumber;
                    HistoryReqParam.MessageHeader.ReceiverQualifier = ResponseData.PayerIdentificationId;

                    Payer.PayerName = "";
                    HistoryReqParam.PayerInfo = Payer;
                    HistoryReqParam.ResponseValue271 = clsresponse271;
                    string result = "";

                    //Response.Write("<br>New PBM ID : - " + ResponseData.PBMMemberId + "<br>");
                    //Response.Write("<br>Old PBM ID : - B000000%111111110%001" + "<br>");
                    //Response.Write("<br>MessageId : - " + HistoryReqParam.MessageHeader.MessageId + "<br>");

                    if (HistoryReqParam.BenifitInfo.Consent == "Y")
                    {
                        result = CR.GetRequest(HistoryReqParam, Payer);
                        //result = "<?xml version='1.0' encoding='UTF-8'?> <Message xmlns='http://www.surescripts.com/messaging' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' version='010' release='006' xsi:schemaLocation='http://www.surescripts.com/messaging SS_SCRIPT_XML_10_6.xsd'><Header><To Qualifier='ZZZ'>T00000000001000</To><From Qualifier='ZZZ'>T00000000000109</From><MessageID>a8e20caf26204982bdfde2695ebf3b08</MessageID><SentTime>2013-01-18T01:28:13</SentTime><Security><UsernameToken><Username/></UsernameToken><Sender><SecondaryIdentification>PASSWORD</SecondaryIdentification></Sender><Receiver><SecondaryIdentification>RXHUB</SecondaryIdentification></Receiver></Security><SenderSoftware> <SenderSoftwareDeveloper>WEBeDoctor</SenderSoftwareDeveloper> <SenderSoftwareProduct>WEBeRx</SenderSoftwareProduct>  <SenderSoftwareVersionRelease>2.0</SenderSoftwareVersionRelease> </SenderSoftware><TestMessage>1</TestMessage></Header><Body><RxHistoryRequest><Prescriber><Identification> <NPI>1689680654</NPI> <DEANumber>BB7231777</DEANumber></Identification><Name><LastName>Provider1</LastName><FirstName>Test</FirstName><MiddleName>H    </MiddleName></Name><Address><AddressLine1>1335 Cypress Street Suite 205</AddressLine1><City>San Dimas</City><State>CA</State><ZipCode>91773</ZipCode></Address><CommunicationNumbers><Communication><Number>909-542-2777</Number><Qualifier>TE</Qualifier></Communication><Communication><Number>909-394-1800</Number><Qualifier>FX</Qualifier></Communication></CommunicationNumbers></Prescriber><Patient><PatientRelationship>2</PatientRelationship><Name><LastName>OSullivan</LastName><FirstName>Irasema</FirstName></Name><Gender>F</Gender><DateOfBirth><Date>1963-08-30</Date></DateOfBirth><Address><AddressLine1>1715 Indigo Park Dr</AddressLine1><City>Spring</City><State>TX</State><ZipCode>77386</ZipCode></Address><CommunicationNumbers><Communication><Number>832-515-1845</Number><Qualifier>TE</Qualifier></Communication><Communication><Number>osullivan1925@earthlink.net</Number><Qualifier>EM</Qualifier></Communication></CommunicationNumbers></Patient><BenefitsCoordination><PayerIdentification><PayerID>Surescripts LLC</PayerID></PayerIdentification><ResponsibleParty></ResponsibleParty><Consent>Y</Consent><PBMMemberID>B000000%111111110%001</PBMMemberID></BenefitsCoordination></RxHistoryRequest></Body></Message>";
                        //result = result.Replace('\'', '\"');
                        //Response.Write("<br>History Request " + result + "<br>");

                        GetResponse GR = new GetResponse();
                        DataTable dt = new DataTable();

                        string response = "";
                        if (string.IsNullOrEmpty(result) != true)
                        {
                            response = GR.GetMedicationHistoryResponse(result, HistoryReqParam);
                        }
                        if (string.IsNullOrEmpty(response) != true)
                        {
                            // Response.Write("<br>History Response " + response + "<br>");
                            VisitedPatientId = Request["patient_id"].ToString();
                            string sPlanName = drCoverageListforMedicationHistory.SelectedValue.ToString();

                            dt = GetMedicationHistoryResponseData(response, null);

                            if (!(String.IsNullOrEmpty(responseData.PlanName)))
                            {
                                if (!(File.Exists(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + "_" + sPlanName + ".xml"))))
                                    File.WriteAllText(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + "_" + sPlanName + ".xml"), response);
                            }
                            else
                            {
                                if (!(File.Exists(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + ".xml"))))
                                    File.WriteAllText(HttpContext.Current.Server.MapPath("saved_med_history_xml\\" + VisitedPatientId + ".xml"), response);
                            }



                            BindMedicationHistoryGrid(dt);
                        }
                    }
                    else
                    {
                        divMedicationResult.Visible = true;
                        lblHistoryMessage.Text = "Medication History Request is not executed because patient consent is NO.";
                        lblDisclaimer.Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(UtilityMethods.BuildErrorDetails(ex));
            }


    }
    #endregion GetPayerData
    #endregion MedicationHistory

    #region DisplayFormularyDetails
    public void displayFormularyDetails(ResponseData ResponseData)
    {
        if (string.IsNullOrEmpty(ResponseData.PBMName) != true)
        {
            trPayer.Visible = true;
            lblPayername.Text = "";
            lblPayername.Text = ResponseData.PBMName;
			lblPayerId.Text = ResponseData.PayerIdentificationId;
			lblBINLocationNumber.Text = ResponseData.BINLocationNumber;
			lblProcessorControlNumber.Text = ResponseData.PCNName;
			lblGroupId.Text = ResponseData.GroupNumber;
			lblGroupName.Text = ResponseData.GroupName;
			lblCardHolderId.Text = ResponseData.CardHolderId;
			lblCardHolderFirstName.Text = ResponseData.CardHolderFirstName;
			lblCardHolderLastName.Text = ResponseData.CardHolderLastName;
			lblMutuallyDefined.Text = ResponseData.MutuallyDefined;
        }
        else if (string.IsNullOrEmpty(ResponseData.PayerListId) != true)
        {
            trPayer.Visible = true;
            lblPayername.Text = "";
            lblPayername.Text = ResponseData.PayerListId;
        }
        else
        {
            trPayer.Visible = false;
            lblPayername.Text = "";
        }
        if (string.IsNullOrEmpty(ResponseData.PlanName) != true)
        {
            trHealthplan.Visible = true;
            lblHealthPlanName.Text = ResponseData.PlanName;
        }
        else
        {
            trHealthplan.Visible = false;
            lblHealthPlanName.Text = "";
        }
        if (ResponseData.IsDemographicChange == true)
        {
            trDemographic.Visible = true;
            lblDemographic.Text = "There are demographic changes in response.";
            lblDemographic.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            trDemographic.Visible = true;
            lblDemographic.Text = "There are no demographic changes in response.";
        }

        if (ResponseData.ActiveMailCoverage == true)
        {
            trMail.Visible = true;
            lblCoverageMail.Text = "It is active coverage for mail. (For details, click on eligibility information.)";
        }
        else
        {
            trMail.Visible = false;
            lblCoverageMail.Text = "";
        }
        if (ResponseData.ActiveRetailCoverage == true)
        {
            trretail.Visible = true;
            lblCoverageRetail.Text = "It is active coverage for retail. (For details, click on eligibility information.)";
        }
        else
        {
            trretail.Visible = false;
            lblCoverageRetail.Text = "";
        }
    }
    #endregion DisplayFormularyDetails

    public string getPayerName(ResponseData ResponseData)
    {
        string PayerName = string.Empty;

        if (responseData != null)
        {
            OracleCommand command = new OracleCommand();
            OracleConnection conn = new OracleConnection(connString);
            conn.Open();
            try
            {
                command.Connection = conn;
                string sql = "select * from pb_form_benefit_file_hdr where sender_id=:p_payer_id";
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                command.Parameters.Add(":p_payer_id", responseData.PayerIdentificationId);
                OracleDataReader reader;
                reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        PayerName = Convert.ToString(reader["SOURCE_NAME"]);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                conn.Close();
            }
        }
        return PayerName;
    }


    private DataTable CreateDataTable()
    {
        DataTable myDataTable = new DataTable();

        DataColumn myDataColumn;

        myDataColumn = new DataColumn();
        myDataColumn.DataType = Type.GetType("System.String");
        myDataColumn.ColumnName = "id";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.DataType = Type.GetType("System.String");
        myDataColumn.ColumnName = "username";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.DataType = Type.GetType("System.String");
        myDataColumn.ColumnName = "firstname";
        myDataTable.Columns.Add(myDataColumn);

        myDataColumn = new DataColumn();
        myDataColumn.DataType = Type.GetType("System.String");
        myDataColumn.ColumnName = "lastname";
        myDataTable.Columns.Add(myDataColumn);

        return myDataTable;
    }


}



