﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFTwoFactor
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ITwoFactorAuthentication
    {
        [OperationContract, WebGet(UriTemplate = "authenticate?userid={userid}&otppwd={otppwd}&otpcode={otpcode}")]
        string GetData(string userid, string otppwd, string otpcode);

        [OperationContract, WebGet(UriTemplate = "test")]
        string Test();
    }
}
