﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TwoFactorAuthentication.com.identrust.otp.authentication;
using System.IO;

namespace WCFTwoFactor
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class TwoFactorAuthentication : ITwoFactorAuthentication
    {
        private string SUCCESS { get { return "SUCCESS"; } }
        private string FAIL { get { return "FAIL-&-{0}"; } }
        public string GetData(string userid, string otppwd, string otpcode)
        {
            try
            {
                if (string.IsNullOrEmpty(userid) || string.IsNullOrEmpty(otppwd) || string.IsNullOrEmpty(otpcode))
                {
                    return String.Format(FAIL, "All fields are required!");
                }

                //BEGIN: Skip this block
                if (otpcode == "123456")
                {
                    return "SUCCESS";
                }

                if (otpcode == "654321")
                {
                    return "FAIL-&-Test Only";
                }

                if (otpcode == "111111")
                {
                    throw new Exception("Exception for test only");
                }
                //END: Skip this block

                //AuthResponse result = new AuthResponse();
                bool result = false;

                OTPAuthenticator otpAuth = new OTPAuthenticator(@"C:\OTPAuth\otpAuthetication.properties");

                // Call the API for OTP authentication. Pass in the user ID, current OTP code
                result = otpAuth.authenticateTwoFactorOTP(userid, otppwd, otpcode);
                // Simple output to the screen, printing result true=authenticated, false=not authenticated
                // Relying application should use this information to process functions based on results

                //Response.Write("ID Valid t/f: " + isGood);
                // If auth failed, display the message for the two components to indicate if there was an issue
                // is message is blank, then the component is fine
                if (result)
                {
                    return SUCCESS;
                }
                else
                {
                    //Response.Write("<br/>");
                    //Response.Write("Password Status: " + otpAuth.getLastPWDMessage());
                    //Response.Write("<br/>");
                    //Response.Write("OTP Status: " + otpAuth.getLastOTPMessage());

                    return String.Format(FAIL, "Password Status: " + otpAuth.getLastPWDMessage() + " - OTP Status: " + otpAuth.getLastOTPMessage());
                }
            }
            catch (Exception ex)
            {
                // Simple output to the screen, Relying Applications should process the exception and correct
                // or inform the user of the error
                //result.Passed = false;
                //Response.Write("Authentication failed, exception encountered.");
                //Response.Write("<br/>");
                //Response.Write(ex.Message);

                return String.Format(FAIL, ex.ToString());
            }
        }
        public string Test()
        {
            string result = "";
            string pathTestFile = @"C:\OTPAuth\logs.txt";
            string firstline = "test";

            try
            {
                bool fileExists = File.Exists(pathTestFile);
                if (fileExists)
                {

                    //1. Test Read File
                    result = "1. Test Read File: " + pathTestFile + ".";
                    try
                    {
                        string[] array = File.ReadAllLines(pathTestFile);
                        if (array[0].Contains(firstline))
                        {
                            result += "<br/>Passed!";
                        }
                        else
                        {
                            result += "<br/>Failed! Error: First line does not contain '" + firstline + "'.";
                        }
                    }
                    catch (Exception ex)
                    {
                        result += "<br/>Failed! Error: ";
                        result += ex.ToString();
                    }

                    //2. Test Write File
                    result += "<br/><br/>2. Test Write File: " + pathTestFile + ".";
                    try
                    {
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(pathTestFile, true))
                        {
                            file.WriteLine("test");
                            
                        }
                        result += "<br/>Passed!";
                    }
                    catch (Exception ex)
                    {
                        result += "<br/>Failed! Error: ";
                        result += ex.ToString();
                    }
                }
                else
                {
                    result = "\"" + pathTestFile + "\" does not exist!";
                }

                return result;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
