<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/functions.inc.asp"-->
<%		
	dim strSQL
    set cmd = server.CreateObject("adodb.command")
	set cmd.ActiveConnection = CONN

    Dim section
	section = Request.QueryString("section")

    Dim streetadd
    streetadd = Request.QueryString("streetadd")

    Dim userid
    userid = Request.QueryString("userid")

    Dim is_epcs
    is_epcs = Request.QueryString("is_epcs")

    if is_epcs = "Y" then
    strSQL = " update doctortable set epcs = 'Y-P' where docuser = '" & userid & "' and bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  2048) <> 0"
    cmd.CommandText =  strSQL
    cmd.Execute
    elseif is_epcs = "N" then
    strSQL = " update doctortable set epcs = 'N-P' where docuser = '" & userid & "' and bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  2048) = 0"
    cmd.CommandText =  strSQL
    cmd.Execute
    end if
%>

<html>
<head>
<%=Application("Styles")%>
<script language="Javascript">

function form_val()
{
  if (document.frm.Accid.value=='')
  {
	alert('Please Enter the Account ID');
	document.frm.Accid.focus();
	return false;
  }

};

function facilityChange() {
    var streetadd = document.getElementById("cbFacility").value;

    var frm = document.getElementById("frmStreetAdd");
    frm.streetadd.value = streetadd;
    frm.submit();
};

function addEPCSControl() {
    var facility = document.getElementById("cbFacility").value || "";
    var prescriber = document.getElementById("cbPrescriber").value || "";

    if (facility.length == 0) {
        alert("You must choose Facility before add EPCS control");
        return;
    }

    if (prescriber.length == 0) {
        alert("You must choose Prescriber before add EPCS control");
        return;
    }

    var userId = document.getElementById("cbPrescriber").value;

    var frm = document.getElementById("frmAddEPCSControl");
    frm.userid.value = userId;
    frm.submit();
};

function addPrescriber() {
    var physician = document.getElementById("cbPhysician").value;

    var arr = physician.split("*and*");

    var userId = arr[0];
    var role = arr[1];
    var prescriber = arr[2];
    var dea = arr[3];

    var frm = document.getElementById("frmAddPrescriber");
    frm.userid.value = userId;
    frm.Physician_Id.value = userId;
    //frm.epcsAuditDetail_Practice.value = practice;
    frm.epcsAuditDetail_Prescriber.value = prescriber;
    frm.epcsAuditDetail_DEA.value = dea;
    frm.epcsAuditAuthor.value = userId;
    frm.epcsAuditRole.value = role;
    frm.submit();
};
</script>
<title>SureScripts Directories - Prescriber Administration</title>


</head>
<body>
[ <a href="/admin/admin_main.asp">Home</a> ] &nbsp;&nbsp;&nbsp;&nbsp;
<br><br>

<div class="titleBox">SureScripts Directories - Prescriber Administration</div>
<br />
<b>[ <a href="ss_provider_admin.asp?section=ss_service_level" onmouseover="window.status='SureScripts Service Level'; return true;" onmouseout="window.status=''; return true;">SureScripts Service Level</a> | 
<a href="ss_provider_admin.asp?section=access_epcs" onmouseover="window.status='EPCS Access Control'; return true;" onmouseout="window.status=''; return true;">EPCS Access Control</a> ] </b>
<br />
<%
if section = "ss_service_level" then
 %>
<p>
<div class="titleBox_HL2">Select Prescriber from the list and check the appropriate Service Level.</div>
<p>

<font color=red>Note:<b>Prescriber must have NPI and DEA, Address (street name must be < 36), Phone and Fax.<b></font>

<form id="frmAddPrescriber" name="frm" action="http://192.168.73.4/surescriptInterface/default.aspx" method="get">
<input type="hidden" name="userid" value="" />
<input type="hidden" name="Physician_Id" value="" />
<input type="hidden" name="epcsAuditAuthor" value="" />
<input type="hidden" name="epcsAuditRole" value="" />
<input type="hidden" name="epcsAuditDetail_Practice" value="" />
<input type="hidden" name="epcsAuditDetail_Prescriber" value="" />
<input type="hidden" name="epcsAuditDetail_DEA" value="" />
<input type="hidden" name="epcsAuditDetail_AddedBy" value="<%=session("admin_userid") %>" />
<input type="hidden" name="epcsAuditDetail_Role" value="Admin WEBeDoctor" />
<%

%>
<input type="hidden" name="MsgType" value="AddPrescriber" />
<table border="0" cellpadding="3" cellspacing="0" width="100%">
              <tr>
                <td>
                  <table cellpadding="3" cellspacing="3" border="0" width="100%">                
                      <td>
						<%
							set Rs_Acc = server.CreateObject("ADODB.RecordSet")	
							strsql= "select last_name|| ' ,' ||first_name as doc_name,docuser,facility from doctortable where (last_name is not null) and (dea_no is not null) and (NPI is not null) and (spi is null or spi='') and (fax is not null) " _
									& " and not exists ( select * from streetreg where trim(streetreg.streetadd) = trim(doctortable.facility) and  length(streetreg.orgname) > 35) and length(nvl(doctortable.street,' ')) < 36 order by last_name"
							
							'Response.Write strsql							
							Rs_Acc.Open strSQL,CONN
							if not(Rs_Acc.EOF or Rs_Acc.BOF) then
						%>                      
							<b>Prescriber Name:</b> <select id="cbPhysician" name="prescriberSelect">
							<option value="">Select Prescriber</option>	
						<%
							set rsEPCSAudit = server.CreateObject("ADODB.Recordset")
                                set rsEPCSAudit2 = server.CreateObject("ADODB.Recordset")
							while not Rs_Acc.eof
                                '--START: Add to EPCS_Audit_Log                                
                                prescriber = ""
                                dea = ""
                                strSQLEPCS = "SELECT * FROM DOCTORTABLE WHERE upper(DOCUSER) = upper('" & Rs_Acc("docuser") & "')"
                                rsEPCSAudit.open strSQLEPCS,con
                                if not (rsEPCSAudit.eof or rsEPCSAudit.bof) then
                                    prescriber = rsEPCSAudit("First_Name") & " " & rsEPCSAudit("Last_Name")
                                    dea = rsEPCSAudit("DEA_NO")
                                end if
                                rsEPCSAudit.close

                                strSQLEPCS = "SELECT userid, role FROM acc WHERE upper(streetadd)=upper('"&Rs_Acc("facility")&"') and upper(userid) = upper('" & Rs_Acc("docuser") & "')"
                                rsEPCSAudit.open strSQLEPCS,con
                                if not (rsEPCSAudit.eof or rsEPCSAudit.bof) then
                                    epcsAuditAuthor = rsEPCSAudit("userid")
                                    epcsAuditRole = rsEPCSAudit("role")
                                end if
                                rsEPCSAudit.close

						%>		<option value="<%=Rs_Acc("docuser") & "*and*" & epcsAuditRole & "*and*" & prescriber & "*and*" & dea %>"><%=Rs_Acc("doc_name")%> </option>
						<%  Rs_Acc.MoveNext 
							wend
							Rs_Acc.close
							end if
						%></select>
      </td>
     </tr> 
     
   <tr><td><b>Service Level:</b></td></tr>          
 
 <tr>	
	<td> 
		<input type="checkbox" value="Y" name="serviceLevel_0">New <br/>
		<input type="checkbox" value="Y" name="serviceLevel_1">Refill <br/>
        <!--
		<input type="checkbox" value="Y" name="serviceLevel_3">RxFill<br/>
        -->
		<input type="checkbox" value="Y" name="serviceLevel_2">Change <br/>
		<input type="checkbox" value="Y" name="serviceLevel_4">Cancel<br/>
        <input type="checkbox" value="Y" name="serviceLevel_5">EPCS <br/>
	</td>
 </tr> 
                    <tr>
                      <td colspan="3" height="75">                                                
                        <input id="btnAddPrescriber" type="button" value="Add Prescriber" onclick="addPrescriber()"/>
                        <hr size="1" color="#8CA5B5">                        
                      </td>
                    </tr>
</table>
</form>

<br>

<%
Response.Write "<b><u>Prescriber and SPI </u></b>" &"<br><br>"
set rs20 = server.CreateObject("adodb.recordset")
strSQL = "select last_name|| ' ,' ||first_name as doc_name,docuser, SPI, nvl(dea_no, ' ') as dea_no, SS_Active_startDate,SS_Active_EndDate , epcs, "
strSQL = strSQL & " bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  1) as newrx, "
strSQL = strSQL & " bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  2) as refillrx, "
strSQL = strSQL & " bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  4) as changerx, "
strSQL = strSQL & " bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  8) as rxfill, "
strSQL = strSQL & " bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  16) as rxcancel, "
strSQL = strSQL & " bitand(cast(nvl(ss_service_level,'0') as varchar2(20)) ,  2048) as epcs_service_level "
strSQL = strSQL & "  from doctortable "
strSQL = strSQL & " where "
strSQL = strSQL & " spi is not null "
strSQL = strSQL & " order by last_name "
rs20.Open strSQL,conn





%>

<table border="1">
<tr bgcolor="#3366CC">
	<td><font color=white>Prescriber Name</td>
	<td> <font color=white> SPI</td>
    <td> <font color=white> DEA</td>
	<td><font color=white>Service Level</td>
	<td><font color=white>Active Start Date</td>
	<td><font color=white>Active End Date</td>	
	<td>&nbsp;</td>
</tr>	
<% while not rs20.EOF %>
<form id="frmUpdatePrescriber" name="frm_<%=rs20("docuser")%>" action="http://192.168.73.4/surescriptInterface/default.aspx" method="get">
<input type="hidden" name="userid" value="<%=rs20("docuser") %>" />

<%
'--START: Add to EPCS_Audit_Log
set rsEPCSAudit = server.CreateObject("ADODB.Recordset")
set rsEPCSAudit2 = server.CreateObject("ADODB.Recordset")
strSQLEPCS = "SELECT * FROM DOCTORTABLE WHERE upper(DOCUSER) = upper('" & rs20("docuser") & "')"
rsEPCSAudit.open strSQLEPCS,con
if not (rsEPCSAudit.eof or rsEPCSAudit.bof) then
    strSQLEPCS2 = "SELECT orgname,streetadd FROM streetreg WHERE acc_status='V' and upper(streetadd) = upper('" & rsEPCSAudit("Facility") & "')"
    rsEPCSAudit2.open strSQLEPCS2,con
    if not (rsEPCSAudit2.eof or rsEPCSAudit2.bof) then
        %>
        <input type="hidden" name="epcsAuditDetail_Practice" value="<%=rsEPCSAudit2("orgname") %>" />
        <input type="hidden" name="epcsAuditDetail_Prescriber" value="<%=rsEPCSAudit("First_Name") & " " & rsEPCSAudit("Last_Name") %>" />
        <input type="hidden" name="epcsAuditDetail_DEA" value="<%=rsEPCSAudit("DEA_NO") %>" />
        <input type="hidden" name="epcsAuditDetail_UpdatedBy" value="<%=session("admin_userid") %>" />
        <input type="hidden" name="epcsAuditDetail_Role" value="Admin WEBeDoctor" />
        <%
    end if 
    rsEPCSAudit2.close
    
    strSQLEPCS2 = "SELECT userid, role FROM acc WHERE upper(streetadd)=upper('"&rsEPCSAudit("facility")&"') and upper(userid) = upper('" & rs20("docuser") & "')"
    rsEPCSAudit2.open strSQLEPCS2,con
    if not (rsEPCSAudit2.eof or rsEPCSAudit2.bof) then
        epcsAuditAuthor = rsEPCSAudit2("userid")
        epcsAuditRole = rsEPCSAudit2("role")
        %>
            <input type="hidden" name="epcsAuditAuthor" value="<%=rsEPCSAudit2("userid") %>" />
            <input type="hidden" name="epcsAuditRole" value="<%=rsEPCSAudit2("role") %>" />
        <%
    end if
    rsEPCSAudit2.close
     
end if
rsEPCSAudit.close
%>
<input type="hidden" name="MsgType" value="UpdatePrescriber" />

	<tr>
		<td><%=rs20("doc_name")%></td>
		<td><%=rs20("SPI")%> </td>
        <td><%=rs20("dea_no")%> </td>
		<input type="hidden" name="Physician_Id" value="<%=rs20("docuser")%>"/>

		<td>    
            <%
                dim isEnable
                dim controlLabel
                dim controlURL
                if (rs20("newrx") = "0" and rs20("refillrx") = "0" and rs20("changerx") = "0" and rs20("rxfill") = "0" and rs20("rxcancel") = "0") then
                    isEnable = false
                    controlLabel = "Enable SPI"
                    controlAction = true
                else
                    isEnable = true
                    controlLabel = "Disable SPI"
                    controlAction = false
                end if

                dim isEpcs
                if rs20("dea_no") = " " then
                    isEpcs = false
                else
                    isEpcs = true
                end if


            %>
			<input id="<%=rs20("docuser")%>_serviceLevel_0" name="serviceLevel_0" type="checkbox" value="Y" <%if rs20("newrx") <> "0" then Response.Write " checked "%> <%if not isEnable then Response.Write " disabled " %> />New &nbsp;&nbsp;
			<input id="<%=rs20("docuser")%>_serviceLevel_1" name="serviceLevel_1" type="checkbox" value="Y" <%if rs20("refillrx") <> "0" then Response.Write " checked "%> <%if not isEnable then Response.Write " disabled " %> />Refill &nbsp;&nbsp;
			<input id="<%=rs20("docuser")%>_serviceLevel_2" name="serviceLevel_2" type="checkbox" value="Y" <%if rs20("changerx") <> "0" then Response.Write " checked "%> <%if not isEnable then Response.Write " disabled " %> />Change &nbsp;&nbsp;
            <!--
			<input id="<%=rs20("docuser")%>_serviceLevel_3" name="serviceLevel_3" type="checkbox" value="Y" <%if rs20("rxfill") <> "0" then Response.Write " checked "%> <%if not isEnable then Response.Write " disabled " %> />RxFill &nbsp;&nbsp;		
            -->
			<input id="<%=rs20("docuser")%>_serviceLevel_4" name="serviceLevel_4" type="checkbox" value="Y" <%if rs20("rxcancel") <> "0" then Response.Write " checked "%> <%if not isEnable then Response.Write " disabled " %> />Cancel 		
            <input id="<%=rs20("docuser")%>_serviceLevel_5" name="serviceLevel_5" type="checkbox" value="Y" <%if rs20("epcs_service_level") <> "0" then Response.Write " checked "%> <%if not isEpcs then Response.Write " disabled " %> />EPCS &nbsp;&nbsp;
		 </td>
		 <td><%=rs20("ss_Active_startDate")%> </td>
		 <td><%=rs20("ss_Active_EndDate")%> </td>
        <td>
            <input type="submit" value="Update SPI" <%if not isEnable then Response.Write " disabled " %> id="b1" name="b1" />
            <input type="submit" value="<%=controlLabel %>" onclick="togglePrescriberActive('<%=controlAction %>', '<%=rs20("docuser") %>');" id="b2" name="b2" />
            <input type="submit" value="Update EPCS"  id="btnUpdateEpcs" <%if not isEpcs then Response.Write " disabled " %>/>
        </td>
	</tr>
</form>
<%
	rs20.MoveNext
	wend
set rs20=nothing
%>
</table>

<%
else if section = "access_epcs" then
 %>

<p>
<div class="titleBox_HL2">Select Prescriber from the list to grant EPCS access control.</div>
<p>

<form id="frmStreetAdd" action="ss_provider_admin.asp" method="get">
    <input type="hidden" name="section" value="access_epcs" />
    <input type="hidden" name="streetadd" value="" />
</form>

<form id="frmAddEPCSControl" action="ss_provider_admin_add.asp" method="get">
<input type="hidden" name="userid" value="" />
</form>
<table border="0" cellpadding="3" cellspacing="0" width="100%">
    <tr>
        <td><b>Facility:</b></td>
        <td>
            <select id="cbFacility" name="facility" onchange="facilityChange()">
			    <option value="">Select a facility</option>
	            <%  strsql= "SELECT orgname,streetadd FROM streetreg where acc_status='V' order by ORGNAME"
		            set Rs_Acc = server.CreateObject("ADODB.RecordSet")
		            Rs_Acc.Open strSQL,CONN
		            if not(Rs_Acc.EOF or Rs_Acc.BOF) then
		            while not Rs_Acc.eof
	            %>		
                <option value="<%=Rs_Acc("STREETADD")%>" <%if streetadd = Rs_Acc("STREETADD") then Response.Write " selected "%>><%=Rs_Acc("ORGNAME")%> </option>
	            <%  Rs_Acc.MoveNext
		            wend
		            Rs_Acc.close
		            end if
	            %>
            </select>
        </td>
    </tr>
    <tr>
        <td><b>Prescriber:</b></td>
	    <td>
            <select id="cbPrescriber" name="prescriber">
			    <option value="">Select a prescriber</option>
                <%
                if streetadd <> "" then
                 %>
	            <%  
                strsql = "select t1.userid, t2.last_name || ', ' || t2.first_name as doctorname " & _
                    " from acc t1 " & _
                    " inner join doctortable t2 on t1.userid = t2.docuser and t2.dea_no is not null " & _
                    " inner join user_pwd t3 on t1.userid = t3.userid and t3.epcs_access is null " & _
                    " where t1.streetadd = '" & streetadd & "'"
		            set rs_prescriber = server.CreateObject("ADODB.RecordSet")
		            rs_prescriber.Open strSQL,CONN
		            if not(rs_prescriber.EOF or rs_prescriber.BOF) then
		                while not rs_prescriber.eof
	                        %>		
                            <option value="<%=rs_prescriber("userid")%>"><%=rs_prescriber("doctorname")%> </option>
	                        <%  rs_prescriber.MoveNext
		                wend
		            end if
                    rs_prescriber.close
	            %>
                <%
                end if
                 %>
            </select>
            &nbsp; (Only show prescribers who have DEA Number)
        </td>
    </tr>
    <tr>
        <td colspan="3" height="75">
        <p>
        <hr size="1" color="#8CA5B5">
        <input type="button" value="Add EPCS Control" onclick="addEPCSControl()"/>
        </td>
    </tr>
</table>

<br><br><br>

<%
Response.Write "<b><u>EPCS access control</u></b>" &"<br><br>"
set rs20 = server.CreateObject("adodb.recordset")

dim queryStr
queryStr = "select t1.userid, t2.docuser, t4.orgname, case t1.epcs_access when 'Y' then 'Approved' else 'Pending' end as epcs_state " & _
            " from user_pwd t1 " & _
            " inner join doctortable t2 on t1.userid = t2.docuser " & _
            " inner join acc t3 on t1.userid = t3.userid " & _
            " inner join streetreg t4 on t3.streetadd = t4.streetadd " & _
            " where t1.epcs_access is not null "

rs20.Open queryStr, conn
%>

<table border="1">
    <tr bgcolor="#3366CC">
	    <td><font color="white">Facility</font></td>
	    <td><font color="white">Precriber</font></td>
	    <td><font color="white">EPCS Access Status</font></td>
	    <td>&nbsp;</td>
    </tr>
    <% while not rs20.EOF %>
	<tr>
		<td><%=rs20("orgname")%></td>
		<td><%=rs20("docuser")%></td>
		<td><%=rs20("epcs_state")%></td>
		<td><a href="ss_provider_admin_delete.asp?userid=<%=rs20("userid")%>">
			<img src="/images/practice/delete_login.gif" border="0" alt="Click here to delete" width="20" height="19" /></a>
		</td>
	</tr>
    <%
	    rs20.MoveNext
	    wend
    %>
<%
rs20.Close
end if
 %>
<script type="text/javascript">
    function togglePrescriberActive(action, prescriber) {
        var checked = false;
        if (action == 'True') checked = true;
        document.getElementById(prescriber + "_serviceLevel_0").disabled = false;
        document.getElementById(prescriber + "_serviceLevel_1").disabled = false;
        document.getElementById(prescriber + "_serviceLevel_0").checked = checked;
        document.getElementById(prescriber + "_serviceLevel_1").checked = checked;
    }
</script>

</body>
</html>
<%
end if
 %>