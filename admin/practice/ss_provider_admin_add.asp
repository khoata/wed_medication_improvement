<%@ Language=VBScript %>

<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/clsEPCSAuditLog.asp"-->

<%
dim epcsAuditLogObj
set epcsAuditLogObj = new clsEPCSAuditLog

userid= Request.QueryString("userid")

set cmd = server.CreateObject("adodb.command")
set cmd.ActiveConnection = CONN

strSQL = " update user_pwd set epcs_access = 'P' where userid = '" & userid & "'"
cmd.CommandText =  strSQL
cmd.Execute

'--START: Add to EPCS_Audit_Log
set rs = server.CreateObject("ADODB.Recordset")
set rs2 = server.CreateObject("ADODB.Recordset")
strSQL = "SELECT * FROM DOCTORTABLE WHERE upper(DOCUSER) = upper('" & userid & "')"
rs.open strSQL,con
if not (rs.eof or rs.bof) then
    strSQL2 = "SELECT orgname,streetadd FROM streetreg WHERE acc_status='V' and upper(streetadd) = upper('" & rs("Facility") & "')"
    rs2.open strSQL2,con
    if not (rs2.eof or rs2.bof) then
        detailAuditLog = "- Practice: " & rs2("orgname") & "<br/>" &_
                         "- Prescriber: " & rs("First_Name") & " " & rs("Last_Name") & "<br/>" &_
                         "- DEA Number: " & rs("DEA_NO") & "<br/>" &_
                         "- Added By: " & session("admin_userid") & ", Role: Admin WEBeDoctor" 
    end if 
    rs2.close
    
    strSQL2 = "SELECT userid, role FROM acc WHERE upper(streetadd)=upper('"&rs("facility")&"') and upper(userid) = upper('" & userid & "')"
    rs2.open strSQL2,con
    if not (rs2.eof or rs2.bof) then
        epcsAuditAuthor = rs2("userid")
        epcsAuditRole = rs2("role")
    end if
    rs2.close

    call epcsAuditLogObj.LogEPCSActivity_WebEdoctorAdmin("Logical Access Control","EPCS Access Control - Add Prescriber", epcsAuditAuthor, epcsAuditRole, detailAuditLog,"SUCCESS")
     
end if
rs.close
'--END

Response.Redirect "ss_provider_admin.asp?section=access_epcs"

%>