<%@ Language=VBScript %>

<!--#include file="../../library/connection.inc.asp"-->
<!--#include file="../../library/clsEPCSAuditLog.asp"-->

<%
dim epcsAuditLogObj
set epcsAuditLogObj = new clsEPCSAuditLog

userid= Request.QueryString("userid")

set rs = server.CreateObject("adodb.recordset")
rs.Open " update user_pwd set epcs_access = '' where userid = '" & userid & "'" ,CONN

if rs.State =1 then rs.Close
set rs= nothing

'--START: Add to EPCS_Audit_Log
set rs1 = server.CreateObject("ADODB.Recordset")
set rs2 = server.CreateObject("ADODB.Recordset")
strSQL = "SELECT * FROM DOCTORTABLE WHERE upper(DOCUSER) = upper('" & userid & "')"
rs1.open strSQL,con
if not (rs1.eof or rs1.bof) then
    strSQL2 = "SELECT orgname,streetadd FROM streetreg WHERE acc_status='V' and upper(streetadd) = upper('" & rs1("Facility") & "')"
    rs2.open strSQL2,con
    if not (rs2.eof or rs2.bof) then
        detailAuditLog = "- Practice: " & rs2("orgname") & "<br/>" &_
                         "- Prescriber: " & rs1("First_Name") & " " & rs1("Last_Name") & "<br/>" &_
                         "- DEA Number: " & rs1("DEA_NO") & "<br/>" &_
                         "- Deleted By: " & session("admin_userid") & ", Role: Admin WEBeDoctor"
    end if 
    rs2.close 
    
    strSQL2 = "SELECT userid, role FROM acc WHERE upper(streetadd)=upper('"&rs1("facility")&"') and upper(userid) = upper('" & userid & "')"
    rs2.open strSQL2,con
    if not (rs2.eof or rs2.bof) then
        epcsAuditAuthor = rs2("userid")
        epcsAuditRole = rs2("role")
    end if
    rs2.close

    call epcsAuditLogObj.LogEPCSActivity_WebEdoctorAdmin("Logical Access Control","EPCS Access Control - Delete Prescriber", epcsAuditAuthor, epcsAuditRole, detailAuditLog,"SUCCESS")

end if
rs1.close
'--END

Response.Redirect "ss_provider_admin.asp?section=access_epcs"

%>